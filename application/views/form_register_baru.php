<html>
<head>
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery-ui-1.10.3.css">
  <!--script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.js"></script-->
  <script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-ui-1.10.3.js"></script>
  <style type="text/css">
    #bingkailogin{
    border:2px solid #a1a1a1;
    border-radius:10px;
    padding:5px 5px; 
    box-shadow:0px 0px 6px 6px #ccc;
    margin-top: 40px;
    margin-left: 350px; 
    margin-right: 350px; 
    
    }
    #headerlogin{
      /*background-color: #428bca;*/
      color: white;
      border-radius:10px 10px 0px 0px;
      background: #1e5799; /* Old browsers */
      /* IE9 SVG, needs conditional override of 'filter' to 'none' */
      background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjI5JSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjczJSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM3ZGI5ZTgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
      background: -moz-linear-gradient(top,  #1e5799 0%, #2989d8 29%, #207cca 73%, #7db9e8 100%); /* FF3.6+ */
      background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(29%,#2989d8), color-stop(73%,#207cca), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
      background: -webkit-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
      background: -o-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* Opera 11.10+ */
      background: -ms-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* IE10+ */
      background: linear-gradient(to bottom,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* W3C */
      filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-8 */
      padding:10px 10px;
      margin-bottom: 20px;
    }
    #mainlogin{
      padding-left: 20px;
      padding-right: 20px;
    }
    #footerlogin{
      font-size: 10px;
    }
</style>
</head>
<body>
  <div id="bingkailogin">
    <div id="headerlogin">
      <img style="float:left; margin-right:20px;" src="<?php echo $base_url; ?>img/logokemenkes.png" width="100px">
      <h3>REGISTRASI</h3>APLIKASI LOGISTIK DAN PERBEKALAN<br>KEMENTRIAN KESEHATAN RI</h4>
    </div>
      <div id="mainlogin"><!-- konten halaman -->
        <form class="form-horizontal" role="form">
          <div class="form-group">
            <label class="col-sm-4 control-label">Username</label>
            <div class="col-sm-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kategori User</label>
            <div class="col-sm-8">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Administrator</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Nama Lengkap</label>
            <div class="col-sm-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Kontak Person</label>
            <div class="col-sm-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Password</label>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Konfirmasi Password</label>
            <div class="col-sm-8">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Status</label>
            <div class="col-sm-8">
              <div class="btn-group">
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                  Action <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Aktif</a></li>
                  <li><a href="#">Non-Aktif</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4 control-label">Reset Password</label>
            <div class="col-sm-8">
              <div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button">Go!</button>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
              <button type="submit" class="btn btn-default">Simpan</button>
            </div>
          </div>
        </form>
      </div><!-- End of konten halaman -->
    <div id="footerlogin">
      <hr><center>
      E-Logistics V1.0<br>
      Develeoped by: PT Sisfomedika<br>2014
      </center>
    </div>
  </div>
</body>
</html>
<script type="text/javascript">
    $(function(){
        $(".pagernya").on("click","a",function (event){
            event.preventDefault();
            var url = $(this).attr("href");
            //$("#pagin").load(url);
            $('#konten').load(url);
        });
    });
</script>
<style>
.master_data{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.list_master {
    float: left;
    padding: 1px;
}

.a_master:link, .a_master:visited {
    display: block;
    width: 40px;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #428bca;
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
    border-style: 1px solid;
    border-color: white;
}

.a_master:hover{
    background-color: red;
}
.a_master:active {
    background-color: red;
    display: block;
}
.pagernya{
    padding: 15px;
}
</style>
<div id="up-konten" class="panel panel-primary">
    <div class="panel-heading"><?php echo $title;?></div>
    <div class="panel-body" style="width: 70%">
        <br/>
        <button id="add" onclick="load_page()"><span class="glyphicon glyphicon-plus"></span> Kirim Laporan</button>
        <br/>
      <div id="pkm_list">
        <div class="pagernya">
            <?php echo $pager;?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr class="active">
                <th>Periode</th>
                <th>Tgl Dilaporkan</th>
                <th>Cara Kirim</th>
                <th>Pengirim</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
          <?php
          if(!empty($result))
          {
            foreach($result as $key => $value)
            { ?>
             <tr>
               <td><?php echo $value->periode;?></td>
               <td><?php echo $value->id;?></td>
               <td><?php echo $value->cara_kirim;?></td>
               <td><?php echo $value->username;?></td>
               <td><?php echo $value->status;?></td>
               <td>
                   <a id="adetail_<?php echo $key+1;?>" data-toggle="modal" href="<?php echo base_url().'index.php/integrasi/pkm2/getdetail/'.$value->namatable;?>" data-target="#detail_<?php echo $key+1;?>">Lihat</a>
                   <!--
                   <button href="#" id="detail_data_<?php echo $key+1;?>"><span class="glyphicon glyphicon-list"></span> Lihat</button>
                   -->
                   <script>
                       $('#adetail_<?php echo $key+1;?>').on('click', function(e) {

                           // From the clicked element, get the data-target arrtibute
                           // which BS3 uses to determine the target modal
                           var target_modal = $(e.currentTarget).data('target');
                           // also get the remote content's URL
                           var remote_content = e.currentTarget.href;

                           // Find the target modal in the DOM
                           var modal = $(target_modal);
                           // Find the modal's <div class="modal-body"> so we can populate it
                           var modalBody = $(target_modal + ' .modal-body');
                           $('.modal-body').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>')
                           // Capture BS3's show.bs.modal which is fires
                           // immediately when, you guessed it, the show instance method
                           // for the modal is called
                           modal.on('show.bs.modal', function () {
                               // use your remote content URL to load the modal body
                               modalBody.load(remote_content);
                           }).modal();
                           // and show the modal

                           // Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
                           // from throwing a 'preventDefault' error due to us overriding the anchor usage.
                           return false;
                       });
                   </script>
               </td>
             </tr>

                <div id="detail_<?php echo $key+1;?>" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 id="termsLabel" class="modal-title">Detail Data</h3>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            <?php
            }
          }?>
            </tbody>
        </table>
            <?php //echo $pager;?>
      </div>
    </div>
</div>

<!-- table wait template -->
<table class="template_table_search" style="display: none;">
    <thead>
    <tr class="active">
        <th>Periode</th>
        <th>Tgl Dilaporkan</th>
        <th>Cara Kirim</th>
        <th>Pengirim</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody class="wait_body">
            <tr>
                <td class='periode'></td>
                <td class='tgldilaporkan'></td>
                <td class='carakirim'></td>
                <td class='pengirim'></td>
                <td class='status'></td>
                <td>
                    <a id="adetail_t_1" data-toggle="modal" href="<?php echo base_url().'index.php/integrasi/pkm/getdetail/';?>" data-target="#detail_t_1">Lihat</a>
                    <!--
                   <button href="#" id="detail_data_<?php echo $key+1;?>"><span class="glyphicon glyphicon-list"></span> Lihat</button>
                   -->
                    <script>
                        $('#adetail_t_1').on('click', function(e) {

                            // From the clicked element, get the data-target arrtibute
                            // which BS3 uses to determine the target modal
                            var target_modal = $(e.currentTarget).data('target');
                            // also get the remote content's URL
                            var remote_content = e.currentTarget.href;

                            // Find the target modal in the DOM
                            var modal = $(target_modal);
                            // Find the modal's <div class="modal-body"> so we can populate it
                            var modalBody = $(target_modal + ' .modal-body');
                            $('.modal-body').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>')
                            // Capture BS3's show.bs.modal which is fires
                            // immediately when, you guessed it, the show instance method
                            // for the modal is called
                            modal.on('show.bs.modal', function () {
                                // use your remote content URL to load the modal body
                                modalBody.load(remote_content);
                            }).modal();
                            // and show the modal

                            // Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
                            // from throwing a 'preventDefault' error due to us overriding the anchor usage.
                            return false;
                        });
                    </script>
                </td>
            </tr>

            <div id="detail_t_1" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 id="termsLabel" class="modal-title">Detail Data</h3>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    </tbody>
</table>

<script type="text/javascript">
    function load_page()
    {
        $('#up-konten').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>');
        $('#up-konten').load('<?php echo base_url()."index.php/integrasi/pkm2";?>');
    }

$(document).ready(function() {
    $("#pager a").bind('click', function(eve)
    {
        eve.preventDefault();
        var cur = parseInt($("#pager .current").html());
        var klik = parseInt($(this).html());
        var link = $(this).attr('href');
        if(!isNaN(klik))
        {
            ori = (klik < cur)? "kiri" : "kanan";
        }
        else
        {
            ori = ($(this).html() == "‹ First" || $(this).html() == "&lt;")? "kiri" : "kanan";
        }

        $.ajax({
            cache: false,
            url: link,
            type: "POST",
            dataType: "json",
            beforeSend: function(){
                showBusySubmit('pkm_list');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status === 401) {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(data)
            {
                updatePage(data,ori);
            }
        });
        return false;
    });

    function updatePage(data,ori)
    {
        var busyarea = "#pkm_list";
        var id = "#pager";


        if(ori == "kanan")
        {
            $(busyarea).unblock();
            $(busyarea + ' table').hide("drop", { direction: "left" }, 500);

            var obj = $(".template_table_search").clone();
            obj.removeClass("template_table_search");
            obj.find('tbody.wait_body tr').remove();
            $.each(data.table, function(i, v){
                var objtr = $(".template_table_search tbody tr:first").clone();
                var objtrdetail = $(".template_table_search tbody tr#show_detail").clone();
                objtr.find('.periode').html(v.periode);
                objtr.find('.tgldilaporkan').html(v.id);
                objtr.find('.carakirim').html(v.cara_kirim);
                objtr.find('.pengirim').html(v.username);
                objtr.find('.status').html(v.status);

                //objtrdetail.attr("id","detail" + v.trdetail);
                //objtrdetail.find('td').attr("id","detailtd" + v.trdetail);
                objtr.appendTo(obj.find('tbody.wait_body'));
                objtrdetail.appendTo(obj.find('tbody.wait_body'));
            })

            obj.removeClass("wait_body");
            obj.show();
            $(busyarea + ' table').html(obj.children()).show("drop", { direction: "right" }, 500);
            //$(busyarea + ' .pagination .results').html(data.paging_result);
            $(busyarea + ' .pagination ' + id).html(data.result);
        }
        else if(ori == "kiri")
        {
            $(busyarea).unblock();
            $(busyarea + ' table').hide("drop", { direction: "right" }, 500);

            var obj = $(".template_table_search").clone();
            obj.removeClass("template_table_search");
            obj.find('tbody.wait_body tr').remove();
            $.each(data.table, function(i, v){
                var objtr = $(".template_table_search tbody tr:first").clone();
                var objtrdetail = $(".template_table_search tbody tr#show_detail").clone();
                objtr.find('.kodebookingwait').html(v.kodebooking);
                objtr.find('.resume').html(v.resume);
                objtr.find('.price').html(v.price);
                objtr.find('.timelimit').html(v.timelimit);
                objtr.find('.action_book').html(v.action_book);
                objtrdetail.attr("id","detail" + v.trdetail);
                objtrdetail.find('td').attr("id","detailtd" + v.trdetail);
                objtr.appendTo(obj.find('tbody.wait_body'));
                objtrdetail.appendTo(obj.find('tbody.wait_body'));
            })

            obj.removeClass("wait_body");
            obj.show();
            $(busyarea + ' table').html(obj.children()).show("drop", { direction: "left" }, 500);
            $(busyarea + ' .pagination .results').html(data.paging_result);
            $(busyarea + ' .pagination ' + id).html(data.search_pager);
        }
    }

    $("#proses").click(function()
    {
        var datapost = $('#form_integrasi').serialize();
        $.ajax({
            cache: false,
            url: "<?php echo base_url();?>index.php/integrasi/pkm/process",
            type: "POST",
            dataType: "json",
            data: datapost,
            beforeSend: function(){
                showBusySubmit();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // XMLHttpRequest.responseText has your json string
                // XMLHttpRequest.status has the 401 status code
                if (XMLHttpRequest.status === 401)
                {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(html)
            {
                if(html.error == "failed")
                    ShowMessage(html.msg);
                else
                {
                    alert(html.msg);
                }
                $('#form_integrasi').unblock();
                $.unblockUI();
            }
        });
    });

    function showBusySubmit(id){
        $('#' + id).block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }

    function ShowMessage(msg)
    {
        $('#dvmsg').append('<p class="bg-warning"><strong>Error!</strong> ' + msg + '</p>');
        $('.bg-warning').hide().fadeIn(3000).delay(3000).fadeOut(3000,function(){$('.bg-warning').remove()});
    }
});
</script>
<table class="table table-striped table-bordered">
    <thead>
    <tr class="active">
        <th>Kode Obat</th>
        <th>Nama Obat</th>
        <th>Sediaan</th>
        <th>Jumlah Pemberian</th>
        <th>Unit Penerima</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $key => $value)
    { ?>
    <tr>
        <td><?php echo $value->kode_obat;?></td>
        <td><?php echo $value->nama_obj;?></td>
        <td><?php echo $value->sediaan;?></td>
        <td><?php echo $value->pemberian;?></td>
        <td><?php echo $value->unit_penerima;?></td>
    </tr>
    <?php
    } ?>
    </tbody>
</table>
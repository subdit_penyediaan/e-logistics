<table class="table table-striped table-bordered">
    <thead>
    <tr class="active">
        <th>Kode Obat</th>
        <th>Nama Obat</th>
        <th>Stok Akhir</th>
        <th>Penggunaan Rata-rata(3 bulan terakhir)</th>
        <th>Ketersediaan (bulan)</th>
        <th>Kelompok</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $key => $value)
    { ?>
    <tr>
        <td><?php echo $value->kode_obat;?></td>
        <td><?php echo $value->nama_obj;?></td>
        <td><?php echo $value->jml_stok;?></td>
        <td><?php echo $value->jml_penggunaan;?></td>
        <td><?php echo $value->cur_mont;?></td>
        <td><?php echo $value->kelompok;?></td>
    </tr>
    <?php
    } ?>
    </tbody>
</table>
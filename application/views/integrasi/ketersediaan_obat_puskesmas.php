 <style>
	
</style>
<script>
$(document).ready(function(){
	//button group filter
	$("#btn_show").click(function(){
		var url2="integrasi/ketersediaan_obat_puskesmas/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			kategori:$("#kategori").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				
				$("#list_ketersediaan_obat").html(e);//+"#list_sedian_obat");
			}
		});
	})

	//default integrasi rata-rata penggunaan 3 bulan sebelumnya
	//var url="integrasi/ketersediaan_obat_puskesmas/get_data_ketersediaan_obat";
	//$('#list_ketersediaan_obat').load(url);	

	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		var urlprint="integrasi/ketersediaan_obat_puskesmas/printOut";
		window.open(urlprint);		
	})
	//end eksport button
	
});
</script>
<div class="panel panel-primary" id="halaman_ketersediaan_obat">
	<div class="panel-heading">integrasi Ketersediaan Obat Puskesmas</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
	<br>
				<table class="table">
						<tr>						
							<td>dari bulan</td>
							<td><div class="input-append date" id="datepicker" data-date="2014-04-15" data-date-format="mm" >
										<input class="span2" size="12" type="text" value="<?php //echo date('Y-m-d');?>" id="periodeawal" readonly="readonly" >
										<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>

									<script>
									$('#datepicker').datepicker(
										{
										    format: "yyyy-mm",
										    viewMode: "months", 
										    minViewMode: "months"
										});
									</script>

							</td>
						</tr>
						<tr>							
							<td>sampai bulan</td>
							<td><div class="input-append date" id="datepicker2" data-date="2014-04-15" data-date-format="yyyy-mm-dd" >
										<input class="span2" size="12" type="text" value="<?php //echo date('Y-m-d');?>"  id="periodeakhir" readonly="readonly">
										<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>

									<script>
									$('#datepicker2').datepicker(
										{
										    format: "yyyy-mm",
										    viewMode: "months", 
										    minViewMode: "months"
										});
									</script>
										
							</td>
						</tr>		
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
								</div>
							</td>
						</tr>
					</table>
				<div id="list_ketersediaan_obat"></div>
			</div>
			<!--/form-->

	</div><!-- end div panel conten -->
</div><!-- end div panel -->
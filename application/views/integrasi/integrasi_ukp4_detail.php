<table class="table table-striped table-bordered">
    <thead>
    <tr class="active">
        <th>Nomer</th>
        <th>Kode UKP4</th>
        <th>Nama Obat</th>
        <th>Perencanaan<br>Kebutuhan</th>
        <th>Stok Akhir</th>
        <th>Penggunaan</th>
        <th>Total</th>
        <th>Ketersediaan (%)</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $key => $value)
    { ?>
    <tr>
        <td><?php echo $value->id;?></td>
        <td><?php echo $value->id_ukp4;?></td>
        <td><?php echo $value->nama_ukp4;?></td>
        <td><?php echo $value->perencanaan;?></td>
        <td><?php echo $value->stok;?></td>
        <td><?php echo $value->pemberian;?></td>
        <td><?php echo $value->jumlah;?></td>
        <td><?php echo $value->ketersediaan;?></td>
    </tr>
    <?php
    } ?>
    </tbody>
</table>
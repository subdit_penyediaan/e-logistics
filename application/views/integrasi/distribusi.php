<div class="panel panel-primary">
    <div class="panel-heading">Distribusi Obat</div>
    <div id="up-konten"class="panel-body" style="width: 70%">
        <div id="dvmsg"></div>
        <br>
        <form class="form-horizontal" role="form" id="form-integrasi" method="post" action="<?php echo site_url('integrasi/distribusi/process');?>">
            <div class="form-group">
                <label for="datepicker" class="col-sm-2 control-label">Periode</label>
                <div class="col-sm-3">
                    <div class="input-group input-append date" id="datepicker" data-date="<?php echo date('d-m-Y')?>" data-date-format="mm-yyyy" >
                        <input class="form-control span2" size="12" type="text" readonly="readonly" name="periode" id="tanggal" required="required" value="<?php echo date('m-Y')?>">
                        <span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="jenis_data" class="col-sm-2 control-label">Jenis Data</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" readonly="readonly" value="distribusi obat">
                    <input type="hidden" value="distribusi" name="jenis_data" id="jenis_data">
                </div>
            </div>
            <div class="form-group">
                <label for="jenis_data" class="col-sm-2 control-label">Metode</label>
                <div class="col-sm-6">
                    <label class="radio-inline">
                        <input type="radio" name="metode_kirim" id="metode_kirim_1" value="online" checked>Web Services
                        <span class="help-block" style="font-size: smaller">Server yang digunakan&nbsp;:&nbsp;<?php echo $bank_data_server1;?></span>
                        <span class="help-block" style="font-size: smaller">Silahkan Test Koneksi dengan server dengan klik tombol <a href="#">ini</a></span>
                    </label>
                    <br/>
                    <label class="radio-inline">
                        <input type="radio" name="metode_kirim" id="metode_kirim_2" value="csv">CSV
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                    <button type="button" class="btn btn-success btn-sm" id="proses">Proses</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $("#proses").click(function() {
        var datapost = $('#form-integrasi').serialize();
        $.ajax({
            cache: false,
            url: "<?php echo base_url();?>index.php/integrasi/distribusi/process",
            type: "POST",
            dataType: "json",
            data: datapost,
            beforeSend: function(){
                showBusySubmit();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status === 401)
                {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(html)
            {
                if(html.error == "failed")
                    ShowMessage(html.msg);
                else
                {
                    alert(html.msg);
                    if(html.url!="-")
                    {
                     location.replace('<?php echo base_url()."index.php/integrasi/distribusi/downloadfile/";?>' + html.jenis_data);
                    }
                    $('#up-konten').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>');
                    $('#konten').load('<?php echo base_url()."index.php/integrasi/distribusi_list";?>');
                }
                $('#form-integrasi').unblock();
                $.unblockUI();
            }
        });
    });

    $('#datepicker').datepicker({
        format: "mm-yyyy",
        viewMode: "months", 
        minViewMode: "months"
    });

    function showBusySubmit(){
        $('#form-integrasi').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }

    function ShowMessage(msg)
    {
        $('#dvmsg').append('<div class="alert alert-danger"><p class=""><strong>Error!</strong> ' + msg + '</p></div>');
        $('.alert-danger').hide().fadeIn(3000).delay(3000).fadeOut(3000,function(){$('.alert-danger').remove()});
    }
</script>
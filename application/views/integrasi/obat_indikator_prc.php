<style>
.master_data{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.list_master {
    float: left;
    padding: 1px;
}

.a_master:link, .a_master:visited {
    display: block;
    width: 40px;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #428bca;
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
    border-style: 1px solid;
    border-color: white;
}

.a_master:hover{
    background-color: red;
}
.a_master:active {
    background-color: red;
    display: block;
}
</style>
<div class="panel panel-primary">
    <div class="panel-heading">Obat Indikator Puskesmas</div>
    <div id="up-konten"class="panel-body" style="width: 70%">
        <div id="dvmsg"></div>
        <!-- bag. isi -->
        <br>
        <form class="form-horizontal" role="form" id="form_obat_indikator" method="post" action="<?php echo site_url('integrasi/obat_indikator_prc/process');?>">
            <div class="form-group">
                <label for="datepicker" class="col-sm-2 control-label">Periode</label>
                <div class="col-sm-3">
                    <div class="input-group input-append date" id="datepicker" data-date="<?php echo date('d-m-Y')?>" data-date-format="mm-yyyy" >
                        <input class="form-control span2" size="12" type="text" readonly="readonly" name="periode" id="tanggal" required="required" value="<?php echo date('m-Y')?>">
                        <span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>

                <script>
                $('#datepicker').datepicker({
                        format: "mm-yyyy",
                        viewMode: "months", 
                        minViewMode: "months"
                    });
                </script>
            </div>
            <div class="form-group">
                <label for="jenis_data" class="col-sm-2 control-label">Jenis Data</label>
                <div class="col-sm-3">
                    <input type="text" class="form-control" readonly="readonly" value="Obat Indikator Puskesmas">
                    <input type="hidden" value="obatindikator" name="jenis_data" id="jenis_data">
                    <!--select class="form-control" name="jenis_data" id="jenis_data">
                        <option value="">------ Pilih ------</option>
                        <?php
                        foreach($jenis_data as $key => $value) :
                            ?>
                            <option value="<?php echo $key; ?>">
                                <?php echo $value; ?>
                            </option>
                        <?php endforeach; ?>
                    </select-->
                </div>
            </div>
            <div class="form-group">
                <label for="jenis_data" class="col-sm-2 control-label">Metode</label>
                <div class="col-sm-6">
                    <label class="radio-inline">
                        <input type="radio" name="metode_kirim" id="metode_kirim_1" value="online" checked>Web Services
                        <span class="help-block" style="font-size: smaller">Server yang digunakan&nbsp;:&nbsp;<?php echo $bank_data_server1;?></span>
                        <span class="help-block" style="font-size: smaller">Silahkan Test Koneksi dengan server dengan klik tombol <a href="#">ini</a></span>
                    </label>
                    <br/>
                    <label class="radio-inline">
                        <input type="radio" name="metode_kirim" id="metode_kirim_2" value="csv">CSV
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-3">
                    <button type="button" class="btn btn-success btn-sm" id="proses">Proses</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $("#proses").click(function()
    {
        var datapost = $('#form_obat_indikator').serialize();
        $.ajax({
            cache: false,
            url: "<?php echo base_url();?>index.php/integrasi/obat_indikator_prc/process",
            type: "POST",
            dataType: "json",
            data: datapost,
            beforeSend: function(){
                showBusySubmit();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // XMLHttpRequest.responseText has your json string
                // XMLHttpRequest.status has the 401 status code
                if (XMLHttpRequest.status === 401)
                {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(html)
            {
                if(html.error == "failed")
                    ShowMessage(html.msg);
                else
                {
                    alert(html.msg);
                    if(html.url!="-")
                    {
                     location.replace('<?php echo base_url()."index.php/integrasi/obat_indikator_prc/downloadfile/";?>' + html.jenis_data);
                    }
                    $('#up-konten').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>');
                    $('#konten').load('<?php echo base_url()."index.php/integrasi/obat_indikator";?>');
                }
                $('#form_obat_indikator').unblock();
                $.unblockUI();
            }
        });
    });

    function showBusySubmit(){
        $('#form_obat_indikator').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }

    function ShowMessage(msg)
    {
        $('#dvmsg').append('<p class="bg-warning"><strong>Error!</strong> ' + msg + '</p>');
        $('.bg-warning').hide().fadeIn(3000).delay(3000).fadeOut(3000,function(){$('.bg-warning').remove()});
    }
</script>
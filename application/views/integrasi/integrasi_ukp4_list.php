<script type="text/javascript">
    $(function(){
        $(".pagernya").on("click","a",function (event){
            event.preventDefault();
            var url = $(this).attr("href");
            //$("#pagin").load(url);
            $('#konten').load(url);
        });
    });
</script>
<style>
.pagernya{
    padding: 15px;
}
.master_data{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.list_master {
    float: left;
    padding: 1px;
}

.a_master:link, .a_master:visited {
    display: block;
    width: 40px;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #428bca;
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
    border-style: 1px solid;
    border-color: white;
}

.a_master:hover{
    background-color: red;
}
.a_master:active {
    background-color: red;
    display: block;
}
</style>
<div id="up-konten" class="panel panel-primary">
    <div class="panel-heading"><?php echo $title;?></div>
    <div class="panel-body" style="width: 70%">
        <!--button id="add" onclick="load_page()"><span class="glyphicon glyphicon-plus"></span></button-->
        <br/>
      <div id="integrasi_list">
        <div class="pagernya">
            <?php echo $pager;?>
        </div>
        <table class="table table-striped table-bordered">
            <thead>
            <tr class="active">
                <th>Periode</th>
                <th>Tgl Dilaporkan</th>
                <th>Cara Kirim</th>
                <th>Pengirim</th>
                <th>Status</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="tabel_konten">
          <?php
          if(!empty($result))
          {
            foreach($result as $key => $value)
            { ?>
             <tr class="index_baris">
               <td><?php echo $value->periode;?>
                    <input type="hidden" value="<?php echo $value->periode;?>" id="periode_lap_<?php echo $key;?>"></td>
               <td><?php echo $value->id;?>
                    <input type="hidden" value="<?php echo $value->id;?>" id="id_lap_<?php echo $key;?>" class="input_hidden"></td>
               <td><?php echo $value->cara_kirim;?></td>
               <td><?php echo $value->username;?></td>
               <td><?php echo $value->status;?></td>
               <td>
                   <?php if($value->status=='pending'): ?>
                   <button class="btn_upload"><span class="glyphicon glyphicon-upload"></span> Upload</button> 
                    <!-- id="adetail_<?php echo $key+1;?>" data-toggle="modal" href="<?php echo base_url().'index.php/integrasi/integrasi_ukp4/proses/'.$value->id;?>" data-target="#detail_<?php echo $key+1;?>">Kirim</a--> 
                   <?php else: ?>
                   <a id="adetail_<?php echo $key+1;?>" data-toggle="modal" href="<?php echo base_url().'index.php/integrasi/integrasi_ukp4/getdetail/'.$value->namatable;?>" data-target="#detail_<?php echo $key+1;?>" class="btn_upload">Lihat</a>
                   <?php endif; ?>
                   <!--
                   <button href="#" id="detail_data_<?php echo $key+1;?>"><span class="glyphicon glyphicon-list"></span> Lihat</button>
                   -->
                   <script>
                       $('#adetail_<?php echo $key+1;?>').on('click', function(e) {
                            //$(this).removeClass("btn_upload");
                           // From the clicked element, get the data-target arrtibute
                           // which BS3 uses to determine the target modal
                           var target_modal = $(e.currentTarget).data('target');
                           // also get the remote content's URL
                           var remote_content = e.currentTarget.href;

                           // Find the target modal in the DOM
                           var modal = $(target_modal);
                           // Find the modal's <div class="modal-body"> so we can populate it
                           var modalBody = $(target_modal + ' .modal-body');
                           $('.modal-body').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>')
                           // Capture BS3's show.bs.modal which is fires
                           // immediately when, you guessed it, the show instance method
                           // for the modal is called
                           modal.on('show.bs.modal', function () {
                               // use your remote content URL to load the modal body
                               modalBody.load(remote_content);
                           }).modal();
                           // and show the modal

                           // Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
                           // from throwing a 'preventDefault' error due to us overriding the anchor usage.
                           return false;
                       });
                   </script>
               </td>
             </tr>

                <div id="detail_<?php echo $key+1;?>" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3 id="termsLabel" class="modal-title">Detail Data</h3>
                            </div>
                            <div class="modal-body">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            <?php
            }
          }?>
            </tbody>
        </table>
            <?php //echo $pager;?>
      </div>
    </div>
</div>

<!-- table wait template -->
<table class="template_table_search" style="display: none;">
    <thead>
    <tr class="active">
        <th>Periode</th>
        <th>Tgl Dilaporkan</th>
        <th>Cara Kirim</th>
        <th>Pengirim</th>
        <th>Status</th>
        <th></th>
    </tr>
    </thead>
    <tbody class="wait_body">
            <tr>
                <td class='periode'></td>
                <td class='tgldilaporkan'></td>
                <td class='carakirim'></td>
                <td class='pengirim'></td>
                <td class='status'></td>
                <td>
                    <a id="adetail_t_1" data-toggle="modal" href="<?php echo base_url().'index.php/integrasi/integrasi/getdetail/';?>" data-target="#detail_t_1">Lihat</a>
                    <!--
                   <button href="#" id="detail_data_<?php echo $key+1;?>"><span class="glyphicon glyphicon-list"></span> Lihat</button>
                   -->
                    <script>
                        $('#adetail_t_1').on('click', function(e) {
                            //$(this).removeClass("btn_upload");
                            // From the clicked element, get the data-target arrtibute
                            // which BS3 uses to determine the target modal
                            var target_modal = $(e.currentTarget).data('target');
                            // also get the remote content's URL
                            var remote_content = e.currentTarget.href;

                            // Find the target modal in the DOM
                            var modal = $(target_modal);
                            // Find the modal's <div class="modal-body"> so we can populate it
                            var modalBody = $(target_modal + ' .modal-body');
                            $('.modal-body').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>')
                            // Capture BS3's show.bs.modal which is fires
                            // immediately when, you guessed it, the show instance method
                            // for the modal is called
                            modal.on('show.bs.modal', function () {
                                // use your remote content URL to load the modal body
                                modalBody.load(remote_content);
                            }).modal();
                            // and show the modal

                            // Now return a false (negating the link action) to prevent Bootstrap's JS 3.1.1
                            // from throwing a 'preventDefault' error due to us overriding the anchor usage.
                            return false;
                        });
                    </script>
                </td>
            </tr>

            <div id="detail_t_1" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 id="termsLabel" class="modal-title">Detail Data</h3>
                        </div>
                        <div class="modal-body">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    </tbody>
</table>

<!--
<table class="template_table_search" style="display: none;">
    <thead>
    <tr>
        <th class="left" colspan="2" style="text-align: center;">Kode Booking</th>
        <th>Flight</th>
        <th>Price</th>
        <th class="selected last">Action</th>
    </tr>
    </thead>
    <tbody class="wait_body">
    <tr>
        <td class='kodebookingwait'></td>
        <td class="resume"></td>
        <td class="price"></td>
        <td class="timelimit"></td>
        <td class="last action_book"></td>
    </tr>
    <tr id="show_detail" style="display: none;" class="hide" style="display: table-row;">
        <td id="show_detail_td" colspan="5" class="last" style="text-align: center;"></td>
    </tr>
    </tbody>
</table>
-->
<!-- end Search Result -->


<script type="text/javascript">
    function load_page()
    {
        $('#up-konten').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>');
        $('#up-konten').load('<?php echo base_url()."index.php/integrasi/integrasi";?>');
    }

$(document).ready(function() {
    $('button.btn_upload').on('click', function(e) {
        //var testindex = $(".btn_upload").index(this);
        //var index=$(".index_baris").each();
        var testindex = $(".btn_upload").index(this);
        var online = "online";var offline="offline";
        var url ="<?php echo base_url(); ?>index.php/integrasi/integrasi_ukp4/process";
        var nilai = $("#id_lap_"+testindex).val();
        //alert(nilai);
        
        var metode = confirm("Apakah Anda akan menggunakan cara online?");
        if(metode){
            var form_data={
                nilai : $("#id_lap_"+testindex).val(),
                periode_ukp4 : $("#periode_lap_"+testindex).val(),
                metode_kirim : online}
        }else{
            var form_data={
                nilai : $("#id_lap_"+testindex).val(),
                periode_ukp4 : $("#periode_lap_"+testindex).val(),
                metode_kirim : offline}
        }

        //alert(form_data.metode_kirim+"<br>"+form_data.nilai);
        //alert(url);
        $.ajax({
            type:"POST",
            url:url,
            data: form_data,
            dataType:'json',
            success:function(html)
            {
                //alert("success upload");
            
                if(html.error == "failed")
                    ShowMessage(html.msg);
                else
                {
                    alert(html.msg);
                    //$('#konten').load('integrasi/integrasi_ukp4_list');
                    if(html.url!="-")
                    {
                     location.replace('<?php echo base_url()."index.php/integrasi/integrasi_ukp4/downloadfile/";?>' + html.jenis_data);
                    }
                    //$('#up-konten').html('<div style="text-align:center"><img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>');
                    $('#konten').load('integrasi/integrasi_ukp4_list');
                }
                //$('#form_integrasi').unblock();
                //$.unblockUI();
            }
                
        })
    })

    $("#pager a").bind('click', function(eve)
    {
        eve.preventDefault();
        var cur = parseInt($("#pager .current").html());
        var klik = parseInt($(this).html());
        var link = $(this).attr('href');
        if(!isNaN(klik))
        {
            ori = (klik < cur)? "kiri" : "kanan";
        }
        else
        {
            ori = ($(this).html() == "‹ First" || $(this).html() == "&lt;")? "kiri" : "kanan";
        }

        $.ajax({
            cache: false,
            url: link,
            type: "POST",
            dataType: "json",
            beforeSend: function(){
                showBusySubmit('integrasi_list');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status === 401) {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(data)
            {
                updatePage(data,ori);
            }
        });
        return false;
    });

    function updatePage(data,ori)
    {
        var busyarea = "#integrasi_list";
        var id = "#pager";


        if(ori == "kanan")
        {
            $(busyarea).unblock();
            $(busyarea + ' table').hide("drop", { direction: "left" }, 500);

            var obj = $(".template_table_search").clone();
            obj.removeClass("template_table_search");
            obj.find('tbody.wait_body tr').remove();
            $.each(data.table, function(i, v){
                var objtr = $(".template_table_search tbody tr:first").clone();
                var objtrdetail = $(".template_table_search tbody tr#show_detail").clone();
                objtr.find('.periode').html(v.periode);
                objtr.find('.tgldilaporkan').html(v.id);
                objtr.find('.carakirim').html(v.cara_kirim);
                objtr.find('.pengirim').html(v.username);
                objtr.find('.status').html(v.status);

                //objtrdetail.attr("id","detail" + v.trdetail);
                //objtrdetail.find('td').attr("id","detailtd" + v.trdetail);
                objtr.appendTo(obj.find('tbody.wait_body'));
                objtrdetail.appendTo(obj.find('tbody.wait_body'));
            })

            obj.removeClass("wait_body");
            obj.show();
            $(busyarea + ' table').html(obj.children()).show("drop", { direction: "right" }, 500);
            //$(busyarea + ' .pagination .results').html(data.paging_result);
            $(busyarea + ' .pagination ' + id).html(data.result);
        }
        else if(ori == "kiri")
        {
            $(busyarea).unblock();
            $(busyarea + ' table').hide("drop", { direction: "right" }, 500);

            var obj = $(".template_table_search").clone();
            obj.removeClass("template_table_search");
            obj.find('tbody.wait_body tr').remove();
            $.each(data.table, function(i, v){
                var objtr = $(".template_table_search tbody tr:first").clone();
                var objtrdetail = $(".template_table_search tbody tr#show_detail").clone();
                objtr.find('.kodebookingwait').html(v.kodebooking);
                objtr.find('.resume').html(v.resume);
                objtr.find('.price').html(v.price);
                objtr.find('.timelimit').html(v.timelimit);
                objtr.find('.action_book').html(v.action_book);
                objtrdetail.attr("id","detail" + v.trdetail);
                objtrdetail.find('td').attr("id","detailtd" + v.trdetail);
                objtr.appendTo(obj.find('tbody.wait_body'));
                objtrdetail.appendTo(obj.find('tbody.wait_body'));
            })

            obj.removeClass("wait_body");
            obj.show();
            $(busyarea + ' table').html(obj.children()).show("drop", { direction: "left" }, 500);
            $(busyarea + ' .pagination .results').html(data.paging_result);
            $(busyarea + ' .pagination ' + id).html(data.search_pager);
        }
    }

    $("#proses").click(function()
    {
        var datapost = $('#form_integrasi').serialize();
        $.ajax({
            cache: false,
            url: "<?php echo base_url();?>index.php/integrasi/integrasi/process",
            type: "POST",
            dataType: "json",
            data: datapost,
            beforeSend: function(){
                showBusySubmit();
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // XMLHttpRequest.responseText has your json string
                // XMLHttpRequest.status has the 401 status code
                if (XMLHttpRequest.status === 401)
                {
                    //location.href = base_url + 'session/member';
                }
            },
            success: function(html)
            {
                if(html.error == "failed")
                    ShowMessage(html.msg);
                else
                {
                    alert(html.msg);
                }
                $('#form_integrasi').unblock();
                $.unblockUI();
            }
        });
    });

    function showBusySubmit(id){
        $('#' + id).block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }

    function ShowMessage(msg)
    {
        $('#dvmsg').append('<p class="bg-warning"><strong>Error!</strong> ' + msg + '</p>');
        $('.bg-warning').hide().fadeIn(3000).delay(3000).fadeOut(3000,function(){$('.bg-warning').remove()});
    }
});
</script>
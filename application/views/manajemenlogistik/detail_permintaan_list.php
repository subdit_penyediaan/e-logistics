<script type="text/javascript">

	$(document).ready(function(){
		//tambah obat jenis lain
		    $('.add_loops').each(function(index){
		    	//alert(index);
		    	i=30*index;
		    	//i=index;
		    	$('#link_add_obat_'+index).click(function(){
		    		i++;
		    		var inputKodeObat = 
		    		$('<input type="hidden"/>')
		    			.attr('name', 'kode_obat[]')
		    			//.attr('size', '5');
		    			.attr('id', 'kode_obat_'+i);

		    		var inputKodeObatReq = 
		    		$('<input type="hidden"/>')
		    			.attr('name', 'kode_obat_req[]')
		    			//.attr('size', '5');
		    			.attr('id', 'kode_obat_req_'+i);

		    		var inputIdStok = 
		    		$('<input type="hidden"/>')
		    			.attr('name', 'id_stok[]')
		    			//.attr('size', '5');
		    			.attr('id', 'id_stok_'+i);

		    		var inputGive = 
		    		$('<input type="text"/>')
		    			.attr('name', 'jml_pemberian[]')
		    			.attr('size', '5');
		    			//.attr('id', 'no_batch_'+i)

		    		var inputBatch = 
		    		$('<input type="text"/>')
		    			.attr('name', 'no_batch[]')
		    			//.attr('size', '50')
		    			.attr('id', 'no_batch_'+i);

		    		var inputText = 
		    		$('<input type="text"/>')
		    			.attr('name', 'nama_obat[]')
		    			.attr('size', '50')
		    			.attr('id', 'nama_obat_'+i)
		    			.attr('class','looptext')
		    		//alert(index+" "+i);
		    		
		    		$('#add_other_drugs_'+index).append(inputText).append(inputKodeObat).append(inputIdStok);
		    		$('#add_other_batch_'+index).append(inputBatch);
		    		$('#add_other_give_'+index).append(inputGive);
		    		$('#add_other_drugs_req_'+index).append(inputKodeObatReq);
		    		inputText.autocomplete({
							source:'manajemenlogistik/distribusi_obat/get_list_obat', 
							minLength:2,
							focus: function( event, ui ) {
						        $( "#nama_obat_"+i ).val( ui.item.value +" "+ ui.item.pwr);
						        return false;
						      },
						    select:function(evt2, ui2)
							{
								console.log('#kode_obat_'+i);
								$('#kode_obat_'+i).val(ui2.item.kode_obat);
								$('#no_batch_'+i).val(ui2.item.batch);
								$('#id_stok_'+i).val(ui2.item.id_stok);
								$( "#nama_obat_"+i ).val( ui2.item.value +" "+ ui2.item.pwr);
								return false;
							}
						  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
						      return $( "<li>" )
						      	.data( "ui-autocomplete-item", item )
						        .append("<a>"+ item.value +" "+item.pwr + "<br><small>Stok: <i>" + item.stok + "</i><br>Expired date:"+ item.ed +"</small></a>")
						        .appendTo( ul );
						};
					return false;
		    		
		    	})
		    	//$('#add_other_drugs_'+index).append(inputText);
		    })

		//autocompale looping
		$('.looptext').each(function(index){
			$( "#nama_obat_"+index ).autocomplete({
				source:'manajemenlogistik/distribusi_obat/get_list_obat', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat_"+index ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{

					$('#kode_obat_'+index).val(ui.item.kode_obat);
					$('#no_batch_'+index).val(ui.item.batch);
					$('#id_stok_'+index).val(ui.item.id_stok);
					$('#jml_stok_'+index).val(ui.item.stok);
					return false;
				}
			})
		
			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small>Stok: <i>" + item.stok + "</i><br>Expired date:"+ item.ed +"</small></a>")
		        .appendTo( ul );
		    };
		    
		})
	 		
	//end autocomplete looping
	$('.looptext2').each(function(index){
		$('#no_barcode_'+index).autocomplete({
					source:'manajemenlogistik/distribusi_obat/get_list_obat_bar', 
					minLength:4,
					focus: function( event, ui ) {
				        $( "#nama_obat_"+index ).val( ui.item.value );
				        return false;
				      },
					select:function(evt, ui)
					{
						$('#kode_obat_'+index).val(ui.item.kode_obat);
						$('#no_batch_'+index).val(ui.item.batch);
						$('#id_stok_'+index).val(ui.item.id_stok);
						$('#jml_stok_'+index).val(ui.item.stok);
						return false;
						
					}
				})
	 		
	 		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small>Stok: <i>" + item.stok + "</i><br>Expired date:"+ item.ed +"</small></a>")
		        .appendTo( ul );
		    };

	})
	//end autocomplete

		$("#pagelistdetail_permintaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_detail_permintaan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});

		//form input
		$('#form_detail_distribusi').submit(function(e){
			e.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            dataType:'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                alert(data.confirm);
	                var url_done='manajemenlogistik/distribusi_obat/get_detail_permintaan_done/'+data.id_lplpo;
	                $('#pagelistdetail_permintaan').load(url_done);
	            }
			})
			
		});
		//end form input
	});

	var seq=0;
	function getMax(val){
		seq++;
		//var obj=$()
		var nilai1=val;
		var nilai2=$('#jml_stok_'+seq).val();

		nilai3= val-nilai2;
		$('#selisih_'+seq).val(nilai3);
	}
</script>
<div id="pagelistdetail_permintaan">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/distribusi_obat/input_data_distribusi" method="post" id="form_detail_distribusi" onkeypress="return event.keyCode != 13;">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>No.</th>
			<th>Obat Permintaan</th>
			<th>Sediaan</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Barcode Obat Pemberian</th>
			<th>Obat Pemberian</th>
			<th>No. Batch</th>
			<th>Pemberian</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=0;$j=1;
				foreach ($result as $rows) {
					echo '<tr style=""><td>'.$j.'</td>
					<td>'.$rows->nama_obat.' '.$rows->kekuatan.'
						<input type="hidden" value="'.$rows->kode_obat.'" name="kode_obat_req[]" id="kode_obat_req_'.$i.'">
						<input type="hidden" value="'.$rows->id_lplpo.'" name="id_lplpo[]">
						<div id="add_other_drugs_req_'.$i.'"></div></td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok_opt.'</td>
					<td>'.$rows->permintaan.'</td>
					<td><input type="text" size="10" name="barcode_obat[]" id="no_barcode_'.$i.'" class="looptext2"></td>
					<td><input type="text" size="50" name="nama_obat[]" id="nama_obat_'.$i.'" class="looptext">
					 	<input type="hidden" name="kode_obat[]" id="kode_obat_'.$i.'">
					 	<input type="hidden" name="id_stok[]" id="id_stok_'.$i.'">
					 	<input type="hidden" name="jml_stok[]" id="jml_stok_'.$i.'">
					 	<div id="add_other_drugs_'.$i.'"></div>
					 	<small><a href="javascript:void(0)" id="link_add_obat_'.$i.'" class="add_loops"><i>Tambah Obat Lain</i></a></small></td>
					<td><input type="text" name="no_batch[]" id="no_batch_'.$i.'">
						<div id="add_other_batch_'.$i.'"></div></td>
					<td><input type="text" size="5" name="jml_pemberian[]" id="jml_pemberian_'.$i.'">
						<div id="add_other_give_'.$i.'"></div></td></tr>';
					$i++;$j++;
				}
			?>
		</tbody>
	</table>
	<button><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
	</form>
</div>
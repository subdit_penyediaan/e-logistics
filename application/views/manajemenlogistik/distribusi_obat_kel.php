 <style>
.datepicker{
	z-index:1151;
}

select {
	border-radius: 0px ! important;
}
</style>
<script>
$(document).ready(function(){
	$("#form_ubah_data").validate({
		submitHandler: function(form){
			var url2="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/change_data";
			var data=$("#form_ubah_data").serialize();
			$.post(url2,data,function(){
				alert("Data berhasil diubah");
				$('#change_distribusi_obat').modal('toggle');
				var url="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/get_data_distribusi_obat";
				$('#list_distribusi_obat').load(url);
			})
		}
	})

	$('#cha_distribusi_obat-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');

	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	var form_data = { id_distribusi: id_array[0] };
	    	$.ajax({
	    		type:"POST",
	    		url:'<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/datatochange/',
	    		data: form_data,
	    		dataType:'json',
	    		success:function(data) {
	    			//$('#labelnofaktur').text(data[0].no_faktur);
	    			$('#cha_no_dok').val(data[0].no_dok);
	    			$('#cha_tanggal').val(data[0].tanggal);
	    			$('#cha_unit_eks').val(data[0].unit_eks);
	    			$('#cha_periode').val(data[0].periode);
	    			$('#cha_user').val(data[0].user);
	    			$('#cha_id_distribusi').val(data[0].id_distribusi);
	    			$('#cha_keperluan').val(data[0].keperluan);
	    			//$( "#cha_dana option:selected" ).text(data[0].dana).attr('value', data[0].dana);
	    			$('#change_distribusi_obat').modal('show');
	    			//return false;
	    		}
	    	});

	    }
	})

	$("#cito_internal").hide();
	$("#cito_eksternal").hide();
	//$("#selection").buttonset();
	$('input:radio[name="pilihan_cito"]').change(function(){
		if($(this).val()=="cito_internal"){
			$("#cito_internal").show();
			$("#cito_eksternal").hide();
		}else{
			$("#cito_internal").hide();
			$("#cito_eksternal").show();
		}
	});

	//=========== del button


	$("#del_distribusi_obat_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	datatype: "json",
	        	success: function(data){
	        		alert(data);
	        		var url_hasil="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/get_data_distribusi_obat"
					$("#list_distribusi_obat").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_distribusi_obat_modal').hide();
	$("#add_distribusi_obat_btn").on("click",function (event){
			//$("#add_distribusi_obat_modal").modal('show');
			$("#form_distribusi_obat_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_distribusi_obat_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_distribusi_obat_modal").modal('show');
			$("#form_distribusi_obat_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_distribusi_obat_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/getdatapermintaan";
	$('#daftarpermintaan').load(url);

	var url="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/get_data_distribusi_obat";
	$('#list_distribusi_obat').load(url);
	//============== submit add form

	$("#btn_add").click(function(){
		var url2="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/input_data";
		var form_data = {
			kec:$('#list_kec').val(),
			periode:$('#periode_cito').val(),
			unit_penerima:$('#nmpuskesmas').val(),
			id_dok:$('#no_dok').val(),
			user_penerima:$('#nama_penerima').val(),
			nip:$('#nip').val(),
			datenow:$('#datenow').val(),
			unit_luar:$('#unit_luar').val(),
			keperluan:$('#keperluan_cito').val(),
			unit:$('input:radio[name="pilihan_cito"]').val()
		}

		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			dataType: "json",
			success:function(res){
				alert(res.message);
				if (res.status == 'success') {
					var url_hasil="<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/get_data_distribusi_obat"
					$("#list_distribusi_obat").load(url_hasil);

					$('#list_kec').val("");
					$('#periode_cito').val("");
					$('#nmpuskesmas').val("");
					$('#no_dok').val("");
					$('#nama_penerima').val("");
					$('#nip').val("");
					$('#unit_luar').val("");
					$('#keperluan_cito').val("");
					$('#datenow').val("")
				}
			}
		});
	})

	//============== end submit add form
	$("#btn-delete-unex").on("click", function(e){
		e.preventDefault();
		var temp_id = $("#unit_luar").val();
		var id = temp_id.split("|");
		var url_success = '<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel';
    var confirmation = confirm("Apakah anda yakin?");

    if (confirmation) {
    		$.ajax({
    			url: '<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/delete_unex/'+id[0],
    			type: "GET",
    			dataType: "json",
    			success: function(res){
    	    		alert(res.message);
    	    		if (res.status == 'success') {
    	    			$('#konten').load(url_success);
    	    		}
    	    	}
    		})
    }
	})
});

//get data puskesmas
function get_puskesmas(val) {
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }else{
    	$.get('<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas').html(data);});
    }
}

//====== end get puskesmas
function get_unitexternal(val){
	if(val == 'add'){
    	var unex = prompt("Nama unit eksternal: ");
    	var data = {unex:unex};
    	var url = '<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel/input_unex';
    	var url_success = '<?= base_url(); ?>manajemenlogistik/distribusi_obat_kel';
    	if (unex != null) {
    		$.ajax({
    			url:url,
    			type: "POST",
    			data: data,
    			dataType: "json",
    			success: function(res){
		    		alert(res.message);
		    		if (res.status == 'success') {
		    			$('#konten').load(url_success);
		    		}
		    	}
    		})
    	} else {
    		$("#unit_luar").val('');
    	}
    }
}
//=================== add keperluan cito
function tambahkeperluan(val){
	if(val == 'add'){
    	prompt("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }
}
</script>
<div class="panel panel-primary" id="halaman_distribusi_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Distribusi Obat</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#listpermintaan" data-toggle="tab">List Permintaan</a></li>
		  <li class=""><a href="#cito" data-toggle="tab">Sewaktu</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="listpermintaan">
				<div id="daftarpermintaan">
					<table>
					</table>
				</div>
			</div>
			<div class="tab-pane" id="cito" style="">
			<br>
			<table class="table">
				<tr>
					<td colspan="4">
						<div class="row">
							<div class="col-md-3">
								<div class="input-group">
								    <span class="input-group-addon">
								        <input type="radio" name="pilihan_cito" value="cito_internal">
								      </span>
								      <input type="text" class="form-control" value="Unit Internal" size="8" readonly>
								</div>
							</div>
							<div class="col-md-3">
								<div class="input-group">
								      <span class="input-group-addon">
								        <input type="radio" name="pilihan_cito" value="cito_eksternal">
								      </span>
								      <input type="text" class="form-control" value="Unit Eksternal" size="10" readonly>
								</div>
							</div>
						</div>
					</td>
					<td>
				</tr>
					<tr id="cito_internal">
						<td>Kecamatan</td>
						<td>
							<select class="form-control" name="list_kec" id="list_kec" onchange="get_puskesmas(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						<td>Unit Penerima</td>
						<td>
							<select class="form-control" name="nmpuskesmas" id="nmpuskesmas">
								<option value="0">--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>

					</tr>
					<tr id="cito_eksternal">
						<td>Unit Eksternal</td>
						<td>
							<div class="input-group">
								<select name="unit_luar" class="form-control" id="unit_luar" onchange="get_unitexternal(this.value);">
									<option value="">--- PILIH ---</option>
									<?php for($i=0;$i<sizeof($ux);$i++): ?>
									<option value="<?php echo $ux[$i]['id']; ?>|<?php echo $ux[$i]['description']; ?>"><?php echo $ux[$i]['description']; ?></option>
									<?php endfor; ?>
									<option value="add">--- TAMBAH ---</option>
								</select>
								<span class="input-group-btn"><button class="btn btn-danger pull-right" id="btn-delete-unex"><span class="glyphicon glyphicon-trash"></span></button></span>
						</td>
						<td></td><td></td>
					</tr>
					<tr>
						<td width="20%">Bulan/Periode</td>
						<td>
							<div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm" >
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								<input class="form-control span2 required" size="12" type="text" value="" readonly="readonly" id="periode_cito" name="periode_cito">
							</div>

							<script>
							$('#datepicker').datepicker(
								{
								    format: "yyyy-mm",
								    viewMode: "months",
								    minViewMode: "months"
								});
							</script>
						</td>
						<td width="20%">Tanggal Pemberian</td>
						<td>
							<div class="input-group input-append date" id="datepickerd" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								<input class="form-control span2" size="12" type="text" value="" readonly="readonly" id="datenow" name="" required="required">
							</div>
							<script>
							$('#datepickerd').datepicker(
								{
								    format: "yyyy-mm-dd"
								});
							</script>
						</td>
					</tr>

					<tr>
						<td>Nomor Dokumen</td>
						<td>
							<input type="text" class="form-control" name="no_dok" id="no_dok">
						</td>
						<td>Keperluan</td>
						<td>
							<input type="text" class="form-control" name="keperluan_cito" id="keperluan_cito">
						</td>
					</tr>
					<tr>
						<td>Nama Penerima</td>
						<td>
							<input type="text" class="form-control" name="nama_penerima" id="nama_penerima">
						</td>
						<td>NIP Penerima</td>
						<td>
							<input type="text" class="form-control" name="nip" id="nip">
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_add" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="del_distribusi_obat_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button>
								<button id="cha_distribusi_obat-btn" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
							</div>
						</td>
						<td colspan="3">

							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
				<!--/form-->

				<div id="list_distribusi_obat"></div>
			</div><!-- end normal -->
		</div>
		</div>
</div>
<div class="modal fade" id="change_distribusi_obat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data Distribusi</h4>
      </div>
      <div class="modal-body">
	<form id="form_ubah_data">
		<table class="table">
			<tr>
				<td>Nomor Dokumen</td>
				<td><input type="text" class="form-control" name="cha[no_dok]" id="cha_no_dok">
					<input type="hidden" class="form-control" name="cha[id_distribusi]" id="cha_id_distribusi"></td>
				<td>Bulan/Periode</td>
				<td>
					<div class="input-append date" id="datepicker4" data-date="<?php date('Y-m-d') ?>" data-date-format="yyyy-mm" >
						<input class="span2" size="12" type="text" value="" readonly="readonly" id="cha_periode" name="cha[periode]">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

						<script>
						$('#datepicker4').datepicker(
							{
							    format: "yyyy-mm",
							    viewMode: "months",
							    minViewMode: "months"
							});
						</script>
				</td>
			</tr>
			<tr>
				<td>Tanggal Pemberian</td>
				<td>
					<div class="input-append date" id="datepicker5" data-date="<?php date('Y-m-d') ?>" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" value="" readonly="readonly" id="cha_tanggal" name="cha[tanggal_trans]">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

						<script>
						$('#datepicker5').datepicker();
						</script>

				</td>
				<td>Keperluan</td>
				<td><input type="text" class="form-control" name="cha[keperluan]" id="cha_keperluan"></td>

			</tr>
			<tr>
				<td>Nama User</td>
				<td><input type="text" class="form-control" name="cha[user]" id="cha_user"></td>
				<td>Nama Unit Eksternal</td>
				<td><input type="text" class="form-control" name="cha[unit_eks]" id="cha_unit_eks"></td>

			</tr>
			<tr>
				<td><div class="pagingContainer">
						<button type="submit" name="Simpan" id="upt_distribusi_obat" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button id="upt_batal"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
				</td>
				<td>
				</td>
			</tr>
		</table>
		</form>
		</div>
		</div>
	</div>
</div>

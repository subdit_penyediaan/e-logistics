<style>
.datepicker{
	z-index:1151;
}	
</style>
<script>
$(document).ready(function(){
	$('#set_value_opt').click(function(){
		var value_opt=prompt("Masukkan nilai buffernya (%)");
		if(value_opt){
			if(value_opt!=''){
				$.ajax({
					url:"manajemenlogistik/history_lplpo_kel/post_opt_value",
					data:"nilai="+value_opt,
					type:"POST",
					success: function(){
						alert("data telah dimasukkan");
					}
				})
			}
		}			
	})

	$('#set_time_distribute').click(function(){
		var value_per=prompt("Masukkan periode distribusi (bulan):");
		if(value_per){
			if(value_per!=''){
				$.ajax({
					url:"manajemenlogistik/history_lplpo_kel/post_periode",
					data:"nilai="+value_per,
					type:"POST",
					success: function(){
						alert("data telah dimasukkan");
					}
				})
			}		
		}		
	})

	$('#download_template_lplpo').click(function(){
		var url4="manajemenlogistik/history_lplpo_kel/getTemplate";
		window.open(url4);
	})	

	//=========== show add form
	$('#form_history_lplpo_modal').hide();
	$("#add_history_lplpo_btn").on("click",function (event){		
		$("#form_history_lplpo_modal").slideDown("slow");
		$("#partbutton_lplpo").fadeOut();
	});
	$("#batal_hist_lplpo").on("click",function (event){
		$("#form_history_lplpo_modal").slideUp("slow");
		$("#partbutton_lplpo").fadeIn();
	});
	//================ end show add form

	var url="manajemenlogistik/history_lplpo_kel/get_data_history_lplpo";
	$('#list_history_lplpo').load(url);	
	//============== submit add form

	$("#btn_history_lplpo").click(function(){
		var url2="manajemenlogistik/history_lplpo_kel/input_data";
		var url3="manajemenlogistik/history_lplpo_kel/data_inputed";
		var form_data = {
			kode_kec:$("#list_kec_hist_lplpo").val(),
			kode_pusk:$("#nmpuskesmas_hist").val(),
			per_lplpo:$("#periode_lplpo").val()			
		}
		$('input[name="data_include"]').attr('checked',function(){
			if($(this).is(':checked')){
				$.ajax({
					type:"POST",
					url:url3,
					data: form_data,
					dataType: 'json',
					beforeSend: function(){
		                  showBusySubmit();
		              },
					success:function(e){
						if(e.con==1){
							confirm(e.confirm);
						}else{
							alert(e.confirm);	
						}
						$("#form_history_lplpo_modal").slideUp("slow");
						$("#partbutton_lplpo").fadeIn();
						var url_hasil="manajemenlogistik/lplpo_kel";
						$("#list_history_lplpo").load(url_hasil);
						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					}
				});
			}else{
				$.ajax({
					type:"POST",
					url:url2,
					data: form_data,
					dataType: 'json',
					beforeSend: function(){
		                  showBusySubmit();
		              },
					success:function(e){
						alert(e.confirm);
						$("#form_history_lplpo_modal").slideUp("slow");
						$("#partbutton_lplpo").fadeIn();
						var url_hasil="manajemenlogistik/lplpo_kel";
						$("#list_history_lplpo").load(url_hasil);
						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					}					
				});
			}
		}) 
	})
	//============== end submit add form
});

function showBusySubmit(){
    $('.up-konten').block({
        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
        css: {
            border: 'none',
            backgroundColor: '#cccccc',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .6,
            color: '#000',
            width: '130px',
            height: '15px',
            padding: '5px'
        }
    });
}
</script>
<!-- bag. isi -->
<div id="partbutton_lplpo" style="padding-top:10px;">
	<div class="row">
		<div class="col-lg-8 col-md-10">
			<div class="btn-group" role="group">					
				<button id="add_history_lplpo_btn" class="btn btn-success" title="Tambah"><span class="glyphicon glyphicon-plus"></span></button>										
				<!-- <button id="cha_history_lplpo-btn" class="btn btn-info" title="Ubah"><span class="glyphicon glyphicon-pencil"></span></button> -->
				<a href="javascript:void(0)" id="download_template_lplpo" class="btn btn-info" title="Download Template LPLPO"><span class="glyphicon glyphicon-download-alt"></span></a>
			    <a href="javascript:void(0)" id="set_value_opt" style="" class="btn btn-warning" title="Set Buffer Optimum Value"><span class="glyphicon glyphicon-wrench"></span></a>
			    <a href="javascript:void(0)" id="set_time_distribute" style="" class="btn btn-primary" title="Set Periode Distribusi"><span class="glyphicon glyphicon-cog"></span></a>
			</div>							 
		</div>				
	</div>							
</div>
<div class="up-konten" id="form_history_lplpo_modal">
	<br>
	<fieldset><legend><h5>Tambah LPLPO</h5></legend>
		<form class="form-horizontal">
			<div class="form-group">
			    <label class="control-label col-sm-2" for="list_kec_hist_lplpo">Kecamatan:</label>
			    <div class="col-sm-3">
				    <select name="list_kec_hist_lplpo" id="list_kec_hist_lplpo" onchange="get_puskesmas(this.value);" class="form-control">
						<option>--- PILIH ---</option>
						<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
						<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
						<?php endfor; ?>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label class="control-label col-sm-2" for="nmpuskesmas_hist">Puskesmas:</label>
			    <div class="col-sm-3">
				    <select name="nmpuskesmas_hist" id="nmpuskesmas_hist" onchange="tambahpuskesmas(this.value);" class="form-control">
						<option>--- PILIH ---</option>
						<option value="add">--- TAMBAH ---</option>
					</select>
			    </div>
			</div>
			<div class="form-group">
			    <label class="control-label col-sm-2" for="datepicker3">Bulan/Periode</label>
			    <div class="col-sm-3">
				    <div class="input-group input-append date" id="datepicker3" data-date="2014-04-15" data-date-format="yyyy-mm" >
						<input class="form-control span2" size="12" type="text" value="" readonly="readonly" id="periode_lplpo">
						<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
					<script>
					$('#datepicker3').datepicker(
						{
						    format: "yyyy-mm",
						    viewMode: "months", 
						    minViewMode: "months"
						});
					</script>
			    </div>
			    <div class="col-sm-4">
				    <div class="input-group">
				      <span class="input-group-addon">
				        <input type="checkbox" name="data_include" id="data_include">
				      </span>
				      <input type="text" class="form-control" value="Gunakan data periode sebelumnya" size="35"readonly>
				    </div>
				</div>
			</div>
		</form>

		<div class="col-sm-12">
			<div class="pull-right">
				<button type="submit" name="Simpan" id="btn_history_lplpo" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
				<button id="batal_hist_lplpo" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
			</div>			
		</div>		
	</fieldset>
	<hr>	
</div>

<div id="list_history_lplpo" style="padding-top:10px;"></div>

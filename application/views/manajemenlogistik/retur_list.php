<script type="text/javascript">
	$(function(){
		$("#pagelistretur").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_retur').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
		$("#print_btn").click(function(){
			var urlprint="manajemenlogistik/retur/printOut";
			window.open(urlprint);		
		})
	});
</script>
<div id="pagelistretur">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<br>
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">
			<!--th><span class="glyphicon glyphicon-trash"></span></th-->
			<th>No.</th>
			<th>Tanggal Retur</th>
			<th>Nomor Faktur</th>
			<th>Kode Sediaan</th>
			<th>Nama Sediaan</th>
			<th>No. Batch</th>
			<th>Nama PBF</th>
			<th>Jumlah</th>
			<th>Keterangan</th>			
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					//<td><input type="checkbox" name="chk[]" id="cek_del_retur" value="'.$rows->id.'" class="chk">
					echo '<tr style="cursor:pointer;">
					
							
					</td>
					<td>'.$i.'</td>					
					<td>'.$rows->tanggal_retur.'</td>
					<td>'.$rows->no_faktur.'</td>
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->nama_obj.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->pbf.'</td>
					<td>'.$rows->jumlah.'</td>
					<td>'.$rows->keterangan.'</td>
					</tr>';
					$i++;
				}
				//<td><button href="'.$base_url.'index.php/manajemenlogistik/detail_faktur/get_detail/'.$rows->id.'" id="detail_faktur"><span class="glyphicon glyphicon-list"></span> Lihat</button></td>
			?>
		</tbody>
	</table>
	<button id="print_btn" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span> Eksport</button>
</div>
<script type="text/javascript">
	$(function(){
		$('.table-content').DataTable();
		$("#pagelistpenerimaan_obat").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_penerimaan_obat').load(url);
		});
		$("#trcontent").on("click",".btn-detail",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn-action{
		text-align: center;	
	}
</style>
<div id="pagelistpenerimaan_obat">
<?php //echo $links; ?>
	<table class="table table-striped table-bordered table-content">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Nomor Faktur</th>
			<th>Tanggal</th>
			<th>Keterangan</th>
			<th>No. Kontrak</th>
			<th>Bulan/Periode</th>
			<th>Nama PBF</th>
			<th>Sumber Dana</th>
			<th>Detail Faktur</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php $i=1; foreach ($result as $rows): ?>
				<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_penerimaan_obat" value="<?= $rows->id ?>" class="chk"></td>
					<td><?= $i ?></td>
					<td><?= $rows->no_faktur ?></td>
					<td><?= $rows->tanggal ?></td>
					<td><?= $rows->nama_dok ?></td>
					<td><?= $rows->no_kontrak ?></td>
					<td><?= $rows->periode ?></td>
					<td><?= $rows->pbf ?></td>
					<td><?= $rows->dana ?> <?= $rows->tahun_anggaran ?></td>
					<td class="btn-action"><button href="<?= $base_url ?>index.php/manajemenlogistik/detail_faktur/get_detail/<?= $rows->id ?>" id="detail_faktur" class="btn-detail"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>					
			<?php $i++;endforeach; ?>
		</tbody>
	</table>
</div>
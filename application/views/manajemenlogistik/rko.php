<div class="panel panel-primary" id="">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Rencana Kebutuhan Obat</b></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<div class="alert alert-info" role="alert">
			<b>RKO</b>
			<p>Rencana Kebutuhan Obat tahunan yang di buat pada awal tahun.</p>
		</div>
		<form class="form-generate" action="<?= base_url() ?>manajemenlogistik/rko/generate" method="POST">
			<div class="col-md-2">
				Tahun Anggaran
			</div>
			<div class="col-md-2">
				<select name="tahun_anggaran" id="tahun-anggaran" class="form-control">
					<?php foreach(tahun() as $key => $value): ?>
	                    <option value="<?php echo $key; ?>">
	                         <?php echo $value; ?>
	                    </option>
	                <?php endforeach; ?>
				</select>
			</div>
			<div class="col-md-2">
				<button class="btn btn-success" type="submit" id="btn-create">Create</button>
			</div>
		</form>
		<div class="col-md-6 custom-div">
			<i>*) klik pada tahun untuk download template versi csv</i>
			<table class="table table-striped table-bordered">
				<thead>
					<tr class="active">
						<th>#</th>
						<th>Tahun</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php $i=1; foreach($history as $key): ?>
                    <tr>
                    	<td><?= $i ?></td>
                        <td><?php echo $key->year; ?></td>
                    	<td>
                    		<button type="button" action="<?= base_url(); ?>manajemenlogistik/rko/show/<?= $key->year?>" class="btn btn-success btn-manual">review</button>
                    		<button type="button" action="<?= base_url(); ?>manajemenlogistik/rko/remove/<?= $key->year?>" class="btn btn-danger btn-delete">delete</button>
                    	</td>
	                <?php $i++; endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<script type="text/javascript">
function showBusySubmitCustom(){
    $('.custom-div').block({
        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
        css: {
            border: 'none',
            backgroundColor: '#cccccc',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .6,
            color: '#000',
            width: '130px',
            height: '15px',
            padding: '5px'
        }
    });
}

$(document).ready(function(){
	$(".form-generate").on("submit", function(e) {
		e.preventDefault();
		$.ajax({
			type: $(this).attr("method"),
			url: $(this).attr("action"),
			data: $(this).serialize(),
			dataType: "json",
			beforeSend: function(){
                showBusySubmit();
            },
			success: function(response) {
				alert(response.message);
				if (response.status == "success") {
					$('#konten').load('manajemenlogistik/rko');
				}
			}
		})
	})

	$('.btn-delete').click(function(e){
		e.preventDefault();
		var url = $(this).attr("action");
		var cnf = confirm("Apakah Anda yakin?");
		if (cnf) {
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				beforeSend: function(){
	                showBusySubmitCustom();
	            },
				success: function(response) {
					alert(response.message);
					if (response.status == "success") {
						$('#konten').load('manajemenlogistik/rko');
					}
				}
			})
		}
	})

	$(".btn-manual").on("click", function(e) {
		e.preventDefault();
		var url = $(this).attr("action");
		$.ajax({
			url: url,
			type: "GET",
			beforeSend: function(){
                showBusySubmit();
            },
			success: function(html) {
				$('#up-konten').html(html);
			}
		})
	})

	$(".btn-detail").on("click", function() {

	})
})
</script>
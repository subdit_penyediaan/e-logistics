<script type="text/javascript">
	$(function(){
		$('.table-content').DataTable();
		$("#pagelisthistory_lplpo").on("click","a",function (event) {
			event.preventDefault();
			var url = $(this).attr("href");
			$('#list_history_lplpo').load(url);
		});

		$("#trcontent").on("click",".btn-detail",function (event) {
			event.preventDefault();
			var url = $(this).attr("href");
			$('#konten').load(url);
		});

		//=========== del button
		$(".table-content").on("click", ".lplpo-del", function (e) {
			var id = $(this).parent().parent().find(".lplpo-id").val();
			var pkm_code = $(this).parent().parent().find(".pkm-code").val();
			var n = confirm("Apakah Anda yakin akan menghapus data ini?");
	        if(n){
	        	$.ajax({
		        	url: "manajemenlogistik/history_lplpo_kel/delete_list/"+id+"/"+pkm_code,
		        	type: "POST",
		        	dataType: "json",
		        	success: function (data) {
		        		if (data.status == "success") {
			        		var url_hasil="manajemenlogistik/history_lplpo_kel/get_data_history_lplpo";
							$("#list_history_lplpo").load(url_hasil);	
		        		} 
		        		alert(data.message);		        		
		        	}
			    })
		    }
		})
		//=========== end del

		$('.table-content').on("click", ".lplpo-upd", function(e){
			var id = $(this).parent().parent().find(".lplpo-id").val();
			var pkm_code = $(this).parent().parent().find(".pkm-code").val();
			$.ajax({
	    		type:"POST",
	    		url:'manajemenlogistik/history_lplpo_kel/datatochange/'+id+'/'+pkm_code,
	    		dataType:'json',
	    		success:function(data) {
	    			if (data.status == "success") {
	    				$('#cha_id_lplpo').val(data.content[0].id);
		    			$('#cha_periode_lplpo').val(data.content[0].periode);	    			
		    			$("#cha_kec_lplpo" ).val(data.content[0].kode_kec);
		    			urlpusk='manajemenlogistik/history_lplpo/get_data_puskesmas/'+data.content[0].kode_kec+'/'+data.content[0].kode_pusk;
		    			$.get(urlpusk, function(html) {$('#cha_namapusk').html(html);});
		    			$('#change_lplpo').modal('show');
	    			} else {
	    				alert(data.message);
	    			}
	    			
	    		}
	    	});
		})

		$('#datepicker31').datepicker({
		    format: "yyyy-mm",
		    viewMode: "months", 
		    minViewMode: "months"
		});

		$("#btn-cha").click(function(){
			var url2="manajemenlogistik/history_lplpo_kel/update_data";
			var form_data = {
				id_lplpo:$("#cha_id_lplpo").val(),
				periode:$("#cha_periode_lplpo").val(),
				kode_kec:$("#cha_kec_lplpo").val(),
				kode_pusk:$("#cha_namapusk").val()
			}
			$.ajax({
				type:"POST",
				url:url2,
				data: form_data,
				dataType: "json",
				success:function(e){			
					alert(e.confirm);
					$('#change_lplpo').modal('toggle');
					$('.modal-backdrop').remove();
					var url_hasil="manajemenlogistik/history_lplpo_kel/get_data_history_lplpo"
					$("#list_history_lplpo").load(url_hasil);
					$("#cha_id_lplpo").val("");
					$("#cha_periode_lplpo").val("");
					$("#cha_kec_lplpo").val("");
					$("#cha_namapusk").val("");
				}
			});
		})
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;
	}
</style>
<div id="pagelisthistory_lplpo">
	<table class="table table-striped table-bordered table-content">
		<thead>
		<tr class="active">
			<th width="3%">#</th>
			<th>Kecamatan</th>
			<th>Unit Penerima</th>
			<th width="15%">Bulan/Periode</th>
			<th width="15%">Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php $i=1;foreach ($result as $rows): ?>
				<tr style="">
					<td class="btn_action">
						<?= $i; ?>
						<input type="hidden" name="" id="" value="<?= $rows->id ?>" class="lplpo-id">
						<input type="hidden" name="" id="" value="<?= $rows->kode_puskesmas ?>" class="pkm-code">
					</td>
					<td><?= $rows->nama_kec ?></td>
					<td><?= $rows->nama_pusk ?></td>
					<td><?= $rows->periode ?></td>
					<td class="btn_action">
						<button href="<?= $base_url ?>index.php/manajemenlogistik/isian_lplpo_kel/get_detail/<?= $rows->id ?>/<?= $rows->kode_puskesmas ?>" id="" class="btn btn-xs btn-detail" title="detail">
							<span class="glyphicon glyphicon-list"></span>
						</button>
						<button class="btn btn-danger btn-xs lplpo-del" title="delete">
							<span class="glyphicon glyphicon-remove"></span>
						</button>
						<button class="btn btn-info btn-xs lplpo-upd" title="change">
							<span class="glyphicon glyphicon-pencil"></span>
						</button>
					</td>
				</tr>
			<?php $i++;endforeach;?>
		</tbody>
	</table>
</div>
<div class="modal fade" id="change_lplpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        		<h4 class="modal-title" id="myModalLabel">Ubah LPLPO</h4>
      		</div>
      		<div class="modal-body">
				<table class="table table-condensed" width="50%">
					<tr>
						<td width="30%">Kecamatan
							<input type="hidden" id="cha_id_lplpo" name="cha_id_lplpo"></td>
						<td width="70%">
							<select class="required" name="kode_kec" id="cha_kec_lplpo" onchange="get_puskesmas(this.value);">
								<option value="">--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select class="required" name="kode_pusk" id="cha_namapusk" onchange="tambahpuskesmas(this.value);">
							</select>
						</td>
					</tr>
					<tr>
						<td>Bulan/Periode</td>
							<td>
								<div class="input-append date" id="datepicker31" data-date="2014-04-15" data-date-format="yyyy-mm" >
									<input class="span2 required" size="12" type="text" value="" readonly="readonly" id="cha_periode_lplpo" name="per_lplpo">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
					</tr>
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn-cha" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal_hist_lplpo"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
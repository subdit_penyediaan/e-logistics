 <script>
function reloadSot(){
	var url="manajemenlogistik/stok_opnam/get_data_so_tahunan";
	$('#list_stok_tahunan').load(url);
}

function reloadTable() {
	var url="manajemenlogistik/stok_opnam/get_data_stok_opnam";
	$("#list-stok-opnam").load(url);
}

$(document).ready(function(){
	$('.trans-date').datepicker({
	    format: "yyyy-mm-dd"
	});

	$('.periode').datepicker({
	    format: "yyyy-mm",
	    viewMode: "months", 
	    minViewMode: "months"
	});

	//=========== del button		
	$("#del_stok_opnam_btn").on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
        	var prompt = confirm("Apakah Anda yakin akan menghapus data ini?");
        	if (prompt) {
        		$.ajax({
		        	url: "manajemenlogistik/stok_opnam/delete_list",
		        	data: "kode="+id_array,
		        	type: "POST",
		        	success: function(){
		        		alert("data berhasil dihapus");
		        		reloadTable();
		        	}
		        })
        	}	        
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_stok_opnam_modal').hide();
	$("#add_stok_opnam_btn").on("click",function (event){
		$("#form_stok_opnam_modal").show();
		$("#partbutton").hide();
	});
	$("#batal").on("click",function (event){
		$("#form_stok_opnam_modal").hide();
		$("#partbutton").show();
	});
	//================ end show add form
	reloadTable();
	//============== submit add form

	$("#btn_stok_opnam").click(function(){
		var url2="manajemenlogistik/stok_opnam/input_data";
		var form_data = {
			no_stok_opnam:$("#no_stok_opnam").val(),
			nama_user:$("#nama_user").val(),
			per_stok_opnam:$("#per_stok_opnam").val(),
			cttn:$("#catatan").val(),
			tgl_trans:$("#tgl_trans").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_stok_opnam_modal").hide();
				$("#partbutton").show();
				reloadTable();
				$("#no_stok_opnam").val("");
				$("#nama_user").val("");
				$("#per_stok_opnam").val("");
				$("#catatan").val("");
			}
		});
	})
 	// reloadSot();
 	$("#form-so-tahunan").submit(function(){
 		event.preventDefault();
		var url=$("#form-so-tahunan").attr('action');
		var data =$("#form-so-tahunan").serialize();
		$.post(url,data,function(data){
			var obj = jQuery.parseJSON(data);			
			if(obj.confirm==1){
				alert("Data berhasil dimasukkan");
			}else{
				alert("Data sudah tersedia");
			}
			reloadSot();
		})
		
	})

	//============== end submit add form
});
</script>
<div class="panel panel-primary" id="halaman_stok_opnam">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Stok Opnam</b></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<!--ul class="nav nav-tabs">
			<li class="active"><a href="#so_bulanan" data-toggle="tab">Bulanan</a></li> 		  
		  	<li class=""><a href="#so_tahunan" data-toggle="tab">Tahunan</a></li>		  	
		</ul--> 
		<!-- <div class="tab-content"> -->
			<!-- <div class="tab-pane active" id="so_bulanan"> -->
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_stok_opnam_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_stok_opnam_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
			</div>				
		</div>
		<div class="" id="form_stok_opnam_modal">
		  	<h4>Form Input</h4>
			<table class="table">
				<tr>
					<td>No. Stok Opnam</td>
					<td><input type="text" class="form-control" name="no_stok_opnam" id="no_stok_opnam"></td>
					<td>Nama Petugas</td>
					<td><input type="text" class="form-control" name="nama_user" id="nama_user"></td>
				</tr>
				<tr>
					<td>Tanggal Pelaksanaan</td>
					<td>
						<div class="input-group input-append date" id="datepicker" data-date="<?= date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
							<input class="form-control span2 trans-date" size="12" type="text" value="<?= date('Y-m-d');?>" id="tgl_trans">
							<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
					</td>
					<td>Bulan/Periode</td>
					<td>
						<div class="input-group input-append date" id="datepicker" data-date="<?= date('Y-m-d');?>" data-date-format="yyyy-mm" >
							<input class="form-control span2 periode" size="12" type="text" value="<?= date('Y-m');?>" id="per_stok_opnam">
							<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>																
					</td>
				</tr>
				<tr>
					<td>Catatan</td>
					<td><input type="text" class="form-control" name="catatan" id="catatan"></td>
				</tr>
				<tr>
					<td><div class="pagingContainer">
							<button type="submit" name="Simpan" id="btn_stok_opnam" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
							<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
						</div>
					</td>
					<td colspan="3">
					</td>
				</tr>
			</table>
		</div>				
		<hr>
		<div id="list-stok-opnam"></div>
			<!-- </div> -->
			<!-- <div class="tab-pane" id="so_tahunan">
				<br>
				<form action="<?= $base_url; ?>index.php/manajemenlogistik/stok_opnam/input_data_tahunan" method="POST" name="form-so-tahunan" id="form-so-tahunan">
					<div class="input-group">
						<label class="">Tahun</label>
						<select class="form-control" name="tahun" id="tahun">
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
							<option value="2018">2018</option>
						</select>	
					</div>
					<button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-star"></span> Create</button>
				</form>
				<div id="list_stok_tahunan"></div>
			</div> -->
		<!-- </div> -->
		
	</div>
</div>
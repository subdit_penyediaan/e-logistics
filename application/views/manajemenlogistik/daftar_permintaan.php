<script type="text/javascript">
	$(function(){
		$("#pagelistdaftar_permintaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_daftar_permintaan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistdaftar_permintaan">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped">
		<thead>
		<tr>
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>Tanggal Masuk</th>
			<th>Kecamatan</th>
			<th>Puskesmas</th>
			<th>Periode</th>
			<th>Status</th>
			<th>Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_daftar_permintaan" value="'.$rows->id.'" class="chk">
					</td>
					<td>'.$rows->cur_date.'</td>
					<td>'.$rows->kecamatan.'</td>
					<td>'.$rows->puskesmas.'</td>
					<td>'.$rows->periode.'</td>
					<td>'.$status.'</td>
					<td><button href="'.$base_url.'index.php/manajemenlogistik/distribusi_obat/get_detail/'.$rows->id.'" id="detail_faktur"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>';
					//$i++;
				}
			?>
			<?php
				//$i=1;
				foreach ($finished as $rows) {
					echo '<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_daftar_permintaan" value="'.$rows->id.'" class="chk">
					</td>
					<td>'.$rows->cur_date.'</td>
					<td>'.$rows->kecamatan.'</td>
					<td>'.$rows->puskesmas.'</td>
					<td>'.$rows->periode.'</td>
					<td>Sudah Diproses</td>
					<td><button href="'.$base_url.'index.php/manajemenlogistik/distribusi_obat/get_detail_done/'.$rows->id.'" id="detail_faktur"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
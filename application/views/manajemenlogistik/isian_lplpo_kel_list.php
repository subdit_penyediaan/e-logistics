<style>
table {
  /* width: 500px; */
}

table {
  text-align: left;
  position: relative;
  border-collapse: collapse;
}

th, td {
  padding: 0.25rem;
}
tr.red th {
  background: red;
  color: white;
}
tr.green th {
  background: green;
  color: white;
}
tr.purple th {
  background: purple;
  color: white;
}
th {
  background: white;
  position: sticky;
  top: 0; /* Don't forget this, required for the stickiness */
  box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
}

.scroll {
  max-height: 600px;
  overflow: auto;
}
</style>
<script type="text/javascript">
	$(function(){

		$( "#nama_obat-x").autocomplete({
				source:'manajemenlogistik/distribusi_obat/get_list_obat',
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat-x" ).val( ui.item.value +" "+ ui.item.pwr);
			        return false;
			      },
				select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#kode_obat_'+index).val(ui.item.kode_obat);
					$('#no_batch_'+index).val(ui.item.batch);
					$('#id_stok_'+index).val(ui.item.id_stok);

					return false;
				}
			})

			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +" "+item.pwr + "<br><small>Stok: <i>" + item.stok + "<i></small></a>")
		        .appendTo( ul );
		    };

		$('#form_detail_lplpo').submit(function(e){
			event.preventDefault();
			$.ajax({
					type: $(this).attr('method'),
          url: $(this).attr('action'),
          data: $(this).serialize(),
          beforeSend: function(){
            showBusySubmit();
        	},
          success: function (data) {
	          	if(data=='0'){
	          			alert("Periode distribusi belum di setting");
	          	}else{
	          			alert('Data berhasil diupdate');
	                var url="manajemenlogistik/isian_lplpo_kel/get_data_isian_lplpo/"+data;
									$('#list_isian_lplpo').load(url);
	          	}
	          	$('#up-konten').unblock();
					}
			})

		});

		$(".loop_respon").each(function(index){
			$('#pemberian_'+index).focus(function(){
				//alert(index);
				$("#add_give_modal").modal('show');
			})
		})
	});

	seq=0;
	function hitung_sisa_stok2cX(val){
		//fungsi di pemakaian
		//seq++;
		nilai1 = parseInt(val);
		nilai2 = parseInt($('#sedia_'+seq).val());
		//nilai4 = parseInt($('#rusak_ed_'+seq).val());

		nilai3 = nilai2 - nilai1;// - nilai4;
		$('#sisa_stok_'+seq).val(nilai3);
		//alert("cek");
		seq++;
	}

	k=0;
	function hitung_sisa_stok3X(val){
		//fungsi di rusak_ed
		//k++;
		nilai1 = parseInt(val);
		nilai2 = parseInt($('#sedia_'+k).val());
		nilai4 = parseInt($('#pemakaian_'+k).val());

		nilai3 = nilai2 - nilai1 - nilai4;
		$('#sisa_stok_'+k).val(nilai3);
		//alert("cek");
		k++;
	}

	j=0;
	function hitung_stok_optimumX(val){
		//fungsi dipemakain
		//j++;
		nilai1=val*3;
		$('#stok_opt_'+j).val(nilai1);
		j++;
	}
	function showBusySubmit(){
        $('#up-konten').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }
</script>
<div id="">
<!--/div-->
	<!--div id="pagin"-->
	<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/isian_lplpo_kel/update_data" method="post" id="form_detail_lplpo">
	<div class="scroll">
			<table class="table table-bordered table-striped">
        <tr class="">
          <th><span class="glyphicon glyphicon-trash"></span></th>
          <th>No.</th>
          <th>Kode Obat</th>
          <th width="40%">Nama Obat</th>
          <th>Satuan</th>
          <th>Stok</th>
          <th>Terima</th>
          <th>Per<br>sediaan</th>
          <th>Pakai</th>
          <th>Rusak/<br>ED</th>
          <th>Sisa<br>Stok</th>
          <th>Stok Opt</th>
          <th>Per<br>mintaan</th>
          <th>Pem<br>berian</th>
          <th>Ket</th>
        </tr>
				<tbody id="">
					<?php $i=0; foreach ($result as $rows): ?>
						<?php $j=$i+1; ?>
							<tr style="">
								<td><input type="checkbox" name="chk[]" id="cek_del_lplpo_list" value="<?= $rows->id ?>" class="chk"></td>
								<td><?= $j ?>
									<input type="hidden" id="hidden_id_lplpo_<?= $i ?>" name="hidden_id_lplpo[]" value="<?= $rows->id_lplpo ?>">
									<input type="hidden" id="hidden_id_detaillplpo_<?= $i ?>" name="hidden_id_detaillplpo[]" value="<?= $rows->id ?>">
									</td>
								<td><?= $rows->kode_generik ?></td>
								<td><?= $rows->nama_obat ?></td>
								<td><?= $rows->sediaan ?></td>
								<td><input type="text" id="stok_awal_<?= $i ?>" style="border:none;" size="4" name="stok_awal[]" value="<?= $rows->stok_awal ?>"></td>
								<td><input type="text" id="terima_<?= $i ?>" style="border:none;" size="4" name="penerimaan[]" value="<?= $rows->terima ?>"></td>
								<td><input type="text" id="sedia_<?= $i ?>" style="border:none;" size="4" name="persediaan[]" value="<?= $rows->sedia ?>"></td>
								<td><input type="text" id="pemakaian_<?= $i ?>" size="4" name="pemakaian[]" value="<?= $rows->pakai ?>" onchange="hitung_stok_optimum(this.value);"></td>
								<td><input type="text" id="rusak_ed_<?= $i ?>" size="4" name="rusak_ed[]" value="<?= $rows->rusak_ed ?>" onchange="hitung_sisa_stok3(this.value);"></td>
								<td><input type="text" id="sisa_stok_<?= $i ?>" style="border:none;" name="sisa_stok[]" size="4" value="<?= $rows->stok_akhir ?>"></td>
								<td><input type="text" id="stok_opt_<?= $i ?>" style="border:none;" name="stok_opt[]" size="4" value="<?= $rows->stok_opt ?>"></td>
								<td><input type="text" id="permintaan_<?= $i ?>" name="permintaan[]" size="4" value="<?= $rows->permintaan ?>"></td>
								<td><input type="text" id="pemberian_<?= $i ?>" style="border:none;" class="loop_responX" name="pemberian[]" size="4" value="<?= $rows->pemberian ?>" readonly></td>
								<td><input type="text" id="ket_<?= $i ?>" name="ket[]" size="4" value="<?= $rows->ket ?>"></td>
							</tr>
					<?php $i++; endforeach; ?>
				</tbody>
			</table>
	</div>
		<button class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-magnet"></span> Update LPLPO</button>
	</form>
</div>

<div class="modal fade" id="add_give_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Pemberian Obat</h4>
      </div>
      <div class="modal-body">
		<table class="table">
			<tr>
				<th>Nama Obat</th>
				<th>No. Batch</th>
				<th>Jumlah Pemberian</th>
			</tr>
			<tr>
				<td><input type="text" name="nama_obat-x" id="nama_obat-x" size="50" class=""/></td>
				<td><input type="text" name="no_batch" id="no_batch" size="10" class=""/></td>
				<td><input type="text" name="jml" id="jml" size="5" class=""/></td>
			</tr>
			<tr>
				<td colspan="3">
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_gol_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
					</div>
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$("#tabel-konten").dataTable();
		$("#pagelistdistribusi_obat").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_distribusi_obat').load(url);
		});
		$("#trcontentxx").on("click",".btn_action2",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;	
	}
	.dataTables_filter{
		float:right;
	}
	.pagination{
		float:right;
	}
</style>
<div id="pagelistdistribusi_obat">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered" id="tabel-konten">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No. Dokumen</th>
			<th>Tanggal Pemberian</th>
			<!-- <th>Nama Petugas</th> -->
			<th>Periode Permintaan</th>
			<th>Unit Penerima</th>
			<th>Status</th>
			<th>Detail</th>
		</tr>
		</thead>
		<tbody id="trcontentxx">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					echo '<tr style="">
					<td class="btn_action"><input type="checkbox" name="chk[]" id="cek_del_distribusi_obat" value="'.$rows->id.'" class="chk">
					</td>
					<td>'.$rows->no_dok.'</td>
					<td>'.$rows->tanggal.'</td>
					
					<td>'.$rows->periode.'</td>
					<td>'.$rows->penerima.'</td>
					<td>'.$rows->status_aktif.'</td>
					<td class="btn_action"><button href="'.$base_url.'index.php/manajemenlogistik/detail_distribusi/get_detail/'.$rows->id.'" class="btn_action2"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>';
					//$i++;
					//<td>'.$rows->nama_user.'</td>
				}
			?>
		</tbody>
	</table>
</div>
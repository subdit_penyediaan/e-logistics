<style>
	.ui-autocomplete-loading { 
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}
</style>
<script>
function reloadTable(){
		var url="manajemenlogistik/detail_distribusi/get_data_detail_distribusi/"+$('#id_distribusi').val();	
		$('#list_detail_distribusi_cito').load(url);		
	}

function resetInput(){
	$('.inputText').val('');
	$('.labelText').text('');
}
	
$(document).ready(function(){
	
	$('#partbutton').show();
	//===========
	//============ end add dinamis
	$('#back_to_').click(function(){
			var url_penerimaan='manajemenlogistik/distribusi_obat_kel';
			$('#konten').load(url_penerimaan);
		})

	$('#nama_obat_beri').autocomplete({
		source:'manajemenlogistik/detail_distribusi/get_list_obat2', 
		minLength:2,
		focus: function( event, ui ) {
	        $( "#nama_obat_beri" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{
			// when a zipcode is selected, populate related fields in this form

			$('#kode_obat_hidden_beri_1').val(ui.item.kode_obat);
			$('#no_batch_1').val(ui.item.nobatch);
			$('#label_batch_1').text(ui.item.nobatch);
			$('#stok_hidden_1').val(ui.item.stok);
			$('#id_stok_hidden_1').val(ui.item.id_stok);
			return false;
		}
	}) 		
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
      return $( "<li>" )
        .append("<a>"+ item.value +"<br><small>Stok/Expired/Batch: <i>"+ item.stok +"/"+item.ed+"/"+item.nobatch+"<br>Sumber dana: "+item.sumber_dana+" "+item.tahun_anggaran+"</small></a>")
        .appendTo( ul );
    };
			    //<br>Produsen: "+item.produsen+"
	//end autocomplete	
	//autocomplete combogrid
	$('#nama_obat_minta').combogrid({
		debug:true,
		  colModel: [
		  	//{'columnName':'id','width':'14','label':'id'}, 
		  	//{'columnName':'name','width':'60','label':'Nama INN/FDA'},
		  	{'columnName':'brand','width':'40','label':'Brand'}
		  	],
		  url: 'manajemenlogistik/detail_distribusi/get_list_obat_grid',
		select:function(evt, ui)
			{//,
		// when a zipcode is selected, populate related fields in this form
				$( "#nama_obat_minta" ).val( ui.item.brand);
				$('#kode_obat_hidden_minta').val(ui.item.kode_obat);
				return false;
			}
		})

	//=========== del button

	$("#del_detail_distribusi_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "manajemenlogistik/detail_distribusi/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="manajemenlogistik/detail_distribusi/get_data_detail_distribusi"
					$("#list_detail_distribusi").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_detail_distribusi_modal').hide();
	$("#add_detail_distribusi_btn").on("click",function (event){	
		$("#form_detail_distribusi_modal").slideDown("slow");
		$("#partbutton").fadeOut();
	});

	$("#batal").on("click",function (event){
			//$("#add_detail_distribusi_modal").modal('show');
			event.preventDefault();
			$("#form_detail_distribusi_modal").slideUp("slow");
			$("#partbutton").fadeIn();
			resetInput();
		});
	//================ end show add form

	var url="manajemenlogistik/detail_distribusi/get_data_detail_distribusi/"+$('#id_distribusi').val();
	$('#list_detail_distribusi_cito').load(url);	
			
	$("#frmInput").validate({
		submitHandler:function(){
			$.ajax({
				type: $("#frmInput").attr('method'),
	            url: $("#frmInput").attr('action'),
	            data: $("#frmInput").serialize(),
	            dataType:'json',
	            success: function (data) {
	                if(data.cek>0){
	                	alert(data.confirm);
	                }else{
	                	alert(data.confirm);
	                	$('.inputText').val("");
	                	$('#label_batch_1').text("");
	                	var url_hasil="manajemenlogistik/detail_distribusi/get_data_detail_distribusi/"+$('#id_distribusi').val();
						$("#list_detail_distribusi_cito").load(url_hasil);
	                }
	                
	            }
			})
		}
	})
	//end form input
});

</script>
<div class="panel panel-primary" id="halaman_penerimaan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Detail Distribusi Sediaan</b> <button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->	
		<div class="alert alert-info" role="alert">
			<?php
				// $date = strtotime($lplpo['periode']);
				// $date_report = date('F Y',strtotime("-1 month",$date));			
				// $date_request = date('F Y',$date);
			?>
			<b>Unit Penerima: </b><?= $cito['unit_eks'] ?><br>
			<b>Bulan/Periode: </b><?= $cito['periode'] ?><br>
			<b>Tanggal Pelayanan: </b><?= $cito['tanggal_trans'] ?><br>
			<?php if($cito['keperluan']!=0): ?>
				<b>Keperluan: </b> <?php echo $cito['keperluan']; ?>
			<?php endif; ?>
		</div>	
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_detail_distribusi_btn" class="btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<!--button id="del_detail_distribusi_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button--> 
				<!--button id="cha_detail_distribusi-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</div><!-- /col6 -->
		</div>
			<div class="" id="form_detail_distribusi_modal">
			  <!--h4>Entri Permintaan</h4-->
				<form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo $base_url; ?>index.php/manajemenlogistik/detail_distribusi/input_data">
					<input type="hidden" value="<?php echo $cito['id_distribusi']; ?>" id="id_distribusi" name="id_distribusi">
					<input type="hidden" value="<?= $cito['tanggal_trans'] ?>" id="" name="tanggal_pelayanan">					
					<table class="" id="konten_pemberian" width="">
					<tbody>
						<!--tr class="active">
							<td>Obat Permintaan</td>
							<td colspan="3"><input type="text" class="form-control" name="nama_obat_minta" id="nama_obat_minta" class="cito">
								<input type="hidden" id="kode_obat_hidden_minta" name="kode_obat_hidden_minta" ></td>
							<td>Jumlah Permintaan</td>
							<td><input type="text" class="form-control" name="jml_minta" id="jml_minta"></td>
						</tr-->
						<tr>
							<td width="25%"><label>Obat Pemberian</label></td>
							<td colspan="">
								
								<input type="text" class="form-control cito required inputText" name="nama_obat_beri" id="nama_obat_beri">
								
								<input type="hidden" id="kode_obat_hidden_beri_1" name="kode_obat_res" class="inputText">
								<input type="hidden" id="stok_hidden_1" name="stok_hidden" class="inputText">
								<input type="hidden" id="id_stok_hidden_1" name="id_stok_hidden" class="inputText">
								<label>No. Batch: <label class="labelText" id="label_batch_1"></label></label>
								<input type="hidden" class="inputText" name="no_batch" id="no_batch_1">
							</td>
						</tr>
						<tr>
							<td><label>Jumlah Pemberian</label></td>
							<td>
								<div class="col-xs-3">
									<input type="text" class="form-control required inputText" name="jml_beri" id="jml_beri_1">
								</div>								
							</td>
						</tr>
					</tbody>
					</table>
					<br>							
					<button type="submit" name="" id="" class="btn btn-success"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
					<button type="reset" id="batal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Batal</button>				
				</form>
			</div>
			<br>

			<div id="list_detail_distribusi_cito"></div>
	</div>
</div>
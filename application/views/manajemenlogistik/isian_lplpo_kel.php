<style>
	.progress {
	    margin-top: 30px;
	    width: 500px;
	}
	/*.table-responsive 
	{   
	    width: 100%;
	    margin-bottom: 15px;
	    overflow-x: auto;   
	    overflow-y: hidden;     
	    -webkit-overflow-scrolling: touch;
	    -ms-overflow-style: -ms-autohiding-scrollbar;	    
	}*/
</style>
<script>
function hitung_persediaan(val){
	nilai1 = parseInt(val);
	nilai2 = parseInt($('#stok_awal').val());

	nilai3 = nilai1 + nilai2;
	$('#persediaan').val(nilai3);
}

function hitung_sisa_stok(val){
	nilai1 = parseInt(val);
	nilai2 = parseInt($('#persediaan').val());
	nilai4 = parseInt($('#pemakaian').val());

	nilai3 = nilai2 - nilai1 - nilai4;
	$('#sisa_stok').val(nilai3);	
}

function hitung_stok_optimum(val){
	var url='manajemenlogistik/isian_lplpo_kel/get_rata2pemakaian';
	var kd_pusk = "<?= $lplpo['kode_pusk']?>";
	var buffer = <?= $value['buffer'] ?>;
	var pd = <?= $value['periode_distribusi'] ?>;
	var kode_generik = $('#obat_hidden').val();
	var data ={kd_pusk:kd_pusk,buffer:buffer,pd:pd,kode_generik:kode_generik}
	$.getJSON(url,data,function(datax){
		console.log(datax.rata2pakai);
		var nilai1 = parseInt(val) + (buffer*datax.rata2pakai);
		$('#stok_op').val(Math.round(nilai1));
	})
}

function resetInput() {
	$("#nama_obat").val("");
	$("#obat_hidden").val("");
	$("#penerimaan").val("");
	$("#persediaan").val("");
	$("#pemakaian").val("");
	$("#stok_op").val("");
	$("#permintaan").val("");
	$("#ket").val("");
	$("#dana").val("");
	$('#sisa_stok').val("");
	$('#stok_awal').val("");
	$('#rusak_ed').val("");
}

function reloadTable() {
	var url="<?= base_url(); ?>manajemenlogistik/isian_lplpo_kel/get_data_isian_lplpo/"+$('#id_lplpo').val();
	$('#list_isian_lplpo').load(url);
}

$(document).ready(function(){
	var progress = setInterval(function () {
	    var $bar = $('.progress-bar');

	    if ($bar.width() >= 480) {
	        clearInterval(progress);
	        $('.progress').removeClass('active');
	    } else {
	        $bar.width($bar.width() + 5);
	    }
	    $bar.text($bar.width() / 5 + "%");
	}, 1345);

	$('#back_to_').click(function(){
		var url_penerimaan='<?= base_url(); ?>manajemenlogistik/lplpo_kel';
		$('#konten').load(url_penerimaan);
	})

	$('#nama_obat').autocomplete({
		source:'<?= base_url(); ?>manajemenlogistik/isian_lplpo_kel/get_list_obat', 
		minLength:2,
		focus: function( event, ui ) {
	        $( "#nama_obat" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{
			$('#obat_hidden').val(ui.item.kode_obat);
			$('#sediaan_hidden').val(ui.item.desc);
			return false;
		}
	})

	//=========== del button

	$("#del_isian_lplpo_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "<?= base_url(); ?>manajemenlogistik/isian_lplpo_kel/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	dataType: "json",
	        	success: function(res){
	        		alert(res.message);
	        		if (res.status == 'success') {
	        			var url_hasil="<?= base_url(); ?>manajemenlogistik/isian_lplpo_kel/get_data_isian_lplpo/"+$('#id_lplpo').val();
						$("#list_isian_lplpo").load(url_hasil);	
	        		}
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_isian_lplpo_modal').hide();
	$("#add_isian_lplpo_btn").on("click",function (event){		
		$("#form_isian_lplpo_modal").slideDown("slow");
		$("#partbutton").fadeOut();
	});

	$("#batal").on("click",function (event){
		$("#form_isian_lplpo_modal").slideUp("slow");
		$("#partbutton").fadeIn();
		resetInput();
	});
	//================ end show add form

	reloadTable();
	
	
	$("#form_isian_lplpo_modal").validate({
		submitHandler: function(form){
			var url = "<?= base_url(); ?>manajemenlogistik/isian_lplpo_kel/input_data";
			$.ajax({
				type: "POST",
				url: url,
				data: $("#form_isian_lplpo_modal").serialize(),
				dataType: "json",
				success: function(response){
					alert(response.message);
					if (response.status == "success") {
						$("#form_isian_lplpo_modal").slideUp("slow");
						$("#partbutton").fadeIn();
						reloadTable();
						resetInput();
					}					
				}
			})
		}
	})

	$("#lplpo-search-btn").on("click", function(e) {
		e.preventDefault();
		var url = "<?= base_url() ?>manajemenlogistik/isian_lplpo_kel/search_detail/"+$('#id_lplpo').val();
		$.ajax({
			url: url,
			type: "POST",
			data: "keyword="+$("#lplpo-search-txt").val(),
			success: function(data) {
				$('#list_isian_lplpo').html(data);
			}
		})
	})
});
</script>
<div class="panel panel-primary" id="halaman_penerimaan_obat">
	<div class="panel-heading">LPLPO <button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
	<!-- bag. isi -->
		<div class="alert alert-info" role="alert">
			<?php
				$date = strtotime($lplpo['periode']);
				$date_report = date('F Y',strtotime("-1 month",$date));
				$date_request = date('F Y',$date);
			?>
			<b>Kecamatan/Puskesmas: </b><?= $lplpo['nama_kec'] ?>/<?= $lplpo['nama_pusk'] ?><br>
			<b>Periode Pelaporan/Permintaan: </b><?= $date_report ?>/<?= $date_request ?>			
		</div>	
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_isian_lplpo_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_isian_lplpo_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" id="lplpo-search-txt" name="lplpo-search-txt" value="">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button" id="lplpo-search-btn"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</div><!-- /col6 -->
		</div>
			<form class="" id="form_isian_lplpo_modal">
			  	<h4>Entri Data Obat</h4>		
				<table class="table table-bordered">
					<tr>
						<td>Nama Sediaan</td>
						<td colspan="3"><input type="text" class="form-control required" name="nama_obj" id="nama_obat">
							<input type="hidden" id="obat_hidden" name="kode_obat" >
							<input type="hidden" id="sediaan_hidden" name="desc" >
							<input type="hidden" value="<?php echo $lplpo['id']; ?>" id="id_lplpo" name="id_lplpo">
							</td>
						<td>Stok Awal</td>
						<td><input type="text" class="form-control" name="stok_awal" id="stok_awal"></td>
					</tr>
					<tr><td>Penerimaan</td>
						<td><input type="text" class="form-control" name="terima" id="penerimaan" onchange="hitung_persediaan(this.value);"></td>
						<td>Persediaan</td>
						<td><input type="text" class="form-control" name="sedia" id="persediaan"></td>
						<td>Pemakaian</td>
						<td><input type="text" class="form-control" name="pakai" id="pemakaian" onchange="hitung_stok_optimum(this.value);"></td>
					</tr>
					<tr><td>Rusak/ED</td>
						<td><input type="text" class="form-control" name="rusak_ed" id="rusak_ed"  onchange="hitung_sisa_stok(this.value);"></td>						
						<td>Sisa Stok</td>
						<td><input type="text" class="form-control" name="sisa_stok" id="sisa_stok"></td>
						<td>Stok Optimum</td>
						<td><input type="text" class="form-control" name="stok_op" id="stok_op"></td>							
					</tr>
					<tr><td>Permintaan</td>
						<td><input type="text" class="form-control" name="minta" id="permintaan"></td>
						<!--td>Pemberian</td>
						<td><input type="text" class="form-control" name="pemberian" id="pemberian"></td-->
						<td>Keterangan</td>
						<td colspan="3"><input type="text" class="form-control" name="ket" id="ket"></td>						
					</tr>
					<tr>
						<td colspan="6"><div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_isian_lplpo" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button type="button" id="batal" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
						
					</tr>
				</table>
			<!--/div-->
			</form>
			
			<br><br>

			<div id="list_isian_lplpo" class="table-responsive">
				<div class="progress">
				  	<div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%;"></div>
				</div>
			</div>
	</div>
</div>
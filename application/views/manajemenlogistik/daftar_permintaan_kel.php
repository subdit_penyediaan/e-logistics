<script type="text/javascript">
	$(function(){
		$('.table-content').DataTable();
		$("#pagelistdaftar_permintaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_daftar_permintaan').load(url);
		});
		$("#trcontent").on("click",".btn-detail",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<br>
<div id="pagelistdaftar_permintaan">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered table-content">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>Tanggal Masuk</th>
			<th>Kecamatan</th>
			<th>Puskesmas</th>
			<th>Periode</th>
			<th>Status</th>
			<th>Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php foreach ($result->result() as $rows): ?>
				<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_daftar_permintaan" value="<?= $rows->id ?>" class="chk"></td>
					<td><?= $rows->cur_date ?></td>
					<td><?= $rows->kecamatan ?></td>
					<td><?= $rows->puskesmas ?></td>
					<td><?= $rows->periode ?></td>
					<td><?= $status ?></td>
					<td><button href="<?= $base_url ?>index.php/manajemenlogistik/distribusi_obat_kel/get_detail/<?= $rows->id ?>" id="" class="btn-detail"><span class="glyphicon glyphicon-list"></span> Lihat</button></td>
				</tr>
			<?php endforeach; ?>
			<?php foreach ($finished->result() as $rows): ?>
				<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_daftar_permintaan" value="<?= $rows->id ?>" class="chk"></td>
					<td><?= $rows->cur_date ?></td>
					<td><?= $rows->kecamatan ?></td>
					<td><?= $rows->puskesmas ?></td>
					<td><?= $rows->periode ?></td>
					<td>Sudah Diproses</td>
					<td><button href="<?= $base_url ?>index.php/manajemenlogistik/distribusi_obat_kel/get_detail_done/<?= $rows->id ?>" id="" class="btn-detail"><span class="glyphicon glyphicon-list"></span> Lihat</button></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>
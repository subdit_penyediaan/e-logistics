<script type="text/javascript">
	$(function(){
		$('.table-content').DataTable();
		// $( "#fornas-name").autocomplete({
		// 		source:'manajemenlogistik/distribusi_obat/get_list_obat', 
		// 		minLength:2,
		// 		focus: function( event, ui ) {
		// 	        $( "#fornas-name" ).val( ui.item.value +" "+ ui.item.pwr);
		// 	        return false;
		// 	      },
		// 		select:function(evt, ui)
		// 		{					
		// 			$('#kode_obat_'+index).val(ui.item.kode_obat);
		// 			$('#no_batch_'+index).val(ui.item.batch);
		// 			$('#id_stok_'+index).val(ui.item.id_stok);
					
		// 			return false;
		// 		}
		// 	})
		
		// 	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		//       return $( "<li>" )
		//       	.data( "ui-autocomplete-item", item )
		//         .append("<a>"+ item.value +" "+item.pwr + "<br><small>Stok: <i>" + item.stok + "<i></small></a>")
		//         .appendTo( ul );
		//     };

		$('.history').click(function(e){
			e.preventDefault();
			var url = $(this).attr("href");

			window.open(url);
		})

		$("#page-table").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_lplpo_list').load(url);
		});

		$('#form_detail_lplpo').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            beforeSend: function(){
		            showBusySubmit();
		        },
	            success: function (data) {
	            	if(data=='0'){
	            		alert("Periode distribusi belum di setting");
	            	}else{
	            		alert('Data berhasil diupdate');
		                var url="manajemenlogistik/isian_lplpo_kel/get_data_isian_lplpo/"+data;
						$('#list_isian_lplpo').load(url);
	            	}
	            	$('#up-konten').unblock();
				}
			})
			
		});

		$('.table-content').on("click", ".btn-save", function(e){
			e.preventDefault();
			var element = $(this);

			id = $(this).parent().parent().find('.id-rko').val();
			stock = $(this).parent().parent().find('.stock-two-years-ago').val();
			average_used = $(this).parent().parent().find('.average-used').val();
			needed_plan = $(this).parent().parent().find('.needed-plan').val();
			needed = $(this).parent().parent().find('.needed').val();
			real = $(this).parent().parent().find('.real-two-years-ago').val();
			info = $(this).parent().parent().find('.information').val();
			procurement_plan = $(this).parent().parent().find('.procurement-plan').val();

			datanew = "dt[stock_two_years_ago]="+stock+
				"&dt[average_used_two_years_ago]="+average_used+"&dt[need]="+needed+"&dt[procurement_plan]="+procurement_plan+
				"&dt[needed_plan]="+needed_plan+"&dt[real_two_years_ago]="+real+"&dt[information]="+info;

			var url='<?= base_url(); ?>manajemenlogistik/rko/update_item/'+id;
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				data: datanew,
				success: function(response){
					alert(response.message);
					if(response.status == 'success') {
						element.parent().parent().find('.needed-plan').val(response.data.needed_plan);
						element.parent().parent().find('.procurement-plan').val(response.data.needed_plan);
						element.parent().parent().find('.needed').val(response.data.need);
					}
				}
			})
		})

		$('.table-content').on("click", ".btn-delete", function(e){
			e.preventDefault();
			var element = $(this);
			id = $(this).parent().parent().find('.id-rko').val();
			var url='<?= base_url(); ?>manajemenlogistik/rko/delete_item/'+id;
			var aprv = confirm("Are you sure?");

			if (aprv) {
				$.ajax({
					url: url,
					type: "GET",
					dataType: "json",
					success: function(response){
						alert(response.message);
						if(response.status == 'success') {
							element.closest("tr").remove();
						}
					}
				})
			}
		})

		$('#btn-send').on("click", function(e) {
			e.preventDefault();

			var url = '<?= base_url(); ?>manajemenlogistik/rko/send/<?= $year; ?>';
			$.ajax({
				url: url,
				type: "GET",
				dataType: "json",
				beforeSend: function(){
		            showBusySubmit();
		        },
				success: function(response) {
					alert(response.message);
					$('#up-konten').unblock();
				}
			})
		})

		$('.btn-integrate').on("click", function(e) {
			e.preventDefault();

			var url = $(this).attr("href");
			$.ajax({
				url: url,
				type: "GET",
				dataType: "json",
				beforeSend: function(){
		            showBusySubmit();
		        },
				success: function(response) {
					alert(response.msg);
					$('#up-konten').unblock();
				}
			})
		})

	});


	function showBusySubmit(){
        $('#up-konten').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }
</script>
<div id="page-table">
	<table class="table table-striped table-bordered table-content">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Kode</th>
			<th>Nama</th>
			<th>Satuan</th>
			<th>Sisa Stok per 31 Des <?= $twoYearsAgo?></th>
			<th>Pemakaian Rata-rata per bulan th <?= $twoYearsAgo?></th>
			<th>Jumlah Kebutuhan <?= $year?></th>
			<th>Rencana Kebutuhan <?= $year?></th>
			<th>Rencana Pengadaan <?= $year?></th>
			<th>Realisasi Pengadaan <?= $twoYearsAgo?></th>
			<th>Harga *)</th>
			<th>Keterangan</th>
			<th width="10%">Action</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php $i=0; foreach ($content as $rows): ?>
				<?php $j=$i+1; ?>
					<tr style="">
						<td>
							<?= $j ?>
							<input type="hidden" class="id-rko" id="" name="" value="<?= $rows->id ?>">
						</td>
						<td><?= $rows->fornas_id ?></td>
						<td><?= $rows->nama_obat ?></td>
						<td><?= $rows->unit ?></td>
						<td><input type="text" id="" size="8" class="stock-two-years-ago" name="stock[]" value="<?= $rows->stock_two_years_ago ?>"></td>
						<td><input type="text" id="" size="8" class="average-used" name="average_two_years_ago[]" value="<?= $rows->average_used_two_years_ago ?>"></td>
						<td><input type="text" id="" size="8" class="needed" name="need[]" value="<?= $rows->need ?>"></td>
						<td><input type="text" id="" size="8" class="needed-plan" name="needed_plan[]" value="<?= $rows->needed_plan ?>" ></td>
						<td><input type="text" id="" size="8" class="procurement-plan" name="procurement_plan[]" value="<?= $rows->procurement_plan ?>"></td>
						<td><input type="text" id="" class="real-two-years-ago" name="real_two_years_ago[]" size="8" value="<?= $rows->real_two_years_ago ?>"></td>
						<td><input type="text" id="" class="price" name="price[]" size="8" value="<?= $rows->price ?>"></td>
						<td><input type="text" id="" class="information" name="information[]" size="8" value="<?= $rows->information ?>"></td>
						<td>
							<button type="button" class="btn btn-success btn-xs btn-save">
								<span class="glyphicon glyphicon-floppy-saved"></span>
							</button>
							<button type="button" class="btn btn-danger btn-xs btn-delete">
								<span class="glyphicon glyphicon-remove"></span>
							</button>
						</td>
					</tr>
			<?php $i++; endforeach; ?>
		</tbody>
	</table>
	<button class="btn btn-success btn-lg"><span class="glyphicon glyphicon-pencil"></span> Update All</button>
	<button class="btn btn-primary btn-lg" id="btn-send"><span class="glyphicon glyphicon-refresh"></span> Kirim</button>
	<a href="<?= base_url(); ?>manajemenlogistik/rko/download/<?= $year?>" class="btn btn-info btn-lg history"><span class="glyphicon glyphicon-download"></span> Export Excel</a>
	<a href="<?= base_url(); ?>manajemenlogistik/rko/integrate/<?= $year?>" class="btn btn-primary btn-lg btn-integrate"><span class="glyphicon glyphicon-upload"></span> Integrate</a>
</div>
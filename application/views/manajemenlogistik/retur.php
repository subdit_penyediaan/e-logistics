<style>
.datepicker{
	z-index:1151;
}
.ui-autocomplete { z-index:2147483647; }
</style>
<script>
$(document).ready(function(){
	$( "#nama_sediaan").autocomplete({
				source:'manajemenlogistik/retur/get_list_obat', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_sediaan").val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{

					$('#kode_obat').val(ui.item.kode_obat);
					$('#no_batch').val(ui.item.batch);
					$('#id_stok').val(ui.item.id_stok);
					$('#tanggal_faktur').val(ui.item.tanggal);
					$('#pbf').val(ui.item.pbf);
					$('#no_faktur').val(ui.item.no_faktur);
					return false;
				}
			})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small>Stok/Batch: " + item.stok + "/" + item.batch + "<br>Expired date: "+ item.ed +"</small></a>")
		        .appendTo( ul );
		    };
	//$("#form_retur").validate();

	//============== submit add form

	$("#upt_retur").click(function(){
		var url2="manajemenlogistik/retur/update_data";
		var form_data = {
			no_faktur:$("#cha_no_faktur").val(),
			periode:$("#cha_periode").val(),
			tanggal:$("#cha_tanggal").val(),
			pbf:$("#cha_pbf").val(),
			nama_dok:$("#cha_nama_dok").val(),
			no_kontrak:$("#cha_no_kontrak").val(),
			dana:$("#cha_dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("berhasil update data");
				$('#change_retur').modal('toggle');
				var url_hasil="manajemenlogistik/retur/get_data_retur"
				$("#list_retur").load(url_hasil);

				$("#cha_no_faktur").val("");
				$("#cha_periode").val("");
				$("#cha_datepicker").val("");
				$("#cha_pbf").val("");
				$("#cha_nama_dok").val("");
				$("#cha_no_kontrak").val("");
			}
		});
	})

	//============== end submit add form
	
	//autocomplete pbf
	$('#cha_pbf').autocomplete({
				source:'manajemenlogistik/retur/get_list_pbf', 
				minLength:2,
			});
	//end autocomplete pbf

	//autocomplete pbf
	$('#pbf').autocomplete({
				source:'manajemenlogistik/retur/get_list_pbf', 
				minLength:2,
				/*select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#obat_hidden').val(ui.item.kode_obat);
					//this.form.obat_hidden.value = ui.item.kode_obat;
				}*/
			});
	//end autocomplete pbf
	$('#cha_retur-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	//$("#change_retur").modal('show');
	    	//alert('manajemenlogistik/retur/datatochange/' + id_array[0]);
	    	//$('#cha_no_faktur').val("testing");
	    	//$('#change_retur').modal('show');
	    	var form_data = { no_faktur: id_array[0] };
	    	$.ajax({
	    		type:"POST",
	    		url:'manajemenlogistik/retur/datatochange/',
	    		data: form_data,
	    		dataType:'json',
	    		success:function(data) {
	    			$('#labelnofaktur').text(data[0].no_faktur);
	    			$('#cha_no_faktur').val(data[0].no_faktur);
	    			$('#cha_tanggal').val(data[0].tanggal);
	    			$('#cha_nama_dok').val(data[0].nama_dok);
	    			$('#cha_periode').val(data[0].periode);
	    			$('#cha_pbf').val(data[0].pbf);
	    			$('#cha_no_kontrak').val(data[0].no_kontrak);
	    			$( "#cha_dana option:selected" ).text(data[0].dana).attr('value', data[0].dana);
	    			$('#change_retur').modal('show');
	    			//return false;
	    		}
	    	});
	    	
	    }
	})

	//=========== del button
		
	$("#del_retur_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array.length > 1) {alert("pilih satu saja")}
        else if(id_array.length ==1){
    		var sure=confirm("Are you sure?");
	        	if(sure){
	        		$.ajax({
			        	url: "manajemenlogistik/retur/delete_list",
			        	data: "kode="+id_array,
			        	type: "POST",
			        	success: function(){
			        		alert("data berhasil dihapus");
			        		var url_hasil="manajemenlogistik/retur/get_data_retur"
							$("#list_retur").load(url_hasil);
			        	}
			        })	
	        	}

        	}
        else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	//$('#form_retur_modal').hide();
	$("#add_retur_btn").on("click",function (event){
			//$("#add_retur_modal").modal('show');
			$("#form_retur_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_retur_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			event.preventDefault();
			$(".form-control").val('');
			//$("#form_retur_modal").slideUp("slow");
			//$("#partbutton").fadeIn();
				//$("#add_retur_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="manajemenlogistik/retur/get_data_retur";
	$('#list_retur').load(url);	
	//============== submit add form
	$("#form_retur").validate({
		submitHandler: function(form){
			var url="manajemenlogistik/retur/input_data";
			var data=$("#form_retur").serialize();
			$.post(url,data,function(){
				alert("sukses tambah data");
				//$("#form_retur_modal").slideUp("slow");
				//$("#form_retur_modal").reset();
				//$("#partbutton").fadeIn();			
				var url_hasil="manajemenlogistik/retur"
				$("#konten").load(url_hasil);
				//$('.form-control').val('');
			});
			return false;
		}
	});

	$("#btn_returXXX").click(function(){
	
		var url2="manajemenlogistik/retur/input_data";
		var form_data = {
			no_faktur:$("#no_faktur").val(),
			periode:$("#periode").val(),
			tanggal:$("#tanggal").val(),
			pbf:$("#pbf").val(),
			nama_dok:$("#nama_dok").val(),
			no_kontrak:$("#no_kontrak").val(),
			dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#form_retur_modal").slideUp("slow");
				//$("#form_retur_modal").reset();
				$("#partbutton").fadeIn();
				//var url_hasil="manajemenlogistik/retur_obat/get_list_retur"
				var url_hasil="manajemenlogistik/retur/get_data_retur"
				$("#list_retur").load(url_hasil);//+"#list_sedian_obat");

				$("#no_faktur").val("");
				$("#periode").val("");
				$("#datepicker").val("");
				$("#pbf").val("");
				$("#nama_dok").val("");
				$("#no_kontrak").val("");
			}
		});
	})

	//============== end submit add form
	
});
</script>
<div class="panel panel-primary" id="halaman_retur">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Retur Obat</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<!--div id="partbutton">
			<div class="col-lg-8">
				<button id="add_retur_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_retur_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_retur-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div>< /input-group >
			</div>
		</div>< /col6 -->
	<ul class="nav nav-tabs">
		<li class="active"><a href="#pane_retur" data-toggle="tab">Form Retur</a></li> 
	    <li><a href="#histori_retur" data-toggle="tab">List Obat Retur</a></li>  
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="pane_retur">
		<br>
		<div id="form_retur_modal">
		<form id="form_retur">
				<table class="table">
					<tr>
						<td><small style="color:red;"><i>*</i></small> Nama Sediaan</td>
						<td><input type="text" class="form-control required" name="nama_sediaan" id="nama_sediaan">
							<input type="hidden" name="kode_obat" id="kode_obat">
							<input type="hidden" name="id_stok" id="id_stok">
							</td>
						<td>No. Batch</td>
						<td>
							<input type="text" class="form-control" name="no_batch" id="no_batch" readonly>
						</td>
					</tr>
						<tr>
							<td>Nomor Faktur</td>
							<td><input type="text" class="form-control" name="no_faktur" id="no_faktur" readonly></td>
							<td>Tanggal Faktur</td>
							<td>
								<input type="text" class="form-control" name="tanggal_faktur" id="tanggal_faktur" readonly>
							</td>
						</tr>
						<tr>
							<td>Nama PBF</td>
							<td><input type="text" class="form-control" name="pbf" id="pbf" readonly></td>
							<td>Keterangan</td>
							<td><input type="text" class="form-control" name="keterangan" id="keterangan"></td>							
						</tr>
						<tr>
							<td><small style="color:red;"><i>*</i></small>Jumlah</td>
							<td><input type="text" class="form-control required" name="jumlah" id="jumlah"></td>
							<td><small style="color:red;"><i>*</i></small>Tanggal Retur</td>
							<td>
								<div class="input-append date" id="datepicker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
									<input class="span2 required" size="12" type="text" readonly="readonly" id="tanggal_retur" name="tanggal_retur">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>
									<script>
									$('#datepicker').datepicker();
									</script>
							</td>							
						</tr>
						<tr><td colspan="4"><small style="color:red;"><i>*Harus diisi</i></small> </td>
						</tr>
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_retur" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Reset</button>
								</div>
							</td>
							
						</tr>
					</table>
				</form>
				</div>
			</div>
			<div class="tab-pane" id="histori_retur">
				<div id="list_retur"></div>
			</div>
		</div>
	</div>
</div>


<!-- MODAL UBAH DATA -->
<div class="modal fade" id="change_retur" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
      </div>
      <div class="modal-body">

<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
	
		<table class="table">
			<tr>
				<td>Nomor Faktur</td>
				<td><label name="labelnofaktur" id="labelnofaktur"></label>
					<input type="hidden" class="form-control" name="cha_no_faktur" id="cha_no_faktur"></td>				
			</tr>
			<tr>
				<td>Tanggal Faktur</td>
				<td>
					<div class="input-append date" id="datepicker5" data-date="2014-04-15" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" value="" readonly="readonly" id="cha_tanggal">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

						<script>
						$('#datepicker5').datepicker();
						</script>
					
				</td>
				<td>Keterangan</td>
				<td><input type="text" class="form-control" name="cha_nama_dok" id="cha_nama_dok"></td>
				
			</tr>
			<tr>
				<td>Nama PBF</td>
				<td><input type="text" class="form-control" name="cha_pbf" id="cha_pbf"></td>
				<td>Nomor Kontrak</td>
				<td><input type="text" class="form-control" name="cha_no_kontrak" id="cha_no_kontrak"></td>
				
			</tr>			
			<tr>
				<td><div class="pagingContainer">
						<button type="submit" name="Simpan" id="upt_retur" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button id="upt_batal"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
				</td>
				<td>
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>
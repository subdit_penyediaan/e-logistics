 <style>
	
</style>
<script>
$(document).ready(function(){
	$(".date").datepicker();
	$('#add_obat_rusak').hide();
	$("#add_save_obat_rusak").click(function(){
		var url2="manajemenlogistik/pemusnahan_obat/input_obat_rusak";
		var form_data = {
			kode_obat:$("#kode_obat").val(),
			no_batch:$("#no_batch").val(),
			no_faktur:$("#no_faktur").val(),
			id_stok:$("#id_stok").val(),
			jumlah:$("#jml_rusak").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_obat_rusak").slideUp("slow");
				//$("#form_pemusnahan_obat_modal3").reset();
				$("#add_obat_rusak").fadeIn();
				var url_hasil="manajemenlogistik/pemusnahan_obat/get_obat_rusak"
				$("#list_pemusnahan_obat3").load(url_hasil);//+"#list_sedian_obat");

				$("#kode_obat").val("");
				$("#no_faktur").val("");
				$("#no_batch").val("");
				$("#jml_rusak").val("");
				$("#nama_obat").val("");
				$("#id_stok").val("");
			}
		});
	})

	//============== end submit add form

	$('#nama_obat').autocomplete({
				source:'manajemenlogistik/pemusnahan_obat/get_list_obat2', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
	
					$('#kode_obat').val(ui.item.kode_obat);
					$('#no_batch').val(ui.item.nobatch);
					$('#label_batch').text(ui.item.nobatch);
					$('#no_faktur').val(ui.item.no_faktur);
					$('#id_stok').val(ui.item.id_stok);
					return false;
				}
			})
 		
 		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			      return $( "<li>" )
			        .append("<a>"+ item.value +"<br><small>Stok: "+ item.stok +
			        	"<br>No. Batch: "+ item.nobatch +
			        	"<br>No. Faktur: "+item.no_faktur+"</small></a>")
			        .appendTo( ul );
			    };
	//end autocomplete	

	//=========== show add form
	$('#form_pemusnahan_obat_modal3').hide();

	$("#add_pemusnahan_obat_btn3").on("click",function (event){
			//$("#add_pemusnahan_obat_modal").modal('show');
			$("#form_pemusnahan_obat_modal3").slideDown("slow");
			$("#partbutton3").fadeOut();
			
		});
	$('#form_obat_rusak').hide();
	$('#add_obat_rusak').on("click",function(event){
		$('#form_obat_rusak').slideDown("slow");
		$(this).fadeOut();
	})
	$("#batal4").on("click",function (event){
			
			$("#form_obat_rusak").slideUp("slow");
			$("#add_obat_rusak").fadeIn();
				
		});
	$("#batal3").on("click",function (event){
			
			$("#form_pemusnahan_obat_modal3").slideUp("slow");
			$("#partbutton3").fadeIn();
				
		});
	//================ end show add form

	var url="manajemenlogistik/pemusnahan_obat/get_obat_rusak";
	$('#list_pemusnahan_obat3').load(url);	
	//============== submit add form

	$("#btn_pemusnahan_obat_rusak3").click(function(){
		var url2="manajemenlogistik/pemusnahan_obat/input_data";
		var form_data = {
			cur_date:$("#cur_date3").val(),
			no_dok:$("#no_dok3").val(),
			saksi1:$("#saksi1c").val(),
			nip1:$("#nip1c").val(),
			saksi2:$("#saksi2c").val(),
			nip2:$("#nip2c").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			dataType: "json",
			success:function(response){
				$('#alert-custom-so').addClass('alert-'+response.status);
				if (response.status == 'success') {
					$("#form_pemusnahan_obat_modal3").slideUp("slow");

					$("#add_pemusnahan_obat_btn3").hide();
					$("#partbutton3").fadeIn();
					$("#add_obat_rusak").show();				

					$("#cur_date3").val("");
					$("#no_dok3").val("");
					$("#saksi1c").val("");
					$("#nip1c").val("");
					$("#saksi2c").val("");
					$("#nip2c").val("");
				}
				$("#alert-title-so").text(response.status);
				$("#message-so").text(response.message);
				$('#alert-custom-so').show();
			}
		});
	})

	//============== end submit add form
});
</script>
	<!-- bag. isi -->
	<br>
	<div class="alert alert-custom" role="alert" id="alert-custom-so" style="display: none;">
		<h4 id="alert-title-so"></h4>
		<p id="message-so"></p>
	</div>
	<div id="partbutton3">
		<div class="col-lg-12">
			<button id="add_pemusnahan_obat_btn3" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Buat Berita Acara Pemusnahan</button>		
		</div>
	</div>
	<hr>
	<div class="" id="form_pemusnahan_obat_modal3">
	  <h4>Info Pemusnahan</h4>
		<table class="table">
			<tr>
				<td width="10%">Tanggal</td>
				<td>
					<div class="input-group input-append date" id="" data-date="<?= date('Y');?>-01-01" data-date-format="yyyy-mm-dd" >
						<input class="form-control span2" size="12" type="text" value="<?= date('Y');?>-01-01" id="cur_date3" name="cur_date3" readonly="readonly" >
						<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</td>
				<td width="10%">Nomor Dokumen</td>
				<td><input type="text" class="form-control" name="no_dok3" id="no_dok3"></td>
			</tr>
			<tr>
				<td>Saksi 1</td>
				<td><input type="text" class="form-control" name="saksi1c" id="saksi1c"></td>
				<td>NIP</td>
				<td><input type="text" class="form-control" name="nip1c" id="nip1c"></td>
				
			</tr>
			<tr>
				<td>Saksi 2</td>
				<td><input tbype="text" class="form-control" name="saksi2" id="saksi2"></td>
				<td>NIP</td>
				<td><input type="text" class="form-control" name="nip2c" id="nip2c"></td>
				
			</tr>
			<tr>
				<td colspan="4">
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_pemusnahan_obat_rusak3" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<button class="btn btn-warning btn-sm" id="batal3"><span class="glyphicon glyphicon-remove"></span> Batal</button>
					</div>
				</td>				
			</tr>
		</table>
	</div>
	<div class="" id="form_obat_rusak">	  	
		<table class="table">
			<tr>
				<td>Nama Obat</td>
				<td><input type="text" class="form-control" name="nama_obat" id="nama_obat">
					<input type="hidden" id="kode_obat" name="kode_obat" >
					<input type="hidden" id="no_faktur" name="no_faktur" >
					<input type="hidden" id="id_stok" name="id_stok" >
					No. batch: <label id="label_batch"></label>
					<input type="hidden" id="no_batch" name="no_batch" >
				</td>
				<td>Jumlah</td>
				<td><input type="text" class="form-control" name="jml_rusak" id="jml_rusak"></td>
			</tr>				
			<tr>
				<td colspan="4">
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="add_save_obat_rusak" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<button class="btn btn-warning btn-sm" id="batal4"><span class="glyphicon glyphicon-remove"></span> Batal</button>
					</div>
				</td>					
			</tr>
		</table>
	</div>
<!--/form-->
	<button id="add_obat_rusak" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Tambah Obat</button>
	<hr>
	<div id="list_pemusnahan_obat3"></div>
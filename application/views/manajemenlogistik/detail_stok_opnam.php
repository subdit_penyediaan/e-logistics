 <style>
 	.datepicker{
		z-index:1151;
	}

	tr{
		padding: 8px;

	}
	td{
		padding: 8px;
	}
	#info_stok_opnam{
		background-color: #99CCFF;
	}

	.dataTables_filter{
		float:right;
	}

	.pagination{
		float:right;
	}

	.table{
		margin-bottom: 0px ! important;
	}
</style>
<script>
$(document).ready(function(){
	$('#cha_nama_inn').autocomplete({
		open: function(){
	        setTimeout(function () {
	            $('.ui-autocomplete').css('z-index', 99999999999999);
	        }, 0);
	    },
		source:'<?= base_url(); ?>manajemenlogistik/detail_faktur/get_list_generik', 
		minLength:2,
		focus: function( event, ui ) {
	        $( "#cha_nama_inn" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{					
			$('#kode_inn_u').val(ui.item.id_generik);
			
			return false;
		}
	})

	$(".form-update-stok").on("submit", function(e) {
		e.preventDefault();
		$.ajax({
			type: $(this).attr("method"),
			url: $(this).attr("action")+'/'+$("#id-stok").val(),
			data: $(this).serialize(),
			dataType: "json",
			success: function(response) {
				alert(response.text);
				if (response.status == "success") {
					$(".btn-cancel").click();
					$('#batch-'+response.id_stok).text(response.batch_new);
					$("#time-"+response.batch_old).text('<?= date('Y-m-d H:i:s')?>');
				}
			}
		})
	})
	$(".btn-edit-so").on("click", function(event) {
		event.preventDefault();

		var url="<?= base_url(); ?>manajemenlogistik/detail_faktur/get_detail_obat_penerimaan/"
		var id_stok = $(this).parent().parent().find('.id_stok').val();
		var form_data = { no_batch: id_stok };

		$.ajax({
			type:"POST",
			url:url,
			data: form_data,
			dataType:'json',
			success:function(data){
				$('#id-stok').val(id_stok);
				$('#dtl_nama_sediaan').text(data.nama_obj);
				$('#cha_batch_ori').val(data.batch_stok);
				$('#cha_batch_new').val(data.batch_stok);
				$('#cha_batch').val(data.id);
				$('#cha_tanggal').val(data.ed_stok);
				$('#cha_nama_inn').val(data.nama_objek);
				$('#kode_inn_u').val(data.id_hlp);				
				$('#cha_kode_obat').val(data.kode_obat);
				$.get('<?= base_url()?>manajemenlogistik/detail_stok_opnam/history/'+id_stok, function(html) {
					$("#history-batch").html(html);
				})
			}
		});

		$("#modal-stok-opnam").modal('show');
	})

	$('#datepicker612').datepicker();

	$("#btn-print").click(function(){
		var urlprint="<?= base_url(); ?>manajemenlogistik/detail_stok_opnam/printOut/"+$('#hide_nostok').val();
		window.open(urlprint);		
	})

	$('.submitEdit').on("click",function(e){
		e.preventDefault();
	      t = $(this);
	      ins_id =  $(this).parent().parent().find('.id_stok').val();
	      ins_idstokopname =  $(this).parent().parent().find('.id_stokopname').val();
	      ins_qty =  $(this).parent().parent().find('.in_fisik').val();
		  ins_notedstok =  $(this).parent().parent().find('.notedstok').val();
	      ins_ket =  $(this).parent().parent().find('.in_ket').val();
	      ins_price =  $(this).parent().parent().find('.in_price').val();
	      	      
	      url="<?=base_url()?>manajemenlogistik/detail_stok_opnam/edit_list";
	      datapost = "up[id]="+ins_idstokopname+"&up[fisik_stok]="+ins_qty+"&up[ket]="+ins_ket+
	      			"&notedstok="+ins_notedstok+"&idstok="+ins_id+"&up[harga_so]="+ins_price;
	      // $.post(url,datapost,function(){
	      // 	alert('data berhasil disimpan');
	     	// var url="manajemenlogistik/detail_stok_opnam/get_detail/<?php echo $detailstok['id']?>";
	     	// $('#konten').load(url);
       //      //reloadTable();              
       //    })

          $.ajax({
          	url: url,
          	type: "POST",
          	data: datapost,
          	dataType: "json",
          	success: function (res) {
          		alert(res.message);
          		if (res.status == 'success') {
          			cum = ins_price * ins_qty;
          			t.parent().parent().find('.new-stock').text(res.dist);
          			t.parent().parent().find('.cumulative-price').text(cum);
          			t.parent().parent().find('.time-update').text('<?= date('Y-m-d H:i:s')?>');
          		}
          	}
          })
	  })

	$('.tabel-stok').dataTable();
	$('#back_to_').click(function(){
			var url_penerimaan='manajemenlogistik/stok_opnam';
			$('#konten').load(url_penerimaan);
		})
	

	//var url="manajemenlogistik/detail_stok_opnam/get_data_detail_stok/<?php echo $detailstok['id']?>";
	//$('#list_detail_stok_opnam_obat').load(url);

	//var url="manajemenlogistik/detail_stok_opnam/get_data_detail_stok/"+$("#hide_nostok_opnam").val();
	//mengambil data dg key no_struk
	//$('#list_detail_stok_opnam').load(url);	
	
	$('#hidden_id_stok_opname').val(<?php echo $detailstok['id']?>);
});

</script>
<div class="panel panel-primary" id="halaman_detail_stok_opnam">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Detail Stok Opname</b><button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<!-- Informasi Detail stok_opnam -->
			<div class="well" id="">				
				<table class="table">
					<tr>
						<td width="20%">Nomor Stok Opname</td>
						<td>: <?php echo $detailstok['no_stok_opnam'];?>
							<input type="hidden" name="hide_nostok" id="hide_nostok" value="<?php echo $detailstok['id']?>">
						</td>
						<td width="20%">Bulan/Periode</td>
						<td>: <?php echo $detailstok['periode']; ?></td>
					</tr>
					<tr>
						<td>Tanggal Transaksi</td>
						<td>: <?php echo $detailstok['tgl_trans']; ?></td>
						<td>Nama User</td>
						<td>: <?php echo $detailstok['nama_user']; ?></td>
					</tr>
					<tr>
						<td>Catatan</td>
						<td colspan="3">: <?php echo $detailstok['cttn']; ?></td>			
					</tr>
				</table>
			</div>
			<hr>
			<!-- end informasi detail stok_opnam -->
			<div id="list_detail_stok_opnam">
				<table cellpadding="0" cellspacing="0" border="0" class="tabel-stok table table-bordered table-striped">
				    <thead>
				      <tr id="list" class="active">
				        <th>No.</th>
						<th>Nama Obat</th>						
						<th>Stok Tercatat</th>
						<th>Stok Fisik</th>
						<th>Selisih</th>
						<th>Keterangan</th>
						<th>Harga Saat Ini</th>
						<th>Kumulatif</th>
						<th>Last Update</th>
						<th width="15%">Action</th>
				      </tr>
				    </thead>
				    <tbody id="list-data">
				    <?php $i=1; foreach ($liststok->result() as $list):?>
				    	<?php
				    		$phisyc_stock = (!empty($list->fisik_stok)) ? $list->fisik_stok : 0;
				    		$cumulative_price = round($list->harga_so * $phisyc_stock, 2);
				    	?>
				      <tr>        
				        <td class="center"><?=$i++?></td> 				        
				        <td><?=$list->nama_obj?><br>
				        	<small>Kode Obat/No batch: <?=$list->kode_obat?>/<b id="batch-<?=$list->id_so?>"><?=$list->no_batch?></b></small>
				        	<input type="hidden" class='id_stok' value="<?=$list->id_so?>" id="">
				            <input type="hidden" class='id_stokopname' value="<?=$list->id?>" id="">
				        </td>
				        <td><?=$list->noted_stok?>
				        	<input type="hidden" class="notedstok" value="<?=$list->noted_stok?>"></td>
				        <td><input type="text" class="in_fisik form-control" value="<?= $phisyc_stock ?>" size="7"></td>
				        <td><p class="new-stock"><?=(!empty($list->selisih)) ? $list->selisih : 0 ?></p></td>
				        <td><input type="text" class="in_ket form-control" value="<?= (!empty($list->ket)) ? $list->ket : '' ?>" ></td>
				        <td><input type="text" class="in_price form-control" value="<?= $list->harga_so; ?>" name="" size="7"></td>
				        <td><p class="cumulative-price"><?= $cumulative_price ?></p></td>
				        <td><p class="time-update" id="time-<?=$list->no_batch?>"><?= $list->last_update ?></p></td>
				        <td class="center"> 				          
				        	<button class="btn-warning btn-sm btn-edit-so" style="" type="button">
				            	<span class="glyphicon glyphicon-pencil">
				            </button>
				            <button class="btn-success btn-sm submitEdit" style="">
				            	<span class="glyphicon glyphicon-floppy-saved">
				            </button>
				        </td> 
				      </tr>
				    <?php endforeach?>               
				    </tbody>
				    <tfoot>
				    </tfoot>
				  </table>
			</div>
	</div>
</div>
<center><button type="button" class="btn btn-info" id="btn-print"><span class="glyphicon glyphicon-print"></span> Print</button>
<div class="modal fade" id="modal-stok-opnam" tabindex="-1" role="dialog" aria-labelledby="modal-stok-opnamLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal-stok-opnamLabel">Detail Barang</h4>
      </div>
      <form action="<?= base_url();?>manajemenlogistik/detail_stok_opnam/update" method="POST" class="form-update-stok">
      <div class="modal-body">
      	<table class="table">
			<tr class="active">
				<td width="20%">Nama Sediaan</td>
				<td width="" colspan="3">
					<label id="dtl_nama_sediaan"></label>
					<input type="hidden" name="" id="id-stok" value="">
				</td>
			</tr>
			<tr class="active">
				<td width="20%">Nama INN/FDA</td>
				<td width="" colspan="3">
					<input type="text" class="form-control edittext" name="cha_nama_inn" id="cha_nama_inn" size="">
					<input type="hidden" id="kode_inn_u" name="data[kode_generik]" value="">
				</td>				
			</tr>
			<!-- <tr>
				<td>No Batch</td>
				<td><label id="dtl_no_batch" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_batch_ori" id="cha_batch_ori">
					<input type="hidden" class="" name="cha_batch" id="cha_batch">
					<input type="hidden" class="" name="cha_kode_obat" id="cha_kode_obat">
					<input type="hidden" class="" name="cha_no_faktur" id="cha_no_faktur">
				</td>
			</tr> -->
			<tr>
				<td>No Batch</td>
				<td>
					<input type="hidden" class="form-control edittext" name="batch_old" id="cha_batch_ori">
					<input type="text" class="form-control edittext" name="batch_new" id="cha_batch_new">
				</td>
				<td>Tanggal Kadaluarsa</td>
				<td>
					<div class="input-group input-append date edittext" id="datepicker612" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >						
						<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
						<input class="form-control span2" size="12" type="text" id="cha_tanggal" name="data[expired]">
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="4">
					<label>
						<input type="checkbox" class="" value="0" name="data[flag]" id="flag-exhausted"> Tandai sebagai obat yang telah habis
					</label>
				</td>
			</tr>
			<!-- <tr>
				<td colspan="4">
					<label>
						<input type="checkbox" class="" value="0" name="exchange" id=""> Tandai sebagai obat yang ditukar
					</label>
				</td>
			</tr> -->
		</table>
		<div id="history-batch">
			
		</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success" id="btn_save_update">Simpan</button>
      </div>
  	</form>
    </div>
  </div>
</div>
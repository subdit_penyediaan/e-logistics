<style type="text/css">
.ui-autocomplete-loading { 
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}
</style>
<script type="text/javascript">
$(function(){
	$('.obat_pemberian').autocomplete({
		source:'manajemenlogistik/detail_distribusi/get_list_obat2', 
		minLength:2,
		focus: function( event, ui ) {
	        $( ".obat_pemberian" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{
			$('.kode_obat').val(ui.item.kode_obat);
			$('.no_batch').val(ui.item.nobatch);
			//$('#label_batch_1').text(ui.item.nobatch);
			$('.jml_beri_new').focus();
			$('.id_stok_hidden').val(ui.item.id_stok);
			return false;
		}
	})
 	.each(function(){
 		$(this).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append("<a>"+ item.value +"<br><small>Stok: <i>"+ item.stok +"</i><br>Expired: "+item.ed+"<br>Produsen: "+item.produsen+"<br>Sumber dana: "+item.sumber_dana+"</small></a>")
	        .appendTo( ul );
	    };
 	});
	
	//end autocomplete	

	$('.editable').hide();
	$("#print_btn").click(function(){
		var urlprint="manajemenlogistik/distribusi_obat/printOut/<?php echo $id_lplpo; ?>";
		window.open(urlprint);		
	})

	$('.btn_edit').click(function(){
		$(this).parent().parent().find('.show_t').hide();
		$(this).parent().parent().find('.editable').show();
		//$(this).parent().find('.btn_ge').hide();
		//$('.editable').show();
		$('.btn_ge').hide();
		return false;
	})

	$('.btn-save').click(function(){
		kode_obat = $(this).parent().parent().find('.kode_obat').val();
		jml_awal = $(this).parent().parent().find('.jml_awal').val();
		jml_beri_new = $(this).parent().parent().find('.jml_beri_new').val();
		no_batch = $(this).parent().parent().find('.no_batch').val();
		batch_awal = $(this).parent().parent().find('.batch_awal').val();
		id_stok = $(this).parent().parent().find('.id_stok_hidden').val();
		id_dd = $(this).parent().parent().find('.id_dd').val();
		id_dl = $(this).parent().parent().find('.id_dl').val();//id_detail_lplpo_kel

		datanew = "dt[kode_obat]="+kode_obat+"&dt[jml_awal]="+jml_awal+
			"&dt[jml_beri_new]="+jml_beri_new+"&dt[no_batch]="+no_batch+
			"&dt[id_stok]="+id_stok+"&dt[id_dd]="+id_dd+"&dt[batch_awal]="+batch_awal+"&dt[id_dl]="+id_dl;

		url='manajemenlogistik/detail_distribusi/update_lplpodist';

		$.post(url,datanew,function(result){			
			var response = jQuery.parseJSON(result); 
			// console.log(result.status);
			if(response.status == '1'){
				reloadTable();	
			}
			alert(response.message);
		})
		//$(this).parent().parent().find('.editable').hide();
		//$(this).parent().parent().find('.show_t').show();
		//$('.btn_ge').show();
		//return false;
	})
})
</script>
<div id="pagelistdetail_permintaan2">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped">
		<thead>
		<tr>
			<th>No.</th>
			<th>Obat Permintaan</th>
			<th>Sediaan</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Obat Pemberian</th>
			<th>No. Batch</th>
			<th>Pemberian</th>
			<th></th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=0;$j=1;
				foreach ($result as $rows) {
					echo '<tr style=""><td>'.$j.'</td>
					<td>'.$rows->nama_obat.' '.$rows->kekuatan.'</td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok_opt.'</td>
					<td>'.$rows->permintaan.'</td>
					<td><input type="text" class="editable obat_pemberian" value="'.$rows->obat_res.'" size="25"><div class="show_t">'.$rows->obat_res.'</div>
						<input type="hidden" class="kode_obat" value="'.$rows->kode_obat_res.'">
						<input type="hidden" class="id_stok_hidden">
						<input type="hidden" class="id_dd" value="'.$rows->id.'"></td>
					<td><input type="text" class="editable no_batch" value="'.$rows->no_batch.'" size="6" readonly><div class="show_t">'.$rows->no_batch.'</div>
						<input type="hidden" class="batch_awal" value="'.$rows->no_batch.'"></td>
					<td><input type="text" class="editable jml_beri_new" value="'.$rows->pemberian.'" size="3"><div class="show_t">'.$rows->pemberian.'</div>
						<input type="hidden" class="jml_awal" value="'.$rows->pemberian.'"></td>
					<td><button class="btn_edit show_t btn_ge"><span class="glyphicon glyphicon-pencil"></span></button>
						<button class="btn-save editable btn-success"><span class="glyphicon glyphicon-ok"></span></button></td></tr>';
					$i++;$j++;
				}
			?>
		</tbody>
	</table>
	<button id="print_btn"><span class="glyphicon glyphicon-print"></span> Print SBBK</button>
</div>
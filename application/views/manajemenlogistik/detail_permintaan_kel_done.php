 <style>
	tr{
		padding: 8px;

	}
	td{
		padding: 8px;
	}
	#info_faktur{
		background-color: #99CCFF;
	}
	.progress {
	    margin-top: 30px;
	    width: 500px;
	}
</style>
<script>
function reloadTable(){
	var url="manajemenlogistik/distribusi_obat_kel/get_detail_permintaan_done/"+$("#hide_lplpo").val();	
	$('#list_detail_permintaan').load(url);		
}

$(document).ready(function(){
	var progress = setInterval(function () {
		    var $bar = $('.bar-stok.progress-bar');

		    if ($bar.width() >= 480) { //panjang progress bar
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5); //penambahan tiap satuan waktu
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 1234); //satuan waktu
			
	$('#back_to_').click(function(){
			var url_penerimaan='manajemenlogistik/distribusi_obat_kel';
			$('#konten').load(url_penerimaan);
		})
	
	var url="manajemenlogistik/distribusi_obat_kel/get_detail_permintaan_done/"+$("#hide_lplpo").val();
	//mengambil data dg key no_struk
	//alert(url);
	$('#list_detail_permintaan').load(url);	
	
	
});

</script>
<div class="panel panel-primary" id="halaman_detail_faktur">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Detail Permintaan </b><button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<!-- Informasi Detail Faktur -->
			<input type="hidden" name="hide_lplpo" id="hide_lplpo" value="<?php echo $lplpo['id'];?>">
			<div class="alert alert-info" role="alert">
				<?php
					$date = strtotime($lplpo['periode']);
					$date_report = date('F Y',strtotime("-1 month",$date));
					//$date_request = date('F Y',strtotime("next month", $date));
					$date_request = date('F Y',$date);					
				?>
				<b>Kecamatan/Puskesmas: </b><?= $lplpo['nama_kec'] ?>/<?= $lplpo['nama_pusk'] ?><br>
				<b>Periode Pelaporan/Permintaan: </b><?= $date_report ?>/<?= $date_request ?>			
			</div>
			<hr>
			<!-- end informasi detail faktur -->
			
			<div id="list_detail_permintaan">
				<div id="" class="progress">
				  <div class="bar-stok progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">
				    
				  </div>
				</div>
			</div>
	</div>
</div>
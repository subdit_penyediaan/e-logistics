<style>
	.datepicker{
		z-index:1151;
	}
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;
	}
</style>
<script type="text/javascript">
$(function(){
	$('#cha_nama_inn').autocomplete({
			open: function(){
		        setTimeout(function () {
		            $('.ui-autocomplete').css('z-index', 99999999999999);
		        }, 0);
		    },
				source:'manajemenlogistik/detail_faktur/get_list_generik', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#cha_nama_inn" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#kode_inn_u').val(ui.item.id_generik);
					
					return false;
				}
			})


 		$('#cha_oi').autocomplete({
 			open: function(){
		        setTimeout(function () {
		            $('.ui-autocomplete').css('z-index', 99999999999999);
		        }, 0);
		    },
			source:'manajemenlogistik/detail_faktur/get_list_indikator', 
			minLength:0,
			focus: function( event, ui ) {
		        $( "#cha_oi" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{					
				$('#kode_ind').val(ui.item.id_indikator);
				
				return false;
			}
		})

		$('#cha_prog').autocomplete({
			open: function(){
		        setTimeout(function () {
		            $('.ui-autocomplete').css('z-index', 99999999999999);
		        }, 0);
		    },
			source:'manajemenlogistik/detail_faktur/get_list_program',
			minLength:0,
			focus: function( event, ui ) {
		        $( "#cha_prog" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{					
				$('#kode_prog').val(ui.item.id_program);
				
				return false;
			}
		})
  
	//end autocomplete fornas

	//autocomplete atc
	
 		$('#cha_atc').autocomplete({
 			open: function(){
		        setTimeout(function () {
		            $('.ui-autocomplete').css('z-index', 99999999999999);
		        }, 0);
		    },
				source:'manajemenlogistik/detail_faktur/get_list_atc', 
				minLength:0,
				focus: function( event, ui ) {
			        $( "#cha_atc" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#kode_atc_u').val(ui.item.kode_atc);
					
					return false;
				}
			})
	//end autocomplete atc

	$( "#cha_fornas" ).catcomplete({
		open: function(){
	        setTimeout(function () {
	            $('.ui-autocomplete').css('z-index', 99999999999999);
	        }, 0);
	    },
      delay: 0,
      source: 'manajemenlogistik/detail_faktur/get_list_fornas',
      focus: function( event, ui ) {
			        $( "#cha_fornas" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#id_fornas_u').val(ui.item.id_fornas);
					
					return false;
				}
	//		}
    });

	$('.labelinput22').hide();
	$('#btn_save_obat_master_u').click(function(e){
		var nama_atc=$('#cha_atc').val();
		var	nama_fornas=$('#cha_fornas').val();
		//var nama_ukp4=$('#nm_ukp4_input').val();
		var nama_indikator = $('#cha_oi').val();
		var nama_program = $('#cha_prog').val();
		//var nama_inn = $('#cha_inn').val();
		e.preventDefault();
		var update_kelompok='manajemenlogistik/detail_faktur/update_kelompok';
		var format_data={
			kode_obat:$('#cha_kode_obat').val(),			
			id_fornas:$('#id_fornas_u').val(),
			//id_ukp4:$('#id_ukp4').val(),
			kode_atc:$('#kode_atc_u').val(),
			kode_program:$('#kode_prog').val(),
			kode_indikator:$('#kode_ind').val(),
			kode_inn:$('#kode_inn_u').val(),
		}

		$.ajax({
			url:update_kelompok,
			data:format_data,
			type:"POST",
			success:function(e){
				$('.labelinput22').hide();
				$('.labeltext22').show();
				$('#dtl_nm_atc').text(nama_atc);
				$('#dtl_nm_fornas').text(nama_fornas);
				//$('#nm_ukp4').text(nama_ukp4);
				$('#dtl_nm_ind').text(nama_indikator);
				$('#dtl_nm_prg').text(nama_program);
			}
		})
		
	})

	$('#btn_update_obat_master_u').click(function(e){
		e.preventDefault();
		$('.labelinput22').show();
		$('.labeltext22').hide();
	})
		//============== submit add form

	$("#btn_save_update").click(function(){
		var url2="manajemenlogistik/detail_faktur/update_data";
		var form_data = {
			no_faktur:$("#cha_no_faktur").val(),			
			no_batch:$("#cha_batch").val(),
			no_batch_ori:$("#cha_batch_ori").val(),
			tanggal:$("#cha_tanggal").val(),
			harga_beli:$("#cha_harga").val(),//kurang update inn
			kode_inn:$('#kode_inn_u').val(),
			jumlah_kec:$('#cha_jml').val(),			
			kategori:$('#cha_kategori_obat').val(),
			jumlah_rusak:$('#cha_jml_rusak').val(),
			satuan:$('#cha_satuan_kecil').val(),
			kode_obat:$('#cha_kode_obat').val(),
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("Data berhasil diupdate");
				$("#myModal").modal('toggle');
				//$('#your-modal-id').modal('hide');
				$('body').removeClass('modal-open');
				$('.modal-backdrop').remove();
				//var url_hasil="manajemenlogistik/detail_faktur_obat/get_list_detail_faktur"
				//$('#konten').reload();
				var url_hasil="manajemenlogistik/detail_faktur/get_data_detail_faktur/"+$("#id_nofaktur").val();
				$("#list_detail_faktur").load(url_hasil);//+"#list_sedian_obat");

			}
		});
	})

	//============== end submit add form

		$('#btn_cancel_update').on("click",function(){
			$('.edittext').hide();
			$('.labeltext2').show();
		})

		$('.edittext').hide();
		$('#btn_update').on("click",function(){
			$('.edittext').show();
			$('.labeltext2').hide();
		})

		$("#pagelistdetail_faktur").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_detail_faktur').load(url);
		});
		/*
		$("#trcontent tr").on("click",function(event){
			event.preventDefault();
			alert("trcontent clicked");
			return false;
		})*/

		$("#pagelistdetail_faktur tr").on("dblclick",function (event){
				
				
				var url="manajemenlogistik/detail_faktur/get_detail_obat_penerimaan/"		
				var testindex = $("#pagelistdetail_faktur tr").index(this);
				var form_data = { no_batch: $("#hidden_kdobat_"+testindex).val() };
				//var nilai = $("#hidden_kdobat_"+testindex).val();
				//alert(nilai);
				//$("#myModal").modal('show');
				
				$.ajax({
					type:"POST",
					url:url,
					data: form_data,
					dataType:'json',
					success:function(data){
						//$('#').text(data.kode_obat);
						$('#dtl_nama_sediaan').text(data.nama_obj);
						$('#dtl_kategori').text(data.kategori);
						$('#cha_kategori_obat option:selected').text(data.kategori).attr('value', data.kategori);
						$('#cha_satuan_kecil option:selected').text(data.sat_unit).attr('value', data.sat_unit);
						$('#dtl_jum_bes').text(data.jum_besar);
						$('#dtl_kemasan').text(data.kemasan);
						$('#dtl_jum_kec').text(data.jum_kec);
						$('#cha_jml').val(data.jum_kec);
						$('#dtl_sat_unit').text(data.sat_unit);
						$('#dtl_no_batch').text(data.no_batch);
						$('#dtl_nama_inn').text(data.nama_objek);
						$('#cha_nama_inn').val(data.nama_objek);
						$('#cha_batch_ori').val(data.no_batch);
						$('#cha_batch').val(data.id);
						$('#dtl_ed').text(data.ed);
						$('#cha_tanggal').val(data.ed);
						$('#dtl_harga').text(data.harga);
						$('#cha_harga').val(data.harga);
						$('#dtl_nm_fornas').text(data.nama_fornas);
						$('#dtl_nm_ind').text(data.nama_indikator);
						$('#dtl_nm_prg').text(data.nama_program);
						$('#id_fornas_u').val(data.id_fornas);
						$('#cha_fornas').val(data.nama_fornas);
						$('#dtl_nm_atc').text(data.nama_atc);
						$('#cha_atc').val(data.nama_atc);
						$('#cha_oi').val(data.nama_indikator);
						$('#cha_prog').val(data.nama_program);
						$('#kode_prog').val(data.id_program);
						$('#kode_ind').val(data.id_indikator);
						$('#kode_inn_u').val(data.id_hlp);
						$('#kode_atc_u').val(data.koatc);
						$('#dtl_nm_ukp4').text(data.nama_ukp4);
						$('#dtl_jum_rusak').text(data.jml_rusak);
						$('#cha_jml_rusak').val(data.jml_rusak);
						$('#cha_kode_obat').val(data.kode_obat);
						$('#cha_no_faktur').val(data.no_faktur);

						//$( "#dtpk_cha option:selected" ).text(data.dtpk).attr('value',data.dtpk);
						
						$("#myModal").modal('show');

					}
				});
				
				//end ajax
			});
			//end pagepuskesmas tr
	});
</script>
<div id="pagelistdetail_faktur">
<?php echo $links;//echo $total_rows; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-hover table-bordered">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>Kode Obat</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>Jumlah</th>
			<th>No. Batch</th>
			<th>Kadaluarsa</th>
			<th>Harga</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td class="btn_action"><input type="checkbox" name="chk[]" id="cek_del_detail_faktur" value="'.$rows->id.'" class="chk">
					<input type="hidden" value="'.$rows->id.'" id="hidden_kdobat_'.$i.'">
					</td>
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->nama_obat.' '.$rows->kekuatan.'</td>
					<td>'.$rows->descr.'</td>
					<td>'.$rows->jumlah_kec.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->expired.'</td>
					<td>'.$rows->harga.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Detail Barang Penerimaan</h4>
      </div>
      <div class="modal-body">
      	<table class="table">
			<tr class="active">
				<td width="20%">Nama Sediaan</td>
					<td width="" colspan="3"><label id="dtl_nama_sediaan"></label>
				</td>
				<!--td width="20%">No. Barcode</td>
				<td width="30%"><label id="dtl_barcode"></label></td-->
			</tr>
			<tr class="active">
				<td width="20%">Nama INN/FDA</td>
					<td width="" colspan="3"><label id="dtl_nama_inn" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_nama_inn" id="cha_nama_inn" size="100">
					<input type="hidden" id="kode_inn_u" name="kode_inn_u" value="">
				</td>				
			</tr>
			<!-- <tr class="active">
				<td colspan="4">
					<b>Kelompok Sediaan</b><br>
					<table>
						<tr class="infokategori"><td>Fornas</td>
							<td>: <label class="labeltext22" id="dtl_nm_fornas"></label>
								<input type="text" class="labelinput22" name="cha_fornas" id="cha_fornas" size="">
								<input type="hidden" id="id_fornas_u" name="id_fornas_u" value="">
							</td></tr>						
						<tr class="infokategori"><td>ATC</td>
							<td>: <label class="labeltext22" id="dtl_nm_atc"></label>
								<input type="text" class="labelinput22" name="cha_atc" id="cha_atc" size="">
								<input type="hidden" id="kode_atc_u" name="kode_atc_u" value="">
							</td></tr>
						<tr class="infokategori"><td>Obat Indikator</td>
							<td>: <label class="labeltext22" id="dtl_nm_ind"></label>
								<input type="text" class="labelinput22" name="cha_oi" id="cha_oi" size="">
								<input type="hidden" id="kode_ind" name="kode_ind" value="">											
							</td></tr>
						<tr class="infokategori"><td>Obat Program</td>
							<td>: <label class="labeltext22" id="dtl_nm_prg"></label>
								<input type="text" class="labelinput22" name="cha_prog" id="cha_prog" size="">
								<input type="hidden" id="kode_prog" name="kode_prog" value="">
							</td></tr>
					</table>
					<button class="labeltext22" id="btn_update_obat_master_u">Update</button><button class="labelinput22" id="btn_save_obat_master_u">Simpan</button>
				</td>
			</tr> -->
			<tr>
				<td>No Batch</td>
				<td><label id="dtl_no_batch" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_batch_ori" id="cha_batch_ori">
					<input type="hidden" class="" name="cha_batch" id="cha_batch">
					<input type="hidden" class="" name="cha_kode_obat" id="cha_kode_obat">
					<input type="hidden" class="" name="cha_no_faktur" id="cha_no_faktur">
				</td>
				<td>Kategori</td>
				<td><label id="dtl_kategori" class="labeltext2"></label>
					<select name="cha_kategori_obat" id="cha_kategori_obat" class="edittext">
						<option value="">---- Pilih ----</option>
						<option value="Generik">Generik</option>
						<option value="Generik Bermerek">Generik Bermerek</option>
						<option value="Paten">Paten</option>
					</select>
				</td>
			</tr>
			<tr>
				
				<td>Jumlah Satuan Kecil</td>
				<td><label id="dtl_jum_kec" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_jml" id="cha_jml" size="4">
					<label id="dtl_sat_unit" class="labeltext2"></label>
					<select name="cha_satuan_kecil" id="cha_satuan_kecil" class="edittext">
						<option value="">-------- Pilih --------</option>
						<?php for($i=0;$i<sizeof($satuan);$i++) :?>
							<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
								<?php echo $satuan[$i]['satuan_obat']; ?>
							</option>
						<?php endfor; ?>
					</select>
				</td>
				<td>Jumlah rusak</td>
				<td><label id="dtl_jum_rusak" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_jml_rusak" id="cha_jml_rusak" ></td>
			</tr>
			<tr>
				<td>Tanggal Kadaluarsa</td>
				<td><label id="dtl_ed" class="labeltext2"></label>
					<div class="input-append date edittext" id="datepicker612" data-date="2014-04-14" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" id="cha_tanggal"readonly="readonly">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

						<script>
						$('#datepicker612').datepicker();
						</script>
					</td>
				<td>Harga Beli</td>
				<td>Rp <label id="dtl_harga" class="labeltext2"></label>
					<input type="text" class="edittext" name="cha_harga" id="cha_harga"></td>
			</tr>
			
		</table>
      </div>
      <div class="modal-footer">
        <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
        <button type="submit" class="btn btn-primary labeltext2" id="btn_update">Ubah</button>
        <button type="submit" class="btn btn-primary edittext" id="btn_save_update">Simpan</button>
        <button type="submit" class="btn btn-primary edittext" id="btn_cancel_update">Batal</button>
      </div>
  	
    </div>
  </div>
</div>
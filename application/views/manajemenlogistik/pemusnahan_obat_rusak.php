 <style>
	
</style>
<script>
$(document).ready(function(){
	$(".date").datepicker();
	$('#frmInputpemusnahan2').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                alert('DATA BERHASIL DISIMPAN');
	                var url='manajemenlogistik/pemusnahan_obat';
		            $('#konten').load(url);	
	            }
			})
			
		});
		//end form submit
	$('#btn_musnah2').hide();

	//=========== show add form
	$('#form_pemusnahan_obat_modal2').hide();
	$("#add_pemusnahan_obat_btn2").on("click",function (event){
			//$("#add_pemusnahan_obat_modal").modal('show');
			$("#form_pemusnahan_obat_modal2").slideDown("slow");
			$("#partbutton2").fadeOut();
			
		});
	$("#batal2").on("click",function (event){
			
			$("#form_pemusnahan_obat_modal2").slideUp("slow");
			$("#partbutton2").fadeIn();
				
		});
	//================ end show add form

	var url="manajemenlogistik/pemusnahan_obat/get_data_pemusnahan_obat";
	//$('#list_pemusnahan_obat').load(url);	
	//============== submit add form

	$("#btn_pemusnahan_obat_rusak").click(function(){

		var url2="manajemenlogistik/pemusnahan_obat/input_data";
		var form_data = {
			cur_date:$("#cur_date2").val(),
			no_dok:$("#no_dok2").val(),
			saksi1:$("#saksi1b").val(),
			nip1:$("#nip1b").val(),
			saksi2:$("#saksi2b").val(),
			nip2:$("#nip2b").val(),
			
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			dataType: "json",
			success:function(response){
				$('#alert-custom-rusak').addClass('alert-'+response.status);
				if (response.status == 'success') {
					$("#form_pemusnahan_obat_modal2").slideUp("slow");

					$("#btn_musnah2").fadeIn();				

					$("#cur_date2").val("");
					$("#no_dok2").val("");
					$("#saksi1b").val("");
					$("#nip1b").val("");
					$("#saksi2b").val("");
					$("#nip2b").val("");
					
					
				}
				$("#alert-title-rusak").text(response.status);
				$("#message-rusak").text(response.message);
				$('#alert-custom-rusak').show();
			}
		});
	})

	//============== end submit add form
});
</script>
	<!-- bag. isi -->
		<br>
<div id="partbutton2">
	<div class="col-lg-8">
		<button id="add_pemusnahan_obat_btn2" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Buat Berita Acara Pemusnahan</button>
		<!--button id="del_pemusnahan_obat_btn2"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
		<button id="cha_pemusnahan_obat-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
	</div>
	<div class="col-lg-4">
		<div class="input-group" style="float:right;">
	      <input type="text" class="form-control">
	      <span class="input-group-btn">
	        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
	      </span>
	    </div><!-- /input-group -->
	</div><!-- /col6 -->
</div>
	<div class="" id="form_pemusnahan_obat_modal2">
	  <h4>Buat Berita Acara Pemusnahan</h4>
		<table class="table">
			<tr>
				<td width="10%">Tanggal</td>
				<td>
					<div class="input-group input-append date" id="" data-date="<?= date('Y');?>-01-01" data-date-format="yyyy-mm-dd" >
						<input class="form-control span2" size="12" type="text" value="<?= date('Y');?>-01-01" id="cur_date2" name="cur_date2" readonly="readonly" >
						<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</td>
				<td width="10%">Nomor Dokumen</td>
				<td><input type="text" class="form-control" name="no_dok2" id="no_dok2"></td>
			</tr>
			<tr>
				<td>Saksi 1</td>
				<td><input type="text" class="form-control" name="saksi1b" id="saksi1b"></td>
				<td>NIP</td>
				<td><input type="text" class="form-control" name="nip1b" id="nip1b"></td>
				
			</tr>
			<tr>
				<td>Saksi 2</td>
				<td><input tbype="text" class="form-control" name="saksi2" id="saksi2"></td>
				<td>NIP</td>
				<td><input type="text" class="form-control" name="nip2b" id="nip2b"></td>
			</tr>
			<tr>
				<td colspan="4">
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_pemusnahan_obat_rusak" class="btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<button id="batal2" class="btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
					</div>
				</td>				
			</tr>
		</table>
	</div>
<!--/form-->
	<div id="list_pemusnahan_obat">
	<form method="POST" name="frmInputpemusnahan2" style="" id="frmInputpemusnahan2" action="<?php echo $base_url; ?>index.php/manajemenlogistik/pemusnahan_obat/update_detail_pemusnahan">
		<div class="alert alert-custom" role="alert" id="alert-custom-rusak" style="display: none;">
			<h4 id="alert-title-rusak"></h4>
			<p id="message-rusak"></p>
		</div>
		<hr>
		<table class="table table-striped table-bordered">
			<thead>
			<tr class="active">
				<th><span class="glyphicon glyphicon-trash"></span></th>		
				<th>No. Faktur</th>
				<th>Kode Obat</th>
				<th>Nama Obat</th>
				<th>Jumlah</th>
				<th>No. batch</th>
			</tr>
			</thead>
			<tbody id="trcontent">
				<?php
					//$i=1;
					foreach ($result as $rows) {			
						echo '<tr class="danger">
						<td><input type="checkbox" name="chk[]" id="cek_del_obat" value="'.$rows->id.'" class="chk"></td>
						<td>'.$rows->nomor_faktur.'
							<input type="hidden" name="kode_obat[]" value="'.$rows->kode_obat.'">
							<input type="hidden" name="no_faktur[]" value="'.$rows->nomor_faktur.'">
							<input type="hidden" name="no_batch[]" value="'.$rows->no_batch.'">
							<input type="hidden" name="id_dp[]" value="'.$rows->id.'">
							<input type="hidden" name="id_stok[]" value="'.$rows->id_stok.'">
						</td>
						<td>'.$rows->kode_obat.'</td>
						<td>'.$rows->nama_obat.'</td>
						<td>'.$rows->jumlah.'
						<input type="hidden" name="jml_rusak[]" value="'.$rows->jumlah.'">
						</td>
						<td>'.$rows->no_batch.'</td></tr>';
						//$i++;
					}
				?>
			</tbody>
		</table>
		<button type="submit" id="btn_musnah2" class="btn-danger">HAPUS</button>
	</form>
	</div>
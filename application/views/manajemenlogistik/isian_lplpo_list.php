<script type="text/javascript">
	$(function(){

		$( "#nama_obat-x").autocomplete({
				source:'manajemenlogistik/distribusi_obat/get_list_obat', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat-x" ).val( ui.item.value +" "+ ui.item.pwr);
			        return false;
			      },
				select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#kode_obat_'+index).val(ui.item.kode_obat);
					$('#no_batch_'+index).val(ui.item.batch);
					$('#id_stok_'+index).val(ui.item.id_stok);
					
					return false;
				}
			})
		
			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +" "+item.pwr + "<br><small>Stok: <i>" + item.stok + "<i></small></a>")
		        .appendTo( ul );
		    };

		$("#pagelistlplpo_list").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_lplpo_list').load(url);
		});

		$('#form_detail_lplpo').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            success: function (data) {
	                alert('DATA BERHASIL DIUPDATE');
	                var url="manajemenlogistik/isian_lplpo/get_data_isian_lplpo/"+$('#hidden_id_lplpo_0').val();
					$('#list_isian_lplpo').load(url);	            }
			})
			
		});

		$('.loop_respon').each(function(index){
			$('#pemberian_'+index).focus(function(){
				//alert(index);
				$("#add_give_modal").modal('show');
			})
		})
	});

	seq=0;
	function hitung_sisa_stok2cX(val){
		//fungsi di pemakaian
		//seq++;
		nilai1 = parseInt(val);
		nilai2 = parseInt($('#sedia_'+seq).val());
		//nilai4 = parseInt($('#rusak_ed_'+seq).val());

		nilai3 = nilai2 - nilai1;// - nilai4;
		$('#sisa_stok_'+seq).val(nilai3);	
		//alert("cek");
		seq++;
	}

	k=0;
	function hitung_sisa_stok3X(val){
		//fungsi di rusak_ed
		//k++;
		nilai1 = parseInt(val);
		nilai2 = parseInt($('#sedia_'+k).val());
		nilai4 = parseInt($('#pemakaian_'+k).val());

		nilai3 = nilai2 - nilai1 - nilai4;
		$('#sisa_stok_'+k).val(nilai3);	
		//alert("cek");
		k++;
	}

	j=0;
	function hitung_stok_optimumX(val){
		//fungsi dipemakain
		//j++;
		nilai1=val*3;
		$('#stok_opt_'+j).val(nilai1);
		j++;
	}
</script>
<div id="pagelistlplpo_list">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/isian_lplpo/update_data" method="post" id="form_detail_lplpo">
	<table class="table table-striped">
		<thead>
		<tr>
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Kode Obat</th>
			<th>Nama Obat</th>
			<th>Satuan</th>
			<th>Stok Awal</th>
			<th>Penerimaan</th>
			<th>Persediaan</th>
			<th>Pemakaian</th>
			<th>Rusak/ED</th>
			<th>Sisa Stok</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Pemberian</th>
			<th>Keterangan</th>
		</tr>
		</thead>
		<tbody id="trcontent">

			<?php
				$i=0;

				foreach ($result as $rows) {
					//$stok_optimum=3*$rows->stok_opt
					$j=$i+1;
					echo '<tr style="">
					<td><input type="checkbox" name="chk[]" id="cek_del_lplpo_list" value="'.$rows->id.'" class="chk"></td>
					<td>'.$j.'.</td>
					<td>'.$rows->kode_obat.'
						<input type="hidden" id="hidden_id_lplpo_'.$i.'" name="hidden_id_lplpo[]" value="'.$rows->id_lplpo.'">
						<input type="hidden" id="hidden_kode_obat_'.$i.'" name="hidden_kode_obat[]" value="'.$rows->kode_obat.'"></td>
					<td>'.$rows->nama_obat.' '.$rows->kekuatan.'</td>
					<td>'.$rows->sediaan.'</td>
					<td><input type="text" id="stok_awal_'.$i.'" style="border:none;" size="4" value="'.$rows->stok_awal.'" name="stok_awal[]"></td>
					<td><input type="text" id="terima_'.$i.'" style="border:none;" size="4" value="'.$rows->terima.'" name="penerimaan[]"></td>
					<td><input type="text" id="sedia_'.$i.'" style="border:none;" size="4" name="persediaan[]" value="'.$rows->sedia.'"></td>
					<td><input type="text" id="pemakaian_'.$i.'" size="4" name="pemakaian[]" value="'.$rows->pakai.'" onchange="hitung_stok_optimum(this.value);"></td>
					<td><input type="text" id="rusak_ed_'.$i.'"size="4" name="rusak_ed[]" value="0" onchange="hitung_sisa_stok3(this.value);"></td>
					<td><input type="text" id="sisa_stok_'.$i.'" style="border:none;" name="sisa_stok[]"size="4" value="'.$rows->stok_akhir.'"></td>
					<td><input type="text" id="stok_opt_'.$i.'" style="border:none;" name="stok_opt[]"size="4" value="'.$rows->stok_opt.'"></td>
					<td><input type="text" id="permintaan_'.$i.'" name="permintaan[]" size="4" value="'.$rows->permintaan.'"></td>
					<td><input type="text" id="pemberian_'.$i.'" class="loop_respon" name="pemberian[]" size="4" value="'.$rows->pemberian.'"></td>			
					<td><input type="text" id="ket_'.$i.'" name="ket[]" size="4" value="'.$rows->ket.'"></td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
	<button><span class="glyphicon glyphicon-magnet"></span> Update LPLPO</button>
	</form>
</div>

<div class="modal fade" id="add_give_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Pemberian Obat</h4>
      </div>
      <div class="modal-body">

<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
	
		<table class="table">
			<tr>
				<th>Nama Obat</th>
				<th>No. Batch</th>
				<th>Jumlah Pemberian</th>
			</tr>
			<tr>
				<td><input type="text" name="nama_obat-x" id="nama_obat-x" size="50" class=""/></td>
				<td><input type="text" name="no_batch" id="no_batch" size="10" class=""/></td>
				<td><input type="text" name="jml" id="jml" size="5" class=""/></td>
			</tr>
			<tr>				
				<td colspan="3">
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_gol_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>
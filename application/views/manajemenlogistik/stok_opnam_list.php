<style type="text/css">
	.alert {
		display: none;
	}
</style>
<script type="text/javascript">
$(function(){
	$('#ed-tgl-trans').datepicker({
		autoclose: true,
	    format: "yyyy-mm-dd",
	});

	$('#ed-periode').datepicker({
		autoclose: true,
	    format: "yyyy-mm",
	    viewMode: "months", 
	    minViewMode: "months",	    
	});

	$('.table-content').DataTable();
	
	$("#trcontent").on("click",".btn-detail",function (event){
		event.preventDefault();
		var url = $(this).attr("href");
		$('#konten').load(url);
	});

	$("#trcontent").on("click", ".btn-change", function (event){
		event.preventDefault();
		var url = $(this).attr("href");
		$.ajax({
			url: url,
			method: "GET",
			dataType: "json",
			success: function (response) {
				$("#st-no").val(response.no_stok_opnam);
				$("#st-id").val(response.id);
				$("#st-user").val(response.nama_user);
				$("#ed-tgl-trans").val(response.tgl_trans);
				$("#ed-periode").val(response.periode_stok_opnam);
				$("#ed-note").val(response.cttn);
			}
		});
		$("#st-modal-update").modal('show');
	});

	$("#st-form-edit").on("submit", function (e) {
		e.preventDefault();
		$.ajax({
			url: $(this).attr("action"),
			method: "POST",
			dataType: "json",
			data: $(this).serialize(),
			success: function (response) {
				$(".alert").addClass("alert-"+response.status);
				$(".message-alert").text(response.message);
				$(".alert").show();
				if (response.status == 'success') {
					setTimeout(function(){ 
						$('#st-modal-update').modal('hide');
						$('body').removeClass('modal-open');
						$('.modal-backdrop').remove();
					}, 3000);
					reloadTable();
				}
			}
		})
	})
});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;
	}
	.datepicker{
		z-index:1151;
	}
</style>
<?php // echo $links; ?>
<table class="table table-striped table-bordered table-content">
	<thead>
	<tr class="active">
		<th width="3%"><span class="glyphicon glyphicon-trash"></span></th>
		<th>ID</th>
		<th>Tanggal</th>
		<th>Nama User</th>
		<th>Bulan/Periode</th>
		<th>Catatan</th>
		<th width="15%">Detail</th>
	</tr>
	</thead>
	<tbody id="trcontent">
		<?php foreach ($result as $rows) :?>
			<tr style="">
				<td class="btn_action"><input type="checkbox" name="chk[]" id="cek_del_penerimaan_obat" value="<?= $rows->id ?>" class="chk"></td>
				<td><?= $rows->no_stok_opnam ?></td>
				<td><?= $rows->tgl_trans ?></td>
				<td><?= $rows->nama_user ?></td>
				<td><?= rtrim($rows->periode_stok_opnam,"-00") ?></td>
				<td><?= $rows->cttn ?></td>
				<td class="btn_action">
					<button class="btn-detail" href="<?= $base_url ?>manajemenlogistik/detail_stok_opnam/get_detail/<?= $rows->id ?>" id="" title="lihat"><span class="glyphicon glyphicon-list"></span></button>
					<button class="btn-change btn-warning" href="<?= $base_url ?>manajemenlogistik/stok_opnam/getdata/<?= $rows->id ?>" title="ubah"><span class="glyphicon glyphicon-pencil"></span></button>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div class="modal fade" id="st-modal-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Form Edit</h4>
		    </div>
		    <form class="" id="st-form-edit" action="<?= base_url();?>manajemenlogistik/stok_opnam/update">
		    <div class="modal-body">
		    	<div class="alert">
		    		<p class="message-alert"></p>
		    	</div>
				<table class="table">
					<tr>
						<td>No. Stok Opname</td>
						<td>
							<input type="text" class="form-control" name="no_stok_opnam" id="st-no">
							<input type="hidden" class="" name="id" id="st-id">
						</td>
						<td>Nama Petugas</td>
						<td><input type="text" class="form-control" name="nama_user" id="st-user"></td>
					</tr>
					<tr>
						<td>Tanggal Pelaksanaan</td>
						<td>
							<div class="input-group input-append date" id="" data-date="<?= date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2 trans-date" size="12" type="text" value="<?= date('Y-m-d');?>" id="ed-tgl-trans" name="tgl_trans">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</td>
						<td>Bulan/Periode</td>
						<td>
							<div class="input-group input-append date" id="" data-date="<?= date('Y-m-d');?>" data-date-format="yyyy-mm" >
								<input class="form-control span2 periode" size="12" type="text" value="<?= date('Y-m');?>" id="ed-periode" name="per_stok_opnam">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>	
						</td>
					</tr>
					<tr>
						<td>Catatan</td>
						<td><input type="text" class="form-control" name="cttn" id="ed-note"></td>
					</tr>					
				</table>				
			</div>
			<div class="modal-footer">
				<button type="submit" name="" id="" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
				<button type="button" id="" class="btn btn-warning btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Batal</button>
			</div>
			</form>
		</div>
	</div>
</div>
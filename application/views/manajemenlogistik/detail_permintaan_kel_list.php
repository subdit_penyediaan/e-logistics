<style>
.ui-autocomplete-loading {
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}
</style>
<script type="text/javascript">
function remBtn(i){
	//alert(i);
	$('#link_rem_obat_'+i).remove();
	$('#kode_obat_req_'+i).remove();
	$('#kode_obat_'+i).remove();
	$('#no_batch_'+i).remove();
	$('#id_stok_'+i).remove();
	$('#jml_stok_'+i).remove();
	$("#nama_obat_"+i ).remove();
	$('#no_batch_'+i).remove();
	$('#jml_pemberian_'+i).remove();
}

function reloadTable(){
	var url_done='manajemenlogistik/distribusi_obat_kel/get_detail_permintaan_done/'+$('.id_lplpo').val();
	$('#pagelistdetail_permintaan').load(url_done);
}
	$(document).ready(function(){
		$('#datepicker612').datepicker();
		//tambah obat jenis lain
	    $('.add_loops').each(function(index){
	    	qty = 0;
	    	i=30*index;
	    	$('#link_add_obat_'+index).click(function(){
	    		qty++;
	    		i++;
	    		var inputKodeObat =
	    		$('<input type="hidden"/>')
	    			.attr('name', 'kode_obat[]')
	    			.attr('class', 'addDetail_'+index)
	    			.attr('id', 'kode_obat_'+i);

	    		var inputKodeObatReq =
	    		$('<input type="hidden"/>')
	    			.attr('name', 'kode_obat_req[]')
	    			.attr('class', 'addDetail_'+index)
	    			.attr('id', 'kode_obat_req_'+i);

	    		var inputIdStok =
	    		$('<input type="hidden"/>')
	    			.attr('name', 'id_stok[]')
	    			.attr('class', 'addDetail_'+index)
	    			.attr('id', 'id_stok_'+i);

	    		var inputJmlStok =
	    		$('<input type="hidden"/>')
	    			.attr('name', 'jml_stok[]')
	    			.attr('class', 'addDetail_'+index)
	    			//.attr('size', '5');
	    			.attr('id', 'jml_stok_'+i);

	    		var inputGive =
	    		$('<input type="text"/>')
	    			.attr('name', 'jml_pemberian[]')
	    			.attr('class', 'addDetail_'+index)
	    			.attr('size', '5')
	    			.attr('id', 'jml_pemberian_'+i);

	    		var inputBatch =
	    		$('<input type="text"/>')
	    			.attr('name', 'no_batch[]')
	    			.attr('class', 'addDetail_'+index)
	    			.attr('readonly', 'readonly')
	    			.attr('id', 'no_batch_'+i);

	    		var inputText =
	    		$('<input type="text"/>')
	    			.attr('name', 'nama_obat[]')
	    			.attr('size', '45')
	    			.attr('id', 'nama_obat_'+i)
	    			.attr('class','looptext addDetail_'+index);

	    		var btnClose =
	    		'<button type="button" id="link_rem_obat_'+i+'" class="rem_add btn-danger" onclick="remBtn('+i+')"><b>x</b></button>';

	    		$('#add_other_drugs_'+index).append(inputText).append(btnClose).append(inputKodeObat).append(inputIdStok).append(inputJmlStok);
	    		$('#add_other_batch_'+index).append(inputBatch);
	    		$('#add_other_give_'+index).append(inputGive);
	    		$('#add_other_drugs_req_'+index).append(inputKodeObatReq);
	    		//inputText.autocomplete({
	    		$('#nama_obat_'+i).autocomplete({
						source:'manajemenlogistik/distribusi_obat_kel/get_list_obat',
						minLength:2,
						focus: function( event, ui ) {
					        $( "#nama_obat_"+i ).val( ui.item.value +" "+ ui.item.pwr);
					        return false;
					      },
					    select:function(evt2, ui2)
						{
							//console.log('#kode_obat_'+i);
							$('#kode_obat_'+i).val(ui2.item.kode_obat);
							$('#no_batch_'+i).val(ui2.item.batch);
							$('#id_stok_'+i).val(ui2.item.id_stok);
							$('#jml_stok_'+i).val(ui2.item.stok);
							$( "#nama_obat_"+i ).val( ui2.item.value +" "+ ui2.item.pwr);
							return false;
						}
					  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
					      return $( "<li>" )
					      	.data( "ui-autocomplete-item", item )
					        .append("<a>"+ item.value +"<br><small><b>Stok/Expired Date/No. Batch: </b>" + item.stok + "/"+ item.ed + "/"+ item.batch +"("+ item.dana +" "+ item.tahun_anggaran +")</small></a>")
					        .appendTo( ul );
					};
				return false;

	    	})
	    })

		//autocompale looping
		$('.looptext').each(function(index){
			/*$('#nama_obat_'+index).combogrid({
				  debug:true,
				  colModel: [
				  	//{'columnName':'id','width':'14','label':'id'},
				  	{'columnName':'nama_obj','width':'60','label':'Nama Obat'},
				  	{'columnName':'stok','width':'60','label':'Jumlah Stok'},
				  	{'columnName':'ed','width':'60','label':'Expired'},
				  	{'columnName':'dana','width':'40','label':'Sumber Dana'}],
				  url: 'manajemenlogistik/distribusi_obat_kel/get_list_obat_grid',
				  select: function( event, ui ) {
					  $('#kode_obat_'+index).val(ui.item.kode_obat);
						$('#no_batch_'+index).val(ui.item.batch);
						$('#id_stok_'+index).val(ui.item.id_stok);
						$('#jml_stok_'+index).val(ui.item.stok);

					  return false;
				  }
			  })
			*/

			$( "#nama_obat_"+index ).autocomplete({
				source:'manajemenlogistik/distribusi_obat_kel/get_list_obat',
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat_"+index ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{

					$('#kode_obat_'+index).val(ui.item.kode_obat);
					$('#no_batch_'+index).val(ui.item.batch);
					$('#id_stok_'+index).val(ui.item.id_stok);
					$('#jml_stok_'+index).val(ui.item.stok);
					return false;
				}
			})

			.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small><b>Stok/Expired Date/No. Batch: </b>" + item.stok + "/"+ item.ed + "/"+ item.batch +"("+ item.dana +" "+ item.tahun_anggaran +")</small></a>")
		        .appendTo( ul );
		    };//*/

		})

	//end autocomplete looping
	$('.looptext2').each(function(index){
		$('#no_barcode_'+index).autocomplete({
					source:'manajemenlogistik/distribusi_obat_kel/get_list_obat_bar',
					minLength:4,
					focus: function( event, ui ) {
				        $( "#nama_obat_"+index ).val( ui.item.value );
				        return false;
				      },
					select:function(evt, ui)
					{
						$('#kode_obat_'+index).val(ui.item.kode_obat);
						$('#no_batch_'+index).val(ui.item.batch);
						$('#id_stok_'+index).val(ui.item.id_stok);
						$('#jml_stok_'+index).val(ui.item.stok);
						return false;

					}
				})

	 		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small>Stok: <i>" + item.stok + "</i><br>Expired date: "+ item.ed +"<br>Sumber dana: "+ item.dana +"</small></a>")
		        .appendTo( ul );
		    };

	})
	//end autocomplete

	$('.btn-sug').each(function(index){
		$('#btn-sugx'+index).click(function(){
			event.preventDefault();
			var url=$(this).attr('href');
			alert(url);
			// $.get(url,function(data){
			// 	$('#kode_obat_'+index).val(ui.item.kode_obat);
			// 	$('#no_batch_'+index).val(ui.item.batch);
			// 	$('#id_stok_'+index).val(ui.item.id_stok);
			// 	$('#jml_stok_'+index).val(ui.item.stok);
			// })
		})
	})

		$("#pagelistdetail_permintaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			$('#list_detail_permintaan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});

		$('#form_detail_distribusi').validate({
			submitHandler:function(){
				$.ajax({
					type: $('#form_detail_distribusi').attr('method'),
		            url: $('#form_detail_distribusi').attr('action'),
		            data: $('#form_detail_distribusi').serialize(),
		            dataType:'json',
		            beforeSend: function(){
		                showBusySubmit();
		            },
		            success: function (data) {
		                $('#up-konten').unblock();
		                alert(data.confirm);
		                var url_done='manajemenlogistik/distribusi_obat_kel/get_detail_permintaan_done/'+data.id_lplpo;
		                $('#pagelistdetail_permintaan').load(url_done);
		            }
				})
				return false;
			}
		});
		//end form input
	});

	var seq=0;
	function getMax(val){
		seq++;
		//var obj=$()
		var nilai1=val;
		var nilai2=$('#jml_stok_'+seq).val();

		nilai3= val-nilai2;
		$('#selisih_'+seq).val(nilai3);
	}

	function getSuggest(val,i){
		event.preventDefault();
		var url = 'manajemenlogistik/distribusi_obat_kel/getSuggest/'+val;
		var jml_beri = $('#jml_minta_'+i).val();
		var jml_stok = $('#jml_stok_'+i).val();
		//alert(jml_beri);
		$.getJSON(url,function(data){
			//console.log(data[0].batch);
			if(data.length!=0){
				$("#nama_obat_"+i).val( data[0].value );
				$('#kode_obat_'+i).val(data[0].kode_obat);
				$('#no_batch_'+i).val(data[0].batch);
				$('#id_stok_'+i).val(data[0].id_stok);
				$('#jml_stok_'+i).val(data[0].stok);
				$('#jml_pemberian_'+i).val(jml_beri);
			}else{
				//alert("tidak ada obat yang tersedia (stok habis atau obat sudah kadaluarsa). Stok yang tersisa masih "+jml_stok);
				alert("maaf pencarian gagal, silakan langsung diketik dikolom obat");
			}
			//var data = jQuery.parseJSON(konten);
			//console.log(data.batch);

		})
	}

	function getRemove(i){
		event.preventDefault();

		$("#nama_obat_"+i).val('');
		$('#kode_obat_'+i).val('');
		$('#no_batch_'+i).val('');
		$('#id_stok_'+i).val('');
		$('#jml_stok_'+i).val('');
		$('#jml_pemberian_'+i).val('');
	}

	function showBusySubmit(){
        $('#up-konten').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }
</script>
<div id="pagelistdetail_permintaan">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/distribusi_obat_kel/input_data_distribusi" method="post" id="form_detail_distribusi" onkeypress="return event.keyCode != 13;">
    <div class="row">
        <div class="col-md-3">
            <label id="dtl_ed" class="labeltext2">Tanggal Pemberian</label>
            <div class="input-group input-append date edittext" id="datepicker612" data-date="<?= date('Y-m-d')?>" data-date-format="yyyy-mm-dd" >
              <input class="form-control span2 required" size="12" type="text" id="tanggal" name="tanggal_pemberian" readonly="readonly">
              <span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
            </div>
        </div>
        <div class="col-md-3">
            <label id="dtl_ed" class="labeltext2">No. Dokumen</label>
            <input class="form-control" size="12" type="text" id="no_dok" name="no_dok">
        </div>
    </div>
		<br>
		<table class="table table-striped table-bordered">
			<thead>
			<tr class="active">
				<th>No.</th>
				<th width="30%">Obat Permintaan</th>
				<th>Sediaan</th>
				<th>Sisa Stok</th>
				<th>Stok Optimum</th>
				<th>Jumlah Permintaan</th>
				<th>Jumlah Rekomendasi</th>
				<!--th>Barcode Obat Pemberian</th-->
				<th>Obat Pemberian</th>
				<th>No. Batch</th>
				<th>Pemberian</th>
				<th width="10%">Suggestion</th>
			</tr>
			</thead>
			<tbody id="trcontent">
				<?php
					$i=0;$j=1;
					foreach ($result as $rows): ?>
					<tr style=""><td><?= $j ?></td>
						<td><?= $rows->nama_obat ?>
							<input type="hidden" value="<?= $rows->kode_obat ?>" name="kode_obat_req[]" id="kode_obat_req_<?= $i ?>">
							<input type="hidden" value="<?= $rows->id_lplpo ?>" name="id_lplpo[]" class="id_lplpo">
							<div id="add_other_drugs_req_<?= $i ?>"></div></td>
						<td><?= $rows->sediaan ?></td>
						<td><?= $rows->stok_akhir ?></td>
						<td><?= $rows->stok_opt ?></td>
						<td><?= $rows->permintaan ?><input type="hidden" id="jml_minta_<?= $i ?>" value="<?= $rows->permintaan ?>"></td>
						<td>
							<?php
								$rekomendasi = $rows->stok_opt-$rows->stok_akhir;
								if($rekomendasi < 0)
									echo 0;
								else
									echo $rekomendasi;
							?>
						</td>
						<!--td><input type="text" size="10" name="barcode_obat[]" id="no_barcode_<?= $i ?>" class="looptext2"></td-->
						<td><input type="text" size="50" name="nama_obat[]" id="nama_obat_<?= $i ?>" class="looptext">
						 	<input type="hidden" name="kode_obat[]" id="kode_obat_<?= $i ?>">
						 	<input type="hidden" name="id_stok[]" id="id_stok_<?= $i ?>">
						 	<input type="hidden" name="jml_stok[]" id="jml_stok_<?= $i ?>">
						 	<div id="add_other_drugs_<?= $i ?>"></div>
						 	<small><a href="javascript:void(0)" id="link_add_obat_<?= $i ?>" class="add_loops"><i>Tambah Obat Lain</i></a></small>
						 </td>
						<td><input type="text" name="no_batch[]" id="no_batch_<?= $i ?>" readonly="readonly">
							<div id="add_other_batch_<?= $i ?>"></div></td>
						<td><input type="text" size="5" name="jml_pemberian[]" id="jml_pemberian_<?= $i ?>">
							<div id="add_other_give_<?= $i ?>"></div></td>
						<td style="padding: 5px ! important;">
							<button class="btn-success btn-group btn-group-xs" role="group" onclick="getSuggest('<?= $rows->kode_obat ?>',<?= $i ?>)"><span class="glyphicon glyphicon-refresh"></span></button>
							<button class="btn-danger btn-group btn-group-xs" role="group" onclick="getRemove(<?= $i ?>)"><span class="glyphicon glyphicon-arrow-left"></span></button>
						</td>
					</tr>

				<?php $i++;$j++; endforeach; ?>
			</tbody>
		</table>
		<button class="btn btn-success btn-lg"><span class="glyphicon glyphicon-floppy-saved"></span> Distribusi</button>
	</form>
</div>

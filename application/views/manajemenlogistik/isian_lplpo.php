 <style>
	
</style>
<script>
$(document).ready(function(){
$('#back_to_').click(function(){
			var url_penerimaan='manajemenlogistik/lplpo';
			$('#konten').load(url_penerimaan);
		})
$('#nama_obat').autocomplete({
				source:'manajemenlogistik/isian_lplpo/get_list_obat', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_obat" ).val( ui.item.value +" "+ ui.item.pwr +" "+ui.item.desc);
			        return false;
			      },
				select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
	
					$('#obat_hidden').val(ui.item.kode_obat);
					return false;
				}
			})
 		
 		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			      return $( "<li>" )
			        .append("<a>"+ item.value +" "+item.pwr + " " + item.desc+"<br><small>"+item.produsen+"</small></a>")
			        .appendTo( ul );
			    };
	//end autocomplete

	//=========== del button

	$("#del_isian_lplpo_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "manajemenlogistik/isian_lplpo/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="manajemenlogistik/isian_lplpo/get_data_isian_lplpo/"+$('#id_lplpo').val();
					$("#list_isian_lplpo").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_isian_lplpo_modal').hide();
	$("#add_isian_lplpo_btn").on("click",function (event){
			//$("#add_isian_lplpo_modal").modal('show');
			$("#form_isian_lplpo_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_isian_lplpo_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_isian_lplpo_modal").modal('show');
			$("#form_isian_lplpo_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_isian_lplpo_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="manajemenlogistik/isian_lplpo_obat/get_list_isian_lplpo";
	//$('#list_isian_lplpo_obat').load(url);

	var url="manajemenlogistik/isian_lplpo/get_data_isian_lplpo/"+$('#id_lplpo').val();
	$('#list_isian_lplpo').load(url);	
	
	//============== submit add form

	$("#btn_isian_lplpo").click(function(){
		var url2="manajemenlogistik/isian_lplpo/input_data";
		var form_data = {
			id_lplpo:$("#id_lplpo").val(),
			kode_obat:$("#obat_hidden").val(),
			terima:$("#penerimaan").val(),
			sedia:$("#persediaan").val(),
			pakai:$("#pemakaian").val(),
			stok_op:$("#stok_op").val(),
			minta:$("#permintaan").val(),
			ket:$("#ket").val(),
			dana:$("#dana").val(),
			stok_awal:$("#stok_awal").val(),
			rusak_ed:$("#rusak_ed").val(),
			nama_obj:$("#nama_obat").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#form_isian_lplpo_modal").slideUp("slow");
				//$("#form_isian_lplpo_modal").reset();
				$("#partbutton").fadeIn();
				//var url_hasil="manajemenlogistik/isian_lplpo_obat/get_list_isian_lplpo"
				var url_hasil="manajemenlogistik/isian_lplpo/get_data_isian_lplpo/"+$('#id_lplpo').val();
				$("#list_isian_lplpo").load(url_hasil);//+"#list_sedian_obat");

				$("#nama_obat").val("");
				$("#obat_hidden").val("");
				$("#penerimaan").val("");
				$("#persediaan").val("");
				$("#pemakaian").val("");
				$("#stok_op").val("");
				$("#permintaan").val("");
				$("#ket").val("");
				$("#dana").val("");
				//$("#nama_obat").val("");
				$('#sisa_stok').val("");
				$('#stok_awal').val("");
				$('#rusak_ed').val("");
			}
		});
	})

	//============== end submit add form
});

function hitung_persediaan(val){
	nilai1 = parseInt(val);
	nilai2 = parseInt($('#stok_awal').val());

	nilai3 = nilai1 + nilai2;
	$('#persediaan').val(nilai3);
}

function hitung_sisa_stok(val){
	nilai1 = parseInt(val);
	nilai2 = parseInt($('#persediaan').val());
	nilai4 = parseInt($('#pemakaian').val());

	nilai3 = nilai2 - nilai1 - nilai4;
	$('#sisa_stok').val(nilai3);	
}

function hitung_stok_optimum(val){
	nilai1=val*3;
	$('#stok_op').val(nilai1);
}
</script>
<div class="panel panel-primary" id="halaman_penerimaan_obat">
	<div class="panel-heading">LPLPO <button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
	<!-- bag. isi -->
	<table class="table">
		<tr>
			<td>Kecamatan</td>
			<td><?php echo $lplpo['nama_kec']; ?>
				<input type="hidden" value="<?php echo $lplpo['id']; ?>" id="id_lplpo" name="id_lplpo"></td>
			
		</tr>
		<tr>
			<td>Puskesmas</td>
			<td><?php echo $lplpo['nama_pusk']; ?></td>
		</tr>
		<tr>
			<td>Bulan/Periode</td>
			<td><?php echo $lplpo['periode']; ?></td>
		</tr>
	</table>
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_isian_lplpo_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_isian_lplpo_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<!--button id="cha_isian_lplpo-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</div><!-- /col6 -->
		</div>
			<div class="" id="form_isian_lplpo_modal">
			  <h4>Entri Data Obat</h4>
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				<table class="table">
						<tr>
							<td>Nama Obat</td>
							<td colspan="3"><input type="text" class="form-control" name="nama_obat" id="nama_obat">
								<input type="hidden" id="obat_hidden" name="obat_hidden" ></td>
							<td>Stok Awal</td>
							<td><input type="text" class="form-control" name="stok_awal" id="stok_awal"></td>
						</tr>
						<tr><td>Penerimaan</td>
							<td><input type="text" class="form-control" name="penerimaan" id="penerimaan" onchange="hitung_persediaan(this.value);"></td>
							<td>Persediaan</td>
							<td><input type="text" class="form-control" name="persediaan" id="persediaan"></td>
							<td>Pemakaian</td>
							<td><input type="text" class="form-control" name="pemakaian" id="pemakaian" onchange="hitung_stok_optimum(this.value);"></td>
						</tr>
						<tr><td>Rusak/ED</td>
							<td><input type="text" class="form-control" name="rusak_ed" id="rusak_ed"  onchange="hitung_sisa_stok(this.value);"></td>						
							<td>Sisa Stok</td>
							<td><input type="text" class="form-control" name="sisa_stok" id="sisa_stok"></td>
							<td>Stok Optimum</td>
							<td><input type="text" class="form-control" name="stok_op" id="stok_op"></td>							
						</tr>
						<tr><td>Permintaan</td>
							<td><input type="text" class="form-control" name="permintaan" id="permintaan"></td>
							<!--td>Pemberian</td>
							<td><input type="text" class="form-control" name="pemberian" id="pemberian"></td-->
							<td>Keterangan</td>
							<td colspan="3"><input type="text" class="form-control" name="ket" id="ket"></td>						
						</tr>
						<tr>
							<td colspan="6"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_isian_lplpo" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							
						</tr>
					</table>
			</div>
			<!--/form-->
			
			<br><br>

			<div id="list_isian_lplpo"></div>
	</div>
</div>
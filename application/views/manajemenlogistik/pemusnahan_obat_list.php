<script type="text/javascript">
	$(function(){
		$("#pagelistpemusnahan_obat").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_pemusnahan_obat').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistpemusnahan_obat">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<hr>
	<table class="table table-striped">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>		
			<th>Kode Obat</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>Stok</th>
			<th>Tanggal Kadaluarsa</th>
			<th>Lama</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {			
					echo '<tr class="danger">
					<td><input type="checkbox" name="chk[]" id="cek_del_detail_faktur" value="'.$rows->id.'" class="chk"></td>
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->nama_obat.'</td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok.'</td>
					<td>'.$rows->ed.'</td>
					<td>'.$rows->lama.'</td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
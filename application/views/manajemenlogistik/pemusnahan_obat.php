<style type="text/css">
.edit-text {
	display: none;
}
</style>
<script>
function resetForm() {
	$("#form-pemusnahan-obat-modal").slideUp("slow");
	$("#btn-musnah-1").fadeIn();
	$(".input-text").val("");
}

$(document).ready(function(){
	$(".date").datepicker();

	$('#btn_export').click(function(){
		var urlprint="manajemenlogistik/pemusnahan_obat/printOut";
		window.open(urlprint);
	})
	//form submit
	$('#frmInputpemusnahan1').submit(function(e){
		event.preventDefault();
		$.ajax({
			type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            //dataType: 'json',
            success: function (data) {
                alert('DATA BERHASIL DISIMPAN');
                var url='manajemenlogistik/pemusnahan_obat';
	            $('#konten').load(url);
            }
		})

	});
	//end form submit
	$('#btn-musnah-1').hide();
	var url_obat_rusak="manajemenlogistik/pemusnahan_obat/obat_rusak";
	$('#penerimaan_rusak').load(url_obat_rusak);

	var url_stok_rusak="manajemenlogistik/pemusnahan_obat/stok_opnam_rusak";
	$('#stok_opnam_rusak').load(url_stok_rusak);

	//=========== show add form
	$('#form-pemusnahan-obat-modal').hide();
	$("#add_pemusnahan_obat_btn").on("click",function (event){
		$("#form-pemusnahan-obat-modal").slideDown("slow");
		$("#partbutton").fadeOut();
	});
	$("#batal").on("click",function (event){
		event.preventDefault();
		$("#form-pemusnahan-obat-modal").slideUp("slow");
		$("#partbutton").fadeIn();
	});
	//================ end show add form

	//============== submit add form
	$("#btn-pemusnahan-obat").click(function(e){
		e.preventDefault();
		var url2="<?= base_url();?>manajemenlogistik/pemusnahan_obat/input_data";
		$.ajax({
			type:"POST",
			url:url2,
			data: $("#form-pemusnahan-obat-modal").serialize(),
			dataType: 'json',
			success:function(response){
				alert(response.message);
				if (response.status == "success") {
					resetForm();
					$("#id-tb-pemusnahan").val(response.id_tb_pemusnahan);
				}
			}
		});
	})
	//============== end submit add form

	$(".btn-edit").on("click", function(e) {
			e.preventDefault();
			$(this).parent().parent().find(".edit-text").show();
			$(this).parent().parent().find(".label-text").hide();
	})

	$(".btn-rasido").on("click", function(e) {
			e.preventDefault();
			$(this).parent().parent().find(".edit-text").hide();
			$(this).parent().parent().find(".label-text").show();
	})

	$(".btn-save").on("click", function(e) {
			e.preventDefault();
			var id = $(this).parent().parent().find('.hidden-id').val();
			var stok_id = $(this).parent().parent().find('.stok-id').val();
			var qty = $(this).parent().parent().find('.edit-text-qty').val();

			btn = $(this);

			$.ajax({
					url: "<?= base_url(); ?>manajemenlogistik/pemusnahan_obat/update_data_detail/"+id,
					type: "POST",
					data: "qty="+qty+"&stok_id="+stok_id,
					dataType: "json",
					success: function(response) {
						 alert(response.message);
						 if (response.status == "success") {
							 	$(".btn-rasido").click();
								btn.parent().parent().find("label").text(response.value);
						 }
					}
			})
	})
});
</script>
<div class="panel panel-primary" id="halaman_pemusnahan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Pemusnahan Obat</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#expired" data-toggle="tab">Sediaan Expired</a></li>
			<li><a href="#penerimaan_rusak" data-toggle="tab">Penerimaan Sediaan Rusak</a></li>
			<li><a href="#stok_opnam_rusak" data-toggle="tab">Stok Opnam</a></li>
			<li><a href="#list_obat_rusak" data-toggle="tab">List Sedian yang telah dimusnahkan</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="expired">
			<br>
				<div id="partbutton">
					<div class="col-lg-8">
						<button id="add_pemusnahan_obat_btn" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Buat Berita Acara Pemusnahan</button>
					</div>
					<div class="col-lg-4">
						<div class="input-group" style="float:right;">
					      <input type="text" class="form-control">
					      <span class="input-group-btn">
					        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
					      </span>
					    </div><!-- /input-group -->
					</div><!-- /col6 -->
				</div>
				<form class="" id="form-pemusnahan-obat-modal">
				  	<label><h4>Berita Acara</h4></label>
					<table class="table">
						<tr>
							<td width="10%">Tanggal</td>
							<td>
								<div class="input-group input-append date" id="" data-date="<?= date('Y');?>-01-01" data-date-format="yyyy-mm-dd" >
									<input class="form-control span2" size="12" type="text" value="<?= date('Y');?>-01-01" id="cur_date" name="cur_date" readonly="readonly" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>
							</td>
							<td width="10%">Nomor Dokumen</td>
							<td><input type="text" class="form-control input-text" name="no_dok" id="no_dok"></td>
						</tr>
						<tr>
							<td>Saksi 1</td>
							<td><input type="text" class="form-control input-text" name="saksi1" id="saksi1"></td>
							<td>NIP</td>
							<td><input type="text" class="form-control input-text" name="nip1" id="nip1"></td>
						</tr>
						<tr>
							<td>Saksi 2</td>
							<td><input type="text" class="form-control input-text" name="saksi2" id="saksi2"></td>
							<td>NIP</td>
							<td><input type="text" class="form-control input-text" name="nip2" id="nip2"></td>
						</tr>
						<tr>
							<td colspan="4">
								<button type="submit" class="btn btn-success btn-sm" name="Simpan" id="btn-pemusnahan-obat"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</td>
						</tr>
					</table>
				</form>
				<form method="POST" name="frmInputpemusnahan1" style="" id="frmInputpemusnahan1" action="<?php echo $base_url; ?>index.php/manajemenlogistik/pemusnahan_obat/input_detail_pemusnahan">
					<hr>
					<input type="hidden" name="id_tb_pemusnahan" id="id-tb-pemusnahan" value="">
					<div id="list_pemusnahan_obat">
						<table class="table table-striped table-bordered">
							<thead>
							<tr class="active">
								<th><span class="glyphicon glyphicon-trash"></span></th>
								<th>Kode Obat</th>
								<th>No.Batch</th>
								<th>Nama Obat</th>
								<th>Jumlah</th>
								<th>Tanggal Kadaluarsa</th>
								<th>Lama</th>
								<th>Sumber Dana</th>
							</tr>
							</thead>
							<tbody id="trcontent">
								<?php foreach ($result as $rows): ?>
									<tr class="danger">
										<td><input type="checkbox" name="chk[]" id="" value="<?= $rows->id ?>" class="chk"></td>
										<td><?= $rows->kode_obat ?>
											<input type="hidden" name="kode_obat[]" value="<?= $rows->kode_obat ?>">
											<input type="hidden" name="no_faktur[]" value="<?= $rows->no_faktur ?>">
											<input type="hidden" name="no_batch[]" value="<?= $rows->no_batch ?>">
											<input type="hidden" name="id_stok[]" value="<?= $rows->id ?>">
										</td>
										<td><?= $rows->no_batch ?></td>
										<td><?= $rows->nama_obat ?></td>
										<td><?= $rows->stok ?>
											<input type="hidden" name="jml_expired[]" value="<?= $rows->stok ?>">
										</td>
										<td><?= $rows->ed ?></td>
										<td><?= $rows->lama ?></td>
										<td><?= $rows->dana ?><?= $rows->tahun_anggaran ?></td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<div id="">
						<button type="submit" class="btn btn-danger" id="btn-musnah-1">HAPUS</button>
					</div>
				</form>
				</div><!-- tab obat expired-->
				<div class="tab-pane" id="penerimaan_rusak">
					Penerimaan Rusak
				</div>
				<div class="tab-pane" id="stok_opnam_rusak">
					Stok Opnam Rusak
				</div>
				<div class="tab-pane" id="list_obat_rusak">
					<br>
					<table class="table table-striped table-bordered">
						<thead>
						<tr class="active">
							<th>Tanggal</th>
							<th>Nomor Faktur</th>
							<th>Nomor Batch</th>
							<th>Nama Obat</th>
							<th>Jumlah</th>
							<th>Kerusakan</th>
							<th>Petugas</th>
							<th>Saksi I</th>
							<th>Saksi II</th>
							<th>Aksi</th>
						</tr>
						</thead>
						<tbody id="trcontent">
							<?php foreach ($result2 as $rows) :?>
								<?php
									if ($rows->jenis_rusak==1) {
										$keterangan="Penerimaan";
									} elseif($rows->jenis_rusak==2) {
										$keterangan="Stok Opnam";
									} elseif($rows->jenis_rusak==3) {
										$keterangan="Expired";
									}
								?>
								<tr>
									<td>
											<?= $rows->tanggal ?>
											<input type="hidden" class="hidden-id" value="<?= $rows->id ?>">
											<input type="hidden" class="stok-id" value="<?= $rows->id_stok ?>">
									</td>
									<td><?= $rows->nomor_faktur ?></td>
									<td><?= $rows->no_batch ?></td>
									<td><?= $rows->object_name ?></td>
									<td>
											<label class="label-text"><?= $rows->jumlah ?></label>
											<input type="text" class="edit-text edit-text-qty" value="<?= $rows->jumlah ?>">
									</td>
									<td><?= $keterangan ?></td>
									<td><?= $rows->long_name ?></td>
									<td><?= $rows->saksi1 ?></td>
									<td><?= $rows->saksi2 ?></td>
									<td>
											<button class="btn btn-warning btn-edit btn-xs label-text">edit</button>
											<button class="btn btn-success btn-save btn-xs edit-text">save</button>
											<button class="btn btn-default btn-rasido btn-xs edit-text">cancel</button>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<button class="btn btn-success" id="btn_export"><span class="glyphicon glyphicon-export"></span>Eksport</button>
				</div>
			</div><!-- tab content-->
	</div>
</div>

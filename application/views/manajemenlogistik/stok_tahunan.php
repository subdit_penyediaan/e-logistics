 <style>
 	.ui-autocomplete-loading { 
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}

	tr{
		padding: 8px;

	}
	td{
		padding: 8px;
	}
	#info_faktur{
		background-color: #99CCFF;
	}
	.infokategori{
		padding: 0px;
		margin: 0px;
	}
	.ui-autocomplete-category {
    font-weight: bold;
    padding: .2em .4em;
    margin: .8em 0 .2em;
    line-height: 1.5;
  }
</style>
<script>
	//tambahan autocomplete with category
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
          currentCategory = item.category;
        }
        that._renderItemData( ul, item );
      });
    }
  });
 </script>
<script>
//var produsen_id;
function resetInput(){
	$('.inputText').val('');
	$('#kategori_obat option:selected').text("---- Pilih ----").attr('value', '');
	$('#satuanbesar option:selected').text("-------- Pilih --------").attr('value', '');
	$('#satuankecil option:selected').text("-------- Pilih --------").attr('value', '');
	$('#nm_fornas').text('');				
	$('#nm_atc').text("");				
	$('#nm_ukp4').text("");
	produsen_id = '';
}

function reloadKonten(){
	var url="manajemenlogistik/stok_opnam/get_detail/<?php echo $nama_tabel;?>";
	//mengambil data dg key no_struk
	$('#konten').load(url);	
}
$(document).ready(function(){
	$('#reset').click(function(){
		event.preventDefault();
		//resetInput();
		reloadKonten();
	})
	
	$('#filter_produsen').autocomplete({
		source: 'manajemenlogistik/detail_faktur/get_produsen',
		minLength: 2,
		focus: function(event,ui){
			$('#filter_produsen').val(ui.item.value);
			return false;
		},select: function(e,ui){
			produsen_id = ui.item.id_produsen;
			//combogrid
			$('#nama_obat').combogrid({
				  debug:true,
				  colModel: [
				  	//{'columnName':'id','width':'14','label':'id'}, 
				  	{'columnName':'name','width':'60','label':'Nama Obat'},
				  	{'columnName':'author','width':'40','label':'Produsen'}],
				  url: 'manajemenlogistik/detail_faktur/get_list_obat_grid/'+produsen_id,
				  select: function( event, ui ) {
					  $( "#nama_obat" ).val( ui.item.name );
					  $( "#kode_generik" ).val( ui.item.id_generik );
					  $('#obat_hidden').val(ui.item.kode_obat);
							//tambahan baru
							var url_get_nm_fornas='manajemenlogistik/detail_faktur/cobagetfornas/'+ui.item.kode_obat;
							var url_get_nm_atc='manajemenlogistik/detail_faktur/getatc/'+ui.item.kode_obat;
							//var url_get_nm_ukp4='manajemenlogistik/detail_faktur/getukp4/'+ui.item.kode_obat;
							var url_get_nm_generik='manajemenlogistik/detail_faktur/getgenerik/'+ui.item.id_generik;

							$.getJSON(url_get_nm_fornas, function(data) {
									$('#nm_fornas_input').val(data.nama_fornas);
									$('#nm_fornas').text(data.nama_fornas);
									$('#id_fornas').val(data.id_fornas);
									}
								);
							$.getJSON(url_get_nm_atc, function(data) {
									
									$('#kode_atc').val(data.atc);
									$('#nm_atc').text(data.nama_atc_eng+'('+data.nama_atc+')');
									$('#nm_atc_input').val(data.nama_atc_eng+'('+data.nama_atc+')');
									}
								);
							$.getJSON(url_get_nm_generik, function(data) {
									
									$('#nama_generik').val(data.nama_objek);
									$('#nm_indikator').text(data.nama_indikator);
									$('#nm_indikator_input').val(data.nama_indikator);
									$('#kode_indikator').val(data.id_obatindikator);
									$('#nm_program').text(data.nama_program);
									$('#nm_program_input').val(data.nama_program);
									$('#kode_program').val(data.id_obatprogram);
									}
								);
							$( "#satuanbesar" ).val(ui.item.kemasan);
							$( "#satuankecil" ).val(ui.item.sat_jual);
							$( "#kategori_obat" ).val(ui.item.kategori);
							//produsen_id = '';
							//console.log(produsen_id);
					  return false;
				  }

			  });
 			produsen_id = '';
		}

	})
	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_detail_faktur").html(data);
	            }
			})
			
		});
	//autocomplete fornas
	
 		$('#nama_generik').autocomplete({
				source:'manajemenlogistik/detail_faktur/get_list_generik', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_generik" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#kode_generik').val(ui.item.id_generik);
					
					return false;
				}
			})
	//end autocomplete atc
	//combogrid
	$('#nama_obat').combogrid({
		  debug:true,
		  colModel: [
		  	//{'columnName':'id','width':'14','label':'id'}, 
		  	{'columnName':'name','width':'60','label':'Nama Obat'},
		  	{'columnName':'author','width':'40','label':'Produsen'}],
		  //url: 'manajemenlogistik/detail_faktur/get_list_obat_grid/'+$('#no_produsen').val(),
		  url: 'manajemenlogistik/detail_faktur/get_list_obat_grid',
		  select: function( event, ui ) {
			  $( "#nama_obat" ).val( ui.item.name );
			  $( "#kode_generik" ).val( ui.item.id_generik );
			  $('#obat_hidden').val(ui.item.kode_obat);
					//tambahan baru
					var url_get_nm_fornas='manajemenlogistik/detail_faktur/cobagetfornas/'+ui.item.kode_obat;
					var url_get_nm_atc='manajemenlogistik/detail_faktur/getatc/'+ui.item.kode_obat;
					//var url_get_nm_ukp4='manajemenlogistik/detail_faktur/getukp4/'+ui.item.kode_obat;
					var url_get_nm_generik='manajemenlogistik/detail_faktur/getgenerik/'+ui.item.id_generik;

					$.getJSON(url_get_nm_fornas, function(data) {
							$('#nm_fornas_input').val(data.nama_fornas);
							$('#nm_fornas').text(data.nama_fornas);
							$('#id_fornas').val(data.id_fornas);
							}
						);

					$.getJSON(url_get_nm_atc, function(data) {
							
							$('#kode_atc').val(data.atc);
							$('#nm_atc').text(data.nama_atc_eng+'('+data.nama_atc+')');
							$('#nm_atc_input').val(data.nama_atc_eng+'('+data.nama_atc+')');
							}
						);

					$.getJSON(url_get_nm_generik, function(data) {
							
							$('#nama_generik').val(data.nama_objek);
							$('#nm_indikator').text(data.nama_indikator);
							$('#nm_indikator_input').val(data.nama_indikator);
							$('#kode_indikator').val(data.id_obatindikator);
							$('#nm_program').text(data.nama_program);
							$('#nm_program_input').val(data.nama_program);
							$('#kode_program').val(data.id_obatprogram);
							//$('#nm_atc').text(data.nama_atc_eng+'('+data.nama_atc+')');
							//$('#nm_atc_input').val(data.nama_atc_eng+'('+data.nama_atc+')');
							}
						);
					$( "#satuanbesar" ).val(ui.item.kemasan);
					$( "#satuankecil" ).val(ui.item.sat_jual);
					$( "#kategori_obat" ).val(ui.item.kategori);
			  return false;
		  }
	  });

	//autocomplete with category
	$( "#nm_fornas_input" ).catcomplete({
      delay: 0,
      source: 'manajemenlogistik/detail_faktur/get_list_fornas',
      focus: function( event, ui ) {
			        $( "#nm_fornas_input" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#id_fornas').val(ui.item.id_fornas);
					
					return false;
				}
	//		}
    });
    //end autocomplete with category

	
	//autocomplete fornas
	
 		$('#nm_indikator_input').autocomplete({
			source:'manajemenlogistik/detail_faktur/get_list_indikator', 
			minLength:0,
			focus: function( event, ui ) {
		        $( "#nm_indikator_input" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{					
				$('#kode_indikator').val(ui.item.id_indikator);
				
				return false;
			}
		})

		$('#nm_program_input').autocomplete({
			source:'manajemenlogistik/detail_faktur/get_list_program',
			minLength:0,
			focus: function( event, ui ) {
		        $( "#nm_program_input" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{					
				$('#kode_program').val(ui.item.id_program);
				
				return false;
			}
		})
  
	//end autocomplete fornas

	//autocomplete atc
	
 		$('#nm_atc_input').autocomplete({
				source:'manajemenlogistik/detail_faktur/get_list_atc', 
				minLength:0,
				focus: function( event, ui ) {
			        $( "#nm_atc_input" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#kode_atc').val(ui.item.kode_atc);
					
					return false;
				}
			})
	//end autocomplete atc

	//button update kelompok obat
	$('.labelinput').hide();
	$('#btn_update_obat_master').click(function(e){
		e.preventDefault();
		$('.labelinput').show();
		$('.labeltext').hide();
	})
	$('#btn_save_obat_master').click(function(e){
		var nama_atc=$('#nm_atc_input').val();
		var	nama_fornas=$('#nm_fornas_input').val();
		//var nama_ukp4=$('#nm_ukp4_input').val();
		var nama_indikator = $('#nm_indikator_input').val();
		var nama_program = $('#nm_program_input').val();
		e.preventDefault();
		var update_kelompok='manajemenlogistik/detail_faktur/update_kelompok';
		var format_data={
			kode_obat:$('#obat_hidden').val(),			
			id_fornas:$('#id_fornas').val(),
			//id_ukp4:$('#id_ukp4').val(),
			kode_atc:$('#kode_atc').val(),
			kode_program:$('#kode_program').val(),
			kode_indikator:$('#kode_indikator').val(),
			kode_inn:$('#kode_generik').val(),
		}

		$.ajax({
			url:update_kelompok,
			data:format_data,
			type:"POST",
			success:function(e){
				$('.labelinput').hide();
				$('.labeltext').show();
				$('#nm_atc').text(nama_atc);
				$('#nm_fornas').text(nama_fornas);
				//$('#nm_ukp4').text(nama_ukp4);
				$('#nm_indikator').text(nama_indikator);
				$('#nm_program').text(nama_program);
			}
		})
		
	})
	//back button
		$('#back_to_penerimaan').click(function(){
			var url_penerimaan='manajemenlogistik/penerimaan_obat';
			$('#konten').load(url_penerimaan);
		})


	//=========== del button
	$("#del_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length > 1) {alert("pilih satu saja")}
        else if(id_array.length ==1){
    		var sure=confirm("Are you sure?");
	        	if(sure){
	        		$.ajax({
						url: "manajemenlogistik/detail_faktur/delete_list",
						data: "kode="+id_array,
			        	type: "POST",
			        	success: function(){
			        		alert("data berhasil dihapus");
			        		var url_hasil="manajemenlogistik/detail_faktur/get_data_detail_faktur/"+$("#id_nofaktur").val();
							$("#list_detail_faktur").load(url_hasil);
			        	}
			        })	
	        	}

        	}
        else {alert("pilih data dulu")}

	})
	//=========== end del

	//=========== show add form
	$('#form_detail_faktur').hide();
	$("#add_penerimaan_obat_btn").on("click",function (event){
			
			$("#form_detail_faktur").slideDown("slow");
			$("#partbutton").fadeOut();
			
		});
	$("#batal").on("click",function (event){
			
			$("#form_detail_faktur").slideUp("slow");
			$("#partbutton").fadeIn();
			$("#nama_obat").val("")
				$("#no_batch").val("");
				$("#jum_satbes").val("");
				$("#tanggal").val("");
				$("#harga_beli").val("");
				$('#obat_hidden').val("");
				$('#jum_satkec').val("");
				$('#id_fornas').val("");
				$('#id_ukp4').val("");
				$('#kode_atc').val("");
				$('#nm_fornas').text('');
				$('#nm_fornas_input').val("");
				$('#nm_atc').text("");
				$('#nm_atc_input').val("");
				$('#nm_ukp4').text("");
				$('#nm_ukp4_input').val("");
				$('#kategori_obat option:selected').text("---- Pilih ----").attr('value', '');
				$('#satuanbesar option:selected').text("-------- Pilih --------").attr('value', '');
				$('#satuankecil option:selected').text("-------- Pilih --------").attr('value', '');
		});
	//================ end show add form

	// var url="manajemenlogistik/detail_faktur/get_data_detail_faktur/"+$("#id_nofaktur").val();
	// //mengambil data dg key no_struk
	// $('#list_detail_faktur').load(url);
	//reloadKonten();	

	//============== submit add form
	$("#detail_obat_form").validate({
		ignore:[],
		rules:{
			kode_generik: 'required'
		},
		messages:{
			kode_generik: 'Nama INN harus dipilih dari database'
		},
		submitHandler: function(form){
			var url2="manajemenlogistik/stok_opnam/input_data_detail";
			var data=$("#detail_obat_form").serialize();
			$.post(url2,data,function(){
				alert("sukses tambah data");			
				reloadKonten();
			})
		}
	})
	
});


</script>
<div class="panel panel-primary" id="halaman_detail_faktur">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Detail Stok Awal Tahun ...</b> <button id="back_to_penerimaan" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<!-- Informasi Detail Faktur -->
			<div id="">			
			</div>
			
			<!-- end informasi detail faktur -->
			<div id="partbutton">
				<div class="col-lg-8">
					<button id="add_penerimaan_obat_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah Obat</button>
					<button id="del_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
					<!--button id="cha_penerimaan_obat-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
				</div>
				<div class="col-lg-4">
				<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/detail_faktur/search_data" method="post" id="form_search">
					<div class="input-group" style="float:right;">
				      <input type="text" class="form-control" name="key[word]" id="key">
				      <span class="input-group-btn">
				        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
				      </span>
				    </div><!-- /input-group -->
				</form>
				</div><!-- /col6 -->
			</div>
			<hr>
			<div class="" id="form_detail_faktur">
			  <h4><label>Tambah Sediaan</label></h4>
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
			<form id="detail_obat_form">
				<table class="table">
						<tr class="active">
							<td><small style="color:red;"><i>*</i></small> Nama Sediaan</td>
							<td colspan="2">
								<label>filter produsen: </label><input type="text" name="filter_produsen" id="filter_produsen" size="60" class="inputText">
									<input type="hidden" id="no_produsen" class="inputText">
								<input type="text" class="form-control required inputText" name="nama_obat" id="nama_obat">
								<input type="hidden" id="obat_hidden" name="kode_obat" value="" class="required inputText">
								<input type="hidden" id="" name="nama_tabel" value="<?= $nama_tabel;?>" class="required inputText">
								<small>
									<input type="radio" name="kategori" id="all" value="all" checked="checked"> Semua
									<input type="radio" name="kategori" id="obat" value="obat"> Obat
									<input type="radio" name="kategori" id="alkes" value="alkes"> Alkes
									<input type="radio" name="kategori" id="bmhp" value="bmhp"> BMHP
								</small>
							</td>
							
							<!--td rowspan="2">No. Barcode: <input type="text" class="inputText" name="no_barcode" id="no_barcode" onchange="getData(this.value);"></td-->
						</tr>
						<tr class="active">
							<td><small style="color:red;"><i>*</i></small> Nama INN/FDA</td>
							<td colspan="2"><input type="text" class="form-control required inputText" name="nama_generik" id="nama_generik">
								<input type="hidden" name="kode_generik" id="kode_generik" class="inputText"></td>
						</tr>						
						<tr class="active">
							<td colspan="4">
								<b>Kelompok Sediaan</b><br>
								<table>
									<tr class="infokategori"><td>Fornas</td>
										<td>: <label class="labeltext" id="nm_fornas"></label><input type="hidden" id="id_fornas" name="id_fornas" value="" class="inputText">
												<input type="text" class="labelinput inputText" id="nm_fornas_input" value="" size="50">
										</td></tr>
									<!--tr class="infokategori"><td>Obat Indikator 2014</td>
										<td>: <label class="labeltext" id="nm_ukp4"></label><input type="hidden" id="id_ukp4" name="id_ukp4" value="" class="inputText">
											<input type="text" class="labelinput inputText" id="nm_ukp4_input" value="" size="50">
										</td></tr-->
									<tr class="infokategori"><td>ATC</td>
										<td>: <label class="labeltext" id="nm_atc"></label><input type="hidden" id="kode_atc" name="kode_atc" value="" class="inputText">
											<input type="text" class="labelinput inputText" id="nm_atc_input" value="" size="50">
										</td></tr>
									<tr class="infokategori"><td>Obat Indikator</td>
										<td>: <label class="labeltext" id="nm_indikator"></label><input type="hidden" id="kode_indikator" name="kode_indikator" value="" class="inputText">
											<input type="text" class="labelinput inputText" id="nm_indikator_input" value="" size="50">
										</td></tr>
									<tr class="infokategori"><td>Obat Program</td>
										<td>: <label class="labeltext" id="nm_program"></label><input type="hidden" id="kode_program" name="kode_program" value="" class="inputText">
											<input type="text" class="labelinput inputText" id="nm_program_input" value="" size="50">
										</td></tr>
								</table>
								<button class="labeltext" id="btn_update_obat_master">Update</button><button class="labelinput" id="btn_save_obat_master">Simpan</button>
							</td>
						</tr>
					</table>
					<table width="">
						<tr>
							<td width="20%"><small style="color:red;"><i>*</i></small> No Batch</td>
							<td width="30%"><input type="text" class="form-control required inputText input-sm" name="no_batch" id="no_batch"></td>
							<td width="10%"><small style="color:red;"><i>*</i></small> Kategori</td>
							<td width="40%"><select name="kategori" id="kategori_obat" class="form-control required inputText">
									<option value="">---- Pilih ----</option>
									<option value="Generik">Generik</option>
									<option value="Generik Bermerek">Generik Bermerek</option>
									<option value="Paten">Paten</option>
								</select>
							</td>
						</tr>
						<tr>							
							<td><small style="color:red;"><i>*</i></small> Total jumlah</td>
							<td><table><tr><td>
								<input type="text" class="form-control required inputText" name="jumlah_kec" id="jum_satkec" size="7">
								</td><td>
								<select name="sat_jual" id="satuankecil" class="form-control required inputText">
									<option value="">-------- Pilih --------</option>
									<!--option value="">Box</option>
									<option value="">Kardus</option>
									<option value="">Strip</option-->
								<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
								<?php endfor; ?>
								</select></td></tr>
								</table>
							</td>
							<td><small style="color:red;"><i>*</i></small> Harga Beli</td>
							<td><div class="input-group">
									<span class="input-group-addon" id="basic-addon1">Rp</span>
									<input type="text" class="form-control required inputText" name="harga_beli" id="harga_beli" aria-describedby="basic-addon1">
								</div>
								<small style="color:red;"><i>(contoh: 200,75)</i></small></td>
						</tr>
						<tr>
							<td><small style="color:red;"><i>*</i></small> Tanggal Kadaluarsa</td>
							<td><div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
									<input class="form-control span2 required inputText" size="12" type="text" id="tanggal"readonly="readonly" name="tanggal">
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>

									<script>
									$('#datepicker').datepicker();
									</script></td>
							<td>Jumlah rusak</td>
							<td><input type="text" class="form-control required inputText" name="jml_rusak" id="jml_rusak" value="0"></td>
						</tr>
						<tr>
							<td colspan="4">
								<small style="color:red;"><i>*Harus Diisi</i></small>
							</td>
							
						</tr>
						<tr>
							<td colspan="4">
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_detail_faktur" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
									<button id="reset" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh"></span> Clear</button>
								</div>
							</td>
							
						</tr>
					</table>
				</form>
			</div>
			<!--/form-->
			<div id="list_detail_faktur"></div>
	</div>
</div>
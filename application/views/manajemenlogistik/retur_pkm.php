<style>
.datepicker{
	z-index:1151;
}
.ui-autocomplete { z-index:2147483647; }
</style>
<script>
$(document).ready(function(){
	$( "#nama_sediaan").autocomplete({
				source:'manajemenlogistik/retur/get_list_obat', 
				minLength:2,
				focus: function( event, ui ) {
			        $( "#nama_sediaan").val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{

					$('#kode_obat').val(ui.item.kode_obat);
					$('#no_batch').val(ui.item.batch);
					$('#id_stok').val(ui.item.id_stok);
					$('#tanggal_faktur').val(ui.item.tanggal);
					$('#pbf').val(ui.item.pbf);
					$('#no_faktur').val(ui.item.no_faktur);
					return false;
				}
			})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		      	.data( "ui-autocomplete-item", item )
		        .append("<a>"+ item.value +"<br><small>Stok: <i>" + item.stok + "</i><br>Expired date:"+ item.ed +"</small></a>")
		        .appendTo( ul );
		    };
	//$("#form_retur").validate();

		
	//autocomplete pbf
	$('#pbf').autocomplete({
				source:'manajemenlogistik/retur/get_list_pbf', 
				minLength:2,
				/*select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#obat_hidden').val(ui.item.kode_obat);
					//this.form.obat_hidden.value = ui.item.kode_obat;
				}*/
			});
	//end autocomplete pbf
	
	$("#batal").on("click",function (event){
			event.preventDefault();
			$(".form-control").val('');
		});
	//================ end show add form

	var url="manajemenlogistik/retur/get_data_retur_pkm";
	$('#list_retur').load(url);	
	//============== submit add form
	$("#form_retur").validate({
		submitHandler: function(form){
			var url="manajemenlogistik/retur/input_retur_pkm";
			var data=$("#form_retur").serialize();
			$.post(url,data,function(){
				alert("sukses tambah data");
				//$("#form_retur_modal").slideUp("slow");
				//$("#form_retur_modal").reset();
				//$("#partbutton").fadeIn();			
				var url_hasil="manajemenlogistik/retur/retur_pkm";
				$("#konten").load(url_hasil);
				//$('.form-control').val('');
			});
			return false;
		}
	});
});
</script>
<div class="panel panel-primary" id="halaman_retur">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Retur Obat</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">		
	<ul class="nav nav-tabs">
		<li class="active"><a href="#pane_retur" data-toggle="tab">Form Retur dari Unit Internal</a></li> 
	    <li><a href="#histori_retur" data-toggle="tab">List Sediaan Retur</a></li>  
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="pane_retur">
		<br>
		<div id="form_retur_modal">
		<form id="form_retur">
				<table class="table">
					<tr>
						<td><small style="color:red;"><i>*</i></small> Nama Sediaan</td>
						<td><input type="text" class="form-control required" name="nama_sediaan" id="nama_sediaan">
							<input type="hidden" name="kode_obat" id="kode_obat">
							<input type="hidden" name="id_stok" id="id_stok">
							</td>
						<td>No. Batch</td>
						<td>
							<input type="text" class="form-control" name="no_batch" id="no_batch" readonly>
						</td>
					</tr>
						<tr>
							<td>Nomor Faktur</td>
							<td><input type="text" class="form-control" name="no_faktur" id="no_faktur" readonly></td>
							<td>Tanggal Faktur</td>
							<td>
								<input type="text" class="form-control" name="tanggal_faktur" id="tanggal_faktur" readonly>
							</td>
						</tr>
						<tr>
							<td>Nama PBF</td>
							<td><input type="text" class="form-control" name="pbf" id="pbf" readonly></td>
							<td>Keterangan</td>
							<td><input type="text" class="form-control" name="keterangan" id="keterangan"></td>							
						</tr>
						<tr>
							<td><small style="color:red;"><i>*</i></small>Jumlah</td>
							<td><input type="text" class="form-control required" name="jumlah" id="jumlah"></td>
							<td><small style="color:red;"><i>*</i></small>Tanggal Retur</td>
							<td>
								<div class="input-append date" id="datepicker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
									<input class="span2 required" size="12" type="text" readonly="readonly" id="tanggal_retur" name="tanggal_retur">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>
									<script>
									$('#datepicker').datepicker();
									</script>
							</td>							
						</tr>
						<tr><td colspan="4"><small style="color:red;"><i>*Harus diisi</i></small> </td>
						</tr>
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_retur" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal"><span class="glyphicon glyphicon-remove"></span> Reset</button>
								</div>
							</td>
							
						</tr>
					</table>
				</form>
				</div>
			</div>
			<div class="tab-pane" id="histori_retur">
				<div id="list_retur"></div>
			</div>
		</div>
	</div>
</div>

<style type="text/css">
.ui-autocomplete-loading {
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}
 .ui-autocomplete { z-index:2147483647; }
</style>
<script type="text/javascript">
$(function(){
	$('.btn-save-date').on("click", function() {
		var url = 'manajemenlogistik/detail_distribusi/updatedate/'+$('#hide_lplpo').val();
		var datanew = $('#tanggal_kejadian').val();

		$.ajax({
			method: "POST",
			url: url,
			data: "dateNew="+datanew+"&no_dok="+$('#no_dok').val(),
			dataType: "json",
			success: function (result) {
				alert(result.message);
			}
		})
	})

	$('#datepicker612').datepicker();

	$('#nama_obat_adt').autocomplete({
		source:'manajemenlogistik/detail_distribusi/get_list_obat2',
		minLength:2,
		focus: function( event, ui ) {
	        $( "#nama_obat_adt" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{
			$('#kode_obat_adt').val(ui.item.kode_obat);
			$('#batch_adt').val(ui.item.nobatch);
			//$('#label_batch_1').text(ui.item.nobatch);
			$('#jml_adt').focus();
			$('#lb_batch').text(ui.item.nobatch)
			$('#id_stok_adt').val(ui.item.id_stok);
			return false;
		}
	})
 	.each(function(){
 		$(this).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append("<a>"+ item.value +"<br><small><b>Stok/batch/ED/sumber dana: </b>"+ item.stok +"/"+item.nobatch+"/"+item.ed+"/"+item.sumber_dana+" "+item.tahun_anggaran+"</small></a>")
	        .appendTo( ul );
	    };
 	});

	$('.obat_pemberian').autocomplete({
		source:'manajemenlogistik/detail_distribusi/get_list_obat2',
		minLength:2,
		focus: function( event, ui ) {
	        $( ".obat_pemberian" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
		{
			$('.kode_obat').val(ui.item.kode_obat);
			$('.no_batch').val(ui.item.nobatch);
			//$('#label_batch_1').text(ui.item.nobatch);
			$('.jml_beri_new').focus();
			$('.id_stok_hidden').val(ui.item.id_stok);
			return false;
		}
	})
 	.each(function(){
 		$(this).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
			.append("<a>"+ item.value +"<br><small><b>Stok/batch/ED/sumber dana: </b>"+ item.stok +"/"+item.nobatch+"/"+item.ed+"/"+item.sumber_dana+" "+item.tahun_anggaran+"</small></a>")
	        .appendTo( ul );
	    };
 	});

	//end autocomplete

	$('.editable').hide();
	$('.btn_cnl').click(function(){
		//$('.editable').hide();
		$(this).parent().parent().find('.show_t').show();
		$(this).parent().parent().find('.editable').hide();
		$('.btn_ge').show();
	})
	$("#print_btn").click(function(){
		var urlprint="manajemenlogistik/distribusi_obat_kel/printOut/<?php echo $id_lplpo; ?>";
		window.open(urlprint);
	})

	$("#edit_btn").click(function(){
		var url="manajemenlogistik/distribusi_obat_kel/get_detail/<?php echo $id_lplpo; ?>";
		//alert(urlprint);
		$('#konten').load(url);
	})

	$('.btn_edit').click(function(){
		$(this).parent().parent().find('.show_t').hide();
		$(this).parent().parent().find('.editable').show();
		//$(this).parent().find('.btn_ge').hide();
		//$('.editable').show();
		$('.btn_ge').hide();
		return false;
	})

	$('.btn_save').click(function(){
		kode_obat = $(this).parent().parent().find('.kode_obat').val();
		jml_awal = $(this).parent().parent().find('.jml_awal').val();
		jml_beri_new = $(this).parent().parent().find('.jml_beri_new').val();
		no_batch = $(this).parent().parent().find('.no_batch').val();
		batch_awal = $(this).parent().parent().find('.batch_awal').val();
		id_stok = $(this).parent().parent().find('.id_stok_hidden').val();
		id_dd = $(this).parent().parent().find('.id_dd').val();
		id_dl = $(this).parent().parent().find('.id_dl').val();//id_detail_lplpo_kel
		happen_date = $('#tanggal_kejadian').val();

		id_lplpo = $(this).parent().parent().find('.id_lplpo').val();
		obat_req = $(this).parent().parent().find('.kode_obat_req').val();

		datanew = "dt[kode_obat]="+kode_obat+"&dt[jml_awal]="+jml_awal+
			"&dt[jml_beri_new]="+jml_beri_new+"&dt[no_batch]="+no_batch+
			"&dt[id_stok]="+id_stok+"&dt[id_dd]="+id_dd+"&dt[batch_awal]="+batch_awal+"&dt[id_dl]="+id_dl+
			"&dt[id_lplpo]="+id_lplpo+"&dt[kode_obat_req]="+obat_req+"&dt[happen_date]="+happen_date;

		url='manajemenlogistik/detail_distribusi/update_lplpodist';

		$.post(url,datanew,function(result){
			var response = jQuery.parseJSON(result);
			if(response.status == '1'){
				reloadTable();
			}
			alert(response.message);
		})
	})

	$('.btn-delete').click(function(){
		jml_awal = $(this).parent().parent().find('.jml_awal').val();
		jml_beri_new = $(this).parent().parent().find('.jml_beri_new').val();
		id_dd = $(this).parent().parent().find('.id_dd').val();
		id_dl = $(this).parent().parent().find('.id_dl').val();//id_detail_lplpo_kel
		happen_date = $('#tanggal_kejadian').val();
		id_lplpo = $(this).parent().parent().find('.id_lplpo').val();

		datanew = "dt[jml_awal]="+jml_awal+
			"&dt[jml_beri_new]="+jml_beri_new+
			"&dt[id_dd]="+id_dd+"&dt[id_dl]="+id_dl+
			"&dt[id_lplpo]="+id_lplpo+"&dt[happen_date]="+happen_date;

		url='manajemenlogistik/detail_distribusi/delete';

		$.post(url,datanew,function(result){
			var response = jQuery.parseJSON(result);
			if(response.status == '1'){
				reloadTable();
			}
			alert(response.message);
		})
	})

	$('.btn_add').click(function(){
		id_lplpo = $(this).parent().parent().find('.id_lplpo').val();
		id_dl = $(this).parent().parent().find('.id_dl').val();
		obat_req = $(this).parent().parent().find('.kode_obat_req').val();
		$('#id_lplpo_adt').val(id_lplpo);
		$('#obat_req_adt').val(obat_req);
		$('#id_dl_adt').val(id_dl);
		$("#modal_adt").modal('show');
	})

	$('#btn-save-adt').click(function(){
		var url = 'manajemenlogistik/distribusi_obat_kel/additional_drug';
		var data = "dt[kode_obat_req]="+$('#obat_req_adt').val()+"&dt[kode_obat_res]="+$('#kode_obat_adt').val()+
			"&dt[pemberian]="+$('#jml_adt').val()+"&dt[no_batch]="+$('#batch_adt').val()+
			"&dt[id_stok]="+$('#id_stok_adt').val()+"&dt[id_dl]="+$('#id_dl_adt').val()+
			"&dt[id_lplpo]="+$('#id_lplpo_adt').val()+"&dt[create_time]="+$('#tanggal_kejadian').val();
		$.post(url, data, function() {
			alert("Data berhasil disimpan");
			$('#modal_adt').modal('toggle');
			reloadTable();
		})
	})
})
</script>
<div id="">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<div class="row">
		<div class="col-md-3">
      <label id="dtl_ed" class="labeltext2">Tanggal Pemberian</label>
      <div class="input-group input-append date edittext" id="datepicker612" data-date="<?php echo date_format(date_create($happen_date),'Y-m-d') ?>" data-date-format="yyyy-mm-dd" >
        <input class="form-control required" size="12" type="text" id="tanggal_kejadian" name="tanggal_pemberian" value="<?php echo date_format(date_create($happen_date),'Y-m-d') ?>">
        <span class="input-group-addon add-on" style=""><span class="glyphicon glyphicon-calendar"></span></span>
      </div>
		</div>
    <div class="col-md-3">
      <label id="" class="labeltext2">No. Dokumen</label>
      <input class="form-control required" size="12" type="text" id="no_dok" name="no_dok" value="<?php echo $lplpo['no_dok'] ?>">
		</div>
    <br>
    <button class="btn btn-success btn-save-date"><span class="glyphicon glyphicon-ok"></span> update</button>
	</div>
	<br>
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Obat Permintaan</th>
			<th>Sediaan</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Obat Pemberian</th>
			<th>No. Batch</th>
			<th>Pemberian</th>
			<th></th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				foreach ($rowspan as $key) {
					$dub[$key->kode_obat] = $key->qty;
				};
				$i=0;$j=1;$flag = 'hem';
				foreach ($result as $rows): ?>
					<tr style=""><td><?= $j ?></td>
					<?php //if ($flag != $rows->kode_obat) : ?>
					<?php if ($flag == 'x') : ?>
						<td rowspan="<?= $dub[$rows->kode_obat] ?>"><?= $rows->nama_obat ?></td>
						<td rowspan="<?= $dub[$rows->kode_obat] ?>"><?= $rows->sediaan ?></td>
						<td rowspan="<?= $dub[$rows->kode_obat] ?>"><?= $rows->stok_opt ?></td>
						<td rowspan="<?= $dub[$rows->kode_obat] ?>"><?= $rows->permintaan ?></td>
					<?php else: ?>

					<?php endif; ?>
					<td rowspan=""><?= $rows->nama_obat ?></td>
					<td rowspan=""><?= $rows->sediaan ?></td>
					<td rowspan=""><?= $rows->stok_opt ?></td>
					<td rowspan=""><?= $rows->permintaan ?></td>
					<td>
						<input type="text" class="editable obat_pemberian" value="<?= $rows->obat_res ?>" size="25"><div class="show_t"><?= $rows->obat_res ?></div>
						<input type="hidden" class="kode_obat_req" value="<?= $rows->kode_obat ?>">
						<input type="hidden" class="id_lplpo" value="<?= $rows->id_lplpo ?>">
						<input type="hidden" class="kode_obat" value="<?= $rows->kode_obat_res ?>">
						<input type="hidden" class="id_stok_hidden" value="<?= $rows->id_stok ?>">
						<input type="hidden" class="id_dd" value="<?= $rows->id ?>">
						<input type="hidden" class="id_dl" value="<?= $rows->id_dl ?>">
					</td>
					<td>
						<input type="text" class="editable no_batch" value="<?= $rows->no_batch ?>" size="6" readonly="readonly"><div class="show_t"><?= $rows->no_batch ?></div>
						<input type="hidden" class="batch_awal" value="<?= $rows->no_batch ?>"></td>
					<td>
						<input type="text" class="editable jml_beri_new" value="<?= $rows->pemberian ?>" size="3"><div class="show_t"><?= $rows->pemberian ?></div>
						<input type="hidden" class="jml_awal" value="<?= $rows->pemberian ?>"></td>
					<td>
						<button class="btn_edit show_t btn_ge"><span class="glyphicon glyphicon-pencil"></span></button>
						<button class="btn_save editable btn-success"><span class="glyphicon glyphicon-ok"></span></button>
						<button class="btn_add editable btn-primary"><span class="glyphicon glyphicon-plus"></span></button>
						<button class="btn_cnl editable btn-warning"><span class="glyphicon glyphicon-arrow-left"></span></button>
						<button class="btn-delete editable btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
					</td>
				</tr>
			<?php $flag= $rows->kode_obat;$i++;$j++;endforeach; ?>
		</tbody>
	</table>
	<div style="float:right;">
		<!--button id="edit_btn"  class="btn-warning"><span class="glyphicon glyphicon-pencil"></span> Ubah List SBBK</button-->
		<button id="print_btn" style="" class="btn btn-primary"><span class="glyphicon glyphicon-print"></span> cetak SBBK</button>
	</div>
</div>

<!-- MODAL UBAH DATA -->
<div class="modal fade" id="modal_adt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        	<h4 class="modal-title" id="myModalLabel">Tambah Obat</h4>
	      	</div>
		  	<div class="modal-body">
		  		<div class="form-group">
		  			<label>Obat Pemberian</label>
		  			<div class="input-group">
		  				<input type="text" id="nama_obat_adt" class="form-control" name="" aria-describedby="lb_batch">
		  				<input type="hidden" id="id_lplpo_adt">
				  		<input type="hidden" id="obat_req_adt">
				  		<input type="hidden" id="id_dl_adt">
				  		<input type="hidden" id="id_stok_adt" name="">
				  		<input type="hidden" id="kode_obat_adt" name="">
				  		<input type="hidden" id="batch_adt" name="">
				  		<span class="input-group-addon" id="lb_batch">nomor batch</span>
		  			</div>
		  		</div>
		  		<div class="form-group">
		  			<div class="input-group">
		  				<label>Jumlah</label>
		  				<input class="form-control" type="text" id="jml_adt">
		  			</div>
		  		</div>
	  			<div class="form-group">
	  				<div class="input-group">
			  			<button style="float: right;" class="btn btn-success btn-sm" id="btn-save-adt"><span class="glyphicon glyphicon-floppy-saved"></span> Save</button>
			  		</div>
	  			</div>
		  	<!--/form-->
			</div>
		</div>
	</div>
</div>

 <style>
	
</style>
<script>
$(document).ready(function(){

	$("#cito_internal").hide();
	$("#cito_eksternal").hide();
	//$("#selection").buttonset();
	$('input:radio[name="pilihan_cito"]').change(function(){
		if($(this).val()=="cito_internal"){
			$("#cito_internal").show();
			$("#cito_eksternal").hide();
		}else{
			$("#cito_internal").hide();
			$("#cito_eksternal").show();
		}
	});

	//=========== del button
		

	$("#del_distribusi_obat_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "manajemenlogistik/distribusi_obat/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="manajemenlogistik/distribusi_obat/get_data_distribusi_obat"
					$("#list_distribusi_obat").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_distribusi_obat_modal').hide();
	$("#add_distribusi_obat_btn").on("click",function (event){
			//$("#add_distribusi_obat_modal").modal('show');
			$("#form_distribusi_obat_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_distribusi_obat_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_distribusi_obat_modal").modal('show');
			$("#form_distribusi_obat_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_distribusi_obat_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="manajemenlogistik/distribusi_obat/getdatapermintaan";
	$('#daftarpermintaan').load(url);

	var url="manajemenlogistik/distribusi_obat/get_data_distribusi_obat";
	$('#list_distribusi_obat').load(url);	
	//============== submit add form

	$("#btn_add").click(function(){
		var url2="manajemenlogistik/distribusi_obat/input_data";
		var form_data = {
			kec:$('#list_kec').val(),
			periode:$('#periode_cito').val(),
			unit_penerima:$('#nmpuskesmas').val(),
			id_dok:$('#no_dok').val(),
			user_penerima:$('#nama_penerima').val(),
			nip:$('#nip').val(),
			datenow:$('#datenow').val(),
			unit_luar:$('#unit_luar').val(),
			keperluan:$('#keperluan_cito').val(),
			unit:$('input:radio[name="pilihan_cito"]').val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				//$("#form_distribusi_obat_modal").slideUp("slow");
				//$("#form_distribusi_obat_modal").reset();
				//$("#partbutton").fadeIn();
				//var url_hasil="manajemenlogistik/distribusi_obat_obat/get_list_distribusi_obat"
				
				var url_hasil="manajemenlogistik/distribusi_obat/get_data_distribusi_obat"
				$("#list_distribusi_obat").load(url_hasil);//+"#list_sedian_obat");

				$('#list_kec').val("");
				$('#periode_cito').val("");
				$('#nmpuskesmas').val("");
				$('#no_dok').val("");
				$('#nama_penerima').val("");
				$('#nip').val("");
				$('#unit_luar').val("");
				$('#keperluan_cito').val("");
				//datenow:$('#$datenow').val()
			}
		});
	})

	//============== end submit add form
});

//get data puskesmas
function get_puskesmas(val) {
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }else{
    	//alert("cek");
    	$.get('manajemenlogistik/distribusi_obat/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas').html(data);});
    }
}

//====== end get puskesmas
function tambahpuskesmas(val){
	if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }
}
//=================== add keperluan cito
function tambahkeperluan(val){
	if(val == 'add'){
    	prompt("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }
}
</script>
<div class="panel panel-primary" id="halaman_distribusi_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Distribusi Sediaan</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#listpermintaan" data-toggle="tab">List Permintaan</a></li>
		  <!--li class=""><a href="#normal" data-toggle="tab">Normal</a></li-->
		  <li class=""><a href="#cito" data-toggle="tab">Sewaktu</a></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="listpermintaan">
				<div id="daftarpermintaan">
					<table>
					</table>
				</div>
			</div>
			<div class="tab-pane" id="cito" style="">
			<br>
			<table class="table">
				<tr>
					<td colspan="2">
						<div class="input-group">
					      <span class="input-group-addon">
					        <input type="radio" name="pilihan_cito" value="cito_internal">
					      </span>
					      <input type="text" class="form-control" value="Unit Internal" size="8" readonly>
					    </div>
					</td><td colspan="2">
						<div class="input-group">
					      <span class="input-group-addon">
					        <input type="radio" name="pilihan_cito" value="cito_eksternal">
					      </span>
					      <input type="text" class="form-control" value="Unit Eksternal" size="10" readonly>
					    </div>
					</td><td></td><td></td>
				</tr>
					<tr id="cito_internal">						
						<td>Kecamatan</td>
						<td>
							<select name="list_kec" id="list_kec" onchange="get_puskesmas(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						<td>Unit Penerima</td>
						<td>
							<select name="nmpuskesmas" id="nmpuskesmas" onchange="tambahpuskesmas(this.value);">
								<option value="0">--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>
						
					</tr>
					<tr id="cito_eksternal">
						<td>Unit Eksternal</td>
						<td><input type="text" class="form-control" name="unit_luar" id="unit_luar"></td>
						<td></td><td></td>
					</tr>
					<tr>
						<td>Bulan/Periode</td>
						<td><input type="hidden" value="<?php echo date('Y-m-d'); ?>" id="datenow">
							<div class="input-append date" id="datepicker" data-date="2014-04-15" data-date-format="yyyy-mm" >
									<input class="span2" size="12" type="text" value="" readonly="readonly" id="periode_cito" name="periode_cito">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>

									<script>
									$('#datepicker').datepicker(
										{
										    format: "yyyy-mm",
										    viewMode: "months", 
										    minViewMode: "months"
										});
									</script>
						</td>
						<td>Nomor Dokumen</td>
						<td>
							<input type="text" class="form-control" name="no_dok" id="no_dok">
						</td>
					</tr>
					<tr>
						<td>Keperluan</td>
						<td colspan="3"><select name="keperluan_cito" id="keperluan_cito" onchange="tambahkeperluan(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($cito);$i++): ?>
								<option value="<?php echo $cito[$i]['keperluan']; ?>"><?php echo $cito[$i]['keperluan']; ?></option>
								<?php endfor; ?>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>
					<tr>
						<td>Nama Penerima</td>
						<td>
							<input type="text" class="form-control" name="nama_penerima" id="nama_penerima">
						</td>
						<td>NIP Penerima</td>
						<td>
							<input type="text" class="form-control" name="nip" id="nip">
						</td>
					</tr>
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_add" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="del_penerimaan_obat_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
								<button id="cha_penerimaan_obat-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
							</div>
						</td>
						<td colspan="3">
							
							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
				<!--/form-->
				
				<br><br>

				<div id="list_distribusi_obat"></div>
			</div><!-- end normal -->
			<!--div class="tab-pane" id="cito">
				CITO CITO CITO CITO CITO CITO
			</div-->
		</div>
		</div>
</div>
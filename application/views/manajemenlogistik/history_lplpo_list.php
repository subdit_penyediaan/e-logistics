<script type="text/javascript">
	$(function(){
		$("#pagelisthistory_lplpo").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_history_lplpo').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;
	}
</style>
<div id="pagelisthistory_lplpo">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<hr>
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">
			<th width="3%"><span class="glyphicon glyphicon-trash"></span></th>
			<th>Kecamatan</th>
			<th>Puskesmas</th>
			<th>Bulan/Periode</th>
			<th width="8%">Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td class="btn_action"><input type="checkbox" name="chk[]" id="cek_del_history_lplpo" value="'.$rows->id.'" class="chk">							
					</td>
					<td>'.$rows->nama_kec.'</td>
					<td>'.$rows->nama_pusk.'</td>
					<td>'.$rows->periode.'</td>
					<td class="btn_action"><button href="'.$base_url.'index.php/manajemenlogistik/isian_lplpo/get_detail/'.$rows->id.'" id="detail_faktur"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
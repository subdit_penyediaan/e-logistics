<script type="text/javascript">
	$(function(){
		$("#pagelistdetail_stok_opnam").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_detail_stok_opnam').load(url);
		});
		//form submit
		$('#form_detail_stok_opnam').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            dataType: 'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                if(data.con==0){
	                	alert(data.confirm);	
	                }else{
	                	alert(data.confirm);
		                //var url='manajemenlogistik/stok_opnam';
		                var url='manajemenlogistik/detail_stok_opnam/get_detail/'+data.id_so;
		                $('#konten').load(url);	
	                }
	            }
			})
			
		});
		//end form submit
	});

	var seq=0;
	function getDelta1(val){
		seq++;
		//var obj=$()
		var nilai1=val;
		var nilai2=$('#noted_stok_'+seq).val();

		nilai3= val-nilai2;
		$('#selisih_'+seq).val(nilai3);
	}
</script>
<div id="pagelistdetail_stok_opnam">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/detail_stok_opnam/input_data" method="post" id="form_detail_stok_opnam">
	<input type="hidden" id="hidden_id_stok_opnam" name="id_stok_opnam" value="<?php echo $detailstok; ?>">
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">	
			<th>No.</th>
			<th>Kode Obat</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>No. Batch</th>
			<th>Stok Tercatat</th>
			<th>Stok Fisik</th>
			<th>Selisih</th>
			<th>Keterangan</th>
			<th>Last Update</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					//if()

					echo '<tr style=""><td>'.$i.'</td>
					<td>'.$rows->kode_obat.'
						<input type="hidden" name="kode_obat[]" value="'.$rows->kode_obat.'">
						<input type="hidden" name="id_stok_obat[]" value="'.$rows->id_stok_obat.'">
					</td>
					<td>'.$rows->nama_obat.' '.$rows->kekuatan.'</td>
					<td>'.$rows->sediaan.'</td>
					<td>-</td>
					<td><input type="text" name="noted_stok[]" id="noted_stok_'.$i.'" value="'.$rows->noted_stok.'" size="5"readonly></td>
					<td><input type="text" name="stok_fisik[]" id="stok_fisik_'.$i.'" size="5" onchange="" value=""></td>
					<td><input type="text" name="selisih[]" id="selisih_'.$i.'" size="5" value="" readonly></td>
					<td><input type="text" name="ket[]" id="ket"></td>
					<td>'.date('d-m-Y h:i:s').'</td>
					<td><button class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span></button></td>
					</tr>';


					$i++;
				}
			?>
		</tbody>
	</table>
	<input type="submit" value="simpan" class="btn btn-success">
	</form>
</div>
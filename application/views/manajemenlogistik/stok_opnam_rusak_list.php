<script>
$(document).ready(function(){	
	$("#update_status_stok_opname_rusak").click(function(){
		var conf = confirm("Apakah Anda yakin?");
		if (conf) {
			var url="manajemenlogistik/pemusnahan_obat/update_stok_opnam_rusak";
			$.get(url, function(){
			    alert("Obat telah dimusnahkan");
			    var url="manajemenlogistik/pemusnahan_obat";
				$('#konten').load(url);
			});	
		}		
	});
})
</script>
<table class="table table-striped table-bordered">
	<thead>
	<tr class="active">
		<th>No. Faktur</th>
		<th>Kode Obat</th>
		<th>Nama Obat</th>
		<th>Jumlah</th>
		<th>No. batch</th>
	</tr>
	</thead>
	<tbody id="trcontent">
		<?php
			foreach ($result as $rows) {
				echo '<tr class="danger">
				<td>'.$rows->nomor_faktur.'</td>
				<td>'.$rows->kode_obat.'</td>
				<td>'.$rows->nama_obat.'</td>
				<td>'.$rows->jumlah.'</td>
				<td>'.$rows->no_batch.'</td></tr>';
			}
		?>
	</tbody>
</table>
<button id="update_status_stok_opname_rusak" class="btn btn-danger">HAPUS</button>
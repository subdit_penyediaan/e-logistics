<style>
.datepicker{
	z-index:1151;
}
.ui-autocomplete { z-index:2147483647; }
</style>
<script>
$(document).ready(function(){
	//============== submit add form

	$("#btn_cha").click(function(){
		var url2="manajemenlogistik/history_lplpo/update_data";
		var form_data = {
			id_lplpo:$("#cha_id_lplpo").val(),
			periode:$("#cha_periode_lplpo").val(),
			kode_kec:$("#cha_kec_lplpo").val(),
			kode_pusk:$("#cha_namapusk").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("berhasil update data");
				$('#change_lplpo').modal('toggle');
				var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
				$("#list_history_lplpo").load(url_hasil);

				$("#cha_id_lplpo").val("");
				$("#cha_periode_lplpo").val("");
				$("#cha_kec_lplpo").val("");
				$("#cha_namapusk").val("");
			}
		});
	})

	$('#cha_history_lplpo-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {	    	
	    	var form_data = { id_lplpo: id_array[0] };
	    	$.ajax({
	    		type:"POST",
	    		url:'manajemenlogistik/history_lplpo/datatochange',
	    		data: form_data,
	    		dataType:'json',
	    		success:function(data) {	    			
	    			$('#cha_id_lplpo').val(data[0].id);	    			
	    			//$('#cha_nama_dok').val(data[0].nama_dok);
	    			$('#cha_periode_lplpo').val(data[0].periode);	    			
	    			$("#cha_kec_lplpo" ).val(data[0].kode_kec);
	    			urlpusk='manajemenlogistik/history_lplpo/get_data_puskesmas/'+data[0].kode_kec+'/'+data[0].kode_pusk;
	    			$.get(urlpusk, function(data) {$('#cha_namapusk').html(data);});
	    			//get_puskesmas(data[0].kode_kec);
	    			//$("#cha_namapusk" ).val(data[0].kode_pusk);
	    			$('#change_lplpo').modal('show');
	    			//return false;
	    		}
	    	});
	    	
	    }
	})

	$('#set_value_opt').click(function(){
		var value_opt=prompt("Masukkan nilai buffernya (%)");
		//alert(value_opt);
		$.ajax({
			url:"manajemenlogistik/history_lplpo/post_opt_value",
			data:"nilai="+value_opt,
			type:"POST",
			success: function(){
				alert("data telah dimasukkan");
			}
		})
	})

	$('#download_template_lplpo').click(function(){
		var url4="manajemenlogistik/history_lplpo/getTemplate";
		window.open(url4);
	})

	//autocomplete pbf
	$('#pbf').autocomplete({
				source:'manajemenlogistik/history_lplpo/get_list_pbf', 
				minLength:2,
				/*select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#obat_hidden').val(ui.item.kode_obat);
					//this.form.obat_hidden.value = ui.item.kode_obat;
				}*/
			});
	//end autocomplete pbf

	//=========== del button
		
	$("#del_history_lplpo_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "manajemenlogistik/history_lplpo/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
					$("#list_history_lplpo").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_history_lplpo_modal').hide();
	$("#add_history_lplpo_btn").on("click",function (event){
			//$("#add_history_lplpo_modal").modal('show');
			$("#form_history_lplpo_modal").slideDown("slow");
			$("#partbutton_lplpo").fadeOut();
				//$("#add_history_lplpo_modal").modal({keyboard:false});
		});
	$("#batal_hist_lplpo").on("click",function (event){
			//$("#add_history_lplpo_modal").modal('show');
			$("#form_history_lplpo_modal").slideUp("slow");
			$("#partbutton_lplpo").fadeIn();
				//$("#add_history_lplpo_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="manajemenlogistik/history_lplpo_obat/get_list_history_lplpo";
	//$('#list_history_lplpo_obat').load(url);

	var url="manajemenlogistik/history_lplpo/get_data_history_lplpo";
	$('#list_history_lplpo').load(url);	
	//============== submit add form
	$("#form_input_lplpo").validate({
		submitHandler: function(form){
			var url2="manajemenlogistik/history_lplpo/input_data";
			var url3="manajemenlogistik/history_lplpo/data_inputed";
			var data=$("#form_input_lplpo").serialize();

			$('input[name="data_include"]').attr('checked',function(){
				if($(this).is(':checked')){
					$.post(url3,data,function(data){
						var obj = jQuery.parseJSON(data);
						if(obj.con==1){
							confirm(obj.confirm);
						}else{
							alert(obj.confirm);	
						}
						$("#form_history_lplpo_modal").slideUp("slow");
						$("#partbutton_lplpo").fadeIn();
						var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
						$("#list_history_lplpo").load(url_hasil);//+"#list_sedian_obat");

						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					})
				}
				else{
					$.post(url2,data,function(data){
						var obj = jQuery.parseJSON(data);
						alert(obj.confirm);
						$("#form_history_lplpo_modal").slideUp("slow");
						$("#partbutton_lplpo").fadeIn();
						var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
						$("#list_history_lplpo").load(url_hasil);
						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					})
				}
			})
		}
	})

	$("#btn_history_lplpoXXX").click(function(){

		//if $('#data_include').checked() {} else {alert()}
		var url2="manajemenlogistik/history_lplpo/input_data";
		var url3="manajemenlogistik/history_lplpo/data_inputed";
		//var url4="manajemenlogistik/history_lplpo/same_as_stok";
		var form_data = {
			kode_kec:$("#list_kec_hist_lplpo").val(),
			kode_pusk:$("#nmpuskesmas_hist").val(),
			per_lplpo:$("#periode_lplpo").val()			
		}
		//alert("cek ajak");
		$('input[name="data_include"]').attr('checked',function(){
			if($(this).is(':checked')){
				//alert("checked");
				$.ajax({
					type:"POST",
					url:url3,
					data: form_data,
					dataType: 'json',
					success:function(e){
						if(e.con==1){
							confirm(e.confirm);
						}else{
							alert(e.confirm);	
						}
						//alert("sukses tambah data");
						
						$("#form_history_lplpo_modal").slideUp("slow");
						//$("#form_history_lplpo_modal").reset();
						$("#partbutton_lplpo").fadeIn();
						//var url_hasil="manajemenlogistik/history_lplpo_obat/get_list_history_lplpo"
						var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
						$("#list_history_lplpo").load(url_hasil);//+"#list_sedian_obat");

						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					}
				});
			}else{
				//alert("not checked");
				$.ajax({
					type:"POST",
					url:url2,
					data: form_data,
					dataType: 'json',
					success:function(e){
						//$("#hasil").hide();
						//$("#list_obat_fornas").html("<h1>berhasil</h1>");
						//alert("sukses tambah data");
						alert(e.confirm);
						$("#form_history_lplpo_modal").slideUp("slow");
						//$("#form_history_lplpo_modal").reset();
						$("#partbutton_lplpo").fadeIn();
						//var url_hasil="manajemenlogistik/history_lplpo_obat/get_list_history_lplpo"
						var url_hasil="manajemenlogistik/history_lplpo/get_data_history_lplpo"
						$("#list_history_lplpo").load(url_hasil);//+"#list_sedian_obat");

						$("#list_kec_hist_lplpo").val("");
						$("#nmpuskesmas_hist").val("");
						$("#periode_lplpo").val("");
					}					
				});
			}
		}) 
		/*
		*/
	})

	//============== end submit add form
});
</script>
	<!-- bag. isi -->
		<div id="partbutton_lplpo" style="padding-top:10px;">
			<div class="col-lg-8">
				<button id="add_history_lplpo_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_history_lplpo_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_history_lplpo-btn" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
				<a href="javascript:void(0)" id="download_template_lplpo" class="btn btn-success btn-sm">Download Template LPLPO</a>
			    <a href="javascript:void(0)" id="set_value_opt" style="" class="btn btn-success btn-sm">Set Buffer Optimum Value</a>
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			    <!--a href="javascript:void(0)" id="download_template_lplpo" style="float:right;">Download Template LPLPO</a-->

			</div><!-- /col6 -->
		</div>
			<div class="" id="form_history_lplpo_modal">
				<br>
			  <h4>Tambah LPLPO</h4>
			  <form id="form_input_lplpo">
				<table class="table table-condensed">
					<tr>
						<td width="30%">Kecamatan</td>
						<td width="70%">
							<select class="required" name="kode_kec" id="list_kec_hist_lplpo" onchange="get_puskesmas(this.value);">
								<option value="">--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select class="required" name="kode_pusk" id="nmpuskesmas_hist" onchange="tambahpuskesmas(this.value);">
								<option value="">--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Bulan/Periode</td>
							<td><!--input type="hidden" value="<?php echo date('Y-m-d'); ?>" id="datenow"-->
								<div class="input-append date" id="datepicker3" data-date="2014-04-15" data-date-format="yyyy-mm" >
									<input class="span2 required" size="12" type="text" value="" readonly="readonly" id="periode_lplpo" name="per_lplpo">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>

									<script>
									$('#datepicker3').datepicker(
										{
										    format: "yyyy-mm",
										    viewMode: "months", 
										    minViewMode: "months"
										});
									</script>
							</td>
					</tr>
					<tr>
						<td colspan="2">
							<div class="input-group">
							      <span class="input-group-addon">
							        <input type="checkbox" name="data_include" id="data_include">
							      </span>
							      <input type="text" class="form-control" value="Gunakan data periode sebelumnya" size="35"readonly>
							    </div><!-- /input-group -->
						</td>
					</tr>
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_history_lplpo" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal_hist_lplpo" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
						<td colspan="3">
							
							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
			</form>
			</div>
			<!--/form-->
			<div id="list_history_lplpo" style="padding-top:10px;"></div>

<div class="modal fade" id="change_lplpo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data LPLPO</h4>
      </div>
      <div class="modal-body">
			<table class="table table-condensed">
					<tr>
						<td width="30%">Kecamatan
							<input type="hidden" id="cha_id_lplpo" name="cha_id_lplpo"></td>
						<td width="70%">
							<select class="required" name="kode_kec" id="cha_kec_lplpo" onchange="get_puskesmas(this.value);">
								<option value="">--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select class="required" name="kode_pusk" id="cha_namapusk" onchange="tambahpuskesmas(this.value);">
								<!--option value="">--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option-->
							</select>
						</td>
					</tr>
					<tr>
						<td>Bulan/Periode</td>
							<td><!--input type="hidden" value="<?php echo date('Y-m-d'); ?>" id="datenow"-->
								<div class="input-append date" id="datepicker31" data-date="2014-04-15" data-date-format="yyyy-mm" >
									<input class="span2 required" size="12" type="text" value="" readonly="readonly" id="cha_periode_lplpo" name="per_lplpo">
									<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>

									<script>
									$('#datepicker31').datepicker(
										{
										    format: "yyyy-mm",
										    viewMode: "months", 
										    minViewMode: "months"
										});
									</script>
							</td>
					</tr>
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_cha" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal_hist_lplpo"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
						<td colspan="3">
							
							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
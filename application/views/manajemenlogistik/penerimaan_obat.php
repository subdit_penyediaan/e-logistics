<style>
.datepicker{
	z-index:1151;
}
.ui-autocomplete { z-index:2147483647; }
</style>
<script>
$(document).ready(function(){
	//$("#form_penerimaan").validate();

	//============== submit add form

	$("#upt_penerimaan_obat").click(function(){
		var url2="manajemenlogistik/penerimaan_obat/update_data";
		var form_data = {
			id_faktur:$("#cha_id_faktur").val(),
			no_faktur:$("#cha_no_faktur").val(),
			periode:$("#cha_periode").val(),
			tanggal:$("#cha_tanggal").val(),
			pbf:$("#cha_pbf").val(),
			nama_dok:$("#cha_nama_dok").val(),
			no_kontrak:$("#cha_no_kontrak").val(),
			dana:$("#cha_dana").val(),
			ta:$("#cha_ta").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("berhasil update data");
				$('#change_penerimaan_obat').modal('toggle');
				var url_hasil="manajemenlogistik/penerimaan_obat/get_data_penerimaan_obat"
				$("#list_penerimaan_obat").load(url_hasil);
				$("#cha_id_faktur").val("");
				$("#cha_no_faktur").val("");
				$("#cha_periode").val("");
				$("#cha_datepicker").val("");
				$("#cha_pbf").val("");
				$("#cha_nama_dok").val("");
				$("#cha_no_kontrak").val("");
			}
		});
	})

	//============== end submit add form

	//autocomplete pbf
	$('#cha_pbf').autocomplete({
				source:'manajemenlogistik/penerimaan_obat/get_list_pbf',
				minLength:2,
			});
	//end autocomplete pbf

	//autocomplete pbf
	$('#pbf').autocomplete({
				source:'manajemenlogistik/penerimaan_obat/get_list_pbf',
				minLength:2,
				/*select:function(evt, ui)
				{
					// when a zipcode is selected, populate related fields in this form
					//this.form.city.value = ui.item.city;
					//this.form.state.value = ui.item.state;

					$('#obat_hidden').val(ui.item.kode_obat);
					//this.form.obat_hidden.value = ui.item.kode_obat;
				}*/
			});
	//end autocomplete pbf
	$('#cha_penerimaan_obat-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');

	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	var form_data = { no_faktur: id_array[0] };
	    	$.ajax({
	    		type:"POST",
	    		url:'manajemenlogistik/penerimaan_obat/datatochange/',
	    		data: form_data,
	    		dataType:'json',
	    		success:function(data) {
	    			$('#labelnofaktur').text(data[0].no_faktur);
	    			$('#cha_id_faktur').val(form_data.no_faktur);
	    			$('#cha_no_faktur').val(data[0].no_faktur);
	    			$('#cha_tanggal').val(data[0].tanggal);
	    			$('#cha_nama_dok').val(data[0].nama_dok);
	    			$('#cha_periode').val(data[0].periode);
	    			$('#cha_pbf').val(data[0].pbf);
	    			$('#cha_no_kontrak').val(data[0].no_kontrak);
	    			$( "#cha_dana" ).val(data[0].dana);
	    			$( "#cha_ta" ).val(data[0].tahun_anggaran);
	    			$('#change_penerimaan_obat').modal('show');
	    			//return false;
	    		}
	    	});

	    }
	})

	//=========== del button

	$("#del_penerimaan_obat_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array.length > 1) {alert("pilih satu saja")}
        else if(id_array.length ==1){
    		var sure=confirm("Are you sure?");
	        	if(sure){
	        		$.ajax({
			        	url: "manajemenlogistik/penerimaan_obat/delete_list",
			        	data: "kode="+id_array,
			        	type: "POST",
			        	success: function(){
			        		alert("data berhasil dihapus");
			        		var url_hasil="manajemenlogistik/penerimaan_obat/get_data_penerimaan_obat"
							$("#list_penerimaan_obat").load(url_hasil);
			        	}
			        })
	        	}

        	}
        else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_penerimaan_obat_modal').hide();
	$("#add_penerimaan_obat_btn").on("click",function (event){
		$("#form_penerimaan_obat_modal").show();
		$("#partbutton").hide();
	});
	$("#batal").on("click",function (event){
		event.preventDefault();
		$("#form_penerimaan_obat_modal").hide();
		$("#partbutton").show();
		// $("#konten").load("manajemenlogistik/penerimaan_obat");
	});
	//================ end show add form

	var url="manajemenlogistik/penerimaan_obat/get_data_penerimaan_obat";
	$('#list_penerimaan_obat').load(url);
	//============== submit add form
	$("#form_penerimaan").validate({
		submitHandler: function(form){
			var url="manajemenlogistik/penerimaan_obat/input_data";
			var data=$("#form_penerimaan").serialize();
			$.post(url,data,function(){
				alert("sukses tambah data");
				$("#form_penerimaan_obat_modal").hide();
				//$("#form_penerimaan_obat_modal").reset();
				$("#partbutton").show();
				var url_hasil="manajemenlogistik/penerimaan_obat";
				$("#konten").load(url_hasil);
				reloadLog();
			});
			return false;
		}
	});

	//============== end submit add form

});
</script>
<div class="panel panel-primary" id="halaman_penerimaan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-bookmark"></span> <b>Penerimaan Obat</b></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<div id="partbutton">
			<div class="col-lg-8 col-md-6">
				<button id="add_penerimaan_obat_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_penerimaan_obat_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button>
				<button id="cha_penerimaan_obat-btn" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			</div>
		</div>
		<div id="form_penerimaan_obat_modal">
		<h4>Form Input</h4>
			<form id="form_penerimaan">
				<table class="table">
					<tr>
						<td width="10%"><small style="color:red;"><i>*</i></small> Nomor Faktur</td>
						<td width="40%">
							<div class="col-md-12">
								<input type="text" class="form-control required" name="no_faktur" id="no_faktur">
							</div>
						</td>
						<td width="10%"><small style="color:red;"><i>*</i></small> Bulan/Periode</td>
						<td>
							<div class="col-md-5">
								<div class="input-group input-append date" id="datepicker2" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									<input class="form-control span2 required" size="12" type="text" readonly="readonly" id="periode" name="periode">
								</div>
							</div>
							<script>
							$('#datepicker2').datepicker(
								{
								    format: "yyyy-mm",
								    viewMode: "months",
								    minViewMode: "months"
								});
							</script>
						</td>
					</tr>
					<tr>
						<td><small style="color:red;"><i>*</i></small> Tanggal Faktur</td>
						<td>
							<div class="col-md-5">
								<div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									<input class="form-control span2 required" size="12" type="text" readonly="readonly" id="tanggal" name="tanggal">
								</div>
							</div>
							<script>
							$('#datepicker').datepicker();
							</script>
						</td>
						<td><small style="color:red;"><i>*</i></small>  Sumber Dana</td>
						<td>
							<div class="col-md-5">
								<?php
									$profile = getProfile();
								?>
								<select name="dana" id="dana" class="form-control required">
									<option value="">--- PILIH ---</option>
									<?php if($profile['levelinstitusi'] > 1): ?>

										<option value="APBN">APBN</option>
										<option value="APBDI">APBDI</option>
										<option value="APBDII">APBDII</option>
										<option value="DAK">DAK</option>
										<option value="Otonomi Khusus">Otonomi Khusus</option>
										<option value="Lain-lain">Lain-lain</option>
									<?php else: ?>
										<?php foreach ($kepemilikan->result() as $key): ?>
											<option value="<?= $key->nama_program?>"><?= $key->nama_program?></option>
										<?php endforeach; ?>
									<?php endif; ?>
								</select>
							</div>
							<div class="col-md-5">
								<select name="tahun_anggaran" id="tahun_anggaran" class="form-control required">
									<option value="">--- PILIH ---</option>
									<?php foreach(tahun() as $key => $value): ?>
                                        <option value="<?php echo $key; ?>">
                                             <?php echo $value; ?>
                                        </option>
                                    <?php endforeach; ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Nama PBF</td>
						<td>
							<div class="col-md-12">
								<input type="text" class="form-control" name="pbf" id="pbf">
							</div>
						</td>
						<td>Nomor Kontrak</td>
						<td>
							<div class="col-md-12">
								<input type="text" class="form-control" name="no_kontrak" id="no_kontrak">
							</div>
						</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td>
							<div class="col-md-12">
								<input type="text" class="form-control" name="nama_dok" id="nama_dok">
							</div>
						</td>
						<td></td>
						<td></td>
					</tr>
					<tr><td colspan="4"><small style="color:red;"><i>*Harus diisi</i></small> </td>
					</tr>
					<tr>
						<td colspan="4"><div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_penerimaan_obat" class="btn btn-success buttonPaging btn-sm"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>

					</tr>
				</table>
			</form>
		</div>
		<hr>
		<div id="list_penerimaan_obat"></div>
	</div>
</div>
<!-- MODAL UBAH DATA -->
<div class="modal fade" id="change_penerimaan_obat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
      </div>
      <div class="modal-body">
		<table class="table">
			<tr>
				<td>Nomor Faktur</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" name="cha_no_faktur" id="cha_no_faktur">
						<input type="hidden" class="" name="cha_id_faktur" id="cha_id_faktur">
					</div>
				</td>
				<td>Bulan/Periode</td>
				<td>
					<div class="col-md-12">
						<div class="input-group input-append date" id="datepicker4" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm" >
							<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							<input class="form-control span2 required" size="12" type="text" readonly="readonly" id="cha_periode">
						</div>
					</div>
					<!-- <div class="input-append date" id="datepicker4" data-date="2014-04-15" data-date-format="yyyy-mm" >
						<input class="span2" size="12" type="text" value="" readonly="readonly" id="cha_periode">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div> -->
					<script>
					$('#datepicker4').datepicker(
						{
						    format: "yyyy-mm",
						    viewMode: "months",
						    minViewMode: "months"
						});
					</script>
				</td>
			</tr>
			<tr>
				<td>Tanggal Faktur</td>
				<td>
					<div class="col-md-12">
						<div class="input-group input-append date" id="datepicker5" data-date="<?php echo date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
							<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							<input class="form-control span2 required" size="12" type="text" readonly="readonly" id="cha_tanggal">
						</div>
					</div>
					<!-- <div class="input-append date" id="datepicker5" data-date="2014-04-15" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" value="" readonly="readonly" id="cha_tanggal">
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div> -->
					<script>
					$('#datepicker5').datepicker();
					</script>

				</td>
				<td>Keterangan</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" name="cha_nama_dok" id="cha_nama_dok">
					</div>
				</td>
			</tr>
			<tr>
				<td>Nama PBF</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" name="cha_pbf" id="cha_pbf">
					</div>
				</td>
				<td>Nomor Kontrak</td>
				<td>
					<div class="col-md-12">
						<input type="text" class="form-control" name="cha_no_kontrak" id="cha_no_kontrak">
					</div>
				</td>
			</tr>
			<tr>
				<td>Sumber Dana</td>
				<td>
					<div class="col-md-6">
						<select name="cha_dana" id="cha_dana" class="form-control">
							<option value="">--- PILIH ---</option>
							<?php if($profile['levelinstitusi'] > 1): ?>
								<option value="APBN">APBN</option>
								<option value="APBDI">APBDI</option>
								<option value="APBDII">APBDII</option>
								<option value="DAK">DAK</option>
								<option value="Otonomi Khusus">Otonomi Khusus</option>
								<option value="Lain-lain">Lain-lain</option>
							<?php else: ?>
								<?php foreach ($kepemilikan->result() as $key): ?>
									<option value="<?= $key->nama_program?>"><?= $key->nama_program?></option>
								<?php endforeach; ?>
							<?php endif; ?>
						</select>
					</div>
					<div class="col-md-6">
						<select name="cha_ta" id="cha_ta" class="form-control">
							<option value="">---- Pilih ----</option>
							<?php foreach(tahun() as $key => $value): ?>
	                            <option value="<?php echo $key; ?>">
	                                 <?php echo $value; ?>
	                            </option>
	                        <?php endforeach; ?>
						</select>
					</div>
				</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="upt_penerimaan_obat" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
					</div>
				</td>
				<td>
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>

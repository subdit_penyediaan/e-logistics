<script type="text/javascript">
$(document).ready(function(){


	$('#form_import_lplpo').submit(function(e){
		var url_lplpo = "manajemenlogistik/lplpo";
		event.preventDefault();
		$.ajax({
			type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData (this),//$(this).serialize(),
            processData: false,
            contentType: false,
            beforeSend: function(){
	            showBusySubmit();
	        },
            success: function (data) {
                alert('DATA BERHASIL DIIMPORT');
                $("#konten").load(url_lplpo);
            }
		})
		
	});

	//============== submit add form

	$("#btn_import_lplpo").click(function(){
		
		var url25="manajemenlogistik/lplpo/getIdLplpo";
		var url26="manajemenlogistik/lplpo/update_data_import";

		var form_data = {
			kode_kec:$("#list_kec_im").val(),
			kode_pusk:$("#nmpuskesmas_im").val(),
			per_lplpo:$("#tanggal_im").val()			
		}
		//alert("cek ajak");

		$('input[name="update_data_import"]').attr('checked',function(){
			if($(this).is(':checked')){
				//alert("SELAMAT! DATA BERHASIL DIUPDATE");
				$.ajax({
				type:"POST",
				url:url26,
				data: form_data,
				dataType: 'json',
				success:function(data){
					//$("#hasil").hide();
					//$("#list_obat_fornas").html("<h1>berhasil</h1>");
					//alert("DATA TELAH DIPILIH");
					alert(data.confirm);
					}
				});	
				//*/
			}else{
				//alert("SELAMAT! DATA BERHASIL DITAMBAH");
				$.ajax({
				type:"POST",
				url:url25,
				data: form_data,
				dataType: 'json',
				success:function(data){
					//$("#hasil").hide();
					//$("#list_obat_fornas").html("<h1>berhasil</h1>");
					//alert("SELAMAT! DATA BERHASIL DITAMBAH");
					alert(data.confirm);
					}
				});	//*/
			}

		})
			
	})

	//============== end submit add form

	var url="manajemenlogistik/isian_lplpo";
	//$('#isianLPLPO').load(url);
	var bulan=<?php echo date('m');?>;
	var urlnone="manajemenlogistik/lplpo/list_kosong_by/"+bulan;
	$('#lplpo_belum_terkirim').load(urlnone)

	var url_his_lplpo="manajemenlogistik/history_lplpo";
	$('#history_lplpo').load(url_his_lplpo);	

	$("#btn_eksport").click(function(){
		//var urlprint="manajemenlogistik/lplpo/printOut";
		//window.open(urlprint);		
	})

	$("#btn_show").click(function(){
		var url2="manajemenlogistik/lplpo/search_data_by";
		// diganti url input tb_lplpo
		var form_data = {
			kec:$("#list_kec_ex").val(),
			per:$("#tanggal_ex").val(),
			nama_pusk:$("#nmpuskesmas_ex").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				$("#list_lplpo_ex").html(e);//+"#list_sedian_obat");
				
			}
		});
	})
});
//======= get puskesmas
function get_puskesmas(val) {
    
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }else{
    	//alert("cek");
    	$.get('manajemenlogistik/lplpo/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas').html(data);});
    	$.get('manajemenlogistik/lplpo/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_ex').html(data);});
    	$.get('manajemenlogistik/lplpo/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_im').html(data);});
    	$.get('manajemenlogistik/lplpo/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_hist').html(data);});
    	$.get('manajemenlogistik/lplpo/get_data_puskesmas/' + val, function(data) {$('#cha_namapusk').html(data);});
    }
}

//====== end get puskesmas
function tambahpuskesmas(val){
	if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }
}

function getTime(val){
	var url="manajemenlogistik/lplpo/list_kosong_by/"+val;

	$('#lplpo_belum_terkirim').load(url);
}
function showBusySubmit(){
        $('#up-konten').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }
</script>

<div class="panel panel-primary">
	<div class="panel-heading">LPLPO</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#history_lplpo" data-toggle="tab">Entri LPLPO</a></li> 
		  <!--li class="active"><a href="#entri" data-toggle="tab">Entri LPLPO</a></li-->
		  <li class=""><a href="#eksport" data-toggle="tab">Eksport LPLPO</a></li>
		  <li><a href="#import" data-toggle="tab">Import LPLPO</a></li>  
		  <li><a href="#notyet" data-toggle="tab">List LPLPO yang belum terkirim</a></li>  
		</ul> 
		<div class="tab-content">
			<div class="tab-pane active" id="history_lplpo"></div>
			
	  		<div class="tab-pane" id="eksport">
			<br>
			<p>Silakan klik tombol ekport untuk mengeluarkan format data stok obat dalam bentuk exel</p>
				<table class="table">
					<tr>
						<td>Kecamatan</td>
						<td>
							<select name="list_kec_ex" id="list_kec_ex" onchange="get_puskesmas(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select name="nmpuskesmas_ex" id="nmpuskesmas_ex" onchange="tambahpuskesmas(this.value);">
								<option>--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Pelaporan Bulan/Periode</td>
						<td>
							<div class="input-append date" id="datepicker" data-date="15-03-2014" data-date-format="yyyy-mm" >
								<input class="span2" size="12" type="text" readonly="readonly" name="tanggal_ex" id="tanggal_ex">
								<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
							$('#datepicker').datepicker({
								    format: "yyyy-mm",
								    viewMode: "months", 
								    minViewMode: "months"
								});
							</script>
						</td>
					</tr>
					<!--tr>
						<td colspan="2">
							<div class="input-group">
							      <span class="input-group-addon">
							        <input type="checkbox" name="data_include_ex" id="data_include_ex">
							      </span>
							      <input type="text" class="form-control" value="Gunakan data periode sebelumnya" size="35"readonly>
							    </div>
						</td>
					</tr-->
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="" id="btn_show" class="buttonPaging"><span class="glyphicon glyphicon-export"></span> Lihat</button>
							</div>
						</td>
						<td>
							
							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
				<hr>
			<!--/form-->
			
			<div id="list_lplpo_ex" class="result">
				
			</div>
			<!--button type="submit" name="Simpan" id="simpan" class="buttonPaging"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
								
		</div><!-- end div eksport -->
		<div class="tab-pane" id="import">
		<br>
			<p>Silakan klik tombol import untuk mengimport laporan LPLPO Puskesmas.</p>
				<table class="table table-condensed">
					<tr>
						<td width="30%" colspan="2">
							<div class="input-group">
						      <span class="input-group-addon">
						        <input type="checkbox" name="update_data_import" id="update_data_import">
						      </span>
						      <input type="text" class="form-control" value="Update Data" size="10" readonly>
						    </div><!-- /input-group -->
						</td>
						<td width="70%"></td>
					<tr>
					<tr>
						<td>Kecamatan</td>
						<td>
							<select name="list_kec_im" id="list_kec_im" onchange="get_puskesmas(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select name="nmpuskesmas_im" id="nmpuskesmas_im" onchange="tambahpuskesmas(this.value);">
								<option>--- PILIH ---</option>
								<option value="add">--- TAMBAH ---</option>
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Pelaporan Bulan/Periode</td>
						<td>
							<div class="input-append date" id="datepicker17" data-date="15-03-2014" data-date-format="yyyy-mm" >
								<input class="span2" size="12" type="text" readonly="readonly" id="tanggal_im">
								<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
							$('#datepicker17').datepicker(
								{
								    format: "yyyy-mm",
								    viewMode: "months", 
								    minViewMode: "months"
								});
							</script>
						</td>
					</tr>
					<tr>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_import_lplpo" class="buttonPaging"><span class="glyphicon glyphicon-ok"></span> Oke</button>
							</div>
						</td>
						<td>
							
							<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
							<input type="reset" name="Reset" id="reset" value="Batal" /-->
						</td>
					</tr>
				</table>
				<hr>
			</form>
			<fieldset><legend>Pilih File</legend> 
			<form action="<?php echo $base_url; ?>index.php/manajemenlogistik/lplpo/do_import" method="post" id="form_import_lplpo" enctype="multipart/form-data">
				<input type="file" name="file_lplpo" value="Browse">
				<br>
				<button type="submit" name="btn_import_file" id="btn_import_file" class="buttonPaging"><span class="glyphicon glyphicon-import"></span> Import</button>
			</form>
			</fieldset>
		</div>
		<!-- end div import -->
		<div class="tab-pane" id="notyet">
			<br>
			Periode:
				<!--div class="input-append date" id="datepicker7" data-date="15-03-2014" data-date-format="yyyy-mm" >
					<input class="span2" size="12" type="text" readonly="readonly" name="tanggal_ex" id="tanggal_ex">
					<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
				</div-->
				<select name="periode_kosong" id="periode_kosong" onchange="getTime(this.value);">
					<option value="1">Januari</option>
					<option value="2">Februari</option>
					<option value="3">Maret</option>
					<option value="4">April</option>
					<option value="5">Mei</option>
					<option value="6">Juni</option>
					<option value="7">Juli</option>
					<option value="8">Agustus</option>
					<option value="9">September</option>
					<option value="10">Oktober</option>
					<option value="11">November</option>
					<option value="12">Desember</option>
				</select>
			<div id="lplpo_belum_terkirim"></div>
		</div><!-- end div import -->
	</div><!-- end div panel content -->
</div><!-- end div panel -->
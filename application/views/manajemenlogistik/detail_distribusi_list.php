<style type="text/css">
.ui-autocomplete-loading { 
 		background:url('../img/loading81.gif') no-repeat right center;
 		background-size: 32px 32px;
 	}
</style>
<script type="text/javascript">
	$(function(){
		$('.btn_delete').click(function(){
			//kode_obat = $(this).parent().parent().find('.kode_obat').val();
			jml_awal = $(this).parent().parent().find('.jml_awal').val();
			//jml_beri_new = $(this).parent().parent().find('.jml_beri_new').val();
			//no_batch = $(this).parent().parent().find('.no_batch').val();
			batch_awal = $(this).parent().parent().find('.batch_awal').val();
			id_stok = $(this).parent().parent().find('.id_stok_hidden').val();//obat pemberian
			id_dd = $(this).parent().parent().find('.id_dd').val();
			//id_dl = $(this).parent().parent().find('.id_dl').val();//id_detail_lplpo_kel

			datanew = "dt[jml_awal]="+jml_awal+"&dt[id_dd]="+id_dd+"&dt[batch_awal]="+batch_awal+"&dt[id_stok]="+id_stok;

			url='manajemenlogistik/detail_distribusi/delete_cito';

			var sure=confirm("Apakah anda yakin?");
			if(sure){
				$.post(url,datanew,function(){
					//if not success belum ada
					reloadTable();
				})	
			}			
		})

		$('.obat_pemberian').autocomplete({
			source:'manajemenlogistik/detail_distribusi/get_list_obat2', 
			minLength:2,
			focus: function( event, ui ) {
		        $( ".obat_pemberian" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{
				$('.kode_obat').val(ui.item.kode_obat);
				$('.no_batch').val(ui.item.nobatch);
				//$('#label_batch_1').text(ui.item.nobatch);
				$('.jml_beri_new').focus();
				$('.id_stok_hidden').val(ui.item.id_stok);
				return false;
			}
		})
			.each(function(){
				$(this).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		      return $( "<li>" )
		        .append("<a>"+ item.value +"<br><small>Stok: <i>"+ item.stok +"</i><br>Expired: "+item.ed+"<br>Produsen: "+item.produsen+"<br>Sumber dana: "+item.sumber_dana+"</small></a>")
		        .appendTo( ul );
		    };
			});

		//end autocomplete	

		$('.btn_save').click(function(){
			kode_obat = $(this).parent().parent().find('.kode_obat').val();
			jml_awal = $(this).parent().parent().find('.jml_awal').val();
			jml_beri_new = $(this).parent().parent().find('.jml_beri_new').val();
			no_batch = $(this).parent().parent().find('.no_batch').val();
			batch_awal = $(this).parent().parent().find('.batch_awal').val();
			id_stok = $(this).parent().parent().find('.id_stok_hidden').val();
			id_dd = $(this).parent().parent().find('.id_dd').val();
			//id_dl = $(this).parent().parent().find('.id_dl').val();//id_detail_lplpo_kel

			datanew = "dt[kode_obat]="+kode_obat+"&dt[jml_awal]="+jml_awal+
				"&dt[jml_beri_new]="+jml_beri_new+"&dt[no_batch]="+no_batch+
				"&dt[id_stok]="+id_stok+"&dt[id_dd]="+id_dd+"&dt[batch_awal]="+batch_awal;//+"&dt[id_dl]="+id_dl;

			url='manajemenlogistik/detail_distribusi/update_lplpodist';

			$.post(url,datanew,function(result){				
				var response = jQuery.parseJSON(result); 				
				if(response.status == '1'){
					reloadTable();	
				}
				alert(response.message);
			})
			//$(this).parent().parent().find('.editable').hide();
			//$(this).parent().parent().find('.show_t').show();
			//$('.btn_ge').show();
			//return false;
		})

		$('.btn_edit').click(function(){
			$(this).parent().parent().find('.show_t').hide();
			$(this).parent().parent().find('.editable').show();
			//$(this).parent().find('.btn_ge').hide();
			//$('.editable').show();
			$('.btn_ge').hide();
			return false;
		})

		$('.editable').hide();

		$("#print_btn").click(function(){
			var urlprint="manajemenlogistik/detail_distribusi/printOut/<?php echo $id; ?>";
			window.open(urlprint);		
		})

		$("#pagelistlplpo_list").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_lplpo_list').load(url);
		});

		$('#form_detail_lplpo').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            success: function (data) {
	                alert('DATA BERHASIL DIUPDATE');
	            }
			})
			
		});
	});

</script>
<div id="pagelistlplpo_list">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Obat Pemberian</th>
			<th>Satuan</th>
			<th>Jumlah Pemberian</th>
			<th>No. Batch</th>
			<th>Sumber Anggaran</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php $i=0; foreach ($result as $rows): ?>
				<?php $j=$i+1; ?>
				<tr style="">
					<td><?= $j ?></td>
					<td>
						<input type="text" class="editable obat_pemberian" value="<?= $rows->obat_pemberian ?>" size="50"><div class="show_t"><?= $rows->obat_pemberian ?></div>
						<input type="hidden" class="kode_obat" value="<?= $rows->kode_obat_res ?>">
						<input type="hidden" class="id_stok_hidden" value="<?= $rows->id_stok ?>">
						<input type="hidden" class="id_dd" value="<?= $rows->id ?>">						
					</td>
					<td><?= $rows->deskripsi ?></td>
					<td>
						<div class="show_t"><?= $rows->pemberian ?></div>
						<input type="text" class="editable jml_beri_new" value="<?= $rows->pemberian ?>" size="3">
						<input type="hidden" class="jml_awal" value="<?= $rows->pemberian ?>">
					</td>
					<td>
						<div class="show_t"><?= $rows->no_batch ?></div>
						<input type="text" class="editable no_batch" value="<?= $rows->no_batch ?>" size="6" readonly>						
						<input type="hidden" class="batch_awal" value="<?= $rows->no_batch ?>">
					</td>
					<td>
						<div class="show_t"><?= $rows->dana ?> <?= $rows->tahun_anggaran ?></div>
						<input type="text" class="editable" value="<?= $rows->dana ?> <?= $rows->tahun_anggaran ?>" size="30" readonly>
					</td>
					<td>
						<button class="btn_edit show_t btn_ge"><span class="glyphicon glyphicon-pencil"></span></button>
						&nbsp<button class="btn_delete show_t btn_ge btn-danger"><span class="glyphicon glyphicon-remove"></span></button>
						<button class="btn_save editable btn-success"><span class="glyphicon glyphicon-ok"></span></button>
					</td>
				</tr>
			<?php $i++; endforeach; ?>
		</tbody>
	</table>
	<button id="print_btn" style="float:right;" class="btn-primary"><span class="glyphicon glyphicon-print"></span> Print SBBK</button>
</div>
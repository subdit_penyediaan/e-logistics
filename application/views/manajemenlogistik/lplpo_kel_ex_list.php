<script type="text/javascript">
	$(function(){
		$("#pagelistlplpo_ex_list").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_lplpo_ex_list').load(url);
		})

		$("#btn_export2").click(function(){
		var url2="manajemenlogistik/lplpo/printOut";
		var form_data = {
			kec:$("#kec").val(),
			per:$("#per").val(),
			nama_pusk:$("#nama_pusk").val()
			//dana:$("#dana").val()
		}
			$.get('manajemenlogistik/lplpo_kel/printOut', 
				function(form_data,e) {
					window.print(e);
				});
		})

		//eksport button
	$("#btn_export").click(function(){
		var urlprint="manajemenlogistik/lplpo_kel/printOut";
		window.open(urlprint);		
	})
	//end eksport button
});
function printexcel(){
	window.open()
}
</script>
<div id="pagelistlplpo_ex_list">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<center>
		<button type="submit" name="" onclick="" id="btn_export" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-export"></span> Eksport ke CSV</button>
	</center>	
	<br>
	<input type="hidden" id="kec" value="<?php //echo $kec; ?>">
	<input type="hidden" id="nama_pusk" value="<?php //echo $nama_pusk; ?>">
	<input type="hidden" id="per" value="<?php //echo $per; ?>">

	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">		
			<th>No.</th>
			<th>Kode</th>
			<th>Nama Obat</th>
			<th>Satuan</th>
			<th>Stok Awal</th>
			<th>Penerimaan</th>
			<th>Persediaan</th>
			<th>Pemakaian</th>
			<th>Rusak/ED</th>
			<th>Sisa Stok</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Pemberian</th>
			<th>Keterangan</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="">
					<td>'.$i.'</td>
					<td>'.$rows->kode_generik.'</td>
					<td>'.$rows->nama_obj.'</td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok_awal.'</td>
					<td>'.$rows->terima.'</td>
					<td>'.$rows->sedia.'</td>
					<td>'.$rows->pakai.'</td>
					<td>'.$rows->rusak_ed.'</td>
					<td>'.$rows->stok_akhir.'</td>
					<td>'.$rows->stok_opt.'</td>
					<td>'.$rows->permintaan.'</td>
					<td>'.$rows->pemberian.'</td>
					<td>'.$rows->ket.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
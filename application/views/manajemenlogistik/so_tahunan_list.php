<script type="text/javascript">
	$(function(){
		$('.delete-btn').on("click",function(){
			event.preventDefault();
			var url = $(this).attr('href');
			var konfirmasi = confirm("Apakah Anda akan menghapus data ini?");
			if(konfirmasi){
				$.get(url,function(data){
					var obj = jQuery.parseJSON(data);			
					if(obj.confirm==1){
						alert("Data berhasil dihapus");
					}else{
						alert("Gagal hapus data");
					}
					reload_sot();
				})
			}			
			//alert(url);
		})
		// $("#so_page").on("click","a",function (event){
		// 	event.preventDefault();
		// 	var url = $(this).attr("href");
		// 	//$("#pagin").load(url);
		// 	$('#list_penerimaan_obat').load(url);
		// });
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
	.btn_action{
		text-align: center;	
	}
</style>
<div id="so_page" class="col-md-6">
<!--/div-->
	<!--div id="pagin"-->
	<br>
	<table class="table table-striped table-bordered">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Tahun</th>
			<th>Tanggal Pembuatan</th>			
			<th>Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($query->result() as $rows) {
					//$rows->no_faktur
					echo '<tr style="cursor:pointer;">										
					<td>'.$i.'</td>
					<td>'.substr($rows->Name, 14).'</td>
					<td>'.$rows->Create_time.'</td>					
					<td class="btn_action">
						<button class="btn btn-default btn-sm" href="'.$base_url.'index.php/manajemenlogistik/stok_opnam/get_detail/'.$rows->Name.'"><span class="glyphicon glyphicon-list"></span> Lihat</button>
						<a class="btn btn-danger btn-sm delete-btn" href="'.$base_url.'index.php/manajemenlogistik/stok_opnam/delete_data/'.$rows->Name.'"><span class="glyphicon glyphicon-remove"></span> Hapus</a>
					</td>
					</tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
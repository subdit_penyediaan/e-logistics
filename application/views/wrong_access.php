<div class="panel panel-primary" id="halaman_manajemen_user">
	<div class="panel-heading"><span class="glyphicon glyphicon-warning-sign"></span> Warning!</div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<div class="alert alert-danger">
			<b>Wrong Access!</b><p id="message-alert">Maaf, hak akses Anda salah</p>
		</div>
	</div>
</div>
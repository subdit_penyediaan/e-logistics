<script type="text/javascript">
	$(function(){
		$("#pagelistlplpo_ex_list").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_lplpo_ex_list').load(url);
		})

		$("#btn_export2").click(function(){
		var url2="laporan/lplpo/printOut";
		var form_data = {
			kec:$("#kec").val(),
			per:$("#per").val(),
			nama_pusk:$("#nama_pusk").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		/*	$.get({
				//type:"POST",
				url:url2,
				data: form_data,
				success:function(e){
					//$("#list_lplpo_ex").html(e);//+"#list_sedian_obat");
					//var urlprint="laporan/lplpo/printOut";
					window.open(e);
					
				}
			});*/
		$.get('laporan/lplpo/printOut', 
			function(form_data,e) {
				window.print(e);
			});
		})

		//eksport button
		$("#btn_eksport").click(function(){
			var urlprint="laporan/lap_lplpo_kel/printOut";
			window.open(urlprint);		
		})
		//end eksport button
});
function printexcel(){
	window.open()
}
</script>
<div id="pagelistlplpo_ex_list">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-condensed">
		<thead>
		<tr>		
			<th>Kode Obat</th>
			<th>Nama Obat</th>
			<th>Satuan</th>
			<th>Stok Awal</th>
			<th>Penerimaan</th>
			<th>Persediaan</th>
			<th>Pemakaian</th>
			<th>Rusak/ED</th>
			<th>Sisa Stok</th>
			<th>Stok Optimum</th>
			<th>Permintaan</th>
			<th>Pemberian</th>
			<th>Keterangan</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					echo '<tr style="">
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->nama_obj.'</td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok_awal.'</td>
					<td>'.$rows->terima.'</td>
					<td>'.$rows->sedia.'</td>
					<td>'.$rows->pakai.'</td>
					<td>'.$rows->rusak_ed.'</td>
					<td>'.$rows->stok_akhir.'</td>
					<td>'.$rows->stok_opt.'</td>
					<td>'.$rows->permintaan.'</td>
					<td>'.$rows->pemberian.'</td>
					<td>'.$rows->ket.'</td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
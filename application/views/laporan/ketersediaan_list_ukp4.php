<style type="text/css">
	.dataTables_filter{
		float:right;
	}	
	.pagination{
		float:right;
	}
</style>
<script type="text/javascript">
	$(function(){
		$("#pagelistukp4").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_ukp4').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistukp4">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-bordered">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>ID UKP4</th>
			<th>Nama Kelompok</th>
			<th>Jumlah Kebutuhan Tahun</th>
			<th>Total Penggunaan</th>
			<th>Sisa Stok</th>
			<th>Jumlah Obat dan Vaksin</th>
			<th>Ketersediaan (%)</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					/*$rata=$rows->jumlah_penggunaan/$rows->cur_month+0.01;
					if($rata<1){
						$ketersediaan='tak berhingga';
						echo '<tr class="info">';
					}else{
						$ketersediaan = floor($rows->jumlah_stok/$rata);

						if ($ketersediaan >= 6){
							echo '<tr class="info">';	
						}elseif (($ketersediaan < 6) && ($ketersediaan >= 4)){
							echo '<tr class="warning">';
						}else {
							echo '<tr class="danger">';
						};
					}*/
					$jumlah=$rows->pemberian+$rows->stok;
					$ketersediaan=round(($jumlah/($rows->perencanaan+0.1))*100,2);
					echo '<tr class="info"><td>'.$i.'</td>
					<td>'.$rows->id_ukp4.'</td>
					<td>'.$rows->nama_ukp4.'</td>
					<td>'.$rows->perencanaan.'</td>
					<td>'.$rows->pemberian.'</td>
					<td>'.$rows->stok.'</td>
					<td>'.$jumlah.'</td>
					<td>'.$ketersediaan.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
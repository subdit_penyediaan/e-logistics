 <style>

</style>
<script type="text/javascript">
$(document).ready(function(){
//============== submit add form

	$('#key_faktur').autocomplete({
		// autoFocus: true,
		source:'laporan/penerimaan/getFaktur',
		minLength:2,
		focus: function( event, ui ) {
	        $( "#key_faktur").val( ui.item.no_faktur);
	      },
	    select:function(evt2, ui2)
		{
			$("#key_faktur").val( ui2.item.no_faktur);
			$('#id_faktur').val(ui2.item.id);
			return false;
		}
	  }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	      	.data( "ui-autocomplete-item", item )
	        .append("<a>"+ item.no_faktur +"<br><small><b>PBF/Tahun Anggaran: </b>" + item.pbf + "/"+ item.anggaran + " "+ item.tahun +"</small></a>")
	        .appendTo( ul );
	};

	$("#btn_show").click(function(){
		var url2="<?= base_url(); ?>laporan/penerimaan/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			tahun_anggaran:$("#tahun-anggaran").val(),
			kategori:$("#kategori").val(),
			nama:$("#key_name").val(),
			id_faktur:$("#id_faktur").val(),
      program:$("#program").val()
		}

		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){

				$("#list_penerimaan").html(e);//+"#list_sedian_obat");

			}
		});
	})

	//============== end submit add form

});
function databydana(val){
	var url="<?= base_url(); ?>laporan/penerimaan/get_data_by/"+val;
	$('#list_penerimaan').load(url);
}
</script>
<div class="panel panel-primary" id="halaman_penerimaan">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan Penerimaan</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
				<table class="table">
						<tr>
							<td width="10%">Kategori</td>
							<td width="40%">
								<div class="col-md-12">
									<select name="kategori_objek" id="kategori_objek" class="form-control">
										<option value="all">---- Semua ----</option>
										<option value="obat">Obat</option>
										<option value="bmhp">Bmhp</option>
										<option value="alkes">Alkes</option>
									</select>
								</div>
							</td>
							<td width="10%">Nama Sediaan</td>
							<td width="40%">
								<div class="col-md-12">
									<input type="text" class="form-control" name="key_name" id="key_name">
								</div>
							</td>
						</tr>
						<tr>
							<td>Sumber Dana</td>
							<td>
								<div class="col-md-5">
									<select name="dana" id="dana" onchange="" class="form-control">
										<option value="all">---- Semua ----</option>
										<option value="APBN">APBN</option>
										<option value="APBDI">APBDI</option>
										<option value="APBDII">APBDII</option>
										<option value="DAK">DAK</option>
										<option value="Otonomi Khusus">Otonomi Khusus</option>
										<option value="Lain-lain">Lain-lain</option>
									</select>
								</div>
								<div class="col-md-5">
									<select name="tahun_anggaran" id="tahun-anggaran" class="form-control">
										<option value="all">---- Semua ----</option>
										<?php foreach(tahun() as $key => $value): ?>
                                            <option value="<?php echo $key; ?>">
                                                 <?php echo $value; ?>
                                            </option>
                                        <?php endforeach; ?>
									</select>
								</div>
							</td>
							<td>No. Faktur</td>
							<td>
								<div class="col-md-12">
									<input type="text" class="form-control" name="key_faktur" id="key_faktur">
									<input type="hidden" class="form-control" name="id_faktur" id="id_faktur">
								</div>
							</td>
						</tr>
						<tr>
							<td>Kategori Obat</td>
							<td>
								<div class="col-md-12">
									<select name="kategori" id="kategori" class="form-control">
										<option value="all">---- Semua ----</option>
										<option value="generik">Generik</option>
										<option value="generik bermerek">Generik Bermerek</option>
										<option value="paten">Paten</option>
									</select>
								</div>
							</td>
							<td>Periode</td>
							<td>
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d')?>" data-date-format="yyyy-mm-dd" >
										<input class="form-control span2" size="12" type="text" value="<?php echo date('Y');?>-01-01" id="periodeawal" readonly="readonly" >
										<span class="input-group-addon add-on" style="cursor:pointer;">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
								</div>
								<div class="col-md-1"><label>-</label></div>
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker2" data-date="2014-04-15" data-date-format="yyyy-mm-dd" >
										<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>"  id="periodeakhir" readonly="readonly">
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
									<script>
										$('#datepicker2').datepicker();
										$('#datepicker').datepicker();
										/*{
										    format: "yyyy-mm",
										    viewMode: "months",
										    minViewMode: "months"
										});*/
									</script>
								</div>
							</td>

						</tr>
            <tr>
                <td>Program</td>
                <td>
                  <div class="col-md-12">
                    <select name="program" id="program" class="form-control">
                      <option value="all">---- Semua ----</option>
                      <?php foreach ($program->result() as $key) : ?>
                        <option value="<?= $key->id?>"><?= $key->nama_program ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </td>
                <td colspan="2"></td>
            </tr>
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<!--button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
								</div>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_penerimaan"></div>
	</div>
</div>

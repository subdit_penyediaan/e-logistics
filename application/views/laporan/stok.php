<style>
/*@import url('../css/progressbar.css');
.container2 {
    margin-top: 30px;
    width: 400px;
}
@import url('../css/bootstrap.min.css');*/
.progress {
    margin-top: 30px;
    width: 500px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#datepicker2').datepicker();

	var url="laporan/stok/search_data_generik";
	$('#list_inn').load(url);

	$('.progress').hide();
	//load kelompok fornas
	var url_kel_fornas="laporan/stok/obat_fornas";
	//$('#fornas').load(url_kel_fornas);

	var url="laporan/stok/get_data_stok";
	$("#btn_show").click(function(){
		var progress = setInterval(function () {
		    var $bar = $('.bar-stok.progress-bar');

		    if ($bar.width() >= 480) { //panjang progress bar
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5); //penambahan tiap satuan waktu
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 1234); //satuan waktu
		$('#stok_progress').show();
		var url2="laporan/stok/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			tahun_anggaran:$("#tahun-anggaran").val(),
			kategori:$("#kategori").val(),
			jprogram:$("#jprogram").val(),
			nama:$("#key_name").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				$("#list_stok").html(e);

			}
		});
	})
	$("#btn_show_fornas").click(function(){
		var progress = setInterval(function () {
		    var $bar = $('.bar-fornas.progress-bar');

		    if ($bar.width() >= 480) { //panjang progress bar
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5); //penambahan tiap satuan waktu
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 2345); //satuan waktu
		$('#fornas_progress').show();
		var url2f="laporan/stok/obat_fornas";
		var form_data = {
			awal:$("#awal_fornas").val(),
			akhir:$("#akhir_fornas").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2f,
			data: form_data,
			success:function(e){
				$("#list_stok_fornas").html(e);

			}
		});
	})
	//============== end submit add form

	/*$('#progressbar').progressbar({
			    warningMarker: 60,
			    dangerMarker: 80,
			    maximum: 100,
			    step: 5
			});
	*/
	//================================
	// var progress = setInterval(function () {
	//     var $bar = $('.progress-bar');

	//     if ($bar.width() >= 480) { //panjang progress bar
	//         clearInterval(progress);
	//         $('.progress').removeClass('active');
	//     } else {
	//         $bar.width($bar.width() + 5); //penambahan tiap satuan waktu
	//     }
	//     $bar.text(Math.round($bar.width() / 5) + "%");
	// }, 2345); //satuan waktu

});
</script>
<div class="panel panel-primary" id="halaman_stok">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan Stok</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#obat_all" data-toggle="tab">Semua Obat</a></li>
			<li><a href="#fornas" data-toggle="tab">Kelompok Fornas</a></li>
			<li><a href="#stok_generik" data-toggle="tab">Kelompok INN</a></li>
			<!--li><a href="#ukp4" data-toggle="tab">Kelompok Ukp4</a></li-->
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="obat_all">
			<br><br>
			<div class="" id="">
				<table class="table">
						<tr>
							<td width="10%" colspan="">Jenis Program</td>
							<td width="40%" colspan=""><!--select name="kategori_objek" id="kategori_objek">
									<option value="all">----- Semua -----</option>
									<option value="obat">Obat</option>
									<option value="bmphp">Bmhp</option>
									<option value="alkes">Alkes</option>
								</select-->
								<div class="col-md-12">
									<select name="jprogram" id="jprogram" class="form-control">
										<option value="all">----- Semua -----</option>
										<?php foreach ($program->result() as $key):?>
										<option value="<?= $key->id?>"><?= $key->nama_program?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</td>
							<td>Sumber Dana</td>
							<td>
								<div class="col-md-5">
									<select name="dana" id="dana" onchange="" class="form-control">
										<option value="all">----- Semua -----</option>
										<?php
											$profile = getProfile();
										?>
										<?php if($profile['levelinstitusi'] > 1): ?>

											<option value="APBN">APBN</option>
											<option value="APBDI">APBDI</option>
											<option value="APBDII">APBDII</option>
											<option value="DAK">DAK</option>
											<option value="Otonomi Khusus">Otonomi Khusus</option>
											<option value="Lain-lain">Lain-lain</option>
										<?php else: ?>
											<?php foreach ($program->result() as $key): ?>
												<option value="<?= $key->nama_program?>"><?= $key->nama_program?></option>
											<?php endforeach; ?>
										<?php endif; ?>
									</select>
								</div>
								<div class="col-md-5">
									<select name="tahun_anggaran" id="tahun-anggaran" class="form-control">
										<option value="all">---- Semua ----</option>
										<?php foreach(tahun() as $key => $value): ?>
                                            <option value="<?php echo $key; ?>">
                                                 <?php echo $value; ?>
                                            </option>
                                        <?php endforeach; ?>
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td>Periode Akhir</td>
							<td>
								<input class="" size="12" type="hidden" value="2013-01-01" id="periodeawal">
								<input class="" size="12" type="hidden" value="all" id="kategori">
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker2" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
										<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>"  id="periodeakhir" readonly="readonly">
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
								</div>
							</td>
							<td>Nama Sediaan</td>
							<td>
								<div class="col-md-12">
									<input type="text" class="form-control" name="key_name" id="key_name">
								</div>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<!--button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
								</div>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_stok">
				<div id="stok_progress" class="progress">
				  <div class="bar-stok progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">

				  </div>
				</div>
			</div>
		</div><!-- end div id=obat all-->
		<div class="tab-pane" id="fornas">
		<br><br>
		<table class="table">
			<tr>
				<td>Dari tanggal</td>
				<td>
					<div class="input-append date" id="datepicker75" data-date="<?php echo date('Y-m-d');?>;?>" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" value="<?php echo date('Y-m-d');?>" id="awal_fornas" readonly="readonly" >
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<script>
					$('#datepicker75').datepicker();
					</script>
				</td>
			</tr>
			<tr>
				<td>Sampai tanggal</td>
				<td>
					<div class="input-append date" id="datepicker76" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
						<input class="span2" size="12" type="text" value="<?php echo date('Y-m-d');?>" id="akhir_fornas" readonly="readonly" >
						<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
					</div>

					<script>
					$('#datepicker76').datepicker();
					</script>
				</td>
			</tr>
		</table>
			<button type="submit" name="Simpan" id="btn_show_fornas" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
			<hr>
			<div id="list_stok_fornas">
				<div id="fornas_progress" class="progress">
				  <div class="bar-fornas progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">

				  </div>
				</div>
			</div>
		</div><!-- end div id=fornas-->
		<div class="tab-pane" id="stok_generik">
			<center>
				<h3>Laporan Stok Sediaan INN/FDA</h3>
				<h4>per <?php echo date('Y-m-d');?></h4>
				<br>
			</center>
			<div id="list_inn"></div>
		</div><!-- end div id=generik-->
	</div>
</div>

<style type="text/css">
	.span_search{
		width: 400px;
		padding: 10px;
		border: 1px solid #428bca;
		margin: 5px;
		background-color: #428bca;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('#key_generik').autocomplete({
				source:'laporan/kartu_stok/search',
				minLength:2,
			});

		$('#key_detail').autocomplete({
			source:'laporan/kartu_stok/get_list_obat',
			minLength:2,
			focus: function( event, ui ) {
		        $( "#key_detail" ).val( ui.item.value );
		        return false;
		      },
			select:function(evt, ui)
			{
				$('#kode_obat').val(ui.item.id_obat);
				$('#kode-batch').val(ui.item.nobatch);
				return false;
			}
		})
		.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
	      return $( "<li>" )
	        .append("<a>"+ item.value +"<br><small>Batch: <b>"+item.nobatch+"</b></small></a>")
	        .appendTo( ul );
	    };
		// $('#btn_srch2').click(function(){
		// 	var url_search='laporan/kartu_stok/search';
		// 	var format_data={key:$('#key_generik').val()}
		// 	$.ajax({
		// 		type:"POST",
		// 		url:url_search,
		// 		data:format_data,
		// 		success:function(data){
		// 			$('#list_nama_generik').html(data);
		// 		}
		// 	});
		// })

		//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                /*if(data.con==0){
	                	alert(data.confirm);
	                }else{
	                	alert(data.confirm);
		                var url='manajemenlogistik/stok_opnam';
		                $('#konten').load(url);
	                }*/
	                $('#list_nama_generik').html(data);
	            }
			})

		});
		//end form search

		$('#form_search_detail').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            success: function (data) {
	                $('#list_nama_detail').html(data);
	            }
			})

		});
	})
</script>
<div class="panel panel-primary" id="halaman_ketersediaan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Kartu Stok</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#search" data-toggle="tab">Nama INN (Generik)</a></li>
			<li><a href="#nama_dagang" data-toggle="tab">Nama Dagang</a></li>
			<li><a href="#list" data-toggle="tab">Daftar Obat Kelompok Generik</a></li>
			<!--li><a href="#ukp4" data-toggle="tab">Kelompok Ukp4</a></li-->
		</ul>
		<div class="tab-content">
		<br>
			<div class="tab-pane active" id="search">
				<label>Cari berdasarkan nama generik:</label>
				<div class="span_search">
				<form action="<?php echo $base_url; ?>index.php/laporan/kartu_stok/get_detail" method="post" id="form_search">
				 <label style="color:white;">Sumber Dana: </label>
				 <select name="sumber_dana" id="sumber_dana">
				 	<option value="all">---- Semua ----</option>
					<option value="APBN">APBN</option>
					<option value="APBDI">APBDI</option>
					<option value="APBDII">APBDII</option>
					<option value="DAK">DAK</option>
					<option value="Otonomi Khusus">Otonomi Khusus</option>
					<option value="APBDII Obat">APBDII Obat</option>
					<option value="APBDII BMHP">APBDII BMHP</option>
					<option value="APBDII Kimia">APBDII Kimia</option>
					<option value="Hibah">Hibah</option>
					<option value="Donasi">Donasi</option>
					<option value="Lain-lain">Lain-lain</option>
				 </select>
					<div class="input-group">
				      <input type="text" class="form-control" name="key_generik" id="key_generik">
				      <span class="input-group-btn">
				        <button class="btn btn-warning" type="submit" id="btn_srch">Go!</button>
				      </span>
				    </div>

				</form>
				</div>
				<div id="list_nama_generik"></div>
			</div><!-- end div tab search -->
			<div class="tab-pane" id="nama_dagang">
				<?php //$this->load->view('manajemenlogistik/kartu_stok_detail')?>
				<label>Cari berdasarkan nama Paten/Brand/Dagang:</label>
				<div class="span_search">
				<form action="<?php echo $base_url; ?>index.php/laporan/kartu_stok/get_detail_detail" method="post" id="form_search_detail">
						<table>
								<tr>
										<td><label style="color:white;">Sumber Dana: </label></td>
										<td>
											<select name="sumber_dana_detail" id="sumber_dana_detail">
					 						 	<option value="all">---- Semua ----</option>
					 							<option value="APBN">APBN</option>
					 							<option value="APBDI">APBDI</option>
					 							<option value="APBDII">APBDII</option>
					 							<option value="DAK">DAK</option>
												<option value="Otonomi Khusus">Otonomi Khusus</option>
					 							<option value="Lain-lain">Lain-lain</option>
					 					 </select>
									 </td>
								</tr>
								<tr>
										<td><label style="color:white;">Item: </label></td>
										<td>
												<select name="batch" id="batch">
														<option value="all">---- Semua ----</option>
														<option value="batch">per batch</option>
											 	</select>
										</td>
								</tr>
						</table>
						<div class="input-group">
				      <input type="text" class="form-control" name="key_detail" id="key_detail">
				      <input type="hidden" class="" name="kode_obat" id="kode_obat">
							<input type="hidden" class="" name="kode_batch" id="kode-batch">
				      <span class="input-group-btn">
				        <button class="btn btn-warning" type="submit" id="btn_srch_detail">Go!</button>
				      </span>
				    </div>
				</form>
				</div>
				<div id="list_nama_detail"></div>
			</div>
			<div class="tab-pane" id="list">
				<br>
				<table class="table">
					<thead>
					<tr class="active">
						<th>No.</th>
						<th>Kode</th>
						<th>Nama Kelompok Generik</th>
						<th>Sediaan</th>
					</tr>
					</thead>
					<tbody id="trcontent">
						<?php
							$i=1;
							foreach ($result as $rows) {
								echo '<tr>
								<td>'.$i.'</td>
								<td>'.$rows->kode_generik.'</td>
								<td>'.$rows->nama_objek.'</td>
								<td>'.$rows->sediaan.'</td></tr>';
								$i++;
							}
						?>
					</tbody>
				</table>
			</div><!-- end div list -->
		</div><!-- end div tab conten -->
	</div><!-- end div panel conten -->
</div><!-- end div panel -->

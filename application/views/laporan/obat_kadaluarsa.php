 <style>

</style>
<script>
$(document).ready(function(){

	var url="laporan/obat_kadaluarsa/get_data_obat_kadaluarsa";
	//$('#list_obat_kadaluarsa').load(url);
	//============== submit add form

	//eksport button
	// $("#btn_eksport").click(function(){
	// 	var urlprint="laporan/obat_kadaluarsa/printOut";
	// 	window.open(urlprint);
	// })
	//end eksport button
	$("#btn_show").click(function(){
		var url2="laporan/obat_kadaluarsa/search_data_by";
		var form_data = {
			bulan_ed:$("#bulan_ed").val(),
			tahun_ed:$("#tahun_ed").val(),
			dana:$("#dana").val(),
			tahun_anggaran:$("#tahun-anggaran").val(),
			program:$("#program").val(),
			nama:$("#key_name").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				$('#list_obat_kadaluarsa').html(e);
			}
		});
	})

	//============== end submit add form

});
</script>
<div class="panel panel-primary" id="halaman_obat_kadaluarsa">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan Obat Kadaluarsa</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
				<table class="table">
						<tr>
							<td width="10%">Tahun Kadaluarsa</td>
							<td width="40%">
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker" data-date="15-03-2014" data-date-format="yyyy" >
										<input class="form-control span2" size="12" type="text" readonly="readonly" name="tahun_ed" id="tahun_ed" value="">
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
								</div>
								<script>
								$('#datepicker').datepicker({
									    format: " yyyy",
									    viewMode: "years",
									    minViewMode: "years"
									});
								</script>
							</td>
							<td width="10%">Kategori</td>
							<td width="40%">
								<div class="col-md-12">
									<select name="kategori_objek" id="kategori_objek" class="form-control">
										<option value="all">---- Semua ----</option>
										<option value="obat">- Obat</option>
										<option value="bmhp">- Bmhp</option>
										<option value="alkes">- Alkes</option>
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td>Bulan Kadaluarsa</td>
							<td>
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker_bln" data-date="15-03-2014" data-date-format="mm" >
										<input class="form-control span2" size="12" type="text" readonly="readonly" name="bulan_ed" id="bulan_ed" value="">
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
								</div>
								<script>
								$('#datepicker_bln').datepicker({
									    format: " mm",
									    viewMode: "months",
									    minViewMode: "months"
									});
								</script>
							</td>
							<td>Nama Sediaan</td>
							<td>
								<div class="col-md-12">
									<input type="text" class="form-control" name="key_name" id="key_name">
								</div>
							</td>
						</tr>
						<tr>
							<td>Sumber Dana</td>
							<td>
								<div class="col-md-5">
									<select name="dana" id="dana" class="form-control">
										<option value="all">---- Semua ----</option>
										<option value="APBN">APBN</option>
										<option value="APBDI">APBDI</option>
										<option value="APBDII">APBDII</option>
										<option value="DAK">DAK</option>
										<option value="Otonomi Khusus">Otonomi Khusus</option>
										<option value="Lain-lain">Lain-lain</option>
									</select>
								</div>
								<div class="col-md-5">
									<select name="tahun_anggaran" id="tahun-anggaran" class="form-control">
										<option value="all">---- Semua ----</option>
										<?php foreach(tahun() as $key => $value): ?>
                                            <option value="<?php echo $key; ?>">
                                                 <?php echo $value; ?>
                                            </option>
                                        <?php endforeach; ?>
									</select>
								</div>
							</td>
							<td>Program</td>
							<td>
								<div class="col-md-12">
									<select name="program" id="program" class="form-control">
										<option value="all">---- Semua ----</option>
										<?php foreach ($program->result() as $key) : ?>
											<option value="<?= $key->id?>"><?= $key->nama_program ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</td>

						</tr>
						<tr>
							<td colspan="4">
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<!--button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
								</div>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_obat_kadaluarsa"></div>
	</div>
</div>

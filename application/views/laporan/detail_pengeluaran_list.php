<div class="panel panel-primary" id="halaman_pengeluaran">
	<div class="panel-heading">
		<span class="glyphicon glyphicon-list-alt"></span> <b>Detail Pengeluaran Obat</b>
		<button id="back_to_" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button>
	</div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
				<table class="table table-bordered">
					<tr>
						<td>Tanggal</td>
						<td>
							: <?php echo str_replace("-", "/", $date_start);?> - <?php echo str_replace("-", "/", $date_end);?>
							<input type="hidden" id="detail-date-start" value="<?= $date_start ?>" name="">
							<input type="hidden" id="detail-date-end" value="<?= $date_end ?>" name="">
						</td>
					</tr>
					<tr>
						<td>Kode Obat</td>
						<td>
							: <?php echo $obat['kode_obat'];?>
						</td>
					</tr>
					<tr>
						<td>Nama Obat</td>
						<td>
							: <?php echo $obat['nama_obat']." ".$obat['kekuatan']." ".$obat['sediaan'];?>
						</td>
					</tr>
					<tr>
						<td>Jumlah</td>
						<td>
							: <span id="sp-total"></span><?php $obat['sediaan'];?>
						</td>
					</tr>
				</table>
			</div>
			<!--/form-->
			<div id="list_detail_pengeluaran">
				<table class="table tabel-bordered">
				<thead>
				<tr class="active">
					<th>No.</th>
					<th>Unit Penerima</th>
					<th>Periode</th>
					<th>No. SBBK</th>
					<th>Jumlah Pengeluaran</th>
					<th>Tanggal dikeluarkan</th>
					<th>Keterangan</th>
					<?php if($this->session->userdata('id')==1):?>
						<th>Aksi</th>
					<?php endif; ?>
				</tr>
				</thead>
				<tbody id="trcontent">
					<?php $tot = 0; $i = 1; foreach ($detailkeluar as $rows): ?>
						<tr style="" id="tr_<?= $rows->id ?>">
							<td><?= $i ?></td>
							<td><?= $rows->nama_pusk ?></td>
							<td><?= $rows->periode ?></td>
							<td><?= $rows->no_dok ?></td>
							<td><?= $rows->pemberian ?></td>
							<td><?= date("d-m-Y", strtotime($rows->tanggal_pemberian)) ?></td>
							<td>LPLPO</td>
							<?php if($this->session->userdata('group_id')==1):?>
								<td>
									<!-- <button class="btn btn-warning btn-edit btn-xs"><span class="glyphicon glyphicon-pencil"></span></button> -->
									<button class="btn btn-danger btn-delete btn-xs" href="<?= base_url();?>index.php/manajemenlogistik/detail_distribusi/deleteAdmin/<?= $rows->id?>"><span class="glyphicon glyphicon-trash"></span></button>
								</td>
							<?php endif; ?>
						</tr>
					<?php $i++; $tot = $tot + $rows->pemberian; endforeach; ?>
					<?php $k=$i; foreach ($detailkeluarcito as $rows): ?>
						<tr style="" id="tr_<?= $rows->id ?>">
							<td><?= $k ?></td>
							<td><?= $rows->unit_eks ?></td>
							<td><?= $rows->periode ?></td>
							<td><?= $rows->no_dok ?></td>
							<td><?= $rows->pemberian ?></td>
							<td><?= date("d-m-Y", strtotime($rows->tanggal_pemberian)) ?></td>
							<td>Sewaktu</td>
							<?php if($this->session->userdata('group_id')==1):?>
								<td>
									<!-- <button class="btn btn-warning btn-edit btn-xs" href="<?= base_url();?>index.php/manajemenlogistik/get"><span class="glyphicon glyphicon-pencil"></span></button> -->
									<button class="btn btn-danger btn-delete btn-xs" href="<?= base_url();?>index.php/manajemenlogistik/detail_distribusi/deleteAdmin/<?= $rows->id?>"><span class="glyphicon glyphicon-trash"></span></button>
								</td>
							<?php endif; ?>
						</tr>
					<?php  $k++; $tot = $tot + $rows->pemberian; endforeach;?>
					<input type="hidden" id="total" value = "<?= $tot ?>" name="">
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- show modal -->
<script type="text/javascript">
$(document).ready(function(){
	$('#sp-total').text($('#total').val());
	$('#back_to_').click(function(){
		var url_penerimaan='laporan/pengeluaran';
		$('#konten').load(url_penerimaan);
		loadData($("#detail-date-start").val(), $("#detail-date-end").val());
	})

	$('.btn-edit').on("click",function(){
		alert()
	})

	$('.btn-delete').on("click",function(){
		var conf = confirm("Apakah Anda yakin akan menghapus data ini?");
		var id = $(this).attr("href").split('/');
		if(conf) {
			$.ajax({
				url:$(this).attr("href"),
				type:"POST",
				dataType:'json',
				success:function(response){
					if(response.status==1) {
						$('#tr_'+id[id.length-1]).remove();
					}
					alert(response.message);
				}
			})
		}
	})
});
</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#btn_show").click(function(){
		var url2="laporan/pengeluaran/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			kec:$("#list_kec_report").val(),
			pusk:$("#nmpuskesmas_report").val(),
			kategori:$("#kategori").val(),
			dana:$("#dana").val(),
			tahun_anggaran:$("#tahun-anggaran").val(),
			nama:$("#key_name").val(),
			unit_eksternal:$("#unit_luar").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				$("#list_pengeluaran").html(e);
			}
		});
	})
});

function loadData(start, end) {
	var url2="laporan/pengeluaran/search_data_by";
	var form_data = {
		awal:start,
		akhir:end,
		kec:'all',
		pusk:'all',
		kategori:'all',
		dana:'all',
		tahun_anggaran:'all',
		nama:''
	}
	$.ajax({
		type:"POST",
		url:url2,
		data: form_data,
		success:function(e){
			$("#list_pengeluaran").html(e);
		}
	});
}
//======= get puskesmas
function get_puskesmas(val) {
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/profil/unit penerima");
    }else{
    	$.get('laporan/pengeluaran/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_report').html(data);});
    }
}
//====== end get puskesmas
</script>
<div class="panel panel-primary" id="halaman_pengeluaran">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan pengeluaran</b></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
				<table class="table">
					<tr>
						<td width="10%">Kecamatan</td>
						<td width="40%">
							<div class="col-md-12">
								<select class="form-control" name="list_kec_report" id="list_kec_report" onchange="get_puskesmas(this.value);">
									<option value="all">---- Semua ----</option>
									<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
									<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
									<?php endfor; ?>
								</select>
							</div>
						</td>
						<td width="10%">Kategori</td>
						<td width="40%" colspan="">
							<div class="col-md-12">
								<select class="form-control" name="kategori_objek" id="kategori_objek">
									<option value="all">---- Semua ----</option>
									<option value="obat">- Obat</option>
									<option value="bmhp">- Bmhp</option>
									<option value="alkes">- Alkes</option>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<div class="col-md-12">
								<select class="form-control" name="nmpuskesmas_report" id="nmpuskesmas_report" onchange="">
									<option value="all">---- Semua ----</option>
								</select>
							</div>
						</td>
						<td>Unit Eksternal</td>
						<td>
							<div class="col-md-12">
									<div class="input-group">
										<select name="unit_luar" class="form-control" id="unit_luar">
											<option value="all">--- Semua ---</option>
											<?php for($i=0;$i<sizeof($ux);$i++): ?>
											<option value="<?php echo $ux[$i]['id']; ?>"><?php echo $ux[$i]['description']; ?></option>
											<?php endfor; ?>
										</select>
									</div>
							</div>
						</td>
					</tr>
					<tr>
						<td>Kategori Obat</td>
						<td>
							<div class="col-md-12">
								<select name="kategori" id="kategori" class="form-control">
									<option value="all">---- Semua ----</option>
									<option value="generik">Generik</option>
									<option value="generik bermerek">Generik Bermerek</option>
									<option value="paten">Paten</option>
								</select>
							</div>
						</td>
						<td>Periode</td>
						<td>
							<div class="col-md-5">
								<div class="input-group input-append date" id="datepicker" data-date="<?= date('Y');?>-01-01" data-date-format="yyyy-mm-dd" >
									<input class="form-control span2" size="12" type="text" value="<?= date('Y');?>-01-01" id="periodeawal" readonly="readonly" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
								</div>
								<script>
										$('#datepicker').datepicker();
								</script>
							</div>
							<div class="col-md-2">sampai</div>
							<div class="col-md-5">
								<div class="input-group input-append date" id="datepicker2" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>"  id="periodeakhir" readonly="readonly">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
							<script>
									$('#datepicker2').datepicker();
							</script>
							</div>
						</td>
					</tr>
					<tr>
						<td>Nama Sediaan</td>
						<td colspan="1">
							<div class="col-md-12">
								<input type="text" class="form-control" name="key_name" id="key_name">
							</div>
						</td>
						<td>Sumber Dana</td>
						<td>
							<div class="col-md-5">
								<select name="dana" id="dana" onchange="" class="form-control">
									<option value="all">----- Semua -----</option>
									<option value="APBN">APBN</option>
									<option value="APBDI">APBDI</option>
									<option value="APBDII">APBDII</option>
									<option value="DAK">DAK</option>
									<option value="Otonomi Khusus">Otonomi Khusus</option>
									<option value="Lain-lain">Lain-lain</option>
								</select>
							</div>
							<div class="col-md-5">
								<select name="tahun_anggaran" id="tahun-anggaran" class="form-control">
									<option value="all">---- Semua ----</option>
									<?php foreach(tahun() as $key => $value): ?>
                                        <option value="<?php echo $key; ?>">
                                             <?php echo $value; ?>
                                        </option>
                                    <?php endforeach; ?>
								</select>
							</div>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
								<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
								<!--button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
							</div>
						</td>
					</tr>
				</table>
			</div>
			<!--/form-->
			<div id="list_pengeluaran"></div>
	</div>
	<div id="detail-konten"></div>
</div>

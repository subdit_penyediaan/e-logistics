<!DOCTYPE html>
<html>
<head><title>Cetak Laporan</title>
<script type="text/javascript">
function cetak(){
	window.print();
}
</script>
<style type="text/css">
	table {
    border-collapse: collapse;
	}

	table, td, th {
	    border: 1px solid black;
	    padding: 5px;
	}
</style>
</head>
<body  onload="cetak();">
	<center>
		<h2>LAPORAN STOK OBAT INN/FDA</h2>
		<h2><?php echo $wilayah; ?></h2>
		<h4>per <?php echo date('Y-m-d');?></h4>
	</center>
	<!-- <h3><?= $dinas ?></h3><h4><?= $date_start ?> sampai <?= $date_end ?></h4> -->
	<div id="">
		<?php echo $konten; ?>

	</div>
	<!--button onclick="cetak();">Cetak Sekarang</button-->
</body>
</html>

<script type="text/javascript">
	$(function(){
		$("#pagelistukp4").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_ukp4').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistukp4">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table">
		<thead>
		<tr class="active">
			<th>ID UKP4</th>
			<th>Nama Kelompok</th>
			<th>Stok Akhir</th>
			<th>Penggunaan Rata-rata (tiap bulan)</th>
			<th>Ketersediaan (bulan)</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					$rata=$rows->jumlah_penggunaan/$rows->cur_month+0.01;
					if($rata<1){
						$ketersediaan='tak berhingga';
						echo '<tr class="info">';
					}else{
						$ketersediaan = floor($rows->jumlah_stok/$rata);

						if ($ketersediaan >= 6){
							echo '<tr class="info">';	
						}elseif (($ketersediaan < 6) && ($ketersediaan >= 4)){
							echo '<tr class="warning">';
						}else {
							echo '<tr class="danger">';
						};
					}
					echo '<td>'.$rows->id_ukp4.'</td>
					<td>'.$rows->nama_ukp4.'</td>
					<td>'.$rows->jumlah_stok.'</td>
					<td>'.floor($rata).'</td>
					<td>'.$ketersediaan.'</td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
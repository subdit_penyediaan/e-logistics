<style type="text/css">
	.dataTables_filter{
		float:right;
	}
	.pagination{
		float:right;
	}
	th{
		text-align: center;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#konten_table').dataTable();
		$("#pagelistpenerimaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_penerimaan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
		$("#btn_eksport").click(function(){
			var urlprint="laporan/penerimaan/printOut";
			window.open(urlprint);
		})
	});
</script>
<div id="pagelistpenerimaan">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<center>
		<h4>LAPORAN PENERIMAAN BARANG<br>GUDANG INSTALASI FARMASI</h4>
		<h5>per tanggal: <?php echo $awal." - ".$akhir?> </h5>
	</center>
	<table class="table table-striped table-bordered" id="konten_table">
		<thead>
		<tr class="active">
			<th>Kode Obat</th>
			<th>No. Batch</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>No. Faktur/Kontrak</th>
			<th>Tanggal Penerimaan</th>
			<th>Tanggal Kadaluarsa</th>
			<th>Jumlah</th>
			<th>Sumber Dana</th>
			<!--th>Total Mutasi</th-->
			<th>Harga Satuan (Rp)</th>
			<th>Nilai Penerimaan (Rp)</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				$total=0;
				foreach ($result as $rows) {
					$nilaipenerimaan=$rows->jumlah*$rows->harga;
					$total=$total+$nilaipenerimaan;
					echo '<tr style="">
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->nama_obj.'<br><small>'.$rows->pabrik.'</small></td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->no_faktur.'<br>No. Kontrak: '.$rows->no_kontrak.'</td>
					<td>'.$rows->tanggal_terima.'</td>
					<td>'.$rows->expired_date.'</td>
					<td>'.$rows->jumlah.'</td>
					<td>'.$rows->dana.' '.$rows->tahun_anggaran.'</td>
					<td>'.number_format($rows->harga,2,",",".").'</td>
					<td>'.number_format($nilaipenerimaan,2,",",".").'</td></tr>';
					//$i++;
				}
				//echo '<td colspan="9">TOTAL PENERIMAAN = Rp '.number_format($total,2,",",".");
			?>
		</tbody>
		<tr>
			<td colspan="11">
				TOTAL PENERIMAAN= Rp. <?php echo number_format($total,2,",","."); ?>
			</td>
		</tr>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>

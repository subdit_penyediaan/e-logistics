<style type="text/css">
th,td{
	padding: 5px;
}

</style>
<script type="text/javascript">
	$(function(){
		$("#btn_eksport").click(function(){
			var urlprint="laporan/pengeluaran/printOut";
			window.open(urlprint);
		})
		//$('#konten_table').dataTable();
		$("#pagelistpengeluaran").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_pengeluaran').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href"),

			awal = $("#periodeawal").val(),
			akhir = $("#periodeakhir").val()

			var new_url = url+"/"+awal+"/"+akhir;

			$('#konten').load(new_url);

		});
		//$('#menuleft').slideUp("slow");
	});
</script>
<div id="pagelistpengeluaran">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
<h4>Periode: <?= $awal ?> sampai <?= $akhir ?></h4>
	<table class="table table-condensed table-bordered" id="konten_table">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Kode Obat</th>
			<th>No. Batch</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>Jumlah Pengeluaran</th>
			<th>Harga (Rp)</th>
			<th>Total (Rp)</th>
			<th>Sumber Dana</th>
			<th>Detail</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php $i=1;$saldo_akhir=0; foreach ($result as $rows): ?>
				<?php $saldo_akhir=$saldo_akhir+$rows->nilai;?>
				<tr style="">
					<td><?= $i ?></td>
					<td><?= $rows->kode_obat ?></td>
					<td><?= $rows->no_batch ?></td>
					<td><?= $rows->nama_obj ?><br><small><?= $rows->pabrik ?></small></td>
					<td><?= $rows->deskripsi ?></td>
					<td><?= $rows->pengeluaran ?></td>
					<td><?= number_format($rows->harga,2,",",".") ?></td>
					<td><?= number_format($rows->nilai,2,",",".") ?></td>
					<td><?= $rows->dana ?> <?= $rows->tahun_anggaran ?></td>
					<td>
						<button href="<?= $base_url ?>index.php/laporan/pengeluaran/get_detail/<?= $rows->id_stok ?>">
							<span class="glyphicon glyphicon-list"></span>
						</button>
					</td>
				</tr>
			<?php $i++; endforeach; ?>
		</tbody>
		<tr>
			<td colspan="10">
				TOTAL ASET KELUAR= Rp. <?php echo number_format($saldo_akhir,2,",","."); ?>
			</td>
		</tr>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>

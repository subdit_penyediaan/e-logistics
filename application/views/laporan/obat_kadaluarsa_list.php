<style type="text/css">
	.dataTables_filter{
		float:right;
	}
	.pagination{
		float:right;
	}
	th{
		text-align: center;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#konten_table').dataTable();
		$("#pagelistobat_kadaluarsa").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_obat_kadaluarsa').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
			$("#btn_eksport").click(function(){
			var urlprint="laporan/obat_kadaluarsa/printOut";
			window.open(urlprint);
		})
	});
</script>
<div id="pagelistobat_kadaluarsa">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<center>
		<h4>LAPORAN SEDIAAN KADALUARSA<br>GUDANG INSTALASI FARMASI</h4>
	</center>
	<table class="table table-bordered" id="konten_table">
		<thead>
		<tr class="active">
			<!-- <th>Kode Obat</th> -->
			<th>No. Batch</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>Sisa Stok</th>
			<th>Tanggal <br> Kadaluarsa</th>
			<th>Masa Berlaku<br>(bulan)</th>
			<th>Harga</th>
			<th>Total</th>
			<th>Sumber Dana</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					$lama = $rows->lamatahun* -12 + $rows->lama;

					if ($rows->id_obatprogram != '1') {
                        if ($lama >= -3){
							echo '<tr class="danger">';
						}elseif (($lama < -3) && ($lama >= -6)){
							echo '<tr class="warning">';
						}else {
							echo '<tr class="info">';
						};
                    } else {
                        if ($lama >= -9)
                        {
                            echo '<tr class="danger">';
                        }
                        elseif (($lama < -9) && ($lama >= -12))
                        {
                            echo '<tr class="warning">';
                        }
                        else {
                            echo '<tr class="info">';
                        };
                    }
					// echo '<td>'.$rows->kode_obat.'</td>
					echo '<td>'.$rows->no_batch.'</td>
					<td>'.$rows->nama_obj.'<br><small>'.$rows->pabrik.'</small></td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->stok.'</td>
					<td>'.$rows->ed.'</td>
					<td>'.$lama.'</td>
					<td>'.uangIndo($rows->harga).'</td>
					<td>'.uangIndo($rows->harga * $rows->stok).'</td>
					<td>'.$rows->dana.' '.$rows->tahun_anggaran.'</td>
					</tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>

<style>
	.progress {
	    margin-top: 30px;
	    width: 500px;
	}	
</style>
<script>
$(document).ready(function(){	
	$('.progress').hide();
	//button group filter
	$("#btn_show").click(function(){
		var progress = setInterval(function () {
		    var $bar = $('.progress-bar');

		    if ($bar.width() >= 480) { //panjang progress bar
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5); //penambahan tiap satuan waktu
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 1234); //satuan waktu
		$('.progress').show();
		var url2="laporan/obat_indikator/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			kategori:$("#kategori").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				
				$("#list_ketersediaan_obat").html(e);//+"#list_sedian_obat");
			}
		});
	})
	

	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		//var urlprint="laporan/obat_indikator/printOut";
		//window.open(urlprint);
		var url2="laporan/obat_indikator/printOut";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			kategori:$("#kategori").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				var x = window.open();
				x.document.open();
				x.document.write(e);
				x.document.close();
				//$("#list_ketersediaan_obat").html(e);//+"#list_sedian_obat");
			}
		});		
	})
	//end eksport button
	
});
</script>
<div class="panel panel-primary" id="halaman_ketersediaan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan Ketersediaan Obat Indikator</b></div>
		<div id="up-konten"class="panel-body" style="padding:15px;">
			<!-- bag. isi -->
			<br>
			<table class="table">
				<tr>						
					<td width="10%">dari bulan</td>
					<td width="20%">
						<div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d');?>" data-date-format="mm" >
							<input class="form-control span2" size="12" type="text" value="<?php echo date('Y');?>-01-01" id="periodeawal" readonly="readonly" >
							<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
						</div>
						<script>
						$('#datepicker').datepicker(
							{
							    format: "yyyy-mm",
							    viewMode: "months", 
							    minViewMode: "months"
							});
						</script>
					</td>					
					<td width="10%">sampai bulan</td>
					<td>
						<div class="col-md-5">
							<div class="input-group input-append date" id="datepicker2" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>"  id="periodeakhir" readonly="readonly">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>						
						<script>
						$('#datepicker2').datepicker(
							{
							    format: "yyyy-mm",
							    viewMode: "months", 
							    minViewMode: "months"
							});
						</script>
					</td>
				</tr>		
				<tr>
					<td colspan="4"><div class="pagingContainer">
							<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
							<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
							<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Print</button>
						</div>
					</td>
				</tr>
			</table>
			<div id="list_ketersediaan_obat">
				<div class="progress">
				  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">
				    
				  </div>
				</div>
			</div>
		</div>
	</div><!-- end div panel conten -->
</div><!-- end div panel -->
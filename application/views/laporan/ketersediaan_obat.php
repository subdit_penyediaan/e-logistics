<style>
.progress {
    margin-top: 30px;
    width: 500px;
}		
</style>
<script>
$(document).ready(function(){
	$('.progress').hide();
	//button group filter Fornas
	$("#btn_show_fornas").click(function(){
		var progress = setInterval(function () {
		    var $bar = $('.bar-fornas.progress-bar');

		    if ($bar.width() >= 480) {
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5);
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 2345);
		$('#bar-fornas').show();
		var url23="laporan/ketersediaan_obat/search_data_fornas";
		var form_data = {
			awal:$("#awal_fornas").val(),
			akhir:$("#akhir_fornas").val(),
			nama:$("#key_name_fornas").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url23,
			data: form_data,
			beforeSend: function() {
				$("#fornas").block({
			        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>'
			    });
			},
			success:function(e){
				$("#fornas").unblock();
				$("#list_stok_fornas").html(e);//+"#list_sedian_obat");
				//$('.progress').hide();
			}
		});
	})

	//button group filter generik
	$("#btn_show_generik").click(function(){
		var progress = setInterval(function () {
		    var $bar = $('.bar-generik.progress-bar');

		    if ($bar.width() >= 480) {
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5);
		    }
		    $bar.text(Math.round($bar.width() / 5) + "%");
		}, 2345);
		$('#bar-generik').show();
		var url23="laporan/ketersediaan_obat/search_data_generik";
		var form_data = {
			awal:$("#awal_gen").val(),
			akhir:$("#akhir_gen").val(),
			nama:$("#key_name_generik").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url23,
			data: form_data,
			beforeSend: function() {
				$("#generik").block({
			        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>'
			    });
			},
			success:function(e){
				$("#generik").unblock()
				$("#list_generik").html(e);//+"#list_sedian_obat");
			}
		});
	})

	//load kelompok generik
	var url_generik="laporan/ketersediaan_obat/obat_generik";
	//$('#generik').load(url_generik);

	//load kelompok fornas
	var url_kel_fornas="laporan/ketersediaan_obat/obat_fornas";
	//$('#fornas').load(url_kel_fornas);

	//button group filter
	$("#btn_show").click(function(){
		var url2="laporan/ketersediaan_obat/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			dana:$("#dana").val(),
			kategori:$("#kategori").val(),
			jprogram:$("#jprogram").val(),
			nama:$("#key_name").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			beforeSend: function() {
				$("#obat_all").block({
			        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>'
			    });
			},
			success:function(e){
				$("#obat_all").unblock()
				$("#list_ketersediaan_obat").html(e);//+"#list_sedian_obat");
			}
		});
	})

	//default laporan rata-rata penggunaan 3 bulan sebelumnya
	var url="laporan/ketersediaan_obat/get_data_ketersediaan_obat";
	//$('#list_ketersediaan_obat').load(url);	

	//============== submit add form

	// //eksport button
	// $("#btn_eksport").click(function(){
	// 	var urlprint="laporan/ketersediaan_obat/printOut";
	// 	window.open(urlprint);		
	// })
	// //end eksport button
	// var progress = setInterval(function () {
	//     var $bar = $('.progress-bar');

	//     if ($bar.width() >= 400) {
	//         clearInterval(progress);
	//         $('.progress').removeClass('active');
	//     } else {
	//         $bar.width($bar.width() + 5);
	//     }
	//     $bar.text($bar.width() / 5 + "%");
	// }, 1000);
	
});
</script>
<div class="panel panel-primary" id="halaman_ketersediaan_obat">
	<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> <b>Laporan Ketersediaan Obat</b></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
			<li class="active"><a href="#obat_all" data-toggle="tab">Semua Obat</a></li> 
			<li><a href="#fornas" data-toggle="tab">Kelompok Fornas</a></li>
			<li><a href="#generik" data-toggle="tab">Kelompok INN/FDA</a></li>
		</ul> 
		<div class="tab-content">
			<div class="tab-pane active" id="obat_all">
				<br>
				<table class="table">
						<tr>
							<td width="10%">Penggunaan dari</td>
							<td width="40%">
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
										<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>" id="periodeawal" readonly="readonly" >
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>

									<script>
									$('#datepicker').datepicker();
									</script>
								</div>
								<div class="col-md-2"><label>sampai</label></div>
								<div class="col-md-5">
									<div class="input-group input-append date" id="datepicker2" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
										<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d',time()+86400);?>"  id="periodeakhir" readonly="readonly">
										<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>

									<script>
									$('#datepicker2').datepicker();
									</script>
								</div>
							</td>
							<td width="10%">Jenis Program</td>
							<td width="40%">
								<div class="col-md-12">
									<select class="form-control" name="jprogram" id="jprogram">
										<option value="all">----- Semua -----</option>
										<?php foreach ($program->result() as $key):?>
										<option value="<?= $key->id?>"><?= $key->nama_program?></option>
										<?php endforeach; ?>
									</select>
								</div>								
							</td>
						</tr>
						<tr>
							<td>Kategori Obat</td>
							<td>
								<div class="col-md-12">
									<select class="form-control" name="kategori" id="kategori">
										<option value="all">---- Semua ----</option>
										<option value="generik">Generik</option>
										<option value="generik bermerek">Generik bermerek</option>
										<option value="paten">paten</option>
									</select>
								</div>								
							</td>
							<td>Nama Sediaan</td>
							<td>
								<div class="col-md-12">
									<input type="text" class="form-control" name="key_name" id="key_name">		
								</div>
							</td>
						</tr>											
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<!--button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button-->
								</div>
							</td>
						</tr>
					</table>
				<div id="list_ketersediaan_obat"></div>
			</div>
			<!--/form-->
		
		<div class="tab-pane" id="fornas">
			<br>
			<table class="table">
				<tr>
					<td width="10%">Penggunaan dari</td>
					<td width="40%">
						<div class="col-md-5">
							<div class="input-group input-append date" id="datepicker75" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>" id="awal_fornas" readonly="readonly" >
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
								$('#datepicker75').datepicker();									
							</script>
						</div>
						<div class="col-md-2"><label>sampai</label></div>
						<div class="col-md-5">
							<div class="input-group input-append date" id="datepicker76" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d',time()+86400);?>"  id="akhir_fornas" readonly="readonly">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
								$('#datepicker76').datepicker();									
							</script>
						</div>
					</td>
					<td width="10%">Nama Fornas</td>
					<td><input type="text" class="form-control" name="key_name_fornas" id="key_name_fornas"></td>
				</tr>
			</table>
			<button type="submit" name="Simpan" id="btn_show_fornas" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
			<hr>
			<div id="list_stok_fornas">
				<div id="bar-fornas" class="progress">
				  <div class="bar-fornas progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">
				    
				  </div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="generik">
			<br>
			<table class="table">
				<tr>
					<td width="10%">Penggunaan dari</td>
					<td width="40%">
						<div class="col-md-5">
							<div class="input-group input-append date" id="datepicker77" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d');?>" id="awal_gen" readonly="readonly" >
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
								$('#datepicker77').datepicker();									
							</script>
						</div>
						<div class="col-md-2"><label>sampai</label></div>
						<div class="col-md-5">
							<div class="input-group input-append date" id="datepicker78" data-date="<?php echo date('Y-m-d');?>" data-date-format="yyyy-mm-dd" >
								<input class="form-control span2" size="12" type="text" value="<?php echo date('Y-m-d',time()+86400);?>"  id="akhir_gen" readonly="readonly">
								<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
								$('#datepicker78').datepicker();									
							</script>
						</div>
					</td>
					<td width="10%">Nama INN/FDA</td>
					<td><input type="text" class="form-control" name="key_name_generik" id="key_name_generik"></td>
				</tr>
			</table>
				<button type="submit" name="Simpan" id="btn_show_generik" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
				<hr>
				<div id="list_generik">
					<div id="bar-generik" class="progress">
					  <div class="bar-generik progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width: 1%;">
					    
					  </div>
					</div>
				</div>
		</div><!-- end div tab generik -->
		</div><!-- end div tab conten -->
	</div><!-- end tab -->
	</div><!-- end div panel conten -->
</div><!-- end div panel -->
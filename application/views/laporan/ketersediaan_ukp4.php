<style>
.progress {
    margin-top: 30px;
    width: 500px;
}		
</style>
<script>
$(document).ready(function(){
	$(".progress").hide();
		var progress = setInterval(function () {
		    var $bar = $('.progress-bar');

		    if ($bar.width() >= 400) {
		        clearInterval(progress);
		        $('.progress').removeClass('active');
		    } else {
		        $bar.width($bar.width() + 5);
		    }
		    $bar.text($bar.width() / 5 + "%");
		}, 1000);

	$('#add_perencanaan_btn').click(function(){
		var url_planing='laporan/ketersediaan_ukp4/add_perencanaan';
		$('#konten').load(url_planing);
	})
	//load kelompok ukp4
	var url_kel_ukp4="laporan/ketersediaan_obat/obat_ukp4";
	$('#ukp4').load(url_kel_ukp4);

	//button group filter
	$("#btn_show").click(function(){

		//$(".progress").show();
		var url2="laporan/ketersediaan_ukp4/search_data_by";
		var form_data = {
			tahun:$("#thn_perencanaan").val(),
			periode:$("#periode_perencanaan").val()
		}
		//var konten1=$('#thn_perencanaan').val();
		//var konten2=$('#periode_perencanaan').val();
		if((form_data.tahun=='')||(form_data.periode=='')){
			alert("pilih periode perencanaan dahulu");
		}else{
			$(".progress").show();
			$.ajax({
				type:"POST",
				url:url2,
				data: form_data,
				success:function(e){
					//$(".progress").show();
					$("#list_ketersediaan_ukp4").html(e);//+"#list_sedian_obat");
				}
			});	
		}		
	})

	//var url="laporan/ketersediaan_obat/get_data_ketersediaan_ukp4";
	//$('#list_ketersediaan_ukp4').load(url);	
	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		var urlprint="laporan/ketersediaan_obat/printOut";
		window.open(urlprint);		
	})
	//end eksport button
	
});

function cek_data(val){
	var url='laporan/ketersediaan_ukp4/cek_perencanaan/'+val;
	$.get(url,function(data){
		if(data==0){
			alert("anda belum membuat perencanaan pada tahun "+val);
		}
		
	})
}
</script>
<div class="panel panel-primary" id="halaman_ketersediaan_ukp4">
	<div class="panel-heading">Laporan Ketersediaan Obat Indikator</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<button id="add_perencanaan_btn"><span class="glyphicon glyphicon-plus"></span> Perencanaan</button><br><br>
			<div style="color:red;">*Harap melakukan input data stok opnam tiap bulan agar laporan valid</div>
				<table class="table">
						<tr>
							<td>Tahun</td>
							<td>
								<select name="thn_perencanaan" id="thn_perencanaan" onchange="cek_data(this.value);">
									<option value="">---- Pilih ----</option>
									
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>Periode</td>
							<td>
								<select name="periode_perencanaan" id="periode_perencanaan">
									<option value="">---- Pilih ----</option>
									<option value="1">B03</option>
									<option value="2">B06</option>
									<option value="3">B09</option>
									<option value="4">B12</option>
								</select>
							</td>
						</tr>
											
						<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
								</div>
							</td>
						</tr>
					</table>
				<div id="list_ketersediaan_ukp4">
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
					    
					  </div>
					</div>			
				</div>
			</div>
			
		</div><!-- end div panel conten -->
</div><!-- end div panel -->
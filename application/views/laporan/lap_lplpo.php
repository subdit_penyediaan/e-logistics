 <style>
	
</style>
<script type="text/javascript">
$(document).ready(function(){		

	
	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		var urlprint="laporan/lap_lplpo/printOut";
		window.open(urlprint);		
	})
	//end eksport button
	$("#btn_show").click(function(){
		var url2="laporan/lap_lplpo/search_data_by";
		var form_data = {
			kec:$("#list_kec").val(),
			per:$("#tanggal_ex").val(),
			nama_pusk:$("#nmpuskesmas").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			dataType: 'json',
			success:function(e){
				alert(e.confirm);
				$("#list_lap_lplpo").html(e.filled);//+"#list_sedian_obat");

				/*$("#no_faktur").val("");				
				$("#no_kontrak").val("");*/
			}
		});
	})

	//============== end submit add form
	
});

function databydana(val){
	var url="laporan/lap_lplpo/get_data_by/"+val;
	//alert(url);
	$('#list_lap_lplpo').load(url);	
}
function get_puskesmas(val) {
    
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/masterdata/puskesmas");
    }else{
    	//alert("cek");
    	$.get('laporan/lap_lplpo/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas').html(data);});    	
    }
}

//====== end get puskesmas
</script>
<div class="panel panel-primary" id="halaman_lap_lplpo">
	<div class="panel-heading">Laporan LPLPO</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				<table class="table">
					<tr>
						<td>Kecamatan</td>
						<td>
							<select name="list_kec" id="list_kec" onchange="get_puskesmas(this.value);">
								<option>--- PILIH ---</option>
								<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
								<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Puskesmas</td>
						<td>
							<select name="nmpuskesmas" id="nmpuskesmas" onchange="">
								<option>--- PILIH ---</option>								
							</select>
						</td>
						
					</tr>
					<tr>
						<td>Pelaporan Bulan/Periode</td>
						<td>
							<div class="input-append date" id="datepicker" data-date="15-03-2014" data-date-format="yyyy-mm" >
								<input class="span2" size="12" type="text" readonly="readonly" name="tanggal_ex" id="tanggal_ex">
								<span class="add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
							</div>

							<script>
							$('#datepicker').datepicker({
								    format: "yyyy-mm",
								    viewMode: "months", 
								    minViewMode: "months"
								});
							</script>
						</td>
					</tr>
					<tr>
							<td colspan="4"><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary"><span class="glyphicon glyphicon-list"></span> Show</button>
									<!--button id="batal"><span class="glyphicon glyphicon-refresh"></span> Reset</button-->
									<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
								</div>
							</td>
						</tr>
					
					</table>
			</div>
			<!--/form-->
			<div id="list_lap_lplpo"></div>
	</div>
</div>
<!--div class="alert alert-danger"> data belum tersedia</div-->
<style type="text/css">
	.dataTables_filter{
		float:right;
	}
	.pagination{
		float:right;
	}
	th{
		text-align: center;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#konten_table').dataTable();
		$("#pagelistketersediaan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_ketersediaan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
		//eksport button
		$("#btn_eksport").click(function(){
			var urlprint="laporan/ketersediaan_obat/printOut";
			window.open(urlprint);
		})
		//end eksport button
	});
</script>
<div id="pagelistketersediaan">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-bordered" id="konten_table">
		<thead>
		<tr class="active">
			<th>Kode Obat</th>
			<th>No. Batch</th>
			<th>Nama Obat</th>
			<th>Stok Akhir</th>
			<th>Penggunaan Rata-rata <br>(<?php echo $bulan_pembagi; ?> bulan terakhir)</th>
			<th>Ketersediaan (bulan)</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				$data = array();
				// foreach ($result_pengeluaran as $key) {
				// 	$data[$key->no_batch]=$key->jumlah_penggunaan;
				// }
				foreach ($result as $rows) {

					// if(isset($data[$rows->no_batch])){
					// 	$penggunaan=$data[$rows->no_batch];
					// }else{ $penggunaan=0;}

					if($bulan_pembagi>0){
						//$rata=$penggunaan/$bulan_pembagi;
						$rata=$rows->rerata_pengeluaran/$bulan_pembagi;
					}else{
						$rata=0;
					}
					//saldo juga dinamis sesuai periode
					//
					if($rata<1){
						$ketersediaan='tak berhingga';
						echo '<tr class="info">';
					}else{
						//$ketersediaan = round($rows->jumlah_stok/$rata);
						$ketersediaan = round($rows->stok/$rata);
						//$ketersediaan = $rata;

						if ($ketersediaan >= 6){
							echo '<tr class="info">';
						}elseif (($ketersediaan < 6) && ($ketersediaan >= 4)){
							echo '<tr class="warning">';
						}else {
							echo '<tr class="danger">';
						};
					}
					echo '<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->nama_obj.'</td>
					<td>'.$rows->stok.'</td>
					<td>'.floor($rata).'</td>
					<td>'.$ketersediaan.'</td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>

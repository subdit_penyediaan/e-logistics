 <style>

</style>
<script type="text/javascript">
$(document).ready(function(){

	var url="laporan/pengeluaran/get_data_pengeluaran";
	//$('#list_pengeluaran').load(url);
	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		var urlprint="laporan/pengeluaran/printOut";
		window.open(urlprint);
	})
	//end eksport button
	$("#btn_show").click(function(){
		var url2="laporan/pengeluaran/search_data";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val(),
			kategori:$("#kategori").val(),
			kode_kab:$(".kab-key").val(),
			kode_prov:$(".prov-key").val(),
			nama:$("#key_name").val(),
      program:$("#program").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){

				$("#list_pengeluaran").html(e);//+"#list_sedian_obat");

			}
		});
	})

	//============== end submit add form

});
function databydana(val){
	var url="laporan/pengeluaran/get_data_by/"+val;
	//alert(url);
	$('#list_pengeluaran').load(url);
}

//======= get puskesmas
function get_puskesmas(val) {
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/profil/unit penerima");
    }else{
    	//alert("cek");
    	//$.get('laporan/pengeluaran/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_report').html(data);});
    	$.get('laporan/pengeluaran/get_data_kabupaten/' + val, function(data) {$('#nmkabupaten_report').html(data);});
    }
}

//====== end get puskesmas
</script>
<div class="panel panel-primary" id="halaman_pengeluaran">
	<div class="panel-heading">Laporan pengeluaran</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/pengeluaran_obat/process_form'); ?>"-->
				<table class="table">
						<tr>
						<?php if($level==2): ?>
						<td>Kabupaten</td>
						<td>
							<select name="nmkabupaten" id="nmkabupaten" class="kab-key">
								<option value="all">---- Semua ----</option>
								<?php for($i=0;$i<sizeof($kabupaten);$i++): ?>
								<option value="<?php echo $kabupaten[$i]['CKabID']; ?>"><?php echo $kabupaten[$i]['CKabDescr']; ?></option>
								<?php endfor; ?>
							</select>
						</td>
						<?php elseif($level==99): ?>
							<td width="10%">*Propinsi</td>
							<td width="40%">
								<select name="list_kec_report" id="list_kec_report" onchange="get_puskesmas(this.value);" class="prov-key form-control">
									<option value="all">---- Semua ----</option>
									<?php for($i=0;$i<sizeof($propinsi);$i++): ?>
									<option value="<?php echo $propinsi[$i]['CPropID']; ?>"><?php echo $propinsi[$i]['CPropDescr']; ?></option>
									<?php endfor; ?>
								</select>
							</td>
							<td width="10%">Kabupaten</td>
							<td width="40%">
								<select name="nmkabupaten_report" id="nmkabupaten_report" onchange="" class="kab-key form-control">
									<option value="all">---- Semua ----</option>
								</select>
							</td>
						<?php endif; ?>
						</tr>
						<tr>
							<!--td>Kategori</td>
							<td><select name="kategori_objek" id="kategori_objek">
									<option value="all">---- Semua ----</option>
									<option value="obat">- Obat</option>
									<option value="bmhp">- Bmhp</option>
									<option value="alkes">- Alkes</option>
								</select>
							</td-->
							<td width="10%">Kategori Obat</td>
							<td width="40%">
								<select name="kategori" id="kategori" class="form-control">
									<option value="all">---- Semua ----</option>
									<option value="generik">Generik</option>
									<option value="generik bermerek">Generik Bermerek</option>
									<option value="paten">Paten</option>
								</select>
							</td>
							<td>Periode Awal</td>
							<td>
								<div class="input-group input-append date" id="datepicker" data-date="<?= date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									<input class="span2 form-control" size="12" type="text" value="<?= date('Y-m-d'); ?>" id="periodeawal" readonly="readonly" >
								</div>
								<script>
								$('#datepicker').datepicker();
									/*{
									    format: "yyyy-mm",
									    viewMode: "months",
									    minViewMode: "months"
									});*/
								</script>
							</td>
						</tr>
						<tr>
              <td>Program</td>
              <td>
                  <select name="program" id="program" class="form-control">
                    <option value="all">---- Semua ----</option>
                    <?php foreach ($program->result() as $key) : ?>
                      <option value="<?= $key->id?>"><?= $key->nama_program ?></option>
                    <?php endforeach; ?>
                  </select>
              </td>
              <td>Periode Akhir</td>
							<td>
								<div class="input-group input-append date" id="datepicker2" data-date="<?= date('Y-m-d'); ?>" data-date-format="yyyy-mm-dd" >
									<span class="input-group-addon add-on" style="cursor:pointer;"><span class="glyphicon glyphicon-calendar"></span></span>
									<input class="form-control span2" size="12" type="text" value="<?= date('Y-m-d'); ?>"  id="periodeakhir" readonly="readonly">
								</div>
								<script>
								$('#datepicker2').datepicker();
									/*{
									    format: "yyyy-mm",
									    viewMode: "months",
									    minViewMode: "months"
									});*/
								</script>
							</td>
						</tr>
            <tr>
							<td>Nama Sediaan</td>
							<td colspan="1"><input type="text" class="form-control" name="key_name" id="key_name"></td>
							<td colspan="2"></td>
						</tr>
					</table>
					<small><i>*instalasi farmasi</i><br></small>
					<button type="submit" name="Simpan" id="btn_show" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list"></span> Show</button>
			</div>
			<!--/form-->
			<div id="list_pengeluaran"></div>
	</div>
</div>

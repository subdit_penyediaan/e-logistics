<style type="text/css">
th{
	text-align: center;
}
th,td{
	padding: 5px;	
}
.dataTables_filter{
	float:right;
}
.input-sm {
	height: 30px;
	padding: 5px 10px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px;
}
	.pagination{
		float:right;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#konten_table').dataTable();
		$("#pageliststok").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_stok').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
		//$('#menuleft').slideUp("slow");
		$("#btn_eksport").click(function(){
			var urlprint="laporan/stok/printOut";
			window.open(urlprint);		
		})
	});
</script>
<div id="pageliststok">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<center>
		<h3>LAPORAN SALDO PERSEDIAAN<br>INSTALASI FARMASI</h3>
		<h5>per tanggal: <?php echo $akhir?> </h5>
	</center>
	<table class="table table-striped table-bordered" id="konten_table" style="">
		<thead>
		<tr class="active">
			<th>No.</th>
			<th>Kode Obat</th>
			<th>No. Batch</th>
			<th>Nama Obat</th>
			<th>Sediaan</th>
			<th>Penerimaan</th>
			<th>Pengeluaran</th>
			<th>Rusak dimusnahkan</th>
			<th>Saldo Akhir</th>
			<!--th>Perhitungan Fisik</th-->
			<th>Harga <br>Satuan (Rp)</th>
			<th>Nilai Persediaan</th>
			<th>Sumber Dana</th>
			<!--th>Nilai Selisih</th-->
		</tr>
		</thead>
		<tbody id="trcontent">			
			<?php
				//query
				$i=1;$total_nilaiaset=0;
				foreach ($result as $rows) {
					//$saldo_akhir = $rows->saldo_persediaan-$rows->pengeluaran;
					$nilaiaset = $rows->stok*$rows->harga;
					echo '<tr style=""><td>'.$i.'.</td>
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->nama_obj.'<br><small>'.$rows->pabrik.'<br>expired: '.$rows->expired.'</small></td>
					<td>'.$rows->sediaan.'</td>
					<td>'.$rows->saldo_persediaan.'</td>
					<td>'.$rows->pengeluaran.'</td>
					<td>'.$rows->jml_rusak.'</td>
					<td>'.$rows->stok.'</td>					
					<td style="">Rp. '.$rows->harga.'</td>
					<td>Rp. '.number_format($nilaiaset,2,",",".").'</td>
					<td>'.$rows->sumber_dana.' '.$rows->tahun_anggaran.'</td></tr>';
					$i++;
					$total_nilaiaset=$total_nilaiaset+$nilaiaset;
				}
			?>
		</tbody>
		<tr>
			<td colspan="12">
				TOTAL NILAI ASET= Rp. <?php echo number_format($total_nilaiaset,2,",","."); ?>
			</td>
		</tr>
	</table>
</div>
<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
<script type="text/javascript">
	$(function(){
		$("#btn_eksport").click(function(){
			var urlprint="laporan/kartu_stok/printOut";
			window.open(urlprint);		
		})

		$("#pagelistkartu_stok").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_kartu_stok').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
</style>
	<!-- bag. isi -->
<div id="pagelistkartu_stok">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered">
		<thead>
		<tr>
			<th colspan="11"><?php //print_r($result[0]->nama_generik);
									//$result; ?></th>
		</tr>
		<tr class="active">
			<th>No.</th>
			<th>Tanggal</th>
			<th>Kode Obat</th>
			<th>No. Batch</th>
			<th>Nama</th>
			<th>Sediaan</th>
			<th>Dari/Ke</th>
			<th>Sumber Dana</th>
			<th>Penerimaan</th>
			<th>Pengeluaran</th>
			<th>Saldo</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				$saldo=0;
				foreach ($result as $rows) {
					$saldo=($saldo+$rows->penerimaan)-$rows->pengeluaran;
					echo '<tr>
					<td>'.$i.'</td>
					<td>'.date('Y-m-d',strtotime($rows->tanggal)).'</td>
					<td>'.$rows->kode_obat.'</td>
					<td>'.$rows->no_batch.'</td>
					<td>'.$rows->nama_obj.'<br><small>'.$rows->pabrik.'</small></td>
					<td>'.$rows->deskripsi.'</td>
					<td>'.$rows->alur.'</td>
					<td>'.$rows->sumber_dana.' '.$rows->tahun_anggaran.'</td>
					<td>'.$rows->penerimaan.'</td>
					<td>'.$rows->pengeluaran.'</td>
					<td>'.$saldo.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
		<tr>
			<td colspan="11"><?php //print_r($result[0]->nama_generik);
									//$result; ?></td>
		</tr>
	</table>
	<button id="btn_eksport" class="btn btn-success"><span class="glyphicon glyphicon-export"></span> Eksport</button>
</div>
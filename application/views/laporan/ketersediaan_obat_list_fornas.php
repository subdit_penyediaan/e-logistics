<style type="text/css">
	.dataTables_filter{
		float:right;
	}	
	.pagination{
		float:right;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#konten_table_fornas').dataTable();
		$("#pagelistfornas").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_fornas').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistfornas">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-bordered" id="konten_table_fornas">
		<thead>
		<tr class="active">
			<th>ID Fornas</th>
			<th>Parent</th>
			<th>Nama Fornas</th>
			<th>Stok Akhir</th>
			<th>Penggunaan Rata-rata <br>(<?php echo $bulan_pembagi; ?> bulan terakhir)</th>
			<th>Ketersediaan (bulan)</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result_pengeluaran as $key) {
					$data[$key->id_fornas]=$key->jumlah_penggunaan;
				}
				foreach ($result as $rows) {
					if(isset($data[$rows->id_fornas])){
						$penggunaan=$data[$rows->id_fornas];
					}else{ $penggunaan=0;}
					//$rata=$rows->jumlah_penggunaan/$rows->cur_month+0.01;
					//$rata=$rows->jumlah_penggunaan/$bulan_pembagi;
					if($bulan_pembagi>0){
						$rata=$penggunaan/$bulan_pembagi;	
					}else{
						$rata=0;
					}
					if($rata<1){
						$ketersediaan='tak berhingga';
						echo '<tr class="info">';
					}else{
						$ketersediaan = floor($rows->jumlah_stok/$rata);

						if ($ketersediaan >= 6){
							echo '<tr class="info">';	
						}elseif (($ketersediaan < 6) && ($ketersediaan >= 4)){
							echo '<tr class="warning">';
						}else {
							echo '<tr class="danger">';
						};
					}
					echo '<td>'.$rows->id_fornas.'</td>
					<td>'.$rows->parent.'</td>
					<td>'.$rows->nama_fornas.'</td>
					<td>'.$rows->jumlah_stok.'</td>
					<td>'.floor($rata).'</td>
					<td>'.$ketersediaan.'</td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
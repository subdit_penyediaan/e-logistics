<html>
<head>
  <link rel="icon" type="image/ico" href="<?php echo $base_url; ?>img/Logo-Kemenkes-2017-1.png">
  <title>Login V3.3</title>
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery-ui-1.10.3.css">
  <!--script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.js"></script-->
  <script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-1.9.1.js"></script>
  <script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-ui-1.10.3.js"></script>
  <style type="text/css">
    #bingkailogin{
    border:2px solid #a1a1a1;
    /*border-radius:10px;*/
    padding:5px 5px;
    box-shadow:0px 0px 6px 6px #ccc;
    width:500px;
	height:450px;
	position:fixed;
    top:50%;
	left:50%;
	margin-top:-230px;
	margin-left:-255px;
    }
    #headerlogin{
      color: white;
      background: #1e5799;
      padding:10px 10px;
      margin-bottom: 20px;
    }
    #mainlogin{
      padding-left: 20px;
      padding-right: 20px;
    }
    #footerlogin{
      font-size: 10px;
    }
</style>
</head>
<body>
  <div id="bingkailogin">
    <div id="headerlogin">
      <img style="float:left; margin-right:20px;" src="<?php echo $base_url; ?>img/Logo-Kemenkes-2017-1.png" width="80px">
      <h3>LOGIN</h3>APLIKASI E-LOGISTIK OBAT DAN PERBEKALAN<br>KEMENTERIAN KESEHATAN REPUBLIK INDONESIA</h4>
    </div>
      <div id="mainlogin">
        <form class="form-horizontal" role="form" action="<?php echo $base_url; ?>index.php/login/signin" method="post">
          <div style="color:red;">
            <i><?php echo $pesanerror; ?></i>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="" placeholder="Username" name="username">
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password">
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-user"></span> Sign in</button>
            </div>
          </div>
        </form>
      </div>
    <div id="footerlogin">
      <hr><center>
      E-Logistics v3.4<br>
      2022
      </center>
    </div>
</div>
</body>
</html>

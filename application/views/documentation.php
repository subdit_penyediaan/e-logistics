<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dokumentasi API Bank Data E-logistik Kemenkes RI</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	#title {
		color: #444;
		background-color: transparent;		
		font-size: 19px;
		font-weight: normal;
		margin-left: 30px;
		padding: 10px;		
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
		/*width: 50%;*/
	}

	.body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	.container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div class="container" style="width:30%;float:right;margin-top:80px;margin-right:50px;background-color:white;">
	<h1>Index</h1>
	<div class="body" style="">
		<code>
			Transaksi
			<ul>
				<li>Melihat data LPLPO puskesmas</li>
				<li>Mengirim data LPLPO puskesmas</li>
				<li>Melihat SBBK obat</li>
				<li>Melihat template LPLPO</li>
				<li>Melihat data RKO Dinas Kesehatan</li>
			</ul>
			Masterdata
			<ul>
				<li>Obat dan Alkes INN</li>	
			</ul>
			Laporan
			<ul>
				<li>SIMBADA (Badan Aset Daerah)</li>	
			</ul>
		</code>
	</div>	
</div>
<div class="container">
	<h1>
		<img src="<?= base_url();?>img/Logo-Kemenkes-2017-1.png" width="30px" style="float:left;">
		<div id="title">Dokumentasi API E-logistik Kemenkes RI</div>
	</h1>

	<div class="body">
		<p>(1) Melihat data LPLPO Puskesmas</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/lplpo/drugs<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload: </b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics
          "content": {
              "area": "zzz",		// kode puskesmas versi lama dari pusdatin kemenkes
              "periode": "2017-02"	// periode pelaporan yyyy-mm
            }
        }
    }
</pre>
		</code>

		<p>(2) Mengirim data LPLPO</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/lplpo/reports<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload:</b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics
          "content": {
              "area": "P1234567",				// kode puskesmas versi lama dari pusdatin kemenkes
              "periode": "2019-03",				// periode pelaporan mm_yyyy              
              "table":[
                    {
                        "kode_generik": "956",
                        "nama_obj": "ASAM GLUKONAT|ASAM SITRAT|ASAM FERULAT|ASAM ASKORBAT INJEKSI 10 ML",
                        "sediaan": "Tablet",
                        "stok_awal": "3870",
                        "terima": "3000",
                        "sedia": "6870",
                        "pakai": "2464",
                        "rusak_ed": "0",
                        "stok_akhir": "4406",
                        "stok_opt": "2464",
                        "permintaan": "3000",
                        "pemberian": "3000",
                        "ket": ""
                    },
                    ...
                    {
                        "kode_generik": "51",
                        "nama_obj": "ACETYLCYSTEINE TABLET 200 MG",
                        "sediaan": "CAPSUL",
                        "stok_awal": "1352",
                        "terima": "1500",
                        "sedia": "2852",
                        "pakai": "1075",
                        "rusak_ed": "0",
                        "stok_akhir": "1777",
                        "stok_opt": "1075",
                        "permintaan": "2400",
                        "pemberian": "2400",
                        "ket": ""
                    }
                ]
            }
        }
    }
</pre>
		</code>

		<p>(3) Melihat data SBBK</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/lplpo/sbbk<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload:</b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"admin_puskesmas","password":"rahasia","area":"P1971021201"},
          "content": {
              "area": "3403",				// kode wilayah 4 digit untuk kabupaten/kota, jika provinsi 2 digit terakhir ditambahkan "00" sesuai kode dari kemendagri
              "periode": "2017-11"			// periode pelaporan yyyy-mm
            }
        }
    }
</pre>
		</code>		

		<p>(4) Melihat template LPLPO</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/lplpo/templates<br>
			<b>Method:</b> GET<br>
			<b>Header:</b> Content/type: application/json<br>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics. area diisi kode puskesmas
          "content": {
              "area": "zzz",		// kode puskesmas versi lama dari pusdatin kemenkes
              "periode": "2017-02"	// periode pelaporan yyyy-mm
            }
        }
    }
</pre>
		</code>
		<p>(5) Melihat data RKO Dinas Kesehatan</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/rko/drugs<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload: </b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics
          "content": {
              "area": "zzz",		// kode wilayah kabupaten/kota atau provinsi
              "periode": "2017"	// periode pelaporan yyyy
            }
        }
    }
</pre>
		</code>

		<p>(6) Masterdata INN</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/masterdata/inn<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload: </b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics
          "content": {
              "area": "zzz"		// kode wilayah kabupaten/kota atau provinsi
            }
        }
    }
</pre>
		</code>
		<p>(7) Simbada</p>
		<code>
			<b>Url:</b> (base url)/e-logistics/api/custom/simbada<br>
			<b>Method:</b> POST<br>
			<b>Header:</b> Content/type: application/json<br>
			<b>Payload: </b>
<pre>
    {
        "dataEnvironment":
        {
          "token":{"username":"xxx","password":"yyy","area":"zzz"},	// username dan password aplikasi e-logistics
          "content": {
              "area": "zzz"		// kode wilayah kabupaten/kota atau provinsi
              "periode": "2017-06"	// periode pelaporan yyyy, boleh dikosongi seperti "periode": "" untuk mendapatkan semua data tanpa di filter
            }
        }
    }
</pre>
		</code>

	<p>Keterangan tambahan: </p>
	<code>
		<ul>
			<li><b>password menggunakan enkripsi base64</b></li>
			<li><i>*jika output yang diinginkan berformat xml maka tiap url ditambahkan /format/xml</i></li>
			<li>Setiap variable dalam payload adalah <b style="color:red">required</b></li>
		</ul>
	</code>

	</div>

	<p class="footer">		
		If you want to explor this, you should start by reading the <a href="documentation/">API Documentation</a>.

		Page rendered in <strong>{elapsed_time}</strong> seconds. 
		API Version <strong>2.0</strong>
	</p>
</div>

</body>
</html>
<style type="text/css">
	tr{
		padding: 3px;
	}
	td{
		padding: 3px;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	//form submit
		$('#form_add_master_alkes').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            dataType: 'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                //if(data.con==0){
	                	alert(data.text);	
	                //}else{
	                //	alert(data.confirm);
		            //    var url='masterdata/data_obat';
		            //    $('#konten').load(url);	
	                //}
	            }
			})
			
		});
		//end form submit

	//btn cancel
	$('#btn_cancel_add_master').click(function(event){
		event.preventDefault();
		var url='masterdata/data_obat';
		$('#konten').load(url);
	})
	//end button cancel

	//autocomplete pbf
	$('#produsen').autocomplete({
		source:'masterdata/data_obat/get_list_pabrik', 
		minLength:2,
	});
	//end autocomplete pbf
	//$('.inputText').hide();
	//autocomplete FDA
	$('#nama_fda').autocomplete({
		source:'masterdata/data_obat/get_list_fda', 
		minLength:2,
		focus: function(event,ui){
			$('#nama_fda').val(ui.item.value);
			return false;
		},
		select: function(evt, ui){
			$('#kode_fda').val(ui.item.kode_fda);
			return false;
		}
	});
	//end autocomplete FDA

	$('#btn_ubah_data').click(function(){
		$('.inputText').show();
		$('.labelText').hide();
	})

});

function autofill(){
	
}
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Tambah Master Sediaan</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">		
		<!-- bag. isi -->
	<form action="<?php echo $base_url; ?>index.php/masterdata/data_obat/input_data" method="post" id="form_add_master_alkes" name="form_add_master_alkes">
		<br><table class="table">
			<tr>
				<td>Kode Obat</td>
				<td>:<label id=""> <?php echo $id_obat;?> </label>
					<input type="hidden" id="new_kode_master" name="new_kode_master" value="<?php echo $id_obat;?>"></td>
			</tr>
			<tr>
				<td>Barcode</td>
				<td>: <input class="inputText" type="text" value="" name="barcode" id="barcode"></td>
			</tr><tr>
				<td>Nama Lengkap</td>
				<td>: <input class="inputText" type="text" value="" size="50" name="nama_obat" id="nama_obat"></td>
			</tr><tr>
				<td>Kode Binfar</td>
				<td>: <input class="inputText" type="text" value="" name="kode_binfar" id="kode_binfar"></td>
			</tr><tr>
				<td>Nomor Registrasi</td>
				<td>: <input class="inputText" type="text" value="" name="no_reg" id="no_reg"></td>
			</tr><tr>
				<td>Konsep</td>
				<td>: <select name="kategori_objek" id="kategori_objek">
						<option value="ALKES">Alat kesehatan</option>
					</select></td>
			</tr><tr>
				<td>Nama Dagang</td>
				<td>: <input class="inputText" type="text" value="" name="nama_dagang" id="nama_dagang" size="50"></td>
			</tr><tr>
				<td>Nama FDA</td>
				<td>: <input class="inputText" type="text" value="" size="50" name="nama_fda" id="nama_fda">
					<input class="inputText" type="hidden" value="" name="kode_fda" id="kode_fda"></td>	
			</tr><tr>
				<td>Ukuran Kemasan</td>
				<td>: <input class="inputText" type="text" name="text_kemasan" id="text_kemasan">
					<select class="inputText" name="kemasan" id="kemasan">
						<option value="">------ Pilih ------</option>
						<?php for($i=0;$i<sizeof($satuan);$i++) :?>
							<option value="<?php echo $satuan[$i]['id']; ?>">
								<?php echo $satuan[$i]['satuan_obat']; ?>
							</option>
							<?php endfor; ?>
					</select></td>
			</tr><tr>
				<td>Bentuk Sediaan</td>
				<td>: <select class="inputText" name="sediaan" id="sediaan">
						<option value="">------ Pilih ------</option>
						<?php for($i=0;$i<sizeof($sediaan);$i++) :?>
							<option value="<?php echo $sediaan[$i]['id']; ?>">
								<?php echo $sediaan[$i]['jenis_sediaan']; ?>
							</option>
							<?php endfor; ?>
						</select> 
					</td>
			</tr><tr>
				<td>Konversi Satuan Jual</td>
				<td>: <!--input class="inputText" type="text" name="val_satuan_jual" id="val_satuan_jual"-->
						<select class="inputText" name="satuan_jual" id="satuan_jual">
							<option value="">------ Pilih ------</option>
							<?php for($i=0;$i<sizeof($satuan);$i++) :?>
							<option value="<?php echo $satuan[$i]['id']; ?>">
								<?php echo $satuan[$i]['satuan_obat']; ?>
							</option>
							<?php endfor; ?>
						</select>
					</td>
			</tr><tr>
				<td>Konversi Satuan Klinis</td>
				<td>: <!--input class="inputText" type="text" name="val_satuan_klin" id="val_satuan_klin"-->
						<select class="inputText" name="satuan_klin" id="satuan_klin">
							<option value="">------ Pilih ------</option>
							<?php for($i=0;$i<sizeof($satuan);$i++) :?>
							<option value="<?php echo $satuan[$i]['id']; ?>">
								<?php echo $satuan[$i]['satuan_obat']; ?>
							</option>
							<?php endfor; ?>
						</select> 
					</td>
			</tr><tr>
				<td>Kategori</td>
				<td>: <select class="inputText" name="kategori" id="kategori">
						<option value="">------ Pilih ------</option>
						<option value="Generik">Generik</option>
						<option value="Generik Bermerek">Generik Bermerek</option>
						<option value="Paten">Paten</option>
						</select> 							
				</td>
			</tr>
			<tr>
				<td>Golongan</td>
				<td>: <select class="inputText" name="gol_obat" id="gol_obat">
						<option value="">------ Pilih ------</option>
						<?php for($i=0;$i<sizeof($golongan);$i++) :?>
							<option value="<?php echo $golongan[$i]['id']; ?>">
								<?php echo $golongan[$i]['nama_golongan']; ?>
							</option>
							<?php endfor; ?>
						</select> 							
				</td>
			</tr>
			<tr><td>Produsen</td>
				<td>: <input class="inputText" type="text" value="" size="50" name="produsen" id="produsen">							
				</td></tr>
			<tr><td>Status</td>
				<td>: <select class="inputText" name="status" id="status">
						<option value="">------ Pilih ------</option>
						<option value="Approved">Approved</option>
						<option value="Pending">Pending</option>
						<option value="Off">Off</option>
						</select> 							
				</td></tr>
			<!--tr><td>Tanggal penetapan</td>
				<td>: <input type="text"></td></tr-->
		</table>
		<div id="buttonSubmit">
			<hr>
			<!--p>pastikan semua field isian terisi</p-->
			<!--button class="labelText" id="btn_ubah_data"><span class="glyphicon glyphicon-pencil"></span> Ubah Data</button-->
			<button type="submit" id="btn_simpan_alkes" class="inputText">
				<span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
				&ensp;
			<button id="btn_cancel_add_master" class="inputText">
				<span class="glyphicon glyphicon-ban_circle"></span> Batal</button>
		</div>
	</form>
	</div>
</div>
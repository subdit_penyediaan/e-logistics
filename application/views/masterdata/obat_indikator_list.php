<script type="text/javascript">
	$(function(){
		$('#form_update').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#upt_modal").modal('toggle');
	                $('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
	                var url_hasil="masterdata/obat_indikator/get_data"
					$("#list_data").load(url_hasil);//+"#list_sedian_obat");
				
	                /*
	                if(data.con==0){
	                	alert(data.confirm);	
	                }else{
	                	alert(data.confirm);
		                var url='manajemenlogistik/stok_opnam';
		                $('#konten').load(url);	
	                }*/
	            }
			})
			
		});
		//end form submit

		$("#pagelistprogram tr").on("dblclick",function (event){
				
				var testindex = $("#pagelistprogram tr").index(this);
				var form_data = { id_indikator: $("#kode_program_"+testindex).val() };
				//var nilai = $("#kode_program_"+testindex).val();
				//alert(nilai);
				//$("#upt_modal").modal('show');

				var url="masterdata/obat_indikator/get_detail/"		
				$.ajax({
					type:"POST",
					url:url,
					data: form_data,
					dataType:'json',
					success:function(data){
						//$('#').text(data.kode_obat);
						$('#upt_nama_indikator').val(data.nama_indikator);
						$('#upt_satuan').val(data.satuan);
						
						$('#upt_kode').val(data.id_indikator);

						$("#upt_modal").modal('show');

					}
				});
				
				//end ajax
			});
			//end pagepuskesmas tr

		$("#pagelistprogram").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_data').load(url);
		});
	});
</script>
<style type="text/css">
	th{
		text-align: center;
	}
</style>
<div id="pagelistprogram">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered table-hover">
		<thead>
		<tr class="active">
			<th width="3%">Select:</th>
			<th width="5%">Nomor</th>
			<th>Nama Program</th>
			<th>Keterangan</th>			
		</tr>
		</thead>
		<tbody>
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;"><td><input type="checkbox" name="chk[]" id="" value="'.$rows->id_indikator.'" class="chk">
					<td><input type="hidden" name="kode_generik" id="kode_program_'.$i.'" value="'.$rows->id_indikator.'">'.$rows->id_indikator.'</td><td>'.$rows->nama_indikator.'</td><td>'.$rows->satuan.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<div class="modal fade" id="upt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Ubah Obat Indikator</h4>
	      </div>
	      <div class="modal-body">
			<form action="<?php echo $base_url; ?>index.php/masterdata/obat_indikator/update_data" method="post" id="form_update">
				<table class="table">
					<tr>
						<td>Nama Obat</td>
						<td>
							<input type="text" name="uptdata[nama_indikator]" id="upt_nama_indikator" size="30" class="form-control"/>
							<input type="hidden" name="uptdata[id_indikator]" id="upt_kode">
						</td>
					</tr>
					<tr>
						<td>Keterangan</td>
						<td>
							<input type="text" name="uptdata[satuan]" id="upt_satuan" size="15" class="form-control"/>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<div class="pagingContainer">
								<button type="submit" name="Simpan" id="" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
							</div>
							
						</td>
					</tr>			
				</table>
			</form>
		</div>
		</div>
	</div>
</div><!-- end modal -->
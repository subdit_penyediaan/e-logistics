 <style>
	
</style>
<script>
$(document).ready(function(){
	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_sediaan").html(data);
	            }
			})
			
		});
		//end form search

	//=========== del button
		

	$("#del_sediaan_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/sediaan_obat/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/sediaan_obat/get_data_sediaan"
					$("#list_sediaan").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_sediaan_btn").on("click",function (event){
			$("#add_sediaan_modal").modal('show');
				//$("#add_sediaan_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/sediaan_obat/get_data_sediaan";
	$('#list_sediaan').load(url);
	
	//============== submit add form

	$("#btn_sediaan_obat").click(function(){
		var url2="masterdata/sediaan_obat/input_data";
		var form_data = {
			nama_sediaan_obat:$("#nama_sediaan_obat").val(),
			
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			//data:{"state="+$("#state1").val(),"bulan="+$("#bulanke").val()},
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_sediaan_modal").modal('toggle');
				var url_hasil="masterdata/sediaan_obat/get_data_sediaan"
				$("#list_sediaan").load(url_hasil);//+"#list_sedian_obat");
				//$("#halaman_sediaan").listview('refresh');//reload(true);
				//$(this).location.reload(true);
				//$("#hasil").html(e);

			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel panel-primary" id="halaman_sediaan">
	<div class="panel-heading">Sediaan Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_sediaan_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Jenis Sediaan Obat</h4>
			      </div>
			      <div class="modal-body">

			<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				
					<table class="table">
						<tr>
							<td>Nama Sediaan</td>
							<td>
								<input type="text" name="nama_sediaan_obat" id="nama_sediaan_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_sediaan_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
								<input type="reset" name="Reset" id="reset" value="Batal" /-->
							</td>
						</tr>
					</table>
						</div>
					</div>
				</div>
			</div>
			<!--/form-->
			<div class="col-lg-8">
				<!--button id="add_sediaan_btn"><span class="glyphicon glyphicon-plus"></span> Tambah Sediaan</button>
				<button id="del_sediaan_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_sediaan-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/sediaan_obat/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 --><br>
			<!--div id="result" class="result">
				<table class="table table-striped">
					<tr>	
						<td width="20%">Select</td>
						<td width="20%">Nomor</td>
						<td width="60%">Nama Sediaan</td>
					</tr>
					<tbody id="list_sediaan_obat">
					</tbody>
				</table>
			</div-->
			<div id="list_sediaan"></div>
		
		
	</div>
</div>
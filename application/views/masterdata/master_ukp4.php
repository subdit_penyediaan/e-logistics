<style>
</style>
<script>
$(document).ready(function(){
	$('#nama_ukp4').autocomplete({
				source:'masterdata/master_ukp4/get_list_ukp4', 
				minLength:2,
				select:function(event,ui){
					$('#id_ukp4').val(ui.item.id_ukp4);
					return false;
				}
			});
	
	$('#cha_master_ukp4-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	
	    	$.ajax({
	    		url:'masterdata/master_ukp4/datatochange/' + id_array[0], 	    		
	    		type:"POST",
	    		select:function(){	    			
	    		},
	    		success:function(ui,item) {
	    			$('#cha_no_faktur').val(ui.item.nama_dok.value);
	    			$('#change_master_ukp4').modal('show');
	    			return false;
	    		}
	    	});
	    }
	})

	//=========== del button
		
	$("#del_master_ukp4_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_ukp4/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_ukp4/get_data_master_ukp4/"+$("#kode_obat_u").val();
					$("#list_master_ukp4").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_master_ukp4_modal').hide();
	$("#add_master_ukp4_btn").on("click",function (event){
			//$("#add_master_ukp4_modal").modal('show');
			$("#form_master_ukp4_modal").slideDown("slow");
			$("#partbutton_ukp4").fadeOut();
				//$("#add_master_ukp4_modal").modal({keyboard:false});
		});
	$("#batal_ukp4").on("click",function (event){
			//$("#add_master_ukp4_modal").modal('show');
			$("#form_master_ukp4_modal").slideUp("slow");
			$("#partbutton_ukp4").fadeIn();
				//$("#add_master_ukp4_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_ukp4/get_data_master_ukp4/"+$("#kode_obat_u").val();
	$('#list_master_ukp4').load(url);	
	//============== submit add form

	$("#btn_master_ukp4").click(function(){
		var url2="masterdata/master_ukp4/input_data";
		var form_data = {
			id_ukp4:$("#id_ukp4").val(),
			kode_obat:$("#kode_obat_u").val()
		}
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_master_ukp4_modal").slideUp("slow");
				$("#partbutton_ukp4").fadeIn();
				var url_hasil="masterdata/master_ukp4/get_data_master_ukp4/"+$("#kode_obat_u").val();
				$("#list_master_ukp4").load(url_hasil);//+"#list_sedian_obat");

				$("#id_ukp4").val("");
				$("#nama_ukp4").val("");
			}
		});
	})

	//============== end submit add form
	
});
</script>
		<!-- bag. isi --><!--br>
		<div id="partbutton_ukp4">
			<button id="add_master_ukp4_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
			<button id="del_master_ukp4_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
		</div-->
			<div class="" id="form_master_ukp4_modal">
			  <h4>Tambah ukp4</h4>		
				<table class="table">
						<tr>
							<td>Nama ukp4</td>
							<td><input type="text" class="form-control" name="nama_ukp4" id="nama_ukp4">
								<input type="hidden" name="id_ukp4" id="id_ukp4">
								<input type="hidden" name="kode_obat_u" id="kode_obat_u" value="<?php echo $kode_obat; ?>"></td>
							
						</tr>
						<tr>
							<td><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_ukp4" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal_ukp4"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							<td>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_master_ukp4"></div>
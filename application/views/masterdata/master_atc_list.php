<script type="text/javascript">
	$(function(){
		$("#pagelistmaster_atc").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_master_atc').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistmaster_atc">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<hr>
	<table class="table table-striped">
		<thead>
		<tr>
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Kode ATC</th>
			<th>Nama ATC (Ger)</th>
			<th>Nama ATC (Eng)</th>
			
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_master_atc" value="'.$rows->id_map.'" class="chk">
							
					</td>
					<td>'.$i.'</td>
					<td>'.$rows->kode_atc.'</td>
					<td>'.$rows->nama_atc.'</td>
					<td>'.$rows->nama_atc_eng.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
	$(function(){
		//form submit
		$('#form_update').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#upt_modal").modal('toggle');
	                $('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
	                var url_hasil="masterdata/master_generik/get_data_master_generik"
					$("#list_master_generik").load(url_hasil);//+"#list_sedian_obat");
				
	                /*
	                if(data.con==0){
	                	alert(data.confirm);	
	                }else{
	                	alert(data.confirm);
		                var url='manajemenlogistik/stok_opnam';
		                $('#konten').load(url);	
	                }*/
	            }
			})
			
		});
		//end form submit

		$("#pagelist tr").on("dblclick",function (event){
				
				var testindex = $("#pagelist tr").index(this);
				var form_data = { kode_generik: $("#kode_generik_"+testindex).val() };
				//var nilai = $("#kode_generik_"+testindex).val();
				//alert(nilai);
				//$("#upt_modal").modal('show');

				var url="masterdata/master_generik/get_detail/"		
				$.ajax({
					type:"POST",
					url:url,
					data: form_data,
					dataType:'json',
					success:function(data){
						//$('#').text(data.kode_obat);
						$('#upt_nama_master_generik').val(data.generik_object);
						$('#upt_dosage_form').val(data.dosage_form);
						
						$('#upt_strength').val(data.strength);
						$('#upt_nama_zat_generik').val(data.nama_zat);
						$('#upt_kode').val(data.id);

						//$( "#dtpk_cha option:selected" ).text(data.dtpk).attr('value',data.dtpk);
						
						$("#upt_modal").modal('show');

					}
				});
				
				//end ajax
			});
			//end pagepuskesmas tr

		$("#pagelist").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_master_generik').load(url);
		});
	});
</script>
<div id="pagelist">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th>Select:</th>
			<th>Nomor</th>
			<th>Nama Generik</th>
			<th>Dosage Form</th>
			<th>Strength</th>
			<th>Nama Zat</th>
		</tr>
		</thead>
		<tbody>
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;"><td><input type="checkbox" name="chk[]" id="cek_del_master_generik" value="'.$rows->id.'" class="chk">
					<td><input type="hidden" name="kode_generik" id="kode_generik_'.$i.'" value="'.$rows->id.'">
						'.$rows->id.'</td>
					<td>'.$rows->generik_object.'</td>
					<td>'.$rows->dosage_form.'</td>
					<td>'.$rows->strength.'</td>
					<td>'.$rows->nama_zat.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<!-- modal -->
<div class="modal fade" id="upt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Ubah Master Generik</h4>
	      </div>
	      <div class="modal-body">

		<form action="<?php echo $base_url; ?>index.php/masterdata/master_generik/update_data" method="post" id="form_update">
		
			<table class="table">
				<tr>
					<td>Nama Generik</td>
					<td>
						<input type="text" name="uptdata[nama_master_generik]" id="upt_nama_master_generik" size="30" class="form-control"/>
						<input type="hidden" name="uptdata[kode_generik]" id="upt_kode">
					</td>
				</tr>
				<tr>
					<td>Dosage Form</td>
					<td>
						<input type="text" name="uptdata[dosage_form]" id="upt_dosage_form" size="15" class="form-control"/>
					</td>
				</tr>
				<tr>
					<td>Strength</td>
					<td>
						<input type="text" name="uptdata[strength]" id="upt_strength" size="15" class="form-control"/>
					</td>
				</tr>
				<tr>
					<td>Nama zat</td>
					<td>
						<input type="text" name="uptdata[nama_zat_generik]" id="upt_nama_zat_generik" size="30" class="form-control"/>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<div class="pagingContainer">
							<button type="submit" name="Simpan" id="btn_master_generik_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
							<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
						</div>
						
					</td>
				</tr>
			</table>
		</form>
			</div>
			</div>
		</div>
	</div><!-- end modal -->
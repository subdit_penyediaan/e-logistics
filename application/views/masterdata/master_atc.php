<style>
</style>
<script>
$(document).ready(function(){
	$('#nama_atc').autocomplete({
				source:'masterdata/master_atc/get_list_atc', 
				minLength:2,
				select:function(event,ui){
					$('#id_atc').val(ui.item.id_atc);
					return false;
				}
			});
	
	$('#cha_master_atc-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	
	    	$.ajax({
	    		url:'masterdata/master_atc/datatochange/' + id_array[0], 	    		
	    		type:"POST",
	    		select:function(){	    			
	    		},
	    		success:function(ui,item) {
	    			$('#cha_no_faktur').val(ui.item.nama_dok.value);
	    			$('#change_master_atc').modal('show');
	    			return false;
	    		}
	    	});
	    }
	})

	//=========== del button
		
	$("#del_master_atc_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_atc/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_atc/get_data_master_atc/"+$("#id_obat_atc").val();
					$("#list_master_atc").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_master_atc_modal').hide();
	$("#add_master_atc_btn").on("click",function (event){
			//$("#add_master_atc_modal").modal('show');
			$("#form_master_atc_modal").slideDown("slow");
			$("#partbutton_atc").fadeOut();
				//$("#add_master_atc_modal").modal({keyboard:false});
		});
	$("#batal_atc").on("click",function (event){
			//$("#add_master_atc_modal").modal('show');
			$("#form_master_atc_modal").slideUp("slow");
			$("#partbutton_atc").fadeIn();
				//$("#add_master_atc_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_atc/get_data_master_atc/"+$("#id_obat_atc").val();
	$('#list_master_atc').load(url);	
	//============== submit add form

	$("#btn_master_atc").click(function(){
		var url2="masterdata/master_atc/input_data";
		var form_data = {
			kode_atc:$("#id_atc").val(),
			kode_obat:$("#id_obat_atc").val(),
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_master_atc_modal").slideUp("slow");
				$("#partbutton_atc").fadeIn();
				var url_hasil="masterdata/master_atc/get_data_master_atc/"+$("#id_obat_atc").val();
				$("#list_master_atc").load(url_hasil);//+"#list_sedian_obat");

				$("#id_atc").val("");
				$("#nama_atc").val("");
			}
		});
	})

	//============== end submit add form
	
});
</script>
		<!-- bag. isi --><!--br>
		<label style="font-color:red;"><i>*pastikan sudah menambah master data obat</i></label>
		<div id="partbutton_atc">
			<button id="add_master_atc_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
			<button id="del_master_atc_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
		</div-->
			<div class="" id="form_master_atc_modal">
			  <h4>Tambah atc</h4>		
				<table class="table">
						<tr>
							<td>Nama atc</td>
							<td><input type="text" class="form-control" name="nama_atc" id="nama_atc">
								<input type="hidden" name="id_atc" id="id_atc" value="">
								<input type="hidden" name="id_obat_atc" id="id_obat_atc" value="<?php echo $kode_obat?>"></td>
							
						</tr>
						<tr>
							<td><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_atc" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal_atc"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							<td>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_master_atc"></div>
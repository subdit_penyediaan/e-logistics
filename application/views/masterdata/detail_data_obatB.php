<style type="text/css">
	tr{
		padding: 3px;
	}
	td{
		padding: 3px;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	//form ubah
        $('#form_ubah_data').submit(function(e){
            event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                //dataType: 'json',
                success: function (data) {
                    alert('DATA BERHASIL DISIMPAN');
                    
                    $('#konten').load('masterdata/data_obat');
                    //$('#menu_page').hide();

                }
            })
            
        });
        //end form

	$('#btn_cancel').click(function(event){
		event.preventDefault();
		var url='masterdata/data_obat';
		$('#konten').load(url);
	})

	//autocomplete pbf
	$('#produsen').autocomplete({
				source:'masterdata/detail_data_obat/get_list_pabrik', 
				minLength:2,
			});
	//end autocomplete pbf

	$('.inputText').hide();

	$('#btn_ubah_data').click(function(){
		alert("Maaf, Anda tidak diberi izin untuk mengubah data.");
		// $('.inputText').show();
		// $('.labelText').hide();
	})
	//$('#detailnamaobat').load('masterdata/detail_data_obat/detail_obat')//url
	//$('#kontenobat').load('masterdata/detail_data_obat/detail_konten_obat')
	$('#form_fornas').load('masterdata/master_fornas',{kode_obat:$('#id_obat_hidden').val()});
	$('#kontenobat').load('masterdata/master_konten',{kode_obat:$('#id_obat_hidden').val()});
	$('#ruteobat').load('masterdata/master_rute',{kode_obat:$('#id_obat_hidden').val()});
	$('#form_atc').load('masterdata/master_atc',{kode_obat:$('#id_obat_hidden').val()});
	//$('#form_fornas').load('masterdata/master_fornas',{kode_obat:$('#new_kode_master').val()});
	$('#form_ukp4').load('masterdata/master_ukp4',{kode_obat:$('#id_obat_hidden').val()});
});

function autofill(){
	
}
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Detail Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#detailnamaobat" data-toggle="tab"><?php echo $result[0]['nama_obat'];//$nama_obat?></a></li>
		  <li><a href="#kontenobat" data-toggle="tab">Konten Obat</a></li>  
		  <li><a href="#ruteobat" data-toggle="tab">Rute Obat</a></li>
		  <li><a href="#form_atc" data-toggle="tab">ATC</a></li>
		  <li><a href="#form_fornas" data-toggle="tab">Formularium Nasional</a></li>
		  <li><a href="#form_ukp4" data-toggle="tab">UKP4</a></li>		  
		</ul> 
		<div class="tab-content">
	  		<div class="tab-pane active" id="detailnamaobat">
	  		<form method="post" action="<?php echo $base_url; ?>index.php/masterdata/detail_data_obat/update_data" id="form_ubah_data" name="form_ubah_data">
				<br><table class="table">
					<tr>
						<td>Kode Obat</td>
						<td>: <label><?php echo $result[0]['id_obat'];//$id_obat;?> </label>
							<input type="hidden" name="upt[id_obat]" id="id_obat_hidden" value="<?php echo $result[0]['id_obat'];//$id_obat;?>"></td>
					</tr>
					<tr>
						<td>Barcode</td>
						<td>: <input class="inputText" type="text" value="<?php echo $result[0]['barcode'];//$barcode;?>" name="upt[barcode]" id="barcode">
							<label class="labelText"><?php echo $result[0]['barcode'];//$barcode;?> </label></td>
					</tr><tr>
						<td>Nama Lengkap</td>
						<td>: <input class="inputText" type="text" value="<?php echo $result[0]['object_name'];//$object_name;?>" size="70" name="upt[object_name]" id="nama_obat">
							<label class="labelText"><?php echo $result[0]['object_name'];//$object_name;?> </label></td>
					</tr><tr>
						<td>Kode Binfar</td>
						<td>: <input class="inputText" type="text" value="<?php echo $result[0]['kode_binfar'];//$kode_binfar;?>" name="upt[kode_binfar]" id="kode_binfar">
							<label class="labelText"><?php echo $result[0]['kode_binfar'];//$kode_binfar;?> </label></td>
					</tr><!--tr>
						<td>Kelompok Registrasi</td>
						<td>: <input class="inputText" type="text" value="" name="kel_reg" id="kel_reg">
							<label class="labelText"><?php ?> </label></td>
					</tr><tr>
						<td>Nomor Registrasi</td>
						<td>: <input class="inputText" type="text" value="<?php //echo $id_reg;?>" name="no_reg" id="no_reg">
							<label class="labelText"><?php //echo $id_reg;?> </label></td>
					</tr-->
					<tr>
						<td>Konsep</td>
						<td>: <label class="labelText"><?php echo $result[0]['kategori_objek'];?> </label>
							<select name="upt[kategori_objek]" id="kategori_objek" class="inputText">
								<option value="<?php echo $result[0]['kategori_objek'];?>"><?php echo $result[0]['kategori_objek'];?></option>
								<!--option value="">--------- Pilih ---------</option-->
								<option value="OBAT">Obat</option>
								<option value="BMHP">Bahan Medis Habis Pakai</option>								
							</select></td>
					</tr><tr>
						<td>Nama Dagang</td>
						<td>: <input class="inputText" type="text" value="<?php echo $result[0]['nama_obat'];?>" name="upt[nama_obat]" id="nama_dagang">
							<label class="labelText"><?php echo $result[0]['nama_obat'];?> </label></td>
					</tr><tr>
						<td>Ukuran Kemasan</td>
						<td>: <label class="labelText"><?php $result[0]['detil_kemasan'];//$detil_kemasan?> </label>
							<!--input class="inputText" type="text"-->
							<select class="inputText" name="upt[detil_kemasan]" id="kemasan">
								<option value="">------ Pilih ------</option>
								<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
							</select>
							</td>
					</tr><tr>
						<td>Bentuk Sediaan</td>
						<td>: <select class="inputText" name="upt[deskripsi]" id="sediaan">
								<option value="<?php echo $result[0]['deskripsi'];//$deskripsi;?>"><?php echo $result[0]['deskripsi'];//$deskripsi;?></option>
								<!--option value="">------ Pilih ------</option-->
								<?php for($i=0;$i<sizeof($sediaan);$i++) :?>
									<option value="<?php echo $sediaan[$i]['jenis_sediaan']; ?>">
										<?php echo $sediaan[$i]['jenis_sediaan']; ?>
									</option>
									<?php endfor; ?>
								</select>
							<label class="labelText"><?php echo $result[0]['deskripsi'];//$deskripsi;?> </label></td>
					</tr><tr>
						<td>Satuan Besar</td>
						<td>: <select class="inputText" name="upt[satuan_besar_unit]" id="sat_bes">
								<option value="<?php echo $result[0]['satuan_besar_unit'];//$satuan_besar_unit;?>"><?php echo $result[0]['satuan_besar_unit'];//$satuan_besar_unit;?></option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select> 
							<label class="labelText"><?php echo $result[0]['satuan_besar_unit'];//$satuan_besar_unit;?> </label></td>
					</tr><tr>
						<td>Konversi Satuan Jual</td>
						<td>: <!--input class="inputText" type="text" name="val_satuan_jual" id="val_satuan_jual"-->
								<select class="inputText" name="upt[satuan_jual_unit]" id="satuan_jual">
									<option value="<?php echo $result[0]['satuan_jual_unit'];//$satuan_jual_unit;?>"><?php echo $result[0]['satuan_jual_unit'];//$satuan_jual_unit;?></option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select>
							<label class="labelText"><?php echo $result[0]['satuan_jual_unit'];//$satuan_jual_unit;?> </label>
							</td>
					</tr><tr>
						<td>Konversi Satuan Klinis</td>
						<td>: <!--input class="inputText" type="text" name="upt[val_satuan_klin" id="val_satuan_klin"-->
								<select class="inputText" name="upt[satuan_klinis_unit]" id="satuan_klin">
									<option value="<?php echo $result[0]['satuan_klinis_unit'];//$satuan_klinis_unit;?>"><?php echo $result[0]['satuan_klinis_unit'];//$satuan_klinis_unit;?></option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select> 
							<label class="labelText"><?php echo $result[0]['satuan_klinis'];//$satuan_klinis;?> <?php echo $result[0]['satuan_klinis_unit'];//$satuan_klinis_unit;?> </label></td>
					</tr><tr>
						<td>Kategori</td>
						<td>: <select class="inputText" name="upt[kategori]" id="kategori">
								<option value="<?php echo $result[0]['kategori'];//$kategori;?>"><?php echo $result[0]['kategori'];//$kategori;?></option>
								<option value="Generik">Generik</option>
								<option value="Generik Bermerek">Generik Bermerek</option>
								<option value="Paten">Paten</option>
								</select> 
							<label class="labelText"><?php echo $result[0]['kategori'];//$kategori;?> </label>
						</td>
					</tr>
					<!--tr><td>Kata Kunci</td>
						<td>: <input type="text"></td></tr-->
					<!--tr><td>Image</td>
						<td>: <input type="file"></td></tr-->
					<tr>
						<td>Golongan</td>
						<td>: <label class="labelText"><?php echo $result[0]['gol_obat'];//$kategori;?> </label>
							<select class="inputText" name="upt[gol_obat]" id="gol_obat">
								<option value="<?php echo $result[0]['gol_obat'];//$kategori;?>"><?php echo $result[0]['gol_obat'];//$kategori;?></option>
								<?php for($i=0;$i<sizeof($golongan);$i++) :?>
									<option value="<?php echo $golongan[$i]['nama_golongan']; ?>">
										<?php echo $golongan[$i]['nama_golongan']; ?>
									</option>
									<?php endfor; ?>
								</select> 							
						</td>
					</tr>
					<tr><td>Jenis Obat</td>
						<td>: <label class="labelText"><?php echo $result[0]['jenis_obat'];//$kategori;?> </label>
							<select class="inputText" name="upt[jenis_obat]" id="jenis_obat">
								<option value="<?php echo $result[0]['jenis_obat'];?>"><?php echo $result[0]['jenis_obat'];?></option>
								<option value="Tunggal">Tunggal</option>
								<option value="Campuran">Campuran</option>
								</select> 							
						</td></tr>
					<tr><td>Produsen</td>
						<td>: <input class="inputText" type="text" value="<?php echo $result[0]['org_pembuat'];//$org_pembuat;?>" size="70" name="upt[org_pembuat]" id="produsen">
							<label class="labelText"><?php echo $result[0]['org_pembuat'];//$org_pembuat;?> </label>
						</td></tr>
					<tr><td>Status</td>
						<td>: <select class="inputText" name="upt[status]" id="status">
								<option value="<?php echo $result[0]['status'];?>"><?php echo $result[0]['status'];?></option>
								<option value="Approved">Approved</option>
								<option value="Pending">Pending</option>
								<option value="Off">Off</option>
								</select>
							<label class="labelText"><?php echo $result[0]['status'];//$status;?> </label>
						</td></tr>
					<!--tr><td>Tanggal penetapan</td>
						<td>: <input type="text"></td></tr-->
				</table>
				<div id="buttonSubmit">
					<hr>
					<!--p>pastikan semua field isian terisi</p-->					
					<button type="submit" id="btn_simpan" class="inputText"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
					&ensp;<button id="btn_cancel" class="inputText"><span class="glyphicon glyphicon-ban_circle"></span> Batal</button>
				</div>
			</form>
				<button class="labelText" id="btn_ubah_data"><span class="glyphicon glyphicon-pencil"></span> Ubah Data</button>
			</div><!-- end div detailnamaobat -->
			<div class="tab-pane" id="kontenobat">
				
			</div><!-- end div kontenobat -->
			<div class="tab-pane" id="ruteobat">
				
			</div><!-- end div ruteobat -->
			<div class="tab-pane" id="form_atc">
				<?php 
					for($i=0;$i<sizeof($result);$i++){
						//echo $result[$i]['nama_atc'].'<br>';
					} 
				?>
				<?php //echo $data['nama_atc']?>
			</div><!-- end div atc -->
			<div class="tab-pane" id="form_fornas">
				<?php 
					for($i=0;$i<sizeof($result);$i++){
						//echo $result[$i]['nama_fornas'].'<br>';
					} 
				?>
			</div><!-- end div fornas -->
			<div class="tab-pane" id="form_ukp4">
				<?php 
					for($i=0;$i<sizeof($result);$i++){
						//echo $result[$i]['nama_ukp4'].'<br>';
					} 
				?>
			</div><!-- end div ukp4 -->			
		</div><!-- end div tab conten-->

		
	</div>
</div>
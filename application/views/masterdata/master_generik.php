 <style>
	
</style>
<script>
$(document).ready(function(){

	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                //$("#add_master_generik_modal").modal('toggle');
	                //var url_hasil="masterdata/master_generik/get_data_master_generik"
					//$("#list_master_generik").load(url_hasil);//+"#list_sedian_obat");
					$("#list_master_generik").html(data);
	            }
			})
			
		});
		//end form search

	//=========== del button
		

	$("#del_master_generik_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_generik/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_generik/get_data_master_generik"
					$("#list_master_generik").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_master_generik_btn").on("click",function (event){
			$("#add_master_generik_modal").modal('show');
				//$("#add_master_generik_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_generik/get_data_master_generik";
	$('#list_master_generik').load(url);
	
	//============== submit add form
	/*
	$("#btn_master_generik_obat").click(function(){
		var url2="masterdata/master_generik/input_data";
		var form_data = {
			nama_master_generik_obat:$("#nama_master_generik_obat").val(),
			
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			//data:{"state="+$("#state1").val(),"bulan="+$("#bulanke").val()},
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_master_generik_modal").modal('toggle');
				var url_hasil="masterdata/master_generik/get_data_master_generik"
				$("#list_master_generik").load(url_hasil);//+"#list_sedian_obat");
				//$("#halaman_master_generik").listview('refresh');//reload(true);
				//$(this).location.reload(true);
				//$("#hasil").html(e);

			}
		});
	})
	*/
	//============== end submit add form
	//form submit
		$('#form_input').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#add_master_generik_modal").modal('toggle');
	                var url_hasil="masterdata/master_generik/get_data_master_generik"
					$("#list_master_generik").load(url_hasil);//+"#list_sedian_obat");
				
	                /*
	                if(data.con==0){
	                	alert(data.confirm);	
	                }else{
	                	alert(data.confirm);
		                var url='manajemenlogistik/stok_opnam';
		                $('#konten').load(url);	
	                }*/
	            }
			})
			
		});
		//end form submit
	
});

</script>
<div class="panel panel-primary" id="halaman_master_generik">
	<div class="panel-heading">Daftar Obat Generik</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_master_generik_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Master Generik</h4>
			      </div>
			      <div class="modal-body">

				<form action="<?php echo $base_url; ?>index.php/masterdata/master_generik/input_data" method="post" id="form_input">
				
					<table class="table">
						<tr>
							<td>Nama Generik</td>
							<td>
								<input type="text" name="indata[nama_master_generik_obat]" id="nama_master_generik_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Dosage Form</td>
							<td>
								<input type="text" name="indata[dosage_form]" id="dosage_form" size="15" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Strength</td>
							<td>
								<input type="text" name="indata[strength]" id="strength" size="15" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Nama zat</td>
							<td>
								<input type="text" name="indata[nama_zat_generik]" id="nama_zat_generik" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_generik_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								
							</td>
						</tr>
					</table>
				</form>
					</div>
					</div>
				</div>
			</div><!-- end modal -->
			
			<div class="col-lg-8">
				<!--button id="add_master_generik_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_master_generik_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_master_generik-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/master_generik/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 -->
			<br>
			<div id="list_master_generik"></div>
		
		
	</div>
</div>
 <style>
	
</style>
<script>
$(document).ready(function(){

	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_master_alkesgen").html(data);
	            }
			})
			
		});
		//end form search

	//=========== del button
		

	$("#del_master_alkesgen_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_alkesgen/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_alkesgen/get_data_master_alkesgen";
					$("#list_master_alkesgen").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_master_alkesgen_btn").on("click",function (event){
			$("#add_master_alkesgen_modal").modal('show');
				//$("#add_master_alkesgen_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_alkesgen/get_data_master_alkesgen";
	$('#list_master_alkesgen').load(url);
	
	//form submit
		$('#form_input').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#add_master_alkesgen_modal").modal('toggle');
	                var url_hasil="masterdata/master_alkesgen/get_data_master_alkesgen"
					$("#list_master_alkesgen").load(url_hasil);//+"#list_sedian_obat");

	            }
			})
			
		});
		//end form submit
	
});

</script>
<div class="panel panel-primary" id="halaman_master_alkesgen">
	<div class="panel-heading">Daftar Alat Kesehatan Generik</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_master_alkesgen_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Master Alat Kesehatan Generik</h4>
			      </div>
			      <div class="modal-body">

				<form action="<?php echo $base_url; ?>index.php/masterdata/master_alkesgen/input_data" method="post" id="form_input">
				
					<table class="table">
						<tr>
							<td>Kode</td>
							<td>
								<input type="text" name="indata[kode_alkesgen]" id="kode_alkesgen" size="30" class="form-control input-sm"/>
							</td>
						</tr>
						<tr>
							<td>Nama Alkes</td>
							<td>
								<input type="text" name="indata[nama_alkesgen]" id="nama_master_alkesgen_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Parent</td>
							<td>
								<input type="text" name="indata[parent]" id="parent" size="15" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Selected</td>
							<td>
								<select name="indata[selected]" id="selected_ornot">
									<option value="yes">YES</option>
									<option value="no">NO</option>
								</select>								
							</td>
						</tr>
						<tr>
							<td>FDA Code</td>
							<td>
								<input type="text" name="indata[fda_code]" id="fda_code" size="15" class="form-control inp-sm"/>
							</td>
						</tr>
						<tr>
							<td>FDA Name</td>
							<td>
								<input type="text" name="indata[fda_name]" id="fda_name" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_alkesgen_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								
							</td>
						</tr>
					</table>
				</form>
					</div>
					</div>
				</div>
			</div><!-- end modal -->
			
			<div class="col-lg-8">
				<!--button id="add_master_alkesgen_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_master_alkesgen_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_master_alkesgen-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/master_alkesgen/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 -->
			<br>
			<div id="list_master_alkesgen"></div>
		
		
	</div>
</div>
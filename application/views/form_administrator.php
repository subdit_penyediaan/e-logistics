<div class="panel panel-primary">
<div class="panel-heading">Form Administrator</div>
  <div id="up-konten"class="panel-body" style="padding:15px;">
<form class="form-horizontal" role="form">
  <div class="form-group">
    <label class="col-sm-4 control-label">Username</label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Kategori User</label>
    <div class="col-sm-8">
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          Action <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Administrator</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Nama Lengkap</label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Kontak Person</label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Email</label>
    <div class="col-sm-8">
      <input type="text" class="form-control">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Konfirmasi Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Status</label>
    <div class="col-sm-8">
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
          Action <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Aktif</a></li>
          <li><a href="#">Non-Aktif</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-4 control-label">Reset Password</label>
    <div class="col-sm-8">
      <div class="input-group">
        <input type="text" class="form-control">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">Go!</button>
        </span>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">
      <button type="submit" class="btn btn-default">Simpan</button>
    </div>
  </div>
</form>
</div>
</div>
</div>
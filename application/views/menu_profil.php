<li class='has-sub'><a href='#'><span>Profil</span></a>
	<ul>
	    <li class='last'><a href="<?php echo $base_url; ?>index.php/profil/setting_institusi"><span class="glyphicon glyphicon-arrow-right"></span> Setting Institusi</a></li>
		<!--li class='last'><a href="<?php echo $base_url; ?>index.php/mainpage/administrator"><span class="glyphicon glyphicon-arrow-right"></span> Administrator</a></li-->
		<li><a href="<?php echo $base_url; ?>index.php/profil/unit_penerima"><span class="glyphicon glyphicon-arrow-right"></span> Unit Penerima</a></li>
		<!--li><a href="<?php echo $base_url; ?>index.php/not_found"><span class="glyphicon glyphicon-arrow-right"></span> Sinkronisasi Data</a></li>
		<li><a href="<?php echo $base_url; ?>index.php/mainpage/settingModul"><span class="glyphicon glyphicon-arrow-right"></span> Setting Modul</a></li-->
		<li><a href="<?php echo $base_url; ?>index.php/profil/manajemen_user"><span class="glyphicon glyphicon-arrow-right"></span> Manajemen User</a></li>
		<li><a href="<?php echo $base_url; ?>index.php/profil/manajemen_db"><span class="glyphicon glyphicon-arrow-right"></span> Manajemen Database</a></li>
		<!--li><a href="<?php echo $base_url; ?>index.php/not_found"><span class="glyphicon glyphicon-arrow-right"></span> Value Stok Optimum</a></li-->
	</ul>
</li>
<li style="" class='has-sub'><a href="#">Master Data</a>
	<ul>
		<!--li><a href='<?php echo $base_url; ?>index.php/manajemen_obat'><span class="glyphicon glyphicon-arrow-right"></span> Data Obat</a></li-->
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/data_obat'><span class="glyphicon glyphicon-arrow-right"></span> Data Sediaan</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/konten_obat'><span class="glyphicon glyphicon-arrow-right"></span> Konten Obat</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/sediaan_obat'><span class="glyphicon glyphicon-arrow-right"></span> Sediaan Obat</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/rute_obat'><span class="glyphicon glyphicon-arrow-right"></span> Rute Pemberian Obat</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/gol_obat'><span class="glyphicon glyphicon-arrow-right"></span> Golongan Obat</a></li>
		<!--li><a href='<?php echo $base_url; ?>index.php/masterdata/ukp4'><span class="glyphicon glyphicon-arrow-right"></span> Data Obat Indikator 2014</a></li-->
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/obat_fornas'><span class="glyphicon glyphicon-arrow-right"></span> Formularium Nasional</a></li> 
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/produsen_obat'><span class="glyphicon glyphicon-arrow-right"></span> Produsen Obat</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/pbf'><span class="glyphicon glyphicon-arrow-right"></span> Data PBF</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/master_generik'><span class="glyphicon glyphicon-arrow-right"></span> Daftar Obat INN</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/master_alkesgen'><span class="glyphicon glyphicon-arrow-right"></span> Daftar Alkes FDA</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/nama_program'><span class="glyphicon glyphicon-arrow-right"></span> Nama Program</a></li>
		<li><a href='<?php echo $base_url; ?>index.php/masterdata/obat_indikator'><span class="glyphicon glyphicon-arrow-right"></span> List Obat Indikator</a></li>
	</ul>
</li>
<li style="" class='has-sub'><a href="#">Data Administrator</a>
	<ul>
		<li><a href='<?php echo $base_url; ?>index.php/laporan/pengeluaran'><span class="glyphicon glyphicon-arrow-right"></span> Data Distribusi</a></li>
	</ul>
</li>
<li style="" class='has-sub'><a href="#">Data Rencana Kebutuhan</a>
	<ul>
		<li><a href='<?= base_url(); ?>manajemenlogistik/rko'><span class="glyphicon glyphicon-arrow-right"></span> Tahunan</a></li>
	</ul>
</li>
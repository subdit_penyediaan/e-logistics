<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/ico" href="<?php echo $base_url; ?>img/Logo-Kemenkes-2017-1.png">
	<!--meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"-->
 <!--meta charset="utf-8"-->
 <title>e-logistics v3.3</title>
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/bootstrap.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery-ui-1.10.3.css">
 	<!-- css menu vertikal -->
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/menuvertikal.css">
 	<!-- css pagination alphabetics -->
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery.listnav-2.1.css">
 	<!--link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/pro_cm.css"-->
 	<!--script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.js"></script-->
 	
 	<!-- css combogrid -->
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery-ui-1.10.1.custom.css">
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/jquery.ui.combogrid.css">


 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-1.9.1.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery-ui-1.10.3.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/bootstrap.min.js"></script>
 	<!-- jquery menu vertikal -->
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/menuvertikal.js"></script>
 	<!-- datepicker -->
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/datepicker/css/datepicker.css"/>
 	<link rel="stylesheet" type="text/css" href="<?php echo $base_url; ?>css/datepicker/less/datepicker.less"/>
	<script src="<?php echo $base_url; ?>css/datepicker/js/bootstrap-datepicker.js"></script>

	<!--script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.autocomplete.js"></script-->
	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.validate2.js"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.browser.js"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.form.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.hotkeys.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.alphanumeric.min.js"></script>
	<script type="text/javascript" src="<?php echo $base_url; ?>js/hoverIntent.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/superfish.js"></script>

 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.ui.combogrid-1.6.3.js"></script>
 	<!-- jquery pagination alphabetic -->
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.cookie.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.listnav.min-2.1.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.blockUI.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/datatables.js"></script>
 	<script type="text/javascript" src="<?php echo $base_url; ?>js/datatable.bootstrap.js"></script>

 	<!-- <script src="https://dhis2-cdn.org/v225-1/plugin/jquery-2.2.4.min.js"></script> -->
  	<!-- <script src="https://dhis2-cdn.org/v225-1/plugin/reporttable.js"></script> -->
  	<!-- <script src="https://dhis2-cdn.org/v225-1/plugin/chart.js"></script> -->
 	
 	<script src="<?php echo $base_url; ?>js/highcharts.js"></script>
	<script src="<?php echo $base_url; ?>js/exporting.js"></script>
	<script src="<?php echo $base_url; ?>js/export-data.js"></script>
	
 	<style type="text/css">
 		#bingkai{
		border:2px solid #a1a1a1;
		border-radius:10px;
		padding:10px 10px; 
		box-shadow:0px 0px 6px 6px #ccc;
		margin: 10px; 
		}
		#kop{
			/*background-color: #428bca;*/
			color: white;
			border-radius:0px 0px 0px 0px;
			background: #1e5799; /* Old browsers */
			/* IE9 SVG, needs conditional override of 'filter' to 'none' */
			background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzFlNTc5OSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjI5JSIgc3RvcC1jb2xvcj0iIzI5ODlkOCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjczJSIgc3RvcC1jb2xvcj0iIzIwN2NjYSIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiM3ZGI5ZTgiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
			background: -moz-linear-gradient(top,  #1e5799 0%, #2989d8 29%, #207cca 73%, #7db9e8 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#1e5799), color-stop(29%,#2989d8), color-stop(73%,#207cca), color-stop(100%,#7db9e8)); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* IE10+ */
			background: linear-gradient(to bottom,  #1e5799 0%,#2989d8 29%,#207cca 73%,#7db9e8 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1e5799', endColorstr='#7db9e8',GradientType=0 ); /* IE6-8 */

		}
		#menupojok ul{
			list-style-type: none;
			margin: 0px;
			padding: 0px;	

		}
		#menupojok ul li a{
			color: white;
			margin: 0px;
			background-color: #428bca;
			padding-top: 6px; 
			padding-bottom: 6px;
			padding-left: 20px;
			display: block;
			text-decoration: none;
			border-bottom: 1px solid;
			font-size: 6px;
		}
		#menupojok li a:hover{
			background-color: red;
			display: block;
		}
		#menupojok a.active {
			background-color: red;
			display: block;
		}
		#running-text{
			background-color: red;
			color: white;
			display: none;
			margin-bottom: 10px;
		}
		.dataTables_filter{
			float:right;
		}
		.pagination{
			float:right;
		}

		.ui-autocomplete {
			max-height: 200px;
			/*overflow-y: auto;*/
			/* prevent horizontal scrollbar */
			overflow-x: hidden;
		}
 </style>
 <script type="text/javascript">
 function showBusySubmit(){
    $('#up-konten').block({
        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
        css: {
            border: 'none',
            backgroundColor: '#cccccc',
            '-webkit-border-radius': '5px',
            '-moz-border-radius': '5px',
            opacity: .6,
            color: '#000',
            width: '130px',
            height: '15px',
            padding: '5px'
        }
    });
}
$(function(){
	var $main = $("#konten");
	$("#cssmenu").on("click","a",function (event){
		$("#cssmenu a.active").removeClass('active');
		$(this).addClass('active');
		event.preventDefault();
		var url = $(this).attr("href");
		$("#konten").load(url);
	});		
});
</script>
</head>
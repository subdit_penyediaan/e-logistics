<script type="text/javascript">
$(document).ready(function(){
	//$("#tabel_up").dataTable();
	$('#btn_update').click(function(){
		var url_update='profil/unit_penerima/update_data';
		var form_data = {
			kec:$('#kec').val(),
			telp:$('#telp_cha').val(),
			desa_cha:$('#desa_cha').val(),
			fax:$('#fax_cha').val(),
			hide_kode_pusk:$('#hide_kode_pusk').val(),
			jenis_pusk:$('#jenis_puskesmas_cha').val(),
			new_kode_pusk:$('#kode_puskesmas_cha').val(),
			poned:$('#poned_cha').val(),
			alamat_pusk:$('#alamat_pusk_cha').val(),
			dtpk:$('#dtpk_cha').val(),
			email:$('#email_cha').val(),
			longi:$('#longi').val(),
			lati:$('#lati').val(),
			buffer:$('#buffer-cha').val(),
			periode_distribusi:$('#periode-distribusi-cha').val(),
			nama_pusk:$('#nama_pusk_cha').val()
		}
		$.ajax({
			type:"POST",
			url:url_update,
			data: form_data,
			success: function(e){
				alert("Data berhasil diupdate");
				$('#myModal').modal('toggle');
				var url_hasil="profil/unit_penerima/puskesmas"
					$("#list_penerima_in").load(url_hasil);
			}
		})
	})		

	$("#pagepuskesmas").on("click","a",function (event){
		event.preventDefault();
		var url = $(this).attr("href");
		$("#pagepuskesmas").load(url);
	});		
			
	$("#tabel_up tr").on("click",function (event){
		var url="profil/unit_penerima/get_detail_puskesmas/"		
		var testindex = $("#tabel_up tr").index(this);
		var form_data = { kodepusk: $("#hidden_kdpusk_"+testindex).val() };		
		$.ajax({
			type:"POST",
			url:url,
			data: form_data,
			dataType:'json',
			success:function(data){
				$('#alamat_pusk_cha').val(data.alamat_pusk);
				$('#nama_pusk_cha').val(data.nama_pusk);
				$('#kode_puskesmas_cha').val(data.kode_pusk);
				$('#hide_kode_pusk').val(data.kode_pusk);
				$('#fax_cha').val(data.fax);
				$('#telp_cha').val(data.telp);
				$('#email_cha').val(data.email);
				$('#longi').val(data.longi);
				$('#lati').val(data.lati);
				$('#buffer-cha').val(data.buffer*100);
				$('#periode-distribusi-cha').val(data.periode_distribusi);
				$( "#kec option:selected" ).text(data.nama_kec).attr('value', data.kode_kec);
				$( "#desa_cha option:selected" ).text(data.nama_desa).attr('value', data.kode_desa);
				$( "#poned_cha option:selected" ).text(data.poned).attr('value', data.poned);
				$( "#dtpk_cha option:selected" ).text(data.dtpk).attr('value',data.dtpk);
				$( "#jenis_puskesmas_cha option:selected" ).text(data.jenis_pusk).attr('value', data.jenis_pusk);
				$("#myModal").modal('show');
			}
		});
	});
		
});

function getVillageChange(val) {
    if(val == 'add'){
    	alert("tambah kecamatan");    	
    }else{
    	$.get('profil/unit_penerima/get_data_desa/' + val, function(data) {$('#desa_cha').html(data);});    	
    }
}
</script>
<div id="pagepuskesmas">
<?php echo $links; ?>
	<table class="table table-hover table-striped table-bordered" id="tabel_up">
		<thead>
		<tr class="active">
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Kode</th>
			<th>Unit Penerima</th>
			<th>Alamat</th>
		</tr>
		</thead>
		<tbody>
			<?php $i=1; foreach ($result as $rows): ?>
			<input type="hidden" value="<?= $rows->KD_PUSK ?>" id="hidden_kdpusk_<?= $i ?>" name="hidden_kdpusk" class="ckode_pusk">
			<tr style="cursor:pointer;">
				<td><input type="checkbox" name="chk[]" id="cek_del_penerimaan_obat" value="<?= $rows->KD_PUSK ?>" class="chk"></td>
				<td><?= $i ?></td>
				<td><?= $rows->KD_PUSK ?></td>
				<td><?= $rows->NAMA_PUSKES ?></td>
				<td><?= $rows->ALAMAT_PUSKESMAS ?></td>
			</tr>
			<?php $i++;endforeach; ?>
		</tbody>
	</table>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data Puskesmas</h4>
      </div>
      <div class="modal-body">
        <table class="table">
			<tr>
				<td>Kode</td>
				<td>: <input type="text" id="kode_puskesmas_cha" name="kode_puskesmas_cha">
					<input type="hidden" id="hide_kode_pusk" name="hide_kode_pusk" ></td>
				<td>Jenis</td>
				<td>: <select name="jenis_puskesmas_cha" id="jenis_puskesmas_cha">
							<option value="">(Belum diisi)</option>
							<option value="Perawatan">Perawatan</option>
							<option value="Non Perawatan">Non Perawatan</option>
					</select></td>
			</tr>
			<tr>
				<td>Unit Penerima</td>
				<td>: <input type="text" name="nama_pusk_cha" id="nama_pusk_cha"></td>
				<td>Poned</td>
				<td>: <select name="poned_cha" id="poned_cha">
							<option value="">(Belum diisi)</option>
							<option value="Ya">Ya</option>
							<option value="Tidak">Tidak</option>
					</select></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>: <input type="text" name="alamat_pusk_cha" id="alamat_pusk_cha" size="50"></td>
				<td>DTPK</td>
				<td>: <select name="dtpk_cha" id="dtpk_cha">
							<option value="">(Belum diisi)</option>
							<option value="Ya">Ya</option>
							<option value="Tidak">Tidak</option>
					</select></td>
			</tr>
			<tr>
				<td>Kecamatan</td>
				<td>: <select name="kec" id="kec" onchange="getVillageChange(this.value);">>
						<option value="">(Belum diisi)</option>
						<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
						<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
						<?php endfor; ?>
						<option value="add">--- TAMBAH ---</option>
					</select>
				</td>
				<td>Telepon</td>
				<td>: <input type="text" name="telp_cha" id="telp_cha"></td>
			</tr>
			<tr>
				<td>Longitude</td>
				<td>: <input type="text" name="longi" id="longi"></td>
				<td>Email</td>
				<td>: <input type="text" name="email_cha" id="email_cha"></td>
			</tr>
			<tr>
				<td>Latitude</td>
				<td>: <input type="text" name="lati" id="lati"></td>
				<td>Fax</td>
				<td>: <input type="text" name="fax_cha" id="fax_cha"></td>
			</tr>
		</table>
		<hr>
		<table class="table table-striped" style="width:50%;">
			<tr class="active">
				<td style="width:10%;">Buffer</td>
				<td>: <input type="text" name="" id="buffer-cha" size="3"> %</td>
				<td style="width:30%;">Periode Distribusi</td>
				<td>: <input type="text" name="" id="periode-distribusi-cha" size="3"> bulan</td>
			</tr>
		</table>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="btn_update">Save changes</button>
      </div>
  	
    </div>
  </div>
</div>
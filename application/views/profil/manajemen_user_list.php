<script type="text/javascript">
function cekpwd(val){
	var pswd = $('#ch-password').val();
	if (val == pswd){
		$('#ok-user').show('slow');
		$('#wrong-user').hide();
	}else{
		$('#wrong-user').show('slow');
		$('#ok-user').hide();
	}	
}


$(function(){
	$(".table-content").DataTable();

	$('#btn-update-user').on("click",function(e){
    	var form_data = { username: $('#cha-username').val(), password: $('#ch-password').val() };
    	$.ajax({
    		type: "POST",
    		url: $("#form-update-user").attr("action"),
    		data: form_data,
    		dataType:'json',
    		success:function(res) {	    			
    			$(".alert").addClass('alert-'+res.status);
    			$("#message").text(res.message);
    			$(".alert").fadeIn('slow');
    			if(res.status == 'success') {
    				setTimeout(function(){ 
    					$(".close").click();
    					reloadContent();
    					resetInput();
    					$( '.modal' ).remove();
						$( '.modal-backdrop' ).remove();
						$( 'body' ).removeClass( "modal-open" );
    				}, 3000);
    			}
    		}
    	});
	})

	$(".table-content").on("click", ".btn-password", function() {		
		var url = $(this).attr("href");
		var username = $(this).parent().parent().find('.username').text();
		$("#form-update-user").attr("action", url);
		$("#cha-username").val(username);
		$('#modal-change-pswd').modal('show');
	})

	$('.table-content').on("click", ".btn-update", function(e) {
    	var url = $(this).attr("href");
    	$.ajax({
    		type:"POST",
    		url:url,
    		dataType:'json',
    		success:function(data) {
    			$('#cha_id_user').val(data.id_user);
    			$('#cha_long_name').val(data.long_name);
    			$('#cha_email').val(data.email);
    			$('#cha_cp').val(data.cp);
    			$('#cha_jabatan').val(data.jabatan);
    			$('#cha_group').val(data.group_id);
    			$('#change_user').modal('show');
    		}
    	});	    	
	})

	$("#btn-save-update").click(function(){
		$.ajax({
			type:"POST",
			url:"<?= base_url(); ?>profil/manajemen_user/update_data",
			data: $("#form-update").serialize(),
			dataType: "json",
			success:function(response){				
				alert(response.message);
				if(response.status == "success") {
					$( '.modal' ).remove();
					$( '.modal-backdrop' ).remove();
					$( 'body' ).removeClass( "modal-open" );

					reloadContent();
					resetInput();
					$("#cha_group").val(2);
				}				
			}
		});
	})
});
</script>
<table class="table table-striped table-bordered table-content">
	<thead>
	<tr class="active">
		<th>Select:</th>
		<th>Username</th>
		<th>Nama lengkap</th>
		<th>Jabatan</th>
		<th>E-Mail</th>
		<th>Cp</th>
		<th>Action</th>
	</tr>
	</thead>
	<tbody>
		<?php foreach ($result as $rows): ?>
			<tr style="cursor:pointer;">
				<td><input type="checkbox" name="chk[]" id="" value="<?= $rows->id_user ?>" class="chk"></td>
				<td><label class="username"><?= $rows->username ?></label></td>
				<td><?= $rows->long_name ?></td>
				<td><?= $rows->jabatan ?></td>
				<td><?= $rows->email ?></td>
				<td><?= $rows->cp ?></td>
				<td>
					<button class="btn-password btn-info btn-sm" href="<?= base_url(); ?>profil/manajemen_user/changePwd/<?= $rows->id_user ?>">ubah password</button>
					<button class="btn-update btn-warning btn-sm" href="<?= base_url(); ?>profil/manajemen_user/datatochange/<?= $rows->id_user ?>">detail</button>					
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<!-- MODAL UBAH DATA -->
<div class="modal fade" id="modal-change-pswd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
		    <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		        <h4 class="modal-title" id="myModalLabel">Ubah Data</h4>
		    </div>
	      	<div class="modal-body">
	      		<div class="alert" style="display:none;"><p id="message"></p></div>
		      	<form id="form-update-user" action="">
					<table class="table">
					  <tr>
					    <td>Username</td>
					    <td>
					      <input type="text" class="form-control input-text" id="cha-username" name="username">
					    </td>
					  </tr>
					  <tr>
					    <td>Password Baru</td>
					    <td>
					      <input type="password" class="form-control input-text" id="ch-password" placeholder="Password" name="">
					    </td>
					  </tr>
					  <tr>
					    <td>Konfirmasi Password Baru</td>
					    <td>
					      <input type="password" class="form-control input-text" id="ch-password-cf" placeholder="Password" name="" onchange="cekpwd(this.value);">
					      <label style="display:none" id="ok-user">password match</label>
					      <label style="display:none" id="wrong-user">password did not match</label>
					    </td>
					  </tr>
					  <tr>
					    <td colspan="2">
					      <button type="button" class="btn btn-default" id="btn-update-user">Simpan</button>
					    </td>
					  </tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- MODAL UBAH DATA -->
<div class="modal fade" id="change_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  	<div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Form Update</h4>
	      </div>
	      	<div class="modal-body">
	      		<form id="form-update">
					<table class="table">
						  <tr>
						    <td>Nama Lengkap</td>
						    <td>
						      <input type="text" class="form-control input-text" id="cha_long_name" name="long_name">
						      <input type="hidden" id="cha_id_user" name="id_user">
						    </td>
						  </tr>
						  <tr>
						    <td>Kategori User</td>
						    <td>
						      	<select class="form-control input-text" name="group" id="cha_group">
						      		<option value="1">Administrator</option>
						      		<option value="2">Petugas Harian Dinas</option>
						      		<option value="3">Puskesmas</option>
						      	</select>
						    </td>
						  </tr>
						  <tr>
						  	<td>
						  		Puskesmas
						  	</td>
						  	<td>
						  		<select class="form-control input-text" name="puskesmas" id="cha_puskesmas">
						  			<option value="">Silakan Pilih</option>
						  			<?php foreach ($puskesmas->result() as $key): ?>
						  			<option value="<?= $key->KD_PUSK ?>"><?= $key->NAMA_PUSKES ?></option>
						  			<?php endforeach; ?>						  			
						  		</select>
						  	</td>
						  </tr>
						  <tr>
						    <td>Kontak Person</td>
						    <td>
						      <input type="text" class="form-control input-text" id="cha_cp" name="cp">
						    </td>
						  </tr>
						  <tr>
						    <td>Email</td>
						    <td>
						      <input type="text" class="form-control input-text" id="cha_email" name="email">
						    </td>
						  </tr>
						  <tr>
						    <td>Jabatan</td>						    
						      <td>
						      <input type="text" class="form-control input-text" id="cha_jabatan" placeholder="jabatan" name="jabatan">
						    </td>					    
						  </tr>		  
						  <tr>
						    <td colspan="2">
						      <button type="button" class="btn btn-default" id="btn-save-update">Simpan</button>
						    </td>
						  </tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
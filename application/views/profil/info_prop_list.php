<style type="text/css">
th{
	text-align: center;
}
th,td{
	padding: 5px;	
}
.dataTables_filter{
	float:right;
}
.input-sm {
	height: 30px;
	padding: 5px 10px;
	font-size: 12px;
	line-height: 1.5;
	border-radius: 3px;
}
	.pagination{
		float:right;
	}
</style>
<script type="text/javascript">
	$(function(){
		var tbl;
		tbl=$('.table-kabupaten').dataTable({
			"sPaginationType": "full_numbers",
			"bLengthChange": false,
			"bPaginate": true,
		});

		// $("#pagelist").on("click","a",function (event){
		// 	event.preventDefault();
		// 	var url = $(this).attr("href");
		// 	//$("#pagin").load(url);
		// 	$('#list_gol').load(url);
		// });

		$("tr").on("dblclick",function (event){
				
				var testindex = $("#pagelist tr").index(this);
				var form_data = { kode_kab: $("#kode_kab_"+testindex).val() };
				//var nilai = $("#kode_generik_"+testindex).val();
				//alert(nilai);
				//$("#upt_modal").modal('show');

				var url="profil/unit_penerima_pusat/get_info/"		
				$.ajax({
					type:"POST",
					url:url,
					data: form_data,
					dataType:'json',
					success:function(data){
						//$('#').text(data.kode_obat);
						$('#upt_nama_kab').val(data.CKabDescr);
						$('#upt_lat').val(data.ex);
						$('#upt_long').val(data.ye);
						$('#upt_kode_kab').val(data.CKabID);
						$('#upt_kode_kab_new').val(data.CKabID);
						
						$("#upt_modal").modal('show');

					}
				});
				//$("#pagelist tbody tr.active").removeClass('active');
				// $(this).addClass('active');
				//end ajax
			});
			//end pagepuskesmas tr
		$('#form_update').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#upt_modal").modal('toggle');
	                $('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
	                var url_hasil="profil/unit_penerima_pusat/get_data_kab/"+$('#id_prop').val();
					$("#list_data").load(url_hasil);
	            }
			})
			
		});
	});
</script>
<div id="pagelist">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered table-kabupaten">
		<thead>
		<tr class="active">
			<th width="5%">Select:</th>
			<th>Unit Penerima</th>
			<th>Latitude</th>
			<th>Longitude</th>			
		</tr>
		</thead>
		<tbody>
			<?php
				$i=1;
				foreach ($result2 as $rows) {
					echo '<tr style="cursor:pointer;" class="dttbl">
					<td><input type="hidden" name="kode_kab" id="kode_kab_'.$i.'" value="'.$rows->CKabID.'">
						<input type="checkbox" name="chk[]" id="cek_del_gol" value="'.$rows->CKabID.'" class="chk"></td>
					<td>'.$rows->CKabDescr.'</td>
					<td>'.$rows->ex.'</td>
					<td>'.$rows->ye.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<!-- modal -->
<div class="modal fade" id="upt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Ubah Master Kabupaten</h4>
	      </div>
	      <div class="modal-body">

		<form action="<?php echo $base_url; ?>index.php/profil/unit_penerima_pusat/update_datakab" method="post" id="form_update">
		<table class="table">
			<tr>
				<td>Nama Unit Penerima</td>
				<td>
					<!--input type="text" name="uptdata[CKabDescr]" id="upt_nama_kab" size="30" class="form-control"/-->
					<input type="text" id="upt_kode_kab_new" name="uptdata[CKabID]" class="form-control">
					<input type="hidden" id="upt_kode_kab" name="kode_new">
				</td>
			</tr>
			<tr>
				<td>Nama Unit Penerima</td>
				<td>
					<input type="text" name="uptdata[CKabDescr]" id="upt_nama_kab" size="30" class="form-control"/>
					<!--input type="hidden" id="upt_kode_kab" name="uptdata[CKabID]"-->
				</td>
			</tr>
			<tr>
				<td>Latitude</td>
				<td>
					<input type="text" name="uptdata[ex]" id="upt_lat" size="30" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td>Longitude</td>
				<td>
					<input type="text" name="uptdata[ye]" id="upt_long" size="30" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_gol_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
					<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
					<input type="reset" name="Reset" id="reset" value="Batal" /-->
				</td>
			</tr>
		</table>
		</form>
			</div>
			</div>
		</div>
	</div><!-- end modal -->
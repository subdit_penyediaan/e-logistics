<style type="text/css">
	.dataTables_filter{
		float:right;
	}
	.pagination{
		float:right;
	}
</style>
<script type="text/javascript">
	$(function(){
		$("#tabel-kecamatan").dataTable();
		$("#pagelistkecamatan").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_kecamatan').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#unit_kecamatan').load(url);
		});
	});
</script>
<div id="pagelistkecamatan">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-bordered" id="tabel-kecamatan">
		<thead>
		<tr class="active">
			<th width="5%">Select:</th>
			<th>Kode Kecamatan</th>
			<th>Nama Kecamatan</th>
			<!--th>Aksi</th-->
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				//$i=1;
				foreach ($result as $rows) {
					echo '<tr style=""><td><input type="checkbox" name="chk[]" id="cek_del_kecamatan" value="'.$rows->KDKEC.'" class="chk">
					<td>'.$rows->KDKEC.'</td>
					<td>'.$rows->KECDESC.'</td></tr>';
					// <td><button href="'.$base_url.'index.php/profil/unit_penerima/add_desa/'.$rows->KDKEC.'" id="" class="btn btn-success btn-sm">
					// 	<span class="glyphicon glyphicon-list"></span> Tambah Desa</button></td></tr>';
					//$i++;
				}
			?>
		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function(){

	$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});

	$('#btn_update').click(function(){
		//alert("cek");
		var url_update='profil/unit_penerima/update_data';
		var form_data = {
			kec:$('#kec').val(),
			telp:$('#telp_cha').val(),
			desa_cha:$('#desa_cha').val(),
			fax:$('#fax_cha').val(),
			hide_kode_pusk:$('#hide_kode_pusk').val(),
			jenis_pusk:$('#jenis_puskesmas_cha').val(),
			poned:$('#poned_cha').val(),
			alamat_pusk:$('#alamat_pusk_cha').val(),
			dtpk:$('#dtpk_cha').val(),
			email:$('#email_cha').val(),
			longi:$('#longi').val(),
			lati:$('#lati').val(),
			nama_pusk:$('#nama_pusk_cha').val()
		}
		$.ajax({
			type:"POST",
			url:url_update,
			data: form_data,
			success: function(e){
				alert("Data berhasil diupdate");
				$('#myModal').modal('toggle');
			}
		})
	})

		$("#page_propinsi").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$("#page_propinsi").load(url);
		});
		//$("#dialogEdit").dialog({autoOpen: false});
		
		//})
		
	});

function getVillageChange(val) {
    if(val == 'add'){
    	alert("tambah kecamatan");
    	//var url_kec = 'profil/unit_penerima/add_kecamatan'
    	//$('#konten').load(url_kec);
    }else{
    	//alert("cek");
    	$.get('profil/unit_penerima/get_data_desa/' + val, function(data) {$('#desa_cha').html(data);});    	
    }
}
</script>
<div id="page_propinsi">


<div class="panel panel-primary">
	<div class="panel-heading">Unit Penerima</div>
  	<div id="up-konten"class="panel-body" style="padding:15px;">

		<div id="partbutton">
					<div class="col-lg-8">
						<button id="add_penerima_in_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
						<button id="del_penerima_in_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
						<!--button id="cha_penerima_in-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
					</div>
					<div class="col-lg-4">
						<div class="input-group" style="float:right;">
					      <input type="text" class="form-control">
					      <span class="input-group-btn">
					        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
					      </span>
					    </div><!-- /input-group -->
					</div><!-- /col6 -->
				</div>
		<?php echo $links; ?>
		<!--/div-->
			<!--div id="pagin"-->
			<table class="table table-striped">
				<thead>
				<tr>
					<th><span class="glyphicon glyphicon-trash"></span></th>
					<th>No.</th>
					<th>Nama Propinsi</th>
					<th>Detail Kabupaten</th>
				</tr>
				</thead>
				<tbody id="trcontent">
					<?php
						$i=1;
						foreach ($result as $rows) {
							echo '<tr style="cursor:pointer;">
								<td><input type="checkbox" name="chk[]" id="cek_del" value="'.$rows->CPropID.'" class="chk"></td>
								<td>'.$i.'</td><td>'.$rows->CPropDescr.'</td>
								<td><button href="'.$base_url.'index.php/profil/unit_penerima_pusat/get_detail/'.$rows->CPropID.'" id="detail_faktur"><span class="glyphicon glyphicon-list"></span> Lihat</button></td></tr>';
							$i++;
						}
					?>
				</tbody>
			</table>
		</div> <!-- end div page_propinsi-->
	</div> <!-- end div panel conten-->
</div>
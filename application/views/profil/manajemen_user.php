<script>
function resetInput() {
	$(".input-text").val("");
}

function reloadContent() {
	var url_hasil="<?= base_url() ?>profil/manajemen_user/get_data_manajemen_user";
	$("#list_manajemen_user").load(url_hasil);
}

$(document).ready(function(){
	//=========== del button
	$("#btn-delete").on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;
        })
        var conf = confirm("Apakah Anda yakin?");
        if (conf) {
        	if(id_array!=0){
		        $.ajax({
		        	url: "<?= base_url(); ?>profil/manajemen_user/delete_list",
		        	data: "kode="+id_array,
		        	type: "POST",
		        	dataType: "json",
		        	success: function(response){
		        		alert(response.message);
		        		if (response.status == 'success') {
		        			reloadContent();
		        		}
		        	}
		        })
		    } else {
		    	alert("pilih data dulu")
		   	}
        }
	})
	//=========== end del

	//=========== show add form

	$("#add_manajemen_user_btn").on("click",function (event){
		$("#add_manajemen_user_modal").modal('show');
	});
	//================ end show add form

	reloadContent();
	//============== submit add form

	$("#btn-add-user").click(function(){
		var url="<?= base_url(); ?>profil/manajemen_user/input_data";
		$.ajax({
			type:"POST",
			url:url,
			data: $("#form-input").serialize(),
			dataType: "json",
			success:function(response){
				alert(response.message);
				if (response.status == "success") {
					$("#add_manajemen_user_modal").modal('toggle');
					reloadContent();
					resetInput();
					$('#group').val(2);
					$('#oke').hide();
					$('#wrong').hide();
				}
			}
		});
	})

	//============== end submit add form

});

function cekpwd(val){
	var pswd = $('#inputPassword3').val();
	if (val == pswd){
		$('#ok').show('slow');
		$('#wrong').hide();
	}else{
		$('#wrong').show('slow');
		$('#ok').hide();
	}

}
</script>
<div class="panel panel-primary" id="halaman_manajemen_user">
	<div class="panel-heading">Manajemen User</div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_manajemen_user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="myModalLabel">Form Tambah</h4>
				    </div>
			      	<div class="modal-body">
				      	<form class="form-horizontal" role="form" id="form-input">
				      		<table class="table">
							  <tr>
							    <td>Username</td>
							    <td>
							      <input type="text" class="form-control input-text" id="username" name="username">
							    </td>
							  </tr>
							  <tr>
							    <td>Kategori User</td>
							    <td>
							      	<select class="form-control input-text" name="group" id="group">
							      		<option value="1">Administrator</option>
							      		<option value="2" selected="selected">Petugas Harian Dinas</option>
							      		<option value="3">Puskesmas</option>
												<option value="4">Auditor</option>
							      	</select>
							    </td>
							  </tr>
							  <tr>
							  	<td>
							  		Puskesmas
							  	</td>
							  	<td>
							  		<select class="form-control input-text" name="puskesmas" id="puskesmas">
							  			<option value="">Silakan Pilih</option>
							  			<?php foreach ($puskesmas->result() as $key): ?>
							  			<option value="<?= $key->KD_PUSK ?>"><?= $key->NAMA_PUSKES ?></option>
							  			<?php endforeach; ?>
							  		</select>
							  	</td>
							  </tr>
							  <tr>
							    <td>Nama Lengkap</td>
							    <td>
							      <input type="text" class="form-control input-text" id="long_name" name="long_name">
							    </td>
							  </tr>
							  <tr>
							    <td>Kontak Person</td>
							    <td>
							      <input type="text" class="form-control input-text" id="cp" name="cp">
							    </td>
							  </tr>
							  <tr>
							    <td>Email</td>
							    <td>
							      <input type="text" class="form-control input-text" id="email" name="email">
							    </td>
							  </tr>
							  <tr>
							    <td>Password</td>
							    <td>
							      <input type="password" class="form-control input-text" id="inputPassword3" placeholder="Password" name="pswd">
							    </td>
							  </tr>
							  <tr>
							    <td>Konfirmasi Password</td>
							    <td>
							      <input type="password" class="form-control input-text" id="confirmPassword" placeholder="Password" name="confirmPassword" onchange="cekpwd(this.value);">
							      <label style="display:none" id="ok">password match</label>
							      <label style="display:none" id="wrong">password did not match</label>
							    </td>
							  </tr>
							  <tr>
							    <td>Jabatan</td>
							      <td>
							      <input type="text" class="form-control input-text" id="jabatan" placeholder="jabatan" name="jabatan">
							    </td>

							  </tr>
							  <tr>
							    <td colspan="2">
							      <button type="button" class="btn btn-default" id="btn-add-user">Simpan</button>
							    </td>
							  </tr>
							</table>
						</form>
					</div><!-- end div modal body -->
					</div><!-- end div modal content -->
				</div><!-- end div modal-dialog -->
			</div><!-- end div modal-->
			<!--/form-->
			<button id="add_manajemen_user_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah User</button>
			<button id="btn-delete" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button>
			<br><br>
			<div id="list_manajemen_user"></div>
	</div>
</div>

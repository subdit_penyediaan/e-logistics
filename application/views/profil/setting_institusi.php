<style type="text/css">
tr, td{
	padding: 5px;
}

a:hover{
	color:red;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$('#btn_cancel').click(function(){
			var url_back='profil/setting_institusi';
			$('#konten').load(url_back);
		})

	$('.set_institusi').hide();

	$('#btn_ubah_set_institusi').click(function(){
		$('.set_institusi').show('slow');
		$('.show_data_institusi').hide();
		if($('#hidden_level').val()==3){
			$('#trkab').slideDown('slow');
			$('#trprop').slideDown('slow');
			$('#level').val("3");
			var id_kab=$('#hidden_kab').val();
			$('#levelkab').val(id_kab);
		}else
		if($('#hidden_level').val()==2){
			$('#trprop').slideDown('slow');
			$('#trkab').hide();
			$('#level').val("2");
			var id_prop=$('#hidden_prop').val();
			$('#levelprop').val(id_prop);
		}else{
			$('#level').val("1");
		}
	})

	$('#level').on("change",function(e){
		if($('#level').val()==3){
			$('#trkab').slideDown('slow');
			//$('#trprop').hide();
			$('#trprop').slideDown('slow');
		}else
		if($('#level').val()==2){
			$('#trprop').slideDown('slow');
			$('#trkab').hide();
		}else if($('#level').val()==1){
			$('#trprop').hide();
			$('#trkab').hide();
		}
	});

	$('#levelkab').on("change",function(data){
		
		var url="profil/setting_institusi/get_location/"		

		var form_data = { id_kab: $('#levelkab').val() };
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url,
			data: form_data,
			dataType:'json',
			success:function(data){
				$('#longi').val(data.ye);
				$('#lat').val(data.ex);
			}
		});
	})

	//============== submit add form

	$("#btn_simpan").click(function(){
		var url2="profil/setting_institusi/input_data";
		var form_data = {
			level:$('#level').val(),
			levelkab:$('#levelkab').val(),
			levelprop:$('#levelprop').val(),
			nama_fasilitas:$('#nama_fasilitas').val(),
			alamat:$('#alamat').val(),
			longi:$('#longi').val(),
			lat:$('#lat').val(),
			notelp:$('#notelp').val(),
			pengelola:$('#pengelola').val(),
			struktur_org:$('#struktur_org').val(),
			bag_pj:$('#bag_pj').val(),
			pj:$('#pj').val(),
			jab_pj:$('#jab_pj').val(),
			cp_pj:$('#cp_pj').val(),
			webservice1:$('#webservice1').val(),
			username:$('#username').val(),
			password:$('#password').val(),
            webservice2:$('#webservice2').val(),
            username_rko:$('#username-rko').val(),
			password_rko:$('#password-rko').val(),
            webservice_rko:$('#webservice-rko').val()
		}

		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){				
				// if($('#level').val()==3){					
				// 	var grub=confirm("Apakah Anda mau menggunakan modul LPLPO Group Generik?");
				// 	if(grub){
				// 		$.post('profil/setting_institusi/getGroupLplpo');
				// 	}else{
				// 		$.post('profil/setting_institusi/resetGroup');
				// 	}
				// }
				alert("Data berhasil di update");				
				$("#konten").load('profil/setting_institusi');
			}
		});
	})

	//============== end submit add form
})
function get_kabupaten(val) {
    
    if(val == '0'){
    	alert("pilih dulu nama kabupatennya");
    }else{
    	//alert("cek");
    	$.get('profil/setting_institusi/get_data_kabupaten/' + val, function(data) {$('#levelkab').html(data);});    	
    }
}
</script>
<div class="panel panel-primary">
	<div class="panel-heading"><span class="glyphicon glyphicon-wrench"></span> <b>Setting Institusi</b></div>
	<div id="up-konten" class="panel-body" style="padding:15px;">
		<div class="row">
			<div class="col-md-6">
				<table class="table table-striped table-condensed" width="50%">
					<tr>
						<td width="20%">Level Fasilitas<b style="float:right">: </b></td><td width="80%">
							<select name="level" id="level" class="form-control set_institusi">
								<option value="1">Kementerian</option>
								<option value="2">Propinsi</option>
								<option value="3">Kabupaten/Kota</option>I
							</select>
							<label class="show_data_institusi">
								<?php 
									$level=$prof_ins['levelinstitusi'];
									if ($level==3){
										echo "Kabupaten";
										echo '<input type="hidden" value="'.$prof_ins['id_kab'].'" name="hidden_kab" id="hidden_kab">';
									}elseif ($level==2){
										echo "Propinsi";
										echo '<input type="hidden" value="'.$prof_ins['id_prop'].'" name="hidden_prop" id="hidden_prop">';
									}else{
										echo "Kementerian";
									}
								?> 
							</label>
							<input type="hidden" value="<?php echo $prof_ins['levelinstitusi']; ?>" name="hidden_level" id="hidden_level">
						</td>
					</tr>
					<?php if(!empty($prof_ins['nama_kab'])): ?>
						<tr class="show_data_institusi">
							<td>Kabupaten <b style="float:right">: </b></td><td> <label><?php echo $prof_ins['nama_kab']; ?></label></td>
						</tr>
					<?php endif; ?>
					<?php if(!empty($prof_ins['nama_prop'])): ?>
						<tr class="show_data_institusi">
							<td>Propinsi <b style="float:right">: </b></td><td> <label><?php echo $prof_ins['nama_prop']; ?></label></td>
						</tr>				
					<?php endif; ?>
					<tr style="display:none;" id="trprop">
						<td>Propinsi <b style="float:right">: </b></td>
						<td>
							<select name="levelprop" id="levelprop" class="form-control set_institusi" onchange="get_kabupaten(this.value);">
							<?php 
								$kode_provinsi = '';
								if($prof_ins['nama_fasilitas']!=''){
									$kode_provinsi = substr($prof_ins['id_kab'],0, 2);
								}													
							 ?>
							<?php for($i=0;$i<sizeof($propinsi);$i++) : ?>
								<option value="<?php echo $propinsi[$i]['CPropID'] ?>" <?= ($kode_provinsi==$propinsi[$i]['CPropID'])?'selected="selected"':''?>><?php echo $propinsi[$i]['CPropDescr'] ?></option>
							<?php endfor; ?>
							</select><br>
							<a href="#" style="text-decoration:none;">
								<i>&ensp;<small>nama propinsi anda belum terdaftar?</small></i>
							</a>
						</td>
					</tr>
					<tr style="display:none;" id="trkab">
						<td>Kabupaten <b style="float:right">: </b></td>
						<td>
							<select name="levelkab" id="levelkab" class="form-control set_institusi" >
							<option value="<?php echo $prof_ins['id_kab']; ?>"><?php echo $prof_ins['nama_kab']; ?></option>
							<?php for($i=0;$i<sizeof($kabupaten);$i++) : ?>
								<option value="<?php echo $kabupaten[$i]['CKabID'] ?>" <?= ($prof_ins['id_kab']==$kabupaten[$i]['CKabID'])?'selected="selected"':''?>><?php echo $kabupaten[$i]['CKabDescr'] ?></option>
							<?php endfor; ?>
							</select><br>
							<a href="#" style="text-decoration:none;">
								<i>&ensp;<small>nama kabupaten anda belum terdaftar?</small></i>
							</a>
						</td>
					</tr>
					<tr>
						<td>Nama Fasilitas <b style="float:right">: </b></td>
						<td>
							<input type="text" name="nama_fasilitas" id="nama_fasilitas" class="form-control set_institusi" value="<?php echo $prof_ins['nama_fasilitas']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['nama_fasilitas']; ?> </label>
						</td>
					</tr>
					<tr>	
						<td>Alamat <b style="float:right">: </b></td>
						<td>
							<textarea name="alamat" id="alamat" class="form-control set_institusi"><?php echo $prof_ins['alamat']; ?></textarea>
							<label class="show_data_institusi"><?php echo $prof_ins['alamat']; ?> </label>
						</td>
					</td>
					<tr>
						<td>Longitude <b style="float:right">: </b></td>
						<td>
							<input type="text" name="longi" id="longi" class="form-control set_institusi" value="<?php echo $prof_ins['longitude']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['longitude']; ?> </label>
						</td>
					</td>
					<tr>
						<td>Latitude <b style="float:right">: </b></td>
						<td>
							<input type="text" name="lat" id="lat" class="form-control set_institusi" value="<?php echo $prof_ins['latitude']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['latitude']; ?> </label>
						</td>
					</td>
					<tr>
						<td>No. Telp <b style="float:right">: </b></td>
						<td>
							<input type="text" name="notelp" id="notelp" class="form-control set_institusi" value="<?php echo $prof_ins['notelp']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['notelp']; ?> </label>
						</td>
					</td>
					<!--tr>
						<td>Pengelola <b style="float:right">: </b></td><td> <select name="pengelola" id="pengelola" class="form-control set_institusi">
														<option value="null"> Pilih </option>
														<option value="IFK">IFK</option>
														<option value="GFK">GFK</option>													
													</select>
													<label class="show_data_institusi"><?php echo $prof_ins['pengelola']; ?> </label></td>
					</tr-->
					<tr>
						<td>Struktur Organisasi <b style="float:right">: </b></td>
						<td>
							<select name="struktur_org" id="struktur_org" class="form-control set_institusi">
								<option value="<?php echo $prof_ins['struktur_org']; ?>"><?php echo $prof_ins['struktur_org']; ?></option>
								<option value="null">----- Pilih -----</option>
								<option value="Sie Farmasi">Sie Farmasi</option>
								<option value="UPTD">UPTD</option>
								<option value="Lain-lain">Lain-lain</option>I
							</select>
							<label class="show_data_institusi"><?php echo $prof_ins['struktur_org']; ?> </label>
						</td>
					</tr>
					<tr>
						<td>Penanggung Jawab <b style="float:right">: </b></td>
						<td>
							<select name="bag_pj" id="bag_pj" class="form-control set_institusi">
								<option value="<?php echo $prof_ins['pj']; ?>"><?php echo $prof_ins['pj']; ?></option>
								<option value="null">----- Pilih -----</option>
								<option value="Apoteker">Apoteker</option>								
								<option value="D3-Farmasi">D3-Farmasi</option>
								<option value="S1-Farmasi">S1-Farmasi</option>
								<option value="Asisten Apoteker/SMF">Asisten Apoteker/SMF</option>
								<option value="Tenaga Kesahatan Lain">Tenaga Kesahatan Lain</option>
								<option value="Lain-lain">Lain-lain</option>
							</select>
							<label class="show_data_institusi"><?php echo $prof_ins['pj']; ?> </label>
						</td>
					</tr>
					<tr>
						<td>Nama Penangung Jawab <b style="float:right">: </b></td>
						<td>
							<input type="text" name="pj" id="pj" class="form-control set_institusi" value="<?php echo $prof_ins['nama_pj']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['nama_pj']; ?> </label>
						</td>
					</tr>
					<tr>
						<td>Jabatan Penangung Jawab <b style="float:right">: </b></td>
						<td>
							<input type="text" name="jab_pj" id="jab_pj" class="form-control set_institusi" value="<?php echo $prof_ins['jabatan_pj']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['jabatan_pj']; ?> </label>
						</td>
					</tr>
					<tr>
						<td>Kontak Penangung Jawab <b style="float:right">: </b></td>
						<td>
							<input type="text" name="cp_pj" id="cp_pj" class="form-control set_institusi" value="<?php echo $prof_ins['cp_pj']; ?>">
							<label class="show_data_institusi"><?php echo $prof_ins['cp_pj']; ?> </label>
						</td>
					</tr>
				</table>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-12">
						<div class="panel panel-info">
							<div class="panel-heading"><span class="glyphicon glyphicon-globe"></span> <b>Account Bankdata Elog</b></div>
							<div id="up-konten" class="panel-body" style="padding:15px;">
								<table class="table">
									<tr>
					                    <td>Web Service <b style="float:right">: </b></td>
					                    <td>
					                    	<input type="text" class="form-control set_institusi" name="webservice1" id="webservice1" size="40" value="<?= ($prof_ins['url_bank_data1']=='')?'bankdataelog.kemkes.go.id/apps':$prof_ins['url_bank_data1']; ?>">
					                        <span class="help-block set_institusi" style="font-size: smaller">Ketik tanpa http://</span>                        
					                        <label class="show_data_institusi"><?= ($prof_ins['url_bank_data1']=='')?'bankdataelog.kemkes.go.id/apps':$prof_ins['url_bank_data1']; ?></label>
					                    </td>
					                </tr>
					                <!--tr>
					                    <td>Web Service2 <b style="float:right">: </b></td><td> <div class="form-control set_institusi"><input type="text" name="webservice2" id="webservice2" size="40" value="<?php echo $prof_ins['url_bank_data2']; ?>">
					                            <span class="help-block" style="font-size: smaller">Ketik tanpa http://</span>
					                        </div>
					                        <label class="show_data_institusi"><?php echo $prof_ins['url_bank_data2']; ?> </label></td>
					                </tr-->                
					                <tr>
										<td>Username<b style="float:right">: </b></td>
										<td>
											<input type="text" name="username" id="username" class="form-control set_institusi" value="<?php echo $prof_ins['username']; ?>">
											<label class="show_data_institusi"><?php echo $prof_ins['username']; ?> </label>
										</td>
									</tr>
									<tr>
										<td>Password<b style="float:right">: </b></td>
										<td>
											<input type="password" name="password" id="password" class="form-control set_institusi" value="<?php echo $prof_ins['password']; ?>">
											<label class="show_data_institusi">*************************</label>
										</td>
									</tr>
								</table>
							</div>
						</div>		
					</div>
					<div class="col-md-12">
						<div class="panel panel-info">
							<div class="panel-heading"><span class="glyphicon glyphicon-globe"></span> <b>Account E-Monev</b></div>
							<div id="up-konten" class="panel-body" style="padding:15px;">
								<table class="table">
									<tr>
					                    <td>Web Service <b style="float:right">: </b></td>
					                    <td>
					                    	<input type="text" class="form-control set_institusi" name="webservice1" id="webservice-rko" size="40" value="<?= ($prof_ins['url_rko']=='')?'monevkatalogobat.kemkes.go.id':$prof_ins['url_rko']; ?>">
					                        <span class="help-block set_institusi" style="font-size: smaller">Ketik tanpa http://</span>                        
					                        <label class="show_data_institusi"><?= ($prof_ins['url_rko']=='')?'monevkatalogobat.kemkes.go.id':$prof_ins['url_rko']; ?></label>
					                    </td>
					                </tr>
					                <tr>
										<td>Username<b style="float:right">: </b></td>
										<td>
											<input type="text" name="username" id="username-rko" class="form-control set_institusi" value="<?php echo $prof_ins['username_rko']; ?>">
											<label class="show_data_institusi"><?php echo $prof_ins['username_rko']; ?> </label>
										</td>
									</tr>
									<tr>
										<td>Password<b style="float:right">: </b></td>
										<td>
											<input type="password" name="password" id="password-rko" class="form-control set_institusi" value="<?php echo $prof_ins['password_rko']; ?>">
											<label class="show_data_institusi">*************************</label>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-12">
				<center>
					<button class="show_data_institusi btn btn-warning btn-sm" id="btn_ubah_set_institusi">Ubah Data</button>
					&ensp;<button id="btn_simpan" class="set_institusi btn btn-success btn-sm">Simpan dan Lanjutkan</button>
					&ensp;<button id="btn_cancel" class="set_institusi btn btn-danger btn-sm">Batal</button>
				</center>				
			</div>
		</div>						
	</div>
</div>
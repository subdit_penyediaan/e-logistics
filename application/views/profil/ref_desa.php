 <style>
	tr{
		padding: 8px;

	}
	td{
		padding: 8px;
	}
	#info_faktur{
		background-color: #99CCFF;
	}
</style>
<script>
$(document).ready(function(){
		$('#back_to_kecamatan').click(function(){
			var url_penerimaan='profil/unit_penerima/add_kecamatan';
			$('#unit_kecamatan').load(url_penerimaan);
		})

	
	//=========== del button
	$("#del_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "profil/unit_penerima/delete_list_kelurahan",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="profil/unit_penerima/get_data_kelurahan/"+$("#hide_kode_kec").val();
					$("#list_kelurahan").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$("#add_kelurahan_btn").on("click",function (event){
      $("#add_kelurahan_modal").modal('show');
        //$("#add_kecamatan_modal").modal({keyboard:false});
    });

	$("#batal").on("click",function (event){
			
			$("#form_unit_penerima").slideUp("slow");
			$("#partbutton").fadeIn();
			
		});
	//================ end show add form

	var url="profil/unit_penerima/get_data_kelurahan/"+$("#hide_kode_kec").val();
	//mengambil data dg key no_struk
	$('#list_kelurahan').load(url);	
	//============== submit add form

	$("#btn_unit_kelurahan").click(function(){
		var url2="profil/unit_penerima/input_data_kelurahan";
		var form_data = {
			kode_kec:$("#hide_kode_kec").val(),
			nama_kel:$("#nama_kel").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_kelurahan_modal").modal('toggle');
				//var url_hasil="profil/unit_penerima_obat/get_list_unit_penerima"
				var url_hasil="profil/unit_penerima/get_data_kelurahan/"+$("#hide_kode_kec").val();
				$("#list_kelurahan").load(url_hasil);//+"#list_sedian_obat");

				//$("#hide_nofaktur").val("");
				$("#nama_kel").val("");
				
			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel">
	<div class="panel-heading"><button id="back_to_kecamatan" style="float:right;color:black;"><span class="glyphicon glyphicon-arrow-left"></span> Back</button></div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<!-- Informasi Detail Faktur -->
			<br>
			<div id="">
			<table class="table table-striped">
				<tr>
					<td>Kode Kecamatan</td>
					<td>: <?php echo $kecamatan['KDKEC'];?>
						<input type="hidden" name="hide_kode_kec" id="hide_kode_kec" value="<?php echo $kecamatan['KDKEC'];?>">
					</td>				
				</tr>
				<tr>
					<td>Nama Kecamatan</td>
					<td>: <?php echo $kecamatan['KECDESC']; ?></td>
				</tr>
			</table>
			</div>
			<hr>
			<!-- end informasi detail faktur -->
			<div id="partbutton">
				<div class="col-lg-8">
					<button id="add_kelurahan_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah Kelurahan</button>
					<button id="cha_penerimaan_obat-btn" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
					<button id="del_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
					
				</div>
				<div class="col-lg-4">
					<div class="input-group" style="float:right;">
				      <input type="text" class="form-control">
				      <span class="input-group-btn">
				        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
				      </span>
				    </div><!-- /input-group -->
				</div><!-- /col6 -->
			</div>
			<br>
			<div class="modal fade" id="add_kelurahan_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Tambah Kelurahan</h4>
            </div>
            <div class="modal-body">

      <!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
        
          <table class="table">
            <tr>
              <td>Nama Kelurahan</td>
              <td>
                <input type="text" name="nama_kel" id="nama_kel" size="30" class="form-control"/>
              </td>
            </tr>
            <tr>
              <td></td>
              <td>
                <div class="pagingContainer">
                  <button type="submit" name="Simpan" id="btn_unit_kelurahan" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
                  <!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
                </div>
                <!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
                <input type="reset" name="Reset" id="reset" value="Batal" /-->
              </td>
            </tr>
          </table>
          </div>
          </div>
        </div>
      </div>
			<!--/form-->
			<div id="list_kelurahan"></div>
	</div>
</div>
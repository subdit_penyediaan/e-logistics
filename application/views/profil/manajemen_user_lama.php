<script type="text/javascript">
$(document).ready(function(){

	//=========== del button
		

	$("#del_rute_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/rute_obat/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/rute_obat/get_data_rute"
					$("#list_rute").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_rute_btn").on("click",function (event){
			$("#add_rute_modal").modal('show');
				//$("#add_rute_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="masterdata/rute_obat/get_list_rute";
	//$('#list_rute_obat').load(url);

	var url="masterdata/rute_obat/get_data_rute";
	$('#list_rute').load(url);	
	//============== submit add form

	$("#btn_rute_obat").click(function(){
		var url2="masterdata/rute_obat/input_data";
		var form_data = {
			nama_rute_obat:$("#nama_rute_obat").val(),
			
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			//data:{"state="+$("#state1").val(),"bulan="+$("#bulanke").val()},
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_rute_modal").modal('toggle');
				//var url_hasil="masterdata/rute_obat/get_list_rute"
				var url_hasil="masterdata/rute_obat/get_data_rute"
				$("#list_rute").load(url_hasil);//+"#list_sedian_obat");
				//$("#halaman_rute").listview('refresh');//reload(true);
				//$(this).location.reload(true);
				//$("#hasil").html(e);

			}
		});
	})

	//============== end submit add form
	
});

	$(function(){
		$("#pagepuskesmas").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			$("#pagin").load(url);
		})
		$("#adduser").on("click",function (event){
			$("#modaluser").modal('show');
		})
		$("#deleteuser").on("click",function (event){
			$("#modaldelete").modal('show');
		})
	});
</script>
<div class="panel panel-primary">
<div class="panel-heading">Manajemen User</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">

<div id="pagepuskesmas">
<?php //echo $links; ?>
</div>
	<div id="pagin">
	<img style="float:right;" src="<?php echo $base_url; ?>img/add.png" width="50px" title="tambah user" id="adduser">
	<table class="table table-striped">
		<thead>
		<tr>
			<th>No.</th>
			<th>Username</th>
			<th>Nama Lengkap</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>
			<?php
				/*$i=1;
				foreach ($result as $rows) {
					echo '<tr><td>'.$i.'</td><td>'.$rows->KD_PUSK.'</td><td>'.$rows->NAMA_PUSKES.'</td><td>'.
						$rows->ALAMAT_PUSKESMAS.'</td></tr>';
					$i++;
				}*/
			?>
			<tr>
				<td>1.</td>
				<td>Admin</td>
				<td>Hendri Kurniawan P.</td>
				<td>
					<img src="<?php echo $base_url; ?>img/add.png" width="30px"> | 
					<img src="<?php echo $base_url; ?>img/edit.png" width="30px"> | 
					<img src="<?php echo $base_url; ?>img/delete.png" width="30px" id="deleteuser">
				</td>
			</tr>
			<tr>
				<td>2.</td>
				<td>User</td>
				<td>wawan</td>
				<td>
					<img src="<?php echo $base_url; ?>img/add.png" width="20px"> | 
					<img src="<?php echo $base_url; ?>img/edit.png" width="20px"> | 
					<img src="<?php echo $base_url; ?>img/delete.png" width="20px" id="deleteuser">
				</td>
			</tr>
		</tbody>
	</table>
	</div>
<div class="modal fade" id="modaluser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data Puskesmas</h4>
      </div>
      <div class="modal-body">
        <?php $this->load->view('form_administrator'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data Puskesmas</h4>
      </div>
      <div class="modal-body">
        <img src="<?php echo $base_url; ?>img/peringatan.png" width="50px">
        Apakah Anda yakin akan menghapus data tersebut?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ya</button>
        <button type="button" class="btn btn-primary">Tidak</button>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
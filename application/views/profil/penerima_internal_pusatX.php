 <style>
	
</style>
<script>
$(document).ready(function(){

	//=========== del button
		

	$("#del_penerima_in_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "profil/unit_penerima/delete_list_pusk",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="profil/unit_penerima/puskesmas"
					$("#list_penerima_in").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#input_up_modal').hide();
	$("#add_penerima_in_btn").on("click",function (event){
			//$("#add_penerima_in_modal").modal('show');
			$("#input_up_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_penerima_in_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_penerima_in_modal").modal('show');
			event.preventDefault();
			$("#input_up_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_penerima_in_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="profil/unit_penerima/puskesmas";
	$('#list_penerima_in').load(url);

	//================ submit with <form>
	$('#form_input_up').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            success: function (data) {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#input_up_modal").slideUp("slow");
					$("#partbutton").fadeIn();			
					var url_hasil="profil/unit_penerima/puskesmas";
					$("#list_penerima_in").load(url_hasil);//+"#list_sedian_obat");

					$("#kode_up").val("");
					$("#nama_up").val("");
					$("#telp").val("");
					$("#poned").val("");
					$("#alamat_up").val("");
					$("#long").val("");
					$("#lat").val("");
					$("#fax").val("");
					$("#email").val("");
	            }
			})
			
		});

	//============== submit add form
});

//get village
function getVillage(val) {
    if(val == 'add'){
    	alert("tambah kecamatan");
    	//var url_kec = 'profil/unit_penerima/add_kecamatan'
    	//$('#konten').load(url_kec);
    }else{
    	//alert("cek");
    	$.get('profil/unit_penerima/get_data_desa/' + val, function(data) {$('#desa').html(data);});    	
    }
}
//end get village
//==== add village
function addVillage(val){
	if(val == 'add'){
    	alert("tambah desa");
    }else{
    	var kode_up='P'+val;
    	$('#kode_up').val(kode_up);
    }
}
</script>
	<!-- bag. isi --><br>
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_penerima_in_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_penerima_in_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<!--button id="cha_penerima_in-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</div><!-- /col6 -->
		</div>
			<div class="" id="input_up_modal">
			  <h4>Tambah Unit Penerima</h4>
			<form action="<?php echo $base_url; ?>index.php/profil/unit_penerima/input_data" method="post" id="form_input_up">
				<table class="table">
					<tr>
						<td>Kecamatan</td>
						<td>: <select name="kec_up" id="kec_cup" onchange="getVillage(this.value);">
									<option>--- PILIH ---</option>
									<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
									<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
									<?php endfor; ?>
									<option value="add">--- TAMBAH ---</option>
							</select></td>
						<td>Telepon</td>
						<td>: <input type="text" name="telp" id="telp"></td>
					</tr>
					<tr>
						<td>Desa</td>
						<td>: <select name="desa" id="desa" onchange="addVillage(this.value);">
								<option>--- PILIH ----</option>
								<option value="add">--- TAMBAH ----</option>
								</select>
						</td>
						<td>Fax</td>
						<td>: <input type="text" name="fax" id="fax"></td>
					</tr>
					<tr>
						<td>Kode</td>
						<td>: <input type="text" name="kode_up" id="kode_up" value="" readonly></td>
						<td>Jenis</td>
						<td>: <select name="jenis_up" id="jenis_up">
									<option value="Perawatan">Perawatan</option>
									<option value="Non Perawatan">Non Perawatan</option>
							</select></td>
					</tr>
					<tr>
						<td>Unit Penerima</td>
						<td>: <input type="text" name="nama_up" id="nama_up"></td>
						<td>Poned</td>
						<td>: <select name="poned" id="poned">
									<option value="Ya">Ya</option>
									<option value="Tidak">Tidak</option>
							</select></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>: <textarea name="alamat_up" id="alamat_up"></textarea></td>
						<td>DTPK</td>
						<td>: <select name="dtpk" id="dtpk">
									<option value="Ya">Ya</option>
									<option value="Tidak">Tidak</option>
							</select></td>
					</tr>
					
					<tr>
						<td>Longitude</td>
						<td>: <input type="text" name="long" id="long"></td>
						<td>Email</td>
						<td>: <input type="text" name="email" id="email"></td>
					</tr>
					<tr>
						<td>Latitude</td>
						<td>: <input type="text" name="lat" id="lat"></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td><div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_penerima_in" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
						<td>
						</td>
					</tr>
				</table>
			</form>	
			</div>
			
			
			<br><br>

			<div id="list_penerima_in">
				<?php
					
				?>
			</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#form_restore_db').submit(function(e){
        var url = "profil/manajemen_db";
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData (this),//$(this).serialize(),
            processData: false,
            contentType: false,
            beforeSend: function(){
                showBusySubmit();
            },
            success: function (data) {
                var obj = jQuery.parseJSON(data);
                alert(obj.msg);
                $("#konten").load(url);
            }
        })    
    });

    $('#truncate_db').click(function(){
        var ans=confirm("Are You sure?");
        var url='profil/manajemen_db/truncate_db';
        if(ans==true){
            $.ajax({
              url:url,
              beforeSend: function(){
                  showBusySubmit();
              },//showBusySubmit();
              success: function(){
                alert("Data berhasil dihapus");
                $('#konten').load('profil/manajemen_db');
              }
          });
        }
    })

  $('#fix_db').click(function(){
      var ans=confirm("Are You sure?");
      var url='profil/manajemen_db/fix_db';
        if(ans==true){
            $.ajax({
              url:url,
              dataType:"json",
              beforeSend: function(){
                  showBusySubmit();
              },//showBusySubmit();
              success: function(data){
                console.log(data);
                if(data.repair_tb.status==1){
                  alert("Proses selesai. "+data.update_code_inn_stock);
                  $('#konten').load('profil/manajemen_db');  
                }else{
                  alert("Proses gagal. "+data.update_code_inn_stock);
                }
                
              }
          });
        }
    })

  $('#btn-update').click(function(){
      var ans=confirm("Are You sure?");
      var url='profil/manajemen_db/update_software';
      //var url='profil/manajemen_db/firstClone';
        if(ans==true){
            $.ajax({
              url:url,
              beforeSend: function(){
                  showBusySubmit();
              },//showBusySubmit();
              success: function(){
                alert("Proses selesai. Jangan lupa klik 'Repair Data Transaction' ");
                $('#konten').load('profil/manajemen_db');
              }
          });
        }
    })

    $('#backup_db').click(function(){
        var url_bc='profil/manajemen_db/backup_db';
        var form_data = {
            tanggal:$('#lati').val(),
            nama_pusk:$('#nama_pusk_cha').val()
        }     
        $.ajax({
            type:"POST",
            url:url_bc,
            beforeSend: function(){
                showBusySubmit();
            },
            //data: form_data,
            success: function(){
                alert("Data berhasil dibackup");            
                $('#konten').load('profil/manajemen_db');
            }
        })
    })
});
</script>
<div class="panel panel-primary">
    <div class="panel-heading">Manajemen Database</div>
        <div id="up-konten"class="panel-body" style="padding:15px;">
          <div class="row">
            <div class="col-md-9">            
                <form action="<?php echo $base_url; ?>index.php/profil/manajemen_db/restore_db" method="post" id="form_restore_db" enctype="multipart/form-data">
                    <div style="position:relative;">
                      <a class='btn btn-primary btn-lg' href='javascript:;'>
                        <span class="glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;Pilih File:
                        <input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="datafile" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
                      </a>
                      &nbsp;
                      <span class='label label-info' id="upload-file-info"></span>
                      <br><span class='label label-danger' id="upload-file-danger">Nama file tidak boleh menggunakan spasi atau karakter lainnya kecuali underscore/garis bawah ( _ )</span>
                    </div>
                  <br><button type="submit" id="" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-open"></span> Restore</button>
                </form>
            <hr>
            <button type="submit" id="backup_db" class="btn btn-primary btn-lg"><span class="glyphicon glyphicon-save"></span><a href="<?php echo $base_url; ?>index.php/profil/manajemen_db/backup_db" style="color:white;text-decoration:none;"> Backup All Database </a></button>
            <button type="button" id="truncate_db" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-remove"></span> Delete All Transaction</button>
            <button type="button" id="fix_db" class="btn btn-warning btn-lg"><span class="glyphicon glyphicon-wrench"></span> Repair Data Transaction</button>
            </div>
            <div class="col-md-3">
              <button type="button" id="btn-update" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-arrow-down"></span> Software Update</button>
              <!-- <p>*untuk update secara offline silakan kunjungi <a href="http://bankdataelog.kemkes.go.id/download/" target="blank">http://bankdataelog.kemkes.go.id/download</a></p> -->
            </div>
          </div>
      </div>
</div>
<script>
$(document).ready(function(){
	$('#cek_up').autocomplete({
		source:'profil/unit_penerima/search_live',
		minLength:1,
		focus: function( event, ui ) {
	        $( "#cek_up" ).val( ui.item.value );
	        return false;
	      },
		select:function(evt, ui)
			{
				$( "#cek_up" ).val( ui.item.value );
				$("#kode_up").val(ui.item.KD_PUSK);
				$("#kode_up_hidden").val(ui.item.KD_PUSK);
				$("#nama_up").val(ui.item.value);
				$("#telp").val(ui.item.telp);
				$("#poned").val(ui.item.poned);
				$("#alamat_up").val(ui.item.ALAMAT_PUSKESMAS);
				$("#long").val(ui.item.longitud);
				$("#lat").val(ui.item.latitud);
				$("#fax").val(ui.item.no_fax);
				$("#email").val(ui.item.email);
				$("#kec_cup").val(ui.item.KODE_KEC);
				return false;
			}
	});

	//form submit
	$('#form_search_up').submit(function(e){
		event.preventDefault();
		$.ajax({
			type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            //dataType: 'json',
            success: function (data) {
                //alert('DATA BERHASIL DISIMPAN');
                /*if(data.con==0){
                	alert(data.confirm);
                }else{
                	alert(data.confirm);
	                var url='manajemenlogistik/stok_opnam';
	                $('#konten').load(url);
                }*/
                //$("#list_penerima_in").load(url_hasil);
                $('#list_penerima_in').html(data);
            }
		})

	});
	//end form submit

	$('#sinkron-btn').click(function(){
      var ans=confirm("Are You sure?");
      var url='profil/unit_penerima/sinkron_unit';
        if(ans==true){
            $.ajax({
              url:url,
              dataType: "json",
              beforeSend: function(){
                  showBusySubmit();
              },//showBusySubmit();
              success: function(data){
                alert(data.msg);
                $('#konten').load('profil/unit_penerima');
              }
          });
        }
    })

		$('#sinkron-back-btn').click(function(){
	      var ans=confirm("Are You sure?");
	      var url='profil/unit_penerima/get_master_puskesmas';
	        if(ans==true){
	            $.ajax({
	              url:url,
	              dataType: "json",
	              beforeSend: function(){
	                  showBusySubmit();
	              },//showBusySubmit();
	              success: function(data){
	                alert(data.msg);
	                $('#konten').load('profil/unit_penerima');
	              }
	          });
	        }
	    })

	//=========== del button


	$("#del_penerima_in_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
        	var p = confirm("Apakah Anda yakin akan menghapus data puskesmas ini?");
        	if(p){
        		$.ajax({
		        	url: "profil/unit_penerima/delete_list_pusk",
		        	data: "kode="+id_array,
		        	type: "POST",
		        	success: function(){
		        		alert("data berhasil dihapus");
		        		var url_hasil="profil/unit_penerima/puskesmas"
						$("#list_penerima_in").load(url_hasil);
		        	}
		        })
        	}
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#input_up_modal').hide();
	$("#add_penerima_in_btn").on("click",function (event){
			//$("#add_penerima_in_modal").modal('show');
			$("#input_up_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_penerima_in_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_penerima_in_modal").modal('show');
			event.preventDefault();
			$("#input_up_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_penerima_in_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="profil/unit_penerima/puskesmas";
	$('#list_penerima_in').load(url);

	//================ submit with <form>
	$('#form_input_up').submit(function(e){
			event.preventDefault();
			var c = confirm("Apakah Anda yakin menggunakan kode puskesmas yang sesuai dengan aturan KEMENDAGRI?");
			if(c){
				$.ajax({
					type: $(this).attr('method'),
		            url: $(this).attr('action'),
		            data: $(this).serialize(),
		            success: function (data) {
		                alert('DATA BERHASIL DISIMPAN');
		                $("#input_up_modal").slideUp("slow");
						$("#partbutton").fadeIn();
						var url_hasil="profil/unit_penerima/puskesmas";
						$("#list_penerima_in").load(url_hasil);//+"#list_sedian_obat");

						$("#kode_up").val("");
						$("#nama_up").val("");
						$("#telp").val("");
						$("#poned").val("");
						$("#alamat_up").val("");
						$("#long").val("");
						$("#lat").val("");
						$("#fax").val("");
						$("#email").val("");
						$("#buffer").val("");
						$("#periode-distribusi").val("");
		            }
				})
			}
		});

	//============== submit add form
});

//get village
function getVillage(val) {
    if(val == 'add'){
    	alert("tambah kecamatan");
    	//var url_kec = 'profil/unit_penerima/add_kecamatan'
    	//$('#konten').load(url_kec);
    }else{
    	//alert("cek");
    	//$.get('profil/unit_penerima/get_data_desa/' + val, function(data) {$('#desa').html(data);});
    	var kode_up='P'+val;
    	$('#kode_up').val(kode_up);
    }
}
//end get village
//==== add village
function addVillage(val){
	if(val == 'add'){
    	alert("tambah desa");
    }else{
    	var kode_up='P'+val;
    	$('#kode_up').val(kode_up);
    }
}
function showBusySubmit(){
        $('#konten').block({
            message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>',
            css: {
                border: 'none',
                backgroundColor: '#cccccc',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .6,
                color: '#000',
                width: '130px',
                height: '15px',
                padding: '5px'
            }
        });
    }
</script>
	<!-- bag. isi --><br>
		<div id="partbutton">
			<div class="col-lg-8">
				<button id="add_penerima_in_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_penerima_in_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button>
				<button id="sinkron-btn" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh"></span> Upload</button>
				<button id="sinkron-back-btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-refresh"></span> Download</button>
			</div>
			<div class="col-lg-4">
				<form action="<?php echo $base_url; ?>index.php/profil/unit_penerima/search_data" method="post" id="form_search_up">
				<div class="input-group" style="float:right;">
			      <input type="text" name="key" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			    </form>
			</div><!-- /col6 -->
		</div>
		<i style="float:right;"><small>*double click untuk mengubah</small></i>
			<div class="" id="input_up_modal">
			  <h4>Tambah Unit Penerima</h4>
			 <div class="col-lg-4">
				 <div class="input-group" style="">
				 	<span class="input-group-addon" id="basic-addon1">Cari: </span>
			      	<input type="text" name="key" id ="cek_up" class="form-control">
			    </div><!-- /input-group -->
			 </div>
			<form action="<?php echo $base_url; ?>index.php/profil/unit_penerima/input_data" method="post" id="form_input_up">
				<table class="table">
					<tr>
						<td>Kecamatan</td>
						<td>: <select name="kec_up" id="kec_cup" onchange="getVillage(this.value);">
									<option>--- PILIH ---</option>
									<?php for($i=0;$i<sizeof($kecamatan);$i++): ?>
									<option value="<?php echo $kecamatan[$i]['KDKEC']; ?>"><?php echo $kecamatan[$i]['KECDESC']; ?></option>
									<?php endfor; ?>
									<option value="add">--- TAMBAH ---</option>
							</select></td>
						<td>Telepon</td>
						<td>: <input type="text" name="telp" id="telp"></td>
					</tr>
					<!--tr>
						<td>Desa</td>
						<td>: <select name="desa" id="desa" onchange="addVillage(this.value);">
								<option>--- PILIH ----</option>
								<option value="add">--- TAMBAH ----</option>
								</select>
						</td>
						<td>Fax</td>
						<td>: <input type="text" name="fax" id="fax"></td>
					</tr-->
					<tr>
						<td>Kode</td>
						<td>: <input type="text" name="kode_up" id="kode_up" value="">
								<input type="hidden" name="kode_up_hidden" id="kode_up_hidden" value="">
							<small><i style="color:red">*harus 11 digit</i></small></td>
						<td>Jenis</td>
						<td>: <select name="jenis_up" id="jenis_up">
									<option value="Perawatan">Perawatan</option>
									<option value="Non Perawatan">Non Perawatan</option>
							</select></td>
					</tr>
					<tr>
						<td>Unit Penerima</td>
						<td>: <input type="text" name="nama_up" id="nama_up"></td>
						<td>Poned</td>
						<td>: <select name="poned" id="poned">
									<option value="Ya">Ya</option>
									<option value="Tidak">Tidak</option>
							</select></td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>: <textarea name="alamat_up" id="alamat_up"></textarea></td>
						<td>DTPK</td>
						<td>: <select name="dtpk" id="dtpk">
									<option value="Ya">Ya</option>
									<option value="Tidak">Tidak</option>
							</select></td>
					</tr>

					<tr>
						<td>Longitude</td>
						<td>: <input type="text" name="long" id="long"></td>
						<td>Email</td>
						<td>: <input type="text" name="email" id="email"></td>
					</tr>
					<tr>
						<td>Latitude</td>
						<td>: <input type="text" name="lat" id="lat"></td>
						<td>Fax</td>
						<td>: <input type="text" name="fax" id="fax"></td>
					</tr>
					<tr class="active">
						<td>Buffer</td>
						<td>: <input type="text" name="buffer" id="buffer" size="3"> %</td>
						<td>Periode Distribusi</td>
						<td>: <input type="text" name="periode_distribusi" id="periode-distribusi" size="3"> bulan</td>
					</tr>
					<tr>
						<td colspan="4"><div class="pagingContainer">
								<button type="submit" name="Simpan" id="btn_penerima_in" class="btn btn-success btn-sm buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
								<button id="batal" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-remove"></span> Batal</button>
							</div>
						</td>
						<!--td>
						</td-->
					</tr>
				</table>
			</form>
			</div>


			<br><br>

			<div id="list_penerima_in"></div>

 <style>
	
</style>
<script>
$(document).ready(function(){

	//=========== del button
		

	$("#del_gol_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "profil/unit_penerima_pusat/delete_list_kab",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="profil/unit_penerima_pusat/get_data_kab/"+$('#id_prop').val();
					$("#list_data").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_gol_btn").on("click",function (event){
			$("#add_gol_modal").modal('show');
				//$("#add_gol_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="masterdata/input/get_list_data";
	//$('#list_data_obat').load(url);

	//var url="masterdata/input/get_data_gol";
	var url="profil/unit_penerima_pusat/get_data_kab/"+$('#id_prop').val();
	$('#list_data').load(url);	
	//============== submit add form

	$("#btn_input").click(function(){
		var url2="profil/unit_penerima_pusat/input_data_kab";
		//var new_kode_kab=$('#id_prop').val()+1;
		var form_data = {
			nama_kab:$("#nama_kab").val(),
			lat:$("#lat").val(),
			longi:$("#long").val(),
			kode_kab:$("#kode_kab").val(),
			kode_prop:$('#id_prop').val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_gol_modal").modal('toggle');
				//var url_hasil="masterdata/input/get_list_data"
				var url_hasil="profil/unit_penerima_pusat/get_data_kab/"+$('#id_prop').val();
				$("#list_data").load(url_hasil);//+"#list_sedian_obat");
			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel panel-primary" id="halaman_gol">
	<div class="panel-heading">Data Unit Penerima</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->

			<div class="modal fade" id="add_gol_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Unit Penerima</h4>
			      </div>
			      <div class="modal-body">

			<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				
					<table class="table">
						<tr>
							<td>Kode Kabupaten</td>
							<td>
								<input type="text" name="kode_kab" id="kode_kab" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Nama Unit Penerima</td>
							<td>
								<input type="text" name="nama_kab" id="nama_kab" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Latitude</td>
							<td>
								<input type="text" name="lat" id="lat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Longitude</td>
							<td>
								<input type="text" name="long" id="long" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_input" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
								<input type="reset" name="Reset" id="reset" value="Batal" /-->
							</td>
						</tr>
					</table>
					</div>
					</div>
				</div>
			</div>
			<!--/form-->
			<h4><label>Provinsi: <?php echo $result['CPropDescr']; ?></label></h4>
				<input type="hidden" id="id_prop" name="id_prop" value="<?php echo $result['CPropID']; ?>">

			<button id="add_gol_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
			<button id="del_gol_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
			<button id="sinkron_btn" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-refresh"></span> Sinkronisasi</button>
			<br><br>

			<div id="list_data"></div>
	</div>
</div>
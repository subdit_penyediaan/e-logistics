<!DOCTYPE html>
<html lang="en">
	<?php $this->load->view('head'); ?>
<body style="background-color:light-blue;padding-top:80px;" id="elogistik">

<!--div id="bingkai"--><div id="" class="">
	<!--div id="kop" class="jumbotron"-->
	<div id="" class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
		      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
		        <span class="sr-only">Toggle navigation</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="#">
		      	<img style="float:left; margin-right:10px;" src="<?php echo $base_url; ?>img/Logo-Kemenkes-2017-1.png" width="40px">
		      </a>
		    </div>
		    <div id="navbar" class="navbar-collapse collapse">
		    	<ul class="nav navbar-nav navbar-left">
		    		<li>
		    			<div class="" style="padding-top:20px;">
		    				<span style="font-size:20px;color:#428bca;" class=""><b>E-LOGISTIK</b></span>
		    				<?php if (!$registered) echo "(Unregistered)"; ?>
		    				<br><span class="" style="font-size:12px;">Aplikasi manajemen obat dan perbekalan kesehatan Kementerian Kesehatan Republik Indonesia</span>
				      	</div>
		    		</li>
		    	</ul>
				<ul class="nav navbar-nav navbar-right">
			        <li class="">
			        	<img style="float:left; margin:15px 5px;" src="<?php echo $base_url; ?>img/user-elog.png" width="40px">
			        	<a href="#">Welcome  <b><?= (strlen($user) > 3) ? $user : "hi, ".$user ?></b></a>
			        </li>
			        <li class="active">
			        	<a href="#">
			        		<span style="font-size:12px;">
				        	<?php if($profil['levelinstitusi']==3): ?>
								Provinsi <?php echo $profil['nama_prop']."<br>Kabupaten ".$profil['nama_kab']; ?>
							<?php elseif($profil['levelinstitusi']==2): ?>
								Provinsi <?php echo $profil['nama_prop'] ?>
							<?php elseif($profil['levelinstitusi']==1): ?>
								<b>Instalasi Farmasi Pusat<br> KEMENTERIAN KESEHATAN</b>
							<?php endif; ?>
							</span>
						</a>
			        </li>
			        <li>
			        	<a href="<?php echo $base_url; ?>index.php/login/logout" style="color:#000000;"> LOGOUT <span class="glyphicon glyphicon-off"></span>
			        		<?php //echo date('d-m-Y') ?>
			        	</a>
			        </li>
			    </ul>
			</div>
		</div>
	</div>
<!--/div-->
<!-- Halaman utama -->
<div id="running-text"></div>
<div class="row">
	<div class="col-xs-4 col-sm-3 col-md-2" id="menuleft"><!-- bag. menu -->
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading"><span class="glyphicon glyphicon-align-justify"></span> Menu Item</div>
					<div id="cssmenu" class="panel-body">
						<ul>
						<!-- <li style="" class=''><a href="">Dashboard</a></li> -->
						<!-- MODUL SUPER ADMIN -->
						<?php
							if ($this->session->userdata('group_id')) {
								$is_admin = $this->session->userdata('group_id');
							} else {
								$is_admin = $this->session->userdata('id');
							}

							if ($is_admin == 1) {
								$this->load->view('menu_profil');
							} elseif ($is_admin == 4) {
									foreach ($menu_all as $rows) {
										if ($rows->modul_id == 5) {
												$menu[$rows->modul_id][$rows->id]='<li><a href="'.$base_url.'index.php/'.$rows->url_menu.'"><span class="glyphicon glyphicon-arrow-right"></span> '.$rows->label_menu.'</a></li>';
										}
									}
									foreach ($menuParent as $key) {
										if ($key->id == 5) {
											echo '<li class="'.$key->class.'"><a href="#"><span>'.$key->label_modul.'</span></a>';
											$id=$key->id;
											echo '<ul>';
											foreach ($menuChild as $key2) {
												if (isset($menu[$id][$key2->id])){
													print_r($menu[$id][$key2->id]);
												}
											}
											echo '</ul></li>';
										}
									}
							} else {
								foreach ($menu_all as $rows) {
									$menu[$rows->modul_id][$rows->id]='<li><a href="'.$base_url.'index.php/'.$rows->url_menu.'"><span class="glyphicon glyphicon-arrow-right"></span> '.$rows->label_menu.'</a></li>';
								}
								foreach ($menuParent as $key) {
									echo '<li class="'.$key->class.'"><a href="#"><span>'.$key->label_modul.'</span></a>';
									$id=$key->id;
									echo '<ul>';
									foreach ($menuChild as $key2) {
										if (isset($menu[$id][$key2->id])){
											print_r($menu[$id][$key2->id]);
										}
									}
									echo '</ul></li>';
								}
							}
						?>
					    </ul>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-primary">
					<div class="panel-heading"><span class="glyphicon glyphicon glyphicon-time"></span> Log Aktivitas 1 Bulan</div>
					<div id="" class="panel-body">
						<div class="alert alert-info">
							<ul id="log-activities" style="overflow:scroll; height:400px;"></ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-8 col-sm-9 col-md-10" style="padding-left:0px;">

		<div id="konten"> <!-- bag. isi -->
			<div class="panel panel-primary">
				<div class="panel-heading"><span class="glyphicon glyphicon-home"></span> <b>Dashboard</b></div>
					<div id="up-konten"class="panel-body" style="padding:15px;">
						<?php if(!$registered): ?>
							<div class="alert alert-danger">
								<p><b>UNREGISTERED</b></p>
								<p>Untuk kepentingan pemantauan penggunaan aplikasi e-logistics, diharapkan
								bapak/ibu petugas dinas kesehatan dapat meregistrasikan aplikasi e-logisticsnya
								ke bankdataelog.kemkes.go.id</p>
								<p>Caranya dengan mendaftarkan diri ke bankdataelog.kemkes.go.id pada menu registrasi.
								Setelah prosesnya berhasil, silakan bapak/ibu memasukkan username dan passwordnya ke e-logistics
								dengan membuka menu setting institusi.</p>
								<p>Dengan meregistrasikan e-logistics bapak/ibu telah menyetujui segala ketentuan yang berlaku.</p>
							</div>
						<?php else: ?>

						<?php endif; ?>
						<?php if (sizeof($warning_message) > 0): ?>
							<?php foreach ($warning_message as $key => $value): ?>
									<div class="alert alert-<?= $value->type ?> fade in alert-dismissible">
										<a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">&times;</a>
										<p><b><?= strtoupper($value->title) ?></b></p>
										<p><?= $value->content ?></p>
										<p><i style="float:right;">periode pesan: <?= $value->date_start ?> sampai <?= $value->date_end ?></i></p>
										<br>
									</div>
							<?php endforeach; ?>
						<?php else: ?>

						<?php endif; ?>
						<div class="row">
							<!-- <div class="col-md-4">
								<div class="panel panel-primary">
									<div class="panel-heading">
										Jumlah Item Obat
									</div>
									<div class="panel-body">
										<div class="alert alert-warning">
											<b>Jumlah item obat</b>
											<p>dihitung berdasarkan surat terbit ijin dan masa kadaluarsanya.</p>
										</div>
									</div>
								</div>
							</div> -->
							<div class="col-md-5">
								<div class="col-md-12">
									<div class="panel panel-primary">
										<div class="panel-heading">
											Jumlah Aset
										</div>
										<div class="panel-body">
											<div class="alert alert-success">
												<b style="font-size: 36px">Rp</b>
												<h1 class="pull-right" id="assets">
													<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>
												</h1>
												<hr>
												<small>dihitung berdasarkan seluruh stok terkini yang masih tersedia</small>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div id="container-part2" style="min-width: 300px; height: 400px; margin: 0 auto">

									</div>
								</div>
								<div class="col-md-12">
									<div id="container" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
								</div>
							</div>
							<div class="col-md-7">
								<div class="panel panel-primary">
									<div class="panel-heading">
										Daftar 10 Besar Obat & BMHP dengan Masa Kedaluwarsa Terpendek
									</div>
									<div class="panel-body">
										<table class="table table-bordered table-striped tabel-hovered">
											<thead>
												<tr class="active">
													<th>No. Batch</th><th>Obat</th><th>Jarak (bulan)</th><th width="20%">ED</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($toptened as $row): ?>
												<tr class="danger">
													<td><?= $row->no_batch ?></td>
													<td><?= $row->nama_obj ?> <?= $row->sediaan ?></td>
													<td><?= $row->jarak ?></td>
													<td><?= $row->ed ?></td>
												</tr>
												<?php endforeach; ?>
											</tbody>
										</table>
										<div class="alert alert-warning">
											<b>Keterangan:</b>
											<p>nilai pada kolom jarak minus (-) artinya adalah kurang sekian bulan lagi.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<script>
				// reportTablePlugin.url = "https://dhis2.asia/idn";
			 //    reportTablePlugin.username = "intan";
			 //    reportTablePlugin.password = "ABCD1234";
			 //    reportTablePlugin.loadingIndicator = true;

			    // chartPlugin.url = "https://dhis2.asia/idn";
			    // chartPlugin.username = "intan";
			    // chartPlugin.password = "ABCD1234";
			    // chartPlugin.loadingIndicator = true;

			    // var r1 = { el: "table1", id: "SUhIjFssXOa" };

			    // chartPlugin.load([r1]);

				// Ext.onReady(function() {

				// DHIS.getTable({
				//   "id": "SUhIjFssXOa",
				//   "url": "https://dhis2.asia/idn",
				//   "el": "table1"
				// });

				// });

				</script>
				<div id="table1"></div>


			</div>
		</div>
	</div>
</div>
<style type="text/css">
	#log-activities {
	    list-style-position: inside;
	    padding-left:0;
	}
</style>
<!--script type="text/javascript" src="<?php echo $base_url; ?>js/jquery.js"></script-->
<script type="text/javascript" src="<?php echo $base_url; ?>js/bootstrap.js"></script>
<script type="text/javascript">
	function reloadLog() {
		var url = '<?= base_url(); ?>mainpage/logactivities';
		$.get(url, function(content) {
			$("#log-activities").html(content);
		})
	}
</script>
<script type="text/javascript">
	reloadLog();
	$("#container-part2").block({
        message: '<img src="<?php echo base_url();?>img/ajax-loader.gif" /> <b>Processing..</b>'
    });

	var options = {
        chart: {
            renderTo:'container',
            type: 'column'
        },
	    title: {
	        text: '10 Besar Obat Stok Terkecil'
	    },
	    subtitle: {
	        text: 'Source: IFK'
	    },
	    xAxis: {
	        type: 'category',
	        labels: {
	            rotation: -45,
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Jumlah Stok'
	        }
	    },
	    legend: {
	        enabled: false
	    },
	    tooltip: {
	        pointFormat: 'Stok: <b>{point.y}</b>'
	    },
	    series: [{
	        name: 'Stok',
	        data: [],
	        dataLabels: {
	            enabled: true,
	            rotation: -90,
	            color: '#FFFFFF',
	            align: 'right',
	            format: '{point.y}', // one decimal
	            y: 10, // 10 pixels down from the top
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    }]
	}

	// var url =  "<?= base_url() ?>mainpage/stock";
 //    $.getJSON(url,  function(data) {
 //        options.series[0].data = data.content;
 //        var chart = new Highcharts.Chart(options);
 //    });

    var url = "<?= base_url(); ?>mainpage/assets";
    $.get(url, function(data) {
    	$("#assets").text(data);
    })

    var options2 = {
        chart: {
            renderTo:'container-part2',
            type: 'column'
        },
	    title: {
	        text: '10 Besar Ketersediaan Obat Terkecil'
	    },
	    subtitle: {
	        text: 'Source: IFK'
	    },
	    xAxis: {
	        type: 'category',
	        labels: {
	            rotation: -45,
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Ketersediaan'
	        }
	    },
	    legend: {
	        enabled: false
	    },
	    tooltip: {
	        pointFormat: 'Ketersediaan: <b>{point.y} bulan</b>'
	    },
	    series: [{
	        name: 'Ketersediaan',
	        data: [],
	        dataLabels: {
	            enabled: true,
	            rotation: -90,
	            color: '#FFFFFF',
	            align: 'right',
	            format: '{point.y}', // one decimal
	            y: 10, // 10 pixels down from the top
	            style: {
	                fontSize: '13px',
	                fontFamily: 'Verdana, sans-serif'
	            }
	        }
	    }]
	}

	var url_ketersediaan =  "<?= base_url() ?>mainpage/ketersediaan";
    $.getJSON(url_ketersediaan,  function(data) {
        options2.series[0].data = data.content;
        var chart = new Highcharts.Chart(options2);
    });
</script>
<!-- footer -->
<hr>
<center>E-Logistics v3.4<br>2022
</div><!--div paling akhir-->
</body>
</html>

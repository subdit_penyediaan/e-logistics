<script type="text/javascript">
    $(function(){
        $('#demoOne').listnav({
            includeAll: false
            //prefixes: ['sus','d']
        });
    });
    $(function(){
        //combogrid
    $('#key_master').combogrid({
          debug:true,
          colModel: [
            //{'columnName':'id','width':'14','label':'id'}, 
            {'columnName':'name','width':'60','label':'Nama Obat'},
            {'columnName':'author','width':'40','label':'Produsen'}],
          url: 'masterdata/data_obat/get_list_obat_grid',
          select: function( event, ui ) {
              $( "#key_master" ).val( ui.item.name );
              
              return false;
          }
      });
        
        $('#add_master_obat_btn').click(function(){
            var x=$("input[name='kategori_objek']:checked").val();
            //alert(x);

            var url='masterdata/data_obat/add_data_obat/'+x;
            $('#konten').load(url);
        })

        //form search
        $('#form_srch_master_obat').submit(function(e){
            event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                //dataType: 'json',
                success: function (data) {
                    //alert('DATA BERHASIL DISIMPAN');
                    
                    $('#data_list_master').html(data); 
                    $('#menu_page').hide();

                }
            })
            
        });
        //end form search

        $("#paging_master").on("click","a",function (event){
            //$("#cssmenu a.active").removeClass('active');
            //$(this).addClass('active');
            event.preventDefault();
            var url = $(this).attr("href");
            $.get(url, function(data) {
                $('#paging_master2').html(data);
            });
            $("#data_list_master").html("");
            $("#paging_master a.active").removeClass('active');
            $(this).addClass('active');
            //event.preventDefault();
            //var url = $(this).attr("href");
            //$("#data_list_master").load(url);
            //alert("pake ajax");
        });

        $("#paging_master2").on("click","a",function(event){
            event.preventDefault();
            var url = $(this).attr("href");
            $("#data_list_master").load(url);
        })
    });
</script>
<style>
.master_data{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.list_master {
    float: left;
    padding: 1px;
}

.a_master:link, .a_master:visited {
    display: block;
    width: 40px;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #428bca;
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
    border-style: 1px solid;
    border-color: white;
}

.a_master:hover{
    background-color: red;
}
.a_master:active {
    background-color: red;
    display: block;
}
</style>
<div class="panel panel-primary">
    <div class="panel-heading">Data Obat</div>
    <div id="up-konten"class="panel-body" style="padding:15px;">
        <!-- bag. isi -->
    <div>
        <input type="radio" name="kategori_objek" id="kategori_objek1" value="obat" checked> Obat<br>
        <input type="radio" name="kategori_objek" id="kategori_objek2" value="bmhp"> Bahan Medis Habis Pakai (BMHP)<br>
        <input type="radio" name="kategori_objek" id="kategori_objek3" value="alkes"> Alat Kesehatan<br><br>
        <button id="add_master_obat_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
        <!--button id="del_master_obat_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button-->
                <!-- /input-group -->
        <hr>
        <form style="" id="form_srch_master_obat" name="form_srch_master_obat" action="<?php echo $base_url; ?>index.php/masterdata/data_obat/search_by" method="post">
            <input type="text" class="" size="40" name="key_master" id="key_master">
            <button type="submit" id="srch_master_obat"><span class="glyphicon glyphicon-search"></span> Cari</button>
        </form>
        
    </div><hr>
    <div id="menu_page">
        <ul id="paging_master" class="master_data">
            <?php echo $list_obat; ?>
        </ul>

        <ul id="paging_master2" class="master_data">
        </ul>
    </div>

        <div id="data_list_master">

        </div>
            
        
    </div>
</div>
<style type="text/css">
	tr{
		padding: 3px;
	}
	td{
		padding: 3px;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	//autocomplete INN
	$('#nama_inn').autocomplete({
		source:'masterdata/data_obat/get_list_inn', 
		minLength:2,
		focus: function(event,ui){
			$('#nama_inn').val(ui.item.value);
			return false;
		},
		select: function(evt, ui){
			$('#kode_inn').val(ui.item.kode_fda);
			return false;
		}
	});
	//end autocomplete INN

	//form submit
		$('#form_add_master').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            dataType: 'json',
	            success: function (data) {
	                //alert('DATA BERHASIL DISIMPAN');
	                //if(data.con==0){
	                	alert(data.text);	
	                //}else{
	                //	alert(data.confirm);
		                //var url='manajemenlogistik/stok_opnam';
		                //$('#konten').load(url);	
	                //}
	            }
			})
			
		});
		//end form submit

	$('#btn_cancel_add_master').click(function(event){
		event.preventDefault();
		var url='masterdata/data_obat';
		$('#konten').load(url);
	})

	//autocomplete pbf
	$('#produsen').autocomplete({
				source:'masterdata/data_obat/get_list_pabrik', 
				minLength:2,
			});
	//end autocomplete pbf
	//$('.inputText').hide();

	$('#btn_ubah_data').click(function(){
		$('.inputText').show();
		$('.labelText').hide();
	})

	var var_glob = $('#new_kode_master').val();
	//$('#detailnamaobat').load('masterdata/detail_data_obat/detail_obat')//url
	$('#kontenobat').load('masterdata/master_konten',{kode_obat:$('#new_kode_master').val()});
	$('#ruteobat').load('masterdata/master_rute',{kode_obat:$('#new_kode_master').val()});
	$('#form_atc').load('masterdata/master_atc',{kode_obat:$('#new_kode_master').val()});
	$('#form_fornas').load('masterdata/master_fornas',{kode_obat:$('#new_kode_master').val()});
	$('#form_ukp4').load('masterdata/master_ukp4',{kode_obat:$('#new_kode_master').val()});
});

function autofill(){
	
}

function getObjectName(){
	var object_name = $('#nama_dagang').val()+' '+$('#sediaan').val()+' '+$('#text_kemasan').val();
	$('#nama_obat').val(object_name);
}
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Tambah Master Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">		
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#detailnamaobat" data-toggle="tab">Data Detail</a></li>
		  <li><a href="#kontenobat" data-toggle="tab">Konten Obat</a></li>  
		  <li><a href="#ruteobat" data-toggle="tab">Rute Obat</a></li>
		  <li><a href="#form_atc" data-toggle="tab">ATC</a></li>
		  <li><a href="#form_fornas" data-toggle="tab">Formularium Nasional</a></li>
		  <li><a href="#form_ukp4" data-toggle="tab">UKP4</a></li>		  
		</ul> 
		<div class="tab-content">
	  		<div class="tab-pane active" id="detailnamaobat">
				<br>
				<form action="<?php echo $base_url; ?>index.php/masterdata/data_obat/input_data" method="post" id="form_add_master">
				<table class="table">
					<tr>
						<td>Kode Obat</td>
						<td>: <label id=""> <?php echo $id_obat;?> </label>
							<input type="hidden" id="new_kode_master" name="new_kode_master" value="<?php echo $id_obat;?>"></td>
					</tr>
					<tr>
						<td>Barcode</td>
						<td>: <input class="inputText" type="text" value="" name="barcode" id="barcode"></td>
					</tr><tr>
						<td>Kode Binfar</td>
						<td>: <input class="inputText" type="text" value="" name="kode_binfar" id="kode_binfar"></td>
					</tr><tr>
						<td>Nomor Registrasi</td>
						<td>: <input class="inputText" type="text" value="" name="no_reg" id="no_reg"></td>
					</tr><tr>
						<td>Konsep</td>
						<td>: <select name="kategori_objek" id="kategori_objek">
								<option value="">--------- Pilih ---------</option>
								<option value="OBAT">Obat</option>
								<option value="BMHP">Bahan Medis Habis Pakai</option>								
							</select></td>
					</tr><tr>
						<td>Nama Dagang</td>
						<td>: <input class="inputText" type="text" value="" name="nama_dagang" id="nama_dagang" onchange="getObjectName();"></td>
					</tr><tr>
						<td>Nama INN</td>
							<td>: <input class="inputText" type="text" value="" size="50" name="nama_inn" id="nama_inn">
								<input class="inputText" type="hidden" value="" name="kode_inn" id="kode_inn"></td>	
					</tr><tr>
						<td>Kekuatan</td>
						<td>: <input class="inputText" type="text" name="text_kemasan" id="text_kemasan" onchange="getObjectName();"></td>
					</tr><tr>
						<td>Bentuk Sediaan</td>
						<td>: <select class="inputText" name="sediaan" id="sediaan"  onchange="getObjectName();">
								<option value="">------ Pilih ------</option>
								<?php for($i=0;$i<sizeof($sediaan);$i++) :?>
									<option value="<?php echo $sediaan[$i]['jenis_sediaan']; ?>">
										<?php echo $sediaan[$i]['jenis_sediaan']; ?>
									</option>
									<?php endfor; ?>
								</select> 
							</td>
					</tr>
					<tr>
						<td>Nama Lengkap</td>
						<td>: <input class="inputText" type="text" value="" size="70" name="nama_obat" id="nama_obat" readonly="readonly"></td>
					</tr><!--tr>
						<td>Satuan Besar</td>
						<td>: <select class="inputText" name="sat_bes" id="sat_bes">
								<option value="">------ Pilih ------</option>
								</select> 
						</td>
					</tr-->
					<tr>
						<td>Detil Kemasan</td>
						<td>: <input class="inputText" type="text" name="detil_kemasan" id="detil_kemasan" size="50"></td>
					</tr>
					<tr>
						<td>Kemasan Unit</td>
						<td>: <select class="inputText" name="kemasan" id="kemasan">
								<option value="">------ Pilih ------</option>
								<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
							</select></td>
					</tr>
					<tr>
						<td>Konversi Satuan Jual</td>
						<td>: <!--input class="inputText" type="text" name="val_satuan_jual" id="val_satuan_jual"-->
								<select class="inputText" name="satuan_jual" id="satuan_jual">
									<option value="">------ Pilih ------</option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select>
							</td>
					</tr><tr>
						<td>Konversi Satuan Klinis</td>
						<td>: <!--input class="inputText" type="text" name="val_satuan_klin" id="val_satuan_klin"-->
								<select class="inputText" name="satuan_klin" id="satuan_klin">
									<option value="">------ Pilih ------</option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['satuan_obat']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select> 
							</td>
					</tr><tr>
						<td>Kategori</td>
						<td>: <select class="inputText" name="kategori" id="kategori">
								<option value="">------ Pilih ------</option>
								<option value="Generik">Generik</option>
								<option value="Generik Bermerek">Generik Bermerek</option>
								<option value="Paten">Paten</option>
								</select> 							
						</td>
					</tr>
					<tr>
						<td>Golongan</td>
						<td>: <select class="inputText" name="gol_obat" id="gol_obat">
								<option value="">------ Pilih ------</option>
								<?php for($i=0;$i<sizeof($golongan);$i++) :?>
									<option value="<?php echo $golongan[$i]['nama_golongan']; ?>">
										<?php echo $golongan[$i]['nama_golongan']; ?>
									</option>
									<?php endfor; ?>
								</select> 							
						</td>
					</tr>
					<tr><td>Jenis Obat</td>
						<td>: <select class="inputText" name="jenis_obat" id="jenis_obat">
								<option value="">------ Pilih ------</option>
								<option value="Tunggal">Tunggal</option>
								<option value="Campuran">Campuran</option>
								</select> 							
						</td></tr>
					<!--tr><td>Kata Kunci</td>
						<td>: <input type="text"></td></tr-->
					<!--tr><td>Image</td>
						<td>: <input type="file"></td></tr-->
					<tr><td>Produsen</td>
						<td>: <input class="inputText" type="text" value="" size="70" name="produsen" id="produsen">							
						</td></tr>
					<tr><td>Status</td>
						<td>: <select class="inputText" name="status" id="status">
								<option value="">------ Pilih ------</option>
								<option value="Approved">Approved</option>
								<option value="Pending">Pending</option>
								<option value="Off">Off</option>
								</select> 							
						</td></tr>
					<!--tr><td>Tanggal penetapan</td>
						<td>: <input type="text"></td></tr-->
				</table>
				<div id="buttonSubmit">
					<hr>
					<p>pastikan semua field isian terisi</p>
					<!--button class="labelText" id="btn_ubah_data"><span class="glyphicon glyphicon-pencil"></span> Ubah Data</button-->
					<button type="submit" id="btn_simpan" class="inputText"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>&ensp;<button id="btn_cancel_add_master" class="inputText"><span class="glyphicon glyphicon-ban_circle"></span> Batal</button>
				</div>
				</form>
			</div><!-- end div detailnamaobat -->
			<div class="tab-pane" id="kontenobat">
				
			</div><!-- end div kontenobat -->
			<div class="tab-pane" id="ruteobat">
				
			</div><!-- end div ruteobat -->
			<div class="tab-pane" id="form_atc">
				
			</div><!-- end div atc -->
			<div class="tab-pane" id="form_fornas">
				
			</div><!-- end div fornas -->
			<div class="tab-pane" id="form_ukp4">
				
			</div><!-- end div ukp4 -->			
		</div><!-- end div tab conten-->

		
	</div>
</div>
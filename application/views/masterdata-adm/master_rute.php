<style>
</style>
<script>
$(document).ready(function(){
	$('#nama_rute').autocomplete({
				source:'masterdata/master_rute/get_list_rute', 
				minLength:2,
				select:function(event,ui){
					$('#id_rute').val(ui.item.id_rute);
					return false;
				}
			});

	
	$('#cha_master_rute-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	
	    	$.ajax({
	    		url:'masterdata/master_rute/datatochange/' + id_array[0], 	    		
	    		type:"POST",
	    		select:function(){	    			
	    		},
	    		success:function(ui,item) {
	    			$('#cha_no_faktur').val(ui.item.nama_dok.value);
	    			$('#change_master_rute').modal('show');
	    			return false;
	    		}
	    	});
	    }
	})

	//=========== del button
		
	$("#del_master_rute_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_rute/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_rute/get_data_master_rute/"+$("#kode_obat_r").val();
					$("#list_master_rute").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_master_rute_modal').hide();
	$("#add_master_rute_btn").on("click",function (event){
			//$("#add_master_rute_modal").modal('show');
			$("#form_master_rute_modal").slideDown("slow");
			$("#partbutton_rute").fadeOut();
				//$("#add_master_rute_modal").modal({keyboard:false});
		});
	$("#batal_rute").on("click",function (event){
			//$("#add_master_rute_modal").modal('show');
			$("#form_master_rute_modal").slideUp("slow");
			$("#partbutton_rute").fadeIn();
				//$("#add_master_rute_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_rute/get_data_master_rute/"+$("#kode_obat_r").val();
	$('#list_master_rute').load(url);	
	//============== submit add form

	$("#btn_master_rute").click(function(){
		var url2="masterdata/master_rute/input_data";
		var form_data = {
			id_rute:$("#id_rute").val(),
			kode_obat:$("#kode_obat_r").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_master_rute_modal").slideUp("slow");
				$("#partbutton_rute").fadeIn();
				var url_hasil="masterdata/master_rute/get_data_master_rute/"+$("#kode_obat_r").val();
				$("#list_master_rute").load(url_hasil);//+"#list_sedian_obat");

				$("#id_rute").val("");
				$("#nama_rute").val("");
			}
		});
	})

	//============== end submit add form
	
});
</script>
		<!-- bag. isi --><br>
		<div id="partbutton_rute">
			<button id="add_master_rute_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
			<button id="del_master_rute_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
		</div>
			<div class="" id="form_master_rute_modal">
			  <h4>Tambah Rute</h4>		
				<table class="table">
						<tr>
							<td>Nama Rute</td>
							<td><input type="text" class="form-control" name="nama_rute" id="nama_rute">
								<input type="hidden" name="id_rute" id="id_rute">
								<input type="hidden" name="kode_obat_r" id="kode_obat_r" value="<?php echo $kode_obat; ?>"></td>
							
						</tr>
						<tr>
							<td><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_rute" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal_rute"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							<td>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_master_rute"></div>
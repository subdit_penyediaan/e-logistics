<script type="text/javascript">
	$(function(){
		$("#pagelistmaster_fornas").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_master_fornas').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistmaster_fornas">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<hr>
	<table class="table table-striped">
		<thead>
		<tr>
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Kode Fornas</th>
			<th>Nama Fornas</th>
			<th>Parent</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_master_fornas" value="'.$rows->id_map.'" class="chk">
					</td>
					<td>'.$i.'</td>
					<td>'.$rows->id_fornas.'</td>
					<td>'.$rows->nama_fornas.'</td>
					<td>'.$rows->parent.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<style>
.datepicker{
	z-index:1151;
}	
</style>
<script>
$(document).ready(function(){
	//autocomplete pbf
	//$('#id_obat_konten').val(var_glob);

	$('#nama_konten').autocomplete({
				source:'masterdata/master_konten/get_list_konten', 
				minLength:2,
				select:function(event,ui){
					$('#id_konten').val(ui.item.id_konten);
					return false;
				}
			});
	//end autocomplete pbf
	$('#cha_master_konten_btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	//$('#change_master_konten').modal('show');
	    	var url='masterdata/master_konten/datatochange/' + id_array[0];

			$.getJSON(url, function(data) {
					//$('#nm_fornas_input').html(data);
					$('#cha_nama_konten').text(data.nama_konten);
					$('#cha_id_map').val(id_array[0]);
					$('#cha_vol_strength').val(data.vol_s);
					$('#cha_strength_unit').val(data.unit_s);
					$('#cha_vol_container').val(data.vol_c);
					$('#cha_container_unit').val(data.unit_c);
					$('#change_master_konten').modal('show');
					
					}
				);
	    	/*
	    	$.ajax({
	    		url:'masterdata/master_konten/datatochange/' + id_array[0], 	    		
	    		type:"POST",
	    		select:function(){	    			
	    		},
	    		success:function(ui,item) {
	    			//$('#cha_no_faktur').val(ui.item.nama_dok.value);
	    			$('#change_master_konten').modal('show');
	    			return false;
	    		}
	    	});
	    	*/
	    }
	})

	//=========== del button
		
	$("#del_master_konten_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_konten/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_konten/get_data_master_konten/"+$("#id_obat_konten").val();
					$("#list_master_konten").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_master_konten_modal').hide();
	$("#add_master_konten_btn").on("click",function (event){
			//$("#add_master_konten_modal").modal('show');
			$("#form_master_konten_modal").slideDown("slow");
			$("#partbutton").fadeOut();
				//$("#add_master_konten_modal").modal({keyboard:false});
		});
	$("#batal").on("click",function (event){
			//$("#add_master_konten_modal").modal('show');
			$("#form_master_konten_modal").slideUp("slow");
			$("#partbutton").fadeIn();
				//$("#add_master_konten_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_konten/get_data_master_konten/"+$("#id_obat_konten").val();
	$('#list_master_konten').load(url);	
	//============== submit add form

	$("#btn_master_konten").click(function(){
		var url2="masterdata/master_konten/input_data";
		var form_data = {
			id_konten:$("#id_konten").val(),
			kode_obat:$("#id_obat_konten").val(),
			strength:$("#vol_strength").val(),
			container:$("#vol_container").val(),
			unit_str:$("#strength_unit").val(),
			unit_cnt:$("#container_unit").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_master_konten_modal").slideUp("slow");
				$("#partbutton").fadeIn();
				var url_hasil="masterdata/master_konten/get_data_master_konten/"+$("#id_obat_konten").val();
				$("#list_master_konten").load(url_hasil);//+"#list_sedian_obat");

				$("#id_konten").val("");
				$("#nama_konten").val("");
				$("#vol_strength").val("")
				$("#vol_container").val("");
				$("#strength_unit").val("");
				$("#container_unit").val("");
			}
		});
	})

	//============== end submit add form
	//============== submit add form

	$("#upt_master_konten").click(function(){
		var url2="masterdata/master_konten/update_data";
		var form_data = {
			id_map:$("#cha_id_map").val(),
			vol_s:$('#cha_vol_strength').val(),
			unit_s:$('#cha_strength_unit').val(),
			vol_c:$('#cha_vol_container').val(),
			unit_c:$('#cha_container_unit').val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("Update data berhasil");
				$('#change_master_konten').toggle();
				var url_hasil="masterdata/master_konten/get_data_master_konten/"+$("#id_obat_konten").val();
				$("#list_master_konten").load(url_hasil);//+"#list_sedian_obat");

				$("#cha_id_map").val("");
				$('#cha_vol_strength').val("");
				$('#cha_strength_unit').val("");
				$('#cha_vol_container').val("");
				$('#cha_container_unit').val("");
			}
		});
	})

	//============== end submit add form
});
</script>
		<!-- bag. isi --><br>
		<div id="partbutton">
			<button id="add_master_konten_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_master_konten_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_master_konten_btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			<!--div class="col-lg-8">
				<button id="add_master_konten_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_master_konten_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_master_konten-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			</div>
			<div class="col-lg-4">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><-!- /input-group -!->
			</div--><!-- /col6 -->
		</div>
			<div class="" id="form_master_konten_modal">
			  <h4>Tambah Konten</h4>
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				<table class="table table-condensed">
						<tr>
							<td width="20%">Nama Konten</td>
							<td width="80%"><input type="text" class="form-control" name="nama_konten" id="nama_konten" size="40">
								<input type="hidden" name="id_konten" id="id_konten">
								<input type="hidden" name="id_obat_konten" id="id_obat_konten" value="<?php echo $kode_obat?>">
								</td>
							
						</tr><tr>
							<td>Strength</td>
							<td><input type="text" name="vol_strength" id="vol_strength" size="5">
								<select name="strength_unit" id="strength_unit">
									<option value="">------ Pilih -----</option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['id']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select></td>
						</tr>
						<tr><td>Container</td>
							<td><input type="text" name="vol_container" id="vol_container" size="5">
								<select name="container_unit" id="container_unit">
									<option value="">------ Pilih -----</option>
									<?php for($i=0;$i<sizeof($satuan);$i++) :?>
									<option value="<?php echo $satuan[$i]['id']; ?>">
										<?php echo $satuan[$i]['satuan_obat']; ?>
									</option>
									<?php endfor; ?>
								</select></td>
						</tr>
						<tr>
							<td><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_konten" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							<td>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_master_konten"></div>
<!-- MODAL UBAH DATA -->
<div class="modal fade" id="change_master_konten" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Data Konten</h4>
      </div>
      <div class="modal-body">

<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
	
		<table class="table">
			<tr>
				<td>Nama Konten</td>
				<td><label id="cha_nama_konten"></label>
					<input type="hidden" id="cha_id_map"></td>
			</tr>
			<tr>
				<td>Strength</td>
				<td><input type="text" name="cha_vol_strength" id="cha_vol_strength" size="5">
					<select name="cha_strength_unit" id="cha_strength_unit">
						<option value="">------ Pilih -----</option>
						<?php for($i=0;$i<sizeof($satuan);$i++) :?>
						<option value="<?php echo $satuan[$i]['id']; ?>">
							<?php echo $satuan[$i]['satuan_obat']; ?>
						</option>
						<?php endfor; ?>
					</select></td>
			</tr>
			<tr><td>Container</td>
				<td><input type="text" name="cha_vol_container" id="cha_vol_container" size="5">
					<select name="cha_container_unit" id="cha_container_unit">
						<option value="">------ Pilih -----</option>
						<?php for($i=0;$i<sizeof($satuan);$i++) :?>
						<option value="<?php echo $satuan[$i]['id']; ?>">
							<?php echo $satuan[$i]['satuan_obat']; ?>
						</option>
						<?php endfor; ?>
					</select></td>
			</tr>
			<tr>
				<td><div class="pagingContainer">
						<button type="submit" name="" id="upt_master_konten" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button id="upt_batal"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
				</td>
				<td>
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		//form submit
		$('#form_update').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function () {
	                alert('DATA BERHASIL DISIMPAN');
	                $("#upt_modal").modal('toggle');
	                $('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
	                var url_hasil="masterdata/master_alkesgen/get_data_master_alkesgen";
					$("#list_master_alkesgen").load(url_hasil);//+"#list_sedian_obat");
				
	            }
			})
			
		});
		//end form submit

		$("#pagelist tr").on("dblclick",function (event){
				
				var testindex = $("#pagelist tr").index(this);
				var form_data = { kode_alkesgen: $("#kode_alkesgen_"+testindex).val() };
				//var nilai = $("#kode_alkesgen_"+testindex).val();
				//alert(nilai);
				//$("#upt_modal").modal('show');

				var url="masterdata/master_alkesgen/get_detail/";
				$.ajax({
					type:"POST",
					url:url,
					data: form_data,
					dataType:'json',
					success:function(data){
						//$('#').text(data.kode_obat);
						$('#upt_nama_master_alkesgen').val(data.generik_name);
						$('#upt_kode_alkesgen').val(data.kode);
						
						$('#upt_parent').val(data.parent);
						$('#upt_selected_ornot').val(data.select);
						$('#upt_fda_code').val(data.fda_code);
						$('#upt_fda_name').val(data.fda_name);

						//$( "#dtpk_cha option:selected" ).text(data.dtpk).attr('value',data.dtpk);
						
						$("#upt_modal").modal('show');

					}
				});
				
				//end ajax
			});
			//end pagepuskesmas tr

		$("#pagelist").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_master_alkesgen').load(url);
		});
	});
</script>
<div id="pagelist">
<?php echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<table class="table table-striped table-hover">
		<thead>
		<tr>
			<th>Select:</th>
			<th>Kode</th>
			<th>Nama Generik</th>
			<th>Parent</th>
			<th>FDA Code</th>
			<th>FDA Name</th>
			<!--th>Sele</th-->
		</tr>
		</thead>
		<tbody>
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;"><td><input type="checkbox" name="chk[]" id="cek_del_master_alkesgen" value="'.$rows->kode.'" class="chk">
					<td><input type="hidden" name="kode_alkesgen" id="kode_alkesgen_'.$i.'" value="'.$rows->kode.'">
						'.$rows->kode.'</td>
					<td>'.$rows->generik_name.'</td>
					<td>'.$rows->parent.'</td>
					<td>'.$rows->fda_code.'</td>
					<td>'.$rows->fda_name.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<!-- modal -->
<div class="modal fade" id="upt_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Ubah Master Generik</h4>
	      </div>
	      <div class="modal-body">

		<form action="<?php echo $base_url; ?>index.php/masterdata/master_alkesgen/update_data" method="post" id="form_update">
		
			<table class="table">
				<table class="table">
					<tr>
						<td>Kode</td>
						<td>
							<input type="text" name="uptdata[kode_alkesgen]" id="upt_kode_alkesgen" size="30" class="form-control input-sm"/>
						</td>
					</tr>
					<tr>
						<td>Nama Alkes</td>
						<td>
							<input type="text" name="uptdata[nama_alkesgen]" id="upt_nama_master_alkesgen" size="30" class="form-control"/>
						</td>
					</tr>
					<tr>
						<td>Parent</td>
						<td>
							<input type="text" name="uptdata[parent]" id="upt_parent" size="15" class="form-control"/>
						</td>
					</tr>
					<tr>
						<td>Selected</td>
						<td>
							<select name="uptdata[selected]" id="upt_selected_ornot">
								<option value="Yes">YES</option>
								<option value="No">NO</option>
							</select>								
						</td>
					</tr>
					<tr>
						<td>FDA Code</td>
						<td>
							<input type="text" name="uptdata[fda_code]" id="upt_fda_code" size="15" class="form-control inp-sm"/>
						</td>
					</tr>
					<tr>
						<td>FDA Name</td>
						<td>
							<input type="text" name="uptdata[fda_name]" id="upt_fda_name" size="30" class="form-control"/>
						</td>
					</tr>
				<tr>
					<td></td>
					<td>
						<div class="pagingContainer">
							<button type="submit" name="Simpan" id="btn_master_alkesgen_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
							<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
						</div>
						
					</td>
				</tr>
			</table>
		</form>
			</div>
			</div>
		</div>
	</div><!-- end modal -->
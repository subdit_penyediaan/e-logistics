<script type="text/javascript">
$(document).ready(function(){
	$('#detailnamaobat').load('masterdata/detail_data_obat/detail_obat')//url
	$('#kontenobat').load('masterdata/detail_data_obat/detail_konten_obat')
});

function autofill(){
	
}
</script>

<div class="panel panel-primary">
	<div class="panel-heading">Detail Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#detailnamaobat" data-toggle="tab">Nama Obat</a></li>
		  <li><a href="#kontenobat" data-toggle="tab">Konten Obat</a></li>  
		  <li><a href="#ruteobat" data-toggle="tab">Rute Obat</a></li>
		  <li><a href="#atc" data-toggle="tab">ATC</a></li>
		  <li><a href="#fornas" data-toggle="tab">Formularium Nasional</a></li>
		  <li><a href="#ukp4" data-toggle="tab">UKP4</a></li>
		  <li><a href="#info" data-toggle="tab">Info Tambahan</a></li>
		</ul> 
		<div class="tab-content">
	  		<div class="tab-pane active" id="detailnamaobat">
				form detail obat
			</div><!-- end div detailnamaobat -->
			<div class="tab-pane" id="kontenobat">
				form konten obat
			</div><!-- end div kontenobat -->
			<div class="tab-pane" id="ruteobat">
				form rute obat
			</div><!-- end div ruteobat -->
			<div class="tab-pane" id="atc">
				form atc
			</div><!-- end div atc -->
			<div class="tab-pane" id="fornas">
				form fornas
			</div><!-- end div fornas -->
			<div class="tab-pane" id="ukp4">
				form ukp4
			</div><!-- end div ukp4 -->
			<div class="tab-pane" id="info">
				<br>
				<table>
					<tr><td>Kata Kunci</td>
						<td>: <input type="text"></td></tr>
					<tr><td>Image</td>
						<td>: <input type="file"></td></tr>
					<tr><td>Produsen</td>
						<td>: <input type="text"></td></tr>
					<tr><td>Status</td>
						<td>: <select><option>Approved</option></select></td></tr>
					<tr><td>Tanggal penetapan</td>
						<td>: <input type="text"></td></tr>
				</table>
			</div><!-- end div info -->
		</div><!-- end div tab conten-->

		<div id="buttonSubmit">
			<hr>
			<p>pastikan semua field isian terisi</p>
			<button>Simpan</button>
		</div>
	</div>
</div>
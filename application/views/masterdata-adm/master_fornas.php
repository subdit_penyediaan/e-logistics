<style>
</style>
<script>
	//tambahan autocomplete with category
  $.widget( "custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function( ul, items ) {
      var that = this,
        currentCategory = "";
      $.each( items, function( index, item ) {
        if ( item.category != currentCategory ) {
          ul.append( "<li class='ui-autocomplete-category'><b>" + item.category + "</b></li>" );
          currentCategory = item.category;
        }
        that._renderItemData( ul, item );
      });
    }
  });
 </script>
<script>
$(document).ready(function(){
	//autocomplete with category
	$( "#nama_fornas" ).catcomplete({
      delay: 0,
      source: 'masterdata/master_fornas/get_list_fornas',
      focus: function( event, ui ) {
			        $( "#nama_fornas" ).val( ui.item.value );
			        return false;
			      },
				select:function(evt, ui)
				{					
					$('#id_fornas').val(ui.item.id_fornas);
					
					return false;
				}
	//		}
    });
    //end autocomplete with category

	$('#cha_master_fornas-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        if(id_array.length<1){
        	alert('pilih data dulu');
	        
	    }else if(id_array.length > 1) {alert("pilih satu saja")}
	    else {
	    	
	    	$.ajax({
	    		url:'masterdata/master_fornas/datatochange/' + id_array[0], 	    		
	    		type:"POST",
	    		select:function(){	    			
	    		},
	    		success:function(ui,item) {
	    			$('#cha_no_faktur').val(ui.item.nama_dok.value);
	    			$('#change_master_fornas').modal('show');
	    			return false;
	    		}
	    	});
	    }
	})

	//=========== del button
		
	$("#del_master_fornas_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/master_fornas/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/master_fornas/get_data_master_fornas/"+$("#kode_obat_f").val();
					$("#list_master_fornas").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form
	$('#form_master_fornas_modal').hide();
	$("#add_master_fornas_btn").on("click",function (event){
			//$("#add_master_fornas_modal").modal('show');
			$("#form_master_fornas_modal").slideDown("slow");
			$("#partbutton_fornas").fadeOut();
				//$("#add_master_fornas_modal").modal({keyboard:false});
		});
	$("#batal_fornas").on("click",function (event){
			//$("#add_master_fornas_modal").modal('show');
			$("#form_master_fornas_modal").slideUp("slow");
			$("#partbutton_fornas").fadeIn();
				//$("#add_master_fornas_modal").modal({keyboard:false});
		});
	//================ end show add form

	var url="masterdata/master_fornas/get_data_master_fornas/"+$("#kode_obat_f").val();
	$('#list_master_fornas').load(url);	
	//============== submit add form

	$("#btn_master_fornas").click(function(){
		var url2="masterdata/master_fornas/input_data";
		var form_data = {
			id_fornas:$("#id_fornas").val(),
			kode_obat:$("#kode_obat_f").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				alert("sukses tambah data");
				$("#form_master_fornas_modal").slideUp("slow");
				$("#partbutton_fornas").fadeIn();
				var url_hasil="masterdata/master_fornas/get_data_master_fornas/"+$("#kode_obat_f").val();
				$("#list_master_fornas").load(url_hasil);//+"#list_sedian_obat");

				$("#id_fornas").val("");
				$("#nama_fornas").val("");
			}
		});
	})

	//============== end submit add form
	
});
</script>
		<!-- bag. isi --><br>
		<div id="partbutton_fornas">
			<button id="add_master_fornas_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
			<button id="del_master_fornas_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
		</div>
			<div class="" id="form_master_fornas_modal">
			  <h4>Tambah fornas</h4>		
				<table class="table">
						<tr>
							<td>Nama fornas</td>
							<td><input type="text" class="form-control" name="nama_fornas" id="nama_fornas">
								<input type="hidden" name="id_fornas" id="id_fornas">
								<input type="hidden" name="kode_obat_f" id="kode_obat_f" value="<?php echo $kode_obat; ?>"></td>
							
						</tr>
						<tr>
							<td><div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_master_fornas" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<button id="batal_fornas"><span class="glyphicon glyphicon-remove"></span> Batal</button>
								</div>
							</td>
							<td>
							</td>
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_master_fornas"></div>
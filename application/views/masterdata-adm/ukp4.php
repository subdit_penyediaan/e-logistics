 <style>
	
</style>
<script>
$(document).ready(function(){
	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_ukp4").html(data);
	            }
			})
			
		});
		//end form search

	//=========== del button
		

	$("#del_ukp4_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/ukp4/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/ukp4/get_data_ukp4"
					$("#list_ukp4").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_ukp4_btn").on("click",function (event){
			$("#add_ukp4_modal").modal('show');
				//$("#add_ukp4_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="masterdata/ukp4_obat/get_list_ukp4";
	//$('#list_ukp4_obat').load(url);

	var url="masterdata/ukp4/get_data_ukp4";
	$('#list_ukp4').load(url);	
	//============== submit add form

	$("#btn_ukp4").click(function(){
		var url2="masterdata/ukp4/input_data";
		var form_data = {
			nama_ukp4_obat:$("#nama_ukp4_obat").val(),
			ket_ukp4_obat:$("#ket_ukp4_obat").val(),
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_ukp4_modal").modal('toggle');
				//var url_hasil="masterdata/ukp4_obat/get_list_ukp4"
				var url_hasil="masterdata/ukp4/get_data_ukp4"
				$("#list_ukp4").load(url_hasil);//+"#list_sedian_obat");
			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel panel-primary" id="halaman_ukp4">
	<div class="panel-heading">UKP4</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_ukp4_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah ukp4ongan Obat</h4>
			      </div>
			      <div class="modal-body">

			<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				
					<table class="table">
						<tr>
							<td>Nama ukp4ongan</td>
							<td>
								<input type="text" name="nama_ukp4_obat" id="nama_ukp4_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td>
								<input type="text" name="ket_ukp4_obat" id="ket_ukp4_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_ukp4_obat" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
								<input type="reset" name="Reset" id="reset" value="Batal" /-->
							</td>
						</tr>
					</table>
					</div>
					</div>
				</div>
			</div>
			<!--/form-->
			<div class="col-lg-8">
				<button id="add_ukp4_btn"><span class="glyphicon glyphicon-plus"></span> Tambah ukp4</button>
				<button id="del_ukp4_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_ukp4-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/ukp4/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 -->
			<br><br>

			<div id="list_ukp4"></div>
	</div>
</div>
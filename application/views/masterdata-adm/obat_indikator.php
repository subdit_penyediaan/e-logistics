 <style>
	
</style>
<script>
$(document).ready(function(){
	//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_data").html(data);
	            }
			})
			
		});
		//end form search

	//=========== del button
		

	$("#del_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/obat_indikator/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/obat_indikator/get_data"
					$("#list_data").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_btn").on("click",function (event){
		$("#add_modal").modal('show');
	});
	//================ end show add form

	//var url="masterdata/pbf_obat/get_list_data";
	//$('#list_pbf_obat').load(url);

	var url="masterdata/obat_indikator/get_data";
	$('#list_data').load(url);	
	//============== submit add form

	$("#btn_input").click(function(){
		var url2="masterdata/obat_indikator/input_data";
		var form_data = {
			nama_obat:$("#nama_obat").val(),
			satuan:$("#satuan").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_modal").modal('toggle');
				//var url_hasil="masterdata/pbf_obat/get_list_data"
				var url_hasil="masterdata/obat_indikator/get_data"
				$("#list_data").load(url_hasil);//+"#list_sedian_obat");
			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel panel-primary" id="">
	<div class="panel-heading">Daftar Nama Obat Indikator</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah Obat Indikator</h4>
			      </div>
			      <div class="modal-body">

			<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				
					<table class="table">
						<tr>
							<td>Nama Obat</td>
							<td>
								<input type="text" name="" id="nama_obat" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Satuan</td>
							<td>
								<input type="text" name="satuan" id="satuan" size="30" class="form-control"/>
							</td>
						</tr>						
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_input" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
								<input type="reset" name="Reset" id="reset" value="Batal" /-->
							</td>
						</tr>
					</table>
					</div>
					</div>
				</div>
			</div>
			<!--/form-->
			<div class="col-lg-8">
				<button id="add_btn" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_btn" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<!--button id="cha_pbf-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button-->
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/obat_indikator/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 -->
			<br><br>

			<div id="list_data"></div>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$("#pagelistmaster_konten").on("click","a",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#list_master_konten').load(url);
		});
		$("#trcontent button").on("click",function (event){
			event.preventDefault();
			var url = $(this).attr("href");
			//$("#pagin").load(url);
			$('#konten').load(url);
		});
	});
</script>
<div id="pagelistmaster_konten">
<?php //echo $links; ?>
<!--/div-->
	<!--div id="pagin"-->
	<hr>
	<table class="table table-striped">
		<thead>
		<tr>
			<th><span class="glyphicon glyphicon-trash"></span></th>
			<th>No.</th>
			<th>Nama Konten</th>
			<th>Stength</th>
			<th>Stength Unit</th>
			<th>Container</th>
			<th>Container Unit</th>
		</tr>
		</thead>
		<tbody id="trcontent">
			<?php
				$i=1;
				foreach ($result as $rows) {
					echo '<tr style="cursor:pointer;">
					<td><input type="checkbox" name="chk[]" id="cek_del_master_konten" value="'.$rows->id_map.'" class="chk"></td>
					<td>'.$i.'</td>
					<td>'.$rows->nama_konten.'</td>
					<td>'.$rows->strength_vol.'</td>
					<td>'.$rows->satuan_strength.'</td>
					<td>'.$rows->container_vol.'</td>
					<td>'.$rows->satuan_container.'</td></tr>';
					$i++;
				}
			?>
		</tbody>
	</table>
</div>
<style type="text/css">
	.ui-autocomplete {
	  z-index:2147483647;
	}
</style>
<script type="text/javascript">
	/*$(function(){
		$('#demoOne').listnav({
			includeAll: false,
		});
	});*/
	$(function(){
		
		//form search
        $('#form_srch_master_obat').submit(function(e){
            event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                //dataType: 'json',
                success: function (data) {
                    //alert('DATA BERHASIL DISIMPAN');
                    
                    $('#data_list_master').html(data); 
                    $('.master_data').hide();

                }
            })
            
        });
        //end form search

		$('#nama_cui').autocomplete({
				source:'masterdata/master_konten/get_list_cui', 
				minLength:2,
				select:function(event,ui){
					$('#kode_cui').val(ui.item.code_cui);
					return false;
				}
			});

		$("#add_konten_btn").on("click",function (event){
			$("#add_konten_modal").modal('show');
			//alert("pake ajax");
		});

		$("#paging_master").on("click","a",function (event){
            //$("#cssmenu a.active").removeClass('active');
            //$(this).addClass('active');
            event.preventDefault();
            var url = $(this).attr("href");
            $.get(url, function(data) {
                $('#paging_master2').html(data);
            });
            $("#data_list_master").html("");
            
        });

        $("#paging_master2").on("click","a",function(event){
            event.preventDefault();
            var url = $(this).attr("href");
            $("#data_list_master").load(url);
        })

        $("#btn_add_konten2").on("click",function(){
        	var url51='masterdata/master_konten/input_master_konten';
        	var form_data={
        		nama_konten:$("#nama_konten").val(),
				kode_cui:$("#kode_cui").val()
        	}
        	$.ajax({
			type:"POST",
			url:url51,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_konten_modal").modal('toggle');
				
				$("nama_konten").val("");
				$("nama_cui").val("");
				$("kode_cui").val("");
			}
		});
     })
});
</script>
<style>
.master_data{
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
}

.list_master {
    float: left;
    padding: 1px;
}

.a_master:link, .a_master:visited {
    display: block;
    width: 40px;
    font-weight: bold;
    color: #FFFFFF;
    background-color: #428bca;
    text-align: center;
    padding: 4px;
    text-decoration: none;
    text-transform: uppercase;
    border-style: 1px solid;
    border-color: white;
}

.a_master:hover{
    background-color: red;
}
.a_master:active {
    background-color: red;
    display: block;
}
</style>
<div class="panel panel-primary">
	<div class="panel-heading">Konten Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
		<button id="add_konten_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
		<form style="float:right;" id="form_srch_master_obat" name="form_srch_master_obat" action="<?php echo $base_url; ?>index.php/masterdata/konten_obat/search_by" method="post">
            <input type="text" class="" size="40" name="key_master" id="key_master">
            <button type="submit" id="srch_master_obat"><span class="glyphicon glyphicon-search"></span> Cari</button>
        </form>
        <hr>
		<!--div id="demoOne-nav" class="listNav"></div>
		<ul id="demoOne" class="demo"-->
		<ul id="paging_master" class="master_data">
			<?php echo $list_konten; ?>
		</ul>
		<ul id="paging_master2" class="master_data">
        </ul>
        <div id="data_list_master"></div>
	</div>
</div>
<div class="modal fade" id="add_konten_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Tambah Konten Obat</h4>
      </div>
      <div class="modal-body">

<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
	
		<table class="table">
			<tr>
				<td>Nama Konten</td>
				<td>
					<input type="text" name="nama_konten" id="nama_konten" size="30" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td>CUI</td>
				<td>
					<input type="text" name="nama_cui" id="nama_cui" size="30" class="form-control"/>
					<input type="hidden" id="kode_cui" name="kode_cui">
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_add_konten2" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
					<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
					<input type="reset" name="Reset" id="reset" value="Batal" /-->
				</td>
			</tr>
		</table>
		</div>
		</div>
	</div>
</div>

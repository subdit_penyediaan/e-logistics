 <style>
	
</style>
<script>
$(document).ready(function(){
//form search
		$('#form_search').submit(function(e){
			event.preventDefault();
			$.ajax({
				type: $(this).attr('method'),
	            url: $(this).attr('action'),
	            data: $(this).serialize(),
	            //dataType: 'json',
	            success: function (data) {
	                $("#list_fornas").html(data);
	            }
			})
			
		});
		//end form search
	//========== change button
	/*
	$('#cha_fornas-btn').on("click",function(e){
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })
        $("#add_fornas_modal").modal('show');
        var url="masterdata/obat_fornas/get_id/"+$('#blabla').val();
        .ajax
        	success:

	})
	*/
	//=========== del button

	$("#del_fornas_btn").on("click",function(e){
		//alert("debug");
		id_array= new Array();
        i=0;
        $("input.chk:checked").each(function(){
            id_array[i] = $(this).val();
            i++;

        })

        if(id_array!=0){
	        $.ajax({
	        	url: "masterdata/obat_fornas/delete_list",
	        	data: "kode="+id_array,
	        	type: "POST",
	        	success: function(){
	        		alert("data berhasil dihapus");
	        		var url_hasil="masterdata/obat_fornas/get_data_fornas"
					$("#list_fornas").load(url_hasil);
	        	}
	        })
	    }else {alert("pilih data dulu")}
	})
	//=========== end del

	//=========== show add form

	$("#add_fornas_btn").on("click",function (event){
			$("#add_fornas_modal").modal('show');
				//$("#add_fornas_modal").modal({keyboard:false});
		});
	//================ end show add form

	//var url="masterdata/fornas_obat/get_list_fornas";
	//$('#list_fornas_obat').load(url);

	var url="masterdata/obat_fornas/get_data_fornas";
	$('#list_fornas').load(url);	
	//============== submit add form

	$("#btn_fornas").click(function(){
		var url2="masterdata/obat_fornas/input_data";
		var form_data = {
			nama_fornas:$("#nama_fornas").val(),
			telp_fornas:$("#telp_fornas").val(),
			almt_fornas:$("#almt_fornas").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				//$("#hasil").hide();
				//$("#list_obat_fornas").html("<h1>berhasil</h1>");
				alert("sukses tambah data");
				$("#add_fornas_modal").modal('toggle');
				//var url_hasil="masterdata/fornas_obat/get_list_fornas"
				var url_hasil="masterdata/obat_fornas/get_data_fornas"
				$("#list_fornas").load(url_hasil);//+"#list_sedian_obat");
			}
		});
	})

	//============== end submit add form
	
});

</script>
<div class="panel panel-primary" id="halaman_fornas">
	<div class="panel-heading">Daftar Formularium Obat Nasional</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="modal fade" id="add_fornas_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-dialog">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h4 class="modal-title" id="myModalLabel">Tambah fornas</h4>
			      </div>
			      <div class="modal-body">

			<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/stok_obat/process_form'); ?>"-->
				
					<table class="table">
						<tr>
							<td>Kode Formularium</td>
							<td>
								<input type="text" name="kode_fornas" id="kode_fornas" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Nama Formularium</td>
							<td>
								<input type="text" name="nama_fornas" id="nama_fornas" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Aktif</td>
							<td>
								<input type="checkbox" name="akt_fornas" id="akt_fornas"/> Yes
							</td>
						</tr>
						<tr>
							<td>Parent</td>
							<td>
								<input type="text" name="parent_fornas" id="parent_fornas" size="30" class="form-control"/>
							</td>
						</tr>
						<tr>
							<td>Catatan</td>
							<td>
								<textarea name="cat_fornas" id="cat_fornas" class="form-control"></textarea>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<div class="pagingContainer">
									<button type="submit" name="Simpan" id="btn_fornas" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
									<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
								</div>
								<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
								<input type="reset" name="Reset" id="reset" value="Batal" /-->
							</td>
						</tr>
					</table>
					</div>
					</div>
				</div>
			</div>
			<!--/form-->
			<div class="col-lg-8">
				<button id="add_fornas_btn"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
				<button id="del_fornas_btn"><span class="glyphicon glyphicon-remove"></span> Hapus</button> 
				<button id="cha_fornas-btn"><span class="glyphicon glyphicon-pencil"></span> Ubah</button>
			</div>
			<div class="col-lg-4">
			<form action="<?php echo $base_url; ?>index.php/masterdata/obat_fornas/search_data" method="post" id="form_search">
				<div class="input-group" style="float:right;">
			      <input type="text" class="form-control" name="key">
			      <span class="input-group-btn">
			        <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span> Cari</button>
			      </span>
			    </div><!-- /input-group -->
			</form>
			</div><!-- /col6 -->
			<br><br>

			<div id="list_fornas"></div>
	</div>
</div>
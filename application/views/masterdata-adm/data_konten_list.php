<script type="text/javascript">
	$(function(){
		$('#demoOneX').listnav({
			includeAll: false
			//prefixes: ['sus','d']
		});
	});
	$(function(){
		$('#form_update').submit(function(e){
			event.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                //dataType: 'json',
                success: function (data) {
                    alert('DATA BERHASIL DISIMPAN');
                    $('#upt_konten_modal').modal('hide');
					$('body').removeClass('modal-open');
					$('.modal-backdrop').remove();
                    $('#konten').load('masterdata/konten_obat');                    
                }
            })
		})
		$('#upt_nama_cui').autocomplete({
				source:'masterdata/master_konten/get_list_cui', 
				minLength:2,
				select:function(event,ui){
					$('#upt_kode_cui').val(ui.item.code_cui);
					return false;
				}
			});
		$("#demoOne").on("click","a",function (event){
			event.preventDefault();	
			var url=$(this).attr('href');	
			$.getJSON(url, function(data) {
					$('#upt_id_konten').val(data.id);
					$('#upt_nama_konten').val(data.nama_konten);
					$('#upt_kode_cui').val(data.kode_cui);
					$('#upt_nama_cui').val(data.name_cui);
				}
			);
			
			$('#upt_konten_modal').modal('show');
		});
	});
</script>
<style type="text/css">
	ul{
		list-style-type: none;
		margin: 0px;
		padding: 0px;	
	}
	ul li a{
		color: white;
		margin: 0px;
		background-color: #8EB9DF;
		padding-top: 6px; 
		padding-bottom: 6px;
		padding-left: 20px;
		display: block;
		text-decoration: none;
		border-bottom: 1px solid;
	}
	li a:hover{
		background-color: red;
		display: block;
		color: #ffffff;
		text-decoration: none;
	}
</style>
<!--div class="panel panel-primary">
	<div class="panel-heading">Data Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-!- bag. isi -->
		<!--div id="demoOne-nav" class="listNav"></div-->
		<ul id="demoOne" class="">
			<?php echo $data_obat; ?>
		</ul>		
			
		
	<!--/div>
</div-->
<div class="modal fade" id="upt_konten_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Ubah Konten Obat</h4>
      </div>
      <div class="modal-body">

<form method="POST" name="form_update" style="" id="form_update" action="<?php echo site_url('masterdata/konten_obat/update_data'); ?>">
	
		<table class="table">
			<tr>
				<td>Nama Konten</td>
				<td>
					<input type="text" name="upt[nama_konten]" id="upt_nama_konten" size="30" class="form-control"/>
					<input type="hidden" name="upt[id]" id="upt_id_konten"/>
				</td>
			</tr>
			<tr>
				<td>CUI</td>
				<td>
					<input type="text" name="nama_cui" id="upt_nama_cui" size="30" class="form-control"/>
					<input type="hidden" name="upt[kode_cui]" id="upt_kode_cui"/>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<div class="pagingContainer">
						<button type="submit" name="Simpan" id="btn_add_konten2" class="buttonPaging"><span class="glyphicon glyphicon-floppy-saved"></span> Simpan</button>
						<!--button type="reset" name="Reset" id="reset" class="buttonPaging"><span class="glyphicon glyphicon-remove"></span> Batal</button-->
					</div>
					<!--input type="submit" name="Simpan" id="simpan" value="Simpan" />
					<input type="reset" name="Reset" id="reset" value="Batal" /-->
				</td>
			</tr>
		</table>
	</form>
		</div>
		</div>
	</div>
</div>
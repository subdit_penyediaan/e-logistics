<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit_penerima_pusat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->url=base_url();
		$this->load->model('profil/unit_penerima_pusat_m');
	}

	function index()
	{
		/*$data['base_url']=$this->url;
		$profil=$this->unit_penerima_m->getDataInstitusi();
		if($profil['levelinstitusi']==3){
			$this->load->view('profil/unit_penerima',$data);	
		}elseif($profil['levelinstitusi']==1){
			$this->load->view('profil/unit_penerima_pusat',$data);
		}*/
		
	}

	function penerima_internal()
	{
		$data['base_url']=$this->url;
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima_pusat/penerima_internal';
		$config['total_rows'] = $this->unit_penerima_pusat_m->countAllPropinsi(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->unit_penerima_pusat_m->getDataPropinsi($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		//$data['propinsi']=$this->unit_penerima_pusat_m->getDataPropinsi();
		$this->load->view('profil/penerima_internal_pusat',$data);
	}

	function get_detail($id_prop){
		//$data['base_url']=$this->url;
		$info=$this->db->query("
			select * from ref_propinsi where CPropID=$id_prop
			");

		$data['result']=$info->row_array();
		//$data['links'] = $this->pagination->create_links();
		//$data['links'] = "";
		$this->load->view('profil/info_prop',$data);
	}

	function get_info(){
		$kode=$this->input->post('kode_kab');
		$data = $this->db->where('CKabID',$kode)->get('ref_kabupaten')->row_array();

		//$obat['kode_obat']=$data['kode']
		print json_encode($data);
	}

	function get_data_kab($id_prop){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima_pusat/get_data_kab';
		$config['total_rows'] = $this->unit_penerima_pusat_m->countAllKab($id_prop); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 5;

		$this->pagination->initialize($config);
		$data['result2']=$this->unit_penerima_pusat_m->getDataKab($id_prop);
		//$data['result2']=$this->unit_penerima_pusat_m->getDataKab($config['per_page'],$this->uri->segment(5),$id_prop);
		$data['links'] = $this->pagination->create_links();
		//$data['kecamatan']=$this->unit_penerima_m->getDataKecamatan();
		$this->load->view('profil/info_prop_list',$data);
	}

	function get_data_kecamatan(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima/get_data_kecamatan';
		$config['total_rows'] = $this->unit_penerima_m->countdatakecamatan(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->unit_penerima_m->getAddDataKecamatan($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('profil/ref_kecamatan_list',$data);
		//$this->load->view('list_puskesmas');
	}

	function unitEksternal(){
		$data['base_url']=$this->url;
		$data['result']=$this->unit_penerima_m->getDataUnitEks();
		//$data['links'] = $this->pagination->create_links();
		$data['links'] = "";
		$this->load->view('list_non_puskesmas',$data);
	}

	function input_data(){
		$data = array();
		$data['kode_kec']=$this->input->post('kec_up');
		$data['telp']=$this->input->post('telp');
		$data['desa']=$this->input->post('desa');
		$data['fax']=$this->input->post('fax');
		$data['kode_up']=$this->input->post('kode_up');
		$data['jenis_up']=$this->input->post('jenis_up');
		$data['poned']=$this->input->post('poned');
		$data['alamat_up']=$this->input->post('alamat_up');
		$data['dtpk']=$this->input->post('dtpk');
		$data['email']=$this->input->post('email');
		$data['long']=$this->input->post('long');
		$data['lat']=$this->input->post('lat');
		$data['nama_puskes']=$this->input->post('nama_up');
		$query=$this->unit_penerima_m->input_data_m($data);
	}

	function input_data_kab(){
		$data['kode_kab']=$this->input->post('kode_kab');
		$data['nama_kab']=$this->input->post('nama_kab');
		$data['long']=$this->input->post('longi');
		$data['lat']=$this->input->post('lat');
		$data['kode_prop']=$this->input->post('kode_prop');
		$query=$this->unit_penerima_pusat_m->input_data_kab($data);
	}

	function add_desa($kodekec){
		$data['base_url']=$this->url;	
		$data['kecamatan']=$this->unit_penerima_m->getDataDetailKecamatan($kodekec);
		//$data['satuan']=$this->detail_faktur_model->getSatuan();
		$this->load->view('profil/ref_desa',$data);
	}

	function get_data_kelurahan($key){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima/get_data_kelurahan';
		//$config['total_rows'] = $this->unit_penerima_m->countdatakelurahan($key); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		//$data['result']=$this->unit_penerima_m->getDataKelurahan($config['per_page'],$this->uri->segment(4));
		//$data['links'] = $this->pagination->create_links();
		$data['result']=$this->unit_penerima_m->getDataKelurahan($key);
		$this->load->view('profil/ref_desa_list',$data);
	}

	function input_data_kecamatan(){
		$nama_kec=$this->input->post('nama_kecamatan');
		$this->unit_penerima_m->inputkecamatan($nama_kec);
	}

	function delete_list() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteData($kode);
    }

    function delete_list_kab() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_pusat_m->deleteDataKab($kode);
    }

    function delete_list_pusk() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteDataPusk($kode);
    }

    function input_data_kelurahan(){
		$data['nama_kel']=$this->input->post('nama_kel');
		$data['kode_kec']=$this->input->post('kode_kec');
		$this->unit_penerima_m->inputkelurahan($data);
	}

	function delete_list_kelurahan() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteDataKelurahan($kode);
    }

    function get_detail_puskesmas(){
    	$kode_pusk=$this->input->post('kodepusk');
    	$data = $this->unit_penerima_m->GetInfoPusk($kode_pusk);
        //print_r($data);
        $puskesmas['nama_pusk']=$data['nama_pusk'];
        $puskesmas['alamat_pusk']=$data['alamat_pusk'];
        $puskesmas['kode_pusk']=$data['kode_pusk'];
        $puskesmas['kode_desa']=$data['kode_desa'];
        $puskesmas['kode_kec']=$data['kode_kec'];
        $puskesmas['longi']=$data['longi'];
        $puskesmas['lati']=$data['lati'];
        $puskesmas['jenis_pusk']=$data['jenis_pusk'];
        $puskesmas['telp']=$data['telp'];
        $puskesmas['fax']=$data['fax'];
        $puskesmas['poned']=$data['poned'];
        $puskesmas['dtpk']=$data['dtpk'];
        $puskesmas['email']=$data['email'];
        $puskesmas['nama_kec']=$data['nama_kec'];
        $puskesmas['nama_desa']=$data['nama_desa'];
        echo json_encode($puskesmas);
    }

    function update_data(){
    	$data = array();
		$data['kode_kec']=$this->input->post('kec');
		$data['telp']=$this->input->post('telp');
		$data['desa']=$this->input->post('desa_cha');
		$data['fax']=$this->input->post('fax');
		$data['kode_up']=$this->input->post('hide_kode_pusk');
		$data['jenis_up']=$this->input->post('jenis_pusk');
		$data['poned']=$this->input->post('poned');
		$data['alamat_up']=$this->input->post('alamat_pusk');
		$data['dtpk']=$this->input->post('dtpk');
		$data['email']=$this->input->post('email');
		$data['long']=$this->input->post('longi');
		$data['lat']=$this->input->post('lati');
		$data['nama_puskes']=$this->input->post('nama_pusk');
		$query=$this->unit_penerima_m->update_data_m($data);
    }


    function update_datakab(){
    	$key = $this->input->post('kode_new');
    	$data = $this->input->post('uptdata');
    	$this->db->where('CKabID',$key);
    	$this->db->update('ref_kabupaten',$data);
    }
}
?>
<?php

Class Manajemen_User extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('profil/manajemen_user_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$temp = getProfile();
			if($temp['id_kab']!='') {
				$data['puskesmas']=$this->db->where('KODE_KAB', $temp['id_kab'])->get('ref_puskesmas');
			} else {
				$data['puskesmas']=$this->db->where('KODE_PROP', $temp['id_kab'])->get('ref_puskesmas');
			}
			$this->load->view('profil/manajemen_user',$data);	
		}else{
			redirect('login');
		}
        
    }

    function input_data(){
		$data = array();
		$data['username']=$this->input->post('username');
		$data['pswd']=md5($this->input->post('pswd'));
		$data['long_name']=$this->input->post('long_name');
		$data['email']=$this->input->post('email');
		$data['cp']=$this->input->post('cp');
		$data['jabatan']=$this->input->post('jabatan');
		$data['group_id']=$this->input->post('group');
		$data['kode_puskesmas']=$this->input->post('puskesmas');
		$result = $this->manajemen_user_model->input_data_m($data);
		if ($result) {
        	$output["status"] = "success";
        	$output["message"] = "Data berhasil disimpan";
        } else {
        	$output["status"] = "error";
        	$output["message"] = "Data gagal disimpan";
        }

        echo json_encode($output);
	}

	function get_data_manajemen_user(){
		$data['base_url']=$this->url;		
		$temp = getProfile();
		if($temp['id_kab']!='') {
			$data['puskesmas']=$this->db->where('KODE_KAB', $temp['id_kab'])->get('ref_puskesmas');
		} else {
			$data['puskesmas']=$this->db->where('KODE_PROP', $temp['id_kab'])->get('ref_puskesmas');
		}
		$data['result']=$this->manajemen_user_model->getData();

		$this->load->view('profil/manajemen_user_list', $data);
	}

	function delete_list() {
        $kode = $this->input->post('kode');
        $temp = explode(",", $kode);
        $newArray = array();
        foreach ($temp as $key => $value) {
        	array_push($newArray, $value);
        }
        $result = $this->manajemen_user_model->deleteData($newArray);
        if ($result) {
        	$output["status"] = "success";
        	$output["message"] = "Data berhasil dihapus";
        } else {
        	$output["status"] = "error";
        	$output["message"] = "Data gagal dihapus";
        }

        echo json_encode($output);
    }

    function datatochange($key){
    	$result = $this->manajemen_user_model->getDataToChange($key)->row_array();
		
        echo json_encode($result);
    }

    function update_data(){
    	$data = array();
		$data['id_user']=$this->input->post('id_user');
		$data['long_name']=$this->input->post('long_name');
		$data['email']=$this->input->post('email');
		$data['jabatan']=$this->input->post('jabatan');
		$data['cp']=$this->input->post('cp');
		$data['group_id']=$this->input->post('group');
		$data['kode_puskesmas']=$this->input->post('puskesmas');
		
		$result = $this->manajemen_user_model->update_data_m($data);

		if ($result) {
        	$output["status"] = "success";
        	$output["message"] = "Data berhasil disimpan";
        } else {
        	$output["status"] = "error";
        	$output["message"] = "Data gagal disimpan";
        }

        echo json_encode($output);
    }

    function changePwd ($id_user) {
    	$data['username'] = $this->input->post('username');
    	$data['pswd'] = md5($this->input->post('password'));
    	$data['update_time'] = date('Y-m-d H:i:s');
    	$this->db->where('id_user', $id_user);
    	if ($this->db->update('tb_user', $data)) {
    		$response['status'] = 'success';
    		$response['message'] = 'data berhasil diubah';
    	} else {
    		$response['status'] = 'failed';
    		$response['message'] = 'data tidak berhasil diubah';
    	}

    	echo json_encode($response);
    }

}

?>
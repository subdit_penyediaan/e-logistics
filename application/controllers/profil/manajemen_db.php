<?php

Class Manajemen_db extends CI_Controller {

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('profil/manajemen_db_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('profil/manajemen_db',$data);
		}else{
			redirect('login');
		}
    }

    function backup_db(){
        $this->load->dbutil();
        $prefs = array(
                'tables'      => array('tb_distribusi', 'tb_detail_distribusi',
                    'tb_penerimaan','tb_detail_faktur','tb_detail_lplpo',
                    'tb_detail_pemusnahan','tb_detail_stok_opnam','tb_lplpo',
                    'tb_pemusnahan','tb_perencanaan','tb_stok_obat','tb_stok_opnam',
                    'tb_detail_lplpo_kel','log_sinkron','tb_retur','tb_user','tb_institusi','tb_unitexternal'),  // Array of tables to backup.
                //'tables'      => array('tb_user_profil'),
                'format'      => 'zip',             // gzip, zip, txt

                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES

              );

        //$this->dbutil->backup($prefs);

        //$backup =& $this->dbutil->backup($prefs); // for mysql
        $backup = $this->dbutil->backup($prefs);  // for mysqli

        $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        $save = 'download/'.$db_name;

        $this->load->helper('file');
        write_file($save, $backup);

        $this->load->helper('download');
        force_download($db_name, $backup);

        header('Content-Description: File Transfer');
        header('Content-Type: application/x-zip');
        header('Content-Disposition: attachment; filename='.basename($dbname));

    }

    function truncate_db(){
        $this->db->truncate('tb_distribusi');
        $this->db->truncate('tb_detail_distribusi');
        $this->db->truncate('tb_penerimaan');
        $this->db->truncate('tb_detail_faktur');
        $this->db->truncate('tb_detail_lplpo');
        $this->db->truncate('tb_detail_pemusnahan');
        $this->db->truncate('tb_detail_stok_opnam');
        $this->db->truncate('tb_lplpo');
        $this->db->truncate('tb_detail_lplpo_kel');
        $this->db->truncate('tb_pemusnahan');
        $this->db->truncate('tb_perencanaan');
        $this->db->truncate('tb_stok_obat');
        $this->db->truncate('tb_stok_opnam');
        $this->db->truncate('log_sinkron');
        $this->db->truncate('tb_retur');
    }

    function restore_db(){
        //upload file
        $this->load->helper('file');
        $config['upload_path'] = './bcdatabase/';
        $config['allowed_types'] = "*";
        $file_name = $_FILES['datafile']['name'];
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('datafile'))
        {
            $confirm = array('msg' => $this->upload->display_errors());
            echo json_encode($confirm);
        }
        else
        {
            $upload_data = $this->upload->data();
            $file =  $upload_data['full_path'];
            $direktori='bcdatabase/'.$file_name;

            $ext = pathinfo($file_name, PATHINFO_EXTENSION);

            if($ext == 'sql'){
                $isi_file=file_get_contents($direktori);
                $string_query=rtrim($isi_file,"\n;");
                $array_query=explode(";",$string_query);
                $db_debug = $this->db->db_debug; //save setting
                $this->db->db_debug = FALSE; //disable debugging for queries
                foreach ($array_query as $query) {
                    if(!empty($query)){
                        if(!$this->db->query($query)){
                            delete_files($upload_data['file_path']);
                            $confirm = array('msg' => "illegal characters (;'/) has found! in ".$query);
                            $this->db->db_debug = $db_debug;
                            break;
                        }else{
                            $this->db->db_debug = $db_debug; //return value to default value
                            $confirm = array('msg' => "congrulation, data has been restored");
                        }
                    }
                };
            }elseif($ext == 'json'){
                $isi_file=file_get_contents($direktori);
                $str = preg_replace('/\\\"/',"\"", $isi_file);
                $hasil = str_replace('"{', '{', $str);
                $hasil = str_replace('}"', '}', $hasil);
                $hasil = json_decode($hasil);
                $i = 0;
                foreach ($hasil as $table) {
                    $temp = (array)$hasil;
                    $id = array_keys($temp);
                    $table_name = $id[$i];
                    foreach ($table as $index) {
                        $data = (array)$index;
                        if(!empty($data))
                        {
                            $cek = $this->manajemen_db_model->ifExist($table_name,$data);
                            if($cek->num_rows() > 0){
                                $this->manajemen_db_model->update($table_name,$data);
                            }else{
                                $this->manajemen_db_model->insert($table_name,$data);
                            }
                        }
                    }
                    $i++;
                }
                $confirm = array('msg' => "Berhasil update masterdata");

            }else{
                $confirm = array('msg' => "Tipe file Anda salah");
            }

            delete_files($upload_data['file_path']);
            echo json_encode($confirm);
        }
    }

    function fix_db(){
        $result['repair_tb'] = $this->repair_tb();
        $result['update_code_inn_stock'] = $this->updateInnCodeStock();
        // comment in 24 Mei 2018
        // $this->update_idstok();
        // $this->updatePriceSo();

        echo json_encode($result);
    }

    function update_idstok(){
        $q = $this->db->get('tb_stok_obat');
        foreach ($q->result() as $key) {
            $data = array('id_stok'=>$key->id_stok);
            $this->db->where('kode_obat_res',$key->kode_obat);
            $this->db->where('no_batch',$key->no_batch);
            $this->db->update('tb_detail_distribusi',$data);
        }
    }

    public function updatePriceSo()
    {
        $q = $this->db->get('tb_stok_obat');
        foreach ($q->result() as $key){
            $data = array('harga_so'=>$key->harga);
            $this->db->where('id_so',$key->id_stok);
            $this->db->update('tb_detail_stok_opnam',$data);
        }
    }

    // update 24 Mei 2018
    public function updateInnCodeStock()
    {
        $drugStock = $this->db->get('tb_stok_obat');
        $message = '';
        // foreach ($drugStock->result() as $index) {
        //     $this->db->where('id_obat', $index->kode_obat);
        //     $master = $this->db->get('ref_obat_all')->row_array();
        //     if (isset($master['id_generik'])) {
        //         if ($master['id_generik'] > 0) {
        //             $data['kode_generik'] = $master['id_generik'];
        //             $this->db->where('kode_obat', $index->kode_obat);
        //             $this->db->update('tb_stok_obat', $data);
        //         } elseif ($master['id_generik'] != '') {
        //             $data['kode_generik'] = $master['id_generik'];
        //             $this->db->where('kode_obat', $index->kode_obat);
        //             $this->db->update('tb_stok_obat', $data);
        //         }
        //     } else {
        //         $message .= "data obat inn ".$index->kode_obat. " belum update, ";
        //     }
        // }

        return $message;
    }

    function repair_tb(){
        $data['status'] = 0;
        $cek = $this->db->query("SHOW COLUMNS FROM `tb_detail_stok_opnam` LIKE 'id_stok_opname'");
        if($cek->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_detail_stok_opnam` ADD COLUMN `id_stok_opname` INT NULL AFTER `id`, ADD COLUMN `last_update` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `ket`");
        }
        $cek2 = $this->db->query("SHOW COLUMNS FROM `tb_detail_distribusi` LIKE 'id_stok'");
        if($cek2->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_detail_distribusi` ADD COLUMN `id_stok` INT NULL AFTER `jum_obat_req`, ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NULL AFTER `id_stok`");
        }
        $cek3 = $this->db->query("SHOW COLUMNS FROM `tb_detail_pemusnahan` LIKE 'id_stok'");
        if($cek3->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_detail_pemusnahan` ADD COLUMN `id_stok` INT NULL AFTER `kode_obat`");
        }
        $cek4 = $this->db->query("SHOW COLUMNS FROM `tb_institusi` LIKE 'periode_distribusi'");
        if($cek4->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_institusi` ADD COLUMN `periode_distribusi` SMALLINT(2) NULL AFTER `buffer`;");
        }
        $this->db->query("UPDATE `ref_propinsi` SET `CPropID`='91',`CPropDescr`='PAPUA BARAT' WHERE `CPropID`='91'");
        $cek5 = $this->db->query("SHOW COLUMNS FROM `tb_distribusi` LIKE 'create_time'");
        if($cek5->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_distribusi` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `keperluan`, ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`;");
        }
        $cek6 = $this->db->query("SHOW COLUMNS FROM `tb_detail_distribusi` LIKE 'create_time_real'");
        if($cek6->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_detail_distribusi` ADD COLUMN `create_time_real` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status`, ADD COLUMN `update_time` DATETIME NULL AFTER `create_time_real`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`;");
        }

        //master data
        $cek7 = $this->db->query("SHOW COLUMNS FROM `ref_obat_all` LIKE 'update_time'");
        if($cek7->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obat_all` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status_update`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `nama_obat` `nama_obat` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, DROP PRIMARY KEY,  ADD PRIMARY KEY(`id_obat`, `nama_obat`);");
        }else{
            $this->db->query("ALTER TABLE `ref_obat_all` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek8 = $this->db->query("SHOW COLUMNS FROM `ref_alkesgenerik` LIKE 'update_time'");
        if($cek8->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_alkesgenerik` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `id_sys` `id_sys` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `generik_name` `generik_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_alkesgenerik` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek9 = $this->db->query("SHOW COLUMNS FROM `ref_fornas` LIKE 'update_time'");
        if($cek9->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_fornas` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `nama_obat` `nama_obat` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_fornas` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek10 = $this->db->query("SHOW COLUMNS FROM `ref_kabupaten` LIKE 'update_time'");
        if($cek10->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_kabupaten` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `status_update`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `CPropID` `CPropID` VARCHAR(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
        }

        $cek11 = $this->db->query("SHOW COLUMNS FROM `ref_kecamatan` LIKE 'update_time'");
        if($cek11->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_kecamatan` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `status_update`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`;");
        }

        $cek12 = $this->db->query("SHOW COLUMNS FROM `ref_konten` LIKE 'update_time'");
        if($cek12->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_konten` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `kode_cui`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`, ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NULL AFTER `delete_time`;");
        }else{
            $this->db->query("ALTER TABLE `ref_konten` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek13 = $this->db->query("SHOW COLUMNS FROM `ref_obat_rute` LIKE 'update_time'");
        if($cek13->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obat_rute` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_obat_rute` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek14 = $this->db->query("SHOW COLUMNS FROM `ref_obatgenerik` LIKE 'update_time'");
        if($cek14->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obatgenerik` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_obatgenerik` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek15 = $this->db->query("SHOW COLUMNS FROM `ref_obatindikator` LIKE 'update_time'");
        if($cek15->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obatindikator` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_obatindikator` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek16 = $this->db->query("SHOW COLUMNS FROM `ref_obatprogram` LIKE 'update_time'");
        if($cek16->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obatprogram` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_obatprogram` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek17 = $this->db->query("SHOW COLUMNS FROM `ref_pabrik` LIKE 'update_time'");
        if($cek17->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_pabrik` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_pabrik` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek18 = $this->db->query("SHOW COLUMNS FROM `ref_pbf` LIKE 'update_time'");
        if($cek18->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_pbf` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`, CHANGE `status` `status` SMALLINT(2) DEFAULT '1' NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");
        }else{
            $this->db->query("ALTER TABLE `ref_pbf` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek19 = $this->db->query("SHOW COLUMNS FROM `ref_puskesmas` LIKE 'status'");
        if($cek19->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_puskesmas` ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NULL AFTER `email`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `KD_PUSK` `KD_PUSK` VARCHAR(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `NAMA_PUSKES` `NAMA_PUSKES` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
        }

        $cek20 = $this->db->query("SHOW COLUMNS FROM `ref_villages` LIKE 'update_time'");
        if($cek20->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_villages` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `name`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`;");
        }

        $cek21 = $this->db->query("SHOW COLUMNS FROM `ref_propinsi` LIKE 'update_time'");
        if($cek21->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_propinsi` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `CPropDescr`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`;");
        }

        $cek22 = $this->db->query("SHOW COLUMNS FROM `ref_obat_sediaan` LIKE 'update_time'");
        if($cek22->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obat_sediaan` ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NULL AFTER `jenis_sediaan`, ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY(`id`);");
        }else{
            $this->db->query("ALTER TABLE `ref_obat_sediaan` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek23 = $this->db->query("SHOW COLUMNS FROM `ref_obat_satuan` LIKE 'update_time'");
        if($cek23->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obat_satuan` ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NOT NULL AFTER `satuan_obat_ind`, ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`;");
        }else{
            $this->db->query("ALTER TABLE `ref_obat_satuan` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
        }

        $cek24 = $this->db->query("SHOW COLUMNS FROM `hlp_generik` LIKE 'update_time'");
        if($cek24->num_rows() < 1){
            $this->db->query("ALTER TABLE `hlp_generik` ADD COLUMN `update_time` DATE NULL AFTER `create_time`, ADD COLUMN `delete_time` DATE NULL AFTER `update_time`,CHANGE `id_hlp` `id_hlp` VARCHAR(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `nama_objek` `nama_objek` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
            $this->db->query("ALTER TABLE `map_obat_atc` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status_update`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`;");
            $this->db->query("ALTER TABLE `map_obat_fornas` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `status_update`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`;");
            $this->db->query("ALTER TABLE `map_obat_rute` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `id_rute`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`;");
            $this->db->query("ALTER TABLE `map_obat_zat_id` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NULL AFTER `strength_order`, ADD COLUMN `update_time` DATE NULL AFTER `create_time`;");
        }else{
            $this->db->query("ALTER TABLE `hlp_generik` CHANGE `update_time` `update_time` DATETIME NULL , CHANGE `delete_time` `delete_time` DATETIME NULL ;");
            $this->db->query("ALTER TABLE `map_obat_fornas` CHANGE `update_time` `update_time` DATETIME NULL ;");
            $this->db->query("ALTER TABLE `map_obat_rute` CHANGE `update_time` `update_time` DATETIME NULL ;");
            $this->db->query("ALTER TABLE `map_obat_zat_id` CHANGE `update_time` `update_time` DATETIME NULL ;");
        }

        $cek25 = $this->db->query("SHOW COLUMNS FROM `tb_detail_stok_opnam` LIKE 'updated_by'");
        if($cek25->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_detail_stok_opnam` ADD COLUMN `updated_by` SMALLINT NULL AFTER `last_update`;");
            $this->db->query("ALTER TABLE `tb_detail_stok_opnam` ADD COLUMN `harga_so` DOUBLE NULL AFTER `ket`;");
        }

        /*[3 Okt 2016][12:20:02 PM][2484 ms]*/
        $this->db->query("ALTER TABLE `tb_distribusi` CHANGE `no_dok` `no_dok` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL , CHANGE `id_user` `id_user` INT(11) NOT NULL, CHANGE `create_time` `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL;");

        /*[13 Okt 2016][13:40:02 PM][2484 ms]*/
        $sql = $this->db->query("UPDATE tb_user  SET STATUS = 1 ;");
        if($sql)
        {
            $data['status']=1;
        }
        $cek26 = $this->db->query("SHOW COLUMNS FROM `tb_user` LIKE 'create_time'");
        if($cek26->num_rows() < 1){
            $this->db->query("
            ALTER TABLE `tb_user` ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `jabatan`, ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`, ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`,CHANGE `username` `username` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `pswd` `pswd` VARCHAR(40) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL, CHANGE `status` `status` SMALLINT(10) DEFAULT '1' NOT NULL;
            ");
        }

        $cek27 = $this->db->query("SHOW INDEX FROM tb_detail_distribusi WHERE Column_name = 'id_lplpo'");
        if($cek27->num_rows() < 1){
            $this->db->query("
            ALTER TABLE `tb_detail_distribusi` ADD INDEX `index_detail_distribusi` (`id_lplpo`);
            ");
        }

        $cek28 = $this->db->query("SHOW INDEX FROM ref_puskesmas WHERE Column_name = 'KD_PUSK'");
        if($cek28->num_rows() < 1){
            $this->db->query("
            ALTER TABLE `ref_puskesmas` ADD INDEX `index_ref_puskesmas` (`KD_PUSK`);
            ");
        }

        // $this->db->query("UPDATE ref_obat_all a SET a.object_name = REPLACE(a.object_name, '\r\n','');");
        // $this->db->query("UPDATE ref_obat_all a SET a.deskripsi = REPLACE(a.deskripsi, '\r\n','');");
        // $this->db->query("UPDATE tb_detail_faktur a SET a.nama_obj = REPLACE(a.nama_obj, '\r\n','');");
        // $this->db->query("UPDATE tb_detail_lplpo_kel a SET a.nama_obj = REPLACE(a.nama_obj, '\r\n',''),a.sediaan = REPLACE(a.sediaan, '\r\n','');");
        // $this->db->query("UPDATE tb_stok_obat a SET a.nama_obj = REPLACE(a.nama_obj, '\r\n','');");
        $this->db->query("ALTER TABLE `tb_penerimaan` CHANGE `no_kontrak` `no_kontrak` VARCHAR(150) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;");
        $this->db->query("ALTER TABLE `tb_stok_opnam` CHANGE `no_stok_opnam` `no_stok_opnam` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ;");
        $this->db->query("UPDATE `sys_menu` SET `id`='11',`label_menu`='Stok Opname',`url_menu`='manajemenlogistik/stok_opnam',`modul_id`='2',`status`='1' WHERE `id`='11';");

        //23 november 2016
        $cek29 = $this->db->query("SHOW COLUMNS FROM ref_fornas LIKE 'user_id'");
        if($cek29->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_fornas` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_konten` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obat_all` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obat_gol` ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`, ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obat_rute` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obat_satuan` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obat_sediaan` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obatgenerik` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obatindikator` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_pabrik` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_obatprogram` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
            $this->db->query("ALTER TABLE `ref_pbf` ADD COLUMN `user_id` SMALLINT NULL AFTER `delete_time`;");
        }

        //14 maret 2017
        $cek30 = $this->db->query("SHOW COLUMNS FROM ref_obatindikator LIKE 'dhis2_id'");
        if($cek30->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obatgenerik` ADD COLUMN `status_150item` SMALLINT(2) DEFAULT '0' NULL AFTER `id_obatprogram`;");
            $this->db->query("ALTER TABLE `ref_obatindikator` ADD COLUMN `code` VARCHAR(10) NULL AFTER `id_indikator`;");
            $this->db->query("ALTER TABLE `ref_obatindikator` ADD COLUMN `dhis2_id` VARCHAR(20) NULL AFTER `code`;");
        }

        //15 maret 2017
        $cek31 = $this->db->query("SHOW COLUMNS FROM tb_retur LIKE 'id_stok'");
        if($cek31->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_retur` ADD COLUMN `id_stok` INT(11) NOT NULL AFTER `id`, ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`;");
        }

        $this->db->query("UPDATE `sys_menu` SET `id`='39',`label_menu`='Persediaan Obat Puskesmas INN/FDA',`url_menu`='integrasi/pkm_list',`modul_id`='3',`status`='0' WHERE `id`='39';");

        /*5 Juni 2017 [10:37:18 AM][  78 ms] --> update in 27 September 2017*/
        $cek33 = $this->db->query("SELECT * FROM `ref_obatgenerik` WHERE `id`='511';");
        if ($cek33->num_rows() < 1 ) {
            $this->db->query("UPDATE `ref_obatgenerik` SET `id`='511',`generik_object`='AMOXYCILLIN TABLET 500 MG',`dosage_form`='TABLET',`strength`='500 MG',`nama_zat`='AMOXYCILLIN',`id_obatindikator`='2',`id_obatprogram`='0',`status_150item`='1',`status`='1',`create_time`='2015-04-22 13:25:44',`update_time`='2017-05-05 10:38:06',`delete_time`=NULL,`user_id`=NULL WHERE `id`='676';");
        }

        //14 Juni 2017
        $cek32 = $this->db->query("SHOW COLUMNS FROM ref_puskesmas LIKE 'buffer'");
        if($cek32->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_puskesmas` ADD COLUMN `buffer` DOUBLE NULL AFTER `email`, ADD COLUMN `periode_distribusi` SMALLINT(2) NULL AFTER `buffer`;");
        }

        // 21 Agustus 2017 11.17 AM
        $cek33 = $this->db->query("SHOW COLUMNS FROM tb_user LIKE 'group_id'");
        if($cek33->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_user` ADD COLUMN `group_id` SMALLINT(2) DEFAULT '2' NOT NULL AFTER `pswd`;");
            $this->db->query("UPDATE `tb_user` SET `group_id`='1' WHERE `id_user`='1';");
        }

        //13 Oktober 2017 10.49 AM
        $cek34 = $this->db->query("SHOW COLUMNS FROM sys_menu LIKE 'puskesmas'");
        if ($cek34->num_rows() < 1) {
            $this->db->query("ALTER TABLE `sys_menu` ADD COLUMN `puskesmas` SMALLINT(2) DEFAULT '0' NULL AFTER `status`, ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `puskesmas`, ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `delete_time` DATETIME NULL AFTER `update_time`;");
            $this->db->query("UPDATE `sys_menu` SET `id`='8',`label_menu`='LPLPO INN/FDA',`url_menu`='manajemenlogistik/lplpo_kel',`modul_id`='2',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='8';");
            $this->db->query("UPDATE `sys_menu` SET `id`='26',`label_menu`='Stok Sediaan',`url_menu`='laporan/stok',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='26';");
            $this->db->query("UPDATE `sys_menu` SET `id`='27',`label_menu`='Sediaan Kadaluarsa',`url_menu`='laporan/obat_kadaluarsa',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='27';");
            $this->db->query("UPDATE `sys_menu` SET `id`='32',`label_menu`='Laporan LPLPO INN/FDA',`url_menu`='laporan/lap_lplpo_kel',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='32';");
            $this->db->query("UPDATE `sys_menu` SET `id`='38',`label_menu`='Persediaan Obat Puskesmas INN/FDA',`url_menu`='laporan/ketersediaan_obat_puskesmas_kel',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='38';");
            $this->db->query("UPDATE `sys_menu` SET `id`='44',`label_menu`='Mutasi Obat Per Puskesmas',`url_menu`='laporan/mutasi_obat',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='44';");
            $this->db->query("UPDATE `sys_menu` SET `id`='42',`label_menu`='Ketersediaan Obat Indikator',`url_menu`='laporan/obat_indikator',`modul_id`='5',`status`='1',`puskesmas`='1',`create_time`='2017-10-13 09:12:30',`update_time`=NULL,`delete_time`=NULL WHERE `id`='42';");
        }

        // 16 Oktober 2017 21.20 PM
        $cek35 = $this->db->query("SHOW COLUMNS FROM tb_user LIKE 'kode_puskesmas'");
        if ($cek35->num_rows() < 1) {
            $this->db->query("ALTER TABLE `tb_user` ADD COLUMN `kode_puskesmas` VARCHAR(16) NULL AFTER `jabatan`;");
        }
        /*14 November 2017 [7:30:05 AM][3172 ms]*/
        $cek36 = $this->db->query("SHOW COLUMNS FROM tb_institusi LIKE 'username'");
        if($cek36->num_rows() < 1) {
            $this->db->query("ALTER TABLE `tb_institusi` ADD COLUMN `username` VARCHAR(255) NULL AFTER `periode_distribusi`, ADD COLUMN `password` VARCHAR(255) NULL AFTER `username`, ADD COLUMN `update_time` DATETIME NULL AFTER `password`, ADD COLUMN `user_id` SMALLINT(3) NULL AFTER `update_time`,CHANGE `id` `id` SMALLINT(11) NOT NULL AUTO_INCREMENT;");
        }

        /*4 Desember 2017 [20:14:05 AM][3172 ms]*/
        $this->db->query("DELETE FROM ref_kabupaten WHERE CKabID IN(2005, 7319, 7320, 7321, 7323, 7324);");

        /*23 Januari 2018 [2:05:05 PM][3172 ms]*/
        $cek37 = $this->db->query("SHOW COLUMNS FROM tb_stok_opnam LIKE 'update_time'");
        if($cek37->num_rows() < 1) {
            $this->db->query("ALTER TABLE `tb_stok_opnam` ADD COLUMN `update_time` DATETIME NULL AFTER `status`;");
            $this->db->query("ALTER TABLE `tb_stok_opnam` CHANGE `periode_stok_opnam` `periode_stok_opnam` VARCHAR(7) NULL ;");
        }

        //9 Maret 2018
        $cek38 = $this->db->query("SHOW COLUMNS FROM ref_puskesmas LIKE 'update_time'");
        if($cek38->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_puskesmas` ADD COLUMN `update_time` DATETIME NULL AFTER `create_time`;");
        }

        // 19 April 2018
        $this->db->query("ALTER TABLE `tb_stok_opnam` CHANGE `periode_stok_opnam` `periode_stok_opnam` VARCHAR(10) NULL ;");

        /*28 Mei 2018 [1:54:56 PM][4797 ms]*/
        $cek40 = $this->db->query("SELECT * FROM information_schema.tables WHERE table_name LIKE '%rko%'");
        if($cek40->num_rows() < 1) {
            $this->db->query("CREATE TABLE `tb_rko`( `id` INT NOT NULL AUTO_INCREMENT , `fornas_id` VARCHAR(13) , `name` VARCHAR(255) , `unit` VARCHAR(20) , `stock_two_years_ago` INT , `average_used_two_years_ago` INT , `need` INT , `needed_plan` INT , `procurement_plan` INT , `procurement_two_years_ago` INT , `information` VARCHAR(100) , `create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `update_time` DATETIME , `delete_time` DATETIME , `user_id` INT , PRIMARY KEY (`id`));");
            $this->db->query("ALTER TABLE `tb_rko` ADD COLUMN `year` VARCHAR(4) NULL AFTER `information`;");
        }

        // 15 Agustus 2018
        $cek41 = $this->db->query("SHOW TABLES LIKE '%map_inn%';");
        if ($cek41->num_rows() < 1) {
            $this->db->query("
                create table map_inn_program as select id as id_inn, id_obatprogram
                from ref_obatgenerik
                where id_obatprogram > 0;
            ");
            $this->db->query("
                ALTER TABLE `map_inn_program` ADD COLUMN `id` INT NOT NULL AUTO_INCREMENT FIRST, ADD COLUMN `update_time` DATETIME NULL AFTER `id_obatprogram`, ADD COLUMN `create_time` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER `update_time`,CHANGE `id_obatprogram` `id_obatprogram` SMALLINT(6) DEFAULT '0' NOT NULL, ADD PRIMARY KEY(`id`);
            ");
            $this->db->query("
                ALTER TABLE `map_inn_program` ADD COLUMN `status` SMALLINT(2) DEFAULT '1' NULL AFTER `id_obatprogram`, ADD COLUMN `delete_time` DATETIME NULL AFTER `create_time`, ADD COLUMN `user_id` SMALLINT(5) NULL AFTER `delete_time`;
            ");
        }

        //4 September 2018
        /*[8:46:31 AM][4594 ms]*/
        $cek42 = $this->db->query("SHOW COLUMNS FROM tb_stok_obat LIKE 'update_time'");
        if($cek42->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_stok_obat` ADD COLUMN `update_time` DATETIME NULL AFTER `flag`, ADD COLUMN `update_by` SMALLINT NULL AFTER `update_time`;");
        }

        // 4 Oktober 2018
        $cek43 = $this->db->query("SHOW TABLES LIKE '%tb_stok_akhir_tahun%';");
        if ($cek43->num_rows() < 1) {
            $this->db->query("
                    CREATE TABLE `tb_stok_akhir_tahun` (
                        `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
                        `id_fornas` VarChar( 16 ) NOT NULL,
                        `tahun` Year NOT NULL,
                        `stock` Double NULL,
                        `create_time` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        `update_time` DateTime NULL,
                        `delete_time` DateTime NULL,
                        `status` TinyInt( 1 ) NOT NULL DEFAULT '1',
                        `id_user` Int( 11 ) NULL,
                        PRIMARY KEY ( `id` ) )
                    ENGINE = InnoDB;
                ");
        }

        // 18 Oktober 2018
        $cek44 = $this->db->query("SHOW COLUMNS FROM tb_rko LIKE '%real_two_years%'");
        if($cek44->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_rko` ADD COLUMN `real_two_years_ago` Int( 11 ) NULL;");
            $this->db->query("ALTER TABLE `ref_fornas` ADD COLUMN `rko_id` Int( 11 ) NOT NULL;");
        }

        // 29 Oktober 2018
        $cek45 = $this->db->query("SHOW COLUMNS FROM ref_fornas LIKE '%sediaan%'");
        if($cek45->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_fornas` ADD COLUMN `sediaan` VarChar( 255 ) NOT NULL;");
        }

        // 16 November 2018
        $cek46 = $this->db->query("SHOW COLUMNS FROM tb_rko LIKE '%rko_id%'");
        if($cek46->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_rko` ADD COLUMN `rko_id` Int( 11 ) NULL;");
        }

        // 19 November 2018
        $cek47 = $this->db->query("SHOW COLUMNS FROM tb_institusi LIKE '%url_rko%'");
        if($cek47->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_institusi` ADD COLUMN `username_rko` VarChar( 255 ) NULL;");
            $this->db->query("ALTER TABLE `tb_institusi` ADD COLUMN `password_rko` VarChar( 255 ) NULL;");
            $this->db->query("ALTER TABLE `tb_institusi` ADD COLUMN `url_rko` VarChar( 255 ) NULL;");
        }

        $this->db->query("ALTER TABLE `ref_fornas` MODIFY `catatan` Text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;");

        // 7 Januari 2019
        $cek48 = $this->db->query("SHOW TABLES LIKE '%tb_batch_history%';");
        if ($cek48->num_rows() < 1) {
            $this->db->query("
                CREATE TABLE `tb_batch_history` (
                    `id` Int( 11 ) AUTO_INCREMENT NOT NULL,
                    `batch_old` VarChar( 255 ) NULL,
                    `batch_new` VarChar( 255 ) NULL,
                    `create_time` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    `update_time` DateTime NULL,
                    `update_by` Smallint( 10 ) NOT NULL,
                    `status` TinyInt( 2 ) NOT NULL DEFAULT 1,
                    PRIMARY KEY ( `id` ) )
                ENGINE = InnoDB;
                ");
        }

        // 8 Januari 2019
        $cek49 = $this->db->query("SHOW COLUMNS FROM tb_batch_history LIKE '%id_stok%'");
        if($cek49->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_batch_history` ADD COLUMN `id_stok` Int( 11 ) NOT NULL;");
        }

        $cek50 = $this->db->where('url_menu', 'integrasi/distribusi_list')->get('sys_menu');
        if ($cek50->num_rows() < 1) {
            $konten['label_menu'] = 'Distribusi Obat';
            $konten['url_menu'] = 'integrasi/distribusi_list';
            $konten['modul_id'] = 3;
            $konten['status'] = 1;
            $konten['puskesmas'] = 0;

            $this->db->insert('sys_menu', $konten);
            $id_menu = $this->db->insert_id();

            // bug fix:
            // replace 45 from last insert id menu
            $menu = array(
                array('level' => 3, 'id_modul'=> 3, 'id_menu' => $id_menu),
                array('level' => 2, 'id_modul'=> 3, 'id_menu' => $id_menu)
            );

            $this->db->insert_batch('sys_group', $menu);

        } else {
            //
            $temp = $cek50->row_array();
            $id_menu = $temp['id'];
            $this->db->where('id_menu', $id_menu);
            $this->db->where('id_modul', 3);
            $cek_menu = $this->db->get('sys_group');
            if ($cek_menu->num_rows() < 1) {
                $menu = array(
                    array('level' => 3, 'id_modul'=> 3, 'id_menu' => $id_menu),
                    array('level' => 2, 'id_modul'=> 3, 'id_menu' => $id_menu)
                );

                $this->db->insert_batch('sys_group', $menu);
            }
        }

        // 8 Mei 2019
        $cek51 = $this->db->query("SHOW COLUMNS FROM ref_fornas LIKE '%rko_id%'");
        if($cek51->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_fornas` ADD COLUMN `rko_id` Int( 11 ) NOT NULL;");
        }

        $cek52 = $this->db->query("SHOW COLUMNS FROM tb_rko LIKE '%average_used%'");
        if($cek52->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_rko` ADD COLUMN `average_used_two_years_ago` Int( 11 ) NOT NULL;");
        }

        // 6 Juni 2019
        // ALTER TABLE `tb_rko` ADD COLUMN `price` Double( 2, 0 ) NULL;
        $cek53 = $this->db->query("SHOW COLUMNS FROM tb_rko LIKE '%price%'");
        if($cek53->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_rko` ADD COLUMN `price` Int( 11 ) NULL;");
        }

        // 12 Juni 2019
        // -- CREATE FIELD "update_time" ----------------------------------
        // ALTER TABLE `tb_unitexternal` ADD COLUMN `update_time` DateTime NULL;
        // -- -------------------------------------------------------------

        // -- CREATE FIELD "delete_time" ----------------------------------
        // ALTER TABLE `tb_unitexternal` ADD COLUMN `delete_time` DateTime NULL;
        // -- -------------------------------------------------------------
        $cek54 = $this->db->query("SHOW COLUMNS FROM tb_unitexternal LIKE '%update%'");
        if($cek54->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_unitexternal` ADD COLUMN `update_time` DateTime NULL;");
            $this->db->query("ALTER TABLE `tb_unitexternal` ADD COLUMN `delete_time` DateTime NULL;");
        }

        $cek55 = $this->db->query("SHOW COLUMNS FROM ref_puskesmas LIKE '%delete_time%'");
        if($cek55->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_puskesmas` ADD COLUMN `delete_time` DateTime NULL;");
            $this->db->query("ALTER TABLE `ref_puskesmas` ADD COLUMN `kode_baru` VarChar( 16 ) NULL;");
        }

        // 22 Agustus 2019
        $kabupaten['CKabDescr'] = 'KEPULAUAN TANIMBAR';
        $kabupaten['update_time'] = '2019-08-22 12:45:00';
        $this->db->where('CKabID', '8101');
        $this->db->update('ref_kabupaten', $kabupaten);

        $pontianak['CKabDescr'] = 'MEMPAWAH';
        $pontianak['update_time'] = '2019-12-23 13:59:00';
        $this->db->where('CKabID', '6104');
        $this->db->update('ref_kabupaten', $pontianak);

        $yapen['CKabDescr'] = 'YAPEN';
        $yapen['update_time'] = '2019-12-23 13:59:00';
        $this->db->where('CKabID', '9408');
        $this->db->update('ref_kabupaten', $yapen);

        $paser['CKabDescr'] = 'PASER';
        $paser['update_time'] = '2019-12-23 13:59:00';
        $this->db->where('CKabID', '6401');
        $this->db->update('ref_kabupaten', $paser);

        $buton['CKabDescr'] = 'BUTON SELATAN';
        $buton['update_time'] = '2019-12-23 13:59:00';
        $this->db->where('CKabID', '7415');
        $this->db->update('ref_kabupaten', $buton);

        // add menu auditor
        // 31 Agustus 2020
        $cek50 = $this->db->where('level', 4)->get('sys_group');
        if ($cek50->num_rows() < 1) {

            $menu = array(
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 26),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 27),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 28),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 29),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 30),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 32),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 33),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 34),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 38),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 42),
                array('level' => 4, 'id_modul'=> 5, 'id_menu' => 44)
            );

            $this->db->insert_batch('sys_group', $menu);

        }

        // 11 November 2020
        $cek56 = $this->db->query("SHOW COLUMNS FROM tb_lplpo LIKE '%no_dok%'");
        if($cek56->num_rows() < 1){
            $this->db->query("ALTER TABLE `tb_lplpo` ADD COLUMN `no_dok` VarChar( 255 ) NULL;");
            $this->db->query("ALTER TABLE `tb_lplpo` ADD COLUMN `update_time` DateTime NULL;");
        }

        // 12 September 2022
        $cek56 = $this->db->query("SHOW COLUMNS FROM ref_obatindikator LIKE '%tipe%'");
        if($cek56->num_rows() < 1){
            $this->db->query("ALTER TABLE `ref_obatindikator` ADD COLUMN `tipe` VarChar( 30 ) NULL;");
        }

        return $data;
    }

    //git first clone to temp_update
    function firstClone()
    {
        system('git clone https://subdit_penyediaan@bitbucket.org/subdit_penyediaan/e-logistics.git c:\web\www\temp-update-git',$out);
        system('xcopy c:\web\www\temp-update\application c:\web\www\e-logistics\application /e /i /y',$out);

        echo $out;
    }

    function update_software(){
        system('cd c:\web\www\temp-update-git && git pull origin master',$out);
        system('xcopy c:\web\www\temp-update-git\application c:\web\www\e-logistics\application /e /i /y',$out);
        system('xcopy c:\web\www\temp-update-git\img c:\web\www\e-logistics\img /e /i /y',$out);
        system('xcopy c:\web\www\temp-update-git\css c:\web\www\e-logistics\css /e /i /y',$out);
        system('xcopy c:\web\www\temp-update-git\js c:\web\www\e-logistics\js /e /i /y',$out);
        system('xcopy c:\web\www\temp-update-git\.htaccess c:\web\www\e-logistics\ /e /i /y',$out);

        $this->fix_db();
        // echo $out;

        // 1. install git
        // 2. clone with rename folder (first)
        // 3. copy folder
        // 4. git pull (second etc)
        // 5. copy folder
    }
}
?>

<?php

Class Setting_Institusi extends CI_Controller {
	function __construct() {
        parent::__construct();
        if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('profil/setting_institusi_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['prof_ins']=$this->setting_institusi_model->getDataInstitusi();
			$data['kabupaten']=$this->setting_institusi_model->getKabupaten(substr($data['prof_ins']['id_kab'],0,2));
			$data['propinsi']=$this->setting_institusi_model->getPropinsi();
			$this->load->view('profil/setting_institusi',$data);	
		}else{
			redirect('login');
		}
        
    }

    function input_data(){
		$data = array();
		//$data['level']=$this->input->post('level');
		if($this->input->post('level')==1){
			$data['level']=$this->input->post('level');
			$data['levelkab']="0000";
			$data['levelprop']="";
			$this->resetGroup();	
		}elseif($this->input->post('level')==2){
			$data['level']=$this->input->post('level');
			$data['levelkab']="";
			$data['levelprop']=$this->input->post('levelprop');
			$this->resetGroup();
		}elseif($this->input->post('level')==3){
			$data['level']=$this->input->post('level');
			$data['levelkab']=$this->input->post('levelkab');
			$data['levelprop']="";
			$this->getGroupLplpo();
		}
		
		$data['nf']=$this->input->post('nama_fasilitas');
		$data['almt']=$this->input->post('alamat');
		$data['longi']=$this->input->post('longi');
		$data['lat']=$this->input->post('lat');
		$data['notelp']=$this->input->post('notelp');
		$data['pengelola']=$this->input->post('pengelola');
		$data['struktur_org']=$this->input->post('struktur_org');
		$data['bag_pj']=$this->input->post('bag_pj');
		$data['pj']=$this->input->post('pj');
		$data['jab_pj']=$this->input->post('jab_pj');
		$data['cp_pj']=$this->input->post('cp_pj');
		$data['webservice1']=$this->input->post('webservice1');
		$data['username']=$this->input->post('username');
		$data['password']=$this->input->post('password');
		$data['webservice2']=$this->input->post('webservice2');
		$data['username_rko']=$this->input->post('username_rko');
		$data['password_rko']=$this->input->post('password_rko');
		$data['url_rko']=$this->input->post('webservice_rko');
		$query=$this->setting_institusi_model->input_data_m($data);
	}

	function get_location(){
		$id_kab=$this->input->post('id_kab');
		$data = $this->setting_institusi_model->GetLocation($id_kab);
        //print_r($data);
        $location['ex']=$data['ex'];
        $location['ye']=$data['ye'];
        echo json_encode($location);
	}

	function getGroupLplpo(){
		$this->setting_institusi_model->change_group();
	}

	function resetGroup(){
		$this->setting_institusi_model->reset_menu();	
	}

	function get_data_kabupaten($idprop) {
        //$data = $this->settin_institusi_model->GetComboKab($idprop);
        //print_r($data);
        $data = $this->db->query("select * from ref_kabupaten where CPropID=$idprop")->result_array();
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['CKabID'].'">'.$data[$i]['CKabDescr'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Unit_penerima extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->url=base_url();
		$this->load->model('profil/unit_penerima_m');
		$this->load->model('profil/setting_institusi_model');
		$this->profil = $this->setting_institusi_model->getDataInstitusi();
	}

	function index()
	{
		$data['base_url']=$this->url;
		$profil=$this->unit_penerima_m->getDataInstitusi();
		if($profil['levelinstitusi']==3){
			$this->load->view('profil/unit_penerima',$data);
		}elseif($profil['levelinstitusi']==1){
			//$this->load->view('profil/unit_penerima_pusat',$data);
			redirect('profil/unit_penerima_pusat/penerima_internal');
		}elseif($profil['levelinstitusi']==2){
			//$this->load->view('profil/unit_penerima_pusat',$data);
			redirect('profil/unit_penerima_pusat/get_detail/'.$profil['id_prop']);
		}

	}

	function penerima_internal()
	{
		$data['base_url']=$this->url;
		$data['kecamatan']=$this->unit_penerima_m->getDataKecamatan();
		$this->load->view('profil/penerima_internal',$data);
	}

	function add_kecamatan()
	{
		$data['base_url']=$this->url;
		$data['kecamatan']=$this->unit_penerima_m->getDataKecamatan();
		$this->load->view('profil/ref_kecamatan',$data);
	}

	function get_data_desa($idkecamatan) {
        $data = $this->unit_penerima_m->GetComboVillage($idkecamatan);
        //print_r($data);
        $ret = '<option value="">--- PILIH ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['code'].'">'.$data[$i]['name'].'</option>';
        }
        $ret .= '<option value="add">--- TAMBAH ---</option>';
        echo $ret;
    }

	function puskesmas(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima/puskesmas';
		$config['total_rows'] = $this->unit_penerima_m->countAllPuskesmas(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->unit_penerima_m->getDataPuskesmas($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$data['kecamatan']=$this->unit_penerima_m->getDataKecamatan();
		$data['kelurahan']=$this->unit_penerima_m->getDataVillages();
		$this->load->view('profil/list_unit_penerima',$data);
		//$this->load->view('list_puskesmas');
	}

	function get_data_kecamatan(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima/get_data_kecamatan';
		$config['total_rows'] = $this->unit_penerima_m->countdatakecamatan(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->unit_penerima_m->getAddDataKecamatan($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('profil/ref_kecamatan_list',$data);
		//$this->load->view('list_puskesmas');
	}

	function unitEksternal(){
		$data['base_url']=$this->url;
		$data['result']=$this->unit_penerima_m->getDataUnitEks();
		//$data['links'] = $this->pagination->create_links();
		$data['links'] = "";
		$this->load->view('list_non_puskesmas',$data);
	}

	function input_data(){
		$data = array();
		$data['kode_kec']=$this->input->post('kec_up');
		$data['telp']=$this->input->post('telp');
		$data['desa']=$this->input->post('desa');
		$data['fax']=$this->input->post('fax');
		$data['kode_up']=$this->input->post('kode_up');
		$data['jenis_up']=$this->input->post('jenis_up');
		$data['poned']=$this->input->post('poned');
		$data['alamat_up']=$this->input->post('alamat_up');
		$data['dtpk']=$this->input->post('dtpk');
		$data['email']=$this->input->post('email');
		$data['long']=$this->input->post('long');
		$data['lat']=$this->input->post('lat');
		$data['nama_puskes']=$this->input->post('nama_up');
		$data['buffer']=$this->input->post('buffer');
		$data['periode_distribusi']=$this->input->post('periode_distribusi');
		$data['kode_up_lama']=$this->input->post('kode_up_hidden');
		if($data['kode_up_lama']==''){
			$query=$this->unit_penerima_m->input_data_m($data);
		}else{
			$data['kode_upnew']=$data['kode_up'];
			$data['kode_up'] = $data['kode_up_lama'];
			$query=$this->unit_penerima_m->update_data_m($data);
		}
	}

	function add_desa($kodekec){
		$data['base_url']=$this->url;
		$data['kecamatan']=$this->unit_penerima_m->getDataDetailKecamatan($kodekec);
		//$data['satuan']=$this->detail_faktur_model->getSatuan();
		$this->load->view('profil/ref_desa',$data);
	}

	function get_data_kelurahan($key){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/profil/unit_penerima/get_data_kelurahan';
		//$config['total_rows'] = $this->unit_penerima_m->countdatakelurahan($key); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		//$data['result']=$this->unit_penerima_m->getDataKelurahan($config['per_page'],$this->uri->segment(4));
		//$data['links'] = $this->pagination->create_links();
		$data['result']=$this->unit_penerima_m->getDataKelurahan($key);
		$this->load->view('profil/ref_desa_list',$data);
	}

	function input_data_kecamatan(){
		$data['nama_kec']=$this->input->post('nama_kecamatan');
		$data['kode']=$this->input->post('kode');
		$this->unit_penerima_m->inputkecamatan($data);
	}

	function delete_list() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteData($kode);
    }

    function delete_list_pusk() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteDataPusk($kode);
    }

    function input_data_kelurahan(){
		$data['nama_kel']=$this->input->post('nama_kel');
		$data['kode_kec']=$this->input->post('kode_kec');
		$this->unit_penerima_m->inputkelurahan($data);
	}

		function delete_list_kelurahan() {
        $kode=$this->input->post('kode');
        $this->unit_penerima_m->deleteDataKelurahan($kode);
    }

    function get_detail_puskesmas(){
    	$kode_pusk=$this->input->post('kodepusk');
    	$data = $this->unit_penerima_m->GetInfoPusk($kode_pusk);

        echo json_encode($data);
    }

    function update_data(){
	    	$data = array();
				$data['kode_kec']=$this->input->post('kec');
				$data['telp']=$this->input->post('telp');
				$data['desa']=$this->input->post('desa_cha');
				$data['fax']=$this->input->post('fax');
				$data['kode_up']=$this->input->post('hide_kode_pusk');
				$data['kode_upnew']=$this->input->post('new_kode_pusk');
				$data['jenis_up']=$this->input->post('jenis_pusk');
				$data['poned']=$this->input->post('poned');
				$data['alamat_up']=$this->input->post('alamat_pusk');
				$data['dtpk']=$this->input->post('dtpk');
				$data['email']=$this->input->post('email');
				$data['long']=$this->input->post('longi');
				$data['lat']=$this->input->post('lati');
				$data['nama_puskes']=$this->input->post('nama_pusk');
				$data['buffer']=$this->input->post('buffer');
				$data['periode_distribusi']=$this->input->post('periode_distribusi');
				$query=$this->unit_penerima_m->update_data_m($data);
    }

    function sinkron_unit(){
	    	$data = $this->db->get('tb_institusi')->row_array();
	    	$accountBankData = getProfile();
	    	if($data['id_kab']!=""){
			    	$wil = $data['id_kab'];
			    	$hasil_kec = $this->db->where('KDKAB',$wil)->get('ref_kecamatan')->result();
			    	$hasil_pusk = $this->db->where('KODE_KAB',$wil)->get('ref_puskesmas')->result();

			    	$gel = '';
			    	foreach($hasil_kec as $key => $value)
				    {
				          $gel .= $value->KDKEC.'|';
				          $gel .= $value->KDKAB.'|';
				          $gel .= $value->KECDESC."+";
				          //$gel .= $value->sedia. "\n";
				    }
				    $gel2 = '';
				    foreach ($hasil_pusk as $key => $value) {
					    	$gel2 .= $value->KD_PUSK.'|';
					    	$gel2 .= $value->NAMA_PUSKES.'|';
					    	$gel2 .= $value->KODE_DESA.'|';
					    	$gel2 .= $value->KODE_KEC.'|';
				        $gel2 .= $value->KODE_KAB.'|';
				        $gel2 .= $value->KODE_PROP.'|';
				        $gel2 .= $value->ALAMAT_PUSKESMAS."+";
				        //$gel2 .= $value->sedia. "\n";
				    }

				    require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
		            $client = new nusoap_client(prep_url($data['url_bank_data1']) . "/sinkron_up?wsdl");
		            $client->soap_defencoding = 'UTF-8';
		            $params = array(
		                'username'      => base64_encode($accountBankData['username']),
		                'password'      => base64_encode($accountBankData['password']),
		                'wil'               => $wil,
		                'data1' 		    => $gel,
		                'data2'         	=> $gel2
		            );
		            $result = $client->call('Receive_up', $params);
		            if ($client->fault)
		            {
		              $ret['error'] = "failed";
		              $ret['msg'] = "The request contains an invalid SOAP body";
		            }
		            else {
		                $err = $client->getError();
		                if ($err) {
		                    $ret['error'] = "failed";
		                    $ret['msg'] = $err;
		                } else {
		                    $ret['error'] = "success";
		                    $ret['msg'] = $result;
		                }
		            }
				}else{
					//provinsi
				}

				echo json_encode($ret);
    }

    function search_data(){
	    	$queryx = $this->db->query("select id_kab from tb_institusi");
				if ($queryx->num_rows() > 0)
				{
					$row=$queryx->row();
					$kode_kab=$row->id_kab;
				}
	    	$data['base_url']=$this->url;
	    	$key = $this->input->post('key');
	    	//$kode_kab = substr($key, 1,4);
	    	$query = $this->db->query("
	    		select * from ref_puskesmas
	    		where NAMA_PUSKES like '%$key%' and KODE_KAB = $kode_kab
	    		");//or ALAMAT_PUSKESMAS like '%$key%'
	    	$data['result']=$query->result();
	    	$data['links']='';
	    	$this->load->view('profil/list_unit_penerima',$data);
    }

    function search_live(){
	    	$key=$this->input->get("term");
	    	$data['base_url']=$this->url;
	    	$query=$this->unit_penerima_m->getResult($key);

				print json_encode($query);
    }

		function get_master_puskesmas()
    {
		    $profile = getProfile();
				$area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];
				$token = $profile['username'].'|'.base64_encode($profile['password']).'|'.$area;


      	require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
        $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
        $client->soap_defencoding = 'UTF-8';
        $params = array(
                'username'      => base64_encode($profile['username']),
                'password'      => base64_encode($profile['password']),
                'tahun' 		    => date('Y'),
                'bulan'             => date('m'),
                'jenis_data'        => 'puskesmas',
                'wil'               => $area,
                'data' 		        => '',
                'logkirim'            => ''
            );

        $result = $client->call('Receive', $params);

        if ($client->fault) {
          	$response['status'] = "failed";
          	$response['msg'] = "The request contains an invalid SOAP body";
        } else {
        		$output = json_decode($result);

						// insert to master
						$this->db->where('KODE_KAB', $area);
						$this->db->delete('ref_puskesmas');

						$data = array();
						foreach ($output as $key => $value) {
								// print_r((array)$value);
								// unset($value['id']);
								$data[$key] = (array)$value;
								unset($data[$key]['id']);
						}
						// print_r($data); die();
						$result =  $this->db->insert_batch('ref_puskesmas', $data);
						if ($result) {
								$response['status'] = "success";
								$response['msg'] = "Berhasil sinkron data";
						} else {
								$response['status'] = "failed";
								$response['msg'] = "Gagal sinkron data";
						}
        }

        echo json_encode($response);
    }

}
?>

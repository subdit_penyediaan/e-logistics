<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          waone.wawanopoanane@gmail.com
 * @link            bankdataelog.kemkes.go.id/api
 */
class Rko extends REST_Controller {

    var $type = 'rko';

    function __construct()
    {
        parent::__construct();
        $this->load->model('manajemenlogistik/rko_model');
    }

    public function auth($data) {
        $password = base64_decode($data['password']);

        $this->db->select('*');        
        $this->db->where('username', $data['username']);
        $this->db->where('pswd', md5($password));
        $result = $this->db->get('tb_user');
        if ($result->num_rows() > 0) {
            // $condition = "id_kab=".$data['area']." OR id_prop=".$data['area'];
            // $this->db->where($condition);
            // $result2 = $this->db->get('tb_institusi');
            $result2 = $this->db->query("select * from tb_institusi where id_kab ='".$data['area']."' OR id_prop='".$data['area']."'");
            if ($result2->num_rows() > 0) {
                return $result->result();
            }
        }        
    }

    public function checkLog($data) {
        if ((!empty($data['periode'])) && (!empty($data['area']))) {
            $this->db->where('year', $data['periode']);

            return $this->db->get('tb_rko')->row_array();
        }
    }

    public function insertLog($data) {
        $periodeLog = $data['periode'].'-00';
        $subdistrictCode = substr($data['area'], 1, 7);
        $content = array(
                'periode'=> $periodeLog,
                'kode_kec'=> $subdistrictCode,
                'kode_pusk'=> $data['area'],
                'create_by'=>$data['user']
                );
        $this->db->insert('tb_lplpo',$content);
        return $this->db->insert_id();
    }

    public function checkTransaction($id_lplpo) {
        $this->db->where('id_lplpo', $id_lplpo);
        return $this->db->get('tb_detail_distribusi');
    }

    // to get data rko http://localhost/e-logistics/api/rko/drugs
    // {
    //     "dataEnvironment":{
    //       "token":{"username":"xxx","password":"yyy","area":"zzz"},
    //       "content": {
    //           "area": "zzz",
    //           "periode": "2017-12"
    //         }
    //     }
    // }

    public function drugs_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                //cek log
                $temp = $this->checkLog($data['content']);
                if(!empty($temp)) {
                    $this->db->select('fornas_id, name, unit, stock_two_years_ago, average_used_two_years_ago, need, needed_plan, procurement_plan, procurement_two_years_ago, information');
                    $rko = $this->db->where('year', $temp['year'])->get('tb_rko')->result_array();                    
                    $content = array(
                                'table' => $rko,
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => strtoupper($this->type)
                            );
                } else {
                    $content = array(
                                'table' => 'no data',
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => strtoupper($this->type)
                            );
                }
                $code = 200;
                $status = true;
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }            
        } else {
            $content = "Maaf, token Anda salah";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }

    public function reports_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {

            if ($data["token"]["area"]==$data['content']['area']) {
                //select table
                $tableName = 'tb_rko';
                //cek log
                $temp = $this->checkLog($data['content']);
                if(empty($temp)) {
                    //create log
                    $user = $this->auth($data["token"]);
                    $data['content']['user'] = $user[0]->id_user;
                    // $id_lplpo = $this->insertLog($data['content']);
                } else {
                    // $id_lplpo = $temp['id'];
                }

                // $if_exist = $this->checkTransaction($id_lplpo);
                $if_exist = 0;
                if ($if_exist->num_rows() > 0) {
                    // consist a transaction
                    $content = "Data can't insert, consist transaction";
                    $code = 200;
                    $status = true;
                } else {
                    // drop lplpo
                    $this->db->where('year', $data['content']['periode']);
                    $this->db->delete($tableName);

                    //insert content
                    // foreach ($data['content']['table'] as $key => $value) {
                    //     $value['year'] = $data['content']['periode'];
                    //     $this->db->insert($tableName, $value);
                    // }

                    $this->db->insert_batch($tableName, $data['content']['table']);

                    $content = "Congrulation data saved successfully";
                    $code = 200;
                    $status = true;
                }    
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }
                                    
        } else {
            $content = "Sorry, wrong token";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }    

    public function report_delete() {
        //delete if not verified yet
    }

    public function template_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                //get template
                $rko = $this->history_lplpo_kel_model->getDataStok();
                $content = array(
                            'table' => $rko,
                            'area' => $data['content']['area'],
                            'periode' => $data['content']['periode'],
                            'type' => strtoupper($this->type)
                        );
                $code = 200;
                $status = true;
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }            
        } else {
            $content = "Maaf, token Anda salah";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);        
    }
}

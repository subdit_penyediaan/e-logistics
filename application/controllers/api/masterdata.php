<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          waone.wawanopoanane@gmail.com
 * @link            bankdataelog.kemkes.go.id/api
 */
class Masterdata extends REST_Controller {

    var $type = 'masterdata';

    function __construct()
    {
        parent::__construct();
    }

    public function auth($data) {
        $password = base64_decode($data['password']);

        $this->db->select('*');        
        $this->db->where('username', $data['username']);
        $this->db->where('pswd', md5($password));
        $result = $this->db->get('tb_user');
        if ($result->num_rows() > 0) {
            $result2 = $this->db->query("select * from tb_institusi where id_kab ='".$data['area']."' OR id_prop='".$data['area']."'");
            if ($result2->num_rows() > 0) {
                return $result->result();
            }
        }        
    }    

    // masterdata inn/fda
    public function inn_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                $this->db->select('id_hlp as code, nama_objek as name, sediaan as unit');
                $table = $this->db->get('hlp_generik');
                if($table->num_rows() > 0) {                    
                    $content = array(
                                'table' => $table->result_array(),
                                'area' => $data['content']['area'],
                                'type' => strtoupper($this->type.'_inn')
                            );
                } else {
                    $content = array(
                                'table' => 'no data',
                                'area' => $data['content']['area'],
                                'type' => strtoupper($this->type.'_inn')
                            );
                }
                $code = 200;
                $status = true;
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }            
        } else {
            $content = "Maaf, token Anda salah";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }    
}

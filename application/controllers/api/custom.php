<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          waone.wawanopoanane@gmail.com
 * @link            bankdataelog.kemkes.go.id/api
 */
class Custom extends REST_Controller {

    var $type = 'custom';

    function __construct()
    {
        parent::__construct();
    }

    public function auth($data) {
        $password = base64_decode($data['password']);

        $this->db->select('*');        
        $this->db->where('username', $data['username']);
        $this->db->where('pswd', md5($password));
        $result = $this->db->get('tb_user');
        if ($result->num_rows() > 0) {
            $result2 = $this->db->query("select * from tb_institusi where id_kab ='".$data['area']."' OR id_prop='".$data['area']."'");
            if ($result2->num_rows() > 0) {
                return $result->result();
            }
        }        
    }    

    // simbada, sim badan aset daerah GK
    public function simbada_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                $where_terima = $where_keluar = '';
                if ($data['content']['periode']!='') {
                    $where_terima = "WHERE DATE_FORMAT(b.tanggal, '%Y-%m') = '".$data['content']['periode']."'";
                    $where_keluar = "WHERE DATE_FORMAT(a.create_time, '%Y-%m') = '".$data['content']['periode']."'";
                }
                $table = $this->db->query("
                    SELECT a.id AS kode_transaksi, b.tanggal, 'TERIMA' AS jenistransaksi, b.no_faktur AS bukti_no,  c.kode_generik AS kodebarang, a.harga, a.jumlah_kec AS jumlah, b.dana AS sumberdana  
                    FROM tb_detail_faktur a  
                    JOIN tb_penerimaan b ON a.id_faktur = b.id  
                    JOIN tb_stok_obat c ON c.id_stok = a.id
                    $where_terima
                    UNION ALL
                    SELECT a.id AS kode_transaksi, DATE_FORMAT(a.create_time, '%Y-%m-%d') AS tanggal, 'KELUAR' AS jenistransaksi, IFNULL(e.no_dok, 'LPLPO') AS bukti_no,  c.kode_generik AS kodebarang, c.harga, a.pemberian AS jumlah, b.dana AS sumberdana  
                    FROM tb_detail_distribusi a  
                    JOIN tb_detail_faktur d ON a.id_stok = d.id
                    JOIN tb_penerimaan b ON d.id_faktur = b.id  
                    JOIN tb_stok_obat c ON c.id_stok = a.id
                    LEFT JOIN tb_distribusi e ON d.id = a.id_distribusi
                    $where_keluar
                    ");
                if($table->num_rows() > 0) {                    
                    $content = array(
                                'count' => $table->num_rows(),
                                'rows' => $table->result_array(),
                                'area' => $data['content']['area'],
                                'type' => strtoupper($this->type.'_simbada')
                            );
                } else {
                    $content = array(
                                'count' => 0,
                                'rows' => 'no data',
                                'area' => $data['content']['area'],
                                'type' => strtoupper($this->type.'_simbada')
                            );
                }
                $code = 200;
                $status = true;
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }            
        } else {
            $content = "Maaf, token Anda salah";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'status' => $status,
                        'content' => $content,                        
                        );

        $this->response($feedBack);
    }    
}

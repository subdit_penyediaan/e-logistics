<?php
Class Phptoexcel extends CI_Controller {
	function __construct() {
        parent::__construct();
        
        $this->url=base_url();
        $this->load->model('Phptoexcel_Model');
    }
	function index(){
		$this->printOut();
	}
	function printOut(){
		$query=$this->Phptoexcel_Model->getData();
		//print_r($query);
		/* for($i=0;$i<sizeof($query);$i++){
			echo $query[$i]['nama_obat']."<br>";
		} */
		/* foreach($query->result() as $rows){
			echo $rows->nama_obat."<br>";
		} */
		//fungsi konvert
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		for($i=0;$i<sizeof($query);$i++){
			//echo $query[$i]['nama_obat']."<br>";
			$j=$i+1;
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->setCellValue('A'.$j, $query[$i]['nama_obat']);
			//change the font size
			//$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
			//make the font become bold
			//$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
			//merge cell A1 until D1
			//$this->excel->getActiveSheet()->mergeCells('A1:D1');
			//set aligment to center for that merged cell (A1 to D1)
			//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		} 
		$filename='horee.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		//header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');

		//end fungsi konvert
	}
}
?>
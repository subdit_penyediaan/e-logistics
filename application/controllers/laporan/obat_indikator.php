<?php

Class Obat_indikator extends CI_Controller {
    
    //var $title = 'Laporan Ketersediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/obat_indikator_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			//$data['kecamatan']=$this->Lplpo_Model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/obat_indikator',$data);
		}else{
			redirect('login');
		}
    }
    

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');

        $kolom = array();
        $this->db->where('delete_time', null);
        $data['n']= $this->db->count_all('ref_obatindikator'); //jumlah obat indikator
        //inisialisasi nilai
        $result=$this->obat_indikator_model->getRows($set);
        foreach($result as $key => $rows)
        {
            $kodepuskesmas = $rows->kode_pusk;            
            $kolom1[$rows->id_obatindikator][$kodepuskesmas]['sedia']=$rows->sedia;
            $kolom1[$rows->id_obatindikator]['indikator']=$rows->nama_indikator;
        }

        //header
        $puskesmas=$this->obat_indikator_model->getPusk($set);
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $tabel='<table class="table table-striped table-bordered"><tr class="active th_head"><td>No.</td><td>LIST INDIKATOR</td>';
        foreach ($puskesmas as $pusk) {
            $persentasi[$pusk->kode_pusk]=0;
            $tabel.='<td>'.$pusk->NAMA_PUSKES.'</td>';
        }$tabel.='</tr>';
        //konten
        $j=0;
        //print_r($kolom1);exit;
        foreach ($kolom1 as $key => $value) {           
            $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['indikator'].'</td>';
            foreach ($puskesmas as $pusk) 
            {
                //print $key;
                //print_r($value);
                //print_r($value[$key][$pusk->kode_pusk]);exit;
                //exit;
            //foreach ($key as $key2) {
                $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['sedia'])) ? $kolom1[$key][$pusk->kode_pusk]['sedia'] : 0;
                if($tmpsedia > 0){
                    $indikator = '<span class="glyphicon glyphicon-ok" style="color:green"></span>';
                    $persentasi[$pusk->kode_pusk]++;
                    $flag = 1;
                }else{
                    $indikator = '<span class="glyphicon glyphicon-remove" style="color:red"></span>';
                    $flag = 0;
                }

                $tabel.='<td class="th_head">'.$indikator.'<input type="hidden" class="persen_'.$pusk->kode_pusk.'" value="'.$flag.'"></td>';
            }
            $tabel.='</tr>';
            
        }
        $tabel.='<tr><td colspan="2">Presentasi: </td>';
        foreach ($puskesmas as $pusk) {
            //$tabel.='<td class="th_head"><button id="btn_'.$pusk->kode_pusk.'" class="btn btn-success btn-sm btn_persentasi"><span class="glyphicon glyphicon-th"></span> Hitung</button><label id="lb_'.$pusk->kode_pusk.'" class="label_txt"></label></td>';
            $nilai_persentasi = ($persentasi[$pusk->kode_pusk]/$data['n'])*100;
            $tabel.='<td class="th_head"><b>'.$nilai_persentasi.' %</b></td>';
        }$tabel.='</tr>';
        $tabel.='</table>';
        $data['obat_indikator']=$tabel;

        /*$data['links']="";
        $data['result']=$this->obat_indikator_model->show_data($set);
        //$data['awal']=$set['awal'];
        //$data['akhir']=$set['akhir'];
        $month1 = date("m",strtotime($set['awal']));
        $month2 = date("m",strtotime($set['akhir']));
        $data['bulan_pembagi']=$month2-$month1;*/
        //print_r($set);
        $this->load->view('laporan/obat_indikator_list',$data);
    }

    function printOut(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        
         $kolom = array();
        $data['n']= $this->db->count_all('ref_obatindikator'); //jumlah obat indikator
        //inisialisasi nilai
        $result=$this->obat_indikator_model->getRows($set);
        foreach($result as $key => $rows)
        {
            //print_r($result);exit;
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            //$n = $this->db->count_all('ref_obatindikator');
            //if($i<=$n){

            //}
            $kodepuskesmas = $rows->kode_pusk;
            // if($rows->kode_pusk!=''){
            //     $kodepuskesmas = $rows->kode_pusk;
            // }else{
            // //    print "sdf";exit;
            //     $kodepuskesmas = $result[$key-1]->kode_pusk;
            //var_dump($kodepuskesmas);

            //}
            $kolom1[$rows->id_obatindikator][$kodepuskesmas]['sedia']=$rows->sedia;
            //$kolom1[$kodepuskesmas]['persentasi']=0;
            $kolom1[$rows->id_obatindikator]['indikator']=$rows->nama_indikator;
        }

        //header
        $puskesmas=$this->obat_indikator_model->getPusk($set);
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $tabel='<table class="table table-striped table-bordered"><tr class="active th_head"><td>No.</td><td>LIST INDIKATOR</td>';
        foreach ($puskesmas as $pusk) {
            $persentasi[$pusk->kode_pusk]=0;
            $tabel.='<td>'.$pusk->NAMA_PUSKES.'</td>';
        }$tabel.='</tr>';
        //konten
        $j=0;
        //print_r($kolom1);exit;
        foreach ($kolom1 as $key => $value) {           
            $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['indikator'].'</td>';
            foreach ($puskesmas as $pusk) 
            {
                //print $key;
                //print_r($value);
                //print_r($value[$key][$pusk->kode_pusk]);exit;
                //exit;
            //foreach ($key as $key2) {
                $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['sedia'])) ? $kolom1[$key][$pusk->kode_pusk]['sedia'] : 0;
                if($tmpsedia > 0){
                    $indikator = 'Tersedia';
                    $persentasi[$pusk->kode_pusk]++;
                    $flag = 1;
                }else{
                    $indikator = 'Kosong';
                    $flag = 0;
                }

                $tabel.='<td class="th_head">'.$indikator.'<input type="hidden" class="persen_'.$pusk->kode_pusk.'" value="'.$flag.'"></td>';
            }
            $tabel.='</tr>';
            
        }
        $tabel.='<tr><td colspan="2">Presentasi: </td>';
        foreach ($puskesmas as $pusk) {
            //$tabel.='<td class="th_head"><button id="btn_'.$pusk->kode_pusk.'" class="btn btn-success btn-sm btn_persentasi"><span class="glyphicon glyphicon-th"></span> Hitung</button><label id="lb_'.$pusk->kode_pusk.'" class="label_txt"></label></td>';
            $nilai_persentasi = ($persentasi[$pusk->kode_pusk]/$data['n'])*100;
            $tabel.='<td class="th_head"><b>'.$nilai_persentasi.' %</b></td>';
        }$tabel.='</tr>';
        $tabel.='</table>';
        $data['obat_indikator']=$tabel;
        $data['date_start']=$set['awal'];$data['date_end']=$set['akhir'];
        $data['dinas']=$kabupaten;
        $this->load->view('laporan/laporan_obat_indikator_print',$data);
    }

}
?>
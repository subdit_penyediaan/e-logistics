<?php

Class Penerimaan extends CI_Controller {

    var $title = 'Laporan Penerimaan';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/penerimaan_model');
    }

    function index() {
      	if($this->session->userdata('login')==TRUE){
      			$data['user']=$this->session->userdata('username');
      			$data['base_url']=$this->url;
      			//$data['kecamatan']=$this->Lplpo_Model->getDataKecamatan();
            $data['program'] = $this->db->where('status', '1')->get('ref_obatprogram');

      			$this->load->view('laporan/penerimaan',$data);
    		}else{
    			  redirect('login');
    		}
    }

    function get_data_penerimaan(){
        $data['base_url']=$this->url;

        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/penerimaan/get_data_penerimaan';
        $config['total_rows'] = $this->penerimaan_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->penerimaan_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $data['awal']=$data['akhir']=date('d-m-Y');
        $this->load->view('laporan/penerimaan_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function get_data_by($key){
        $data['base_url']=$this->url;

        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/penerimaan/get_data_by/'.$key;
        $config['total_rows'] = $this->penerimaan_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);

        $data['result']=$this->penerimaan_model->getDataBy($config['per_page'],$this->uri->segment(5),$key);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/penerimaan_list',$data);
    }

    function printOut(){
        //$query=$this->penerimaan_model->getDataEksport();
        $set['awal']=$this->session->userdata('awal');
        $set['akhir']=$this->session->userdata('akhir');//dana,kategori
        $set['dana']=$this->session->userdata('dana');
        $set['kategori']=$this->session->userdata('kategori');
        $set['tahun_anggaran']=$this->session->userdata('tahun_anggaran');
        $set['nama']=$this->session->userdata('nama');
        $set['eksport']=1;
        if(empty($set['awal'])){
            echo "klik show terlebih dahulu";
            die();
        }else{
            $query=$this->penerimaan_model->show_data($set);
        }

        //print_r($query);
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PENERIMAAN SEDIAAN');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:N3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );

        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Produsen');
        $this->excel->getActiveSheet()->setCellValue('G5', 'No. Faktur');
        $this->excel->getActiveSheet()->setCellValue('H5', 'No. Kontrak');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Jumlah');
        $this->excel->getActiveSheet()->setCellValue('J5', 'Tanggal Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Tanggal Kadaluarsa');
        $this->excel->getActiveSheet()->setCellValue('L5', 'Sumber Dana');
        $this->excel->getActiveSheet()->setCellValue('M5', 'Harga Satuan');
        $this->excel->getActiveSheet()->setCellValue('N5', 'Nilai Penerimaan');
        //konten tabel
        $j=5;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+6;
            $n=$j-5;
            $nilai_penerimaan = ($query[$i]['jumlah'])*($query[$i]['harga']);
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['nama_obat']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['sediaan']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['pabrik']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['no_faktur']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['no_kontrak']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['jumlah']);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['tanggal_terima']);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]['expired_date']);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]['dana'].' '.$query[$i]['tahun_anggaran']);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $query[$i]['harga']);
            $this->excel->getActiveSheet()->setCellValue('N'.$j, $nilai_penerimaan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('A5:N5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:N'.$j)->applyFromArray($styleArray);
        unset($styleArray);

        $cur_date = date('d-m-Y');
        $tanggalsbbk=$tt.', '.$cur_date;
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('L'.($j+4).':N'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('L'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('L'.($j+5).':'.'N'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('L'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('L'.($j+10).':'.'N'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('L'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $this->session->unset_userdata('awal');$this->session->unset_userdata('akhir');$this->session->unset_userdata('dana');
        $this->session->unset_userdata('kategori');$this->session->unset_userdata('nama');
        $filename='lap_penerimaan.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['dana']=$this->input->post('dana');
        $set['kategori']=$this->input->post('kategori');
        $set['tahun_anggaran']=$this->input->post('tahun_anggaran');
        $set['nama']=$this->input->post('nama');
        $set['id_faktur']=$this->input->post('id_faktur');
        $set['program']=$this->input->post('program');

        $this->session->set_userdata($set);

        $data['links']="";
        $data['result']=$this->penerimaan_model->show_data($set);
        $data['awal']=$set['awal'];
        $data['akhir']=$set['akhir'];

        $this->load->view('laporan/penerimaan_list',$data);
    }

    function getFaktur() {
        $key=$this->input->get("term");
        //var $term;
        $query = $this->penerimaan_model->getDataFaktur($key);
        print json_encode($query);
    }
}
?>

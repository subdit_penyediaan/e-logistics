<?php

Class Ketersediaan_ukp4 extends CI_Controller {
    

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/ketersediaan_ukp4_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			//$data['kecamatan']=$this->Lplpo_Model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/ketersediaan_ukp4',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_ketersediaan_ukp4(){
        $data['base_url']=$this->url;      

        $data['result']=$this->ketersediaan_ukp4_model->getKelUkp4();
        
        $this->load->view('laporan/ketersediaan_obat_list_ukp4',$data);
    }

    function printOut(){
        $query=$this->ketersediaan_ukp4_model->getDataEksport();
        //print_r($query);
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert
        
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A1', 'Nomor');
        $this->excel->getActiveSheet()->setCellValue('B1', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C1', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('D1', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('E1', 'Jumlah');
        $this->excel->getActiveSheet()->setCellValue('F1', 'Tanggal ketersediaan_obat');
        $this->excel->getActiveSheet()->setCellValue('G1', 'Tanggal Kadaluarsa');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Sumber Dana');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+2;
            $n=$j-1;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['nama_obat']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['sediaan']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['jumlah']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['tanggal_terima']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['expired_date']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['dana']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, "-");
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } 
        $filename='lap_ketersediaan_obat.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function search_data_by(){
        $set['tahun']=$this->input->post('tahun');
        $set['periode']=$this->input->post('periode');

        //$data['links']="";
        $cek=$this->ketersediaan_ukp4_model->cek_data($set);
        if($cek){
            $namatabel=$cek['namatable'];
            $data['result']=$this->ketersediaan_ukp4_model->show_exist($namatabel);
        }else{
            $data['result']=$this->ketersediaan_ukp4_model->show_data($set); 
            //echo "create table";
        }
        //$data['result']=$this->ketersediaan_ukp4_model->getKelUkp4();
        //$data['result']=$this->ketersediaan_ukp4_model->show_data($set);
        //print_r($set);
        $this->load->view('laporan/ketersediaan_list_ukp4',$data);
    }

  
    function obat_ukp4(){
        $data['base_url']=$this->url;      

        $data['result']=$this->ketersediaan_ukp4_model->getKelUkp4();
        //$data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_obat_list_ukp4',$data);
        //$this->load->view('list_puskesmas');
    }

    function add_perencanaan(){
        $data['base_url']=$this->url;      
        $data['result']=$this->ketersediaan_ukp4_model->mk_perencanaan();
        $this->load->view('laporan/perencanaan_ukp4',$data);
    }

    function get_data_tahun(){
        $data['base_url']=$this->url;      

        $data['result']=$this->ketersediaan_ukp4_model->getTahun();
        
        $this->load->view('laporan/perencanaan_ukp4_list',$data);
    }

    function input_data(){
        $arr_id_ukp4 = $this->input->post('id_ukp4');
        $arr_jml = $this->input->post('jml_perencanaan');
        $tahun = $this->input->post('thn_perencanaan');
        //print_r($arr_jml);die;
        $continue=true;
        for($k=0;$k<sizeof($arr_id_ukp4);$k++){
            if($arr_jml[$k]==""){
                $continue=false;$n=$k+1;
                $message['confirm']='Jummlah perencanaan obat No.'.$arr_id_ukp4[$k].' belum diisi';
                $message['con']=0;
                echo json_encode($message);
                die;
            }
        }
        //$message['confirm']='Sukses input data';
        //echo json_encode($message);
        //die;
        for($i=0;$i<sizeof($arr_id_ukp4);$i++) {
            //print_r($arr_jml);die;
            if($arr_id_ukp4[$i] && $arr_jml!='') {
                //$xarr_treatment_id[] = $arr_treatment_id[$i];
                $ins = $this->db->query("
                    INSERT INTO `tb_perencanaan` (
                        `tahun`, 
                        `id_ukp4`, 
                        `jumlah`
                        ) 
                    VALUES (?,?,?)
                ",
                array($tahun,
                        $arr_id_ukp4[$i],
                        $arr_jml[$i]
                        )
                );
                //echo $this->db->last_query();
                
            }
        }

        //$this->detail_stok_opnam_model->updateStatus($this->input->post('id_stok_opnam'));
        //return $ins;
        $message['confirm']='Sukses input data';
        $message['con']=1;
        echo json_encode($message);
    }

    function mk_laporan(){

    }

    function cek_perencanaan($tahun){
        $query=$this->db->query("
            select * from tb_perencanaan
            where tahun = $tahun
            ");
        if($query->num_rows()>0){
            echo 1;
        }else echo 0;
    }

    function search_data_byB(){}
}
?>
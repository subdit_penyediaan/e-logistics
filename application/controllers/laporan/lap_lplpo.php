<?php

Class Lap_lplpo extends CI_Controller {
    
    var $title = 'Laporan lap_lplpo';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/lap_lplpo_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->lap_lplpo_model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/lap_lplpo',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_puskesmas($idpuskesmas) {
        $data = $this->lap_lplpo_model->GetComboPuskesmas($idpuskesmas);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        //$ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function get_data_lap_lplpo(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/lap_lplpo/get_data_lap_lplpo';
        $config['total_rows'] = $this->lap_lplpo_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->lap_lplpo_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/lap_lplpo_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function search_data_by(){
        $set['kec']=$this->input->post('kec');
        $set['per']=$this->input->post('per');
        $set['nama_pusk']=$this->input->post('nama_pusk');
        //print_r($set);die;
        $cek=$this->lap_lplpo_model->eksis($set);
        if($cek){
            //print_r($set);die;
            $data['result']=$this->lap_lplpo_model->show_data($set);
            //print_r($data['result']);die;
            $this->session->set_userdata($set);
            $message['confirm']='Success load data';
            $message['filled']=$this->load->view('laporan/lplpo_ex_list',$data,TRUE);
            echo json_encode($message);
        }else{
            $message['confirm']='Data tidak tersedia';
            echo json_encode($message);
        }
    }

    function get_data_by($key){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/lap_lplpo/get_data_by/'.$key;
        $config['total_rows'] = $this->lap_lplpo_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);

        $data['result']=$this->lap_lplpo_model->getDataBy($config['per_page'],$this->uri->segment(5),$key);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/lap_lplpo_list',$data);
    }
    
    function printOut(){
        //$query=$this->lplpo_model->getDataEksport();
        $set['kec']=$this->session->userdata('kec');
        $set['per']=$this->session->userdata('per');
        $set['nama_pusk']=$this->session->userdata('nama_pusk');
        $set['eksport']=1;
        $query=$this->lap_lplpo_model->show_data($set);
        //print_r($query[0]->id);
        $profil=$this->lap_lplpo_model->getProfilPrint($set['nama_pusk']);
        
        //print_r($profil);
        $formattahun=strtotime($set['per']);
        $tahun=date('Y',$formattahun);
        $bulan=date('F',$formattahun);
        //echo $bulan.', '.$tahun;
        //die;
        
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert
        
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');
        
        //JUDUL
        
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(lPLPO)');
        
        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A4', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C4', $profil['nama_pusk']);
        $this->excel->getActiveSheet()->setCellValue('A5', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C5', $profil['nama_kec']);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C6', $profil['nama_kab']);
        $this->excel->getActiveSheet()->setCellValue('A7', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C7', $profil['nama_prop']);
        $this->excel->getActiveSheet()->setCellValue('G4', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H4', $bulan);
        $this->excel->getActiveSheet()->setCellValue('G5', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H5', $tahun);

        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('J9', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('K9', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('L9', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('M9', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('N9', 'Keterangan');
        
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obat);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->sediaan);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->stok_awal);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->terima);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->sedia);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->pakai);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->rusak_ed);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]->stok_akhir);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]->stok_opt);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]->permintaan);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('N'.$j, $query[$i]->ket);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename=$set['nama_pusk'].'-lplpo-'.$set['per'].'.csv'; //save our workbook as this file name
        //header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007'); 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV'); 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
        //session destroy
        //$this->session->sess_destroy();
    }

}
?>
<?php

Class Ketersediaan_obat extends CI_Controller {
    
    var $title = 'Laporan Ketersediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/ketersediaan_obat_model');
        $this->load->model('laporan/stok_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
            $data['program']=$this->db->where('status', '1')->get('ref_obatprogram');

			$this->load->view('laporan/ketersediaan_obat',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_ketersediaan_obat(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/ketersediaan_obat/get_data_ketersediaan_obat';
        $config['total_rows'] = $this->ketersediaan_obat_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 5; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        //default
        $data['bulan_pembagi']=3;
        $data['result']=$this->ketersediaan_obat_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_obat_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function printOut(){
        $set['awal']=$this->session->userdata('awal');
        $set['akhir']=$this->session->userdata('akhir');//dana,kategori
        $set['nama']=$this->session->userdata('nama');
        $set['kategori']=$this->session->userdata('kategori');
        $bulan_pembagi=$this->session->userdata('bulan_pembagi');
        $set['jprogram']=$this->session->userdata('jprogram');
        $set['eksport']=1;
        $query=$this->ketersediaan_obat_model->show_data($set);
        $query2=$this->ketersediaan_obat_model->get_pengeluaran($set);
        //$query=$this->ketersediaan_obat_model->getDataEksport();
        //print_r($query);
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:F1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN KETERSEDIAAN BARANG');

        $this->excel->getActiveSheet()->mergeCells('A2:F2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:F3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten); 
        
        //header tabel
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );

        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor');
        $this->excel->getActiveSheet()->setCellValue('B5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('C5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Stok Akhir');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Penggunaan Rata-rata');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Ketersediaan (bulan)');
        /*
        $this->excel->getActiveSheet()->setCellValue('G1', 'Tanggal Kadaluarsa');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Sumber Dana');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');*/
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+6;
            $n=$j-5;
            $jumlah_penggunaan = (isset($query2[$i]['jumlah_penggunaan'])) ? $query2[$i]['jumlah_penggunaan'] : 0;            
            if($bulan_pembagi>0){
                $rata=$jumlah_penggunaan/$bulan_pembagi;
            }else{
                $rata=0;
            }
            if($rata<1){
                $ketersediaan = 'tak berhingga';
            }else{
                $ketersediaan = round($query[$i]['stok']/$rata);    
            }
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['nama_obj']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['stok']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, floor($rata));
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $ketersediaan);
            /*
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['expired_date']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['dana']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, "-");
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);
            */
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $this->excel->getActiveSheet()->getStyle('A5:F5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:F'.$j)->applyFromArray($styleArray);
        //unset($styleArray);

        $cur_date = date('d-m-Y');
        $tanggalsbbk=$tt.', '.$cur_date;
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('E'.($j+4).':F'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('E'.($j+5).':'.'F'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+10).':'.'F'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $this->session->unset_userdata('awal');$this->session->unset_userdata('akhir');
        //$this->session->unset_userdata('dana');
        $this->session->unset_userdata('kategori');

        $filename='lap_ketersediaan_obat.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['jprogram']=$this->input->post('jprogram');

        $set['kategori']=$this->input->post('kategori');
        $set['nama']=$this->input->post('nama');

        $month1 = new DateTime($set['awal']);
        $month2 = new DateTime($set['akhir']);
        $diff = $month1->diff($month2);

        $set['bulan_pembagi']= $diff->m;

        $this->session->set_userdata($set);

        $data['links']="";
        $data['result']=$this->ketersediaan_obat_model->show_data($set);
        //$data['result_pengeluaran']=$this->ketersediaan_obat_model->get_pengeluaran($set);
        //$data['result']=$this->stok_model->show_data($set);
        $data['bulan_pembagi']= $diff->m;
        $this->load->view('laporan/ketersediaan_obat_list',$data);
    }

    function obat_fornas(){
        $data['base_url']=$this->url;
        
        //pagination
        //$this->load->library('pagination');
        //$config['base_url']= $data['base_url'].'index.php/laporan/ketersediaan_obat/get_data_ketersediaan_obat';
        //$config['total_rows'] = $this->ketersediaan_obat_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        //$config['per_page'] = 5; //banyaknya rows yang ingin ditampilkan
        //$config['uri_segment'] = 4;

        //$this->pagination->initialize($config);

        $data['result']=$this->ketersediaan_obat_model->getKelFornas();
        //$data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_obat_list_fornas',$data);
        //$this->load->view('list_puskesmas');
    }

    function obat_ukp4(){
        $data['base_url']=$this->url;      

        $data['result']=$this->ketersediaan_obat_model->getKelUkp4();
        //$data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_obat_list_ukp4',$data);
        //$this->load->view('list_puskesmas');
    }

    function obat_generik(){
        $data['base_url']=$this->url;      

        $data['result']=$this->ketersediaan_obat_model->get_obat_generik();
        //$data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_list_generik',$data);
        //$this->load->view('list_puskesmas');
    }

    function search_data_generik(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['nama']=$this->input->post('nama');

        $month1 = new DateTime($set['awal']);
        $month2 = new DateTime($set['akhir']);
        $diff = $month1->diff($month2);

        $data['bulan_pembagi']= $diff->m;
            
        $data['result']=$this->ketersediaan_obat_model->get_obat_generik($set);
        $data['result_pengeluaran']=$this->ketersediaan_obat_model->get_pengeluaran_generik($set);
        
        $this->load->view('laporan/ketersediaan_list_generik',$data);
    }

    function search_data_fornas(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['nama']=$this->input->post('nama');

        $month1 = new DateTime($set['awal']);
        $month2 = new DateTime($set['akhir']);
        $diff = $month1->diff($month2);

        $data['bulan_pembagi']= $diff->m;
        $data['result']=$this->ketersediaan_obat_model->getKelFornas($set);
        $data['result_pengeluaran']=$this->ketersediaan_obat_model->getfornasout($set);
        $this->load->view('laporan/ketersediaan_obat_list_fornas',$data);
    }
}
?>
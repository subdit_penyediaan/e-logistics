<?php

Class Obat_Kadaluarsa extends CI_Controller {

    var $title = 'Laporan Obat Kadaluarsa';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/obat_kadaluarsa_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['program'] = $this->db->where('status', '1')->get('ref_obatprogram');

			$this->load->view('laporan/obat_kadaluarsa',$data);
		}else{
			redirect('login');
		}
    }

    function get_data_obat_kadaluarsa(){
        $data['base_url']=$this->url;

        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/obat_kadaluarsa/get_data_obat_kadaluarsa';
        $config['total_rows'] = $this->obat_kadaluarsa_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 5; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->obat_kadaluarsa_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/obat_kadaluarsa_list',$data);
        //$this->load->view('list_puskesmas');
    }
    function printOut(){
        $set['bulan_ed']=$this->session->userdata('bulan_ed');
        $set['tahun_ed']=$this->session->userdata('tahun_ed');//dana,kategori
        $set['dana']=$this->session->userdata('dana');
        $set['nama']=$this->session->userdata('nama');
        $set['tahun_anggaran']=$this->session->userdata('tahun_anggaran');
        $set['program']=$this->session->userdata('program');
        $set['eksport']=1;
        if(empty($set['program'])){
            echo "klik show terlebih dahulu";
            die();
        }else{
            $query=$this->obat_kadaluarsa_model->show_data($set);
        }

        //$query=$this->obat_kadaluarsa_model->getDataEksport();
        //print_r($query);
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINSI '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }

        $this->load->library('excel');
        //activate worksheet number 1
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:K1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN SEDIAAN KADALUARSA');

        $this->excel->getActiveSheet()->mergeCells('A2:K2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:K3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        //fungsi konvert

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );

        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'No.');
        // $this->excel->getActiveSheet()->setCellValue('B5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('B5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('C5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Produsen');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Tanggal Kadaluarsa');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Masa Berlaku (bulan)');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Harga (Rp)');
        $this->excel->getActiveSheet()->setCellValue('J5', 'Total (Rp)');
        $this->excel->getActiveSheet()->setCellValue('K5', 'Sumber Dana');
        //$this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
        //$this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');
        //konten tabel
        $j=5;
        for($i=0;$i<sizeof($query);$i++){
            $j=$i+6;
            $n=$j-5;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            // $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['nama_obj']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['sediaan']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['pabrik']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['stok']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['ed']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['jarak']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['harga']);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, ($query[$i]['harga'] * $query[$i]['stok']));
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]['dana'].' '.$query[$i]['tahun_anggaran']);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('A5:K5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:K'.$j)->applyFromArray($styleArray);
        unset($styleArray);

        $cur_date = date('d-m-Y');
        $tanggalsbbk=$tt.', '.$cur_date;
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('H'.($j+4).':K'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('H'.($j+5).':'.'K'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('H'.($j+10).':'.'K'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $this->session->unset_userdata('bulan_ed');$this->session->unset_userdata('tahun_ed');
        $this->session->unset_userdata('dana');$this->session->unset_userdata('program');
        $filename='laporan_obat_kadaluarsa.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function search_data_by(){
        $set['bulan_ed']=$this->input->post('bulan_ed');
        $set['tahun_ed']=$this->input->post('tahun_ed');
        $set['dana']=$this->input->post('dana');
        $set['tahun_anggaran']=$this->input->post('tahun_anggaran');
        $set['program']=$this->input->post('program');
        $set['nama']=$this->input->post('nama');

        $this->session->set_userdata($set);

        $data['links']="";
        $data['result']=$this->obat_kadaluarsa_model->show_data($set);

        $this->load->view('laporan/obat_kadaluarsa_list',$data);
    }
}
?>

<?php

Class Lap_lplpo_kel extends CI_Controller {
    
    //var $title = 'Laporan lap_lplpo';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/lap_lplpo_kel_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->lap_lplpo_kel_model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/lap_lplpo_kel',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_puskesmas($idpuskesmas) {
        $data = $this->lap_lplpo_kel_model->GetComboPuskesmas($idpuskesmas);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        //$ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function get_data_lap_lplpo(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/lap_lplpo/get_data_lap_lplpo';
        $config['total_rows'] = $this->lap_lplpo_kel_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->lap_lplpo_kel_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/lap_lplpo_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function search_data_by(){
        $set['kec']=$this->input->post('kec');
        $set['per']=$this->input->post('per');
        $set['nama_pusk']=$this->input->post('nama_pusk');
        //print_r($set);die;
        $this->session->set_userdata($set);
        $cek=$this->lap_lplpo_kel_model->eksis($set);
        if($cek){
            //print_r($set);die;
            $data['result']=$this->lap_lplpo_kel_model->show_data($set);
            //print_r($data['result']);die;
            $this->session->set_userdata($set);
            $message['confirm']='Success load data';
            $message['filled']=$this->load->view('laporan/lplpo_ex_list',$data,TRUE);
            echo json_encode($message);
        }else{
            $message['confirm']='Data tidak tersedia';
            echo json_encode($message);
        }
    }

    function get_data_by($key){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/lap_lplpo/get_data_by/'.$key;
        $config['total_rows'] = $this->lap_lplpo_kel_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);

        $data['result']=$this->lap_lplpo_kel_model->getDataBy($config['per_page'],$this->uri->segment(5),$key);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/lap_lplpo_list',$data);
    }
    
    function printOut(){
        $set['kec']=$this->session->userdata('kec');
        $set['per']=$this->session->userdata('per');
        $set['nama_pusk']=$this->session->userdata('nama_pusk');

        $query=$this->lap_lplpo_kel_model->show_data($set);
        //print_r($query[0]->id);
        $profil=$this->lap_lplpo_kel_model->getProfilPrint($set['nama_pusk']);
        
        //print_r($profil);
        $formattahun=strtotime($set['per']);
        $tahun=date('Y',$formattahun);
        $bulan=date('F',$formattahun);
        //echo $bulan.', '.$tahun;
        //die;
        
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert
        
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');
        
        //JUDUL
        
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(LPLPO)');
        
        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C5', $profil['nama_pusk']);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C6', $profil['nama_kec']);
        $this->excel->getActiveSheet()->setCellValue('A7', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C7', $profil['nama_kab']);
        $this->excel->getActiveSheet()->setCellValue('A8', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C8', $profil['nama_prop']);
        $this->excel->getActiveSheet()->setCellValue('G5', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H5', $bulan);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H6', $tahun);

        //HEADER
        //$this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('A11', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('B11', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('C11', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D11', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('E11', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('F11', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('G11', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('H11', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('I11', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('J11', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('K11', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('L11', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('M11', 'Keterangan');
        
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+12;
            //$n=$j-9;
            //set cell A1 content with some text
            //$this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->sediaan);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->stok_awal);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->terima);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->sedia);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->pakai);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->rusak_ed);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->stok_akhir);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]->stok_opt);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]->permintaan);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $query[$i]->ket);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        } 
        $filename='lap_lap_lplpo.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

}
?>
<?php

Class Stok extends CI_Controller {
    
    var $title = 'Laporan stok';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/stok_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
            $data['program']=$this->db->where('status', '1')->get('ref_obatprogram');
            
			$this->load->view('laporan/stok',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_stok(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/stok/get_data_stok';
        //$config['total_rows'] = $this->stok_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->stok_model->getData($config['per_page'],$this->uri->segment(4));
        $data['awal']=$data['akhir']=date('d-m-Y');
        //print_r($data['result']);
        //print_r($data['result2']);
        //die;
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/stok_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function get_data_by($key){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/stok/get_data_by/'.$key;
        $config['total_rows'] = $this->stok_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);

        $data['result']=$this->stok_model->getDataBy($config['per_page'],$this->uri->segment(5),$key);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/stok_list',$data);
    }
    
    function printOut(){
        $set['awal']=$this->session->userdata('awal');
        $set['akhir']=$this->session->userdata('akhir');//dana,kategori
        $set['dana']=$this->session->userdata('dana');
        $set['kategori']=$this->session->userdata('kategori');
        $set['jprogram']=$this->session->userdata('jprogram');
        $set['nama']=$this->session->userdata('nama');
        $set['eksport']=1;
        if(empty($set['kategori'])){
            echo "klik show terlebih dahulu";
            die();
        }else{
            $query=$this->stok_model->show_data($set);
        }
        //$query=$this->stok_model->getDataEksport();
        
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINSI '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //$kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($profil['nama_kab']);
        //fungsi konvert
        
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN SALDO PERSEDIAAN');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:N3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten); 
        
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor')->getStyle('A5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('B5', 'Kode Obat')->getStyle('B5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('C5', 'No. Batch')->getStyle('C5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('D5', 'Nama Obat')->getStyle('D5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('E5', 'Sediaan')->getStyle('E5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('F5', 'Produsen')->getStyle('F5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('G5', 'Expired')->getStyle('G5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('H5', 'Saldo Persediaan')->getStyle('H5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('I5', 'Pengeluaran')->getStyle('I5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('J5', 'Rusak dimusnahkan')->getStyle('J5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('K5', 'Saldo Akhir')->getStyle('K5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('L5', 'Harga Satuan')->getStyle('L5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('M5', 'Nilai Persediaan')->getStyle('M5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('N5', 'Sumber Dana')->getStyle('N5')->applyFromArray($styleArrayHeader);
        //$this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+6;
            $n=$j-5;
            $nilai_penerimaan = $query[$i]['stok']*$query[$i]['harga'];
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['nama_obj']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['sediaan']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['pabrik']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['expired']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['saldo_persediaan']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['pengeluaran']);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['jml_rusak']);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]['stok']);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]['harga']);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $nilai_penerimaan);
            $this->excel->getActiveSheet()->setCellValue('N'.$j, $query[$i]['sumber_dana'].' '.$query[$i]['tahun_anggaran']);
            //$this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('A5:N'.$j)->applyFromArray($styleArray);
        unset($styleArray);
        
        $cur_date = date('d-m-Y');
        $tanggalsbbk=$tt.', '.$cur_date;
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('K'.($j+4).':N'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('K'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('K'.($j+5).':'.'N'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('K'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('K'.($j+10).':'.'N'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('K'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $this->session->unset_userdata('awal');$this->session->unset_userdata('akhir');
        $this->session->unset_userdata('dana');$this->session->unset_userdata('kategori');
        $this->session->unset_userdata('nama');
        $filename='laporan_stok.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['dana']=$this->input->post('dana');
        $set['tahun_anggaran']=$this->input->post('tahun_anggaran');
        $set['kategori']=$this->input->post('kategori');
        $set['nama']=$this->input->post('nama');
        $set['jprogram']=$this->input->post('jprogram');
        $this->session->set_userdata($set);        
        $data['base_url']=$this->url;        
        $data['links']="";
        $data['result']=$this->stok_model->show_data($set);
        $data['awal']=$set['awal'];
        $data['akhir']=$set['akhir'];
        $this->load->view('laporan/stok_list',$data);
    }

     function obat_fornas(){
        $data['base_url']=$this->url;
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');        
        $data['result']=$this->stok_model->getKelFornas($set);
        $this->load->view('laporan/stok_obat_fornas',$data);
    }

    function search_data_generik(){
        //$set['awal']=$this->input->post('awal');
        //$set['akhir']=$this->input->post('akhir');
        
        $kolom = array();
        //inisialisasi nilai
        //$result=$this->stok_model->getRows($set);
        $result=$this->stok_model->getRows();
        foreach($result as $key => $rows)
        {
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            $kolom1[$rows->id_hlp][$rows->dana]['sedia']=$rows->penerimaan;
            $kolom1[$rows->id_hlp][$rows->dana]['out']=$rows->pemberian;
            $kolom1[$rows->id_hlp]['generik']=$rows->nama_objek;
        }

        //ambil saldo awal
        $result_saldo=$this->stok_model->getRowSaldo();
        foreach($result_saldo as $key => $rows)
        {
            //1$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            // $kolom1[$rows->id_hlp][$rows->dana]['sedia']=$rows->penerimaan;
            // $kolom1[$rows->id_hlp][$rows->dana]['out']=$rows->pemberian;
            $kolom1[$rows->id_hlp]['saldo']=$rows->saldo;
        }
        //header
        $sumber=$this->stok_model->getSumberDana();
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $colspan=sizeof($sumber);
        $tabel='<table class="table table-striped table-bordered"><tr><td rowspan="2">No. </td><td rowspan="2" style="text-align:center;">Nama INN/FDA</td><td rowspan="2">Saldo Awal Tahun</td><td colspan="'.$colspan.'" style="text-align:center;">Penerimaan</td>
                <td colspan="'.$colspan.'" style="text-align:center">Pengeluaran</td><td rowspan="2" style="text-align:center">Sisa Stok</td></tr>';        
        foreach ($sumber as $anggaran) {
            $tabel.='<td>'.$anggaran->dana.'</td>';
        }
        foreach ($sumber as $anggaran) {
            $tabel.='<td>'.$anggaran->dana.'</td>';
        }
        //$tabel.='<td>Pengeluaran</td><td>Sisa Stok</td></tr>';
        //konten
        $j=1;

        foreach ($kolom1 as $key => $value) {
            $saldo_awal = (isset($kolom1[$key]['saldo'])) ? $kolom1[$key]['saldo'] : 0;
            $noname = 'Unknow';
            $nama_generik = (isset($kolom1[$key]['generik'])) ? $kolom1[$key]['generik'] : $noname;
            $tabel.='<tr><td>'.$j.'.</td><td>'.$nama_generik.'</td><td>'.$saldo_awal.'</td>';
            $temp_in=0;
            $temp_out=0;
            foreach ($sumber as $anggaran) 
            {
                
                $tmpsedia = (isset($kolom1[$key][$anggaran->dana]['sedia'])) ? $kolom1[$key][$anggaran->dana]['sedia'] : 0;
                // $tmp_pemberian = (isset($kolom1[$key][$anggaran->dana]['out'])) ? $kolom1[$key][$anggaran->dana]['out'] : 0;
                $temp_in=$temp_in+$tmpsedia;
                // $temp_out=$temp_out+$tmp_pemberian;
                $tabel.='<td>'.$tmpsedia.'</td>';
            }
            foreach ($sumber as $anggaran) 
            {
                
                // $tmpsedia = (isset($kolom1[$key][$anggaran->dana]['sedia'])) ? $kolom1[$key][$anggaran->dana]['sedia'] : 0;
                $tmp_pemberian = (isset($kolom1[$key][$anggaran->dana]['out'])) ? $kolom1[$key][$anggaran->dana]['out'] : 0;
                // $temp_in=$temp_in+$tmpsedia;
                $temp_out=$temp_out+$tmp_pemberian;
                $tabel.='<td>'.$tmp_pemberian.'</td>';
            }
            $saldo=$temp_in-$temp_out+$saldo_awal;
            // $tabel.='<td>'.$temp_out.'</td><td>'.$saldo.'</td></tr>';
            $tabel.='<td>'.$saldo.'</td></tr>';
            $j++;
        }
        $tabel.='</table>';
        $data['konten_laporan']=$tabel;

        $this->load->view('laporan/stok_generik',$data);
    }

    function printOutInn(){
        //$set['awal']=$this->input->post('awal');
        //$set['akhir']=$this->input->post('akhir');
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $wilayah='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $wilayah='DINAS KESEHATAN PROVINSI '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $wilayah='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        
        $kolom = array();
        //inisialisasi nilai
        //$result=$this->stok_model->getRows($set);
        $result=$this->stok_model->getRows();
        foreach($result as $key => $rows)
        {
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            $kolom1[$rows->id_hlp][$rows->dana]['sedia']=$rows->penerimaan;
            $kolom1[$rows->id_hlp][$rows->dana]['out']=$rows->pemberian;
            $kolom1[$rows->id_hlp]['generik']=$rows->nama_objek;
        }

        //ambil saldo awal
        $result_saldo=$this->stok_model->getRowSaldo();
        foreach($result_saldo as $key => $rows)
        {
            //1$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            // $kolom1[$rows->id_hlp][$rows->dana]['sedia']=$rows->penerimaan;
            // $kolom1[$rows->id_hlp][$rows->dana]['out']=$rows->pemberian;
            $kolom1[$rows->id_hlp]['saldo']=$rows->saldo;
        }
        //header
        $sumber=$this->stok_model->getSumberDana();
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $colspan=sizeof($sumber);
        $tabel='<table class="table table-striped table-bordered"><tr><td rowspan="2">No. </td><td rowspan="2" style="text-align:center;">Nama INN/FDA</td><td rowspan="2">Saldo Awal Tahun</td><td colspan="'.$colspan.'" style="text-align:center;">Penerimaan</td>
                <td colspan="'.$colspan.'" style="text-align:center">Pengeluaran</td><td rowspan="2" style="text-align:center">Sisa Stok</td></tr>';
        foreach ($sumber as $anggaran) {
            $tabel.='<td>'.$anggaran->dana.'</td>';
        }
        foreach ($sumber as $anggaran) {
            $tabel.='<td>'.$anggaran->dana.'</td>';
        }
        //$tabel.='<td>Pengeluaran</td><td>Sisa Stok</td></tr>';
        //konten
        $j=1;

        foreach ($kolom1 as $key => $value) {
            $saldo_awal = (isset($kolom1[$key]['saldo'])) ? $kolom1[$key]['saldo'] : 0;
            $noname = 'Unknow';
            $nama_generik = (isset($kolom1[$key]['generik'])) ? $kolom1[$key]['generik'] : $noname;
            $tabel.='<tr><td>'.$j.'.</td><td>'.$nama_generik.'</td><td>'.$saldo_awal.'</td>';
            $temp_in=0;
            $temp_out=0;
            foreach ($sumber as $anggaran) 
            {
                
                $tmpsedia = (isset($kolom1[$key][$anggaran->dana]['sedia'])) ? $kolom1[$key][$anggaran->dana]['sedia'] : 0;
                // $tmp_pemberian = (isset($kolom1[$key][$anggaran->dana]['out'])) ? $kolom1[$key][$anggaran->dana]['out'] : 0;
                $temp_in=$temp_in+$tmpsedia;
                // $temp_out=$temp_out+$tmp_pemberian;
                $tabel.='<td>'.$tmpsedia.'</td>';
            }
            foreach ($sumber as $anggaran) 
            {
                
                // $tmpsedia = (isset($kolom1[$key][$anggaran->dana]['sedia'])) ? $kolom1[$key][$anggaran->dana]['sedia'] : 0;
                $tmp_pemberian = (isset($kolom1[$key][$anggaran->dana]['out'])) ? $kolom1[$key][$anggaran->dana]['out'] : 0;
                // $temp_in=$temp_in+$tmpsedia;
                $temp_out=$temp_out+$tmp_pemberian;
                $tabel.='<td>'.$tmp_pemberian.'</td>';
            }
            $saldo=$temp_in-$temp_out+$saldo_awal;
            // $tabel.='<td>'.$temp_out.'</td><td>'.$saldo.'</td></tr>';
            $tabel.='<td>'.$saldo.'</td></tr>';
            $j++;
        }
        $tabel.='</table>';
        $data['wilayah']=$wilayah;
        $data['konten']=$tabel;

        $this->load->view('laporan/stok_generik_print',$data);
    }

}
?>
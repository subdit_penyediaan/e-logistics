<?php

Class Pengeluaran extends CI_Controller {

    var $title = 'Laporan pengeluaran';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/pengeluaran_model');
    }

    function index() {
      	if($this->session->userdata('login')==TRUE){
      			$data['user']=$this->session->userdata('username');
      			$data['base_url']=$this->url;
      			$data['kecamatan']=$this->pengeluaran_model->getDataKecamatan();
            $data['program'] = $this->db->where('status', '1')->get('ref_obatprogram');
            $data['ux'] = $this->db->where('status', 1)->get('tb_unitexternal')->result_array();
            $profil=$this->pengeluaran_model->getDataInstitusi();
            if($profil['levelinstitusi']==3){
                $data['kecamatan']=$this->pengeluaran_model->getDataKecamatan();
                $this->load->view('laporan/pengeluaran',$data);
            }elseif(($profil['levelinstitusi']==1)||($profil['levelinstitusi']==2)){
                $data['level']=$profil['levelinstitusi'];
                $data['propinsi']=$this->pengeluaran_model->getPropinsi();
                $data['kabupaten']=$this->pengeluaran_model->getKabupaten($profil['id_prop']);
                $this->load->view('laporan/pengeluaran_pusat',$data);
            }
    		}else{
    			redirect('login');
    		}
    }

    function get_data_pengeluaran($awal,$akhir){
        $data['base_url']=$this->url;

        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/pengeluaran/get_data_pengeluaran';
        //$config['total_rows'] = $this->pengeluaran_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->pengeluaran_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $data['awal'] = $awal;
        $data['akhir'] = $akhir;
        $this->load->view('laporan/pengeluaran_list',$data);
        //$this->load->view('list_puskesmas');
    }

    function get_data_by($key){
        $data['base_url']=$this->url;

        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/pengeluaran/get_data_by/'.$key;
        $config['total_rows'] = $this->pengeluaran_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);

        $data['result']=$this->pengeluaran_model->getDataBy($config['per_page'],$this->uri->segment(5),$key);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/pengeluaran_list',$data);
    }

    function printOut(){
        $set['awal']=$this->session->userdata('awal');
        $set['akhir']=$this->session->userdata('akhir');//dana,kategori
        $set['dana']=$this->session->userdata('dana');
        $set['nama']=$this->session->userdata('nama');
        $set['kategori']=$this->session->userdata('kategori');
        $set['pusk']=$this->session->userdata('pusk');
        $set['kode_kab']=$this->session->userdata('kode_kab');
        $set['kode_prov']=$this->input->post('kode_prov');
        $set['tahun_anggaran']=$this->session->userdata('tahun_anggaran');
        $set['unit_eksternal']=$this->session->userdata('unit_eksternal');
        $set['eksport']=1;

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }

        if(empty($set['kategori'])){
            echo "klik show terlebih dahulu";
            die();
        }elseif($temp['levelinstitusi']!=3){
            $query=$this->pengeluaran_model->show_data2($set);
        }else{
            $query=$this->pengeluaran_model->show_data($set);
        }
        // print_r($query);die();

        //load our new PHPExcel library
        $this->load->library('excel');
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:J1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PENGELUARAN BARANG');

        $this->excel->getActiveSheet()->mergeCells('A2:J2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:J3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Produsen');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Jumlah Pengeluaran');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Harga (Rp)');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Total (Rp)');
        $this->excel->getActiveSheet()->setCellValue('J5', 'Sumber Dana');
        /*
        $this->excel->getActiveSheet()->setCellValue('G1', 'Tanggal Kadaluarsa');
        $this->excel->getActiveSheet()->setCellValue('H1', 'Sumber Dana');
        $this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');*/
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //konten tabel
        $j=2;
        for($i=0;$i<sizeof($query);$i++){
            // echo $query[$i]['nama_obj']."<br>";
            $j=$i+6;
            $n=$j-5;
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['nama_obj']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['deskripsi']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['pabrik']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['pengeluaran']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['harga']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['nilai']);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['dana'].' '.$query[$i]['tahun_anggaran']);
        }

        $this->excel->getActiveSheet()->getStyle('A5:J5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:J'.$j)->applyFromArray($styleArray);

        $cur_date = date('d-m-Y');
        $tanggalsbbk=$tt.', '.$cur_date;
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('H'.($j+4).':J'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('H'.($j+5).':'.'J'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('H'.($j+10).':'.'J'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $this->session->unset_userdata('awal');$this->session->unset_userdata('akhir');
        $this->session->unset_userdata('pusk');$this->session->unset_userdata('kategori');
        $this->session->unset_userdata('kode_kab');$this->session->unset_userdata('nama');

        $filename='lap_pengeluaran.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function get_data_puskesmas($idpuskesmas) {
        $data = $this->pengeluaran_model->GetComboPuskesmas($idpuskesmas);
        $ret = '<option value="all">--- Semua ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        //$ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function get_detail($id_stok, $awal="", $akhir=""){
        $data['base_url']=$this->url;
        $data['obat']=$this->pengeluaran_model->getInfoObat($id_stok);
        $data['detailkeluar']=$this->pengeluaran_model->getDataDetail($id_stok,$awal,$akhir);
        $data['detailkeluarcito']=$this->pengeluaran_model->getDataDetailCito($id_stok,$awal,$akhir);
        //$data['satuan']=$this->detail_faktur_model->getSatuan();
        $data['date_start'] = $awal;
        $data['date_end'] = $akhir;
        $this->session->unset_userdata('pusk');
        $this->load->view('laporan/detail_pengeluaran_list', $data);
    }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['pusk']=$this->input->post('pusk');
        $set['kategori']=$this->input->post('kategori');
        $set['nama']=$this->input->post('nama');
        $set['dana']=$this->input->post('dana');
        $set['unit_eksternal']=$this->input->post('unit_eksternal');
        $set['tahun_anggaran']=$this->input->post('tahun_anggaran');
        //$this->session->set_userdata($set);
        $set['kode_kab']=$this->input->post('kode_kab');
        $data['base_url']=$this->url;
        $this->session->set_userdata($set);
        $data['result']=$this->pengeluaran_model->show_data($set);
        $data['awal']=$set['awal'];
        $data['akhir']=$set['akhir'];

        $this->load->view('laporan/pengeluaran_list',$data);

    }

    function search_data(){
        $data['base_url']=$this->url;
        $data['links']="";
        $set['kode_kab']=$this->input->post('kode_kab');
        $set['kode_prov']=$this->input->post('kode_prov');
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        $set['kategori']=$this->input->post('kategori');
        $set['nama']=$this->input->post('nama');
        $set['program']=$this->input->post('program');

        $this->session->set_userdata($set);
        $data['result']=$this->pengeluaran_model->show_data2($set);
        $data['awal']=$set['awal'];
        $data['akhir']=$set['akhir'];

        $this->load->view('laporan/pengeluaran_list',$data);
    }

    function get_data_kabupaten($id_prop) {
        $data = $this->pengeluaran_model->GetComboKab($id_prop);
        //print_r($data);
        $ret = '<option value="0">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['CKabID'].'">'.$data[$i]['CKabDescr'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }
}
?>

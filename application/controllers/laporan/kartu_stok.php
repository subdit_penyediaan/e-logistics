<?php

Class Kartu_stok extends CI_Controller {

    //var $title = 'Penerimaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/kartu_stok_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
            $data['result']=$this->kartu_stok_model->getDaftar();
			$this->load->view('laporan/kartu_stok',$data);
		}else{
			redirect('login');
		}

    }


    function search(){
    	$key=$this->input->get("term");
    	//$key=$this->input->post('key_generik');
    	$data['base_url']=$this->url;
    	//$data['result']=$this->kartu_stok_model->getResult($key);
    	$query=$this->kartu_stok_model->getResult($key);
    	print json_encode($query);
    	//echo $tabel;
    	//$this->load->view('laporan/list_nama_generik',$data);
    }

    function get_detail(){
    	$key['nama_generik']=$this->input->post('key_generik');
        $key['sumber_dana']=$this->input->post('sumber_dana');
    	//$set['key']=$key['nama_generik'];
        $set['key']=$key;
    	$this->session->set_userdata($set);
    	$data['base_url']=$this->url;
    	$data['result']=$this->kartu_stok_model->getKartuStok($key);
    	$this->load->view('laporan/kartu_stok_list',$data);
    }

    function get_detail_detail(){
        $key['kode']=$this->input->post('kode_obat');
        $key['sumber_dana']=$this->input->post('sumber_dana_detail');
        $key['batch']=$this->input->post('batch');
        $key['kode_batch']=$this->input->post('kode_batch');
        //$set['key']=$key['nama_generik'];
        $set['key']=$key;
        $this->session->set_userdata($set);
        $data['base_url']=$this->url;
        $data['result']=$this->kartu_stok_model->getKartuStokDetail($key);
        $this->load->view('laporan/kartu_stok_detail_list',$data);
    }

    function get_list_obat(){
        $key=$this->input->get("term");
        //var $term;
        $query = $this->kartu_stok_model->GetListObat($key);
        print json_encode($query);
    }

    function printOut(){
        $key=$this->session->userdata('key');
        //$set['eksport']=1;
        $query=$this->kartu_stok_model->getKartuStokEksport($key);
        //$query=$this->pengeluaran_model->getDataEksport();
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');

        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN KARTU STOK');

        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:I3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Tanggal');
        $this->excel->getActiveSheet()->setCellValue('C5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('D5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Dari/Ke');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Sumber dana');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Pengeluaran');
        $this->excel->getActiveSheet()->setCellValue('J5', 'Saldo');
        /*$this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
        $this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');*/
        //konten tabel
        $saldo=0;$j=5;
        if(sizeof($query)>0){
            for($i=0;$i<sizeof($query);$i++){
                //echo $query[$i]['nama_obat']."<br>";
                $j=$i+6;
                $n=$j-5;
                $saldo=($saldo+$query[$i]['penerimaan'])-$query[$i]['pengeluaran'];
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
                $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['tanggal']);
                $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['kode_obat']);
                $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['no_batch']);
                $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['nama_obj']);
                $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['alur']);
                $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['sumber_dana']);
                $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['penerimaan']);
                $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['pengeluaran']);
                $this->excel->getActiveSheet()->setCellValue('J'.$j, $saldo);
                /*$this->excel->getActiveSheet()->setCellValue('I'.$j, "-");
                $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);*/
                //change the font size
                //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
                //make the font become bold
                //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
                //merge cell A1 until D1
                //$this->excel->getActiveSheet()->mergeCells('A1:D1');
                //set aligment to center for that merged cell (A1 to D1)
                //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }

        $this->excel->getActiveSheet()->getStyle('A5:J5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:J'.$j)->applyFromArray($styleArray);

        $this->session->unset_userdata('key');//$this->session->unset_userdata('akhir');
        //$this->session->unset_userdata('pusk');$this->session->unset_userdata('kategori');

        $filename='Katu_Stok.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function printOut2(){
        $key=$this->session->userdata('key');
        //$set['eksport']=1;
        $query=$this->kartu_stok_model->getKartuStokDetailEksport($key);
        //$query=$this->pengeluaran_model->getDataEksport();
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');

        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN KARTU STOK');

        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:I3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Tanggal');
        $this->excel->getActiveSheet()->setCellValue('C5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('D5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Dari/Ke');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Sumber dana');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Pengeluaran');
        $this->excel->getActiveSheet()->setCellValue('J5', 'Saldo');

        //konten tabel
        $saldo=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+6;
            $n=$j-5;
            $saldo=($saldo+$query[$i]['penerimaan'])-$query[$i]['pengeluaran'];
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['tanggal']);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['kode_obat']);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['no_batch']);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['nama_obj']);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['alur']);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['sumber_dana']);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['penerimaan']);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['pengeluaran']);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $saldo);
        }

        $this->excel->getActiveSheet()->getStyle('A5:J5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:J'.$j)->applyFromArray($styleArray);

        $this->session->unset_userdata('key');//$this->session->unset_userdata('akhir');
        //$this->session->unset_userdata('pusk');$this->session->unset_userdata('kategori');

        $filename='Katu_Stok.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }
}
?>

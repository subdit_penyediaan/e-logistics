<?php

Class Ketersediaan_obat_puskesmas_kel extends CI_Controller {
    
    //var $title = 'Laporan Ketersediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/ketersediaan_obat_puskesmas_kel_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			//$data['kecamatan']=$this->Lplpo_Model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/ketersediaan_obat_puskesmas_kel',$data);	
		}else{
			redirect('login');
		}
    }

    function get_data_ketersediaan_obat(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/laporan/ketersediaan_obat/get_data_ketersediaan_obat';
        $config['total_rows'] = $this->ketersediaan_obat_puskesmas_kel_model->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 5; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        //default
        $data['bulan_pembagi']=3;
        $data['result']=$this->ketersediaan_obat_puskesmas_kel_model->getData($config['per_page'],$this->uri->segment(4));
        $data['links'] = $this->pagination->create_links();
        $this->load->view('laporan/ketersediaan_obat_list',$data);
        //$this->load->view('list_puskesmas');
    }

    // function printOut(){
    //     $query=$this->ketersediaan_obat_puskesmas_kel_model->getDataEksport();
    //     //print_r($query);
    //     /* for($i=0;$i<sizeof($query);$i++){
    //         echo $query[$i]['nama_obat']."<br>";
    //     } */
    //     /* foreach($query->result() as $rows){
    //         echo $rows->nama_obat."<br>";
    //     } */
    //     //fungsi konvert
        
    //     //load our new PHPExcel library
    //     $this->load->library('excel');
    //     //activate worksheet number 1
    //     $this->excel->setActiveSheetIndex(0);
    //     //name the worksheet
    //     $this->excel->getActiveSheet()->setTitle('laporan');
    //     //header tabel
    //     $this->excel->getActiveSheet()->setCellValue('A1', 'Nomor');
    //     $this->excel->getActiveSheet()->setCellValue('B1', 'Kode Obat');
    //     $this->excel->getActiveSheet()->setCellValue('C1', 'Nama Obat');
    //     $this->excel->getActiveSheet()->setCellValue('D1', 'Sediaan');
    //     $this->excel->getActiveSheet()->setCellValue('E1', 'Jumlah');
    //     $this->excel->getActiveSheet()->setCellValue('F1', 'Tanggal ketersediaan_obat');
    //     $this->excel->getActiveSheet()->setCellValue('G1', 'Tanggal Kadaluarsa');
    //     $this->excel->getActiveSheet()->setCellValue('H1', 'Sumber Dana');
    //     $this->excel->getActiveSheet()->setCellValue('I1', 'Total Mutasi');
    //     $this->excel->getActiveSheet()->setCellValue('J1', 'Harga Satuan');
    //     //konten tabel
    //     for($i=0;$i<sizeof($query);$i++){
    //         //echo $query[$i]['nama_obat']."<br>";
    //         $j=$i+2;
    //         $n=$j-1;
    //         //set cell A1 content with some text
    //         $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
    //         $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['kode_obat']);
    //         $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['nama_obat']);
    //         $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['sediaan']);
    //         $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['jumlah']);
    //         $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['tanggal_terima']);
    //         $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['expired_date']);
    //         $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]['dana']);
    //         $this->excel->getActiveSheet()->setCellValue('I'.$j, "-");
    //         $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);
    //         //change the font size
    //         //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
    //         //make the font become bold
    //         //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
    //         //merge cell A1 until D1
    //         //$this->excel->getActiveSheet()->mergeCells('A1:D1');
    //         //set aligment to center for that merged cell (A1 to D1)
    //         //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    //     } 
    //     $filename='lap_ketersediaan_obat.xls'; //save our workbook as this file name
    //     header('Content-Type: application/vnd.ms-excel'); //mime type
    //     header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
    //     header('Cache-Control: max-age=0'); //no cache
                     
    //     //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
    //     //if you want to save it as .XLSX Excel 2007 format
    //     $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
    //     //force user to download the Excel file without writing it to server's HD
    //     $objWriter->save('php://output');

    //     //end fungsi konvert
    // }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');
        //$set['dana']=$this->input->post('dana');
        //$set['kategori']=$this->input->post('kategori');

        $kolom = array();
        //inisialisasi nilai
        $result=$this->ketersediaan_obat_puskesmas_kel_model->getRows($set);
        foreach($result as $key => $rows)
        {
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            $kolom1[$rows->kode_obat][$rows->kode_pusk]['sedia']=$rows->sedia;
            $kolom1[$rows->kode_obat]['generik']=$rows->nama_obj;
        }

        //header
        $puskesmas=$this->ketersediaan_obat_puskesmas_kel_model->getPusk($set);
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $tabel='<table class="table table-striped table-bordered"><tr class="active th_head"><td>No.</td><td>LIST OBAT</td>';
        foreach ($puskesmas as $pusk) {
            $tabel.='<td>'.$pusk->NAMA_PUSKES.'</td>';
        }$tabel.='</tr>';
        //konten
        $j=0;
        //print_r($kolom1);exit;
        foreach ($kolom1 as $key => $value) {           
            $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['generik'].'</td>';
            foreach ($puskesmas as $pusk) 
            {
                //print $key;
                //print_r($value);
                //print_r($value[$key][$pusk->kode_pusk]);exit;
                //exit;
            //foreach ($key as $key2) {
                $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['sedia'])) ? $kolom1[$key][$pusk->kode_pusk]['sedia'] : 0;
                $tabel.='<td>'.$tmpsedia.'</td>';
            }
            $tabel.='</tr>';
            
        }
        $tabel.='</table>';
        $data['obat_puskesmas']=$tabel;

        /*$data['links']="";
        $data['result']=$this->ketersediaan_obat_puskesmas_kel_model->show_data($set);
        //$data['awal']=$set['awal'];
        //$data['akhir']=$set['akhir'];
        $month1 = date("m",strtotime($set['awal']));
        $month2 = date("m",strtotime($set['akhir']));
        $data['bulan_pembagi']=$month2-$month1;*/
        //print_r($set);
        $this->load->view('laporan/ketersediaan_obat_puskesmas_list',$data);
    }

    function printOut(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        
        $kolom = array();
        //inisialisasi nilai
        $result=$this->ketersediaan_obat_puskesmas_kel_model->getRows($set);
        foreach($result as $key => $rows)
        {
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            $kolom1[$rows->kode_obat][$rows->kode_pusk]['sedia']=$rows->sedia;
            $kolom1[$rows->kode_obat]['generik']=$rows->nama_obj;
        }

        //header
        $puskesmas=$this->ketersediaan_obat_puskesmas_kel_model->getPusk($set);
        
        $tabel='<table><tr><th>LIST OBAT</th>';
        foreach ($puskesmas as $pusk) {
            $tabel.='<th>'.$pusk->NAMA_PUSKES.'</th>';
        }$tabel.='</tr>';
        //konten
        $j=0;
        //print_r($kolom1);exit;
        foreach ($kolom1 as $key => $value) {           
            $tabel.='<tr><td>'.$kolom1[$key]['generik'].'</td>';
            foreach ($puskesmas as $pusk) 
            {
                $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['sedia'])) ? $kolom1[$key][$pusk->kode_pusk]['sedia'] : 0;
                $tabel.='<td>'.$tmpsedia.'</td>';
            }
            $tabel.='</tr>';
            $j++;
        }
        $tabel.='</table>';
        $data['obat_puskesmas']=$tabel;
        $data['date_start']=$set['awal'];$data['date_end']=$set['akhir'];
        $data['dinas']=$kabupaten;
        $this->load->view('laporan/laporan_persediaan_pkm_print',$data);
    }

}
?>
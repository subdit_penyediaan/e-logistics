<?php

Class Mutasi_obat extends CI_Controller {
    
    //var $title = 'Laporan Ketersediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('laporan/mutasi_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			//$data['kecamatan']=$this->Lplpo_Model->getDataKecamatan();

			//print_r($data);
			$this->load->view('laporan/mutasi_obat',$data);	
		}else{
			redirect('login');
		}
    }

    function search_data_by(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');        

        $kolom1 = array();
        //inisialisasi nilai
        $result=$this->mutasi_obat_model->getRows($set);
        $tabel='<table class="table table-striped table-bordered"><tr class="active th_head"><td>No.</td><td>LIST OBAT</td>';
        if($result->num_rows() > 0){
            foreach($result->result() as $key => $rows)
            {
                //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
                $kolom1[$rows->kode_obat][$rows->kode_pusk]['keluar']=$rows->keluar;
                $kolom1[$rows->kode_obat]['kode_obat']=$rows->nama_obj;
            }

            //header
            $puskesmas=$this->mutasi_obat_model->getPusk($set);                
            foreach ($puskesmas as $pusk) {
                $tabel.='<td>'.$pusk->faskes.'</td>';
            }$tabel.='</tr>';
            //konten
            $j=0;
            foreach ($kolom1 as $key => $value) {           
                $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['kode_obat'].'</td>';
                foreach ($puskesmas as $pusk) 
                {                
                    $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['keluar'])) ? $kolom1[$key][$pusk->kode_pusk]['keluar'] : 0;
                    $tabel.='<td>'.$tmpsedia.'</td>';
                }
                $tabel.='</tr>';
                
            }
        }else{
            $tabel .='</tr><tr><td colspan="2">Data tidak ditemukan</td></tr>';
        }
        
        $tabel.='</table>';
        $data['obat_mutasi']=$tabel;
        
        $this->load->view('laporan/mutasi_obat_list',$data);
    }

    function printOut(){
        $set['awal']=$this->input->post('awal');
        $set['akhir']=$this->input->post('akhir');

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        
        $kolom1 = array();
        //inisialisasi nilai
        $result=$this->mutasi_obat_model->getRows($set);
        $tabel='<table><tr><th>No.</th><th>LIST OBAT</th>';
        if($result->num_rows() > 0){
            foreach($result->result() as $key => $rows)
            {
                //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
                $kolom1[$rows->kode_obat][$rows->kode_pusk]['keluar']=$rows->keluar;
                $kolom1[$rows->kode_obat]['kode_obat']=$rows->nama_obj;
            }

            //header
            $puskesmas=$this->mutasi_obat_model->getPusk($set);                
            foreach ($puskesmas as $pusk) {
                $tabel.='<td>'.$pusk->faskes.'</td>';
            }$tabel.='</tr>';
            //konten
            $j=0;
            foreach ($kolom1 as $key => $value) {           
                $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['kode_obat'].'</td>';
                foreach ($puskesmas as $pusk) 
                {                
                    $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['keluar'])) ? $kolom1[$key][$pusk->kode_pusk]['keluar'] : 0;
                    $tabel.='<td>'.$tmpsedia.'</td>';
                }
                $tabel.='</tr>';
                
            }
        }else{
            $tabel .='</tr><tr><td colspan="2">Data tidak ditemukan</td></tr>';
        }
        $tabel.='</table>';
        $data['obat_puskesmas']=$tabel;
        $data['date_start']=$set['awal'];$data['date_end']=$set['akhir'];
        $data['dinas']=$kabupaten;
        $this->load->view('laporan/laporan_persediaan_pkm_print',$data);
    }

}
?>
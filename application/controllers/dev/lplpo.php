<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          waone.wawanopoanane@gmail.com
 * @link            bankdataelog.kemkes.go.id/api
 */
class Lplpo extends REST_Controller {

    var $type = 'lplpo';

    function __construct()
    {
        parent::__construct();
        $this->load->model('manajemenlogistik/distribusi_obat_kel_model');
    }

    public function auth($data) {
        $this->db->select('*');        
        $this->db->where('username', $data['username']);
        $this->db->where('pswd', md5($data['password']));
        $this->db->where('kode_puskesmas', $data['area']);

        return $this->db->get('tb_user')->result();
    }

    public function checkLog($data) {
        if ((!empty($data['periode'])) && (!empty($data['area']))) {            
            $this->db->where('kode_pusk', $data['area']);
            $this->db->where('periode', $data['periode'].'-00');

            return $this->db->get('tb_lplpo')->row_array();
        }
    }

    public function insertLog($data) {
        $periodeLog = $data['periode'].'-00';
        $subdistrictCode = substr($data['area'], 1, 7);
        $content = array(
                'periode'=> $periodeLog,
                'kode_kec'=> $subdistrictCode,
                'kode_pusk'=> $data['area'],
                'create_by'=>$data['user']
                );
        $this->db->insert('tb_lplpo',$content);
        return $this->db->insert_id();
    }

    public function checkTransaction($id_lplpo) {
        $this->db->where('id_lplpo', $id_lplpo);
        return $this->db->get('tb_detail_distribusi');
    }

    // to get data sbbk http://localhost/e-logistics/api/lplpo/drugs
    // {
    //     "dataEnvironment":{
    //       "token":{"username":"xxx","password":"yyy","area":"zzz"},
    //       "content": {
    //           "area": "zzz",
    //           "periode": "2017-12"
    //         }
    //     }
    // }

    public function drugs_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                //cek log
                $temp = $this->checkLog($data['content']);
                if(!empty($temp)) {
                    $lplpo = $this->db->where('id_lplpo', $temp['id'])->get('tb_detail_lplpo_kel')->result_array();
                    $content = array(
                                'table' => $lplpo,
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => strtoupper($this->type)
                            );
                } else {
                    $content = array(
                                'table' => 'no data',
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => strtoupper($this->type)
                            );
                }
                $code = 200;
                $status = true;
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }            
        } else {
            $content = "Maaf, token Anda salah";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }

    // to get data sbbk http://localhost/e-logistics/api/lplpo/reports
    // {
    //     "dataEnvironment":{
    //       "token":{"username":"xxx","password":"yyy","area": "zzz"},
    //       "content": {
    //           "area": "P1971020101",
    //           "periode": "2017-12",
    //           "table":[
    //                 {
    //                     "kode_generik": "956",
    //                     "nama_obj": "ASAM GLUKONAT|ASAM SITRAT|ASAM FERULAT|ASAM ASKORBAT INJEKSI 10 ML",
    //                     "sediaan": "Tablet",
    //                     "stok_awal": "3870",
    //                     "terima": "3000",
    //                     "sedia": "6870",
    //                     "pakai": "2464",
    //                     "rusak_ed": "0",
    //                     "stok_akhir": "4406",
    //                     "stok_opt": "2464",
    //                     "permintaan": "3000",
    //                     "pemberian": "3000",
    //                     "dana": "0",
    //                     "ket": ""
    //                 },
    //                 {
    //                     "kode_generik": "51",
    //                     "nama_obj": "ACETYLCYSTEINE TABLET 200 MG",
    //                     "sediaan": "CAPSUL",
    //                     "stok_awal": "1352",
    //                     "terima": "1500",
    //                     "sedia": "2852",
    //                     "pakai": "1075",
    //                     "rusak_ed": "0",
    //                     "stok_akhir": "1777",
    //                     "stok_opt": "1075",
    //                     "permintaan": "2400",
    //                     "pemberian": "2400",
    //                     "dana": "0",
    //                     "ket": ""
    //                 },
    //                 {
    //                     "kode_generik": "804",
    //                     "nama_obj": "ANTACIDA SIRUP 400 MG/5 ML",
    //                     "sediaan": "SUSPENSI",
    //                     "stok_awal": "131",
    //                     "terima": "0",
    //                     "sedia": "131",
    //                     "pakai": "60",
    //                     "rusak_ed": "0",
    //                     "stok_akhir": "71",
    //                     "stok_opt": "60",
    //                     "permintaan": "100",
    //                     "pemberian": "100",
    //                     "dana": "0",
    //                     "ket": ""
    //                 },
    //                 {
    //                     "kode_generik": "119",
    //                     "nama_obj": "ACYCLOVIR TABLET 200 MG",
    //                     "sediaan": "TABLET",
    //                     "stok_awal": "140",
    //                     "terima": "0",
    //                     "sedia": "140",
    //                     "pakai": "40",
    //                     "rusak_ed": "0",
    //                     "stok_akhir": "100",
    //                     "stok_opt": "40",
    //                     "permintaan": "200",
    //                     "pemberian": "200",
    //                     "dana": "0",
    //                     "ket": ""
    //                 }
    //             ]
    //         }
    //     }
    // }

    public function reports_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {

            if ($data["token"]["area"]==$data['content']['area']) {
                //select table
                $tableName = 'tb_detail_lplpo_kel';
                //cek log
                $temp = $this->checkLog($data['content']);
                if(empty($temp)) {
                    //create log
                    $user = $this->auth($data["token"]);
                    $data['content']['user'] = $user[0]->id_user;
                    $id_lplpo = $this->insertLog($data['content']);
                } else {
                    $id_lplpo = $temp['id'];
                }

                $if_exist = $this->checkTransaction($id_lplpo);
                if ($if_exist->num_rows() > 0) {
                    // consist a transaction
                    $content = "Data can't insert, consist transaction";
                    $code = 200;
                    $status = true;
                } else {
                    // drop lplpo
                    $this->db->where('id_lplpo', $id_lplpo);
                    $this->db->delete($tableName);

                    //insert content
                    foreach ($data['content']['table'] as $key => $value) {
                        $value['id_lplpo'] = $id_lplpo;
                        $this->db->insert($tableName,$value);
                    }

                    $content = "Congrulation data saved successfully";
                    $code = 200;
                    $status = true;
                }    
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }
                                    
        } else {
            $content = "Sorry, wrong token";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }    

    public function report_delete() {
        //delete if not verified yet
    }

    // to get data sbbk http://localhost/e-logistics/api/lplpo/sbbk
    // {
    //     "dataEnvironment":{
    //       "token":{"username":"admin_puskesmas","password":"rahasia","area":"P1971021201"},
    //       "content": {
    //           "area": "P1971021201",
    //           "periode": "2017-11"
    //         }
    //     }
    // }

    public function sbbk_post() {
        $data = $this->post('dataEnvironment');        
        if ($this->auth($data["token"])) {
            if ($data["token"]["area"]==$data['content']['area']) {
                $temp = $this->checkLog($data['content']);
                if (!empty($temp)) {                    
                    $sbbk = $this->distribusi_obat_kel_model->show_data_inn($temp['id']);
                    if ($sbbk) {
                        $content = array(
                                'table' => $sbbk,
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => 'SBBK'
                            );
                        $code = 200;
                        $status = true;
                    } else {
                        $content = array(
                                'table' => 'no data',
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => 'SBBK'
                            );
                        $code = 200;
                        $status = true;
                    }
                    
                } else {
                    $content = array(
                                'table' => 'lplpo does not exist',
                                'area' => $data['content']['area'],
                                'periode' => $data['content']['periode'],
                                'type' => 'SBBK'
                            );
                        $code = 200;
                        $status = true;
                }                
            } else {
                $content = "Sorry, wrong access";
                $code = 201;
                $status = false;
            }
        } else {
            $content = "Sorry, wrong token";
            $code = 201;
            $status = false;
        }

        $feedBack = array(
                        'code' => $code,
                        'content' => $content,
                        'status' => $status,
                        );

        $this->response($feedBack);
    }
}

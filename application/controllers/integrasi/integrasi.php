<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Integrasi extends CI_Controller {

    var $title = 'Integrasi Ketersediaan Obat';
    var $profil;

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('integrasi/integrasi_model');
        $this->load->model('integrasi/log_sinkron_model','lsinkron_model');
        $this->load->model('profil/setting_institusi_model');
        $this->profil = $this->setting_institusi_model->getDataInstitusi();

    }

    function index()
    {
        if($this->session->userdata('login')==TRUE){
            $data['user']=$this->session->userdata('username');
            $data['base_url']=$this->url;
            $this->getView($data);
            //$this->load->view('masterdata/Data_Obat',$data);
        }else{
            redirect('login');
        }
    }

    function validasi()
    {
        $error = array();
        //$tahun = $this->input->post('tahun');
        //$bulan = $this->input->post('bulan');
        $jenis_data = $this->input->post('jenis_data');
        /*
        if($tahun=="")
        {
            $error[] = "Tahun harus diisi/dipilih";
        }

        if($bulan=="")
        {
            $error[] = "Bulan harus diisi/dipilih";
        }
        */
        if($jenis_data=="")
        {
            $error[] = "Jenis Data harus diisi/dipilih";
        }

        if(!empty($error))
        {
            $ret['msg'] = implode("<br>",$error);
            $ret['error'] = "failed";
        }
        else
        {
            $ret['error'] = "success";
            $ret['msg'] = "Sinkronisasi Berhasil";
        }
        return $ret;
    }

    function log_save($status,$timenow,$bulan,$tahun,$jenis_data,$wil,$mode="")
    {
       $data['periode'] = $bulan ."/". $tahun;
       $data['cara_kirim'] = $this->input->post('metode_kirim');
       $data['tglsistem'] = $timenow;
       $data['status'] = $status;
       $data['namatable'] = $jenis_data."_".$bulan."_".$tahun."_".$wil;
       $data['id_user'] = $this->session->userdata('id');
       if($mode=="sinkron")
       {
         $data['id'] = date('d-m-Y')."/".date('H:i:s');
         $data['id_user'] = $this->session->userdata('username');
         return $data;
       }
       else
       {
         $h = $this->lsinkron_model->DoAdd($data);
         return $h;
       }

    }

    function downloadfile($jenis_data)
    {
        $filename = $jenis_data;
        $pathname = "temp_upload/". $filename;
        header('Content-Disposition: attachment; '.$filename);
        header('Content-type: application/zip');
        header('Pragma: no-cache');
        header('Expires: 0');
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        readfile($pathname);
    }

    function process()
    {
      $accountBankData = getProfile();
      $ret = array();
      $timenow = timeNow();
      // $tmptime = splitDate($timenow," ");
      // list($tahun,$bulan) =  splitDate($tmptime[0],"-");
      $metode_kirim = $this->input->post('metode_kirim');
      $periode = explode('-',$this->input->post('periode'));
      $tahun = $periode[1];
      $bulan = $periode[0];
      $tmptime = $tahun.'-'.$bulan.'-28';
      $jenis_data = $this->input->post('jenis_data');
      if($this->profil['id_kab']==""){
        $wil = $this->profil['id_prop'].'00';
        $get = $this->setting_institusi_model->getNamaProp($this->profil['id_prop']);
        $namawil = str_replace(" ","",$get['CPropDescr']);
      }else{
        $wil = $this->profil['id_kab'];
        $get = $this->setting_institusi_model->getNamaKab($wil);
        $namawil = str_replace(" ","",$get['CKabDescr']);
      }
      //$wil = $this->profil['id_kab'];
      $ret = $this->validasi();
      $ret['url'] = "-";
      $hasil1 = array();
      $log = array();
      //$hasil3 = array();
      if($ret['error'] == 'success')
      {
          $hasil1 = $this->integrasi_model->proses($jenis_data,$tmptime,$bulan,$tahun,$wil);
          $log = $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil,"sinkron");

          //$hasil3 = $this->integrasi_model->proses($jenis_data,"upk4",$tmptime[0],$bulan,$tahun,$wil);
          $gel1 = "";
          $logdata = "";
          $gel3 = "";
          if($metode_kirim == "online")
          {
              foreach($hasil1 as $key => $value)
              {
                  $gel1 .= $value->kode_obat . '|';
                  $gel1 .= $value->jml_penggunaan.'|';
                  $gel1 .= $value->jml_stok.'|';
                  $gel1 .= $value->cur_mont.'|';
                  $gel1 .= $value->nama_obj.'|';
                  $gel1 .= $value->kelompok. "\n";
              }

              foreach($log as $key => $value)
              {
                  $logdata .= $log['id'] . '|';
                  $logdata .= $log['periode'] . '|';
                  $logdata .= $log['cara_kirim'].'|';
                  $logdata .= $log['tglsistem'].'|';
                  $logdata .= $log['status'].'|';
                  $logdata .= $log['namatable'].'|';
                  $logdata .= $log['id_user']."\n";
              }

              /*
              foreach($hasil2 as $key => $value)
              {
                  $gel2 .= $value->kode_obat . ';';
                  $gel2 .= $value->jml_penggunaan.';';
                  $gel2 .= $value->jml_stok.';';
                  $gel2 .= $value->cur_mont.';';
                  $gel2 .= $value->nama_obj. "\n";
              }

              foreach($hasil3 as $key => $value)
              {
                  $gel3 .= $value->kode_obat . ';';
                  $gel3 .= $value->jml_penggunaan.';';
                  $gel3 .= $value->jml_stok.';';
                  $gel3 .= $value->cur_mont.';';
                  $gel3 .= $value->nama_obj. "\n";
              }
              */
           require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
            $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
            $client->soap_defencoding = 'UTF-8';
            $params = array(
                'username'      => base64_encode($accountBankData['username']),
                'password'      => base64_encode($accountBankData['password']),
                'tahun' 		    => $tahun,
                'bulan'             => $bulan,
                'jenis_data'        => $jenis_data,
                'wil'               => $wil,
                'data1' 		        => $gel1,
                'logkirim' 		        => $logdata
                //'data2' 		        => $gel2,
                //'data3' 		        => $gel3
            );
            $result = $client->call('Receive', $params);
            if ($client->fault)
            {
              $ret['error'] = "failed";
              $ret['msg'] = "The request contains an invalid SOAP body";
            }elseif($result!='success'){
              $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['error'] = "failed";
              $ret['msg'] = $result;
            }else{
                $err = $client->getError();
                if ($err) {
                    $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "failed";
                    $ret['msg'] = $err;

                    //echo '<h2>Error</h2><pre>' . $err . '</pre>';
                } else {
                    $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "success";
                    $ret['msg'] = $result;

                    //echo '<h2>Result</h2><pre>'; print_r($result); echo '</pre>';
                }
            }
          }
          else
          {
              $filename1 = $jenis_data."_".$bulan."_".$tahun."_".$wil.".csv";
              $pathname1 = "temp_upload/". $filename1;
              //$f = fopen ($pathname1,'w');
              $nl = "\n";
              foreach($hasil1 as $key => $value)
              {
                  $gel1 .= '"'."$value->kode_obat".'","'."$value->jml_penggunaan".
                      '","'."$value->jml_stok".
                      '","'."$value->cur_mont".
                      '","'."$value->nama_obj".
                      '","'."$value->kelompok".'"'.$nl;
                  //fwrite($f, $gel);
              }
              //fclose($f);

              $filename2 = "loghistori_".$bulan."_".$tahun."_".$wil.".csv";
              $pathname2 = "temp_upload/". $filename2;
              //$f = fopen ($pathname2,'w');
              foreach($log as $key => $value)
              {
                  $id = $log['id'];
                  $periode = $log['periode'];
                  $cara_kirim = $log['cara_kirim'];
                  $tglsistem = $log['tglsistem'];
                  $status = $log['status'];
                  $namatable = $log['namatable'];
                  $id_user = $log['id_user'];


                  $gel2 = '"'."$id".'","'."$periode".
                      '","'."$cara_kirim".
                      '","'."$tglsistem".
                      '","'."$status".
                      '","'."$namatable".
                      '","'."$id_user".'"'.$nl;
                  //fwrite($f, $gel);
              }
              //fclose($f);
              $data = array(
                  $filename1 => $gel1,
                  $filename2 => $gel2
              );
              $filename = $jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip';
              $this->load->library('zip');
              $this->zip->add_data($data);
              $this->zip->archive('temp_upload/'.$jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip');
              //$datazip = $this->zip->download($filename);

              /*
              $lokfile = path(APP_ROOT_PATH, 'tmp', 'sms.csv');
              $f = fopen ($smsName,'w');

              header("Pragma: public");
              header("Expires: 0");
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header("Content-Type: application/force-download");
              header("Content-Disposition: attachment; filename=".$jenis_data.".csv");
              header("Content-Description: File Transfer");
              */

              /*
              header('Content-Disposition: attachment; filename='.$jenis_data.'.csv');
              header('Content-type: application/vnd.ms-excel');
              header('Pragma: no-cache');
              header('Expires: 0');
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              */
              //echo $gel;
              $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['jenis_data'] = $filename;
              $ret['url'] = "downloadfile";
              $ret['error'] = "success";
              $ret['msg'] = "Eksport CSV Berhasil";

          }
      }
        echo json_encode($ret);
        //echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
        //echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
        //echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';

        //redirect('integrasi');
    }

    function tahun()
    {
        $now = array();
        $start = array();
        $start['mktime'] = strtotime("-1 day");
        $start['year'] = date("Y", $start['mktime']);
        $now['year'] = date("Y");
        $now['year_start'] = date("Y")-5;
        for($i=$now['year_start'];$i<=$now['year'];$i++)
        {
            $hasil[$i] = $i;
        }
        return $hasil;
    }

    function jenis_integrasi()
    {
       $hasil = array('ketersediaanobat' => 'Ketersediaan Obat','obatkadaluwarsa_all' => 'Obat Kadaluwarsa');
       return $hasil;
    }

    function getdetail($namatable)
    {
        $data['result'] = $this->lsinkron_model->getData($namatable);
        $this->load->view('integrasi/integrasi_detail', $data);
    }

    function getView($data)
    {
        //$data['bulannow'] = (strlen(date("m"))==1) ? "0".date("m") : date("m");
        //$data['tahunnow'] = date("Y");
        //$data['bulan'] = bulan();
        //$data['tahun'] = $this->tahun();
        $data['jenis_data'] = $this->jenis_integrasi();
        $data['bank_data_server1'] = $this->profil['url_bank_data1'];
        $this->load->view('integrasi/integrasi', $data);
    }
}
?>
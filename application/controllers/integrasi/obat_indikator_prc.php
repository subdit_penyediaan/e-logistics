<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Obat_indikator_prc extends CI_Controller {

    var $title = 'Integrasi';
    var $profil;

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('integrasi/obat_indikator_model');
        $this->load->model('integrasi/log_sinkron_oi_model','lsinkron_model');
        $this->load->model('profil/setting_institusi_model');
        $this->profil = $this->setting_institusi_model->getDataInstitusi();

    }

    function index()
    {
        if($this->session->userdata('login')==TRUE){
            $data['user']=$this->session->userdata('username');
            $data['base_url']=$this->url;
            $this->getView($data);
            //$this->load->view('masterdata/Data_Obat',$data);
        }else{
            redirect('login');
        }
    }

    function validasi()
    {
        $error = array();
        //$tahun = $this->input->post('tahun');
        //$bulan = $this->input->post('bulan');
        $jenis_data = $this->input->post('jenis_data');
        /*
        if($tahun=="")
        {
            $error[] = "Tahun harus diisi/dipilih";
        }

        if($bulan=="")
        {
            $error[] = "Bulan harus diisi/dipilih";
        }
        */
        if($jenis_data=="")
        {
            $error[] = "Jenis Data harus diisi/dipilih";
        }

        if(!empty($error))
        {
            $ret['msg'] = implode("<br>",$error);
            $ret['error'] = "failed";
        }
        else
        {
            $ret['error'] = "success";
            $ret['msg'] = "Sinkronisasi Berhasil";
        }
        return $ret;
    }

    function log_save($status,$timenow,$bulan,$tahun,$jenis_data,$wil,$mode="")
    {
       $data['periode'] = $bulan ."/". $tahun;
       $data['cara_kirim'] = $this->input->post('metode_kirim');
       $data['tglsistem'] = $timenow;
       $data['status'] = $status;
       $data['namatable'] = $jenis_data."_".$bulan."_".$tahun."_".$wil;
       //$data['namatable'] = $jenis_data."_".$wil;
       $data['id_user'] = $this->session->userdata('id');
       if($mode=="sinkron")
       {
         $data['id'] = date('d-m-Y')."/".date('H:i:s');
         $data['id_user'] = $this->session->userdata('username');
         return $data;
       }
       else
       {
         $h = $this->lsinkron_model->DoAdd($data);
         return $h;
       }
    }

    function downloadfile($jenis_data)
    {
        $filename = $jenis_data;
        $pathname = "temp_upload/". $filename;
        header('Content-Disposition: attachment; '.$filename);
        header('Content-type: application/vnd.ms-excel');
        header('Pragma: no-cache');
        header('Expires: 0');
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        readfile($pathname);
    }

    function process()
    {
      $accountBankData = getProfile();
      $ret = array();
      $timenow = timeNow();      
      // $tmptime = splitDate($timenow," ");
      // list($tahun,$bulan) =  splitDate($tmptime[0],"-");
      $metode_kirim = $this->input->post('metode_kirim');
      $periode = explode('-',$this->input->post('periode'));
      $tahun = $periode[1];
      $bulan = $periode[0];
      $tmptime = $tahun.'-'.$bulan.'-28';
      $jenis_data = $this->input->post('jenis_data');
      if($this->profil['id_kab']==""){
        $wil = $this->profil['id_prop'].'00';
        $get = $this->setting_institusi_model->getNamaProp($this->profil['id_prop']);
        $namawil = str_replace(" ","",$get['CPropDescr']);
      }else{
        $wil = $this->profil['id_kab'];
        $get = $this->setting_institusi_model->getNamaKab($wil);
        $namawil = str_replace(" ","",$get['CKabDescr']);  
      }
      //$wil = $this->profil['id_kab'];
      $ret = $this->validasi();
      $ret['url'] = "-";
      if($ret['error'] == 'success')
      {
          $hasil = $this->obat_indikator_model->proses($jenis_data,$tmptime,$bulan,$tahun,$wil);
          $log = $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil,"sinkron");

          //$hasil3 = $this->integrasi_model->proses($jenis_data,"upk4",$tmptime[0],$bulan,$tahun,$wil);
          //$gel1 = "";
          $logdata = "";
          $gel3 = "";
          $gel = "";
          if($metode_kirim == "online")
          {
              /*
              $data['periode'] = $bulan ."/". $tahun;
              $data['cara_kirim'] = $this->input->post('metode_kirim');
              $data['tglsistem'] = $timenow;
              $data['namatable'] = $jenis_data."_".$bulan."_".$tahun."_".$wil;
              $data['id_user'] = $this->session->userdata('id');
              */

              foreach($hasil as $key => $value)
              {
                  $gel .= $value->kode_pusk. '|';
                  $gel .= $value->id_obatindikator.'|';
                  $gel .= $value->nama_indikator.'|';                  
                  $gel .= $value->sedia. "\n";
              }

              foreach($log as $key => $value)
              {
                  $logdata .= $log['id'] . '|';
                  $logdata .= $log['periode'] . '|';
                  $logdata .= $log['cara_kirim'].'|';
                  $logdata .= $log['tglsistem'].'|';
                  $logdata .= $log['status'].'|';
                  $logdata .= $log['namatable'].'|';
                  $logdata .= $log['id_user']."\n";
              }
           require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
            $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
            $client->soap_defencoding = 'UTF-8';
            $params = array(
                'username'      => base64_encode($accountBankData['username']),
                'password'      => base64_encode($accountBankData['password']),
                'tahun' 		    => $tahun,
                'bulan'             => $bulan,
                'jenis_data'        => $jenis_data,
                'wil'               => $wil,
                'data' 		        => $gel,
                'logkirim'            => $logdata
            );
            $result = $client->call('Receive', $params);
            //print_r($params);die;
            if ($client->fault)
            {
              $ret['error'] = "failed";
              $ret['msg'] = "The request contains an invalid SOAP body";
            }elseif($result!='success'){
              $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['error'] = "failed";
              $ret['msg'] = $result;
            }else{
                $err = $client->getError();
                if ($err) {
                    $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "failed";
                    $ret['msg'] = $err;

                    //echo '<h2>Error</h2><pre>' . $err . '</pre>';
                } else {
                    $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "success";
                    $ret['msg'] = $result;

                    //echo '<h2>Result</h2><pre>'; print_r($result); echo '</pre>';
                }
            }
          }
          else
          {
              //$filename1 = $jenis_data."_".$bulan."_".$tahun."_".$wil.".csv";
              $filename1 = $jenis_data."_".$wil.".csv";
              $pathname1 = "temp_upload/". $filename1;
              //$f = fopen ($pathname,'w');
              $nl = "\n";
              foreach($hasil as $key => $value)
              {
                  $gel .= '"'."$value->kode_pusk".'","'."$value->id_obatindikator".
                      '","'."$value->nama_indikator".                      
                      '","'."$value->sedia".'"'.$nl;
                  //fwrite($f, $gel);
              }
              //fclose($f);
              $filename2 = "loghistori_".$bulan."_".$tahun."_".$wil.".csv";
              $pathname2 = "temp_upload/". $filename2;

              foreach($log as $key => $value)
              {
                  $id = $log['id'];
                  $periode = $log['periode'];
                  $cara_kirim = $log['cara_kirim'];
                  $tglsistem = $log['tglsistem'];
                  $status = $log['status'];
                  $namatable = $log['namatable'];
                  $id_user = $log['id_user'];


                  $gel2 = '"'."$id".'","'."$periode".
                      '","'."$cara_kirim".
                      '","'."$tglsistem".
                      '","'."$status".
                      '","'."$namatable".
                      '","'."$id_user".'"'.$nl;
                  //fwrite($f, $gel);
              }
              //fclose($f);
              $data = array(
                  $filename1 => $gel,
                  $filename2 => $gel2
              );
              $filename = $jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip';
              $this->load->library('zip');
              $this->zip->add_data($data);
              $this->zip->archive('temp_upload/'.$jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip');
              
              /*
              $lokfile = path(APP_ROOT_PATH, 'tmp', 'sms.csv');
              $f = fopen ($smsName,'w');

              header("Pragma: public");
              header("Expires: 0");
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header("Content-Type: application/force-download");
              header("Content-Disposition: attachment; filename=".$jenis_data.".csv");
              header("Content-Description: File Transfer");
              */

              /*
              header('Content-Disposition: attachment; filename='.$jenis_data.'.csv');
              header('Content-type: application/vnd.ms-excel');
              header('Pragma: no-cache');
              header('Expires: 0');
              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              */
              //echo $gel;
              $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['jenis_data'] = $filename;
              $ret['url'] = "downloadfile";
              $ret['error'] = "success";
              $ret['msg'] = "Eksport CSV Berhasil";

          }
      }
        echo json_encode($ret);
        //echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
        //echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
        //echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';

        //redirect('integrasi');
    }

    function tahun()
    {
        $now = array();
        $start = array();
        $start['mktime'] = strtotime("-1 day");
        $start['year'] = date("Y", $start['mktime']);
        $now['year'] = date("Y");
        $now['year_start'] = date("Y")-5;
        for($i=$now['year_start'];$i<=$now['year'];$i++)
        {
            $hasil[$i] = $i;
        }
        return $hasil;
    }

    function jenis_integrasi()
    {
       $hasil = array('ketersediaanobat' => 'Ketersediaan Obat','obatkadaluarsa' => 'Obat Kadaluarsa');
       return $hasil;
    }

    function getdetail($namatable)
    {
        //$data['result'] = $this->lsinkron_model->getData($namatable);
        $kolom = array();
        //inisialisasi nilai
        //$result=$this->ketersediaan_obat_puskesmas_kel_model->getRows($set);
        $data['n']= $this->db->count_all('ref_obatindikator');
        $result = $this->lsinkron_model->getData($namatable);
        foreach($result as $key => $rows)
        {
            //print_r($result);exit;
            //$kolom1[$rows->kode_obat][$rows->kode_pusk]['kode_obat']=$rows->kode_obat;
            //$n = $this->db->count_all('ref_obatindikator');
            //if($i<=$n){

            //}
            $kodepuskesmas = $rows->kode_pusk;
            // if($rows->kode_pusk!=''){
            //     $kodepuskesmas = $rows->kode_pusk;
            // }else{
            // //    print "sdf";exit;
            //     $kodepuskesmas = $result[$key-1]->kode_pusk;
            //var_dump($kodepuskesmas);

            //}
            $kolom1[$rows->id_obatindikator][$kodepuskesmas]['sedia']=$rows->sedia;
            //$kolom1[$kodepuskesmas]['persentasi']=0;
            $kolom1[$rows->id_obatindikator]['indikator']=$rows->nama_indikator;
        }

        //header
        $puskesmas=$this->lsinkron_model->getPusk($namatable);
        /*
        $list_obat=$this->firstload_model->getListObat();
        $i=0;
        foreach ($list_obat as $rows_drug) {
            $nama_obat[$i]=$rows_drug->kode_obat;
            $i++;
        }*/
        $tabel='<table class="table table-striped table-bordered"><tr class="active th_head"><td>No.</td><td>LIST INDIKATOR</td>';
        foreach ($puskesmas as $pusk) {
            $persentasi[$pusk->kode_pusk]=0;
            $tabel.='<td>'.$pusk->NAMA_PUSKES.'</td>';
        }$tabel.='</tr>';
        //konten
        $j=0;
        //print_r($kolom1);exit;
        foreach ($kolom1 as $key => $value) {           
            $tabel.='<tr><td>'.++$j.'</td><td>'.$kolom1[$key]['indikator'].'</td>';
            foreach ($puskesmas as $pusk) 
            {
                //print $key;
                //print_r($value);
                //print_r($value[$key][$pusk->kode_pusk]);exit;
                //exit;
            //foreach ($key as $key2) {
                $tmpsedia = (isset($kolom1[$key][$pusk->kode_pusk]['sedia'])) ? $kolom1[$key][$pusk->kode_pusk]['sedia'] : 0;
                if($tmpsedia > 0){
                    $indikator = '<span class="glyphicon glyphicon-ok" style="color:green"></span>';
                    $persentasi[$pusk->kode_pusk]++;
                    $flag = 1;
                }else{
                    $indikator = '<span class="glyphicon glyphicon-remove" style="color:red"></span>';
                    $flag = 0;
                }

                $tabel.='<td class="th_head">'.$indikator.'<input type="hidden" class="persen_'.$pusk->kode_pusk.'" value="'.$flag.'"></td>';
            }
            $tabel.='</tr>';
            
        }
        $tabel.='<tr><td colspan="2">Presentasi: </td>';
        foreach ($puskesmas as $pusk) {
            //$tabel.='<td class="th_head"><button id="btn_'.$pusk->kode_pusk.'" class="btn btn-success btn-sm btn_persentasi"><span class="glyphicon glyphicon-th"></span> Hitung</button><label id="lb_'.$pusk->kode_pusk.'" class="label_txt"></label></td>';
            $nilai_persentasi = ($persentasi[$pusk->kode_pusk]/$data['n'])*100;
            $tabel.='<td class="th_head"><b>'.$nilai_persentasi.' %</b></td>';
        }$tabel.='</tr>';
        $tabel.='</table>';
        $data['obat_indikator']=$tabel;
        $this->load->view('integrasi/obat_indikator_list',$data);
    }

    function getView($data)
    {
        //$data['bulannow'] = (strlen(date("m"))==1) ? "0".date("m") : date("m");
        //$data['tahunnow'] = date("Y");
        //$data['bulan'] = bulan();
        //$data['tahun'] = $this->tahun();
        $data['jenis_data'] = $this->jenis_integrasi();
        $data['bank_data_server1'] = $this->profil['url_bank_data1'];
        $this->load->view('integrasi/obat_indikator_prc', $data);
    }
}
?>
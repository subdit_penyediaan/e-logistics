<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Obat_indikator extends CI_Controller {

    var $title = 'Daftar Laporan Obat Indikator Puskesmas';
    var $limit = 10;

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('integrasi/log_sinkron_oi_model','m_log_sinkron');
    }

    function index()
    {
        if($this->session->userdata('login')==TRUE)
        {
            $this->getList2();
            //$this->load->view('masterdata/Data_Obat',$data);
        }else{
            redirect('login');
        }
    }

    function getList($search='',$offset=0)
    {
        if(IS_AJAX == true && $offset!=0)
        {
            //$this->form_validation->set_rules('search_name', 'lang:search_name', 'required|min_length[3]');
            $search = $this->input->post('search_name');
            $filter = $this->input->post('filter');
        }
        else
        {
            if($search!="")
            {
                $tmpname = explode("_",$search);
                $search = $tmpname[0];
                $filter = $tmpname[1];
            }
            else
            {
                $search = "";
                $filter = "";
            }
        }

        $data['search_name'] = $search;
        $data['filter'] = $filter;
        $this->pagination->initialize(array(
            'base_url' => base_url().'index.php/integrasi/kadaluarsa_list/getList/'.$search."_".$filter,
            'per_page' => $this->limit,
            'total_rows' => $this->m_log_sinkron->getList($search,$filter,'total'),
            'uri_segment' => 10,
            'full_tag_open' => '<div id="pager" class="pagination">',
            'full_tag_close' => '</div>'
        ));

        $data['total'] =  $this->m_log_sinkron->getList($search,$filter,'total');
        $data['pager'] = $this->pagination->create_links();
        $data['result'] = $this->m_log_sinkron->getList($search,$filter,'data',$this->limit,$offset);
        if(IS_AJAX == true && $offset!=0)
        {
            foreach($data['result'] as $key => $value)
            {
              $item['periode'] = $value->periode;
              $item['id'] = $value->id;
              $item['cara_kirim'] = $value->cara_kirim;
              $item['username'] = $value->username;
              $item['status'] = $value->status;
              $sent['table'][$key] = $item;
            }
            echo json_encode($sent);
        }
        else
            $this->getView($data);
    }


    function getView($data)
    {
        $data['title'] = $this->title;
        $this->load->view('integrasi/obat_indikator', $data);
    }

    function getList2(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/integrasi/obat_indikator/getList2';
        $config['total_rows'] = $this->m_log_sinkron->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->m_log_sinkron->getData2($config['per_page'],$this->uri->segment(4));
        $data['pager'] = $this->pagination->create_links();
        //$this->load->view('masterdata/rute_obat_list',$data);
        $this->getView($data);
    }
}
?>
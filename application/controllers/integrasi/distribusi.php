<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Distribusi extends CI_Controller {

    var $title = 'Integrasi distribusi';
    var $profil;

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('integrasi/distribusi_model');
        $this->load->model('integrasi/log_sinkron_distribusi_model','lsinkron_model');
        $this->load->model('profil/setting_institusi_model');
        $this->profil = $this->setting_institusi_model->getDataInstitusi();
    }

    function index()
    {
        if($this->session->userdata('login')==TRUE){
            $data['user']=$this->session->userdata('username');
            $data['base_url']=$this->url;

            $this->getView($data);
        }else{
            redirect('login');
        }
    }

    function validasi()
    {
        $error = array();
        $jenis_data = $this->input->post('jenis_data');

        if($jenis_data=="")
        {
            $error[] = "Jenis Data harus diisi/dipilih";
        }

        if(!empty($error))
        {
            $ret['msg'] = implode("<br>",$error);
            $ret['error'] = "failed";
        }
        else
        {
            $ret['error'] = "success";
            $ret['msg'] = "Sinkronisasi Berhasil";
        }
        return $ret;
    }

    function log_save($status,$timenow,$bulan,$tahun,$jenis_data,$wil,$mode="")
    {
       $data['periode'] = $bulan ."/". $tahun;
       $data['cara_kirim'] = $this->input->post('metode_kirim');
       $data['tglsistem'] = $timenow;
       $data['status'] = $status;
       $data['namatable'] = $jenis_data."_".$bulan."_".$tahun."_".$wil;
       $data['id_user'] = $this->session->userdata('id');
       if($mode=="sinkron")
       {
         $data['id'] = date('d-m-Y')."/".date('H:i:s');
         $data['id_user'] = $this->session->userdata('username');

         return $data;
       } else {
         $h = $this->lsinkron_model->DoAdd($data);

         return $h;
       }
    }

    function downloadfile($jenis_data)
    {
        $filename = $jenis_data;
        $pathname = "temp_upload/". $filename;
        header('Content-Disposition: attachment; '.$filename);
        header('Content-type: application/vnd.ms-excel');
        header('Pragma: no-cache');
        header('Expires: 0');
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        readfile($pathname);
    }

    function process()
    {
      $accountBankData = getProfile();
      $ret = array();
      $timenow = timeNow();
      $tmptime = splitDate($timenow," ");
      // list($tahun,$bulan) =  splitDate($tmptime[0],"-");
      $metode_kirim = $this->input->post('metode_kirim');
      $periode = explode('-',$this->input->post('periode'));
      $tahun = $periode[1];
      $bulan = $periode[0];
      $jenis_data = $this->input->post('jenis_data');
      if($this->profil['id_kab']==""){
        $wil = $this->profil['id_prop'].'00';
        $get = $this->setting_institusi_model->getNamaProp($this->profil['id_prop']);
        $namawil = str_replace(" ","",$get['CPropDescr']);
      }else{
        $wil = $this->profil['id_kab'];
        $get = $this->setting_institusi_model->getNamaKab($wil);
        $namawil = str_replace(" ","",$get['CKabDescr']);
      }

      $ret = $this->validasi();
      $ret['url'] = "-";
      if($ret['error'] == 'success')
      {
          $hasil = $this->distribusi_model->proses($jenis_data,'',$bulan,$tahun,$wil);
          $log = $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil,"sinkron");

          $logdata = "";
          $gel3 = "";
          $gel = "";
          if($metode_kirim == "online")
          {
              foreach($hasil as $key => $value)
              {
                  $gel .= $value->kode_obat . '|';
                  $gel .= $value->pemberian.'|';
                  $gel .= $value->kode_penerima. "|";
                  $gel .= $value->unit_penerima. "\n";
              }

              foreach($log as $key => $value)
              {
                  $logdata .= $log['id'] . '|';
                  $logdata .= $log['periode'] . '|';
                  $logdata .= $log['cara_kirim'].'|';
                  $logdata .= $log['tglsistem'].'|';
                  $logdata .= $log['status'].'|';
                  $logdata .= $log['namatable'].'|';
                  $logdata .= $log['id_user']."\n";
              }
           require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
            $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
            $client->soap_defencoding = 'UTF-8';
            $params = array(
                'username'	 		=> base64_encode($accountBankData['username']),
                'password' 			=> base64_encode($accountBankData['password']),
                'tahun' 		    => $tahun,
                'bulan'         => $bulan,
                'jenis_data'    => $jenis_data,
                'wil'           => $wil,
                'data' 		      => $gel,
                'logkirim'      => $logdata
            );
            $result = $client->call('Receive', $params);
            if ($client->fault)
            {
              $ret['error'] = "failed";
              $ret['msg'] = "The request contains an invalid SOAP body";
            }elseif($result!='success'){
              $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['error'] = "failed";
              $ret['msg'] = $result;
            }else{
                $err = $client->getError();
                if ($err) {
                    $this->log_save('gagal',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "failed";
                    $ret['msg'] = $err;
                } else {
                    $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
                    $ret['error'] = "success";
                    $ret['msg'] = $result;
                }
            }
          }
          else
          {
              $filename1 = $jenis_data."_".$wil.".csv";
              $pathname1 = "temp_upload/". $filename1;

              $nl = "\n";
              foreach($hasil as $key => $value)
              {
                  $gel .= '"'."$value->kode_obat".
                      '","'."$value->pemberian".
                      '","'."$value->kode_penerima".'"'.$nl;
              }
              $filename2 = "loghistori_".$bulan."_".$tahun."_".$wil.".csv";
              $pathname2 = "temp_upload/". $filename2;

              foreach($log as $key => $value)
              {
                  $id = $log['id'];
                  $periode = $log['periode'];
                  $cara_kirim = $log['cara_kirim'];
                  $tglsistem = $log['tglsistem'];
                  $status = $log['status'];
                  $namatable = $log['namatable'];
                  $id_user = $log['id_user'];


                  $gel2 = '"'."$id".'","'."$periode".
                      '","'."$cara_kirim".
                      '","'."$tglsistem".
                      '","'."$status".
                      '","'."$namatable".
                      '","'."$id_user".'"'.$nl;
              }
              $data = array(
                  $filename1 => $gel,
                  $filename2 => $gel2
              );
              $filename = $jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip';
              $this->load->library('zip');
              $this->zip->add_data($data);
              $this->zip->archive('temp_upload/'.$jenis_data.'_'. $bulan . "_" . $tahun. "_". $namawil. '.zip');

              $this->log_save('berhasil',$timenow,$bulan,$tahun,$jenis_data,$wil);
              $ret['jenis_data'] = $filename;
              $ret['url'] = "downloadfile";
              $ret['error'] = "success";
              $ret['msg'] = "Eksport CSV Berhasil";

          }
      }
        echo json_encode($ret);
    }

    function tahun()
    {
        $now = array();
        $start = array();
        $start['mktime'] = strtotime("-1 day");
        $start['year'] = date("Y", $start['mktime']);
        $now['year'] = date("Y");
        $now['year_start'] = date("Y")-5;
        for($i=$now['year_start'];$i<=$now['year'];$i++)
        {
            $hasil[$i] = $i;
        }
        return $hasil;
    }

    function getdetail($namatable)
    {
        $data['result'] = $this->lsinkron_model->getData($namatable);
        $this->load->view('integrasi/distribusi_detail', $data);
    }

    function getView($data)
    {
        //$data['bulannow'] = (strlen(date("m"))==1) ? "0".date("m") : date("m");
        //$data['tahunnow'] = date("Y");
        //$data['bulan'] = bulan();
        //$data['tahun'] = $this->tahun();
        // $data['jenis_data'] = $this->jenis_integrasi();
        $data['bank_data_server1'] = $this->profil['url_bank_data1'];
        $this->load->view('integrasi/distribusi', $data);
    }
}
?>

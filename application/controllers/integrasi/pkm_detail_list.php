<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Pkm_detail_list extends CI_Controller {

    var $title = 'Daftar Laporan Persediaan Puskesmas';
    var $limit = 10;

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('integrasi/log_sinkron_pkm_detail_model','m_log_sinkron');
    }

    function index()
    {
        if($this->session->userdata('login')==TRUE)
        {
            $this->getList2();
            //$this->load->view('masterdata/Data_Obat',$data);
        }else{
            redirect('login');
        }
    }    

    function getView($data)
    {
        $data['title'] = $this->title;
        $this->load->view('integrasi/pkm_detail_list', $data);
    }

    function getList2(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/integrasi/pkm_detail_list/getList2';
        $config['total_rows'] = $this->m_log_sinkron->countAllData(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);

        $data['result']=$this->m_log_sinkron->getData2($config['per_page'],$this->uri->segment(4));
        $data['pager'] = $this->pagination->create_links();
        //$this->load->view('masterdata/rute_obat_list',$data);
        $this->getView($data);
    }
}
?>
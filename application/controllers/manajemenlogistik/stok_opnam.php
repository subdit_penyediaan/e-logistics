<?php

Class Stok_Opnam extends CI_Controller {
    
    var $title = 'Stok Opnam';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/stok_opnam_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/stok_opnam',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['no_stok_opnam']=$this->input->post('no_stok_opnam');
		$data['nama_user']=$this->input->post('nama_user');
		$data['per_stok_opnam']=$this->input->post('per_stok_opnam');
		$data['cttn']=$this->input->post('cttn');
		$data['tgl_trans']=$this->input->post('tgl_trans');

		// $this->db->trans_start();
		$this->stok_opnam_model->input_data_m($data);
		$id_stok_opnam = $this->db->select_max('id')->get('tb_stok_opnam')->row_array();//get id stok opnam terakhir
		$data_stok = $this->db->get('tb_stok_obat');
		foreach ($data_stok->result() as $key) {
			$konten['id_stok_opname']=$id_stok_opnam['id'];
			$konten['id_so']=$key->id_stok;//bukan id stok opnam
			$konten['kode_obat']=$key->kode_obat;
			$konten['noted_stok']=$key->stok;
			$konten['harga_so']=$key->harga;
			$this->db->insert('tb_detail_stok_opnam',$konten);
		}
		// $this->db_trans_complete();
	}


	function get_data_stok_opnam(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/stok_opnam/get_data_stok_opnam';
		$config['total_rows'] = $this->stok_opnam_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->stok_opnam_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/stok_opnam_list',$data);
		//$this->load->view('list_puskesmas');
	}
	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->stok_opnam_model->deleteData($kode);
    }

    function input_data_tahunan(){
    	$tahun=$this->input->post('tahun');        

        $nama_tabel='tb_stok_tahun_'.$tahun;
        //cek if exist
        $cek = mysql_query("SHOW TABLES LIKE '$nama_tabel'");
        $exists = (mysql_num_rows($cek))?TRUE:FALSE;
        if(!$exists){
            $qry=$this->db->query("
            		CREATE TABLE `$nama_tabel` ( PRIMARY KEY(`id_stok`) )ENGINE=INNODB COLLATE = latin1_swedish_ci COMMENT = '' SELECT `id_stok`, `id_faktur`, `kode_obat`, `nama_obj`, `kategori`, `kode_generik`, `expired`, `stok`, `harga`, `sumber_dana`, `no_batch`, `flag` FROM `tb_stok_obat` WHERE 1 = 0;
	            ");
            $message['confirm']=1;
	        // if($qry){
	            
	        // }else{
	            
	        // }
        }else{
        	$message['confirm']=0;
        }
        
        echo json_encode($message);
    }

    function get_data_so_tahunan(){
    	$data['base_url']=$this->url;
    	$data['query'] = $this->db->query("SHOW TABLE STATUS LIKE 'tb_stok_tahun%'");
    	$this->load->view('manajemenlogistik/so_tahunan_list',$data);
    }

    function delete_data($nama_tabel){
    	$query = $this->db->query("DROP TABLE $nama_tabel");
    	if($query){
    		$message['confirm']=1;
    	}else{
    		$message['confirm']=0;
    	}

    	echo json_encode($message);
    }

    function get_detail($nama_tabel){
    	$data['base_url']=$this->url;
		//$data['detailfaktur']=$this->detail_faktur_model->getDataDetailFaktur($nomorfaktur);
		$data['satuan']=$this->db->get('ref_obat_satuan')->result_array();
		$data['nama_tabel']=$nama_tabel;
		$this->load->view('manajemenlogistik/stok_tahunan',$data);
    }

    function clean($string) {
	   $string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
		$string = str_replace('/', '-', $string);		
	   return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}
	
    function input_data_detail(){
		$data = array();
		//$data['id_faktur']=$this->input->post('id_faktur');
		$data['nama_tabel']=$this->input->post('nama_tabel');
		$data['nama_obat']=$this->input->post('nama_obat');
		$data['no_batch']=$this->clean($this->input->post('no_batch'));
		$data['jumlah']=$this->input->post('jumlah');
		$data['tanggal']=$this->input->post('tanggal');
		$data['harga_beli']=str_replace(",",".", $this->input->post('harga_beli'));
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['jumlah_kec']=$this->input->post('jumlah_kec');
		$data['jml_rusak']=$this->input->post('jml_rusak');

		$data['id_fornas']=$this->input->post('id_fornas');
		$data['id_ukp4']=$this->input->post('id_ukp4');
		$data['kode_atc']=$this->input->post('kode_atc');
		$data['kategori']=$this->input->post('kategori');
		$data['kemasan']=$this->input->post('kemasan');
		$data['sat_jual']=$this->input->post('sat_jual');
		$data['kode_generik']=$this->input->post('kode_generik');

		//change koma ke titik, ubah type data di database ke double
		//obat rusak mau masuk mana?
		$query=$this->stok_opnam_model->input_detail_tahunan($data);
	}

	function getData($id)
	{
		$result = $this->stok_opnam_model->getDetail($id)->row_array();

		echo json_encode($result);
	}

	function update()
	{
		$id = $this->input->post('id');
		$data['no_stok_opnam']=$this->input->post('no_stok_opnam');
		$data['nama_user']=$this->input->post('nama_user');
		$data['periode_stok_opnam']=$this->input->post('per_stok_opnam');
		$data['cttn']=$this->input->post('cttn');
		$data['tgl_trans']=$this->input->post('tgl_trans');
		$data['update_time'] = date('Y-m-d H:i:s');
		$data['create_by'] = $this->session->userdata('id');

		$result = $this->stok_opnam_model->updateData($id, $data);
		if ($result) {
			$output['status'] = 'success';
			$output['message'] = 'Data berhasil disimpan';
		} else {
			$output['status'] = 'error';
			$output['message'] = 'Data gagal disimpan';
		}

		echo json_encode($output);
	}
}
?>
<?php

Class Rko extends CI_Controller {   

    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('id')) redirect('login');
        $this->load->model('manajemenlogistik/rko_model');
        $this->load->model('laporan/stok_model');        
        $this->load->model('profil/setting_institusi_model');
        $this->profil = $this->setting_institusi_model->getDataInstitusi();
    }
    
    function index() 
    {
    	$data['user']=$this->session->userdata('username');
    	$data['history'] = $this->rko_model->get_history()->result();
		
		$this->load->view('manajemenlogistik/rko', $data);
    }
	
	function save()
	{
		// 
	}


	function show($year)
	{		
        $condition['year'] = $year;
		$data['year'] = $year;
		$data['twoYearsAgo'] = $year-2;
		$data['result'] = array();
		$data['base_url'] = base_url();
        $data['content'] = $this->rko_model->get_data_by($condition)->result();
		
		$this->load->view('manajemenlogistik/rko_list', $data);
	}

	  
	public function delete()
	{
        
    }
 	
 	public function generate()
 	{
 		$year = $this->input->post('tahun_anggaran');

        // generate stok akhir tahun
        $this->generate_yearly_stock($year-2);
        $saldo = $this->rko_model->get_data_stok_akhir($year-2);
        // get saldo $two_years_ago
        foreach ($saldo->result() as $key) {
            $remaining_stock[$key->id_fornas] = $key->stock; // column a
        }
        // get realisasion and avarage_used in two years ago
        $filter['awal'] = ($year-2).'-01-01';
        $filter['akhir'] = ($year-2).'-12-31';
        $data_two_years_ago = $this->stok_model->show_data_fornas($filter);
        foreach ($data_two_years_ago->result() as $item) {
            $average_used_two_years_ago[$item->id_fornas] = $item->pengeluaran; // column b
            $real_two_years_ago[$item->id_fornas] = $item->saldo_persediaan; // column g
            $needed[$item->id_fornas] = $item->pengeluaran * 18; // column c
            $price[$item->id_fornas] = $item->harga;
        }

 		$table = $this->rko_model->template();
 		$i = 0;
 		foreach ($table->result() as $key) {
            $remaining_stock_var = isset($remaining_stock[$key->id_fornas]) ? $remaining_stock[$key->id_fornas] : 0;
            $values[$i]['rko_id'] = $key->rko_id;
 			$values[$i]['fornas_id'] = $key->id_fornas;
 			$values[$i]['name'] = $key->nama_obat;
 			$values[$i]['year'] = $year;
            $values[$i]['stock_two_years_ago'] = $remaining_stock_var;
            $values[$i]['need'] = isset($needed[$key->id_fornas]) ? $needed[$key->id_fornas] : 0;
            $values[$i]['needed_plan'] = isset($needed[$key->id_fornas]) ? ($needed[$key->id_fornas] - $remaining_stock_var) : 0; // column d
            $values[$i]['real_two_years_ago'] = isset($real_two_years_ago[$key->id_fornas]) ? $real_two_years_ago[$key->id_fornas] : 0 ;
            $values[$i]['procurement_plan'] = 0; // $needed[$key->id_fornas] - $remaining_stock[$key->id_fornas]; // column d
            $values[$i]['average_used_two_years_ago'] = isset($average_used_two_years_ago[$key->id_fornas]) ? $average_used_two_years_ago[$key->id_fornas] : 0;
            $values[$i]['price'] = isset($price[$key->id_fornas]) ? $price[$key->id_fornas] : 0;
 			$i++;
 		}

 		$result = $this->rko_model->create($values);
 		if ($result) {
 			$output['status'] = "success";
 			$output['message'] = "Data berhasil dibuat";
 		} else {
 			$output['status'] = "error";
 			$output['message'] = "Data gagal dibuat";
 		}

 		echo json_encode($output);
 	}

    public function generate_yearly_stock($tahun)
    {        
        $check = $this->rko_model->get_data_stok_akhir($tahun);
        if ($check->num_rows() == 0) {
            // saldo stock $tahun-1 = get remaining stock in $tahun-1
            // calculate stock in $tahun (income-outcome in $tahun)
            // saldo stock $tahun = saldo stock $tahun-1 + saldo stock $tahun
            // insert into tb_stok_akhir_tahun

            // saldo stock $tahun-1 = get remaining stock in $tahun-1
            $variable = array();
            $last_year_stock = $this->rko_model->get_data_stok_akhir($tahun-1);
            // calculate stock in $tahun (income-outcome in $tahun)
            $filter['awal'] = $tahun.'-01-01';
            $filter['akhir'] = $tahun.'-12-31';
            $current_year = $this->stok_model->show_data_fornas($filter);
            if ($current_year->num_rows() > 0) {
                // compare quantity
                // if last year more than current year
                if ($last_year_stock->num_rows() > $current_year->num_rows()) {
                    // 
                    foreach ($current_year->result_array() as $key => $value) {
                        $variable[$value['id_fornas']] = $value['stok'];
                    }
                    $i=0;
                    foreach ($last_year_stock->result() as $key) {                    
                        // make data_array
                        $added_stock = (isset($variable[$key->id_fornas]) ? $variable[$key->id_fornas] : 0);
                        if ($key->id_fornas != '') {
                            $current_saldo = $key->stock + $added_stock;
                            $data_array[$i]['id_fornas'] = $key->id_fornas;
                            $data_array[$i]['stock'] = $current_saldo;
                            $data_array[$i]['tahun'] = $tahun;
                            $data_array[$i]['id_user'] = $this->session->userdata('id');
                            $i++;    
                        }
                    }
                } else {
                // if last year less than current year
                    if ($last_year_stock->num_rows() > 0 ) {
                        foreach ($last_year_stock->result_array() as $key => $value) {
                            $variable[$value['id_fornas']] = $value['stock'];
                        }
                        $i=0;
                        foreach ($current_year->result() as $key) {
                            $added_stock = (isset($variable[$key->id_fornas]) ? $variable[$key->id_fornas] : 0);
                            // make data_array
                            if ($key->id_fornas != '') {
                                $current_saldo = $key->stok + $added_stock;
                                $data_array[$i]['id_fornas'] = $key->id_fornas;
                                $data_array[$i]['stock'] = $current_saldo;
                                $data_array[$i]['tahun'] = $tahun;
                                $data_array[$i]['id_user'] = $this->session->userdata('id');
                                $i++;    
                            }
                        }
                    } else {
                        // if no data in lastyear or last year is 0
                        $i=0;
                        foreach ($current_year->result() as $key) {
                            // make data_array
                            if ($key->id_fornas != '') {
                                $data_array[$i]['id_fornas'] = $key->id_fornas;
                                $data_array[$i]['stock'] = $key->stok;
                                $data_array[$i]['tahun'] = $tahun;
                                $data_array[$i]['id_user'] = $this->session->userdata('id');
                                $i++;
                            }
                        }
                    }
                }
                $this->rko_model->insert_array($data_array);
            }
        }
    }

 	public function page_import()
 	{
 		$this->load->view();
 	}

 	public function remove($year='')
 	{
 		if ($year!='') {
 			$result = $this->rko_model->delete_by_year($year);
	 		if ($result) {
	 			$output['status'] = "success";
	 			$output['message'] = "Data berhasil dihapus";
	 		} else {
	 			$output['status'] = "error";
	 			$output['message'] = "Data gagal dihapus";
	 		}	
 		} else {
 			$output['status'] = "success";
	 		$output['message'] = "Data kosong";
 		}
 		

 		echo json_encode($output);
 	}

 	public function import()
 	{
 		// 
 	}

 	public function download($year)
 	{
 		$where['year'] = $year;
 		$twoYearsAgo = $year - 2;
 		$nextYear = $year + 1;
 		$query = $this->rko_model->get_data_by($where)->result();
        $profile = getProfile();
        $username = $profile['username_rko'];
        $password = $profile['password_rko'];

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINSI '.strtoupper($namakab['CPropDescr']);    
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }

 		//load our new PHPExcel library
        $this->load->library('excel');
        
        $start_row = 19;
        $header_row = $start_row - 2;
        $header_row_description = $start_row - 1;

        //activate worksheet number 1
        
        //JUDUL
        $styleArrayTitle = array(
            'font' => array(
                'bold' => true,
                'size' => 14,
            ),
            'alignment' => array(
                'horizontal' => 'center',
            ),
        );        
        
        $this->excel->getActiveSheet()->setCellValue('C12', 'RENCANA KEBUTUHAN OBAT '.$year);
        $this->excel->setActiveSheetIndex(0)->mergeCells('C12:L12');
        
        //INFO LOCATION
        
        $this->excel->getActiveSheet()->setCellValue('C13', 'SATUAN KERJA: '.$kabupaten);
        $this->excel->setActiveSheetIndex(0)->mergeCells('C13:L13');
        
        $this->excel->getActiveSheet()->getStyle('C12:C13')->applyFromArray($styleArrayTitle);

        //HEADER
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            ),
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'b8b9c7')
            ),
            'alignment' => array(
                'horizontal' => 'center',
            ),
            'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
        );

        $this->excel->getActiveSheet()->setCellValue('C'.$header_row, 'NO');
        // $this->excel->getActiveSheet()->setCellValue('D'.$header_row, 'KODE');
        $this->excel->getActiveSheet()->setCellValue('D'.$header_row, 'NAMA');
        $this->excel->getActiveSheet()->setCellValue('E'.$header_row, 'SATUAN');
        $this->excel->getActiveSheet()->setCellValue('F'.$header_row, 'SISA STOK PER 31 DES '.$twoYearsAgo);
        $this->excel->getActiveSheet()->getStyle('F'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
        $this->excel->getActiveSheet()->setCellValue('G'.$header_row, 'PEMAKAIAN RATA-RATA PER BULAN Th '.$twoYearsAgo);
        $this->excel->getActiveSheet()->getStyle('G'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('H'.$header_row, 'JUMLAH KEBUTUHAN Th '.$year);
        $this->excel->getActiveSheet()->getStyle('H'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('I'.$header_row, 'RENCANA KEBUTUHAN '.$year);
        $this->excel->getActiveSheet()->getStyle('I'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('J'.$header_row, 'RENCANA PENGADAAN '.$year);
        $this->excel->getActiveSheet()->getStyle('J'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('K'.$header_row, 'REALISASI PENGADAAN '.$twoYearsAgo);
        $this->excel->getActiveSheet()->getStyle('K'.$header_row)->getAlignment()->setWrapText(true);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(18);
        $this->excel->getActiveSheet()->setCellValue('L'.$header_row, 'KETERANGAN');
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
        
        $this->excel->getActiveSheet()->getStyle('C'.$header_row.':L'.$header_row)->applyFromArray($styleArrayHeader);

        $this->excel->getActiveSheet()->setCellValue('F'.$header_row_description, '(a)');
        $this->excel->getActiveSheet()->setCellValue('G'.$header_row_description, '(b)');
        $this->excel->getActiveSheet()->setCellValue('H'.$header_row_description, '(c) = (b) x 18');
        $this->excel->getActiveSheet()->setCellValue('I'.$header_row_description, '(d) = (c) - (a)');
        $this->excel->getActiveSheet()->setCellValue('J'.$header_row_description, '(e)');
        $this->excel->getActiveSheet()->setCellValue('K'.$header_row_description, '(f)');
        $this->excel->getActiveSheet()->setCellValue('L'.$header_row_description, '(g)');
        $this->excel->getActiveSheet()->getStyle('F'.$header_row_description.':L'.$header_row_description)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //konten tabel
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(53);
        
        $this->excel->getActiveSheet()->setCellValue('A1', md5($username.$password));
        for($i=0;$i<sizeof($query);$i++){
            $j= $i+$start_row;
            $number= $i+1;
            // kolom A untuk unik id rko
            // data dimulai dari baris 19
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $query[$i]->rko_id);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $number);
            // $this->excel->getActiveSheet()->setCellValue('D'.$j, strval($query[$i]->fornas_id));
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->name);
            $this->excel->getActiveSheet()->getStyle('D'.$j)->getAlignment()->setWrapText(true);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->unit);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->stock_two_years_ago);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->average_used_two_years_ago);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->need);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->needed_plan);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]->procurement_plan);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]->procurement_two_years_ago);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]->information);            
        }

        // hide kolom A & B
        $this->excel->getActiveSheet()->getColumnDimension('A')->setVisible(false);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setVisible(false);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setVisible(false);
        // hide row
        for($k=1; $k<11; $k++) {
            $this->excel->getActiveSheet()->getRowDimension($k)->setVisible(false);    
        }

        $this->excel->getActiveSheet()->getStyle('C'.$header_row_description.':L'.$j)->applyFromArray($styleArray);
        $this->excel->getActiveSheet()->freezePane('C'.$header_row_description);
        
        $filename='template_rko.xlsx'; //save our workbook as this file name
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        // header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007'); 

        $objWriter->save('php://output');
 	}

    public function update_item($id)
    {
        $data = $this->input->post('dt');
        $data['need'] = $data['average_used_two_years_ago'] * 18;
        $data['needed_plan'] = $data['need'] - $data['stock_two_years_ago'];
        $data['procurement_plan'] = $data['needed_plan'];

        $result = $this->rko_model->update($id, $data);
        if ($result) {
            $output['status'] = "success";
            $output['message'] = "Data berhasil diupdate";
            $output['data'] = $data;
        } else {
            $output['status'] = "error";
            $output['message'] = "Data gagal diupdate";
            $output['data'] = $data;
        }

        echo json_encode($output);
    }

    public function delete_item($id)
    {
        $result = $this->rko_model->delete($id);
        if ($result) {
            $output['status'] = "success";
            $output['message'] = "Data berhasil dihapus";
        } else {
            $output['status'] = "error";
            $output['message'] = "Data gagal dihapus";
        }

        echo json_encode($output);
    }

    public function send($year)
    {
        $profile = getProfile();
        $information = "from E-logistics";

        $area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];

        $username = $profile['username_rko'];
        $password = $profile['password_rko'];
        $content_type = 'application/json';
        $header_rko = array(
                            'TAHUN' => $year,
                            'KETERANGAN' => $information,
                            'SISIPAN' => 'N',
                        );

        $detail_content_type = 'application/json';

        $url = 'http://'.$profile['url_rko'];
        $method = 'POST';

        // how to check curl is active
        $send = false;
        if(in_array('curl', get_loaded_extensions())) {
            // "CURL is available on your web server";
            $send = true;
        }

        if ($send) {
            // send header
            $ch = curl_init( $url.'/front/services/HdrRKO' );        
            curl_setopt( $ch, CURLOPT_USERPWD, "$username:$password");
            // curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:'.$content_type));
            curl_setopt($ch, CURLOPT_POST, 1);
            // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
            curl_setopt( $ch, CURLOPT_POSTFIELDS, $header_rko );        
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            # Send request.
            $response = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($response);
            // print_r($result); die();
            if (empty($result)) {
                $output['status'] = 'error';
                $output['message'] = 'Wrong username or password';
            } else {
                $rko_id = $result->val;
            
                // return header id
                if ((!empty($rko_id)) || ($rko_id > 0)) {
                    $where['year'] = $year;
                    $rko = $this->rko_model->custom_select_by($where);
                    $detail_rko = array(
                        'HDR' => array(
                            'RKO_ID' => $rko_id,
                        ),
                        'DTL' => $rko->result()
                    );

                    $payload = json_encode($detail_rko);

                    // post detail
                    $ch = curl_init( $url.'/front/services/DtlRKO');
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);        
                    curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );        
                    curl_setopt( $ch, CURLOPT_USERPWD, "$username:$password");
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type: '.$detail_content_type));            
                    
                    # Send request.
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $hasil = json_decode($response);

                    if ($hasil->msg == 'success') {
                        $output['status'] = 'success';
                        $output['message'] = $hasil->val;
                    } else {
                        $output['status'] = 'error';
                        $output['message'] = 'Failed send detail RKO';
                    }
                } else {
                    $output['status'] = 'error';
                    $output['message'] = 'Failed send header RKO';
                }
            }
        } else {
            $output['status'] = 'error';
            $output['message'] = 'CURL is not available on your web server';
        }
        

        echo json_encode($output);
    }

    function integrate($year = '')
    {
        $accountBankData = getProfile();
        $ret = array();
        $timenow = timeNow();
        $tmptime = splitDate($timenow," ");        
        $metode_kirim = "online";      
        $tahun = $year;
        $jenis_data = "rkoh";
        if($this->profil['id_kab']==""){
            $wil = $this->profil['id_prop'].'00';
            $get = $this->setting_institusi_model->getNamaProp($this->profil['id_prop']);
            $namawil = str_replace(" ","",$get['CPropDescr']);
        }else{
            $wil = $this->profil['id_kab'];
            $get = $this->setting_institusi_model->getNamaKab($wil);
            $namawil = str_replace(" ","",$get['CKabDescr']);  
        }

        $ret['url'] = "-";
        $hasil = $this->rko_model->get_data_by(array( 'year' => $tahun ));
        $log = $this->log_save('berhasil',$timenow,'',$tahun,$jenis_data,$wil,"sinkron");
        $logdata = "";
        $gel3 = "";
        $gel = "";

        if($metode_kirim == "online")
        {
            foreach($hasil->result() as $key => $value)
            {              
              $gel .= $value->fornas_id.'|';
              $gel .= $value->stock_two_years_ago.'|';
              $gel .= $value->average_used_two_years_ago.'|';
              $gel .= $value->need.'|';
              $gel .= $value->needed_plan.'|';              
              $gel .= $value->procurement_plan.'|';
              // $gel .= $value->procurement_two_years_ago.'|';
              $gel .= $value->information.'|';
              $gel .= $value->rko_id . '|';
              $gel .= $value->real_two_years_ago.'|';
              $gel .= $value->price. "\n";              
            }

            foreach($log as $key => $value)
            {
              $logdata .= $log['id'] . '|';
              $logdata .= $log['periode'] . '|';
              $logdata .= $log['cara_kirim'].'|';
              $logdata .= $log['tglsistem'].'|';
              $logdata .= $log['status'].'|';
              $logdata .= $log['namatable'].'|';
              $logdata .= $log['id_user']."\n";
            }
            require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
            $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
            $client->soap_defencoding = 'UTF-8';
            $params = array(
                'username'          => base64_encode($accountBankData['username']),
                'password'          => base64_encode($accountBankData['password']),
                'tahun'             => $tahun,
                'bulan'         => '',
                'jenis_data'    => $jenis_data,
                'wil'           => $wil,
                'data'            => $gel,
                'logkirim'      => $logdata
            );
            $result = $client->call('Receive', $params);
            if ($client->fault)
            {
              $ret['error'] = "failed";
              $ret['msg'] = "The request contains an invalid SOAP body";
            }elseif($result!='success'){
              $this->log_save('gagal',$timenow,'',$tahun,$jenis_data,$wil);
              $ret['error'] = "failed";
              $ret['msg'] = $result;
            }else{
                $err = $client->getError();
                if ($err) {
                    $this->log_save('gagal',$timenow,'',$tahun,$jenis_data,$wil);
                    $ret['error'] = "failed";
                    $ret['msg'] = $err;
                } else {
                    $this->log_save('berhasil',$timenow,'',$tahun,$jenis_data,$wil);
                    $ret['error'] = "success";
                    $ret['msg'] = $result;
                }
            }
        }

        
        echo json_encode($ret);
    }

    function log_save($status,$timenow,$bulan,$tahun,$jenis_data,$wil,$mode="")
    {
       $data['periode'] = $tahun;
       $data['cara_kirim'] = 'online';
       $data['tglsistem'] = $timenow;
       $data['status'] = $status;
       $data['namatable'] = $jenis_data."_".$tahun."_".$wil;
       $data['id_user'] = $this->session->userdata('id');
       if($mode=="sinkron")
       {
         $data['id'] = date('d-m-Y')."/".date('H:i:s');
         $data['id_user'] = $this->session->userdata('username');

         return $data;
       } else {
         $h = $this->rko_model->insert_log($data);

         return $h;
       }
    }
}

?>
<?php

Class Lplpo_kel extends CI_Controller {
    
    var $title = 'lPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->helper('file');
        $this->load->model('manajemenlogistik/lplpo_kel_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->lplpo_kel_model->getDataKecamatan();

			//print_r($data);
			$this->load->view('manajemenlogistik/lplpo_kel',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }

    function get_data_puskesmas($idpuskesmas) {
        $data = $this->lplpo_kel_model->GetComboPuskesmas($idpuskesmas);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }
	
	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    
    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);
        
        print json_encode($query);
    }

    function update_data_import(){
        //$data['kode_kec']=$this->input->post('kode_kec');
        $kode_pusk=$this->input->post('kode_pusk');
        $periode=$this->input->post('per_lplpo');

        $per=$periode.'-00';
        $id_lplpo=$this->db->query("
            select id
            from tb_lplpo
            where kode_pusk='$kode_pusk' and periode='$per'
            ");
        if($id_lplpo->num_rows > 0){
            $message['confirm']='Data telah dipilih';
            $id=$id_lplpo->row_array();            
            $data['new_id_lplpo']=$id['id'];            
            $this->session->set_userdata($data);
        }else{
            $message['confirm']='Data belum tersedia';
        }
        echo json_encode($message);
        
    }

	function getIdlplpo(){
        $data['kode_kec']=$this->input->post('kode_kec');
        $data['kode_pusk']=$this->input->post('kode_pusk');
        $data['per_lplpo']=$this->input->post('per_lplpo');
        
        //cek
        //if (true)
        //apakah mau update?
        $query=$this->lplpo_kel_model->input_data_m($data);
        if($query){
            $message['confirm']='Sukses Input Data';
            echo json_encode($message);
        }else{
            $message['confirm']='Data sudah tersedia';
            echo json_encode($message);
            die;    
        }
        

        //$set['kec']=$this->input->post('kec');
        $periode=$data['per_lplpo'];
        $kode_pusk=$data['kode_pusk'];
        $per=$periode.'-00';
        $id_lplpo=$this->db->query("
            select id
            from tb_lplpo
            where kode_pusk='$kode_pusk' and periode='$per'
            ");
        $id=$id_lplpo->row_array();
        
        //die;
        $data['new_id_lplpo']=$id['id'];
        //print_r($new_id_lplpo);
        $this->session->set_userdata($data);

    }

    function search_data_by(){
        $set['kec']=$this->input->post('kec');
        $set['per']=$this->input->post('per');
        $set['nama_pusk']=$this->input->post('nama_pusk');
        //input database

        $this->session->set_userdata($set);
        $data['links']="";
        $data['result']=$this->lplpo_kel_model->show_data($set);
        //$data['kec']=$this->input->post('kec');
        //$data['per']=$this->input->post('per');
        //$data['nama_pusk']=$this->input->post('nama_pusk');

        //print_r($set);
        //$this->session->set_userdata($set);

        $this->load->view('manajemenlogistik/lplpo_kel_ex_list',$data);

        //$this->printOut($set);
    }

    function printOut(){
        //$query=$this->lplpo_kel_model->getDataEksport();
        $set['kec']=$this->session->userdata('kec');
        $set['per']=$this->session->userdata('per');
        $set['nama_pusk']=$this->session->userdata('nama_pusk');

        $query=$this->lplpo_kel_model->show_data($set);
        //print_r($query[0]->id);
        $profil=$this->lplpo_kel_model->getProfilPrint($set['nama_pusk']);
        
        //print_r($profil);
        $formattahun=strtotime($set['per']);
        $tahun=date('Y',$formattahun);
        $bulan=date('F',$formattahun);
        //echo $bulan.', '.$tahun;
        //die;
        
        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert
        
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');
        
        //JUDUL
        
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(lPLPO)');
        
        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A4', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C4', $profil['nama_pusk']);
        $this->excel->getActiveSheet()->setCellValue('A5', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C5', $profil['nama_kec']);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C6', $profil['nama_kab']);
        $this->excel->getActiveSheet()->setCellValue('A7', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C7', $profil['nama_prop']);
        $this->excel->getActiveSheet()->setCellValue('G4', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H4', $bulan);
        $this->excel->getActiveSheet()->setCellValue('G5', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H5', $tahun);

        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        //$this->excel->getActiveSheet()->setCellValue('B9', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('J9', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('K9', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('L9', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('M9', 'Keterangan');
        
        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            //$this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->nama_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->sediaan);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->stok_awal);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->terima);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->sedia);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->pakai);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->rusak_ed);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->stok_akhir);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]->stok_opt);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]->permintaan);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $query[$i]->ket);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename=$set['nama_pusk'].'-lplpo-'.$set['per'].'.csv'; //save our workbook as this file name
        //header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007'); 
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV'); 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
        //session destroy
        //$this->session->sess_destroy();
    }
    
    function list_kosong(){
        $data['result']=$this->lplpo_kel_model->getlistempty();
        $this->load->view('manajemenlogistik/list_lplpo_kosong',$data);
    }

    function list_kosong_by($key){
        $data['result']=$this->lplpo_kel_model->getlistemptyby($key);
        $this->load->view('manajemenlogistik/list_lplpo_kosong',$data);
    }

    function do_import(){
        //$set['kec']=$this->session->userdata('id_lplpo');
        $id_lplpo=$this->session->userdata('new_id_lplpo');
        //$this->session->sess_destroy();
        //print_r($id_lplpo);
        //die;
        $config['upload_path'] = './temp_upload/';
        $config['allowed_types'] = 'csv';
        //$file_name = $this->input->post('file_lplpo');
        //print_r($_FILES);
        $file_name = $_FILES['file_lplpo']['name'];
        //print_r($file_name);
        //print_r('file_lplpo');
        //die;                
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_lplpo'))
        {
            $error = array('error' => $this->upload->display_errors());
            //echo "error upload";
            print_r($error);
            //$this->load->view('upload_form', $error);
        }
        else
        {
            //$data = array('upload_data' => $this->upload->data());
            //echo "success upload";
            //$this->load->view('upload_success', $data);
            $upload_data = $this->upload->data();
            $file =  $upload_data['full_path'];
        }

        //echo "<br>".$file_name."<br>";
        //delete_files($upload_data['file_path']);
        //die;
        $this->load->library('excel');

        $objReader = PHPExcel_IOFactory::createReader('CSV');
        
        //Set to read only
        $objReader->setReadDataOnly(true);

        $objPHPExcel=$objReader->load('./temp_upload/'.$file_name);
        $objWorksheet=$objPHPExcel->getSheet(0);

        $highestRow = $objWorksheet->getHighestRow();
        if($this->lplpo_kel_model->cek_eksis($id_lplpo)){
            //update
            //echo 'update';
            for($i=10;$i<=$highestRow;$i++)
              {
                    $this->db->query("
                        UPDATE `tb_detail_lplpo_kel`
                        SET
                        `nama_obj`=?,
                        `stok_awal`=?,
                        `terima`=?,
                        `sedia`=?,
                        `pakai`=?,
                        `rusak_ed`=?,
                        `stok_akhir`=?,
                        `stok_opt`=?,
                        `permintaan`=?,
                        `pemberian`=?,
                        `ket`=?
                        WHERE
                        `id_lplpo`=? and
                        `kode_obat`=?
                        ",
                        array(
                            //$objWorksheet->getCellByColumnAndRow(1,$i)->getValue(),
                            
                            $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(),
                            //$objWorksheet->getCellByColumnAndRow(4,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(4,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(6,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(7,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(8,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(9,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(10,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(11,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(12,$i)->getValue(),
                            $objWorksheet->getCellByColumnAndRow(13,$i)->getValue(),
                            $id_lplpo,
                            $objWorksheet->getCellByColumnAndRow(1,$i)->getValue()

                            )
                    );
              }
              //end for row 
        }else{
            //echo 'insert new';

            //loop from first data untill last data
        
          for($i=10;$i<=$highestRow;$i++)
          {
                $this->db->query("
                    INSERT INTO `tb_detail_lplpo_kel` (
                    `id_lplpo`,
                    `kode_generik`,
                    `nama_obj`,
                    `sediaan`,
                    `stok_awal`,
                    `terima`,
                    `sedia`,
                    `pakai`,
                    `rusak_ed`,
                    `stok_akhir`,
                    `stok_opt`,
                    `permintaan`,
                    `pemberian`,                   
                    `ket`
                    ) VALUES (
                        ?,?,?,?,?,?,?,?,?,?,?,?,?,?
                    )",
                    array(
                        //$objWorksheet->getCellByColumnAndRow(1,$i)->getValue(),
                        $id_lplpo,
                        $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(4,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(6,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(7,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(8,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(9,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(10,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(11,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(12,$i)->getValue(),
                        $objWorksheet->getCellByColumnAndRow(13,$i)->getValue(),

                        )
                );
          }//end for row
          
        }

        
          /*
          for($i=10;$i<=10;$i++)
          {
              for($j=0;$j<14;$j++){
                echo $konten[$j][$i]."\t";//= $objWorksheet->getCellByColumnAndRow($j,$i)->getValue();
              }
              echo "<br>";
          }*/  
        //die;
        delete_files($upload_data['file_path']);
        //die;    
        //$this->load->view('hasil', $data);
    }
}
<?php

Class Pemusnahan_Obat extends CI_Controller {

    var $title = 'Pemusnahan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/pemusnahan_obat_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['result2']=$this->pemusnahan_obat_model->getLog();
			$data['result']=$this->pemusnahan_obat_model->getData();
			$this->load->view('manajemenlogistik/pemusnahan_obat',$data);
		}else{
			redirect('login');
		}

    }

    function obat_rusak(){
    	$data['base_url']=$this->url;
		$data['result']=$this->pemusnahan_obat_model->getDataRusak();
		$this->load->view('manajemenlogistik/pemusnahan_obat_rusak',$data);

    }

	function stok_opnam_rusak(){
    	$data['base_url']=$this->url;
		//$data['result']=$this->pemusnahan_obat_model->getDataStokRusak();
		$this->load->view('manajemenlogistik/stok_opnam_rusak',$data);

    }

    function get_obat_rusak(){
    	$data['base_url']=$this->url;
		$data['result']=$this->pemusnahan_obat_model->getDataStokRusak();
		$this->load->view('manajemenlogistik/stok_opnam_rusak_list',$data);

    }

    function update_stok_opnam_rusak(){
    	$this->db->query("
    		update tb_detail_pemusnahan
    		set status=0
    		where jenis_rusak=2 and status=1
    		");
    }

    function get_list_obat2() {
        $key=$this->input->get("term");
        //var $term;
        $query = $this->pemusnahan_obat_model->GetListObatBeri($key);
        print json_encode($query);
    }

	function input_data(){
		$data = array();
		$data['cur_date']=$this->input->post('cur_date');
		$data['no_dok']=$this->input->post('no_dok');
		$data['saksi1']=$this->input->post('saksi1');
		$data['nip1']=$this->input->post('nip1');
		$data['saksi2']=$this->input->post('saksi2');
		$data['nip2']=$this->input->post('nip2');

		$id=$this->pemusnahan_obat_model->input_data_m($data);
        if ($id != '') {
            $output['status'] = "success";
            $output['message'] = "Data berhasil disimpan. Silakan pilih obat yang akan dihapus. Kemudian klik tombol 'Hapus'";
            $output['id_tb_pemusnahan'] = $id;
        } else {
            $output['status'] = "error";
            $output['message'] = "Data gagal disimpan.";
            $output['id_tb_pemusnahan'] = 0;
        }

        echo json_encode($output);
	}


	function get_data_Pemusnahan_obat(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/pemusnahan_obat/get_data_Pemusnahan_obat';
		$config['total_rows'] = $this->pemusnahan_obat_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->pemusnahan_obat_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/pemusnahan_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}


	function delete_list() {
        $kode=$this->input->post('kode');
        //$this->pemusnahan_obat_model->deleteData($kode);
        $this->pemusnahan_obat_model->pemusnahandata($kode);
    }

 	function get_list_pbf() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->pemusnahan_obat_model->GetListPbf($key);
        print json_encode($query);
    }

    function input_obat_rusak(){
    	$data = array();
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['no_batch']=$this->input->post('no_batch');
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['jml_rusak']=$this->input->post('jumlah');
        $data['id_stok']=$this->input->post('id_stok');
		//kurang id_tb_pemusnahan
		$query=$this->pemusnahan_obat_model->insertStokRusak($data);
    }

    function input_detail_pemusnahan(){
    	$arr_check=$this->input->post('chk');
    	$arr_kode_obat=$this->input->post('kode_obat');
		$arr_no_faktur=$this->input->post('no_faktur');
		$arr_no_batch=$this->input->post('no_batch');
		$arr_jumlah=$this->input->post('jml_expired');
		$arr_id_stok=$this->input->post('id_stok');
		$last_id=$this->input->post('id_tb_pemusnahan');

		for ($i=0; $i < sizeof($arr_check) ; $i++) {
			for ($j=0; $j < sizeof($arr_id_stok) ; $j++) {
				if ($arr_id_stok[$j]==$arr_check[$i]) {
					$this->db->query("
					insert INTO tb_detail_pemusnahan(
						id_tb_pemusnahan,
						nomor_faktur,
						no_batch,
						kode_obat,
						jumlah,
						jenis_rusak,
						status,
                        id_stok
					)
					value(?,?,?,?,?,3,0,?)
					",array(
						$last_id,
						$arr_no_faktur[$j],
						$arr_no_batch[$j],
						$arr_kode_obat[$j],
						$arr_jumlah[$j],
                        $arr_id_stok[$j])
					);

                    $this->db->query("
                        UPDATE tb_stok_obat
                        set flag=0,
                        stok= (stok-?)
                        WHERE
                        id_stok = ?",
                        array($arr_jumlah[$j], $arr_id_stok[$j])
                    );
				}
			}
		}
	}

	function update_detail_pemusnahan(){
    	$arr_kode_obat=$this->input->post('kode_obat');
		$arr_no_faktur=$this->input->post('no_faktur');
		$arr_no_batch=$this->input->post('no_batch');
		$arr_jumlah=$this->input->post('jml_rusak');
		$arr_chk=$this->input->post('chk');
		$arr_id_dp=$this->input->post('id_dp');
        $arr_idstok=$this->input->post('id_stok');

		$getID=$this->db->query("
			SELECT id FROM tb_pemusnahan ORDER BY id DESC LIMIT 1
			");
		if ($getID->num_rows() > 0)
			{
				$row=$getID->row();
				$last_id=$row->id;
			}
		//print_r($kode);
		//$last_id=$getID['id'];
			$status=0;
		for ($i=0; $i < sizeof($arr_chk) ; $i++) {
			for ($j=0; $j < sizeof($arr_id_dp) ; $j++) {
				if($arr_chk[$i]==$arr_id_dp[$j]){
					$this->db->query("
					update tb_detail_pemusnahan
					set
						id_tb_pemusnahan=?,
						status=?
					where
						nomor_faktur=? and
						no_batch=? and
						kode_obat=? and
                        id_stok=?
					",array(
						$last_id,$status,
						$arr_no_faktur[$j],
						$arr_no_batch[$j],
						$arr_kode_obat[$j],
                        $arr_idstok[$j])
					);

					$upt = $this->db->query("
			            update tb_stok_obat
			            set stok= (stok-?)
			            where id_stok = ?
				        ",
				        array($arr_jumlah[$j],
								$arr_idstok[$j])
			        );
				}
			}
		}
	}

	function printOut(){

        $query=$this->pemusnahan_obat_model->getDataEksport();
        //$profil=$this->distribusi_obat_model->getDataKab($id_lplpo);
        $profil=$this->pemusnahan_obat_model->getDataInstitusi();
        if($profil['levelinstitusi']==3){
            $level = 'KABUPATEN '.$profil['CKabDescr'];
            $t4_surat = $profil['CKabDescr'];
        }elseif($profil['levelinstitusi']==2){
        	$level = 'PROVINSI '.$profil['CPropDescr'];
        	$t4_surat = $profil['CPropDescr'];
        }else{
            $level = 'KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $t4_surat ='Jakarta';
        }
        //$kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($profil['nama_kab']);
        //fungsi konvert
        //print_r($level);die();
       //}

       //function test(){
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:J1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan_obat_dimusnahkan');

        //JUDUL
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );


        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMUSNAHAN SEDIAAN FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A2:J2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI'.$level);

        $this->excel->getActiveSheet()->mergeCells('A3:J3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', 'No:...');

        //INFO LOCATION
        //$this->excel->getActiveSheet()->setCellValue('A5', 'Nomor Dokumen');
        //    $this->excel->getActiveSheet()->setCellValue('C5', ': ');
        //$this->excel->getActiveSheet()->setCellValue('A6', 'Unit Penerima');
        //    $this->excel->getActiveSheet()->setCellValue('C6', ': '.$profil['nama_pusk']);
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );

        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Tanggal');
        $this->excel->getActiveSheet()->setCellValue('C9', 'No. Faktur');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Nama Sediaan');
        $this->excel->getActiveSheet()->setCellValue('E9', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Jumlah');
        $this->excel->getActiveSheet()->setCellValue('G9', 'PBF');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Saksi1');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Saksi2');
        $this->excel->getActiveSheet()->setCellValue('J9', 'Keterangan');

        //konten tabel
        //$totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+6;
            $n=$j-5;
            //$total=$query[$i]->pemberian*$query[$i]->harga;
            //$totalharga=$totalharga+$total;
            //set cell A1 content with some text
            if($query[$i]->jenis_rusak==1){
            	$keterangan="Penerimaan";
			}elseif($query[$i]->jenis_rusak==2){
				$keterangan="Stok Opnam";
			}elseif($query[$i]->jenis_rusak==3){
				$keterangan="Expired";
			}
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->tanggal);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nomor_faktur);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->object_name);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->no_batch);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->jumlah);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->pbf);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->saksi1);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->saksi2);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $keterangan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $this->excel->getActiveSheet()->getStyle('A5:J5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:J'.$j)->applyFromArray($styleArray);
        unset($styleArray);
        //$tanggalsbbk=$profil['nama_kab'].', '.$profil['tanggal'];
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);
        $tanggal = date('d-m-Y');
        $this->excel->getActiveSheet()->mergeCells('H'.($j+4).':J'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+4), $t4_surat.', '.$tanggal);
        //$this->excel->getActiveSheet()->mergeCells('E'.($j+5).':'.'H'.($j+5));
        //$this->excel->getActiveSheet()->setCellValue('E'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('H'.($j+10).':'.'J'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('H'.($j+10), '(.........................)');

        //$this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        //$this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        //$this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        //$this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        //$filename=$profil['nama_pusk'].'-SBBK-'.$profil['tanggal'].'.xlsx'; //save our workbook as this file name
        $filename='list_pemusnahan.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5'); //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function update_data_detail($id){
  		$data['jumlah'] = $this->input->post('qty');

      $stok_id = $this->input->post('stok_id');
      $stok['flag'] = 1;

  		$result = $this->pemusnahan_obat_model->update_qty($id, $data);
      if ($id != '') {
          $this->db->where('id_stok', $stok_id);
          $this->db->update('tb_stok_obat', $stok);

          $output['status'] = "success";
          $output['message'] = "Data berhasil disimpan.";
          $output['value'] = $data['jumlah'];
      } else {
          $output['status'] = "error";
          $output['message'] = "Data gagal disimpan.";
          $output['value'] = 0;
      }

      echo json_encode($output);
  	}
}
?>

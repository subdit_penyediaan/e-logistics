<?php

Class Detail_stok_opnam extends CI_Controller {

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/detail_stok_opnam_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/detail_stok_opnam',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
        $arr_selisih=0;
        $id_so=$this->input->post('id_stok_opnam');
		$arr_kode_obat = $this->input->post('kode_obat');
        $arr_noted_stok = $this->input->post('noted_stok');
        $arr_fisik_stok = $this->input->post('stok_fisik');
        $arr_ket = $this->input->post('ket');
        $id_stok_obat = $this->input->post('id_stok_obat');
        $continue=true;
        for($k=0;$k<sizeof($arr_noted_stok);$k++){
            if($arr_fisik_stok[$k]==""){
                $continue=false;$n=$k+1;
                $message['confirm']='Stok Fisik No.'.$n.' belum diisi';
                $message['con']=0;
                echo json_encode($message);
                die;
            }
        }
        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_noted_stok!='') {
                //$xarr_treatment_id[] = $arr_treatment_id[$i];
                $arr_selisih=$arr_noted_stok[$i]-$arr_fisik_stok[$i];
                $ins = $this->db->query("
                    INSERT INTO `tb_detail_stok_opnam` (
                        `id_so`,
                        `kode_obat`,
                        `noted_stok`,
                        `fisik_stok`,
                        `selisih`,
                        `ket`)
                    VALUES (?,?,?,?,?,?)
                ",
                array($id_so,
                        $arr_kode_obat[$i],
                        $arr_noted_stok[$i],
                        $arr_fisik_stok[$i],
                        $arr_selisih,
                        $arr_ket[$i])
                );
                //echo $this->db->last_query();
                $upt = $this->db->query("
                    update tb_stok_obat
                    set stok=?
                    where id_stok = ?
                ",
                array($arr_fisik_stok[$i],
                        $id_stok_obat[$i])
                );
            }
        }

        $this->detail_stok_opnam_model->updateStatus($this->input->post('id_stok_opnam'));
        $message['confirm']='Sukses input data';
        $message['con']=1;
        $message['id_so']=$id_so;
        echo json_encode($message);
	}


	function get_data_detail_stok($key){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/detail_stok_opnam/get_data_detail_stok';
		$config['total_rows'] = $this->detail_stok_opnam_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 7; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

        $data['saved']=$this->detail_stok_opnam_model->getDataSaved($key);
        if (empty($data['saved'])){
    		$data['result']=$this->detail_stok_opnam_model->getData($config['per_page'],$this->uri->segment(4));
            $data['links'] = $this->pagination->create_links();
            $data['detailstok']=$key;
            $this->load->view('manajemenlogistik/detail_stok_opnam_list',$data);
        }else{
            $this->load->view('manajemenlogistik/detail_stok_opnam_list_saved',$data);
        }


	}


    function get_detail($nomorstok){
		$data['detailstok']=$this->detail_stok_opnam_model->getDataDetailStok($nomorstok);
        $key=array(
            'so.flag'=>1,
            'dso.id_stok_opname'=>$nomorstok,
            );
        $this->db->select('*');
        $this->db->from('tb_detail_stok_opnam dso');
        $this->db->join('tb_stok_obat so','so.id_stok=dso.id_so');
        $this->db->where($key);
        // $this->db->where('so.stok <>', 0); // tidak bisa di hilangkan
        $data['liststok']= $this->db->get();
		$this->load->view('manajemenlogistik/detail_stok_opnam',$data);
     }

    function edit_list(){
        $data=$this->input->post('up');
        $noted_stok=$this->input->post('notedstok');
        $idstok=$this->input->post('idstok');
        $data['last_update']=date('Y-m-d H:i:s');
        $data['selisih']=$data['fisik_stok']-$noted_stok;
        $data['updated_by']=$this->session->userdata('id');

        //harga yang diubah pada menu stok opnam tidak merubah harga perolehan

        $this->db->trans_start();
            $this->db->where('id',$data['id']);
            $this->db->update('tb_detail_stok_opnam',$data);
            $newstok = array('stok'=>$data['fisik_stok'], 'harga'=>$data['harga_so']);
            // $newstok = array('harga'=>$data['harga_so']);
            $this->db->where('id_stok',$idstok);
            $this->db->update('tb_stok_obat',$newstok);
        $result = $this->db->trans_complete();

        if ($result) {
            $output['message'] = "Data berhasil disimpan";
            $output['status'] = "success";
            $output['dist'] = abs($data['selisih']);
        } else {
            $output['message'] = "Data gagal disimpan";
            $output['status'] = "failed";
            $output['dist'] = null;
        }

        echo json_encode($output);
    }

    function update($id) {
        $data = $this->input->post('data');
        $batch_old = $this->input->post('batch_old');
        $batch_new = $this->input->post('batch_new');
        $data['update_time'] = date('Y-m-d H:i:s');
        $data['update_by'] = $this->session->userdata('id');


        if ($batch_new != $batch_old) {
            $data['no_batch'] = $batch_new;
            // add history
            $batch_array['batch_old'] = $batch_old;
            $batch_array['batch_new'] = $batch_new;
            $batch_array['id_stok'] = $id;
            $batch_array['update_by'] = $this->session->userdata('id');

            $this->db->insert('tb_batch_history', $batch_array);
        }

        $this->db->where('id_stok', $id);
        $result = $this->db->update('tb_stok_obat', $data);

        // $this->db->where('id', $id);
        // $this->db->update('tb_detail_faktur', $data);

        if ($result) {
            $output['text'] = "Data berhasil disimpan";
            $output['status'] = "success";
            $output['batch_old'] = $batch_old;
            $output['batch_new'] = $batch_new;
            $output['id_stok'] = $id;
        } else {
            $output['text'] = "Data gagal disimpan";
            $output['status'] = "failed";
        }

        echo json_encode($output);
    }

    function printOut($id_stok_opnam){
        $key_array = array('id_stok_opname'=>$id_stok_opnam);
        $query=$this->detail_stok_opnam_model->getDataByWhere($key_array)->result_array();
        //print_r($query);die();

        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINIS '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');

        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );

        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN STOK OPNAM');

        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A3:I3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //header tabel
        $this->excel->getActiveSheet()->setCellValue('A5', 'No.');
        $this->excel->getActiveSheet()->setCellValue('B5', 'Tanggal');
        //$this->excel->getActiveSheet()->setCellValue('C5', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C5', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('D5', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('E5', 'Stok Tercatat');
        $this->excel->getActiveSheet()->setCellValue('F5', 'Stok Fisik');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Harga');
        $this->excel->getActiveSheet()->setCellValue('H5', 'Nilai Stok');
        $this->excel->getActiveSheet()->setCellValue('I5', 'Keterangan');
        //konten tabel
        $total_aset=0;$j=5;
        if(sizeof($query)>0){
            for($i=0;$i<sizeof($query);$i++){
                //echo $query[$i]['nama_obat']."<br>";
                $j=$i+6;
                $n=$j-5;
                $nilai_stok = $query[$i]['fisik_stok']*$query[$i]['harga_so'];
                $total_aset=$total_aset+$nilai_stok;
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
                $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]['last_update']);
                //$this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['kode_obat']);
                $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]['no_batch']);
                $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]['nama_obj']);
                $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]['noted_stok']);
                $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]['fisik_stok']);
                $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]['harga_so']);
                $this->excel->getActiveSheet()->setCellValue('H'.$j, $nilai_stok);
                $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]['ket']);
                /*$this->excel->getActiveSheet()->setCellValue('I'.$j, "-");
                $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]['harga']);*/
                //change the font size
                //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
                //make the font become bold
                //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
                //merge cell A1 until D1
                //$this->excel->getActiveSheet()->mergeCells('A1:D1');
                //set aligment to center for that merged cell (A1 to D1)
                //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }

        $this->excel->getActiveSheet()->getStyle('A5:I5')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A5:I'.$j)->applyFromArray($styleArray);

        $filename='Laporan_stok_opname.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function history($id)
    {
        $this->db->where('id_stok', $id);
        $this->db->order_by('create_time');
        $result = $this->db->get('tb_batch_history');
        if ($result->num_rows() > 0) {
            $html = '<table border="1"><tr><th style="padding: 10px;">No. Batch Lama</th><th style="padding: 10px;">Tanggal berubah</th></tr>';
            foreach ($result->result() as $key) {
                $html .= '<tr><td>'.$key->batch_old.'</td><td>'.$key->create_time.'</td></tr>';
            }
            $html .='</table>';
        } else {
            $html = "<i>belum ada history</i>";
        }


        echo $html;
    }
}
?>

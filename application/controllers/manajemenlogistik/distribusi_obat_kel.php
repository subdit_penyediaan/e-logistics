<?php

Class Distribusi_obat_kel extends CI_Controller {

    //var $title = 'Distribusi Obat';

    function __construct() {
        parent::__construct();
        if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/distribusi_obat_kel_model');
    }

    function index() {
      	if($this->session->userdata('login')==TRUE){
    		$data['user']=$this->session->userdata('username');
    		$data['base_url']=$this->url;

              $data['cito']=$this->distribusi_obat_kel_model->getDataCito();
              $profil=$this->distribusi_obat_kel_model->getDataInstitusi();
              if($profil['levelinstitusi']==3){
                  $data['kecamatan']=$this->distribusi_obat_kel_model->getDataKecamatan();
                  $data['ux'] = $this->db->where('status', 1)->get('tb_unitexternal')->result_array();
                  $this->load->view('manajemenlogistik/distribusi_obat_kel',$data);
              }elseif($profil['levelinstitusi']==1){
                  $data['propinsi']=$this->distribusi_obat_kel_model->getPropinsi();
                  $this->load->view('manajemenlogistik/distribusi_obat_pusat',$data);
              }

    		}else{
    			redirect('login');
    		}

    }

	function input_data(){
        $level="";
        $profil=$this->distribusi_obat_kel_model->getDataInstitusi();
        if($profil['levelinstitusi']==3){
            $level=3;
        }elseif($profil['levelinstitusi']==1){
            $level=1;
        }else $level=1;

        $data = array();

        if($level==3){
            if($this->input->post('unit_penerima')=='0'){
                $data['puskesmas']= null;
                $data['unit_penerima']= '';
                $data['unit_luar']= '';
                if ($this->input->post('unit_luar') != '') {
                    $temp = explode("|", $this->input->post('unit_luar'));
                    $data['unit_penerima']=$temp[0];
                    $data['unit_luar']=$temp[1];
                }

            }else{
                $nama_puskes=$this->db->query("
                select NAMA_PUSKES from ref_puskesmas
                where KD_PUSK=?",array($this->input->post('unit_penerima'))
                );
                $nama=$nama_puskes->row_array();
                $data['unit_penerima']=$this->input->post('unit_penerima');
                $data['unit_luar']=$nama['NAMA_PUSKES'];
            }
        }elseif($level==1){
            if($this->input->post('unit_penerima')==0){
                //propinsi
                $nama_prop=$this->db->query("
                select CPropDescr from ref_propinsi
                where CPropID=?",array($this->input->post('kec'))
                );
                $nama=$nama_prop->row_array();
                $data['unit_penerima']=$this->input->post('unit_penerima');
                $data['unit_luar']=$nama['CPropDescr'];
            }else{
                //kabupaten
                $nama_kab=$this->db->query("
                select CKabDescr from ref_kabupaten
                where CKabID=?",array($this->input->post('unit_penerima'))
                );
                $nama=$nama_kab->row_array();
                $data['unit_penerima']=$this->input->post('unit_penerima');
                $data['unit_luar']=$nama['CKabDescr'];
            }

        }else echo "level propinsi";

        $data['periode']=$this->input->post('periode');
		$data['id_dok']=$this->input->post('id_dok');
		$data['user_penerima']=$this->input->post('user_penerima');
		$data['nip']=$this->input->post('nip');
		$data['datenow']=$this->input->post('datenow');
        $data['keperluan']=$this->input->post('keperluan');

        if (($data['periode'] != '') && ($data['unit_penerima'] != '') && ($data['unit_luar'] != '') && ($data['datenow'])) {
            $query = $this->distribusi_obat_kel_model->input_data_m($data);
            if ($query) {
                $response['status'] = 'success';
                $response['message'] = 'Data berhasil disimpan';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Data gagal disimpan';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Gagal simpan. Data belum lengkap';
        }

        echo json_encode($response);
	}

    function datatochange(){
        $key=$this->input->post("id_distribusi");
        $change=$this->distribusi_obat_kel_model->getDataToChange($key);

        print json_encode($change);
    }

	function get_data_distribusi_obat(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/distribusi_obat_kel/get_data_distribusi_obat';
		$config['total_rows'] = $this->distribusi_obat_kel_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->distribusi_obat_kel_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/distribusi_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}


	function delete_list() {
        $kode=$this->input->post('kode');
        $result=$this->distribusi_obat_kel_model->deleteData($kode);
        if($result){
            $message="data berhasil dihapus";
        }else{
            $message="data tidak bisa dihapus";
        }
        print json_encode($message);
    }

 	function detail_faktur(){
 		$this->load->view('manajemenlogistik/detail_faktur');
 	}

 	function get_data_puskesmas($idpuskesmas) {
        $data = $this->distribusi_obat_kel_model->GetComboPuskesmas($idpuskesmas);
        $ret = '<option value="0">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function get_data_kabupaten($id_prop) {
        $data = $this->distribusi_obat_kel_model->GetComboKab($id_prop);
        $ret = '<option value="0">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['CKabID'].'">'.$data[$i]['CKabDescr'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function getdatapermintaan(){
    	$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/distribusi_obat_kel/getdatapermintaan';
		$limit = $config['per_page'] = 5; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 6;
        $config['total_rows'] = 100; //$data['result']->num_rows() + $data['finished']->num_rows();
        $this->pagination->initialize($config);

        $start = '0';
		if($this->uri->segment(6)!='')
            $start = $this->uri->segment(6);
        $data['result']=$this->distribusi_obat_kel_model->getpermintaan();
        $data['status']="Belum diproses";
        $data['finished']=$this->distribusi_obat_kel_model->getpermintaandone();

        $data['links'] = '';// $this->pagination->create_links();
		$this->load->view('manajemenlogistik/daftar_permintaan_kel',$data);
    }

    function get_detail($id){
    	$data['base_url']=$this->url;
    	$data['lplpo']=$this->distribusi_obat_kel_model->getdetailpermintaan($id);
    	$this->load->view('manajemenlogistik/detail_permintaan_kel',$data);
    }

    function get_detail_done($id){
        $data['base_url']=$this->url;
        $data['lplpo']=$this->distribusi_obat_kel_model->getdetailpermintaan($id);
        $this->load->view('manajemenlogistik/detail_permintaan_kel_done',$data);
    }

    function get_detail_permintaan($id_lplpo){
    	$data['base_url']=$this->url;
    	$data['result']=$this->distribusi_obat_kel_model->getdetailpermintaanlist($id_lplpo);
    	$this->load->view('manajemenlogistik/detail_permintaan_kel_list',$data);
    }

    function get_detail_permintaan_done($id_lplpo){
        $data['base_url']=$this->url;
        $data['result']=$this->distribusi_obat_kel_model->getdetailpermintaanlistdone($id_lplpo);
        $data['rowspan']=$this->distribusi_obat_kel_model->getQtyDistribution($id_lplpo);
        $data['id_lplpo']=$id_lplpo;
        $temp = $this->db->where('id_lplpo',$id_lplpo)->get('tb_detail_distribusi')->row_array();
        $data['happen_date'] = (isset($temp['create_time'])) ? $temp['create_time'] : '';
        $data['lplpo'] = $this->db->where('id',$id_lplpo)->get('tb_lplpo')->row_array();
        $this->load->view('manajemenlogistik/detail_permintaan_kel_list_done', $data);
    }

    function get_list_obat() {
        $key=$this->input->get("term");

		$query = $this->distribusi_obat_kel_model->GetListObat($key);

        print json_encode($query);
    }

    function getSuggest($kode_generik){
        $query = $this->distribusi_obat_kel_model->getDataSuggest($kode_generik);

        print json_encode($query);
    }

    function get_list_obat_grid(){
        $response= new stdClass();

        $searchTerm=$this->input->get("searchTerm");
        $page=$this->input->get("page");
        $sidx=$this->input->get("sidx");
        $limit=$this->input->get("rows");
        $sord=$this->input->get("sord");

        if(!$sidx) $sidx =1;
        if ($searchTerm=="") {
            $searchTerm="%";
        } else {
            $searchTerm = "%" . $searchTerm . "%";
        }

        $result = $this->db->query("
            SELECT COUNT(*) AS COUNT
            FROM tb_stok_obat so
            JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
            WHERE hg.nama_objek LIKE '$searchTerm' OR so.nama_obj LIKE '$searchTerm'
            ");
        $row = $result->row_array();
        $count = $row['count'];

        if( $count >0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) $page=$total_pages;
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)

        if($total_pages!=0)
            $SQL = "
                    SELECT *
                    FROM tb_stok_obat so
                    JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
                    JOIN tb_penerimaan p ON(p.id=so.id_faktur)
                    WHERE hg.nama_objek LIKE '$searchTerm' OR so.nama_obj LIKE '$searchTerm'
                    ORDER BY $sidx $sord LIMIT $start , $limit";
        else $SQL = "
                    SELECT *
                    FROM tb_stok_obat so
                    JOIN tb_penerimaan p ON(p.id=so.id_faktur)
                    JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
                    WHERE hg.nama_objek LIKE '$searchTerm' OR so.nama_obj LIKE '$searchTerm'
                    ORDER BY $sidx $sord";
        $query=$this->db->query($SQL);

        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;
        $i=0;
        //$r = array();
        foreach ($query->result_array() as $key) {
            $response->rows[$i]['kode_obat']=$key['kode_obat'];
            $response->rows[$i]['no_batch']=$key['no_batch'];
            //$response->rows[$i]['kemasan']=$key['detil_kemasan'];
            $response->rows[$i]['dana']=$key['dana'];
            $response->rows[$i]['nama_obj']=$key['nama_obj'];
            $response->rows[$i]['ed']=$key['expired'];
            $response->rows[$i]['stok']=$key['stok'];
            $response->rows[$i]['id_stok']=$key['id_stok'];
            //$response->rows[$i]['id_generik']=$key['id_generik'];
            $i++;
        }

        echo json_encode($response);
    }

    function get_list_obat_bar() {
        $key=$this->input->get("term");
        $query = $this->distribusi_obat_kel_model->GetListObatBar($key);
        print json_encode($query);
    }
    function input_data_distribusi(){
        //$time=date('Y-m-d H:i:s A');
        $time   =$this->input->post('tanggal_pemberian');
        $no_dok =$this->input->post('no_dok');
        $id_user=$this->session->userdata('id');
        $arr_nama_obat = $this->input->post('nama_obat');
    		$arr_kode_obat = $this->input->post('kode_obat');
    		$arr_kode_obat_req = $this->input->post('kode_obat_req');
        $arr_jml      = $this->input->post('jml_pemberian');
        $arr_no_batch = $this->input->post('no_batch');

        $id_lplpo = $this->input->post('id_lplpo');
        $id_stok = $this->input->post('id_stok');

        $arr_jml_max= $this->input->post('jml_stok');
        $continue=true;
        for($k=0;$k<sizeof($arr_jml_max);$k++){
            if($arr_jml[$k]>$arr_jml_max[$k]){
                $continue=false;
                $n=$k+1;
                $message['confirm']='Pemberian '.$arr_nama_obat[$k].' batch: '.$arr_no_batch[$k].' melebihi stok. Jumlah stok hanya ='.$arr_jml_max[$k];
                echo json_encode($message);
                die();
            }
        }

        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_jml[$i]!='') {
                //input ke tabel detail distribusi
                if ($arr_jml[$i]=='') {
                    $arr_jml[$i]==0;
                }

                $update_akumulasi=false;
                if(empty($arr_kode_obat_req[$i])){
                    $arr_kode_obat_req[$i]=$arr_kode_obat_req[$i-1];
                    $akumuluasi=$arr_jml[$i]+$arr_jml[$i-1];
                    $update_akumulasi=true;
                }

                $ins = $this->db->query("
                    INSERT INTO `tb_detail_distribusi` (
                        `id_lplpo`,
                        `kode_obat_req`,
                        `kode_obat_res`,
                    	`pemberian`,
                        `no_batch`,
                        `create_time`, `create_by`,`id_stok`)
                    VALUES (?,?,?,?,?,?,?,?)
                ",
                array($id_lplpo[0],
                        $arr_kode_obat_req[$i],
                        $arr_kode_obat[$i],
                        $arr_jml[$i],
                        $arr_no_batch[$i],
                        $time,
                        $id_user,
                        $id_stok[$i])
                );
                //ngurangin stok
                $upt = $this->db->query("
                    update tb_stok_obat
                    set stok= (stok-?)
                    where id_stok = ?
                ",
                array($arr_jml[$i],
                        $id_stok[$i])
                );
                //update field pemberian tb_detail_lplpo
                if($update_akumulasi){$arr_jml[$i]=$akumuluasi;}
                $upt = $this->db->query("
                    update tb_detail_lplpo_kel
                    set pemberian= ?
                    where id_lplpo = ? and kode_generik = ?
                ",
                array($arr_jml[$i],
                        $id_lplpo[0],
                        $arr_kode_obat_req[$i]
                        )
                );
                //cara ngemacthkan pemberian di tabel tb_detail_lplpo_kel
                //ambil permintaan sekalian id tb_detail_lplpo_kel
            }
        }

        //update nomor dokumen
        $lplpo['no_dok'] = $no_dok;
        $lplpo['update_time'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id_lplpo[0]);
        $this->db->update('tb_lplpo', $lplpo);

        $message['confirm']='Sukses input data';
        $message['id_lplpo']=$id_lplpo[0];
        echo json_encode($message);
	}

    function printOut($id_lplpo){

        $query=$this->distribusi_obat_kel_model->show_data($id_lplpo);
        $profil=$this->distribusi_obat_kel_model->getDataKab($id_lplpo);
        $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($profil['nama_kab']);
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:I1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('sbbk');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SURAT BUKTI BARANG KELUAR (SBBK)');

        $this->excel->getActiveSheet()->mergeCells('A2:I2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI KABUPATEN');

        $this->excel->getActiveSheet()->mergeCells('A3:I3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A3')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor Dokumen');
            $this->excel->getActiveSheet()->setCellValue('C5', ': ');
        $this->excel->getActiveSheet()->setCellValue('A6', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C6', ': '.$profil['nama_pusk']);

        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //HEADER
        for($col = 'B'; $col !== 'J'; $col++) {
            $this->excel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Obat/Barang');
        $this->excel->getActiveSheet()->setCellValue('D9', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Jumlah Distribusi');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Expired');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Harga (Rp)');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Jumlah (Rp)');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Tahun Anggaran');

        //konten tabel
        $totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            if(($query[$i]->dana)==''){
                $anggaran = "";
            }else {
                $anggaran = $query[$i]->dana.' '.$query[$i]->tahun_anggaran;
            }
            $j=$i+10;
            $n=$j-9;
            $total=$query[$i]->pemberian*$query[$i]->harga;
            $totalharga=$totalharga+$total;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->no_batch);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->expired);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->harga);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $total);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $anggaran);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('A9:I9')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A9:I'.($j+1))->applyFromArray($styleArray);

        $tanggalsbbk=$profil['nama_kab'].', '.$profil['tanggal'];
        $this->excel->getActiveSheet()->setCellValue('H'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->getStyle('F'.($j+4))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('F'.($j+4).':I'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('F'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->getStyle('F'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('F'.($j+5).':'.'I'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('F'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->getStyle('F'.($j+10))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('F'.($j+10).':'.'I'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('F'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->getStyle('A'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->getStyle('A'.($j+5))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $filename=$profil['nama_pusk'].'-SBBK-'.$profil['tanggal'].'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function change_data(){
        $data=$this->input->post('cha');
        $data['periode']=$data['periode'].'-00';
        $data['update_time']=date('Y-m-d H:m:s');
        $data['id_user']=$this->session->userdata('id');
        $this->db->where('id_distribusi',$data['id_distribusi']);
        $this->db->update('tb_distribusi',$data);
    }

    function input_unex(){
        $data['description'] = $this->input->post('unex');

        if ($data['description'] != '') {
            $result = $this->db->insert('tb_unitexternal',$data);

            if ($result) {
                $response['status'] = 'success';
                $response['message'] = "Data berhasil disimpan";
            } else {
                $response['status'] = 'error';
                $response['message'] = "Data gagal disimpan";
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Data gagal disimpan. Mohon diisi";
        }


        echo json_encode($response);
    }

    function delete_unex($id = "") {
        if ($id != '') {
            $this->db->where('id', $id);
            $result = $this->db->update('tb_unitexternal', array('status' => 0, 'delete_time' => date('Y-m-d H:i:s')) );

            if ($result) {
                $response['status'] = 'success';
                $response['message'] = "Data berhasil dihapus";
            } else {
                $response['status'] = 'error';
                $response['message'] = "Data gagal dihapus";
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = "Data gagal dihapus. Parameter kurang";
        }


        echo json_encode($response);
    }

    function additional_drug(){
        $data = $this->input->post('dt');

        //nambah jumlah pemberian
        //$this->db->update('tb_detail_lplpo');
        $this->db->query("
            update tb_detail_lplpo_kel
            set pemberian = pemberian + ?
            where id = ?
            ",array($data['pemberian'],$data['id_dl'])
            );
        //ngurangi stok
        // $this->db->where('id_stok',$data['id_stok']);
        // $this->db->update('tb_stok_obat');
        $this->db->query("
            update tb_stok_obat
            set stok = stok - ?
            where id_stok = ?
            ", array($data['pemberian'],$data['id_stok'])
            );

        unset($data['id_dl']);
        $data['update_time']=date('Y-m-d H:i:s');
        $data['create_by']=$this->session->userdata('id');
        //unset($data['id_dl']);
        $this->db->insert('tb_detail_distribusi',$data);
    }
}
?>

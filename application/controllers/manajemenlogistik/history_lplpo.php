<?php

Class History_Lplpo extends CI_Controller {

    var $title = 'Daftar LPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/history_lplpo_model');

    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->history_lplpo_model->getDataKecamatan();
			$this->load->view('manajemenlogistik/history_lplpo',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');
		$query=$this->history_lplpo_model->input_data_m($data);
		if($query){
			$message['confirm']='Sukses Input Data';
		}else{
			$message['confirm']='Data sudah tersedia';
		}
		print json_encode($message);
	}

	function getTemplate(){
		$query=$this->history_lplpo_model->getDataStok();
		//print_r($query);
		//die;
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(lPLPO)');

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A4', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C4', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A5', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C5', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C6', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A7', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C7', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('G4', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H4', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H5', ': kosong');

        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('J9', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('K9', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('L9', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('M9', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('N9', 'Keterangan');

        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            //set cell A1 content with some text
            $obatdosis = $query[$i]->nama_obat.' '.$query[$i]->kekuatan;
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $obatdosis);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->sediaan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename='template_lplpo.csv'; //save our workbook as this file name
        //header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    
	}

	function same_as_stok(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');

		$query=$this->history_lplpo_model->input_data_m($data);

		$prd=$data['per_lplpo'].'-00';
		//get data from stok
		$arr_newlplpo=$this->history_lplpo_model->getDataStok();

		//get data lplpo selected
		$past=$this->db->query("
			SELECT id
			FROM tb_lplpo
			WHERE kode_pusk =? AND periode=?
			",array($data['kode_pusk'],
					$prd)
			);
		$return=$past->row_array();
		$id_lplpo_baru=$return['id'];

		//insert into new detail lplpo
		for($i=0;$i<sizeof($arr_newlplpo);$i++){
			//echo $arr_newlplpo[$i]['kode_obat'].$arr_newlplpo[$i]['stok_akhir'].$arr_newlplpo[$i]['pemberian']."<br>";
			//$persediaan=$arr_newlplpo[$i]['pemberian']+$arr_newlplpo[$i]['stok_akhir'];
			$this->db->query("
					INSERT INTO `tb_detail_lplpo` (
							`id_lplpo`,
							`kode_obat`,
							`nama_obj`
					) VALUES (
						?,?,?
					)",
					array(
						$id_lplpo_baru,
						$arr_newlplpo[$i]['kode_obat'],
						$arr_newlplpo[$i]['nama_obj']
						//$arr_newlplpo[$i]['stok_akhir'],
						//$persediaan
					)
				);
		}
	}

	function data_inputed(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');
		//input lplpo baru
		$query=$this->history_lplpo_model->input_data_m($data);
		if(!($query)){
			$message['confirm']='Data sudah tersedia';
			echo json_encode($message);
			die;
		}
		/*if($query){
			$message['confirm']='Sukses Input Data';
			echo json_encode($message);
		}else{
			$message['confirm']='Data sudah tersedia';
			echo json_encode($message);
			die;
		}*/

		$str=strtotime($data['per_lplpo']);
		$bulan=date('m',$str);
		$keypast=$bulan-1;
		//print_r($keypast);
		//untuk mendapatkan id lplpo periode sebelumnya
		$past=$this->db->query("
			SELECT id
			FROM tb_lplpo
			WHERE kode_pusk =? AND EXTRACT(MONTH from periode)=?
			",array($data['kode_pusk'],
					$keypast)
			);
		$return=$past->row_array();
		//print_r($return['id']);
		$id_lplpo_lama=$return['id'];
		//print_r($id_lplpo_lama);
		//die;
		//echo "<br>";
		$newlplpo=$this->history_lplpo_model->getNewLplpo($id_lplpo_lama);
		//print_r(sizeof($newlplpo));
		//echo "<br>";
		$cekpermintaan=$this->history_lplpo_model->cekpermintaan($id_lplpo_lama);
		if($cekpermintaan){
			//$message['confirm']='LPLPO periode sebelumnya belum diproses';
			$message['confirm']='Apakah permintaan LPLPO sebelumnya sudah terpenuhi?';
			//delete current lplpo
			$message['con']=1;
			$this->history_lplpo_model->deletecurrentlplpo();
			echo json_encode($message);
			die;
		}else{
			$message['con']=0;
			$message['confirm']='Sukses Input Data';
			//print_r($newlplpo);
			//echo json_encode($message);
		}
		//print_r(sizeof($newlplpo));
		//die;

		//ambil id_lplpo yang baru saja diinput
		$new=$this->history_lplpo_model->getIdLPLPO();
		//$return2=$new->row_array();

		$id_lplpo_baru=$new['id'];
		//inputkan data['newlplpo'] pd lplpo yang baru
		$arr_newlplpo=$newlplpo;
		//print_r($arr_newlplpo);
		for($i=0;$i<sizeof($arr_newlplpo);$i++){
			//echo $arr_newlplpo[$i]['kode_obat'].$arr_newlplpo[$i]['stok_akhir'].$arr_newlplpo[$i]['pemberian']."<br>";
			$persediaan=$arr_newlplpo[$i]['pemberian']+$arr_newlplpo[$i]['stok_akhir'];
			$this->db->query("
					INSERT INTO `tb_detail_lplpo` (
							`id_lplpo`,
							`kode_obat`,
							`terima`,
							`stok_awal`,
							`sedia`,
							`nama_obj`
					) VALUES (
						?,?,?,?,?,?
					)",
					array(
						$id_lplpo_baru,
						$arr_newlplpo[$i]['kode_obat'],
						$arr_newlplpo[$i]['pemberian'],
						$arr_newlplpo[$i]['stok_akhir'],
						$persediaan,
						$arr_newlplpo[$i]['nama_obj']
					)
				);
		}
		//$this->load->view('manajemenlogistik/lplpo_new',$data)
		echo json_encode($message);
	}


	function get_data_history_lplpo(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/history_lplpo/get_data_history_lplpo';
		$config['total_rows'] = $this->history_lplpo_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);
		$limit=($this->uri->segment(4)=='')?0:$this->uri->segment(4);

		$data['result']=$this->history_lplpo_model->getData($config['per_page'],$limit);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/history_lplpo_list',$data);
		//$this->load->view('list_puskesmas');
	}


	function delete_list() {
        $kode=$this->input->post('kode');
        $this->history_lplpo_model->deleteData($kode);
    }

    function post_opt_value(){
    	$temp=$this->input->post('nilai');
    	$nilai_opt=$temp/100;
    	$this->db->query("
    		update tb_institusi
    		set buffer=$nilai_opt
    		");
    }

    function datatochange(){
    	$key=$this->input->post("id_lplpo");
    	$change=$this->history_lplpo_model->getDataToChange($key);
		//$this->load->view('manajemenlogistik/penerimaan_obat_list',$data);
		//$key=$this->input->get("term");
		//var $term;
		//$query = $this->Detail_Faktur_Model->GetListObat($key);
        print json_encode($change);
    }

    function get_data_puskesmas($kodekec,$idpuskesmas) {
        $data = $this->history_lplpo_model->GetComboPuskesmas($kodekec);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
        	$select = '';
        	if($data[$i]['KD_PUSK']==$idpuskesmas){
        		$select = 'selected';
        	}
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'" '.$select.'>'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function update_data(){
    	$data['id']=$this->input->post('id_lplpo');
    	$data['kode_kec']=$this->input->post('kode_kec');
    	$data['kode_pusk']=$this->input->post('kode_pusk');
    	$data['periode']=$this->input->post('periode');
    	$this->db->where('id',$data['id']);
    	$this->db->update('tb_lplpo',$data);
    }
}
?>

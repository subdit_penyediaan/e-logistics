<?php

Class Retur extends CI_Controller {
    
    var $title = 'Retur Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/retur_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/retur',$data);	
		}else{
			redirect('login');
		}
        
    }

    function retur_pkm(){
        if($this->session->userdata('login')==TRUE){
            $data['user']=$this->session->userdata('username');
            $data['base_url']=$this->url;
            $this->load->view('manajemenlogistik/retur_pkm',$data); 
        }else{
            redirect('login');
        }
    }

    function input_retur_pkm(){
        $data = array();
        $data['no_faktur']=$this->input->post('no_faktur');
        $data['kode_obat']=$this->input->post('kode_obat');
        //$data['tanggal_faktur']=$this->input->post('tanggal_faktur');
        //$data['pbf']=$this->input->post('pbf');
        $data['keterangan']=$this->input->post('keterangan');
        $data['no_batch']=$this->input->post('no_batch');
        $data['id_stok']=$this->input->post('id_stok');
        $data['jumlah']=$this->input->post('jumlah');
        $data['tanggal_retur']=$this->input->post('tanggal_retur');
        $this->retur_model->input_data_pkm($data);
        $datax['jumlah']=$data['jumlah'];
        $datax['id_stok']=$data['id_stok'];
        $this->retur_model->tambah_stok($datax);
    }
	
	function input_data(){
		$data = array();
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['kode_obat']=$this->input->post('kode_obat');
		//$data['tanggal_faktur']=$this->input->post('tanggal_faktur');
		//$data['pbf']=$this->input->post('pbf');
		$data['keterangan']=$this->input->post('keterangan');
		$data['no_batch']=$this->input->post('no_batch');
		$data['id_stok']=$this->input->post('id_stok');
		$data['jumlah']=$this->input->post('jumlah');
		$data['tanggal_retur']=$this->input->post('tanggal_retur');
		$this->retur_model->input_data_m($data);
		$datax['jumlah']=$data['jumlah'];
		$datax['id_stok']=$data['id_stok'];
		$this->retur_model->kurangi_stok($datax);
	}


	function get_data_retur(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/retur/get_data_retur';
		$config['total_rows'] = $this->retur_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);
		$limit=($this->uri->segment(4)=='')?0:$this->uri->segment(4);

		$data['result']=$this->retur_model->getData($config['per_page'],$limit);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/retur_list',$data);
		//$this->load->view('list_puskesmas');
	}

    function get_data_retur_pkm(){
        $data['base_url']=$this->url;
        
        //pagination
        $this->load->library('pagination');
        $config['base_url']= $data['base_url'].'index.php/manajemenlogistik/retur/get_data_retur_pkm';
        $config['total_rows'] = $this->retur_model->countAllDataPkm(); //untuk menghitung banyaknya rows
        //$config['total_rows'] = 50;
        $config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
        $config['uri_segment'] = 4;

        $this->pagination->initialize($config);
        $limit=($this->uri->segment(4)=='')?0:$this->uri->segment(4);

        $data['result']=$this->retur_model->getDataPkm($config['per_page'],$limit);
        $data['links'] = $this->pagination->create_links();
        $this->load->view('manajemenlogistik/retur_pkm_list',$data);
        //$this->load->view('list_puskesmas');
    }

	function get_list_obat() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->retur_model->GetListObat($key);
        print json_encode($query);
    }
	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->retur_model->deleteData($kode);
    }

 	function detail_faktur(){
 		$this->load->view('manajemenlogistik/detail_faktur');
 	}

 	function get_list_pbf() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->retur_model->GetListPbf($key);
        print json_encode($query);
    }

    function datatochange(){
    	$key=$this->input->post("no_faktur");
    	$change=$this->retur_model->getDataToChange($key);
		//$this->load->view('manajemenlogistik/retur_list',$data);
		//$key=$this->input->get("term");
		//var $term;
		//$query = $this->Detail_Faktur_Model->GetListObat($key);
        print json_encode($change);
    }

    function update_data(){
    	$data = array();
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['periode']=$this->input->post('periode');
		$data['tanggal']=$this->input->post('tanggal');
		$data['pbf']=$this->input->post('pbf');
		$data['nama_dok']=$this->input->post('nama_dok');
		$data['dana']=$this->input->post('dana');
		$data['no_kontrak']=$this->input->post('no_kontrak');
		$query=$this->retur_model->update_data_m($data);
    }

     function printOut(){
        
        $query=$this->retur_model->getDataEksport();
        //$profil=$this->distribusi_obat_model->getDataKab($id_lplpo);
        $profil=$this->retur_model->getDataInstitusi();
        if($profil['levelinstitusi']==3){
            $level = 'KABUPATEN '.$profil['CKabDescr'];
            $t4_surat = $profil['CKabDescr'];
        }elseif($profil['levelinstitusi']==2){
        	$level = 'PROVINSI '.$profil['CPropDescr'];
        	$t4_surat = $profil['CPropDescr'];
        }else{
            $level = 'KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $t4_surat ='Jakarta';
        }
        //function test(){
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:G1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('form_retur');
        
        //JUDUL
        
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'FORM RETUR SEDIAAN FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A2:G2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI'.$level);

        $this->excel->getActiveSheet()->mergeCells('A3:G3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A3', 'No:...');
        
        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'No. Faktur');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D9', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Jumlah');
        $this->excel->getActiveSheet()->setCellValue('F9', 'PBF');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Keterangan');        
        
        //konten tabel
        //$totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            //$total=$query[$i]->pemberian*$query[$i]->harga;
            //$totalharga=$totalharga+$total;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->no_faktur);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->no_batch);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->jumlah);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->pbf);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->keterangan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //$tanggalsbbk=$profil['nama_kab'].', '.$profil['tanggal'];
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);
        $tanggal = date('d-m-Y');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+4).':H'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+4), $t4_surat.', '.$tanggal);
        //$this->excel->getActiveSheet()->mergeCells('E'.($j+5).':'.'H'.($j+5));
        //$this->excel->getActiveSheet()->setCellValue('E'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+10).':'.'H'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+10), '(.........................)');

            
        //$filename=$profil['nama_pusk'].'-SBBK-'.$profil['tanggal'].'.xlsx'; //save our workbook as this file name
        $filename='form_retur.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007'); 
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV'); 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }

    function printOutPkm(){
        
        $query=$this->retur_model->getDataEksportPkm();
        //$profil=$this->distribusi_obat_model->getDataKab($id_lplpo);
        $profil=$this->retur_model->getDataInstitusi();
        if($profil['levelinstitusi']==3){
            $level = 'KABUPATEN '.$profil['CKabDescr'];
            $t4_surat = $profil['CKabDescr'];
        }elseif($profil['levelinstitusi']==2){
            $level = 'PROVINSI '.$profil['CPropDescr'];
            $t4_surat = $profil['CPropDescr'];
        }else{
            $level = 'KEMENTRIAN KESEHATAN REPUBLIK INDONESIA';
            $t4_surat ='Jakarta';
        }
        //function test(){
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:G1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('form_retur');
        
        //JUDUL
        
        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'FORM RETUR SEDIAAN FARMASI');

        $this->excel->getActiveSheet()->mergeCells('A2:G2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI'.$level);

        $this->excel->getActiveSheet()->mergeCells('A3:G3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A3', 'No:...');
        
        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'No. Faktur');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D9', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Jumlah');
        $this->excel->getActiveSheet()->setCellValue('F9', 'PBF');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Keterangan');        
        
        //konten tabel
        //$totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            //$total=$query[$i]->pemberian*$query[$i]->harga;
            //$totalharga=$totalharga+$total;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->no_faktur);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->no_batch);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->jumlah);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->pbf);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->keterangan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        //$tanggalsbbk=$profil['nama_kab'].', '.$profil['tanggal'];
        //$this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);
        $tanggal = date('d-m-Y');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+4).':H'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+4), $t4_surat.', '.$tanggal);
        //$this->excel->getActiveSheet()->mergeCells('E'.($j+5).':'.'H'.($j+5));
        //$this->excel->getActiveSheet()->setCellValue('E'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+10).':'.'H'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+10), '(.........................)');

            
        //$filename=$profil['nama_pusk'].'-SBBK-'.$profil['tanggal'].'.xlsx'; //save our workbook as this file name
        $filename='form_retur.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
                     
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007'); 
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV'); 
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }
}
?>
<?php

Class Isian_Lplpo extends CI_Controller {
    
    var $title = 'Isian LPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/isian_lplpo_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/isian_lplpo',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['id_lplpo']=$this->input->post('id_lplpo');
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['terima']=$this->input->post('terima');
		$data['sedia']=$this->input->post('sedia');
		$data['pakai']=$this->input->post('pakai');
		$data['stok_op']=$this->input->post('stok_op');
		$data['minta']=$this->input->post('minta');
		$data['ket']=$this->input->post('ket');
		$data['dana']=$this->input->post('dana');
		$data['stok_awal']=$this->input->post('stok_awal');
		$data['rusak_ed']=$this->input->post('rusak_ed');
        $data['nama_obj']=$this->input->post('nama_obj');
		$query=$this->isian_lplpo_model->input_data_m($data);
	}


	function get_data_isian_lplpo($id_lplpo){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/isian_lplpo/get_data_isian_lplpo/'.$id_lplpo;
		$config['total_rows'] = $this->isian_lplpo_model->countAllData($id_lplpo); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 5;

		$this->pagination->initialize($config);

		$data['result']=$this->isian_lplpo_model->getData($config['per_page'],$this->uri->segment(5),$id_lplpo);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/isian_lplpo_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->isian_lplpo_model->deleteData($kode);
    }

 	function get_detail($nomorlplpo){
     	//$qry=$this->db->query("select * from tb_penerimaan where no_faktur='$nomorfaktur'");
		//$data=$qry->row_array();
		$data['lplpo']=$this->isian_lplpo_model->getDataDetaillplpo($nomorlplpo);
		//$data['satuan']=$this->Detail_Faktur_Model->getSatuan();
		$this->load->view('manajemenlogistik/isian_lplpo',$data);
		//print_r($data['satuan']);

		//echo $satuan[1]['satuan_obat'];
     }

     function get_list_obat() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->isian_lplpo_model->GetListObat($key);
        print json_encode($query);
    }

    function update_data(){
        //ambil buffer
        $query=$this->db->query("select buffer from tb_institusi");
        $temp=$query->row_array();
        $buffer=$temp['buffer'];
    	//where
    	$arr_id_lplpo = $this->input->post('hidden_id_lplpo');
		$arr_kode_obat = $this->input->post('hidden_kode_obat');
		//konten tb_detail_lplpo
        $arr_stok_awal = $this->input->post('stok_awal');
        $arr_penerimaan = $this->input->post('penerimaan');
        $arr_persediaan = $this->input->post('persediaan');
        $arr_pemakaian = $this->input->post('pemakaian');
        $arr_rusak_ed = $this->input->post('rusak_ed');
        $arr_sisa_stok = $this->input->post('sisa_stok');
        $arr_stok_opt = $this->input->post('stok_opt');
        $arr_permintaan = $this->input->post('permintaan');
        $arr_ket = $this->input->post('ket');
        //konten tb_detail_distribusi
        $id_pemberian = $this->input->post('pemberian');
        //print_r($arr_concept_name);
        //print_r($arr_concept_id);
        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_id_lplpo!='') {
                //$xarr_treatment_id[] = $arr_treatment_id[$i];
                $arr_persediaan[$i]=$arr_stok_awal[$i]+$arr_penerimaan[$i];
                $sisa_stokX=$arr_persediaan[$i]-($arr_pemakaian[$i]+$arr_rusak_ed[$i]);
                $stok_optX=$arr_pemakaian[$i]+($buffer*$arr_pemakaian[$i]);
                $ins = $this->db->query("
                   UPDATE tb_detail_lplpo SET pakai=?,rusak_ed=?,stok_akhir=?,
                   	stok_opt=?,permintaan=?,ket=?
					WHERE id_lplpo=? AND kode_obat=? 
                    
                ",
                array($arr_pemakaian[$i],
                        $arr_rusak_ed[$i],
                        $sisa_stokX,
                        $stok_optX,
                        $arr_permintaan[$i],
                        $arr_ket[$i],
                		$arr_id_lplpo[$i],
                		$arr_kode_obat[$i])
                );
                //echo $this->db->last_query();
                /*$upt = $this->db->query("
                    update tb_stok_obat
                    set stok=?
                    where id_stok = ?
                ",
                array($arr_fisik_stok[$i],
                        $id_stok_obat[$i])
                );*/
            }
        }

        //$this->Detail_Stok_Opnam_Model->updateStatus($this->input->post('id_stok_opnam'));
        //return $ins;
        echo $arr_id_lplpo[0];
	}
}
?>
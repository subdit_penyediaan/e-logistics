<?php

Class History_lplpo_kel extends CI_Controller {

    var $title = 'Daftar LPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->id_user = $this->session->userdata('id');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/history_lplpo_kel_model');
        $this->load->model('manajemenlogistik/history_lplpo_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->history_lplpo_kel_model->getDataKecamatan();
			$this->load->view('manajemenlogistik/history_lplpo_kel',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');
		$user = getUser($this->id_user);
		if (($user['kode_puskesmas']==$data['kode_pusk'])||($user['group_id']!=3)) {
			$query=$this->history_lplpo_kel_model->input_data_m($data);
			if($query){
				$message['confirm']='Sukses Input Data';
			}else{
				$message['confirm']='Data sudah tersedia';
			}
		} else {
			$message['confirm']='Maaf, Hak akses anda salah';
		}

		echo json_encode($message);
	}

	function getTemplate(){
		    $query  = $this->history_lplpo_kel_model->getDataStok();

        // $query_bmhp  = $this->history_lplpo_kel_model->getDataStokBmhp();
        //
        // array_push($query, $query_bmhp);

		    //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(LPLPO)');

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C5', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C6', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A7', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C7', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('A8', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C8', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('G5', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H5', ': kosong');
        $this->excel->getActiveSheet()->setCellValue('G6', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H6', ': kosong');

        //HEADER
        //$this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('A11', 'Kode');
        $this->excel->getActiveSheet()->setCellValue('B11', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('C11', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D11', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('E11', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('F11', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('G11', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('H11', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('I11', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('J11', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('K11', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('L11', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('M11', 'Keterangan');

        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+12;//$j=$i+10;
            //$n=$j-11;//$n=$j-9;
            //set cell A1 content with some text
            //$obatdosis = $query[$i]->nama_obat.' '.$query[$i]->kekuatan;
            //$this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $query[$i]->kode_generik);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->sediaan);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename='template_lplpo.csv'; //save our workbook as this file name
        //header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert

	}

	function same_as_stok(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');

		$query=$this->history_lplpo_kel_model->input_data_m($data);

		$prd=$data['per_lplpo'].'-00';
		//get data from stok
		$arr_newlplpo=$this->history_lplpo_kel_model->getDataStok();

		//get data lplpo selected
		$past=$this->db->query("
			SELECT id
			FROM tb_lplpo
			WHERE kode_pusk =? AND periode=?
			",array($data['kode_pusk'],
					$prd)
			);
		$return=$past->row_array();
		$id_lplpo_baru=$return['id'];

		//insert into new detail lplpo
		for($i=0;$i<sizeof($arr_newlplpo);$i++){
			//echo $arr_newlplpo[$i]['kode_obat'].$arr_newlplpo[$i]['stok_akhir'].$arr_newlplpo[$i]['pemberian']."<br>";
			//$persediaan=$arr_newlplpo[$i]['pemberian']+$arr_newlplpo[$i]['stok_akhir'];
			$this->db->query("
					INSERT INTO `tb_detail_lplpo` (
							`id_lplpo`,
							`kode_obat`,
							`nama_obj`
					) VALUES (
						?,?,?
					)",
					array(
						$id_lplpo_baru,
						$arr_newlplpo[$i]['kode_obat'],
						$arr_newlplpo[$i]['nama_obj']
						//$arr_newlplpo[$i]['stok_akhir'],
						//$persediaan
					)
				);
		}
	}

	function data_inputed(){
		$data = array();
		$data['kode_kec']=$this->input->post('kode_kec');
		$data['kode_pusk']=$this->input->post('kode_pusk');
		$data['per_lplpo']=$this->input->post('per_lplpo');
		//input lplpo baru
		$query=$this->history_lplpo_kel_model->input_data_m($data);
		if(!($query)){
			$message['confirm']='Data sudah tersedia';
			echo json_encode($message);
			die;
		}

		$str=strtotime($data['per_lplpo']);
		//$bulan=date('m',$str);
		$keypast = date('Y-m-00',strtotime("last month", $str));
		//$keypast=$bulan-1;
		//untuk mendapatkan id lplpo periode sebelumnya
		$past=$this->db->query("
			SELECT id
			FROM tb_lplpo
			WHERE kode_pusk =? AND periode=?
			",array($data['kode_pusk'],
					$keypast)
			);
		if($past->num_rows() > 0){
			$return=$past->row_array();
			$id_lplpo_lama=$return['id'];
		}else{
			$message['confirm']='tidak ada LPLPO periode sebelumnya';
			$message['con']=1;
			$this->history_lplpo_kel_model->deletecurrentlplpo();
			echo json_encode($message);
			die;
		}


		$newlplpo=$this->history_lplpo_kel_model->getNewLplpo($id_lplpo_lama);

		//$cekpermintaan=$this->history_lplpo_kel_model->cekpermintaan($id_lplpo_lama);
		$cekpermintaan = false;
		if($cekpermintaan){
			$message['confirm']='LPLPO periode sebelumnya belum diproses';
			//$message['confirm']='Apakah permintaan LPLPO sebelumnya sudah terpenuhi?';

			//delete current lplpo
			$message['con']=1;
			$this->history_lplpo_kel_model->deletecurrentlplpo();
			echo json_encode($message);
			die;
		}else{
			$message['con']=0;
			$message['confirm']='Sukses Input Data';
		}
		//ambil id_lplpo yang baru saja diinput
		$new=$this->history_lplpo_kel_model->getIdLPLPO();
		$id_lplpo_baru=$new['id'];
		//inputkan data['newlplpo'] pd lplpo yang baru
		$arr_newlplpo=$newlplpo;
		for($i=0;$i<sizeof($arr_newlplpo);$i++){
			//echo $arr_newlplpo[$i]['kode_obat'].$arr_newlplpo[$i]['stok_akhir'].$arr_newlplpo[$i]['pemberian']."<br>";
			$persediaan=$arr_newlplpo[$i]['pemberian']+$arr_newlplpo[$i]['stok_akhir'];
			$this->db->query("
					INSERT INTO `tb_detail_lplpo_kel` (
							`id_lplpo`,
							`kode_generik`,
							`terima`,
							`stok_awal`,
							`sedia`,
							`nama_obj`,
							`sediaan`
					) VALUES (
						?,?,?,?,?,?,?
					)",
					array(
						$id_lplpo_baru,
						$arr_newlplpo[$i]['kode_generik'],
						$arr_newlplpo[$i]['pemberian'],
						$arr_newlplpo[$i]['stok_akhir'],
						$persediaan,
						$arr_newlplpo[$i]['nama_obj'],
						$arr_newlplpo[$i]['sediaan']
					)
				);
		}
		echo json_encode($message);
	}


	function get_data_history_lplpo(){
		$data['base_url']=$this->url;
		$data['kecamatan']=$this->history_lplpo_kel_model->getDataKecamatan();
		//pagination
		// $this->load->library('pagination');
		// $config['base_url']= $data['base_url'].'index.php/manajemenlogistik/history_lplpo_kel/get_data_history_lplpo';
		// $config['total_rows'] = $this->history_lplpo_kel_model->countAllData(); //untuk menghitung banyaknya rows
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		// $config['uri_segment'] = 4;
		// $this->pagination->initialize($config);
		$limit=($this->uri->segment(4)=='')?0:$this->uri->segment(4);

		$data['result']=$this->history_lplpo_kel_model->getData($config['per_page'],$limit);
		// $data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/history_lplpo_kel_list',$data);
	}


	function delete_list($id, $kode_pkm) {
        $user = getUser($this->id_user);
		if (($user['kode_puskesmas']==$kode_pkm)||($user['group_id']!=3)) {
	        $cek = $this->db->where('id_lplpo', $id)->get('tb_detail_distribusi');
	        if ($cek->num_rows() < 1) {
	        	if ($this->history_lplpo_kel_model->deleteData($id)) {
	        		$result['message'] = "Data berhasil dihapus";
	        		$result['status'] = "success";
	        	} else {
	        		$result['message'] = "Data tidak berhasil dihapus";
	        		$result['status'] = "failed";
	        	}
	        } else {
	        	// ada transaksi
	        	$result['message'] = "Data tidak berhasil dihapus, ada transaksi";
	        	$result['status'] = "failed";
	        }
	    } else {
	    	// ga punya hak akses
	    	$result['message'] = "Maaf, hak akses salah";
	        $result['status'] = "failed";
	    }

        echo json_encode($result);
    }

    function post_opt_value(){
    	$profile = getProfile();
    	$temp=$this->input->post('nilai');
    	$nilai_opt=$temp/100;
    	$this->db->query("
    		update tb_institusi
    		set buffer=$nilai_opt
    		");

    	//update nilai bufer semua puskesmas di kabupaten $kode_kabupaten
    	$kode_kabupaten 	= $profile['id_kab'];
    	$data['buffer'] 	= $nilai_opt;
    	$data['update_time'] 	= date('Y-m-d H:i:s');
    	$data['create_by']		= $this->session->userdata('id');
    	$this->db->where('KODE_KAB',$kode_kabupaten);
    	$this->db->update('ref_puskesmas',$data);
    }

    function post_periode(){
    	$profile = getProfile();
    	$temp['periode_distribusi']=$this->input->post('nilai');
    	$this->db->update('tb_institusi',$temp);

    	$kode_kabupaten = $profile['id_kab'];
    	$data['periode_distribusi'] = $this->input->post('nilai');
    	$data['update_time'] 		= date('Y-m-d H:i:s');
    	$data['create_by']			= $this->session->userdata('id');
    	$this->db->where('KODE_KAB',$kode_kabupaten);
    	$this->db->update('ref_puskesmas',$data);
    }

    function datatochange($id, $kode_pkm){
    	$user = getUser($this->id_user);
		if (($user['kode_puskesmas']==$kode_pkm)||($user['group_id']!=3)) {
    		$result['content'] =$this->history_lplpo_model->getDataToChange($id);
    		$result['message'] = '';
    		$result['status'] = 'success';
    	} else {
    		$result['message'] = 'Maaf, hak akses salah';
    		$result['status'] = 'failed';
    	}

        echo json_encode($result);
    }

    function get_data_puskesmas($kodekec,$idpuskesmas) {
    	$this->load->model('manajemenlogistik/history_lplpo_model');
        $data = $this->history_lplpo_model->GetComboPuskesmas($kodekec);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
        	$select = '';
        	if($data[$i]['KD_PUSK']==$idpuskesmas){
        		$select = 'selected';
        	}
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'" '.$select.'>'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function update_data(){
    	$data['id']=$this->input->post('id_lplpo');
    	$data['kode_kec']=$this->input->post('kode_kec');
    	$data['kode_pusk']=$this->input->post('kode_pusk');
    	$data['periode']=$this->input->post('periode').'-00';
    	$user = getUser($this->id_user);
    	if (($user['kode_puskesmas']==$data['kode_pusk'])||($user['group_id']!=3)) {
    		$this->db->where('id',$data['id']);
    		if($this->db->update('tb_lplpo',$data)) {
    			$message['confirm']='Data berhasil diubah';
    		} else {
    			$message['confirm']='Data tidak berhasil diubah';
    		}
    	} else {
    		$message['confirm']='Maaf, hak akses anda salah';
    	}

    	echo json_encode($message);
    }
}
?>

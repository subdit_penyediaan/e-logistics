<?php

Class Penerimaan_Obat extends CI_Controller {
    
    var $title = 'Penerimaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/penerimaan_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kepemilikan']= $this->db->where('delete_time', null)->get('ref_obatprogram');
			$this->load->view('manajemenlogistik/penerimaan_obat',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['periode']=$this->input->post('periode');
		$data['tanggal']=$this->input->post('tanggal');
		$data['pbf']=$this->input->post('pbf');
		$data['nama_dok']=$this->input->post('nama_dok');
		$data['dana']=$this->input->post('dana');
		$data['no_kontrak']=$this->input->post('no_kontrak');
		$data['tahun']=$this->input->post('tahun_anggaran');
		$query=$this->penerimaan_obat_model->input_data_m($data);
	}


	function get_data_penerimaan_obat(){
		$data['base_url']=$this->url;
		
		//pagination
		// $this->load->library('pagination');
		// $config['base_url']= $data['base_url'].'index.php/manajemenlogistik/penerimaan_obat/get_data_penerimaan_obat';
		// $config['total_rows'] = $this->penerimaan_obat_model->countAllData(); //untuk menghitung banyaknya rows
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		// $config['uri_segment'] = 4;

		// $this->pagination->initialize($config);
		$limit=($this->uri->segment(4)=='')?0:$this->uri->segment(4);

		$data['result']=$this->penerimaan_obat_model->getData($config['per_page'],$limit);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/penerimaan_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->penerimaan_obat_model->deleteData($kode);
    }

 	function detail_faktur(){
 		$this->load->view('manajemenlogistik/detail_faktur');
 	}

 	function get_list_pbf() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->penerimaan_obat_model->GetListPbf($key);
        print json_encode($query);
    }

    function datatochange(){
    	$key=$this->input->post("no_faktur");
    	$change=$this->penerimaan_obat_model->getDataToChange($key);
		//$this->load->view('manajemenlogistik/penerimaan_obat_list',$data);
		//$key=$this->input->get("term");
		//var $term;
		//$query = $this->Detail_Faktur_Model->GetListObat($key);
        print json_encode($change);
    }

    function update_data(){
    	$data = array();
    	$data['id_faktur']=$this->input->post('id_faktur');
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['periode']=$this->input->post('periode');
		$data['tanggal']=$this->input->post('tanggal');
		$data['pbf']=$this->input->post('pbf');
		$data['nama_dok']=$this->input->post('nama_dok');
		$data['dana']=$this->input->post('dana');
		$data['no_kontrak']=$this->input->post('no_kontrak');
		$data['tahun_anggaran']=$this->input->post('ta');
		$query=$this->penerimaan_obat_model->update_data_m($data);
    }
}
?>
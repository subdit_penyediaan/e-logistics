<?php

Class Distribusi_Obat extends CI_Controller {

    var $title = 'Distribusi Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/distribusi_obat_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;

            //$data['propinsi']=
            $data['cito']=$this->distribusi_obat_model->getDataCito();
            $profil=$this->distribusi_obat_model->getDataInstitusi();
            if($profil['levelinstitusi']==3){
                $data['kecamatan']=$this->distribusi_obat_model->getDataKecamatan();
                $this->load->view('manajemenlogistik/distribusi_obat',$data);
                //$this->load->view('profil/unit_penerima',$data);
            }elseif(($profil['levelinstitusi']==1)||($profil['levelinstitusi']==2)){
                $data['level']=$profil['levelinstitusi'];
                $data['ux'] = $this->db->where('status', 1)->get('tb_unitexternal')->result_array();
                $data['propinsi']=$this->distribusi_obat_model->getPropinsi();
                $data['kabupaten']=$this->distribusi_obat_model->getKabupaten($profil['id_prop']);
                $this->load->view('manajemenlogistik/distribusi_obat_pusat',$data);
            }

		}else{
			redirect('login');
		}

    }

	function input_data(){
        $level="";
        $profil=$this->distribusi_obat_model->getDataInstitusi();
        if($profil['levelinstitusi']==3){
            $level=3;
        }elseif($profil['levelinstitusi']==1){
            $level=1;
        }else $level=2;

        $data = array();

        if($level==3){
            if($this->input->post('unit_penerima')=='0'){
            $data['puskesmas']='null';
            //print_r($data['puskesmas']);
            $data['unit_luar']=$this->input->post('unit_luar');
            }else{
                $nama_puskes=$this->db->query("
                select NAMA_PUSKES from ref_puskesmas
                where KD_PUSK=?",array($this->input->post('unit_penerima'))
                );
                $nama=$nama_puskes->row_array();
                $data['unit_luar']=$nama['NAMA_PUSKES'];
            }

            //print_r($data['unit_luar']);
        }elseif($level==1){
            // if($this->input->post('unit_penerima')==0){
            //     //propinsi
            //     $nama_prop=$this->db->query("
            //     select CPropDescr from ref_propinsi
            //     where CPropID=?",array($this->input->post('kec'))
            //     );
            //     $nama=$nama_prop->row_array();
            //     $data['unit_luar']=$nama['CPropDescr'];
            // }else{
            //     //kabupaten
            //     $nama_kab=$this->db->query("
            //     select CKabDescr from ref_kabupaten
            //     where CKabID=?",array($this->input->post('unit_penerima'))
            //     );
            //     $nama=$nama_kab->row_array();
            //     $data['unit_luar']=$nama['CKabDescr'];
            // }
            $temp = explode("|", $this->input->post('unit_luar'));
            $data['unit_penerima']=$temp[0];
            $data['unit_luar']=$temp[1];
        }else {
            if($this->input->post('unit_penerima')==0){
                //propinsi
                //$data['unit_luar']=$this->input->post('unit_luar');
                $temp = explode("|", $this->input->post('unit_luar'));
                $data['unit_penerima']=$temp[0];
                $data['unit_luar']=$temp[1];
            }else{
                //kabupaten
                $nama_kab=$this->db->query("
                select CKabDescr from ref_kabupaten
                where CKabID=?",array($this->input->post('unit_penerima'))
                );
                $nama=$nama_kab->row_array();
                $data['unit_luar']=$nama['CKabDescr'];
            }
        }

		//$data['kec']=$this->input->post('kec');
		$data['periode']=$this->input->post('periode');
		$data['unit_penerima']=$this->input->post('unit_penerima');
		$data['id_dok']=$this->input->post('id_dok');
		$data['user_penerima']=$this->input->post('user_penerima');
		$data['nip']=$this->input->post('nip');
		$data['datenow']=$this->input->post('datenow');
        //$data['unit_luar']=$this->input->post('unit_luar');
        $data['keperluan']=$this->input->post('keperluan');
		$query=$this->distribusi_obat_model->input_data_m($data);
	}


	function get_data_distribusi_obat(){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/distribusi_obat/get_data_distribusi_obat';
		$config['total_rows'] = $this->distribusi_obat_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->distribusi_obat_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/distribusi_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}

	function datatochange(){
        $key=$this->input->post("id_distribusi");
        $change=$this->distribusi_obat_model->getDataToChange($key);

        print json_encode($change);
    }

    function change_data(){
        $data=$this->input->post('cha');
        $data['periode']=$data['periode'].'-00';
        $this->db->where('id_distribusi',$data['id_distribusi']);
        $this->db->update('tb_distribusi',$data);
    }

	function delete_list() {
        $kode=$this->input->post('kode');
        $result=$this->distribusi_obat_model->deleteData($kode);
        if($result){
            $message="data berhasil dihapus";
        }else{
            $message="maaf, sudah ada transaksi. data tidak bisa dihapus";
        }
        print json_encode($message);
    }

 	function detail_faktur(){
 		$this->load->view('manajemenlogistik/detail_faktur');
 	}

 	function get_data_puskesmas($idpuskesmas) {
        $data = $this->distribusi_obat_model->GetComboPuskesmas($idpuskesmas);
        //print_r($data);
        $ret = '<option value="0">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function get_data_kabupaten($id_prop) {
        $data = $this->distribusi_obat_model->GetComboKab($id_prop);
        //print_r($data);
        $ret = '<option value="0">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['CKabID'].'">'.$data[$i]['CKabDescr'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

    function getdatapermintaan(){
    	$data['base_url']=$this->url;

		//pagination
		//$this->load->library('pagination');
		//$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/distribusi_obat/getdatapermintaan';
		//$config['total_rows'] = $this->distribusi_obat_model->countdata(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		//$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		//$config['uri_segment'] = 4;

		//$this->pagination->initialize($config);
    	$data['status']="Belum diproses";
		//$data['result']=$this->distribusi_obat_model->getData($config['per_page'],$this->uri->segment(4));
		$data['result']=$this->distribusi_obat_model->getpermintaan();
        $data['finished']=$this->distribusi_obat_model->getpermintaandone();
		//$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/daftar_permintaan',$data);
    }

    function get_detail($id){
    	$data['base_url']=$this->url;
    	$data['lplpo']=$this->distribusi_obat_model->getdetailpermintaan($id);
    	$this->load->view('manajemenlogistik/detail_permintaan',$data);
    }

    function get_detail_done($id){
        $data['base_url']=$this->url;
        $data['lplpo']=$this->distribusi_obat_model->getdetailpermintaan($id);
        $this->load->view('manajemenlogistik/detail_permintaan_done',$data);
    }

    function get_detail_permintaan($id_lplpo){
    	$data['base_url']=$this->url;
    	$data['result']=$this->distribusi_obat_model->getdetailpermintaanlist($id_lplpo);
    	$this->load->view('manajemenlogistik/detail_permintaan_list',$data);
    }

    function get_detail_permintaan_done($id_lplpo){
        $data['base_url']=$this->url;
        $data['result']=$this->distribusi_obat_model->getdetailpermintaanlistdone($id_lplpo);
        $data['id_lplpo']=$id_lplpo;
        $this->load->view('manajemenlogistik/detail_permintaan_list_done',$data);
    }

    function get_list_obat() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->distribusi_obat_model->GetListObat($key);
        print json_encode($query);
    }

    function get_list_obat_bar() {
        $key=$this->input->get("term");
        //var $term;
        $query = $this->distribusi_obat_model->GetListObatBar($key);
        print json_encode($query);
    }
    function input_data_distribusi(){
        $time=date('Y-m-d H:i:s A');
        $id_user=$this->session->userdata('id');

		$arr_kode_obat = $this->input->post('kode_obat');
		$arr_kode_obat_req = $this->input->post('kode_obat_req');
        $arr_jml = $this->input->post('jml_pemberian');
        $arr_no_batch = $this->input->post('no_batch');

        $id_lplpo = $this->input->post('id_lplpo');
        $id_stok = $this->input->post('id_stok');

        $arr_jml_max= $this->input->post('jml_stok');
        $continue=true;
        for($k=0;$k<sizeof($arr_jml_max);$k++){
            if($arr_jml[$k]>$arr_jml_max[$k]){
                $continue=false;$n=$k+1;
                $message['confirm']='Pemberian No.'.$n.' melebihi stok';
                echo json_encode($message);
                die;
            }
        }
        //print_r($arr_concept_name);
        //print_r($arr_concept_id);
        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_jml!='') {
                //input ke tabel detail distribusi
                $update_akumulasi=false;
                if(empty($arr_kode_obat_req[$i])){
                    $arr_kode_obat_req[$i]=$arr_kode_obat_req[$i-1];
                    //$id_lplpo[$i]=$id_lplpo[$i-1];
                    //$arr_no_batch[$i]=$arr_no_batch[$i-1];
                    //$arr_jml=$arr_jml[$i]+$arr_jml[$i-1];
                    $akumuluasi=$arr_jml[$i]+$arr_jml[$i-1];
                    $update_akumulasi=true;
                }

                $ins = $this->db->query("
                    INSERT INTO `tb_detail_distribusi`
                    (`id_lplpo`, `kode_obat_req`, `kode_obat_res`,
                    	`pemberian`, `no_batch`,
                        `create_time`, `create_by`)
                    VALUES (?,?,?,?,?,?,?)
                ",
                array($id_lplpo[0],
                        $arr_kode_obat_req[$i],
                        $arr_kode_obat[$i],
                        $arr_jml[$i],
                        $arr_no_batch[$i],
                        $time,
                        $id_user)
                );
                //ngurangin stok
                $upt = $this->db->query("
                    update tb_stok_obat
                    set stok= (stok-?)
                    where id_stok = ?
                ",
                array($arr_jml[$i],
                        $id_stok[$i])
                );
                //update field pemberian tb_detail_lplpo
                if($update_akumulasi){$arr_jml[$i]=$akumuluasi;}
                $upt = $this->db->query("
                    update tb_detail_lplpo
                    set pemberian= ?
                    where id_lplpo = ? and kode_obat = ?
                ",
                array($arr_jml[$i],
                        $id_lplpo[0],
                        $arr_kode_obat_req[$i]
                        )
                );
            }
        }
        $message['confirm']='Sukses input data';
        $message['id_lplpo']=$id_lplpo[0];
        echo json_encode($message);

        //$this->Detail_Stok_Opnam_Model->updateStatus($this->input->post('id_stok_opnam'));
        //return $ins;
	}

    function printOut($id_lplpo){

        $query=$this->distribusi_obat_model->show_data($id_lplpo);
        $profil=$this->distribusi_obat_model->getDataKab($id_lplpo);
        $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($profil['nama_kab']);
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:H1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('sbbk');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SURAT BUKTI BARANG KELUAR (SBBK)');

        $this->excel->getActiveSheet()->mergeCells('A2:H2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI KABUPATEN');

        $this->excel->getActiveSheet()->mergeCells('A3:H3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor Dokumen');
            $this->excel->getActiveSheet()->setCellValue('C5', ': ');
        $this->excel->getActiveSheet()->setCellValue('A6', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C6', ': '.$profil['nama_pusk']);

        //HEADER
        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Obat/Barang');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Jumlah Distribusi');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Expired');
        $this->excel->getActiveSheet()->setCellValue('F9', 'Harga (Rp)');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Jumlah (Rp)');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Keterangan');

        //konten tabel
        $totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+10;
            $n=$j-9;
            $total=$query[$i]->pemberian*$query[$i]->harga;
            $totalharga=$totalharga+$total;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->expired);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->harga);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $total);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $tanggalsbbk=$profil['nama_kab'].', '.$profil['tanggal'];
        $this->excel->getActiveSheet()->setCellValue('G'.($j+1), $totalharga);

        $this->excel->getActiveSheet()->mergeCells('E'.($j+4).':H'.($j+4));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+4), $tanggalsbbk);
        $this->excel->getActiveSheet()->mergeCells('E'.($j+5).':'.'H'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+5), 'Yang menyerahkan/memeriksa');
        $this->excel->getActiveSheet()->mergeCells('E'.($j+10).':'.'H'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('E'.($j+10), '(.........................)');

        $this->excel->getActiveSheet()->mergeCells('A'.($j+5).':'.'C'.($j+5));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+5), 'Yang menerima');
        $this->excel->getActiveSheet()->mergeCells('A'.($j+10).':'.'C'.($j+10));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+10), '(.........................)');

        $filename=$profil['nama_pusk'].'-SBBK-'.$profil['tanggal'].'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
    }
}
?>

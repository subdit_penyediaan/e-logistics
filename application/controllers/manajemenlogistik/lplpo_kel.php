<?php

Class Lplpo_kel extends CI_Controller {

    var $title = 'lPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->helper('file');
        $this->load->model('manajemenlogistik/lplpo_kel_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['kecamatan']=$this->lplpo_kel_model->getDataKecamatan();

			//print_r($data);
			$this->load->view('manajemenlogistik/lplpo_kel',$data);
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }

    function get_data_puskesmas($idpuskesmas) {
        $data = $this->lplpo_kel_model->GetComboPuskesmas($idpuskesmas);
        //print_r($data);
        $ret = '<option value="">--- Pilih ---</option>';
        for($i=0;$i<sizeof($data);$i++) {
            $ret .= '<option value="'.$data[$i]['KD_PUSK'].'">'.$data[$i]['NAMA_PUSKES'].'</option>';
        }
        $ret .= '<option value="add">--- Tambah ---</option>';
        echo $ret;
    }

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }

    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);

        print json_encode($query);
    }

    function update_data_import(){
        //$data['kode_kec']=$this->input->post('kode_kec');
        $kode_pusk=$this->input->post('kode_pusk');
        $periode=$this->input->post('per_lplpo');

        $per=$periode.'-00';
        $id_lplpo=$this->db->query("
            select id
            from tb_lplpo
            where kode_pusk='$kode_pusk' and periode='$per'
            ");
        if($id_lplpo->num_rows > 0){
            $message['confirm']='Data telah dipilih';
            $id=$id_lplpo->row_array();
            $data['new_id_lplpo']=$id['id'];
            $this->session->set_userdata($data);
        }else{
            $message['confirm']='Data belum tersedia';
        }
        echo json_encode($message);

    }

	function getIdlplpo(){
        $data['kode_kec']=$this->input->post('kode_kec');
        $data['kode_pusk']=$this->input->post('kode_pusk');
        $data['per_lplpo']=$this->input->post('per_lplpo');

        //cek
        //if (true)
        //apakah mau update?
        $query=$this->lplpo_kel_model->input_data_m($data);
        if($query){
            $message['confirm']='Sukses Input Data';
			//$set['kec']=$this->input->post('kec');
			$periode=$data['per_lplpo'];
			$kode_pusk=$data['kode_pusk'];
			$per=$periode.'-00';
			$id_lplpo=$this->db->query("
				select id
				from tb_lplpo
				where kode_pusk='$kode_pusk' and periode='$per'
				");
			$id=$id_lplpo->row_array();

			//die;
			$data['new_id_lplpo']=$id['id'];
			//print_r($new_id_lplpo);
			$this->session->set_userdata($data);
            echo json_encode($message);
        }else{
            $message['confirm']='Data sudah tersedia';
            echo json_encode($message);
            //die;
        }




    }

    function search_data_by(){
        $set['kec']=$this->input->post('kec');
        $set['per']=$this->input->post('per');
        $set['nama_pusk']=$this->input->post('nama_pusk');
        //input database

        $this->session->set_userdata($set);
        $data['links']="";
        $data['result']=$this->lplpo_kel_model->show_data($set);
        //$data['kec']=$this->input->post('kec');
        //$data['per']=$this->input->post('per');
        //$data['nama_pusk']=$this->input->post('nama_pusk');

        //print_r($set);
        //$this->session->set_userdata($set);

        $this->load->view('manajemenlogistik/lplpo_kel_ex_list',$data);

        //$this->printOut($set);
    }

    function printOut(){
        //$query=$this->lplpo_kel_model->getDataEksport();
        $set['kec']=$this->session->userdata('kec');
        $set['per']=$this->session->userdata('per');
        $set['nama_pusk']=$this->session->userdata('nama_pusk');

        $query=$this->lplpo_kel_model->show_data($set);
        //print_r($query[0]->id);
        $profil=$this->lplpo_kel_model->getProfilPrint($set['nama_pusk']);

        //print_r($profil);
        $formattahun=strtotime($set['per']);
        $tahun=date('Y',$formattahun);
        $bulan=date('F',$formattahun);
        //echo $bulan.', '.$tahun;
        //die;

        /* for($i=0;$i<sizeof($query);$i++){
            echo $query[$i]['nama_obat']."<br>";
        } */
        /* foreach($query->result() as $rows){
            echo $rows->nama_obat."<br>";
        } */
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:N1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('laporan');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A1', 'LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN OBAT');

        $this->excel->getActiveSheet()->mergeCells('A2:N2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->setCellValue('A2', '(LPLPO)');

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C5', $profil['nama_pusk']);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Kecamatan');
            $this->excel->getActiveSheet()->setCellValue('C6', $profil['nama_kec']);
        $this->excel->getActiveSheet()->setCellValue('A7', 'Kabupaten');
            $this->excel->getActiveSheet()->setCellValue('C7', $profil['nama_kab']);
        $this->excel->getActiveSheet()->setCellValue('A8', 'Provinsi');
            $this->excel->getActiveSheet()->setCellValue('C8', $profil['nama_prop']);
        $this->excel->getActiveSheet()->setCellValue('G5', 'Bulan');
            $this->excel->getActiveSheet()->setCellValue('H5', $bulan);
        $this->excel->getActiveSheet()->setCellValue('G6', 'Tahun');
            $this->excel->getActiveSheet()->setCellValue('H6', $tahun);

        //HEADER
        //$this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('A11', 'Kode Obat');
        $this->excel->getActiveSheet()->setCellValue('B11', 'Nama Obat');
        $this->excel->getActiveSheet()->setCellValue('C11', 'Sediaan');
        $this->excel->getActiveSheet()->setCellValue('D11', 'Stok Awal');
        $this->excel->getActiveSheet()->setCellValue('E11', 'Penerimaan');
        $this->excel->getActiveSheet()->setCellValue('F11', 'Persediaan');
        $this->excel->getActiveSheet()->setCellValue('G11', 'Pemakaian');
        $this->excel->getActiveSheet()->setCellValue('H11', 'Rusak/Ed');
        $this->excel->getActiveSheet()->setCellValue('I11', 'Sisa Stok');
        $this->excel->getActiveSheet()->setCellValue('J11', 'Stok Optimum');
        $this->excel->getActiveSheet()->setCellValue('K11', 'Permintaan');
        $this->excel->getActiveSheet()->setCellValue('L11', 'Pemberian');
        $this->excel->getActiveSheet()->setCellValue('M11', 'Keterangan');

        //konten tabel
        for($i=0;$i<sizeof($query);$i++){
            //echo $query[$i]['nama_obat']."<br>";
            $j=$i+12;
            //$n=$j-9;
            //set cell A1 content with some text
            //$this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $query[$i]->kode_generik);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->nama_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->sediaan);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->stok_awal);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->terima);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->sedia);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->pakai);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->rusak_ed);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $query[$i]->stok_akhir);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $query[$i]->stok_opt);
            $this->excel->getActiveSheet()->setCellValue('K'.$j, $query[$i]->permintaan);
            $this->excel->getActiveSheet()->setCellValue('L'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('M'.$j, $query[$i]->ket);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $filename=$profil['nama_pusk'].'-'.$set['nama_pusk'].'-lplpo-'.$set['per'].'.csv'; //save our workbook as this file name
        //header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');

        //end fungsi konvert
        //session destroy
        //$this->session->sess_destroy();
    }

    function list_kosong(){
        $data['result']=$this->lplpo_kel_model->getlistempty();
        $this->load->view('manajemenlogistik/list_lplpo_kosong',$data);
    }

    function list_kosong_by($key){
        $data['result']=$this->lplpo_kel_model->getlistemptyby($key);
        $this->load->view('manajemenlogistik/list_lplpo_kosong',$data);
    }

    function do_import(){
        //$set['kec']=$this->session->userdata('id_lplpo');
        $id_lplpo=$this->session->userdata('new_id_lplpo');
        //ambil buffer
        $query=$this->db->query("select buffer from tb_institusi");
        $temp=$query->row_array();
        $buffer=$temp['buffer'];

        $config['upload_path'] = 'temp_upload/';
        $config['allowed_types'] = 'csv';
        //$file_name = $this->input->post('file_lplpo');
        //print_r($_FILES);
        $file_name = $_FILES['file_lplpo']['name'];
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file_lplpo'))
        {
            $confirm = array('msg' => $this->upload->display_errors());
            //echo "error upload";
            //$this->load->view('upload_form', $error);
        }
        else
        {
            $upload_data = $this->upload->data();
            $file =  $upload_data['full_path'];

            $this->load->library('excel');

            $objReader = PHPExcel_IOFactory::createReader('CSV');

            //Set to read only
            $objReader->setReadDataOnly(true);

            $objPHPExcel=$objReader->load('./temp_upload/'.$file_name);
            $objWorksheet=$objPHPExcel->getSheet(0);

            $highestRow = $objWorksheet->getHighestRow();
            // harusnya ngecek per id obat dan id lplpo

            if ($this->lplpo_kel_model->cek_eksis($id_lplpo)) {
                //update
                //echo 'update';
                $k = 0;
                $data_lplpo_baru = array();
                for ($i=12;$i<=$highestRow;$i++) {
                    $sisa_stokX = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue()-($objWorksheet->getCellByColumnAndRow(6,$i)->getValue()+$objWorksheet->getCellByColumnAndRow(7,$i)->getValue());
                    $stok_optX  = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue()+($buffer*$objWorksheet->getCellByColumnAndRow(6,$i)->getValue());

                    $data_lplpo['nama_obj']       = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                    $data_lplpo['sediaan']        = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                    $data_lplpo['stok_awal']      = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                    $data_lplpo['terima']         = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                    $data_lplpo['sedia']          = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
                    $data_lplpo['pakai']          = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                    $data_lplpo['rusak_ed']       = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
                    $data_lplpo['stok_akhir']     = $sisa_stokX;
                    $data_lplpo['stok_opt']       = $stok_optX;
                    $data_lplpo['permintaan']     = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
                    $data_lplpo['pemberian']      = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
                    $data_lplpo['ket']            = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();

                    $this->db->where('id_lplpo', $id_lplpo);
                    $this->db->where('kode_generik', strval($objWorksheet->getCellByColumnAndRow(0,$i)->getValue()));
                    $check = $this->db->update('tb_detail_lplpo_kel', $data_lplpo);
                    if ($check) {
                       // make array
                       $data_lplpo_baru[$k]['id_lplpo']       = $id_lplpo;
                       $data_lplpo_baru[$k]['kode_generik']   = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                       $data_lplpo_baru[$k]['nama_obj']       = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                       $data_lplpo_baru[$k]['sediaan']        = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                       $data_lplpo_baru[$k]['stok_awal']      = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                       $data_lplpo_baru[$k]['terima']         = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                       $data_lplpo_baru[$k]['sedia']          = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
                       $data_lplpo_baru[$k]['pakai']          = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                       $data_lplpo_baru[$k]['rusak_ed']       = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
                       $data_lplpo_baru[$k]['stok_akhir']     = $sisa_stokX;
                       $data_lplpo_baru[$k]['stok_opt']       = $stok_optX;
                       $data_lplpo_baru[$k]['permintaan']     = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
                       $data_lplpo_baru[$k]['pemberian']      = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
                       $data_lplpo_baru[$k]['ket']            = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
                       $k++;
                    }
                }
                //end for row
                if (!empty($data_lplpo_baru)) {
                    $this->db->insert_batch('tb_detail_lplpo_kel', $data_lplpo_baru);
                }
            } else {
                $k = 0;
                for($i=12;$i<=$highestRow;$i++)
                {
                    $data_lplpo[$k]['id_lplpo']       = $id_lplpo;
                    $data_lplpo[$k]['kode_generik']   = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
                    $data_lplpo[$k]['nama_obj']       = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
                    $data_lplpo[$k]['sediaan']        = $objWorksheet->getCellByColumnAndRow(2,$i)->getValue();
                    $data_lplpo[$k]['stok_awal']      = $objWorksheet->getCellByColumnAndRow(3,$i)->getValue();
                    $data_lplpo[$k]['terima']         = $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
                    $data_lplpo[$k]['sedia']          = $objWorksheet->getCellByColumnAndRow(5,$i)->getValue();
                    $data_lplpo[$k]['pakai']          = $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
                    $data_lplpo[$k]['rusak_ed']       = $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
                    $data_lplpo[$k]['stok_akhir']     = $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
                    $data_lplpo[$k]['stok_opt']       = $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
                    $data_lplpo[$k]['permintaan']     = $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
                    $data_lplpo[$k]['pemberian']      = $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
                    $data_lplpo[$k]['ket']            = $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
                    $k++;
                }
                $this->db->insert_batch('tb_detail_lplpo_kel', $data_lplpo);
            }

            $confirm = array('msg' => 'Data berhasil diimport');
            delete_files($upload_data['file_path']);
        }

        echo json_encode($confirm);

    }
}

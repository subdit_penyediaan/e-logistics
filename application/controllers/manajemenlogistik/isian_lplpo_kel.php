<?php

Class Isian_lplpo_kel extends CI_Controller {

    //var $title = 'Isian LPLPO';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->id_user = $this->session->userdata('id');
        $this->url =base_url();
        $this->load->model('manajemenlogistik/isian_lplpo_kel_model');
        $this->load->model('profil/setting_institusi_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/isian_lplpo_kel',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
		$data['id_lplpo']=$this->input->post('id_lplpo');
		$data['kode_generik']=$this->input->post('kode_obat');
		$data['terima']=$this->input->post('terima');
		$data['sedia']=$this->input->post('sedia');
		$data['pakai']=$this->input->post('pakai');
		$data['stok_opt']=$this->input->post('stok_op');
		$data['permintaan']=$this->input->post('minta');
		$data['ket']=$this->input->post('ket');
		$data['dana']=$this->input->post('dana');
		$data['stok_awal']=$this->input->post('stok_awal');
		$data['rusak_ed']=$this->input->post('rusak_ed');
        $data['nama_obj']=$this->input->post('nama_obj');
        $data['sediaan']=$this->input->post('desc');

		$result = $this->isian_lplpo_kel_model->input_data_m($data);
        if ($result) {
            $output['status'] = "success";
            $output['message'] = "Data berhasil disimpan";
        } else {
            $output['status'] = "error";
            $output['message'] = "Data gagal disimpan";
        }

        echo json_encode($output);
	}

    function get_rata2pemakaian(){
        $kode_pusk=$this->input->get('kd_pusk');
        $pd=$this->input->get('pd');
        $kg=$this->input->get('kode_generik');
        $periode =date('Y-m');
        $tanggal_mula = date( "Y-m", strtotime( $periode."- ".$pd. "month" ) );
        $query = $this->db->query("
            SELECT SUM(dl.pakai)/$pd as rata2pakai
            FROM tb_detail_lplpo_kel dl
            JOIN tb_lplpo l ON l.id=dl.id_lplpo
            WHERE dl.kode_generik = '$kg' AND l.kode_pusk = '$kode_pusk' AND l.periode >= '$tanggal_mula' AND l.periode < NOW()
            GROUP BY dl.kode_generik
            ")->row_array();
        if(empty($query)){
            $query['rata2pakai']=0;
        }
        print json_encode($query);
    }

	function get_data_isian_lplpo($id_lplpo = ''){
		$data['base_url']=$this->url;
		$data['result']=$this->isian_lplpo_kel_model->getData($id_lplpo);
		if ($id_lplpo != '') {
        $this->load->view('manajemenlogistik/isian_lplpo_kel_list',$data);
    } else {
        $alert = '
        <div class="alert alert-danger" role="alert">
          <b>ERROR</b><br>
          <p>Terjadi kesalahan saat mengambil data. silakan reload halaman (F5)</p>
        </div>
        ';

        echo $alert;
    }
	}

    function search_detail($id_lplpo) {
        $key = $this->input->post('keyword');
        $data['links'] = '';
        $data['base_url']=$this->url;
        $data['result']=$this->isian_lplpo_kel_model->getData($id_lplpo, $key);

        $this->load->view('manajemenlogistik/isian_lplpo_kel_list', $data);

    }


	function delete_list() {
        $kode=$this->input->post('kode');
        // check
        $this->db->where('id', $kode);
        $lplpo = $this->db->get('tb_detail_lplpo_kel')->row_array();

        $this->db->where('id_lplpo', $lplpo['id_lplpo']);
        $this->db->where('kode_obat_req', $lplpo['kode_generik']);
        $this->db->where('pemberian >', 0);
        $check = $this->db->get('tb_detail_distribusi');
        if ($check->num_rows() > 0) {
            $response['status'] = "error";
            $response['message'] = "Data gagal dihapus, sudah ada transaksi";
        } else {
            $this->isian_lplpo_kel_model->deleteData($kode);

            $response['status'] = "success";
            $response['message'] = "Data berhasil dihapus";
        }

        echo json_encode($response);
    }

 	function get_detail($nomorlplpo, $kode_pusk){
        $data['lplpo']=$this->isian_lplpo_kel_model->getDataDetaillplpo($nomorlplpo);
        $data['value']=$this->db->get('tb_institusi')->row_array();
        $user = getUser($this->id_user);
        if (($user['kode_puskesmas']==$kode_pusk)||($user['group_id']!=3)) {
            $this->load->view('manajemenlogistik/isian_lplpo_kel',$data);
        } else {
            $this->load->view('wrong_access');
        }
    }

     function get_list_obat() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->isian_lplpo_kel_model->GetListObat($key);
        print json_encode($query);
    }

    function getDetailPuskesmas($id_lplpo)
    {
        $this->db->where('id',$id_lplpo);
        $lplpo = $this->db->get('tb_lplpo')->row_array();

        $this->db->where('KD_PUSK',$lplpo['kode_pusk']);
        $puskesmas = $this->db->get('ref_puskesmas')->row_array();

        return $puskesmas;
    }

    function update_data(){
        //ambil buffer
        // $query=$this->db->query("select buffer,periode_distribusi from tb_institusi");
        // $temp=$query->row_array();
        // $buffer=$temp['buffer'];
        // $pd = $temp['periode_distribusi'];

    	  //where
    	  $arr_id_lplpo = $this->input->post('hidden_id_lplpo');
        $temp = $this->getDetailPuskesmas($arr_id_lplpo[0]);
        $buffer=$temp['buffer'];
        $pd = $temp['periode_distribusi'];
        //get periode, select interval distribusi

		    $arr_kode_obat = $this->input->post('hidden_id_detaillplpo');

    		//konten tb_detail_lplpo
        $arr_stok_awal = $this->input->post('stok_awal');
        $arr_penerimaan = $this->input->post('penerimaan');
        $arr_persediaan = $this->input->post('persediaan');
        $arr_pemakaian = $this->input->post('pemakaian');
        $arr_rusak_ed = $this->input->post('rusak_ed');
        $arr_sisa_stok = $this->input->post('sisa_stok');
        $arr_stok_opt = $this->input->post('stok_opt');
        $arr_permintaan = $this->input->post('permintaan');
        $arr_ket = $this->input->post('ket');
        //konten tb_detail_distribusi
        $id_pemberian = $this->input->post('pemberian');

        if($pd > 0)
        {
            for($i=0;$i<sizeof($arr_kode_obat);$i++) {
                if(empty($arr_persediaan[$i])){
                    //$arr_persediaan[$i]=$arr_stok_awal[$i]+$arr_penerimaan[$i];
                }
                if($arr_kode_obat[$i] && $arr_id_lplpo!='') {
                    $arr_persediaan[$i]=$arr_stok_awal[$i]+$arr_penerimaan[$i];
                    $sisa_stokX=$arr_persediaan[$i]-($arr_pemakaian[$i]+$arr_rusak_ed[$i]);
                    //get nilai rata2 pemakaian WHERE id_lplpo=? AND id=?
                    $x = $this->isian_lplpo_kel_model->getRata2Pakai($pd,$arr_kode_obat[$i],$arr_id_lplpo[$i]);
                    if($x->num_rows() > 0)
                    {
                        $y = $x->row_array();
                        $pemakaian_rata2 = round($y['rata2pakai']);
                    }else{
                        $pemakaian_rata2 = 0;
                    }

                    $stok_optimum=$arr_pemakaian[$i]+($buffer*$pemakaian_rata2);

                    if($arr_pemakaian[$i] == '') $arr_pemakaian[$i] = 0;
                    if($arr_rusak_ed[$i] == '') $arr_rusak_ed[$i] = 0;
                    if($arr_permintaan[$i] == '') $arr_permintaan[$i] = 0;

                    $ins = $this->db->query("
                       UPDATE tb_detail_lplpo_kel
                       SET stok_awal=?,terima=?,sedia=?,pakai=?,rusak_ed=?,stok_akhir=?,
                       	stok_opt=?,permintaan=?,ket=?
    				           WHERE id_lplpo=? AND id=?
                    ",
                    array($arr_stok_awal[$i],
                            $arr_penerimaan[$i],
                            $arr_persediaan[$i],
                            $arr_pemakaian[$i],
                            $arr_rusak_ed[$i],
                            $sisa_stokX,
                            $stok_optimum,
                            $arr_permintaan[$i],
                            $arr_ket[$i],
                    		    $arr_id_lplpo[$i],
                    		    $arr_kode_obat[$i])
                    );
                }
            }
            echo $arr_id_lplpo[0];
        }else{
            echo 0;
        }
	}
}
?>

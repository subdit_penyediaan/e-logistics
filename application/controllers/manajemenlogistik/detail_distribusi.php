<?php

Class Detail_Distribusi extends CI_Controller {

    var $title = 'Detail Distribusi';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('manajemenlogistik/detail_distribusi_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/detail_distribusi',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
		$data = array();
		$data['id_distribusi']=$this->input->post('id_distribusi');
		$data['kode_obat_req']=0;//$this->input->post('kode_obat_req');
		$data['jml_minta']=0;//$this->input->post('jml_minta');
		$data['kode_obat_res']=$this->input->post('kode_obat_res');
        $data['jml_beri']=$this->input->post('jml_beri');
        $data['no_batch']=$this->input->post('no_batch');
        $data['id_stok'] = $this->input->post('id_stok_hidden');
        $data['jml_max']= $this->input->post('stok_hidden');
        $data['tanggal_pelayanan']= $this->input->post('tanggal_pelayanan');
		if($data['jml_beri']>$data['jml_max']){
            //$continue=false;$n=$k+1;
			$message['cek']=1;
            $message['confirm']='maaf, pemberian melebihi stok';
        }else{
            $this->detail_distribusi_model->input_data_m($data);
            $cek = $this->detail_distribusi_model->getDataUp($data['id_distribusi']);
            // jika puskesmas
            if (substr($cek['kode_up'], 0, 1)=='P') {
                $lplpo = $this->detail_distribusi_model->getLPLPO($cek['kode_up'],$cek['periode'])->row_array();
                $inn_fda = $this->detail_distribusi_model->getObatGenerikFromStock($data['kode_obat_res'])->row_array();
                if ((!empty($inn_fda['kode_generik']))||($inn_fda['kode_generik'] > 0)) {
                    if(!empty($lplpo)) {
                        $tempLPLPO['id_lplpo'] = $lplpo['id'];
                        $tempLPLPO['kode_generik'] = $inn_fda['kode_generik'];
                        $tempLPLPO['pemberian'] = $data['jml_beri'];
                        $result = $this->detail_distribusi_model->updateLPLPO($tempLPLPO);
                        if ($result) {
                            $message['confirm']='Sukses input data dan data LPLPO terupdate';
                        } else {
                            $message['confirm']='Sukses input data dan gagal update LPLPO';
                        }
                    } else {
                        $message['confirm']='Sukses input data distribusi. Tidak ada LPLPO yang tersedia';
                    }
                } else {
                    $message['confirm'] = 'Sukses input data. Mohon update pemberian LPLPO secara manual';
                }
            } else {
                $message['confirm']='Sukses input data distribusi';
            }
            $message['cek']=0;
        }
		echo json_encode($message);
	}

    function input_dataX(){
        $time=date('Y-m-d H:i:s A');
        $id_user=$this->session->userdata('id');

        $id_distribusi=$this->input->post('id_distribusi');
        $id_lplpo = 0;
        $arr_kode_obat_req = $this->input->post('kode_obat_hidden_minta');
        $jml_obat_req = $this->input->post('jml_minta');

        $arr_kode_obat = $this->input->post('kode_obat_hidden_beri');
        $arr_jml = $this->input->post('jml_beri');
        $arr_no_batch = $this->input->post('no_batch');
        $id_stok = $this->input->post('id_stok_hidden');
        $arr_jml_max= $this->input->post('stok_hidden');

        $continue=true;
        for($k=0;$k<sizeof($arr_jml_max);$k++){
            if($arr_jml[$k]>$arr_jml_max[$k]){
                $continue=false;$n=$k+1;
                $message['cek']=1;
                $message['confirm']='Pemberian No.'.$n.' melebihi stok';
                echo json_encode($message);
                die;
            }
        }
        //print_r($arr_concept_name);
        //print_r($arr_concept_id);
        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_jml!='') {
                //input ke tabel detail distribusi

                $ins = $this->db->query("
                    INSERT INTO `tb_detail_distribusi`
                    (`id_lplpo`, `kode_obat_req`, `kode_obat_res`,
                        `pemberian`, `no_batch`,
                        `create_time`, `create_by`, `id_distribusi`, `jum_obat_req`)
                    VALUES (?,?,?,?,?,?,?,?,?)
                ",
                array($id_lplpo,
                        $arr_kode_obat_req,
                        $arr_kode_obat[$i],
                        $arr_jml[$i],
                        $arr_no_batch[$i],
                        $time,
                        $id_user,
                        $id_distribusi,
                        $jml_obat_req)
                );
                //ngurangin stok
                $upt = $this->db->query("
                    update tb_stok_obat
                    set stok= (stok-?)
                    where id_stok = ?
                ",
                array($arr_jml[$i],
                        $id_stok[$i])
                );

            }
        }
        $this->db->query("
            update tb_distribusi
            set status= 'sudah diproses'
            where id_distribusi = ?
        ",
        array($id_distribusi)
        );
        $message['cek']=0;
        $message['confirm']='Sukses input data';
        //$message['id_lplpo']=$id_lplpo[0];
        echo json_encode($message);

        //$this->Detail_Stok_Opnam_Model->updateStatus($this->input->post('id_stok_opnam'));
        //return $ins;
    }


	function get_data_detail_distribusi($id){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/detail_distribusi/get_data_detail_distribusi/'.$id;
		$config['total_rows'] = $this->detail_distribusi_model->countAllDataCito($id); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 5;

		$this->pagination->initialize($config);
        $data['id']=$id;
		$data['result']=$this->detail_distribusi_model->getData($config['per_page'],$this->uri->segment(5),$id);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/detail_distribusi_list',$data);
		//$this->load->view('list_puskesmas');
	}


	function delete_list() {
        $kode=$this->input->post('kode');
        $this->Penerimaan_Obat_Model->deleteData($kode);
    }

 	function get_detail($id){
     	//$qry=$this->db->query("select * from tb_penerimaan where no_faktur='$nomorfaktur'");
		//$data=$qry->row_array();
        $data['base_url']=$this->url;
		$data['cito']=$this->detail_distribusi_model->getInfoDistribusi($id);
		//$data['satuan']=$this->Detail_Faktur_Model->getSatuan();
		$this->load->view('manajemenlogistik/detail_distribusi',$data);
		//print_r($data['satuan']);

		//echo $satuan[1]['satuan_obat'];
     }

     function get_list_obat() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_distribusi_model->GetListObat($key);
        print json_encode($query);
    }

    function get_list_obat_grid(){
        $response= new stdClass();

        $searchTerm=$this->input->get("searchTerm");
        $page=$this->input->get("page");
        $sidx=$this->input->get("sidx");
        $limit=$this->input->get("rows");
        $sord=$this->input->get("sord");
        //$rows=$this->input->get("rows");
        if(!$sidx) $sidx =1;
        if ($searchTerm=="") {
            $searchTerm="%";
        } else {
            $searchTerm = "%" . $searchTerm . "%";
        }

        $result = $this->db->query("
            SELECT COUNT(*) AS count
            FROM ref_obat_all oa
            WHERE oa.object_name like '$searchTerm'
            ");
        $row = $result->row_array();
        $count = $row['count'];

        if( $count >0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) $page=$total_pages;
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)

        if($total_pages!=0)
            $SQL = "
                    SELECT oa.object_name as brand,oa.id_obat as kode_obat
                    FROM ref_obat_all oa

                    WHERE oa.object_name like '$searchTerm'
                    ORDER BY $sidx $sord LIMIT $start , $limit";
        else $SQL = "
                    SELECT oa.object_name as brand,oa.id_obat as kode_obat
                    FROM ref_obat_all oa

                    WHERE oa.object_name like '$searchTerm'
                    ORDER BY $sidx $sord";
        $query=$this->db->query($SQL);

        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;
        $i=0;
        //$r = array();
        foreach ($query->result_array() as $key) {
            $response->rows[$i]['kode_obat']=$key['kode_obat'];
            //$response->rows[$i]['name']=$key['inn'];
            //$response->rows[$i]['kemasan']=$key['detil_kemasan'];
            $response->rows[$i]['brand']=$key['brand'];
            //$response->rows[$i]['kemasan']=$key['kemasan_unit'];
            //$response->rows[$i]['sat_jual']=$key['satuan_jual_unit'];
            //$response->rows[$i]['kategori']=$key['kategori'];
            //$response->rows[$i]['nama_generik']=$key['nama_generik'];
            //$response->rows[$i]['id_generik']=$key['id_generik'];
            $i++;
        }
        //return $r;

        //$query = $this->detail_faktur_model->GetListObat($key);
        echo json_encode($response);
    }

    function get_list_obat2() {
        $key=$this->input->get("term");
        //var $term;
        $query = $this->detail_distribusi_model->GetListObatBeri($key);
        print json_encode($query);
    }

    function update_data(){
    	//where
    	$arr_id_lplpo = $this->input->post('hidden_id_lplpo');
		$arr_kode_obat = $this->input->post('hidden_kode_obat');
		//konten tb_detail_lplpo
        $arr_pemakaian = $this->input->post('pemakaian');
        $arr_rusak_ed = $this->input->post('rusak_ed');
        $arr_sisa_stok = $this->input->post('sisa_stok');
        $arr_stok_opt = $this->input->post('stok_opt');
        $arr_permintaan = $this->input->post('permintaan');
        $arr_ket = $this->input->post('ket');
        //konten tb_detail_distribusi
        $id_pemberian = $this->input->post('pemberian');
        //print_r($arr_concept_name);
        //print_r($arr_concept_id);
        for($i=0;$i<sizeof($arr_kode_obat);$i++) {
            if($arr_kode_obat[$i] && $arr_id_lplpo!='') {
                //$xarr_treatment_id[] = $arr_treatment_id[$i];
                $ins = $this->db->query("
                   UPDATE tb_detail_lplpo SET pakai=?,rusak_ed=?,stok_akhir=?,
                   	stok_opt=?,permintaan=?,ket=?
					WHERE id_lplpo=? AND kode_obat=?

                ",
                array($arr_pemakaian[$i],
                        $arr_rusak_ed[$i],
                        $arr_sisa_stok[$i],
                        $arr_stok_opt[$i],
                        $arr_permintaan[$i],
                        $arr_ket[$i],
                		$arr_id_lplpo[$i],
                		$arr_kode_obat[$i])
                );
                //echo $this->db->last_query();
                /*$upt = $this->db->query("
                    update tb_stok_obat
                    set stok=?
                    where id_stok = ?
                ",
                array($arr_fisik_stok[$i],
                        $id_stok_obat[$i])
                );*/
            }
        }

        //$this->Detail_Stok_Opnam_Model->updateStatus($this->input->post('id_stok_opnam'));
        //return $ins;
	}

    function printOut($id){

        $query=$this->detail_distribusi_model->show_data_inn($id);
        $profil_up=$this->detail_distribusi_model->getDataUp($id);
        $temp=$this->db->get('tb_institusi')->row_array();
        if($temp['levelinstitusi']==3){
            $namakab=$this->db->where('CKabID',$temp['id_kab'])->get('ref_kabupaten')->row_array();
            $kabupaten='DINAS KESEHATAN KABUPATEN '.strtoupper($namakab['CKabDescr']);
            $tt=$namakab['CKabDescr'];
        }elseif($temp['levelinstitusi']==2){
            $namakab=$this->db->where('CPropID',$temp['id_prop'])->get('ref_propinsi')->row_array();
            $kabupaten='DINAS KESEHATAN PROVINSI '.strtoupper($namakab['CPropDescr']);
            $tt=$namakab['CPropDescr'];
        }else{
            $kabupaten='KEMENTERIAN KESEHATAN REPUBLIK INDONESIA';
            $tt='Jakarta';
        }
        //fungsi konvert

        //load our new PHPExcel library
        $this->load->library('excel');
        $styleArrayHeader = array(
            'font' => array(
                'bold' => true
            )
        );
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0)->mergeCells('A1:J1');
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('sbbk');

        //JUDUL

        //make the font become bold
        //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
        //merge cell A1 until D1
        //$this->excel->getActiveSheet()->mergeCells('A1:M1');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A1', 'SURAT BUKTI BARANG KELUAR (SBBK)');

        $this->excel->getActiveSheet()->mergeCells('A2:J2');
        //set aligment to center for that merged cell (A1 to D1)
        $this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->getStyle('A2')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->setCellValue('A2', 'INSTALASI FARMASI '.$kabupaten);

        $this->excel->getActiveSheet()->mergeCells('A3:J3');
        $this->excel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        //$this->excel->getActiveSheet()->setCellValue('A3', $kabupaten);

        //INFO LOCATION
        $this->excel->getActiveSheet()->setCellValue('A5', 'Nomor Dokumen');
            $this->excel->getActiveSheet()->setCellValue('C5', ': '.$profil_up['no_dok']);
        $this->excel->getActiveSheet()->setCellValue('A6', 'Unit Penerima');
            $this->excel->getActiveSheet()->setCellValue('C6', ': '.$profil_up['unit_eks']);
        $styleArray = array(
             'borders' => array(
                 'allborders' => array(
                     'style' => PHPExcel_Style_Border::BORDER_THIN
                 )
             )
         );
        //HEADER
        for($col = 'B'; $col !== 'K'; $col++) {
            $this->excel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        $this->excel->getActiveSheet()->setCellValue('A9', 'No');
        $this->excel->getActiveSheet()->setCellValue('B9', 'Kode');
        $this->excel->getActiveSheet()->setCellValue('C9', 'Nama Obat/Perbekkes');
        $this->excel->getActiveSheet()->setCellValue('D9', 'Satuan');
        $this->excel->getActiveSheet()->setCellValue('E9', 'Jumlah Distribusi');
        $this->excel->getActiveSheet()->setCellValue('F9', 'No. Batch');
        $this->excel->getActiveSheet()->setCellValue('G9', 'Expired');
        $this->excel->getActiveSheet()->setCellValue('H9', 'Harga (Rp)');
        $this->excel->getActiveSheet()->setCellValue('I9', 'Jumlah (Rp)');
        $this->excel->getActiveSheet()->setCellValue('J9', 'Keterangan');

        //konten tabel
        $totalharga=0;
        for($i=0;$i<sizeof($query);$i++){
            if(($query[$i]->dana)==''){
                $anggaran = "";
            }else {
                $anggaran = $query[$i]->dana.' '.$query[$i]->tahun_anggaran;
            }
            $j=$i+10;
            $n=$j-9;
            $total=$query[$i]->pemberian*$query[$i]->harga;
            $totalharga=$totalharga+$total;
            //set cell A1 content with some text
            $this->excel->getActiveSheet()->setCellValue('A'.$j, $n);
            $this->excel->getActiveSheet()->setCellValue('B'.$j, $query[$i]->kode_obat);
            $this->excel->getActiveSheet()->setCellValue('C'.$j, $query[$i]->nama_obj);
            $this->excel->getActiveSheet()->setCellValue('D'.$j, $query[$i]->deskripsi);
            $this->excel->getActiveSheet()->setCellValue('E'.$j, $query[$i]->pemberian);
            $this->excel->getActiveSheet()->setCellValue('F'.$j, $query[$i]->no_batch);
            $this->excel->getActiveSheet()->setCellValue('G'.$j, $query[$i]->expired);
            $this->excel->getActiveSheet()->setCellValue('H'.$j, $query[$i]->harga);
            $this->excel->getActiveSheet()->setCellValue('I'.$j, $total);
            $this->excel->getActiveSheet()->setCellValue('J'.$j, $anggaran);
            //change the font size
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setSize(20);
            //make the font become bold
            //$this->excel->getActiveSheet()->getStyle('A'.$i)->getFont()->setBold(true);
            //merge cell A1 until D1
            //$this->excel->getActiveSheet()->mergeCells('A1:D1');
            //set aligment to center for that merged cell (A1 to D1)
            //$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }

        $this->excel->getActiveSheet()->getStyle('A9:J9')->applyFromArray($styleArrayHeader);
        $this->excel->getActiveSheet()->getStyle('A9:J'.($j+1))->applyFromArray($styleArray);
        unset($styleArray);

        $tanggalsbbk= $tt.', '.$profil_up['tanggal_trans'];
        $this->excel->getActiveSheet()->setCellValue('I'.($j+1), $totalharga);
        $this->excel->getActiveSheet()->setCellValue('A'.($j+3), "Total Berat: ");
        $this->excel->getActiveSheet()->setCellValue('A'.($j+4), "Total Koli: ");

        $this->excel->getActiveSheet()->getStyle('G'.($j+6))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('G'.($j+6).':J'.($j+6));
        $this->excel->getActiveSheet()->setCellValue('G'.($j+6), $tanggalsbbk);

        $this->excel->getActiveSheet()->getStyle('G'.($j+7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('G'.($j+7).':'.'J'.($j+7));
        $this->excel->getActiveSheet()->setCellValue('G'.($j+7), 'Yang menyerahkan/memeriksa');

        $this->excel->getActiveSheet()->getStyle('G'.($j+12))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('G'.($j+12).':'.'J'.($j+12));
        $this->excel->getActiveSheet()->setCellValue('G'.($j+12), '(.........................)');

        $this->excel->getActiveSheet()->getStyle('A'.($j+7))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A'.($j+7).':'.'C'.($j+7));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+7), 'Yang menerima');
        $this->excel->getActiveSheet()->getStyle('A'.($j+12))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A'.($j+12).':'.'C'.($j+12));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+12), $profil_up['user']);
        $this->excel->getActiveSheet()->getStyle('A'.($j+13))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('A'.($j+13).':'.'C'.($j+13));
        $this->excel->getActiveSheet()->setCellValue('A'.($j+13), "(".$profil_up['nip'].")");

        $this->excel->getActiveSheet()->getStyle('D'.($j+14))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('D'.($j+14).':'.'F'.($j+14));
        $this->excel->getActiveSheet()->setCellValue('D'.($j+14), 'Mengetahui,');
        $this->excel->getActiveSheet()->getStyle('D'.($j+19))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $this->excel->getActiveSheet()->mergeCells('D'.($j+19).':'.'F'.($j+19));
        $this->excel->getActiveSheet()->setCellValue('D'.($j+19), '(.........................)');

        // $filename=$profil_up['unit_eks'].'-SBBK.xlsx';
        $filename=$profil_up['unit_eks'].'-SBBK.xls'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache

        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        // $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel2007');
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'CSV');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
        //end fungsi konvert
    }

     function update_lplpodist(){
        $data = $this->input->post('dt');
        $cek = $this->checkStok($data['id_stok'],$data['jml_awal'],$data['jml_beri_new']);
        $id_user = $this->session->userdata('id');
        if ($cek) {
            if(!empty($data['id_dd'])) {
                //ngembalikan ke stok
                $this->db->query("
                    update tb_stok_obat
                    set stok= (stok+?)
                    where id_stok = ?
                ",
                array($data['jml_awal'],
                        $data['id_stok'])
                );

                //update ke detail_distribusi
                $this->db->query("
                    update tb_detail_distribusi
                    set
                        kode_obat_res = ?,
                        pemberian = ?,
                        no_batch =?,
                        id_stok =?,
                        update_time =?,
                        create_by =?
                    where id = ?
                ",
                array($data['kode_obat'],
                        $data['jml_beri_new'],
                        $data['no_batch'],
                        $data['id_stok'], date('Y-m-d H:i:s'), $id_user,
                        $data['id_dd'])
                );
            } else {
                //insert ke detail_distribusi
                $time = $data['happen_date'];
                $this->db->query("
                    INSERT INTO `tb_detail_distribusi`
                    (`id_lplpo`, `kode_obat_req`, `kode_obat_res`,
                            `pemberian`, `no_batch`,
                            `create_time`, `create_by`,`id_stok`)
                        VALUES (?,?,?,?,?,?,?,?)
                ",
                array($data['id_lplpo'],
                    $data['kode_obat_req'],$data['kode_obat'],
                        $data['jml_beri_new'],
                        $data['no_batch'],$time,$id_user,$data['id_stok'])
                );
            }

            //ngurangin stok
            $this->db->query("
                update tb_stok_obat
                set stok= (stok-?)
                where id_stok = ?
            ",
            array($data['jml_beri_new'],
                    $data['id_stok'])
            );

            if (isset($data['id_dl'])) {
                $this->db->query("
                    update tb_detail_lplpo_kel
                    set pemberian= ?
                    where id = ?
                ",
                array($data['jml_beri_new'],
                        $data['id_dl'])
                );

                $this->db->query("
                    update tb_detail_lplpo
                    set pemberian= ?
                    where id = ?
                ",
                array($data['jml_beri_new'],
                        $data['id_dl'])
                );
            }

            $response['status'] = '1';
            $response['message'] = 'Data berhasil diupdate';
        } else {
            $response['status'] = '0';
            $response['message'] = 'Data tidak berhasil diupdate. Stok tidak cukup';
        }

        echo json_encode($response);

    }

    function delete(){
        $data = $this->input->post('dt');
        if(($data['jml_awal']==0)&&($data['jml_beri_new']==0)){
            $this->db->where('id',$data['id_dd']);
            if($this->db->delete('tb_detail_distribusi')){
                $response['status'] = '1';
                $response['message'] = 'Data berhasil dihapus';
            }else{
                $response['status'] = '0';
                $response['message'] = 'Data tidak berhasil dihapus.';
            }
        }else{
            $response['status'] = '0';
            $response['message'] = 'Data tidak berhasil dihapus. Ada transaksi yang terjadi';
        }

        echo json_encode($response);

    }

    public function checkStok($id_stok,$jml_kembali='0',$jml_beri)
    {
        $stok_status = false;

        $this->db->where('id_stok',$id_stok);
        $stok = $this->db->get('tb_stok_obat')->row_array();

        $temp_stok = $stok['stok']+$jml_kembali;
        if($temp_stok >= $jml_beri)
        {
            $stok_status = true;
        }

        return $stok_status;
    }

    function delete_cito(){
        $data = $this->input->post('dt');

        //ngembalikan ke stok
        $this->db->query("
            update tb_stok_obat
            set stok= (stok+?)
            where id_stok = ?
        ",
        array($data['jml_awal'],
                $data['id_stok'])
        );

        $this->db->query("
            delete
            from tb_detail_distribusi
            where id=?
            ",array($data['id_dd'])
            );
    }

    public function deleteAdmin($detail_distribusi_id) {
        $this->db->where('id',$detail_distribusi_id);
        $query = $this->db->delete('tb_detail_distribusi');
        if($query){
            $response['status'] = '1';
            $response['message'] = 'Data berhasil dihapus';
        }else{
            $response['status'] = '0';
            $response['message'] = 'Data tidak berhasil dihapus.';
        }

        echo json_encode($response);
    }

    public function updateAdmin($detail_distribusi_id)
    {

    }

    public function updateDate($lplpo_id)
    {
        $data['create_time'] = $this->input->post('dateNew');
        $data['update_time'] = date('Y-m-d H:i:s');
        $this->db->where('id_lplpo', $lplpo_id);
        $query = $this->db->update('tb_detail_distribusi', $data);

        if ($query) {
            $lplpo['no_dok'] = $this->input->post('no_dok');
            $lplpo['update_time'] = date('Y-m-d H:i:s');
            $this->db->where('id', $lplpo_id);
            $query = $this->db->update('tb_lplpo', $lplpo);

            $response['status'] = '1';
            $response['message'] = 'Data berhasil diupdate';
        }else{
            $response['status'] = '0';
            $response['message'] = 'Data tidak berhasil diupdate. Stok tidak cukup';
        }

        echo json_encode($response);
    }
}
?>

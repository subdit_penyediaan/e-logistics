<?php

Class Detail_Faktur extends CI_Controller {

    var $title = 'Detail Faktur';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        //$this->response= new stdClass();
        $this->load->model('manajemenlogistik/detail_faktur_model');
    }

    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('manajemenlogistik/detail_faktur',$data);
		}else{
			redirect('login');
		}

    }

	function input_data(){
		$data = array();
		$data['id_faktur']=$this->input->post('id_faktur');
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['nama_obat']= trim(preg_replace('/\s+/', ' ', $this->input->post('nama_obat')));
		$data['no_batch']=clean($this->input->post('no_batch'));
		$data['jumlah']=$this->input->post('jumlah');
		$data['tanggal']=$this->input->post('tanggal');
		$data['harga_beli']=str_replace(",",".", $this->input->post('harga_beli'));
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['jumlah_kec']=$this->input->post('jumlah_kec');
		$data['jml_rusak']=$this->input->post('jml_rusak');

		$data['id_fornas']=$this->input->post('id_fornas');
		$data['id_ukp4']=$this->input->post('id_ukp4');
		$data['kode_atc']=$this->input->post('kode_atc');
		$data['kategori']=$this->input->post('kategori');
		$data['kemasan']=$this->input->post('kemasan');
		$data['sat_jual']=$this->input->post('sat_jual');
		$data['kode_generik']=$this->input->post('kode_generik');


    $restriction = getProfile();
    if ($restriction['levelinstitusi'] != 1) {
        $this->db->where('no_batch', $data['no_batch']);
        $check = $this->db->get('tb_stok_obat');
        $match = $check->num_rows();
    } else {
        $match = 0;
    }


		if ($match > 0) {
			$result['status'] = 'error';
			$result['message'] = 'Data gagal disimpan, nomor batch sudah tersedia';
		} else {
			$query=$this->detail_faktur_model->input_data_m($data);
			if ($query) {
				$result['status'] = 'success';
				$result['message'] = 'Data berhasil disimpan';
			} else {
				$result['status'] = 'error';
				$result['message'] = 'Data gagal disimpan';
			}
		}

		echo json_encode($result);
	}

	function update_kelompok(){
		$data['id_fornas']=$this->input->post('id_fornas');
		//$data['id_ukp4']=$this->input->post('id_ukp4');
		$data['kode_atc']=$this->input->post('kode_atc');
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['kode_inn']=$this->input->post('kode_inn');
		$data['kode_obatprogram']=$this->input->post('kode_program');
		$data['kode_obatindikator']=$this->input->post('kode_indikator');
		$query=$this->detail_faktur_model->updateKelompok($data);
	}

	function get_data_detail_faktur($nomorfaktur){
		$data['base_url']=$this->url;

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/manajemenlogistik/detail_faktur/get_data_detail_faktur/'.$nomorfaktur;
		$config['total_rows'] = $this->detail_faktur_model->countAllData($nomorfaktur); //untuk menghitung banyaknya rows
		//$data['total_rows'] = $this->detail_faktur_model->countAllData($nomorfaktur); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 10;
		//$data['total_rows'] = 10;
		$config['per_page'] = 15; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 5;

		$this->pagination->initialize($config);
		$data['satuan']=$this->db->get('ref_obat_satuan')->result_array();
		if($this->uri->segment(5)==""){
			$start='0';
		}else $start=$this->uri->segment(5);
		$data['result']=$this->detail_faktur_model->getData($config['per_page'],$start,$nomorfaktur);
		$data['links'] = $this->pagination->create_links();
		$this->load->view('manajemenlogistik/detail_faktur_list',$data);
		//$this->load->view('list_puskesmas');
	}


	function delete_list() {
        $kode=$this->input->post('kode');
        $this->detail_faktur_model->deleteData($kode);
    }

    function get_detail($nomorfaktur){
     	//$qry=$this->db->query("select * from tb_penerimaan where no_faktur='$nomorfaktur'");
		//$data=$qry->row_array();
		$data['base_url']=$this->url;
		$data['detailfaktur']=$this->detail_faktur_model->getDataDetailFaktur($nomorfaktur);
		$data['satuan']=$this->detail_faktur_model->getSatuan();
		$this->load->view('manajemenlogistik/detail_faktur',$data);
		//print_r($data['satuan']);

		//echo $satuan[1]['satuan_obat'];
     }

    function get_list_obat1() {
        $data = $this->detail_faktur_model->GetListObat1($this->input->post('q'));
        for($i=0;$i<sizeof($data);$i++) {
            echo $data[$i]['nama_obat'] . "|" . $data[$i]['kode_obat'] . "\n";
        }
    }

    function get_list_obat(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListObat($key);
        print json_encode($query);
    }

    function get_list_obat_grid($key=''){
    	$where='';

    	if(!empty($key)){
    		$temp=$this->db->select('nama_prod_obat')->where('id',$key)->get('ref_pabrik')->row_array();
    		$word=explode(' ', $temp['nama_prod_obat']);
    		$where='AND org_pembuat like "%'.$word[0].'%"';
    	}
    //	print_r($where);
    //}
    //function aneh(){
    	$response= new stdClass();

        $searchTerm=$this->input->get("searchTerm");
        $page=$this->input->get("page");
        $sidx=$this->input->get("sidx");
        $limit=$this->input->get("rows");
        $sord=$this->input->get("sord");
        //$rows=$this->input->get("rows");
		if(!$sidx) $sidx =1;
		if ($searchTerm=="") {
			$searchTerm="%";
		} else {
			$searchTerm = "%" . $searchTerm . "%";
		}

		$result = $this->db->query("
			SELECT COUNT(*) AS count FROM ref_obat_all WHERE object_name like '$searchTerm' $where
			");
		$row = $result->row_array();
		$count = $row['count'];

		if( $count >0 ) {
			$total_pages = ceil($count/$limit);
		} else {
			$total_pages = 0;
		}

		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit; // do not put $limit*($page - 1)

		if($total_pages!=0)
			$SQL = "
					SELECT * FROM ref_obat_all
					WHERE object_name like '$searchTerm' $where
					ORDER BY $sidx $sord LIMIT $start , $limit";
		else $SQL = "
					SELECT * FROM ref_obat_all
					WHERE object_name like '$searchTerm' $where
					ORDER BY $sidx $sord";
		$query=$this->db->query($SQL);

		$response->page = $page;
		$response->total = $total_pages;
		$response->records = $count;
		$i=0;
		//$r = array();
	    foreach ($query->result_array() as $key) {
	    	$response->rows[$i]['kode_obat']=$key['id_obat'];
	    	$response->rows[$i]['name']=$key['object_name'].'('.$key['detil_kemasan'].')';
	    	//$response->rows[$i]['kemasan']=$key['detil_kemasan'];
	    	$response->rows[$i]['author']=$key['org_pembuat'];
	    	$response->rows[$i]['kemasan']=$key['kemasan_unit'];
	    	$response->rows[$i]['sat_jual']=$key['satuan_jual_unit'];
	    	$response->rows[$i]['kategori']=$key['kategori'];
	    	//$response->rows[$i]['nama_generik']=$key['nama_generik'];
	    	$response->rows[$i]['id_generik']=$key['id_generik'];
	    	$i++;
	    }
	    //return $r;

		//$query = $this->detail_faktur_model->GetListObat($key);
        echo json_encode($response);
    }


    function get_list_obat_bc(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListObatBC($key);
        print json_encode($query);
    }

    function get_list_atc(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListAtc($key);
        print json_encode($query);
    }

    function get_list_fornas(){
        $key=$this->input->get("term");
		$query = $this->detail_faktur_model->GetListFornas($key);
        print json_encode($query);
    }

    function get_list_indikator(){
        $key=$this->input->get("term");
		$query = $this->detail_faktur_model->GetListIndikator($key);
        print json_encode($query);
    }

    function get_list_program(){
        $key=$this->input->get("term");
		$query = $this->detail_faktur_model->GetListProgram($key);
        print json_encode($query);
    }

    function get_list_ukp4(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListUkp4($key);
        print json_encode($query);
    }

    function get_list_generik(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListGenerik($key);
        print json_encode($query);
    }

    function get_produsen(){
        $key=$this->input->get("term");
		//var $term;
		$query = $this->detail_faktur_model->GetListProdusen($key);
        print json_encode($query);
    }

    function input_data_kecamatan(){
		$data = array();
		$data['nama_kec']=$this->input->post('nama_kecamatan');
		$query=$this->unit_penerima_m->input_data_kecamatan_m($data);
	}

	function get_detail_obat_penerimaan(){
		$no_batch=$this->input->post('no_batch');
		$data = $this->detail_faktur_model->GetInfoObat($no_batch);

		//$obat['kode_obat']=$data['kode']
		print json_encode($data);
	}

	function cobagetfornas($id_obat){
		$query=$this->db->query("
			SELECT mof.id_fornas AS id_fornas, rf.nama_obat AS nama_fornas
			FROM map_obat_fornas mof
			JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			WHERE mof.id_obat='$id_obat'
			");
		if($query->num_rows()>0){
			$result=$query->row_array();
		}else{
			$result['id_fornas']="silakan diupdate";
			//$result['id_ukp4']="silakan diupdate";
			$result['nama_fornas']="silakan diupdate";
			//$result['nama_ukp4']="silakan diupdate";
		}

		//print_r($result);
		//echo $result['nama_fornas'];
		print json_encode($result);
	}

	function getatc($id_obat){
		$query=$this->db->query("
			SELECT moa.atc AS atc, rwa.nama_atc AS nama_atc, rwa.nama_atc_eng AS nama_atc_eng
			FROM map_obat_atc moa
			JOIN ref_who_atc rwa ON(rwa.kode_atc=moa.atc)
			WHERE moa.id_obat='$id_obat'
			");
		if($query->num_rows()>0){
			$result=$query->row_array();
		}else{
			$result['atc']="silakan diupdate";
			//$result['id_obat']="silakan diupdate";
			$result['nama_atc']="silakan diupdate";
			$result['nama_atc_eng']="silakan diupdate";
		}
		print json_encode($result);
	}

	function getgenerik($id_generik){
		// $query=$this->db->query("
		// 	SELECT nama_objek FROM hlp_generik
		// 	WHERE id_hlp='$id_generik'
		// 	");
		$query=$this->db->query("
			SELECT hg.nama_objek,ro.id_obatindikator,ri.nama_indikator,ro.id_obatprogram,rp.nama_program
			FROM hlp_generik hg
			LEFT JOIN ref_obatgenerik ro ON ro.id = hg.id_hlp
			LEFT JOIN ref_obatindikator ri ON ri.id_indikator = ro.id_obatindikator
			LEFT JOIN ref_obatprogram rp ON rp.id = ro.id_obatprogram
			WHERE id_hlp='$id_generik'
			");
		$result=$query->row_array();
		print json_encode($result);
	}

	function getukp4($id_obat){
		$query=$this->db->query("
			SELECT mou.id_ukp4 AS id_ukp4, ru.nama_ukp4 AS nama_ukp4
			FROM map_obat_ukp4 mou
			JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			WHERE mou.id_obat='$id_obat'
			");
		if($query->num_rows()>0){
			$result=$query->row_array();
		}else{
			$result['id_ukp4']="silakan diupdate";
			//$result['nama_fornas']="silakan diupdate";
			$result['nama_ukp4']="silakan diupdate";
		}
		print json_encode($result);
	}

	function update_data(){
		$data = array();
		$data['no_batch']=$this->input->post('no_batch');//udah diganti pake id_stok
		$data['batch']=$this->input->post('no_batch_ori');
		$data['tanggal']=$this->input->post('tanggal');
		$data['harga_beli']=str_replace(",",".", $this->input->post('harga_beli'));//$this->input->post('harga_beli');
		$data['jumlah_kec']=$this->input->post('jumlah_kec');
		$data['kategori']=$this->input->post('kategori');
		$data['jml_rusak']=$this->input->post('jumlah_rusak');
		$data['sat_jual']=$this->input->post('satuan');
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['no_faktur']=$this->input->post('no_faktur');
		$data['kode_generik']=$this->input->post('kode_inn');
		$data['kemasan']='';
		$query=$this->detail_faktur_model->update_data_m($data);
	}

	function search_data(){
		$key=$this->input->post('key');
		$data['base_url']=$this->url;
		$data['result']=$this->detail_faktur_model->searchData($key);
		$data['links'] = '';
		$this->load->view('manajemenlogistik/detail_faktur_list',$data);
	}
}
?>

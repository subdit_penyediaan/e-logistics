<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->url = base_url();
		$this->load->model('Login_Model');
		$this->load->model('profil/setting_institusi_model');
		$this->profil = $this->setting_institusi_model->getDataInstitusi();
	}

	function index()
	{
		$data['base_url']=$this->url;
		$data['pesanerror']="";
		$this->load->view('form_login',$data);
	}

	function signin()
	{
		$username=$this->input->post('username');
		$password=md5($this->input->post('password'));

		$result = $this->Login_Model->cek($username,$password);
		if($result){
			$qry=$this->db->query("select * from tb_user where username='$username'");
			if ($qry->num_rows() > 0)
			{
			   	$row = $qry->row_array(); 
			   	$id=$row['id_user'];
			   	if(isset($row['group_id'])) {
			   		$group_id=$row['group_id'];
			   		$data=array('username'=>$username, 'login'=>TRUE, 'id'=>$id, 'group_id' => $group_id);
			   	} else {
			   		$data=array('username'=>$username, 'login'=>TRUE, 'id'=>$id);
			   	}
				$this->session->set_userdata($data);
				$thisMonth = date('m');
				$thisYear = date('Y');
				
				// for ($i=1; $i <= $thisMonth; $i++) {
				// 	$this->sendLog($thisMonth, $thisYear);
				// }

				$this->sendLog();
				redirect('mainpage');
			}
		} else {				
			$data['base_url']=$this->url;
			if(($username=="")||($password=="")){
				$data['pesanerror']="Maaf, Username dan Password harus diisi";	
			}else{
				$data['pesanerror']="Maaf, Username atau Password Anda Salah";
			}
			$this->load->view('form_login',$data);
		}	
	}

	public function sendLogWithCurl($month='', $year='')
	{
		$profile = getProfile();
		$area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];
		$temp['token'] = array(
					'username' => $profile['username'],
					'password' => base64_encode($profile['password']),
					'area' => $area,
				);

		$periodeNow = date('Y').'-'.date('m'); // current date, if no input in a month no data that can be represent
		
		$queryPenerimaan = $this->Login_Model->getLogPenerimaan($periodeNow)->row_array();
		$queryLplpo = $this->Login_Model->getLogLplpo($periodeNow)->row_array();
		$queryDistribusi = $this->Login_Model->getLogDistribusi($periodeNow)->row_array();
		
		$temp['content'] = array(
				'area' => $area,
				'periode' => $periodeNow,
				'penerimaan' => (isset($queryPenerimaan['date_log'])) ? $queryPenerimaan['date_log'] : null,
				'lplpo' => (isset($queryLplpo['date_log'])) ? $queryLplpo['date_log'] : null,
				'distribusi' => (isset($queryDistribusi['date_log'])) ? $queryDistribusi['date_log'] : null
			);

		$data_send = array('dataEnvironment'=>$temp);
	    $payload = json_encode($data_send);     

        $url = 'http://bankdataelog.kemkes.go.id/api/log/activities';
        // $url = 'http://localhost/api/log/activities';

        $ch = curl_init( $url );        
        # Setup request to send json via POST.        
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );        
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $response = curl_exec($ch);
        
        curl_close($ch);
        $result = json_decode($response);
        $output = (isset($result->status)) ? $result->status : false;

        return $output;
	}

	function sendLog()
    {
	    $profile = getProfile();
		$area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];
		$token = $profile['username'].'|'.base64_encode($profile['password']).'|'.$area;

		$periodeNow = date('Y').'-'.date('m'); // current date, if no input in a month no data that can be represent
		
		$queryPenerimaan = $this->Login_Model->getLogPenerimaan($periodeNow)->row_array();
			$penerimaan = (isset($queryPenerimaan['date_log'])) ? $queryPenerimaan['date_log'] : '(NULL)';
		$queryLplpo = $this->Login_Model->getLogLplpo($periodeNow)->row_array();
			$lplpo = (isset($queryLplpo['date_log'])) ? $queryLplpo['date_log'] : '(NULL)';
		$queryDistribusi = $this->Login_Model->getLogDistribusi($periodeNow)->row_array();
			$distribusi = (isset($queryDistribusi['date_log'])) ? $queryDistribusi['date_log'] : '(NULL)';
		
		$data = $area.'|'.$periodeNow.'|'.$penerimaan.'|'.$lplpo.'|'.$distribusi;

      	require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
        $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
        $client->soap_defencoding = 'UTF-8';
        $params = array(
                'username'      => base64_encode($profile['username']),
                'password'      => base64_encode($profile['password']),
                'tahun' 		    => date('Y'),
                'bulan'             => date('m'),
                'jenis_data'        => 'activity_log',
                'wil'               => $area,
                'data' 		        => $data,
                'logkirim'            => ''
            );
        $result = $client->call('Receive', $params);
        if ($client->fault) {
          	$ret['error'] = "failed";
          	$ret['msg'] = "The request contains an invalid SOAP body";
        } elseif($result!='success') {
            $ret['error'] = "failed";
            $ret['msg'] = $result.' sing iki po?';
        } else {
            $err = $client->getError();
            if ($err) {
                $ret['error'] = "failed";
                $ret['msg'] = $err.' sing iki';
            } else {
                $ret['error'] = "success";
                $ret['msg'] = $result;
            }
        }
        // echo json_encode($ret);
    }

	function logout(){
		$this->sendLog();
		$this->session->sess_destroy();
		redirect('login');
	}

	function regBaru(){
		$data['base_url']=$this->url;
		$this->load->view('form_register_baru',$data);
	}
}

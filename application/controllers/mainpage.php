<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mainpage extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->url=base_url();
		$this->load->model('elogistik_m');
		$this->load->model('profil/setting_institusi_model');
		$this->load->model('laporan/obat_kadaluarsa_model');
		$this->load->model('laporan/stok_model');
		$this->profil = $this->setting_institusi_model->getDataInstitusi();
	}

	function index()
	{
		if($this->session->userdata('login')==TRUE){
			$limit['limit'] = 10;
			$limit['jarak'] = ' < 0 ';
			$limit['dana'] = 'all';
			$limit['tahun_anggaran'] = 'all';
			$limit['kategori'] = 'all';
			$data['user']=$this->session->userdata('username');
			$group['puskesmas'] = $this->session->userdata('group_id');
			$data['base_url']=$this->url;
			$data['profil']=$this->elogistik_m->getProfil();
			$data['menuParent']=$this->elogistik_m->getMenuParent($group);
			$data['menuChild']=$this->elogistik_m->getMenuChild($group);
			$data['menu_all']=$this->elogistik_m->getMenuAll($group);
			$data['toptened'] = $this->obat_kadaluarsa_model->show_data($limit);
			$data['assets'] = 0; //$this->assets();
			$warning=$this->elogistik_m->cek_stok_opnam();
			if(($warning==false) && (date('d')>19)){
				$data['warning_stok_opnam']="<marquee>Anda belum melakukan stok opnam pada bulan ini</marquee>";
			}else{
				$data['warning_stok_opnam']="";
			}
			// $data['registered'] = $this->loginBankData();
			$data['registered'] = true;
			$data['warning_message'] = array(); // $this->getMessageBankData();
			$this->load->view('mainlayout',$data);
		}else{
			redirect('login');
		}
	}

	public function loginBankData()
	{
		$profile = getProfile();
		$area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];
		$temp = array(
					'username' => $profile['username'],
					'password' => base64_encode($profile['password']),
					'area' => $area,
				);
		$data_send = array('dataEnvironment'=>$temp);
	    $payload = json_encode($data_send);
        $url = 'http://bankdataelog.kemkes.go.id/api/login/member';
        // $url = 'http://localhost/api/login/member';
        $ch = curl_init( $url );
        # Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        # Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        # Send request.
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response);
        $output = (isset($result->status)) ? $result->status : false;

        return $output;
	}

	function puskesmas(){
		$data['base_url']=$this->url;
		//$result=$this->preprocess_m->paginationpage($config['per_page'],$this->uri->segment(3));
		//$data['result']=$this->elogistik_m->getDataPuskesmas();

		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/mainpage/puskesmas';
		$config['total_rows'] = $this->elogistik_m->countAllPuskesmas(); //untuk menghitung banyaknya rows
		$config['per_page'] = 15; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 3;

		$this->pagination->initialize($config);

		$data['result']=$this->elogistik_m->getDataPuskesmas($config['per_page'],$this->uri->segment(3));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('list_puskesmas',$data);
		//$this->load->view('list_puskesmas');
	}

	function getMessageBankData()
    {
	    $profile = getProfile();
		$area = ($profile['id_prop'] != '') ? $profile['id_prop'] : $profile['id_kab'];
		$token = $profile['username'].'|'.base64_encode($profile['password']).'|'.$area;


      	require_once APPPATH . "libraries/nusoap/lib/nusoap.php";
        $client = new nusoap_client(prep_url($this->profil['url_bank_data1']) . "/sinkron?wsdl");
        $client->soap_defencoding = 'UTF-8';
        $params = array(
                'username'      => base64_encode($profile['username']),
                'password'      => base64_encode($profile['password']),
                'tahun' 		    => date('Y'),
                'bulan'             => date('m'),
                'jenis_data'        => 'warning_message',
                'wil'               => $area,
                'data' 		        => '',
                'logkirim'            => ''
            );
        $result = $client->call('Receive', $params);

        if ($client->fault) {
          	$output['error'] = "failed";
          	$output['msg'] = "The request contains an invalid SOAP body";
        } else {
        	$output = json_decode($result);
        }

        return $output;
    }
		

    function expired()
    {
    	//
    }

    function stock()
    {
    	$sql = "select b.nama_objek, b.sediaan, sum(a.stok) as jml
				from tb_stok_obat a
				join hlp_generik b on a.kode_generik = b.id_hlp
				where a.flag = 1 and a.stok > 0
				GROUP by b.id_hlp
				order by jml asc
				limit 10";
		$query = $this->db->query($sql);

		$data = array();
    	foreach ($query->result() as $key) {
    		$temp[0]=$key->nama_objek;
            $temp[1]=$key->jml;

            array_push($data, $temp);
    	}

    	$result['content'] = $data;

    	echo json_encode($result, JSON_NUMERIC_CHECK);
    }

    function ketersediaan()
    {
    	$date_now = date('Y-m-d');
    	$date_three_month_ago = date('Y-m-d', strtotime("-3 Months"));
    	$sql = "select b.nama_objek, b.sediaan, sum(a.stok) as jml, avg(c.pemberian) as rerata_pemberian,
    			ROUND(sum(a.stok)/avg(c.pemberian), 2) as ketersediaan
				from tb_stok_obat a
				join tb_detail_distribusi c on c.id_stok = a.id_stok
				join hlp_generik b on a.kode_generik = b.id_hlp
				where a.flag = 1 and a.stok > 0 and c.create_time BETWEEN '".$date_three_month_ago."' AND '".$date_now."'
				GROUP by b.id_hlp
				order by ketersediaan asc
				limit 10";

		$query = $this->db->query($sql);

		$data = array();
    	foreach ($query->result() as $key) {
    		$temp[0]=$key->nama_objek;
            $temp[1]=$key->ketersediaan;

            array_push($data, $temp);
    	}

    	$result['content'] = $data;

    	echo json_encode($result, JSON_NUMERIC_CHECK);
    }

    function assets()
    {
    	$default_filter['kategori'] = 'all';
    	$default_filter['jprogram'] = 'all';
    	$default_filter['nama'] = '';
    	$default_filter['awal'] = '2014-01-01';
    	$default_filter['akhir'] = date('Y-m-d');

    	$query = $this->stok_model->show_data($default_filter);
    	$total_assets = 0;
    	foreach ($query as $key) {
    		$total_assets = $total_assets + ($key->stok * $key->harga);
    	}

    	echo number_format($total_assets, 0, ",", ".");
    }

	function konfigurasi()
	{
		$data['base_url']=$this->url;
		$this->load->view('form_reg_institusi',$data);
	}

	function administrator()
	{
		$data['base_url']=$this->url;
		$this->load->view('form_administrator',$data);
	}

	function settingModul()
	{
		$data['base_url']=$this->url;
		$this->load->view('setting_modul',$data);
	}

	function menejemenUser(){
		$data['base_url']=$this->url;
		$this->load->view('list_user',$data);
	}

	function logactivities()
	{
		$start_date= date("Y-m-d", strtotime("-1 Months"));
		$end_date= date('Y-m-d', strtotime("+1 Day"));

		$this->db->select('tb_penerimaan.*');
		$this->db->select('tb_user.long_name');
		$this->db->join('tb_user', 'tb_user.id_user = tb_penerimaan.create_by');
		$this->db->where('tb_penerimaan.create_time BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$this->db->order_by('tb_penerimaan.create_time', 'desc');

		// penerimaan
		$penerimaan = $this->db->get('tb_penerimaan')->result_array();

		$html ='';
		foreach ($penerimaan as $key => $value) {
			$html .= '<li>';
			$html .= $value['long_name'].' telah menambahkan data penerimaan pada tanggal '.$value['create_time'];
			$html .= '</li>';
		}

		echo $html;

		// distribusi
		// LPLPO
		// Stok opnam
		// integrasi
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

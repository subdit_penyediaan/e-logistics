<?php

Class Master_Atc extends CI_Controller {
    
    //var $title = 'Golongan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_atc_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
    		$data['kode_obat']=$this->input->post('kode_obat');
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/master_atc',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['kode_atc']=$this->input->post('kode_atc');
		$data['kode_obat']=$this->input->post('kode_obat');
		$query=$this->master_atc_model->input_data_m($data);
	}


	function get_data_master_atc($id_obat){
		$data['base_url']=$this->url;		
		$data['result']=$this->master_atc_model->getData($id_obat);
		$this->load->view('masterdata/master_atc_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_atc_model->deleteData($kode);
    }

 	function get_list_atc(){
 		$key=$this->input->get("term");
		$query = $this->master_atc_model->GetListAtc($key);		
        print json_encode($query);
 	}
}
?>
<?php

Class Nama_program extends CI_Controller {
    
    var $title = 'Nama Program';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/nama_program_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/nama_program',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['nama_program']=$this->input->post('nama_program');
		$data['keterangan']=$this->input->post('keterangan');		
		//$query=$this->nama_program_model->input_data_m($data);
		$this->db->insert('ref_obatprogram',$data);
	}


	function get_data_program(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/nama_program/get_data_program';
		$config['total_rows'] = $this->nama_program_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->nama_program_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/nama_program_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        //$this->nama_program_model->deleteData($kode);
        $data = array('status'=>0);
        $this->db->where_in('id',$kode);
        $this->db->update('ref_obatprogram',$data);
    }

	function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->nama_program_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/nama_program_list',$data);
	}

	function get_detail(){
		$kode=$this->input->post('id_program');
		$data = $this->nama_program_model->GetInfo($kode);

		//$obat['kode_obat']=$data['kode']
		print json_encode($data);
	}

	function update_data(){
		$data = array();
		$data=$this->input->post('uptdata');
		//print_r($data);
		$this->db->where('id',$data['id']);
		$this->db->update('ref_obatprogram',$data);
		//$query=$this->master_generik_model->update_data_m($data);	
	}
}
?>
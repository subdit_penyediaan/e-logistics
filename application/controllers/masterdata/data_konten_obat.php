<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Data_Konten_Obat extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->url=base_url();
		//$this->load->model('unit_penerima_m');
	}

	function index()
	{
		$data['base_url']=$this->url;
		$this->load->view('masterdata/form_konten_obat',$data);
	}

	function puskesmas(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/unit_penerima/puskesmas';
		//$config['total_rows'] = $this->elogistik_m->countAllPuskesmas(); //untuk menghitung banyaknya rows
		$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 3;

		$this->pagination->initialize($config);

		$data['result']=$this->unit_penerima_m->getDataPuskesmas($config['per_page'],$this->uri->segment(3));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('list_unit_penerima',$data);
		//$this->load->view('list_puskesmas');
	}

	function unitEksternal(){
		$data['base_url']=$this->url;
		$data['result']=$this->unit_penerima_m->getDataUnitEks();
		//$data['links'] = $this->pagination->create_links();
		$data['links'] = "";
		$this->load->view('list_non_puskesmas',$data);
	}

}
<?php

Class Obat_Fornas extends CI_Controller {
    
    var $title = 'Formularium Obat Nasional';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/Obat_Fornas_Model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->getobatfornas($data);
			//$this->load->view('masterdata/Data_Obat',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }
	
    function getobatfornas($data){
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_fornas where id_fornas like '%0000000000'");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<tr><td>'.$rows->id_fornas.'</td><td>'.$rows->nama_obat.'</td>
						<td>'.$rows->selected.'</td><td>'.$rows->parent.'</td>
						<td><button>Lihat</button></td></tr>';
		}
		$data['list_obat']=$list_obat;
    	$this->load->view('masterdata/obat_fornas', $data);	
    }

	function input_data(){
		$data = array();
		$data['nama_obat_fornas']=$this->input->post('nama_obat_fornas');
		$data['tanggal_ex']=$this->input->post('tanggal_ex');
		$data['jumlah_stok']=$this->input->post('jumlah_stok');
		$data['sumber_dana']=$this->input->post('sumber_dana');
		$data['harga']=$this->input->post('harga');
		$query=$this->Stok_Obat_Model->input_data_m($data);
	}

	function list_input_data(){
		$qry=$this->db->query("select * from stok_obat");
		foreach ($qry->result() as $rows) {
			echo '<tr><td>'.$rows->id_fornas.'</td>';
			echo '<td>'.$rows->nama_obat.'</td>';
			echo '<td>'.$rows->stok.'</td>';
			echo '<td>'.$rows->expired.'</td>';
			echo '<td>'.$rows->harga.'</td>';
			echo '<td>'.$rows->sumber_dana.'</td></td>';
		}
	}

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    
    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);
        
        print json_encode($query);

        /*for($i=0;$i<sizeof($query);$i++) {
            echo $query[$i]['id_fornas'] . "|" . $query[$i]['nama_obat'] ."\n";
        }*/
    }

	function delete_list() {
        $ret['command'] = "deleting data";
        if($this->Bank_Model->DoDeleteData()) {
            $ret['msg'] = "Sukses Menghapus";
            $ret['status'] = "success";
        } else {
            $ret['msg'] = "Gagal Menghapus";
            $ret['status'] = "error";
        }
		
		echo json_encode($ret);
    }

    function process_form() {
		$ret = array();
        if($this->input->post('saved_id')) {
			$ret['command'] = "updating data";
			if($this->Bank_Model->DoUpdateData()) {
				$ret['msg'] = $this->lang->line('msg_update');
				$ret['status'] = "success";
			} else {
				$ret['msg'] = $this->lang->line('msg_error_update');
				$ret['status'] = "error";
			}
        } else {
			$ret['command'] = "adding data";
			$last_id = $this->Stok_Obat_Model->DoAddData();
			if($last_id > 0) {
				$ret['msg'] = $this->lang->line('msg_save');
				$ret['status'] = "success";
			} else {
				$ret['msg'] = $this->lang->line('msg_error_save');
				$ret['status'] = "error";
			}
        }
		echo json_encode($ret);
    }
    
}

<?php

Class Detail_Data_Obat extends CI_Controller {
    
    var $title = 'detail data obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/detail_data_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/detail_data_obat',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }

	function getAllDt($id){
		//$qry=$this->db->query("select * from ref_obat_all where id_obat='$id'");
		//print_r($qry->row_array());
		$qry=$this->db->query("
			SELECT oa.*,
			rf.id_fornas AS id_fornas,rf.nama_obat AS nama_fornas,
			ru.id_ukp4 AS id_ukp4, ru.nama_ukp4 AS nama_ukp4,
			rwa.kode_atc AS kode_atc,rwa.nama_atc AS nama_atc,hg.nama_objek AS nama_inn
			FROM ref_obat_all oa
			LEFT JOIN map_obat_fornas mof ON(mof.id_obat=oa.id_obat)
			LEFT JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			LEFT JOIN map_obat_ukp4 mou ON(mou.id_obat=oa.id_obat)
			LEFT JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			LEFT JOIN map_obat_atc moa ON(moa.id_obat=oa.id_obat)
			LEFT JOIN ref_who_atc rwa ON(rwa.kode_atc=moa.atc)
			LEFT JOIN hlp_generik hg ON hg.id_hlp=oa.id_generik
			WHERE oa.id_obat='$id'
			");
		$data['result']=$qry->result_array();
		//print_r($data);
		/*foreach ($qry->result() as $rows) {
			echo $rows->nama_obat
		}*/
		//$data=$qry->row_array();
		$data['base_url']=$this->url;
		$data['sediaan']=$this->detail_data_obat_model->getSediaan();
    	$data['satuan']=$this->detail_data_obat_model->getSatuan();
    	//$data['satuan_klinis']="";
    	$data['golongan']=$this->detail_data_obat_model->getGolongan();
		$this->load->view('masterdata/detail_data_obatB',$data);
	}

	function input_data(){
		$data = array();
		$data['nama_obat_fornas']=$this->input->post('nama_obat_fornas');
		$data['tanggal_ex']=$this->input->post('tanggal_ex');
		$data['jumlah_stok']=$this->input->post('jumlah_stok');
		$data['sumber_dana']=$this->input->post('sumber_dana');
		$data['harga']=$this->input->post('harga');
		$query=$this->Stok_Obat_Model->input_data_m($data);
	}

	function list_input_data(){
		$qry=$this->db->query("select * from stok_obat");
		foreach ($qry->result() as $rows) {
			echo '<tr><td>'.$rows->id_fornas.'</td>';
			echo '<td>'.$rows->nama_obat.'</td>';
			echo '<td>'.$rows->stok.'</td>';
			echo '<td>'.$rows->expired.'</td>';
			echo '<td>'.$rows->harga.'</td>';
			echo '<td>'.$rows->sumber_dana.'</td></td>';
		}
	}

	function detail_obat(){
		$this->load->view('masterdata/form_detail_obat');
	}

	function detail_konten_obat()
	{
		$data['base_url']=$this->url;
		$this->load->view('masterdata/form_konten_obat',$data);
	}

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    
    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);
        
        print json_encode($query);

        /*for($i=0;$i<sizeof($query);$i++) {
            echo $query[$i]['id_fornas'] . "|" . $query[$i]['nama_obat'] ."\n";
        }*/
    }

	   
    function get_list_pabrik(){
        $key=$this->input->get("term");
        //var $term;
        $query = $this->detail_data_obat_model->GetListPabrik($key);
        print json_encode($query);
    }

    function update_data(){
    	$data=$this->input->post('upt');
    	$this->db->where('id_obat',$data['id_obat']);
    	$this->db->update('ref_obat_all',$data);

    	$cek = $this->db->where('kode_obat',$data['id_obat'])->get('tb_stok_obat');
    	if($cek->num_rows() > 0){
    		$stok_update = array('kode_generik'=>$data['id_generik']);
    		$this->db->where('kode_obat',$data['id_obat']);
    		$this->db->update('tb_stok_obat',$stok_update);
    	}    	
    }
}
?>
<?php

Class Master_Konten extends CI_Controller {
    
    //var $title = 'Golongan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_konten_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
    		$data['kode_obat']=$this->input->post('kode_obat');
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$data['satuan']=$this->master_konten_model->getSatuan();
			$this->load->view('masterdata/master_konten',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['id_konten']=$this->input->post('id_konten');
		$data['kode_obat']=$this->input->post('kode_obat');
		$data['strength']=$this->input->post('strength');
		$data['container']=$this->input->post('container');
		$data['unit_str']=$this->input->post('unit_str');
		$data['unit_cnt']=$this->input->post('unit_cnt');
		$query=$this->master_konten_model->input_data_m($data);
	}


	function get_data_master_konten($id_obat){
		$data['base_url']=$this->url;
		$data['result']=$this->master_konten_model->getData($id_obat);
		
		$this->load->view('masterdata/master_konten_list',$data);
		
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_konten_model->deleteData($kode);
    }

    function get_list_konten(){
    	$key=$this->input->get("term");
		$query = $this->master_konten_model->GetListKonten($key);		
        print json_encode($query);
    }

    function datatochange($id_map){
    	$query=$this->db->query("
			SELECT rk.nama_konten AS nama_konten,moz.strength_vol AS vol_s,
			moz.strength_unit AS unit_s,moz.container_vol AS vol_c,
			moz.container_unit AS unit_c
			FROM map_obat_zat_id moz
			JOIN ref_konten rk ON(rk.id=moz.id_konten)
			WHERE moz.id_map='$id_map'
			");
    	$result=$query->row_array();
		print json_encode($result);
    }

    function update_data(){
    	
    }

    function get_list_cui(){    
 		$key=$this->input->get("term");
		$query = $this->master_konten_model->GetListCui($key);		
        print json_encode($query);
 	
    }

    function input_master_konten(){
		$data = array();
		$data['nama_konten']=$this->input->post('nama_konten');
		$data['kode_cui']=$this->input->post('kode_cui');
		$query=$this->master_konten_model->input_master_m($data);
	}
}
?>
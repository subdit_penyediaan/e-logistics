<?php

Class Obat_indikator extends CI_Controller {
    
    var $title = 'Nama Program';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/obat_indikator_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/obat_indikator',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['nama_indikator']=$this->input->post('nama_obat');
		$data['satuan']=$this->input->post('satuan');		
		//$query=$this->obat_indikator_model->input_data_m($data);
		$this->db->insert('ref_obatindikator',$data);
	}


	function get_data(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/obat_indikator/get_data';
		$config['total_rows'] = $this->obat_indikator_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->obat_indikator_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/obat_indikator_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        //$this->obat_indikator_model->deleteData($kode);
        $data = array('status'=>0);
        $this->db->where_in('id_indikator',$kode);
        $this->db->update('ref_obatindikator',$data);
    }

	function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->obat_indikator_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/obat_indikator_list',$data);
	}

	function get_detail(){
		$kode=$this->input->post('id_indikator');
		$data = $this->obat_indikator_model->GetInfo($kode);

		//$obat['kode_obat']=$data['kode']
		print json_encode($data);
	}

	function update_data(){
		$data = array();
		$data=$this->input->post('uptdata');
		//print_r($data);
		$this->db->where('id_indikator',$data['id_indikator']);
		$this->db->update('ref_obatindikator',$data);
		//$query=$this->master_generik_model->update_data_m($data);	
	}
}
?>
<?php

Class Master_Rute extends CI_Controller {
    
    //var $title = 'Golongan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_rute_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['kode_obat']=$this->input->post('kode_obat');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/master_rute',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['id_rute']=$this->input->post('id_rute');
		$data['kode_obat']=$this->input->post('kode_obat');
		$query=$this->master_rute_model->input_data_m($data);
	}


	function get_data_master_rute($id_obat){
		$data['base_url']=$this->url;
		
		//pagination
		//$this->load->library('pagination');
		//$config['base_url']= $data['base_url'].'index.php/masterdata/gol_obat/get_data_gol';
		//$config['total_rows'] = $this->gol_obat_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		//$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		//$config['uri_segment'] = 4;

		//$this->pagination->initialize($config);

		$data['result']=$this->master_rute_model->getData($id_obat);
		//$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/master_rute_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_rute_model->deleteData($kode);
    }

    function get_list_rute(){
 		$key=$this->input->get("term");
		$query = $this->master_rute_model->GetListRute($key);		
        print json_encode($query);
 	}   
}
?>
<?php

Class Produsen_obat extends CI_Controller {
    
    var $title = 'Produsen_Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/produsen_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/produsen_obat',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['nama_produsen']=$this->input->post('nama_produsen_obat');
		//$data['ket_gol_obat']=$this->input->post('ket_gol_obat');
		$query=$this->produsen_obat_model->input_data_m($data);
	}


	function get_data_produsen_obat(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/produsen_obat/get_data_produsen_obat';
		$config['total_rows'] = $this->produsen_obat_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->produsen_obat_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/produsen_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->produsen_obat_model->deleteData($kode);
    }

    function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->produsen_obat_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/produsen_obat_list',$data);
	}
        
}
?>
<?php

Class Pbf extends CI_Controller {
    
    var $title = 'Pbf';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/pbf_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/pbf',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['nama_pbf']=$this->input->post('nama_pbf');
		$data['telp_pbf']=$this->input->post('telp_pbf');
		$data['almt_pbf']=$this->input->post('almt_pbf');
		$query=$this->pbf_model->input_data_m($data);
	}


	function get_data_pbf(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/pbf/get_data_pbf';
		$config['total_rows'] = $this->pbf_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->pbf_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/pbf_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->pbf_model->deleteData($kode);
    }

	function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->pbf_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/pbf_list',$data);
	}        
}
?>
<?php

Class Master_alkesgen extends CI_Controller {
    
    var $title = 'Sediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_alkesgen_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/master_alkesgen',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }
	
	function input_data(){
		$data = array();
		$data=$this->input->post('indata');
		//print_r($data);
		$query=$this->master_alkesgen_model->input_data_m($data);
	}


	function get_data_master_alkesgen(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/master_alkesgen/get_data_master_alkesgen';
		$config['total_rows'] = $this->master_alkesgen_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 15; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->master_alkesgen_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/master_alkesgen_list',$data);
		//$this->load->view('list_puskesmas');
	}

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    

	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_alkesgen_model->deleteData($kode);
    }

	function get_detail(){
		$kode=$this->input->post('kode_alkesgen');
		$data = $this->master_alkesgen_model->GetInfoObat($kode);

		//$obat['kode_obat']=$data['kode']
		print json_encode($data);
	}

	function update_data(){
		$data = array();
		$data=$this->input->post('uptdata');
		//print_r($data);
		$query=$this->master_alkesgen_model->update_data_m($data);	
	}

	function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
	

		$data['result']=$this->master_alkesgen_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/master_alkesgen_list',$data);
	}
}
?>
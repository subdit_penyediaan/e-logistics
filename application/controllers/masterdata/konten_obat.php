<?php

Class Konten_Obat extends CI_Controller {
    
    var $title = 'Konten Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/konten_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->getkontenobat($data);
			//$this->load->view('masterdata/Data_Obat',$data);	
		}else{
			redirect('login');
		}
     }
	
    function getkontenobat($data){
        //memunculkan index abjad 1 huruf depan
    	$list_obat="";
    	$qry=$this->db->query("
    		SELECT SUBSTRING(rk.nama_konten,1,1) AS index_page
			FROM ref_konten rk
			GROUP BY index_page
			ORDER BY index_page
    		");
		foreach ($qry->result() as $rows) {
			//$list_obat.= '<li>'.$rows->nama_konten.'</li>';
			$list_obat.= '<li class="list_master"><a href="masterdata/konten_obat/getpage_2/'.$rows->index_page.'" class="a_master">'.$rows->index_page.'</a></li>';
		}
		$data['list_konten']=$list_obat;
    	$this->load->view('masterdata/konten_obat', $data);	
    }

    function getpage_2($char){
        //memunculkan index 3 huruf depan berdasarkan pilihan pada function getkontenobat()
    	$list_obat="";
    	$qry=$this->db->query("
    		SELECT SUBSTRING(rk.nama_konten,1,3) AS index_page
			FROM ref_konten rk
			WHERE nama_konten like '$char%'
			GROUP BY index_page
			ORDER BY index_page
    	");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li class="list_master"><a href="masterdata/konten_obat/getlistmaster/'.$rows->index_page.'" class="a_master">'.$rows->index_page.'</a></li>';
		}
		
    	echo $list_obat;		
    }

    function getlistmaster($char){
        //memunculkan list konten
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_konten where nama_konten like '$char%'");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li><a href="masterdata/konten_obat/getAllDt/'.$rows->id.'">'.$rows->nama_konten.'</a></li>';
		}
		$data['data_obat']=$list_obat;
    	$this->load->view('masterdata/data_konten_list', $data);
    	//echo $data['data_obat'];
    }

	function input_data(){
		$data = array();
		$data['nama_obat_fornas']=$this->input->post('nama_obat_fornas');
		$data['tanggal_ex']=$this->input->post('tanggal_ex');
		$data['jumlah_stok']=$this->input->post('jumlah_stok');
		$data['sumber_dana']=$this->input->post('sumber_dana');
		$data['harga']=$this->input->post('harga');
		//$query=$this->Stok_Obat_Model->input_data_m($data);
	}


	function delete_list() {
        $ret['command'] = "deleting data";
        if($this->Bank_Model->DoDeleteData()) {
            $ret['msg'] = "Sukses Menghapus";
            $ret['status'] = "success";
        } else {
            $ret['msg'] = "Gagal Menghapus";
            $ret['status'] = "error";
        }
		
		echo json_encode($ret);
    }

  	function search_by(){
    	$key=$this->input->post('key_master');
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_konten where nama_konten like '%$key%'");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li><a href="masterdata/konten_obat/getAllDt/'.$rows->id.'">'.$rows->nama_konten.'</a></li>';
		}
		$data['data_obat']=$list_obat;
    	$this->load->view('masterdata/data_konten_list', $data);
    	//echo $list_obat;
    }

    function getAllDt($key){
        //$kode=$this->input->post('kode_generik');
        $data = $this->konten_obat_model->GetInfoObat($key);

        //$obat['kode_obat']=$data['kode']
        print json_encode($data);
    }

    function update_data(){
        $data=$this->input->post('upt');
        $this->db->where('id',$data['id']);
        $this->db->update('ref_konten',$data);
    }
    
}

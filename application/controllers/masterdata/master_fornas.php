<?php

Class Master_Fornas extends CI_Controller {
    
    //var $title = 'Golongan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_fornas_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['kode_obat']=$this->input->post('kode_obat');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/master_fornas',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['id_fornas']=$this->input->post('id_fornas');
		$data['kode_obat']=$this->input->post('kode_obat');
		$query=$this->master_fornas_model->input_data_m($data);
	}


	function get_data_master_fornas($id_obat){
		$data['base_url']=$this->url;
		$data['result']=$this->master_fornas_model->getData($id_obat);		
		$this->load->view('masterdata/master_fornas_list',$data);		
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_fornas_model->deleteData($kode);
    }

    function get_list_fornas(){
 		$key=$this->input->get("term");
		$query = $this->master_fornas_model->GetListFornas($key);		
        print json_encode($query);
 	}
}
?>
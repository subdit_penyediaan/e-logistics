<?php

Class Obat_Fornas extends CI_Controller {
    
    var $title = 'Formularium Obat Nasional';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/obat_fornas_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/obat_fornas',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['nama_pbf']=$this->input->post('nama_pbf');
		$data['telp_pbf']=$this->input->post('telp_pbf');
		$data['almt_pbf']=$this->input->post('almt_pbf');
		$query=$this->Pbf_Model->input_data_m($data);
	}


	function get_data_fornas(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/obat_fornas/get_data_fornas';
		$config['total_rows'] = $this->obat_fornas_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->obat_fornas_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/obat_fornas_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->Pbf_Model->deleteData($kode);
    }

    function get_data_by_id(){
    	$key=$this->input->post('id');
    	$query=$this->obat_fornas_model->getDataComp($key);
    	
    }

    function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->obat_fornas_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/obat_fornas_list',$data);
	}
}
?>
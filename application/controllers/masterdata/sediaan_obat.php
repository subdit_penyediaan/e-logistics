<?php

Class Sediaan_Obat extends CI_Controller {
    
    var $title = 'Sediaan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/sediaan_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/sediaan_obat',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }
	
	function input_data(){
		$data = array();
		$data['nama_sediaan_obat']=$this->input->post('nama_sediaan_obat');
		
		$query=$this->sediaan_obat_model->input_data_m($data);
	}

	function get_list_sediaan(){
		$qry=$this->db->query("select * from ref_obat_sediaan");
		foreach ($qry->result() as $rows) {
			echo '<tr><td><input type="checkbox" name="chk[]" id="cek_del_sediaan" value="'.$rows->id.'" class="chk"></td>';
			echo '<td>'.$rows->id.'</td>';
			echo '<td>'.$rows->jenis_sediaan.'</td></tr>';
			//echo '<td><button><span class="glyphicon glyphicon-remove"></span></button> <button><span class="glyphicon glyphicon-pencil"></span></button></td></tr>';
		}
	}

	function get_data_sediaan(){
		$data['base_url']=$this->url;
		
		//pagination
		$this->load->library('pagination');
		$config['base_url']= $data['base_url'].'index.php/masterdata/sediaan_obat/get_data_sediaan';
		$config['total_rows'] = $this->sediaan_obat_model->countAllData(); //untuk menghitung banyaknya rows
		//$config['total_rows'] = 50;
		$config['per_page'] = 10; //banyaknya rows yang ingin ditampilkan
		$config['uri_segment'] = 4;

		$this->pagination->initialize($config);

		$data['result']=$this->sediaan_obat_model->getData($config['per_page'],$this->uri->segment(4));
		$data['links'] = $this->pagination->create_links();
		$this->load->view('masterdata/sediaan_obat_list',$data);
		//$this->load->view('list_puskesmas');
	}

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    
    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);
        
        print json_encode($query);

        /*for($i=0;$i<sizeof($query);$i++) {
            echo $query[$i]['id_fornas'] . "|" . $query[$i]['nama_obat'] ."\n";
        }*/
    }

	function delete_list() {
        $kode=$this->input->post('kode');
        $this->sediaan_obat_model->deleteData($kode);
    }

    function search_data(){
		$key=$this->input->post('key');

		$data['base_url']=$this->url;
		$data['result']=$this->sediaan_obat_model->searchData($key);
		$data['links'] = '';
		$this->load->view('masterdata/sediaan_obat_list',$data);
	}
}
?>
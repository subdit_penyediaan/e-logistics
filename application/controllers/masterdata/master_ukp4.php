<?php

Class Master_Ukp4 extends CI_Controller {
    
    //var $title = 'Golongan Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/master_ukp4_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['kode_obat']=$this->input->post('kode_obat');
			$data['base_url']=$this->url;
			$this->load->view('masterdata/master_ukp4',$data);	
		}else{
			redirect('login');
		}
        
    }
	
	function input_data(){
		$data = array();
		$data['id_ukp4']=$this->input->post('id_ukp4');
		$data['kode_obat']=$this->input->post('kode_obat');
		$query=$this->master_ukp4_model->input_data_m($data);
	}


	function get_data_master_ukp4($id_obat){
		$data['base_url']=$this->url;
		
		$data['result']=$this->master_ukp4_model->getData($id_obat);
		
		$this->load->view('masterdata/master_ukp4_list',$data);
		//$this->load->view('list_puskesmas');
	}

	  
	function delete_list() {
        $kode=$this->input->post('kode');
        $this->master_ukp4_model->deleteData($kode);
    }

	function get_list_ukp4(){
 		$key=$this->input->get("term");
		$query = $this->master_ukp4_model->GetListUkp4($key);		
        print json_encode($query);
 	}        
}
?>
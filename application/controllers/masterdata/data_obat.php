<?php

Class Data_Obat extends CI_Controller {
    
    var $title = 'Data Obat';

    function __construct() {
        parent::__construct();
        //if(!$this->session->userdata('id')) redirect('login');
        $this->url=base_url();
        $this->load->model('masterdata/data_obat_model');
    }
    
    function index() {
    	if($this->session->userdata('login')==TRUE){
			$data['user']=$this->session->userdata('username');
			$data['base_url']=$this->url;
			$this->getobatallB($data);
			//$this->load->view('masterdata/Data_Obat',$data);	
		}else{
			redirect('login');
		}
        //$data['content_page'] = "admdata/bank";
		//$data['user'] = "ibnu";
		//$this->load->view('Stok_Obat', $data);
    }
	
    function getobatall($data){
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_obat_all limit 100");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li><a href="masterdata/detail_data_obat/getAllDt/'.$rows->id_obat.'">'.$rows->object_name.'
						'.$rows->detil_kemasan.'-'.$rows->org_pembuat.' ('.$rows->status.')</a></li>';
		}
		$data['list_obat']=$list_obat;
    	$this->load->view('masterdata/data_obat', $data);	
    }

    function getobatallB($data){
    	//$data['base_url']=$thiss->url;
    	$list_obat="";
    	$qry=$this->db->query("
    		SELECT SUBSTRING(oa.object_name,1,1) AS index_page
			FROM ref_obat_all oa
			GROUP BY index_page
			ORDER BY index_page
    	");
		foreach ($qry->result() as $rows) {
			//$list_obat.= '<li class="list_master"><a href="masterdata/data_obat/getlistmaster/'.$rows->index_page.'" class="a_master">'.$rows->index_page.'</a></li>';
			$list_obat.= '<li class="list_master"><a href="masterdata/data_obat/getpage_2/'.$rows->index_page.'" class="a_master">'.$rows->index_page.'</a></li>';
		}
		$data['list_obat']=$list_obat;
    	$this->load->view('masterdata/data_obatB', $data);
    	//echo $list_obat;	
    }

    function getpage_2($char){
    	$list_obat="";
    	$qry=$this->db->query("
    		SELECT SUBSTRING(oa.object_name,1,3) AS index_page
			FROM ref_obat_all oa
			WHERE object_name like '$char%'
			GROUP BY index_page
			ORDER BY index_page
    	");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li class="list_master"><a href="masterdata/data_obat/getlistmaster/'.$rows->index_page.'" class="a_master">'.$rows->index_page.'</a></li>';
		}
		//$data['list_obat']=$list_obat;
    	//$this->load->view('masterdata/data_obatB', $data);
    	echo $list_obat;		
    }

    function getlistmaster($char){
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_obat_all where object_name like '$char%'");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li><a href="masterdata/detail_data_obat/getAllDt/'.$rows->id_obat.'">'.$rows->object_name.'
						'.$rows->detil_kemasan.'-'.$rows->org_pembuat.' ('.$rows->status.')</a></li>';
		}
		$data['data_obat']=$list_obat;
    	$this->load->view('masterdata/data_obat', $data);
    	//echo $data['data_obat'];
    }

    function search_by(){
    	$key=$this->input->post('key_master');
    	$list_obat="";
    	$qry=$this->db->query("select * from ref_obat_all where object_name like '%$key%'");
		foreach ($qry->result() as $rows) {
			$list_obat.= '<li><a href="masterdata/detail_data_obat/getAllDt/'.$rows->id_obat.'">'.$rows->object_name.'
						'.$rows->detil_kemasan.'-'.$rows->org_pembuat.' ('.$rows->status.')</a></li>';
		}
		$data['data_obat']=$list_obat;
    	$this->load->view('masterdata/data_obat', $data);
    	//echo $list_obat;
    }

	function input_data(){
		$data = array();
		$data['new_kode_master']=$this->input->post('new_kode_master');
		$data['barcode']=$this->input->post('barcode');
		//$data['nama_obat']=$this->input->post('nama_obat');
        $data['nama_obat']=$this->input->post('nama_dagang').' '.$this->input->post('sediaan').' '.$this->input->post('text_kemasan');
		$data['kode_binfar']=$this->input->post('kode_binfar');
		$data['no_reg']=$this->input->post('no_reg');
        $data['kategori_objek']=$this->input->post('kategori_objek');
        $data['nama_dagang']=$this->input->post('nama_dagang');
        $data['text_kemasan']=$this->input->post('text_kemasan');//kekuatan
        $data['kemasan']=$this->input->post('kemasan');
        $data['sediaan']=$this->input->post('sediaan');        
        $data['detil_kemasan']=$this->input->post('detil_kemasan');
        //$data['satuan_jual']=$this->input->post('satuan_jual');
        $data['val_satuan_klin']=$this->input->post('val_satuan_klin');
        $data['satuan_klin']=$this->input->post('satuan_klin');
        $data['kategori']=$this->input->post('kategori');
        $data['gol_obat']=$this->input->post('gol_obat');
        $data['jenis_obat']=$this->input->post('jenis_obat');
        $data['produsen']=$this->input->post('produsen');
        $data['status']=$this->input->post('status');
        //if(empty($this->input->post('kode_fda'))){
        if(($this->input->post('kode_fda'))==''){
            $data['kode_generik']=$this->input->post('kode_inn');
        }else{
            $data['kode_generik']=$this->input->post('kode_fda');    
        }
		$query=$this->data_obat_model->input_data_m($data);
        if($query){
            $message['text']="Data berhasil dimasukkan";
        }else{
            $message['text']="Data gagal dimasukkan";
        }
        echo json_encode($message);
	}

	function list_input_data(){
		$qry=$this->db->query("select * from stok_obat");
		foreach ($qry->result() as $rows) {
			echo '<tr><td>'.$rows->id_fornas.'</td>';
			echo '<td>'.$rows->nama_obat.'</td>';
			echo '<td>'.$rows->stok.'</td>';
			echo '<td>'.$rows->expired.'</td>';
			echo '<td>'.$rows->harga.'</td>';
			echo '<td>'.$rows->sumber_dana.'</td></td>';
		}
	}

	function get_data_by_id() {
        $ret = $this->Stok_Obat_Model->GetDataById();
		echo json_encode($ret);
    }
    
    function get_list_fornas() {
        $key=$this->input->get("term");
		//var $term;
		$query = $this->Stok_Obat_Model->getlistfornas($key);
        
        print json_encode($query);

        /*for($i=0;$i<sizeof($query);$i++) {
            echo $query[$i]['id_fornas'] . "|" . $query[$i]['nama_obat'] ."\n";
        }*/
    }

	function delete_list() {
        $ret['command'] = "deleting data";
        if($this->Bank_Model->DoDeleteData()) {
            $ret['msg'] = "Sukses Menghapus";
            $ret['status'] = "success";
        } else {
            $ret['msg'] = "Gagal Menghapus";
            $ret['status'] = "error";
        }
		
		echo json_encode($ret);
    }

    function add_data_obat($x){
    	$data['base_url']=$this->url;
    	//$data['kemasan']="";
    	$data['sediaan']=$this->data_obat_model->getSediaan();
    	$data['satuan']=$this->data_obat_model->getSatuan();
    	//$data['satuan_klinis']="";
    	$data['golongan']=$this->data_obat_model->getGolongan();
    	if($x=='obat'){
            $query=$this->db->query("
                SELECT SUBSTRING(id_obat,4,9) AS kode_int
                FROM ref_obat_all
                WHERE id_obat LIKE 'DRG%'
                ORDER BY id_obat DESC
                LIMIT 1
                ");
            $result=$query->row_array();
            $kode=$result['kode_int']+1;
            $data['id_obat']="DRG".$kode;
            //$this->session->
    		$this->load->view('masterdata/add_data_obat',$data);	
    	}elseif ($x=='bmhp'){
            $query=$this->db->query("
                SELECT SUBSTRING(id_obat,5,9) AS kode_int
                FROM ref_obat_all
                WHERE id_obat LIKE 'MDVx%'
                ORDER BY id_obat DESC
                LIMIT 1
                ");
            $result=$query->row_array();
            $kode=$result['kode_int']+1;
    		$data['id_obat']="MDVx".$kode;
            $this->load->view('masterdata/add_data_obat',$data);
            
    	}else{
            $query=$this->db->query("
                SELECT SUBSTRING(id_obat,4,9) AS kode_int
                FROM ref_obat_all
                WHERE id_obat LIKE 'SBx%'
                ORDER BY id_obat DESC
                LIMIT 1
                ");
            $result=$query->row_array();
            $kode=$result['kode_int']+1;
            $data['id_obat']="SBx".$kode;
            //echo "alkes";
            $this->load->view('masterdata/add_data_alkes',$data);
        }
    	
    }

    function get_list_pabrik(){
        $key=$this->input->get("term");
        //var $term;
        $query = $this->data_obat_model->GetListPabrik($key);
        print json_encode($query);
    }

    function get_list_fda(){
        $key=$this->input->get("term");
        //var $term;
        $query = $this->data_obat_model->GetListFDA($key);
        print json_encode($query);
    }

    function get_list_inn(){
        $key=$this->input->get("term");
        //var $term;
        $query = $this->data_obat_model->GetListINN($key);
        print json_encode($query);
    }

    function get_list_obat_grid(){
        $response= new stdClass();

        $searchTerm=$this->input->get("searchTerm");
        $page=$this->input->get("page");
        $sidx=$this->input->get("sidx");
        $limit=$this->input->get("rows");
        $sord=$this->input->get("sord");
        //$rows=$this->input->get("rows");
        if(!$sidx) $sidx =1;
        if ($searchTerm=="") {
            $searchTerm="%";
        } else {
            $searchTerm = "%" . $searchTerm . "%";
        }

        $result = $this->db->query("
            SELECT COUNT(*) AS count FROM ref_obat_all WHERE object_name like '$searchTerm'
            ");
        $row = $result->row_array();
        $count = $row['count'];

        if( $count >0 ) {
            $total_pages = ceil($count/$limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages) $page=$total_pages;
        $start = $limit*$page - $limit; // do not put $limit*($page - 1)
        
        if($total_pages!=0) 
            $SQL = "
                    SELECT * FROM ref_obat_all 
                    WHERE object_name like '$searchTerm'  
                    ORDER BY $sidx $sord LIMIT $start , $limit";
        else $SQL = "
                    SELECT * FROM ref_obat_all
                    WHERE object_name like '$searchTerm'  
                    ORDER BY $sidx $sord";
        $query=$this->db->query($SQL);
        
        $response->page = $page;
        $response->total = $total_pages;
        $response->records = $count;
        $i=0;
        //$r = array();
        foreach ($query->result_array() as $key) {            
            $response->rows[$i]['name']=$key['object_name'];
            //$response->rows[$i]['kemasan']=$key['detil_kemasan']; 
            $response->rows[$i]['author']=$key['org_pembuat'];            
            $i++;
        }
        //return $r;

        //$query = $this->detail_faktur_model->GetListObat($key);
        echo json_encode($response);
    }

    
}
?>
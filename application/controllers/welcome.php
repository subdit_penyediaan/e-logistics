<?php

class Welcome extends CI_Controller {

	function Welcome()
	{
		parent::__construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('file');
	}
	
	function index()
	{
		$this->load->view('upload_form');
	}

    function do_upload()
	{
		$config['upload_path'] = './temp_upload/';
		$config['allowed_types'] = 'xls';
                
		$this->load->library('upload', $config);
                

		if ( ! $this->upload->do_upload())
		{
			$data = array('error' => $this->upload->display_errors());
			
		}
		else
		{
            $data = array('error' => false);
			$upload_data = $this->upload->data();

            $this->load->library('excel_reader');
			$this->excel_reader->setOutputEncoding('CP1251');

			$file =  $upload_data['full_path'];
			$this->excel_reader->read($file);
			error_reporting(E_ALL ^ E_NOTICE);

			// Sheet 1
			$data = $this->excel_reader->sheets[0] ;
                        $dataexcel = Array();
			for ($i = 1; $i <= $data['numRows']; $i++) {

                            if($data['cells'][$i][1] == '') break;
                            $dataexcel[$i-1]['nama'] = $data['cells'][$i][1];
                            $dataexcel[$i-1]['alamat'] = $data['cells'][$i][2];

			}
                        
                        
            delete_files($upload_data['file_path']);
            $this->load->model('User_model');
            $this->User_model->tambahuser($dataexcel);
            $data['user'] = $this->User_model->getuser();
		}
        $this->load->view('hasil', $data);
	}
	
	function run_import(){
	/* $config['upload_path'] = './temp_upload/';
	//$config['allowed_types'] = 'xls,xlsx';
	$this->load->library('upload', $config);
	$upload_data = $this->upload->data();
	$file =  $upload_data['full_path']; */
	
	$file1 = './temp_upload/cobaimport.xlsx';
 
    //$file   = explode('.',$_FILES['database']['name']);
	$file   = explode('.',$file1);
    $length = count($file);
	
    if($file[$length -1] == 'xlsx' || $file[$length -1] == 'xls'){//jagain barangkali uploadnya selain file excel <span class="wp-smiley emoji emoji-smile" title=":-)">:-)</span>
        //$tmp    = $_FILES['database']['tmp_name'];//Baca dari tmp folder jadi file ga perlu jadi sampah di server :-p
        $tmp = $file1;
		//echo $tmp;
		$this->load->library('excel');//Load library excelnya
        $read   = PHPExcel_IOFactory::createReaderForFile($tmp);
		//print_r($read);
        $read->setReadDataOnly(true);
        $excel  = $read->load($tmp);
		//print_r($excel);
        $sheets = $read->listWorksheetNames($tmp);//baca semua sheet yang ada
        foreach($sheets as $sheet){
            if($this->db->table_exists($sheet)){//check sheet-nya itu nama table ape bukan, kalo bukan buang aja... nyampah doank :-p
                $_sheet = $excel->setActiveSheetIndexByName($sheet);//Kunci sheetnye biar kagak lepas :-p
                $maxRow = $_sheet->getHighestRow();
                $maxCol = $_sheet->getHighestColumn();
                $field  = array();
                $sql    = array();
                $maxCol = range('A',$maxCol);
                foreach($maxCol as $key => $coloumn){
                    $field[$key]    = $_sheet->getCell($coloumn.'1')->getCalculatedValue();//Kolom pertama sebagai field list pada table
                }
                for($i = 2; $i <= $maxRow; $i++){
                    foreach($maxCol as $k => $coloumn){
                        $sql[$field[$k]]  = $_sheet->getCell($coloumn.$i)->getCalculatedValue();
                    }
                    $this->db->insert($sheet,$sql);//ribet banget tinggal insert doank...
                }
            }
        }
    }else{
        exit('do not allowed to upload');//pesan error tipe file tidak tepat
    }
    //redirect('home');//redirect after success
	echo "berhasil import";
	}
	
	function importexcel(){
		$file = './temp_upload/cobaimport.xlsx';
 
		//load the excel library
		$this->load->library('excel');
		echo "1";
		$objReader = PHPExcel_IOFactory::createReader('excel2007');
		//read file from path
		echo "2";
		//$objPHPExcel = PHPExcel_IOFactory::load($file);
		$objPHPExcel = $objReader->load($file); 
		//$cell_collection = $objPHPExcel->setActiveSheetIndex(0);
		echo "3";
		//get only the Cell Collection
		//$cell_collection = $objPHPExcel->getActiveSheet();//->getCellCollection();
		$cell_collection = $objPHPExcel->getSheet(0);
		echo "4"; 
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
			$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
			$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
			$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		 
			//header will/should be in row 1 only. of course this can be modified to suit your need.
			if ($row == 1) {
				$header[$row][$columns] = $data_value;
			} else {
				$arr_data[$row][$col] = $data_value;
			}
		}
		 
		//send the data in an array format
		$data['header'] = $header;
		$data['values'] = $arr_data;
		
		print_r($data);
		echo "test";
	}
	
	function importlagi(){
	//$file = './temp_upload/cobaimport.xlsx';
		//load library phpExcel
		  $this->load->library("excel");
		  //here i used microsoft excel 2007
		  $objReader = PHPExcel_IOFactory::createReader('Excel2007');
		  //set to read only
		  $objReader->setReadDataOnly(true);
		  //load excel file
		  $objPHPExcel = $objReader->load("cobaimport.xlsx");
		  $objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		  //load model
		  $this->load->model("User_model");
		  //loop from first data until last data
		  for($i=2; $i<=77; $i++){
		   $name = $objWorksheet->getCellByColumnAndRow(0,$i)->getValue();
		   $address = $objWorksheet->getCellByColumnAndRow(1,$i)->getValue();
		   $data_user = array(
			"name" => $name,
			"username" => $address       );
		   $this->User_model->add_data($data_user);
			}
	}
	
	function importkeempat(){
	
		$inputFileName = 'cobaimport.xlsx';
		$this->load->library("excel");
		
		try {
			$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = $objReader->load($inputFileName);
		} catch (Exception $e) {
			die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) 
			. '": ' . $e->getMessage());
		}

		//  Get worksheet dimensions
		$sheet = $objPHPExcel->getSheet(0);
		$highestRow = $sheet->getHighestRow();
		$highestColumn = $sheet->getHighestColumn();

		//  Loop through each row of the worksheet in turn
		for ($row = 1; $row <= $highestRow; $row++) {
			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, 
			NULL, TRUE, FALSE);
			foreach($rowData[0] as $k=>$v)
				echo "Row: ".$row."- Col: ".($k+1)." = ".$v."<br />";
		}
	}
}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */
<?php
	class obat_m extends CI_Model{
		function construct(){
			parent::__construct();
		}

		public function get_autocomplete($search_data) {
	        $this->db->select('id_fornas');
	        $this->db->select('nama_obat');
	        $this->db->like('nama_obat', $search_data);
	        return $this->db->get('ref_fornas');
    	}
    }
?>

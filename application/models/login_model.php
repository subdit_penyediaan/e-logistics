<?php
Class Login_Model extends CI_Model {
	
	function cek($user,$pswd){
		$query=$this->db->query("
			select *
			from tb_user
			where username='$user' and pswd='$pswd'
			");// and status = '1'

		if($query->num_rows()>0){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	function getLogPenerimaan($periodeNow)
	{
		$this->db->select("DATE_FORMAT(create_time, '%Y-%m-%d') AS date_log", false);
		$this->db->like('periode', $periodeNow, 'after');
		$this->db->order_by('create_time', 'desc');
		$this->db->limit(1);

		return $this->db->get('tb_penerimaan');
	}

	function getLogLplpo($periodeNow)
	{
		$this->db->select("DATE_FORMAT(create_time, '%Y-%m-%d') AS date_log", false);
		$this->db->like('periode', $periodeNow, 'after');
		$this->db->order_by('create_time', 'desc');
		$this->db->limit(1);

		return $this->db->get('tb_lplpo');
	}

	function getLogDistribusi($periodeNow)
	{
		$this->db->select("DATE_FORMAT(create_time_real, '%Y-%m-%d') AS date_log", false);
		$this->db->like('create_time', $periodeNow, 'after');
		$this->db->order_by('create_time_real', 'desc');
		$this->db->limit(1);

		return $this->db->get('tb_detail_distribusi');	
	}
}
?>
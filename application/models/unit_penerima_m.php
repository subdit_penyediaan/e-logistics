<?php
	class Unit_penerima_m extends CI_Model{
		function construct(){
			parent::__construct();
		}

		function getDataPuskesmas($limit,$start){
			$this->db->limit($limit, $start);
			//$this->db->order_by("KECDESC");
			$this->db->order_by("NAMA_PUSKES");
			$query=$this->db->get("ref_puskesmas");
			//$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
			return $query->result();
		}

		function countAllPuskesmas(){
			return $this->db->count_all("ref_puskesmas");
		}

		function getDataUnitEks(){
			$query = $this->db->query("select * from ref_puskesmas where KODE_DESA like '006%'");

			return $query->result();
		}

	}
?>
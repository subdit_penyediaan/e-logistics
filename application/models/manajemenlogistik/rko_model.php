<?php
Class Rko_model extends CI_Model {

	function create($values)
	{
		$query = $this->db->insert_batch('tb_rko', $values);

		return $query;
	}

	function insert_array($data_array)
	{
		return $this->db->insert_batch('tb_stok_akhir_tahun', $data_array);
	}

	function template()
	{
		$this->db->where('selected', 'y');
		$this->db->where('rko_id >', '0');
		$this->db->where('delete_time', null);
		$query = $this->db->get('ref_fornas');

		return $query;
	}

	function get_data_by($condition)
	{
		$this->db->select('tb_rko.*');
		$this->db->select('ref_fornas.nama_obat');
		$this->db->join('ref_fornas', 'ref_fornas.id_fornas = tb_rko.fornas_id');
		foreach ($condition as $key => $value) {
			$this->db->where($key, $value);
		}
		$query = $this->db->get('tb_rko');

		return $query;
	}

	function custom_select_by($condition)
	{
		$this->db->select('ref_fornas.rko_id as OBAT_ID');
		$this->db->select('stock_two_years_ago as STOK, average_used_two_years_ago as PAKAI, procurement_plan as PENGADAAN');
		$this->db->select('real_two_years_ago as REALISASI, information as KETERANGAN');
		$this->db->join('ref_fornas', 'ref_fornas.id_fornas = tb_rko.fornas_id');
		foreach ($condition as $key => $value) {
			$this->db->where($key, $value);
		}
		$this->db->where('stock_two_years_ago >', '0');
		$this->db->where('procurement_plan >', '0');
		$this->db->group_by('tb_rko.rko_id');
		$query = $this->db->get('tb_rko');

		return $query;
	}

	function get_history()
	{
		$this->db->group_by('year');
		$query = $this->db->get('tb_rko');

		return $query;
	}

	function delete_by_year($year)
	{
		$this->db->where('year', $year);
		$query = $this->db->delete('tb_rko');

		return $query;
	}

	function get_data_stok_akhir($tahun)
	{
		$this->db->where('tahun', $tahun);
		$query = $this->db->get('tb_stok_akhir_tahun');

		return $query;
	}

	function update($id, $data)
	{
		$this->db->where('id', $id);
		return $this->db->update('tb_rko', $data);
	}

 	function delete($id)
 	{
 		$this->db->where('id', $id);
 		return $this->db->delete('tb_rko');
 	}

 	function insert_log($data)
    {
        date_default_timezone_set('Asia/Jakarta');

        $log['id'] = date('d-m-Y')."/".date('H:i:s');
        $log['periode'] = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];
        $log['namatable'] = $data['namatable'];
        $log['jenis_laporan'] = 10;
        $hasil = $this->db->insert('log_sinkron', $log);
        return $hasil;
    }
}

<?php
Class Lplpo_Model extends CI_Model {

	function getDataKecamatan(){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			select id_kab
			from tb_institusi
			");
		$kab = $datakab->row_array();
		//print_r($kab);
		$kabupaten=$kab['id_kab'];
		$query = $this->db->query("
			select *
			from ref_kecamatan
			where KDKAB='$kabupaten'");
		return $query->result_array();
	}

	function GetComboPuskesmas($key){
		$sql = $this->db->query("
            SELECT KD_PUSK, NAMA_PUSKES FROM ref_puskesmas WHERE KODE_KEC='$key' ORDER BY NAMA_PUSKES"
        );
        return $sql->result_array();
	}

	function show_data($set){
		//order by dl.nama_obj
		$query = $this->db->query("
			SELECT dl.stok_awal as stok_awal, dl.rusak_ed as rusak_ed,
			dl.id AS id, dl.id_lplpo AS id_lplpo,dl.kode_obat AS kode_obat,
			oa.nama_obat AS nama_obat, oa.deskripsi AS sediaan,
			dl.terima AS terima, dl.sedia AS sedia, dl.pakai AS pakai,
			dl.stok_akhir AS stok_akhir, dl.stok_opt AS stok_opt,
			dl.permintaan AS permintaan, dl.dana AS dana_mahasiswa, dl.ket AS ket,
			dl.nama_obj as nama_obj,dl.pemberian as pemberian
			FROM tb_detail_lplpo dl
			JOIN ref_obat_all oa ON(oa.id_obat = dl.kode_obat)
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			WHERE l.kode_kec =? AND l.kode_pusk = ? AND l.periode =?
			order by dl.nama_obj
			",array(
				$set['kec'],
				$set['nama_pusk'],
				$set['per'].'-00',
				)
			);
		return $query->result();
	}

	function getlistempty(){
		$query=$this->db->query("
			SELECT p.NAMA_PUSKES as nama_pusk, kec.KECDESC as nama_kec
			FROM ref_puskesmas p
			JOIN ref_kecamatan kec ON (kec.KDKEC=p.KODE_KEC)
			WHERE p.KODE_KAB='1116' AND p.KD_PUSK NOT IN(SELECT kode_pusk FROM tb_lplpo WHERE EXTRACT(MONTH FROM periode)=3)
			");
		return $query->result();
	}

	function getlistemptyby($key){
		$profil=$this->db->query("
			SELECT id_kab from tb_institusi
			");
		$id_kab=$profil->row_array();
		$keykab=$id_kab['id_kab'];
		$query=$this->db->query("
			SELECT p.NAMA_PUSKES as nama_pusk, kec.KECDESC as nama_kec
			FROM ref_puskesmas p
			JOIN ref_kecamatan kec ON (kec.KDKEC=p.KODE_KEC)
			WHERE p.KODE_KAB='$keykab' AND p.KD_PUSK NOT IN(SELECT kode_pusk FROM tb_lplpo WHERE EXTRACT(MONTH FROM periode)=$key)
			");
		return $query->result();
	}

	function getProfilPrint($key){
		$query=$this->db->query("
			SELECT p.NAMA_PUSKES AS nama_pusk, kec.KECDESC AS nama_kec, kab.CKabDescr AS nama_kab,
			prop.CPropDescr AS nama_prop
			FROM ref_puskesmas p
			JOIN ref_kecamatan kec ON (kec.KDKEC = p.KODE_KEC)
			JOIN ref_kabupaten kab ON (kab.CKabID=p.KODE_KAB)
			JOIN ref_propinsi prop ON (prop.CPropID=p.KODE_PROP)
			WHERE p.KD_PUSK='$key'
			");
		return $query->row_array();
	}

	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$cek = $this->db->query("
			select * from tb_lplpo
			where `kode_pusk`=? and `periode`=?
			",
			array($data['kode_pusk'],
				$data['per_lplpo'].'-00')
			);
		if ($cek->num_rows > 0){
			return false;
		}else{
			$sql = $this->db->query("
				INSERT INTO `tb_lplpo` (
						`kode_kec`,
						`kode_pusk`,
						`periode`,
						`create_time`,
						`create_by`
				) VALUES (
					?,?,?,?,?
				)",
				array(
					$data['kode_kec'],
					$data['kode_pusk'],
					$data['per_lplpo'].'-00',
					$time,
					$id_user
				)
			);
			return true;
		}	
	}

	function cek_eksis($id_lplpo){
		$query=$this->db->query("
			select * from tb_detail_lplpo where id_lplpo=$id_lplpo
			");

		if($query->num_rows()>0){
			//echo 'eksis';
			return true;
		}else{
			//echo 'not eksis';
			return false;
		}
	}
}
?>
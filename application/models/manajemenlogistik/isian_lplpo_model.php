<?php
Class Isian_Lplpo_Model extends CI_Model {
	
	function input_data_m($data){

		$stok_akhir=$data['terima']+$data['stok_awal']-$data['pakai'];
		$sql = $this->db->query("
			INSERT INTO `tb_detail_lplpo` (
					`id_lplpo`,
					`kode_obat`,
					`terima`,
					`sedia`,
					`pakai`,
					`stok_akhir`,
					`stok_opt`,
					`permintaan`,
					`dana`,
					`ket`,
					`stok_awal`,
					`rusak_ed`,
					`nama_obj`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['id_lplpo'],
				$data['kode_obat'],
				$data['terima'],
				$data['sedia'],
				$data['pakai'],
				$stok_akhir,
				$data['stok_op'],
				$data['minta'],
				$data['dana'],
				$data['ket'],
				$data['stok_awal'],
				$data['rusak_ed'],
				$data['nama_obj']
			)
		);
		//return $sql;	
	}

	function countAllData($id_lplpo){
		$this->db->where('id_lplpo',$id_lplpo);
		$this->db->from('tb_detail_lplpo');

		return $this->db->count_all_results();
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_detail_lplpo
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start,$id_lplpo){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("");
		//$this->db->order_by("");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT dl.id_lplpo as id_lplpo,dl.stok_awal as stok_awal, dl.rusak_ed as rusak_ed,
			dl.id AS id, dl.id_lplpo AS id_lplpo,dl.kode_obat AS kode_obat,
			oa.nama_obat AS nama_obat, oa.deskripsi AS sediaan,
			dl.terima AS terima, dl.sedia AS sedia, dl.pakai AS pakai,
			dl.stok_akhir AS stok_akhir, dl.stok_opt AS stok_opt,
			dl.permintaan AS permintaan, dl.dana AS dana_mahasiswa, dl.ket AS ket,
			oa.kekuatan as kekuatan, dl.pemberian as pemberian
			FROM tb_detail_lplpo dl
			JOIN ref_obat_all oa ON(oa.id_obat = dl.kode_obat)
			where dl.id_lplpo = $id_lplpo
			order by dl.nama_obj
			");
		$this->db->limit($limit, $start);
		return $query->result();

	}

	function getSatuan(){
		$query=$this->db->query("
			select satuan_obat as satuan_obat
			from  ref_obat_satuan
			limit 10");
		return $query->result_array();
	}

	function getDataDetaillplpo($nolplpo){
		$query=$this->db->query("
			SELECT l.id as id,kec.KECDESC AS nama_kec, pusk.NAMA_PUSKES AS nama_pusk,DATE_FORMAT(l.periode,'%M %Y') AS periode
			FROM tb_lplpo l
			JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
			JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk) 
			where l.id='$nolplpo'
			");
		return $query->row_array();	
	}

	function GetListObat($key){
	    $query = $this->db->query("
	    	SELECT oa.org_pembuat as produsen,so.kode_obat as id_obat,oa.nama_obat AS nama_obat, oa.deskripsi AS deskripsi, oa.kekuatan AS kekuatan
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON(oa.id_obat=so.kode_obat)
	    	where oa.nama_obat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obat,
	    		'desc'	=>$key->deskripsi,
	    		'pwr'	=>$key->kekuatan,
	    		'produsen'=>$key->produsen
	    	);
	    }
	    return $r;
	}

	//tb_detail_lplpo update
	//tb_detail_distribusi insert
}
?>
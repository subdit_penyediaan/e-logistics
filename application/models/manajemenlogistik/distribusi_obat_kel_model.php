<?php
Class Distribusi_obat_kel_model extends CI_Model {

	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `tb_distribusi` (

					`kode_up`,
					`tanggal_trans`,
					`no_dok`,
					`user`,
					`nip`,
					`periode`,
					`id_user`,
					`unit_eks`,
					`keperluan`
			) VALUES (
				?,?,?,?,?,?,?,?,?
			)",
			array(
				//$data['kec'],
				$data['unit_penerima'],
				$data['datenow'],
				$data['id_dok'],
				$data['user_penerima'],
				$data['nip'],
				$data['periode'].'-00',
				$this->session->userdata('id'),
				$data['unit_luar'],
				$data['keperluan']
			)
		);

		return $sql;
	}

	function getDataToChange($key){
		$query = $this->db->query("select * from tb_distribusi where id_distribusi ='$key'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_distribusi'	=>$key->id_distribusi,
	    		'tanggal'	=>$key->tanggal_trans,
	    		'unit_eks'	=>$key->unit_eks,
	    		'periode'	=>$key->periode,
	    		'user'	=>$key->user,
	    		'keperluan'	=>$key->keperluan,
	    		'no_dok'	=>$key->no_dok

	    	);
	    }
	    return $r;
	}

	function countAllData(){
		return $this->db->count_all("tb_distribusi");
	}

	function deleteData($kode){
		$this->db->where_in('id_distribusi',$kode);
		$this->db->where('status','belum diproses');
		$this->db->delete('tb_distribusi');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}

	}

	function getData($limit,$start){
		$query=$this->db->query("
			SELECT dist.id_distribusi as id,dist.tanggal_trans AS tanggal, dist.user AS nama_user, DATE_FORMAT(dist.periode,'%M %Y') AS periode,
			dist.unit_eks AS penerima, dist.status AS status_aktif, dist.no_dok AS no_dok
			FROM tb_distribusi dist
			");
		$this->db->limit($limit, $start);

		return $query->result();
	}

	function getDataKecamatan(){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			select id_kab
			from tb_institusi
			");
		$kab = $datakab->row_array();
		$kabupaten=$kab['id_kab'];
		$query = $this->db->query("
			select *
			from ref_kecamatan
			where KDKAB='$kabupaten'");
		return $query->result_array();
	}

	function getDataCito(){
		$query = $this->db->query("
			select *
			from ref_cito
			");
		return $query->result_array();
	}

	function GetComboPuskesmas($key){
		$sql = $this->db->query("
            SELECT KD_PUSK, NAMA_PUSKES FROM ref_puskesmas WHERE KODE_KEC='$key' ORDER BY NAMA_PUSKES"
        );
        return $sql->result_array();
	}

	function GetComboKab($key){
		$sql = $this->db->query("
            SELECT CKabID, CKabDescr FROM ref_kabupaten WHERE CPropID='$key' ORDER BY CKabDescr"
        );
        return $sql->result_array();
	}

	function getpermintaan($start='',$limit=''){
		$sql_limit = '';
		if($limit!='')
			$sql_limit = "LIMIT ".$start.",".$limit;
		$query=$this->db->query("
			SELECT distinct l.id as id,kec.KECDESC AS kecamatan, pusk.NAMA_PUSKES AS puskesmas,DATE_FORMAT(l.periode,'%M %Y') AS periode,
			DATE_FORMAT(l.create_time,'%d %M %Y') AS cur_date
			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON(l.id = dl.id_lplpo)
			JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
			JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk)
			WHERE NOT dl.permintaan=0 AND l.id NOT IN (SELECT id_lplpo FROM tb_detail_distribusi)
				$sql_limit
			");

		return $query;
	}

	function getpermintaandone($start='',$limit=''){
		$sql_limit = '';
		if($limit!='')
			$sql_limit = "LIMIT ".$start.",".$limit;
		$query=$this->db->query("
		SELECT DISTINCT l.id AS id,kec.KECDESC AS kecamatan, pusk.NAMA_PUSKES AS puskesmas,DATE_FORMAT(l.periode,'%M %Y') AS periode,
		DATE_FORMAT(l.create_time,'%d %M %Y') AS cur_date
		FROM tb_detail_distribusi dd
		JOIN tb_lplpo l ON(l.id = dd.id_lplpo)
		JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
		JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk)
			$sql_limit
		");

		return $query;
	}

	function getdetailpermintaanlist($id_lplpo){
		$query=$this->db->query("
			SELECT dl.id_lplpo as id_lplpo,dl.stok_opt as stok_opt,stok_akhir,
			dl.kode_generik as kode_obat,dl.nama_obj AS nama_obat, dl.permintaan AS permintaan, dl.sediaan as sediaan
			FROM tb_detail_lplpo_kel dl

			WHERE NOT permintaan=0 AND id_lplpo = $id_lplpo
			ORDER BY nama_obat
			");

		return $query->result();
	}

	function getdetailpermintaanlistdone($id_lplpo){
		// SELECT *
		// 	FROM (
		// 		SELECT dl.id_lplpo AS id_lplpo,dl.stok_opt AS stok_opt,dd.id_stok,
		// 		dl.kode_generik AS kode_obat, hg.nama_objek AS nama_obat,
		// 		dl.permintaan AS permintaan, dl.sediaan AS sediaan,
		// 		dd.pemberian AS pemberian, IF(dl.pemberian = 0,0,dd.no_batch) AS no_batch,
		// 			IF(dl.pemberian > 0,roa.nama_obj,'(edited)') AS obat_res,dd.id AS id,dd.kode_obat_res AS kode_obat_res,dl.id AS id_dl
		// 		FROM tb_detail_distribusi dd
		// 		JOIN tb_stok_obat roa ON roa.kode_obat = dd.kode_obat_res
		// 		JOIN hlp_generik hg ON hg.id_hlp = dd.kode_obat_req
		// 		JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = dd.kode_obat_req
		// 		WHERE dd.id_lplpo = $id_lplpo AND dl.id_lplpo = $id_lplpo
		// 		group by kode_obat, kode_obat_res
		// 		UNION
		// 		SELECT dl.id_lplpo AS id_lplpo,dl.stok_opt AS stok_opt,0 AS id_stok,
		// 		dl.kode_generik AS kode_obat,dl.nama_obj AS nama_obat, dl.permintaan AS permintaan, dl.sediaan AS sediaan,0 AS pemberian,
		// 		0 AS no_batch, '' AS obat_res,'' AS id, 0 AS kode_obat_res, id AS id_dl
		// 		FROM tb_detail_lplpo_kel dl

		// 		WHERE permintaan > 0 AND id_lplpo = $id_lplpo AND pemberian < 1
		// 		ORDER BY nama_obat
		// 	) ex
		// 	GROUP BY kode_obat
		// 	ORDER BY nama_obat
		$query=$this->db->query("
			SELECT dl.id_lplpo AS id_lplpo,dl.stok_opt AS stok_opt,dd.id_stok,
			dl.kode_generik AS kode_obat, hg.nama_objek AS nama_obat,
			dl.permintaan AS permintaan, dl.sediaan AS sediaan,
			dd.pemberian AS pemberian, IF(dl.pemberian = 0,0,dd.no_batch) AS no_batch,
				IF(dl.pemberian > 0,roa.nama_obj,'-') AS obat_res,dd.id AS id,dd.kode_obat_res AS kode_obat_res,dl.id AS id_dl
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat roa ON roa.kode_obat = dd.kode_obat_res
			JOIN hlp_generik hg ON hg.id_hlp = dd.kode_obat_req
			JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = dd.kode_obat_req
			WHERE dd.id_lplpo = $id_lplpo AND dl.id_lplpo = $id_lplpo AND dd.pemberian > 0
			group by no_batch, kode_obat_res
			UNION
			SELECT dl.id_lplpo AS id_lplpo,dl.stok_opt AS stok_opt,0 AS id_stok,
			dl.kode_generik AS kode_obat,dl.nama_obj AS nama_obat, dl.permintaan AS permintaan, dl.sediaan AS sediaan,'' AS pemberian,
			'' AS no_batch, '' AS obat_res,'' AS id, 0 AS kode_obat_res, id AS id_dl
			FROM tb_detail_lplpo_kel dl

			WHERE permintaan > 0 AND id_lplpo = $id_lplpo AND ifnull(pemberian, 0) < 1
			ORDER BY nama_obat
			");

		return $query->result();
	}

	function getQtyDistribution($idLplpo) {
		$query=$this->db->query("
			SELECT dl.id_lplpo AS id_lplpo,
			dl.kode_generik AS kode_obat,COUNT(dl.kode_generik) AS qty
			FROM tb_detail_distribusi dd
			JOIN ref_obat_all roa ON roa.id_obat = dd.kode_obat_res
			JOIN hlp_generik hg ON hg.id_hlp = dd.kode_obat_req
			JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = dd.kode_obat_req
			WHERE dd.id_lplpo = $idLplpo AND dl.id_lplpo = $idLplpo
			GROUP BY kode_obat
			");

		return $query->result();
	}


	function getdetailpermintaan($id){
		$query=$this->db->query("
			SELECT DISTINCT l.id as id,kec.KECDESC AS nama_kec, pusk.NAMA_PUSKES AS nama_pusk,DATE_FORMAT(l.periode,'%Y/%m/01') AS periode

			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON(l.id = dl.id_lplpo)
			JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
			JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk)
			WHERE l.id = $id
			");

		return $query->row_array();
	}

	function GetListObat($key){
	    $query = $this->db->query("
	    	SELECT so.id_stok AS id_stok,so.kode_obat AS kode_obat,hg.nama_objek AS nama_generik,
			so.no_batch AS no_batch, so.expired AS ed, so.stok AS stok,
			so.nama_obj AS nama_obj,p.dana,p.tahun_anggaran,
			((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak
			FROM tb_stok_obat so
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			WHERE so.nama_obj like '%$key%'
			HAVING jarak <= -1 and so.stok > 0
			ORDER BY so.expired

	    	");
		$r = array();
		//ambil id stoknya disini
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obj,
	    		'batch'	=>$key->no_batch,
	    		'stok'	=>$key->stok,
	    		//'pwr'	=>$key->kekuatan,
	    		'id_stok'	=>$key->id_stok,
	    		'ed' =>$key->ed,
	    		'tahun_anggaran' =>$key->tahun_anggaran,
	    		'dana' =>$key->dana
	    	);
	    }

	    return $r;
	}

	function getDataSuggest($key){
	    $query = $this->db->query("
	    	SELECT so.id_stok AS id_stok,so.kode_obat AS kode_obat,hg.nama_objek AS nama_generik,
			so.no_batch AS no_batch, so.expired AS ed, so.stok AS stok,
			so.nama_obj AS nama_obj,p.dana,
			((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak
			FROM tb_stok_obat so
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			WHERE so.kode_generik = '$key'
			HAVING jarak < -1 and so.stok > 0
			ORDER BY so.expired
			LIMIT 1
	    	");
		$r = array();
		//ambil id stoknya disini
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obj,
	    		'batch'	=>$key->no_batch,
	    		'stok'	=>$key->stok,
	    		//'pwr'	=>$key->kekuatan,
	    		'id_stok'	=>$key->id_stok,
	    		'ed' =>$key->ed,
	    		'dana' =>$key->dana
	    	);
	    }

	    return $r;
	}

	function GetListObatBar($key){
	    $query = $this->db->query("
	    	SELECT so.id_stok as id_stok,so.kode_obat AS kode_obat, oa.nama_obat AS nama_obat, oa.kekuatan AS kekuatan,
			so.no_batch AS no_batch, so.expired AS ed, so.stok AS stok,
			oa.deskripsi as sediaan, so.nama_obj as nama_obj
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON(oa.id_obat=so.kode_obat)
			WHERE so.barcode='$key'
			ORDER BY so.expired

	    	");
		$r = array();
		//ambil id stoknya disini
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->kode_obat,
	    		'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obj,
	    		'batch'	=>$key->no_batch,
	    		'stok'	=>$key->stok,
	    		'pwr'	=>$key->kekuatan,
	    		'id_stok'	=>$key->id_stok,
	    		'ed' =>$key->ed
	    	);
	    }
	    return $r;
	}

	function show_data($id_lplpo){
		$query=$this->db->query("
			SELECT dd.kode_obat_res AS kode_obat,so.nama_obj AS nama_obj,
			dd.pemberian AS pemberian,so.no_batch AS no_batch,
			so.expired AS expired,so.harga AS harga,p.dana,p.tahun_anggaran,
			DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat so ON (so.id_stok=dd.id_stok)
			JOIN tb_lplpo l ON (l.id=dd.id_lplpo)
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			WHERE dd.id_lplpo=$id_lplpo and dd.pemberian > 0
			ORDER BY so.nama_obj
			");

		return $query->result();
	}

	function show_data_inn($id_lplpo){
		$query=$this->db->query("
			SELECT hg.id_hlp AS kode_obat,hg.nama_objek AS nama_obj,dd.pemberian AS pemberian,so.no_batch AS no_batch,
			so.expired AS expired,so.harga AS harga,
			DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal,
			p.dana,p.tahun_anggaran,p.no_faktur,roa.id_obat, l.no_dok as nomor_sbbk
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat so ON (so.id_stok=dd.id_stok)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_lplpo l ON (l.id=dd.id_lplpo)
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = dd.kode_obat_req
			JOIN ref_obat_all roa ON roa.id_obat = dd.kode_obat_res
			WHERE dd.id_lplpo=$id_lplpo AND dl.id_lplpo = $id_lplpo AND dl.permintaan > 0
			ORDER BY so.nama_obj
			");

		return $query->result();
	}

	function show_data_inn_cito($id_distribusi){
		$query=$this->db->query("
			SELECT hg.id_hlp AS kode_obat,hg.nama_objek AS nama_obj,dd.pemberian AS pemberian,so.no_batch AS no_batch,
			so.expired AS expired,so.harga AS harga,
			DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal,
			p.dana,p.tahun_anggaran,p.no_faktur,roa.id_obat, d.no_dok as nomor_sbbk
			FROM tb_distribusi d
			JOIN tb_detail_distribusi dd ON (dd.id_distribusi=d.id_distribusi)
			JOIN tb_stok_obat so ON (so.id_stok=dd.id_stok)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			JOIN ref_obat_all roa ON roa.id_obat = dd.kode_obat_res
			WHERE d.id_distribusi = $id_distribusi
			ORDER BY so.nama_obj
			");

		return $query->result();
	}

	function getDataKab($id_lplpo){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			SELECT DISTINCT kab.CKabDescr AS nama_kab,pusk.NAMA_PUSKES AS nama_pusk,
			DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal
			FROM tb_lplpo l
			JOIN ref_puskesmas pusk ON (pusk.KD_PUSK=l.kode_pusk)
			JOIN ref_kabupaten kab ON (kab.CKabID=pusk.KODE_KAB)
			JOIN tb_detail_distribusi dd ON(dd.id_lplpo=l.id)
			WHERE l.id=$id_lplpo
			");
		return $datakab->row_array();
	}

	function getDataInstitusi(){
		$query=$this->db->query("
			select * from tb_institusi
			");
		return $query->row_array();
	}

	function getPropinsi(){
		$query = $this->db->query("
			select *
			from ref_propinsi
			");
		return $query->result_array();
	}
}
?>

<?php
Class Detail_Faktur_Model extends CI_Model {
	
	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$sql = $this->db->query("
			INSERT INTO `tb_detail_faktur` (
					`kode_obat`,
					`no_faktur`,
					`nama_obj`,
					`no_batch`,
					`jumlah`,
					`expired`,
					`jumlah_kec`,
					`harga`,
					`create_time`,
					`create_by`,
					`kategori`,
					`id_faktur`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['kode_obat'],
				$data['no_faktur'],
				$data['nama_obat'],
				$data['no_batch'],
				$data['jumlah'],
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$time,
				$id_user,
				$data['kategori'],
				$data['id_faktur']
			)
		);
			
		$this->insertStok($data);
		$this->updateMaster($data);
		if($data['jml_rusak']>0){
			$this->insertRusak($data);	
		}

		return $sql;
		
		//$this->updateFornas($data);
		//$this->updateAtc($data);
	}

	function insertRusak($data){
		//ambil id stok dulu $id_stok
		$query = $this->db->query("select id_stok from tb_stok_obat order by id_stok desc limit 1")->row_array();
		//$last_id = $query['id_stok'];
		$sql = $this->db->query("
			INSERT INTO `tb_detail_pemusnahan` (
					`kode_obat`,
					`nomor_faktur`,
					`no_batch`,
					`jumlah`,
					`jenis_rusak`,
					`id_stok`
			) VALUES (
				?,?,?,?,1,?
			)",
			array(
				$data['kode_obat'],
				$data['no_faktur'],
				$data['no_batch'],
				$data['jml_rusak'],
				$query['id_stok']
			)
		);
	}

	function insertStok($data){
		$sql = $this->db->query("
			INSERT INTO `tb_stok_obat` (
					`kode_obat`,
					`no_faktur`,
					`no_batch`,
					`expired`,
					`stok`,
					`harga`,
					`nama_obj`,
					`flag`,
					
					`kategori`,
					`kode_generik`,
					`id_faktur`
			) VALUES (
				?,?,?,?,?,?,?,'1',?,?,?
			)",
			array(
				$data['kode_obat'],
				$data['no_faktur'],
				$data['no_batch'],
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$data['nama_obat'],
				
				$data['kategori'],
				$data['kode_generik'],
				$data['id_faktur']
			)
		);
	}

	function updateMaster($data){
		$time=date('Y-m-d H:i:s');
		$query=$this->db->query("
			update ref_obat_all
			set kemasan_unit=?,
			satuan_jual_unit=?,
			id_generik=?,
			status_update=1,
			update_time = ?
			where id_obat=?
			",array(
				$data['kemasan'],
				$data['sat_jual'],
				$data['kode_generik'], 
				$time,
				$data['kode_obat']
				)
			);
	}

	function updateKelompok($data){
		if($data['id_fornas']!=''){
			$sql1 = $this->db->query("
				INSERT INTO `map_obat_fornas` (
						`id_obat`,
						`id_fornas`,
						`status_update`
				) VALUES (
					?,?,1
				)",
				array(
					$data['kode_obat'],
					$data['id_fornas']
				)
			);	
		}
		// if($data['id_ukp4']!=''){
		// 	$sql2 = $this->db->query("
		// 		INSERT INTO `map_obat_ukp4` (
		// 				`id_obat`,
		// 				`id_ukp4`,
		// 				`status_update`
		// 		) VALUES (
		// 			?,?,1
		// 		)",
		// 		array(
		// 			$data['kode_obat'],
		// 			$data['id_ukp4']
		// 		)
		// 	);	
		// }
		if($data['kode_atc']!=''){
			$sql3 = $this->db->query("
				INSERT INTO `map_obat_atc` (
						`id_obat`,
						`atc`,
						`status_update`
				) VALUES (
					?,?,1
				)",
				array(
					$data['kode_obat'],
					$data['kode_atc']
				)
			);	
		}
		
		if($data['kode_inn']!=''){
			$id_indikator = $data['kode_obatindikator'];
			$id_program = $data['kode_obatprogram'];
			$konten = array('id_obatindikator'=>$id_indikator,'id_obatprogram'=>$id_program);
			$this->db->where('id',$data['kode_inn']);
			$this->db->update('ref_obatgenerik',$konten);
		}
	}

	function countAllData($nomorfaktur){
		//$this->db->where('no_faktur',$nomorfaktur);
		//return $this->db->count_all("tb_detail_faktur");

		//$query=$this->db->query("select count(no_faktur)
		//	from tb_detail_faktur
		//	where no_faktur='$nomorfaktur'");
		//return $query->row_array();

		$this->db->where('id_faktur',$nomorfaktur);
		$this->db->from('tb_detail_faktur');

		return $this->db->count_all_results();
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_detail_faktur
			WHERE 
				id = '$kode'"
			);
		$this->db->query(
			"DELETE FROM
				tb_stok_obat
			WHERE 
				id_stok = '$kode'"
			);
	}

	function getData($limit,$start,$nomorfaktur){
		/*$this->db->limit($limit, $start);
		$this->db->where('no_faktur',$nomorfaktur);
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("tb_detail_faktur");*/

		$query=$this->db->query("
				SELECT df.id as id,df.kode_obat AS kode_obat, df.nama_obj AS nama_obat, oa.deskripsi AS descr,
				df.jumlah_kec AS jumlah_kec,df.no_batch AS no_batch, df.expired AS expired, df.harga AS harga,
				null as kekuatan
				FROM tb_detail_faktur df
				JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat) 
				WHERE df.id_faktur = '$nomorfaktur'
				order by df.nama_obj
				LIMIT $start,$limit
				");
		//$this->db->limit($limit, $start);
		return $query->result();
	}

	function getSatuan(){
		$query=$this->db->query("
			select satuan_obat as satuan_obat
			from  ref_obat_satuan
			
			");
		return $query->result_array();
	}

	function getDataDetailFaktur($no_faktur){
		$query=$this->db->query("
			select id,no_faktur,tanggal,nama_dok,DATE_FORMAT(periode,'%M %Y') as periode,
			pbf,dana,no_kontrak,tahun_anggaran 
			from tb_penerimaan 
			where id='$no_faktur'");
		return $query->row_array();	
	}

	function GetListObat1($q) {
        $sql = $this->db->query("
            SELECT `nama_obat`, `kode_obat`
            FROM `ref_obat_all`
            WHERE `nama_obat` LIKE ?
        ",
            array(
                "%" . $q . "%"
            )
        );
        return $sql->result_array();
    }

    function GetListObat($key){
    	
	    $query = $this->db->query("
	    	SELECT oa.id_obat AS id_obat,oa.object_name AS nama_obat,oa.org_pembuat AS org_pembuat,			
			oa.kemasan_unit AS kemasan,oa.satuan_jual_unit AS sat_jual,
			oa.kategori AS kategori,oa.detil_kemasan AS detil_kemasan
			FROM ref_obat_all oa			
			WHERE oa.object_name LIKE '$key%'
			ORDER BY oa.id_obat
	    	");
	    //print_r($query->result_array());
	    //die;
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		'kemasan'	=>$key->kemasan,
	    		'detil_kemasan'	=>$key->detil_kemasan,
	    		'kategori'	=>$key->kategori,
	    		'sat_jual'	=>$key->sat_jual,
	    		'value'	=>$key->nama_obat,
	    		//'idf'	=>$key->id_fornas,
	    		'pbf'	=>$key->org_pembuat,
	    	);
	    }
	    return $r;
	}

	function GetListAtc($key){
    	
	    $query = $this->db->query("
	    	select * from ref_who_atc
	    	where nama_atc like '%$key%' or nama_atc_eng like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_atc'	=>$key->kode_atc,
	    		'value'	=>$key->nama_atc
	    	);
	    }
	    return $r;
	}

	function GetListFornas($key){
    	
	    $query = $this->db->query("
	    	SELECT rf.id_fornas as id_fornas,rf.nama_obat as nama_obat,
	    	mfp.nama_obat AS parent
			FROM ref_fornas rf
			LEFT JOIN map_fornas_parent mfp ON(mfp.id_fornas= rf.parent)
				    	WHERE rf.nama_obat LIKE '%$key%'
				    	AND rf.selected = 'y'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_fornas'	=>$key->id_fornas,
	    		'value'	=>$key->nama_obat,
	    		'category'	=>$key->parent
	    	);
	    }
	    return $r;
	}

	function GetListIndikator($key){
    	$this->db->like('nama_indikator',$key,'both');
	    $query = $this->db->get('ref_obatindikator');
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_indikator'	=>$key->id_indikator,
	    		'value'	=>$key->nama_indikator
	    	);
	    }
	    return $r;
	}

	function GetListProgram($key){
    	$this->db->like('nama_program',$key,'both');
	    $query = $this->db->get('ref_obatprogram');
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_program'=>$key->id,
	    		'value'	=>$key->nama_program
	    	);
	    }
	    return $r;
	}

	function GetListUkp4($key){
    	
	    $query = $this->db->query("
	    	select * from ref_ukp4
	    	where nama_ukp4 like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_ukp4'	=>$key->id_ukp4,
	    		'value'	=>$key->nama_ukp4
	    	);
	    }
	    return $r;
	}

	function GetListGenerik($key){
    	
	    $query = $this->db->query("
	    	select * from hlp_generik
	    	where nama_objek like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_generik'	=>$key->id_hlp,
	    		'value'	=>$key->nama_objek
	    	);
	    }
	    return $r;
	}

	function GetListProdusen($key){
    	
	    $query = $this->db->query("
	    	select * from ref_pabrik
	    	where nama_prod_obat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_produsen'	=>$key->id,
	    		'value'	=>$key->nama_prod_obat
	    	);
	    }
	    return $r;
	}

	function GetListObatBC($key){
    	
	    $query = $this->db->query("
	    	SELECT oa.id_obat AS id_obat,oa.object_name AS nama_obat,oa.org_pembuat AS org_pembuat,
			mof.id_fornas AS id_fornas,rf.nama_obat AS nm_fornas,mou.id_ukp4 AS id_ukp4,
			ru.nama_ukp4 AS nm_ukp4,moa.atc AS kode_atc,rw.nama_atc AS nm_atc_ger,
			rw.nama_atc_eng AS nm_atc_eng,oa.kemasan_unit as kemasan,oa.satuan_jual_unit as sat_jual,
			oa.kategori as kategori,oa.detil_kemasan as detil_kemasan
			FROM ref_obat_all oa
			LEFT JOIN map_obat_fornas mof ON(mof.id_obat=oa.id_obat)
			LEFT JOIN map_obat_ukp4 mou ON(mou.id_obat=oa.id_obat)
			LEFT JOIN map_obat_atc moa ON(moa.id_obat=oa.id_obat)
			LEFT JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			LEFT JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			LEFT JOIN ref_who_atc rw ON(rw.kode_atc=moa.atc)
			WHERE oa.barcode LIKE '%$key%'
			ORDER BY oa.id_obat
	    	");
	    //print_r($query->result_array());
	    //die;
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		'kemasan'	=>$key->kemasan,
	    		'detil_kemasan'	=>$key->detil_kemasan,
	    		'kategori'	=>$key->kategori,
	    		'sat_jual'	=>$key->sat_jual,
	    		'value'	=>$key->nama_obat,
	    		'idf'	=>$key->id_fornas,
	    		'pbf'	=>$key->org_pembuat,
	    		'idukp4'	=>$key->id_ukp4,
	    		'idatc'	=>$key->kode_atc,
	    		'nukp4'=>$key->nm_ukp4,
	    		'natc_g'=>$key->nm_atc_ger,
				'natc_e'=>$key->nm_atc_eng,
				'nf'=>$key->nm_fornas
	    	);
	    }
	    return $r;
	}

	function GetInfoObat($no_batch){
		$query=$this->db->query("
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj,so.kategori AS kategori,
			df.jumlah AS jum_besar,oa.kemasan_unit AS kemasan,df.jumlah_kec AS jum_kec,
			oa.satuan_jual_unit AS sat_unit,df.no_batch AS no_batch,df.expired AS ed,
			df.harga,rf.nama_obat AS nama_fornas,rwa.nama_atc AS nama_atc,ru.nama_ukp4 AS nama_ukp4,
			dpm.jumlah AS jml_rusak,df.no_faktur AS no_faktur,df.id AS id,hg.id_hlp, hg.nama_objek,
			oi.id_indikator,oi.nama_indikator,op.id AS id_program,op.nama_program,rf.id_fornas,rwa.kode_atc AS koatc,
			so.expired as ed_stok, so.no_batch as batch_stok, so.id_stok
			FROM tb_detail_faktur df JOIN tb_stok_obat so ON(df.id=so.id_stok)
			LEFT JOIN ref_obat_all oa ON(df.kode_obat=oa.id_obat)
			LEFT JOIN map_obat_fornas mof ON(mof.id_obat=so.kode_obat)
			LEFT JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			LEFT JOIN map_obat_atc moa ON(moa.id_obat=so.kode_obat)
			LEFT JOIN ref_who_atc rwa ON(rwa.kode_atc=moa.atc)
			LEFT JOIN map_obat_ukp4 mou ON(mou.id_obat=so.kode_obat)
			LEFT JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			LEFT JOIN tb_detail_pemusnahan dpm ON(dpm.id_stok=df.id)
			LEFT JOIN hlp_generik hg ON hg.id_hlp = so.kode_generik
			LEFT JOIN ref_obatgenerik rog ON rog.id = so.kode_generik
			LEFT JOIN ref_obatindikator oi ON oi.id_indikator=rog.id_obatindikator
			LEFT JOIN ref_obatprogram op ON op.id = rog.id_obatprogram
			WHERE df.id='$no_batch'
			");
			//WHERE df.no_batch='$no_batch'
		return $query->row_array();
	}

	function update_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');
		//`no_batch`=?
		$sql = $this->db->query("
			UPDATE `tb_detail_faktur` 
			SET													
					`expired`=?,
					`jumlah_kec`=?,
					`harga`=?,
					`create_time`=?,
					`create_by`=?,
					`kategori`=?,
					`no_batch`=?
			WHERE
				`id`=?
			",
			array(
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$time,
				$id_user,
				$data['kategori'],
				$data['batch'],
				$data['no_batch']
			)
		);
		//return $sql;	
		$this->updateMaster($data);
		$this->updateStok($data);
		if($data['jml_rusak']>0){
			$this->updateStokRusak($data);	
		}
		
	}

	function updateStok($data){
		$sql = $this->db->query("
			UPDATE `tb_stok_obat` 
			SET
					`expired`=?,
					`stok`=?,
					`harga`=?,
					`kategori`=?,
					`no_batch`=?,
					`kode_generik`=?
			WHERE
				`id_stok`=?
			",
			array(
				
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$data['kategori'],
				$data['batch'],
				$data['kode_generik'],
				$data['no_batch']//udah diganti pake id_stok
			)
		);
	}

	function updateStokRusak($data){
		$cek=$this->db->query("
			select * from tb_detail_pemusnahan where no_batch=?",
			array($data['no_batch'])
			);
		if($cek->num_rows()>0){
			$sql = $this->db->query("
				UPDATE `tb_detail_pemusnahan` 
				SET
					`jumlah`=?						
				WHERE
					`no_batch`=?
				",
				array(
					$data['jml_rusak'],					
					$data['no_batch']
				)
			);
		}else{
			$this->insertRusak($data);
		}
		
	}

	function searchData($key){
		$id_faktur = $key['id_faktur'];
		$word=$key['word'];
		$query=$this->db->query("
			SELECT df.id as id,oa.id_obat AS kode_obat, oa.nama_obat AS nama_obat, oa.deskripsi AS descr,
			df.jumlah_kec AS jumlah_kec,df.no_batch AS no_batch, df.expired AS expired, df.harga AS harga,
			oa.kekuatan as kekuatan
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat) 
			WHERE df.nama_obj like '%$word%' and df.id_faktur = '$id_faktur'
			");
		return $query->result();
	}
}
?>
<?php
Class Isian_lplpo_kel_model extends CI_Model {

	function input_data_m($data){

		$data['stok_akhir'] = $data['terima']+$data['stok_awal']-$data['pakai'];
		$sql = $this->db->insert('tb_detail_lplpo_kel', $data);

		return $sql;
	}

	function countAllData($id_lplpo){
		$this->db->where('id_lplpo',$id_lplpo);
		$this->db->from('tb_detail_lplpo');

		return $this->db->count_all_results();
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_detail_lplpo_kel
			WHERE
				id IN ($kode)"
			);
	}

	function getData($id_lplpo, $key = ''){
		$where = '';
		if ($key != '') {
			$where = 'AND nama_obj LIKE "%'.$key.'%"';
		}
		$query=$this->db->query("
			SELECT dl.id_lplpo AS id_lplpo,dl.stok_awal AS stok_awal, dl.rusak_ed AS rusak_ed,
			dl.id AS id,hg.nama_objek AS nama_obat,dl.sediaan AS sediaan,
			dl.terima AS terima, dl.sedia AS sedia, dl.pakai AS pakai,
			dl.stok_akhir AS stok_akhir, dl.stok_opt AS stok_opt,
			dl.permintaan AS permintaan, dl.dana AS dana_mahasiswa, dl.ket AS ket,
			 dl.pemberian AS pemberian,dl.kode_generik as kode_generik
			FROM tb_detail_lplpo_kel dl
			join hlp_generik hg on(hg.id_hlp=dl.kode_generik)
			WHERE dl.id_lplpo = $id_lplpo $where
			ORDER BY dl.nama_obj
			");

		return $query->result();

	}

	function getSatuan(){
		$query=$this->db->query("
			select satuan_obat as satuan_obat
			from  ref_obat_satuan
			limit 10");
		return $query->result_array();
	}

	function getDataDetaillplpo($nolplpo){
		$query=$this->db->query("
			SELECT l.id as id,kec.KECDESC AS nama_kec, pusk.NAMA_PUSKES AS nama_pusk,DATE_FORMAT(l.periode,'%Y/%m/01') AS periode,l.kode_pusk
			FROM tb_lplpo l
			JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
			JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk)
			where l.id='$nolplpo'
			");
		return $query->row_array();
	}

	function GetListObat($key){
	    $query = $this->db->query("
	    	SELECT DISTINCT hg.nama_objek AS nama_obat,so.kode_generik as id_obat,
	    	hg.sediaan as deskripsi
			FROM tb_stok_obat so
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN ref_obat_all roa ON(roa.id_obat=so.kode_obat)
	    	where hg.nama_objek like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		//'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obat,
	    		'desc'	=>$key->deskripsi
	    		//'pwr'	=>$key->kekuatan,
	    		//'produsen'=>$key->produsen
	    	);
	    }
	    return $r;
	}

	function getRata2Pakai($pd,$id_dl,$id_lplpo){
		$temp = $this->db->where('id',$id_dl)->get('tb_detail_lplpo_kel')->row_array();
		$kode_generik = $temp['kode_generik'];
		$date=$this->db->where('id',$id_lplpo)->get('tb_lplpo')->row_array();
		$kode_puskesmas = $date['kode_pusk'];
		$periode = $date['periode'];
		$temp2 = explode("-", $periode);
		$new_m = $temp2[1]-$pd;
		if(strlen($new_m)<2){
			$new_m = '0'.$new_m;
		}
		$tanggal_mula = $temp2[0].'-'.$new_m;
		//harus realtime | waktu kekinian belum dinamis (belum menyesuaikan periode lplpo)
		$sql = "
			SELECT l.periode,dl.kode_generik,SUM(dl.pakai)/$pd as rata2pakai
			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON l.id=dl.id_lplpo
			WHERE dl.kode_generik = '$kode_generik' AND l.kode_pusk = '$kode_puskesmas' AND l.periode > '$tanggal_mula' AND l.periode <= '$periode'
			GROUP BY dl.kode_generik
			";
		$query = $this->db->query($sql);

		return $query;
	}
}
?>

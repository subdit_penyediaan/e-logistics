<?php
Class Stok_Opnam_Model extends CI_Model {
	
	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$sql = $this->db->query("
			INSERT INTO `tb_stok_opnam` (
					`no_stok_opnam`,
					`nama_user`,
					`periode_stok_opnam`,
					`cttn`,
					`tgl_trans`,
					`create_time`,
					`create_by`
			) VALUES (
				?,?,?,?,?,?,?
			)",
			array(
				$data['no_stok_opnam'],
				$data['nama_user'],
				$data['per_stok_opnam'].'-00',
				$data['cttn'],
				$data['tgl_trans'],
				$time,$id_user
				//$data['dana']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("tb_stok_opnam");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_stok_opnam
			WHERE 
				id IN ($kode)"
			);
		$this->db->query(
			"DELETE FROM
				tb_detail_stok_opnam
			WHERE 
				id_so IN ($kode)"
			);
	}

	function getData($limit,$start){
		// $this->db->limit($limit, $start);
		$query=$this->db->get("tb_stok_opnam");
		
		return $query->result();
	}

	function input_detail_tahunan($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');
		$nama_tabel = $data['nama_tabel'];
		$data['id_faktur'] = 'SAT'.substr($nama_tabel, 14);
		$sql = $this->db->query("
			INSERT INTO `$nama_tabel` (
					`kode_obat`,
					
					`no_batch`,
					`expired`,
					`stok`,
					`harga`,
					`nama_obj`,
					`flag`,					
					`kategori`,
					`kode_generik`,
					`id_faktur`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['kode_obat'],
				//$data['no_faktur'],
				$data['no_batch'],
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$data['nama_obat'],1,
				$data['kategori'],
				$data['kode_generik'],
				$data['id_faktur']//SAT+tahun_stok
			)
		);
		//return $sql;	
		$this->insertStok($data);
		$this->updateMaster($data);
		if($data['jml_rusak']>0){
			$this->insertRusak($data);	
		}
		
		//$this->updateFornas($data);
		//$this->updateAtc($data);
	}

	function insertRusak($data){
		//ambil id stok dulu $id_stok
		$query = $this->db->query("select id_stok from tb_stok_obat order by id_stok desc limit 1")->row_array();
		//$last_id = $query['id_stok'];
		$sql = $this->db->query("
			INSERT INTO `tb_detail_pemusnahan` (
					`kode_obat`,
					`nomor_faktur`,
					`no_batch`,
					`jumlah`,
					`jenis_rusak`,
					`id_stok`
			) VALUES (
				?,?,?,?,1,?
			)",
			array(
				$data['kode_obat'],
				$data['no_faktur'],
				$data['no_batch'],
				$data['jml_rusak'],
				$query['id_stok']
			)
		);
	}

	function insertStok($data){
		$sql = $this->db->query("
			INSERT INTO `tb_stok_obat` (
					`kode_obat`,
					
					`no_batch`,
					`expired`,
					`stok`,
					`harga`,
					`nama_obj`,
					`flag`,					
					`kategori`,
					`kode_generik`,
					`id_faktur`
			) VALUES (
				?,?,?,?,?,?,'1',?,?,?
			)",
			array(
				$data['kode_obat'],
				//$data['no_faktur'],
				$data['no_batch'],
				$data['tanggal'],
				$data['jumlah_kec'],
				$data['harga_beli'],
				$data['nama_obat'],				
				$data['kategori'],
				$data['kode_generik'],
				$data['id_faktur']//SAT+tahun_stok
			)
		);
	}

	function getDetail($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('tb_stok_opnam');

		return $query;
	}

	function updateData($id, $data)
	{
		$this->db->where('id', $id);
		$query = $this->db->update('tb_stok_opnam', $data);

		return $query;
	}
}
?>
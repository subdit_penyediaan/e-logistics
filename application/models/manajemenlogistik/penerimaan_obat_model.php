<?php
Class Penerimaan_Obat_Model extends CI_Model {
	
	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$sql = $this->db->query("
			INSERT INTO `tb_penerimaan` (
					`no_faktur`,
					`periode`,
					`tanggal`,
					`pbf`,
					`nama_dok`,
					`no_kontrak`,
					`dana`,
					`create_time`,
					`create_by`,
					`tahun_anggaran`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['no_faktur'],
				$data['periode'].'-00',
				$data['tanggal'],
				$data['pbf'],
				$data['nama_dok'],
				$data['no_kontrak'],
				$data['dana'],
				$time,
				$id_user,
				$data['tahun']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("tb_penerimaan");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM tb_penerimaan WHERE 
				id = '$kode'"
			);
			//no_faktur = '$kode'"
		$this->db->query(
			"DELETE FROM
				tb_detail_faktur
			WHERE 
				id_faktur = '$kode'"
			);
		$this->db->query(
			"DELETE FROM
				tb_stok_obat
			WHERE 
				id_faktur = '$kode'"
			);

	}

	function getData($limit="",$start=0){		
		$query=$this->db->query("
			SELECT id,no_faktur,tanggal,nama_dok,DATE_FORMAT(periode,'%M %Y') as periode,
			pbf,dana,no_kontrak,tahun_anggaran
			FROM tb_penerimaan
			order by id desc
			
			"); //limit $start,$limit
		return $query->result();
	}

	function GetListPbf($key){
	    $query = $this->db->query("
	    	select * from ref_pbf 
	    	where SuplierName like '%$key%' or alamat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function getDataToChange($key){
		$query = $this->db->query("select * from tb_penerimaan where id ='$key'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'no_faktur'	=>$key->no_faktur,
	    		'tanggal'	=>$key->tanggal,
	    		'nama_dok'	=>$key->nama_dok,
	    		'periode'	=>$key->periode,
	    		'pbf'	=>$key->pbf,
	    		'dana'	=>$key->dana,
	    		'no_kontrak'	=>$key->no_kontrak,
	    		'tahun_anggaran'	=>$key->tahun_anggaran
	    	);
	    }
	    return $r;
	}

	function update_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$sql = $this->db->query("
			UPDATE `tb_penerimaan` 
			SET
					`periode`=?,
					`tanggal`=?,
					`pbf`=?,
					`nama_dok`=?,
					`no_kontrak`=?,
					`dana`=?,
					`create_time`=?,
					`create_by`=?,
					`no_faktur`=?,
					`tahun_anggaran`=?
			WHERE 
				`id`=?
			",
			array(
				$data['periode'].'-00',
				$data['tanggal'],
				$data['pbf'],
				$data['nama_dok'],
				$data['no_kontrak'],
				$data['dana'],
				$time,
				$id_user,
				$data['no_faktur'],
				$data['tahun_anggaran'],
				$data['id_faktur']
			)
		);
		$konten = array('no_faktur'=>$data['no_faktur']);
		$this->db->where('id_faktur',$data['id_faktur'])->update('tb_detail_faktur',$konten);
		$this->db->where('id_faktur',$data['id_faktur'])->update('tb_stok_obat',$konten);
	}
}
?>
<?php
Class Retur_model extends CI_Model {
	
	function input_data_m($data){
		$data['id_user']=$this->session->userdata('id');
		$this->db->insert('tb_retur',$data);
	
	}

	function input_data_pkm($data){
		$data['id_user']=$this->session->userdata('id');
		$this->db->insert('tb_retur_pkm',$data);
	
	}

	function countAllData(){
		return $this->db->count_all("tb_retur");
	}

	function countAllDataPkm(){
		return $this->db->count_all("tb_retur_pkm");
	}

	function deleteDataX($kode){
		$this->db->query(
			"DELETE FROM tb_retur WHERE 
				no_faktur = '$kode'"
			);

		$this->db->query(
			"DELETE FROM
				tb_detail_faktur
			WHERE 
				no_faktur = '$kode'"
			);
		$this->db->query(
			"DELETE FROM
				tb_stok_obat
			WHERE 
				no_faktur = '$kode'"
			);

	}

	function getData($limit,$start=0){		
		$query=$this->db->query("
			SELECT tr.*,p.pbf,so.nama_obj
			FROM tb_retur tr
			JOIN tb_stok_obat so ON(so.id_stok=tr.id_stok)			
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			order by tr.id desc
			limit $start,$limit
			");
		return $query->result();
	}

	function getDataPkm($limit,$start=0){		
		$query=$this->db->query("
			SELECT tr.*,p.pbf,so.nama_obj
			FROM tb_retur_pkm tr
			JOIN tb_stok_obat so ON(so.id_stok=tr.id_stok)			
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			order by tr.id desc
			limit $start,$limit
			");
		return $query->result();
	}

	function getDataEksport(){		
		$query=$this->db->query("
			SELECT tr.*,p.pbf,so.nama_obj
			FROM tb_retur tr
			JOIN tb_stok_obat so ON(so.id_stok=tr.id_stok)			
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			order by tr.id desc			
			");
		return $query->result();
	}

	function getDataEksportPkm(){		
		$query=$this->db->query("
			SELECT tr.*,p.pbf,so.nama_obj
			FROM tb_retur_pkm tr
			JOIN tb_stok_obat so ON(so.id_stok=tr.id_stok)			
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
			order by tr.id desc			
			");
		return $query->result();
	}

	function getDataInstitusi(){
		$query=$this->db->query("
			SELECT * 
			FROM tb_institusi ins
			LEFT JOIN ref_kabupaten rk ON (rk.CKabID = ins.id_kab)
			LEFT JOIN ref_propinsi rp ON (rp.CPropID = ins.id_prop)
			");
		return $query->row_array();
	}

	function GetListPbf($key){
	    $query = $this->db->query("
	    	select * from ref_pbf 
	    	where SuplierName like '%$key%' or alamat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function getDataToChange($key){
		$query = $this->db->query("select * from tb_penerimaan where no_faktur ='$key'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'no_faktur'	=>$key->no_faktur,
	    		'tanggal'	=>$key->tanggal,
	    		'nama_dok'	=>$key->nama_dok,
	    		'periode'	=>$key->periode,
	    		'pbf'	=>$key->pbf,
	    		'dana'	=>$key->dana,
	    		'no_kontrak'	=>$key->no_kontrak
	    		
	    	);
	    }
	    return $r;
	}

	function GetListObat($key){
	    $query = $this->db->query("
	    	SELECT so.id_stok AS id_stok,so.kode_obat AS kode_obat,
			so.no_batch AS no_batch, so.expired AS ed, so.stok AS stok,
			so.nama_obj AS nama_obj,p.no_faktur,p.tanggal,p.pbf
			FROM tb_stok_obat so						
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			WHERE so.nama_obj LIKE '%$key%' AND so.flag='1'	
	    	");
		$r = array();
		//ambil id stoknya disini
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->kode_obat,	    		
	    		'value'	=>$key->nama_obj,
	    		'batch'	=>$key->no_batch,
	    		'pbf'	=>$key->pbf,
	    		'no_faktur'	=>$key->no_faktur,
	    		'id_stok'	=>$key->id_stok,
	    		'ed' =>$key->ed,
	    		'tanggal' =>$key->tanggal,
	    		'stok' =>$key->stok
	    	);
	    }
	    return $r;
	}

	function kurangi_stok($data){
		$this->db->query("
			UPDATE `tb_stok_obat` 
			SET
					`stok`=`stok`-?
			WHERE 
				`id_stok`=?
			",array($data['jumlah'],$data['id_stok'])
		);
	}

	function tambah_stok($data){
		$this->db->query("
			UPDATE `tb_stok_obat` 
			SET
					`stok`=`stok`+?
			WHERE 
				`id_stok`=?
			",array($data['jumlah'],$data['id_stok'])
		);
	}
}
?>
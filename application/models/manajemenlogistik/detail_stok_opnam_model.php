<?php
Class Detail_Stok_Opnam_Model extends CI_Model {
	
	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

	}

	function countAllData(){
		//$this->db->where('no_faktur',$nomorfaktur);
		//return $this->db->count_all("tb_stok_obat");

		//$query=$this->db->query("select count(no_faktur)
		//	from tb_detail_faktur
		//	where no_faktur='$nomorfaktur'");
		//return $query->row_array();

		$this->db->where('flag',1);
		$this->db->from('tb_stok_obat');

		return $this->db->count_all_results();
	}

	function getData($limit,$start){
		/*$this->db->limit($limit, $start);
		$this->db->where('no_faktur',$nomorfaktur);
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("tb_detail_faktur");*/
		//if ($start=""){
		//	$start=0;
		//}
		$this->db->limit($limit, $start);
		$query=$this->db->query("
				SELECT so.kode_obat AS kode_obat,oa.nama_obat AS nama_obat, oa.kekuatan AS kekuatan,
				oa.deskripsi AS sediaan,so.stok AS noted_stok,so.id_stok as id_stok_obat
				FROM tb_stok_obat so
				JOIN ref_obat_all oa ON(oa.id_obat = so.kode_obat)
				where so.flag = 1
			");
		
		//$this->db->limit(5,0);
		return $query->result();
	}

	function getSatuan(){
		$query=$this->db->query("
			select satuan_obat as satuan_obat
			from  ref_obat_satuan
			limit 10");
		return $query->result_array();
	}

	function getDataDetailStok($no_stok){
		$query=$this->db->query("
			select id,no_stok_opnam,nama_user,tgl_trans,DATE_FORMAT(periode_stok_opnam,'%M %Y') as periode, cttn
			from tb_stok_opnam 
			where id='$no_stok'");
		return $query->row_array();	
	}

	

	function updateStatus($key){
		$query=$this->db->query("
			update tb_stok_opnam
			set status='sudah diproses'
			where id=$key
			");
	}

	function getID(){

	}

	function getDataSaved($key){
		$query=$this->db->query("
			SELECT so.status AS 'status',dso.kode_obat AS kode_obat, oa.nama_obat AS nama_obat,
			oa.deskripsi AS sediaan, dso.noted_stok AS noted_stok, dso.fisik_stok AS fisik_stok,
			dso.selisih AS selisih, dso.ket AS ket, oa.kekuatan as kekuatan
			FROM tb_stok_opnam so
			JOIN tb_detail_stok_opnam dso ON(dso.id_so=so.id)
			JOIN ref_obat_all oa ON(oa.id_obat=dso.kode_obat)
			WHERE dso.id_so=$key
			");
		return $query->result();
	}

	function getDataByWhere($data_array)
	{
		$this->db->where($data_array);
		$this->db->join('tb_stok_obat AS b','a.id_so=b.id_stok');
		return $this->db->get('tb_detail_stok_opnam AS a');
	}
}
?>
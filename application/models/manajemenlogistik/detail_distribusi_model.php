<?php
Class Detail_distribusi_model extends CI_Model {
	
	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
        $id_user=$this->session->userdata('id');
		
		$sql = $this->db->query("
			INSERT INTO `tb_detail_distribusi` (					
					`id_distribusi`,
					`kode_obat_req`,
					`jum_obat_req`,
					`kode_obat_res`,
					`pemberian`,
					`no_batch`,
					`create_time`,
					`create_by`,
					`id_lplpo`,
					`id_stok`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['id_distribusi'],
				$data['kode_obat_req'],
				$data['jml_minta'],
				$data['kode_obat_res'],
				$data['jml_beri'],
				$data['no_batch'],
				$data['tanggal_pelayanan'],
				$id_user,0,$data['id_stok']
			)
		);

		//ngurangin stok
        $upt = $this->db->query("
            update tb_stok_obat
            set stok= (stok-?)
            where id_stok = ?
        ",
        array($data['jml_beri'],
				$data['id_stok'])
        );
        //update field pemberian tb_detail_lplpo
        /*$upt = $this->db->query("
            update tb_detail_lplpo
            set pemberian= ?
            where id_lplpo = ? and kode_obat = ?
        ",
        array($arr_jml[$i],
                $id_lplpo[$i],
                $arr_kode_obat_req[$i]
                )
        );*/
		$upt = $this->db->query("
            update tb_distribusi
            set status= 'sudah diproses'
            where id_distribusi = ?
        ",
        array($data['id_distribusi'])
        );
	}

	function countAllData($id_lplpo){
		$this->db->where('id_lplpo',$id_lplpo);
		$this->db->from('tb_detail_lplpo');

		return $this->db->count_all_results();
	}

	function countAllDataCito($id){
		$this->db->where('id_distribusi',$id);
		$this->db->from('tb_detail_distribusi');

		return $this->db->count_all_results();	
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_penerimaan
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start,$id){		
		$query=$this->db->query("
			SELECT so.nama_obj AS obat_pemberian, dd.id AS id,so.id_stok AS id_stok,
			dd.kode_obat_res AS kode_obat_res, dd.no_batch AS no_batch, dd.pemberian AS pemberian,
			dd.jum_obat_req AS jum_req,p.dana,p.tahun_anggaran,oa.satuan_jual_unit as deskripsi
			FROM tb_detail_distribusi dd			
			JOIN tb_stok_obat so ON(so.id_stok=dd.id_stok)		
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON(oa.id_obat=so.kode_obat)
			WHERE dd.id_distribusi=$id
			");
		$this->db->limit($limit, $start);
		return $query->result();

	}
	function getDataXX($limit,$start,$id){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("");
		//$this->db->order_by("");
		//$query=$this->db->get("tb_detail_faktur");
		/*$query=$this->db->query("
			SELECT so.nama_obj AS obat_pemberian, dd.id as id,
			dd.kode_obat_res AS kode_obat_res, dd.no_batch AS no_batch, dd.pemberian AS pemberian,
			oa.nama_obat AS nama_obat,oa.kekuatan AS kekuatan, oa.deskripsi AS sediaan, dd.jum_obat_req AS jum_req
			FROM tb_detail_distribusi dd
			JOIN ref_obat_all oa ON(oa.id_obat=dd.kode_obat_req)
			JOIN tb_stok_obat so ON(so.no_batch=dd.no_batch)
			WHERE dd.id_distribusi=$id
			");*/
		$query=$this->db->query("
			SELECT so.nama_obj AS obat_pemberian, dd.id AS id,
			dd.kode_obat_res AS kode_obat_res, dd.no_batch AS no_batch, dd.pemberian AS pemberian,
			hg.nama_objek AS nama_obat,hg.sediaan AS sediaan, dd.jum_obat_req AS jum_req
			FROM tb_detail_distribusi dd
			JOIN hlp_generik hg ON(hg.id_hlp=dd.kode_obat_req)
			JOIN tb_stok_obat so ON(so.no_batch=dd.no_batch)
			WHERE dd.id_distribusi=$id
			UNION
			
			SELECT so.nama_obj AS obat_pemberian, dd.id AS id,
			dd.kode_obat_res AS kode_obat_res, dd.no_batch AS no_batch, dd.pemberian AS pemberian,
			oa.object_name AS nama_obat,oa.deskripsi AS sediaan, dd.jum_obat_req AS jum_req
			FROM tb_detail_distribusi dd
			JOIN ref_obat_all oa ON(oa.id_obat=dd.kode_obat_req)
			JOIN tb_stok_obat so ON(so.no_batch=dd.no_batch)		
			WHERE dd.id_distribusi=$id
			");
		$this->db->limit($limit, $start);
		return $query->result();

	}

	function getSatuan(){
		$query=$this->db->query("
			select satuan_obat as satuan_obat
			from  ref_obat_satuan
			limit 10");
		return $query->result_array();
	}

	function getInfoDistribusi($id){
		$query=$this->db->query("
			SELECT id_distribusi,unit_eks,tanggal_trans,keperluan,
			DATE_FORMAT(periode,'%M %Y') AS periode
			FROM tb_distribusi
			WHERE id_distribusi=$id
			");
		return $query->row_array();	
	}

	function GetListObat($key){
		$query = $this->db->query("
	    	SELECT so.kode_obat,hg.id_hlp AS id_obat,hg.nama_objek AS nama_obat
			FROM tb_stok_obat so
			JOIN hlp_generik hg ON (hg.id_hlp=so.kode_generik)
	    	WHERE hg.nama_objek like '%$key%'
	    	");
	    /*$query = $this->db->query("
	    	SELECT oa.org_pembuat as produsen,so.kode_obat as id_obat,oa.nama_obat AS nama_obat, oa.deskripsi AS deskripsi, oa.kekuatan AS kekuatan
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON(oa.id_obat=so.kode_obat)
	    	where oa.nama_obat like '%$key%' and so.flag=1
	    	");*/
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obat
	    	);
	    }
	    return $r;
	}

	function GetListObatBeri($key){
		//where so.nama_obj like '%$key%' or hg.nama_objek like '%$key%'
		// SELECT so.kode_obat as id_obat,so.expired as ed,
	 //    	so.nama_obj as nama_obj, so.stok as stok, so.no_batch as no_batch,
	 //    	so.id_stok as id_stok, p.dana AS sumber_dana
		// 	FROM tb_stok_obat so			
		// 	JOIN tb_penerimaan p ON(p.no_faktur=so.no_faktur)
		// 	join hlp_generik hg on(hg.id_hlp=so.kode_generik)
	 //    	where hg.nama_objek like '%$key%'
	 //    		and so.flag=1 AND so.stok > 0
	 //    	order by so.expired
	    $query = $this->db->query("
	    	SELECT so.kode_obat AS id_obat,so.expired AS ed,
	    	so.nama_obj AS nama_obj, so.stok AS stok, so.no_batch AS no_batch,
	    	so.id_stok AS id_stok, p.dana AS sumber_dana,p.tahun_anggaran,roa.org_pembuat as produsen
			FROM tb_stok_obat so			
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			join ref_obat_all roa on roa.id_obat = so.kode_obat
	    	WHERE hg.nama_objek LIKE '%$key%'
	    		AND so.flag=1 AND so.stok > 0 AND 
	    		((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) < 0
			UNION
			SELECT so.kode_obat AS id_obat,so.expired AS ed,
				    	so.nama_obj AS nama_obj, so.stok AS stok, so.no_batch AS no_batch,
				    	so.id_stok AS id_stok, p.dana AS sumber_dana,p.tahun_anggaran,roa.org_pembuat as produsen
						FROM tb_stok_obat so			
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						join ref_obat_all roa on roa.id_obat = so.kode_obat
				    	WHERE so.nama_obj LIKE '%$key%'
				    		AND so.flag=1 AND so.stok > 0 AND 
				    		((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) < 0
				    	ORDER BY ed
	    	");
	    //where hg.nama_objek like '%$key%' or so.nama_obj like '%$key%'
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		//'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obj,
	    		'nobatch'	=>$key->no_batch,
	    		'stok'	=>$key->stok,
	    		'produsen'=>$key->produsen,
	    		'ed'=>$key->ed,
	    		'id_stok' =>$key->id_stok,
	    		'tahun_anggaran' =>$key->tahun_anggaran,
	    		'sumber_dana' =>$key->sumber_dana
	    	);
	    }
	    return $r;
	}
	//tb_detail_lplpo update
	//tb_detail_distribusi insert
	function show_data($id){
		$query=$this->db->query("
			SELECT dd.kode_obat_res AS kode_obat,so.nama_obj AS nama_obj,dd.pemberian AS pemberian,so.no_batch as no_batch,
			so.expired AS expired,so.harga AS harga,DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat so ON (so.id_stok=dd.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			WHERE d.id_distribusi=$id
			ORDER BY so.nama_obj
			");
		//$this->db->limit($limit, $start);
		//$query=$this->db->get("tb_penerimaan");
		return $query->result();		
	}

	function show_data_inn($id){
		//SELECT hg.id_hlp AS kode_obat,hg.nama_objek AS nama_obj,dd.pemberian AS pemberian,so.no_batch AS no_batch,
		//JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
		$query=$this->db->query("
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj,dd.pemberian AS pemberian,ro.satuan_jual_unit as deskripsi,so.no_batch AS no_batch,
			so.expired AS expired,so.harga AS harga,
			DATE_FORMAT(dd.create_time,'%d %M %Y') AS tanggal,
			p.dana,p.tahun_anggaran
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat so ON (so.id_stok=dd.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)			
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all ro ON(ro.id_obat = so.kode_obat)
			WHERE d.id_distribusi=$id
			ORDER BY so.nama_obj
			");
		//$this->db->limit($limit, $start);
		//$query=$this->db->get("tb_penerimaan");
		return $query->result();		
	}

	function getDataUp($id){
		$query=$this->db->query("
			select * from tb_distribusi where id_distribusi=$id
			");	
		return $query->row_array();
	}

	public function getLPLPO($kodePuskesmas, $periode)
	{
		$this->db->where('kode_pusk', $kodePuskesmas);
		$this->db->where('periode', $periode);
		$result = $this->db->get('tb_lplpo');
		return $result;
	}

	public function getObatGenerikFromStock($kodeObat)
	{
		$this->db->where('kode_obat', $kodeObat);
		$result = $this->db->get('tb_stok_obat');
		return $result;
	}

	public function updateLPLPO($data)
	{
		$query = $this->db->query("
			update tb_detail_lplpo_kel
                    set pemberian= pemberian + ?
                    where id_lplpo = ? and kode_generik = ?
            ", array(
            	$data['pemberian'],
            	$data['id_lplpo'],
            	$data['kode_generik']
            	)
        );
		// echo "<pre>".$this->db->last_query()."</pre>";
        return $query;
	}
}
?>
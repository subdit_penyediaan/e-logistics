<?php
Class History_Lplpo_Model extends CI_Model {

	function getDataKecamatan(){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			select id_kab
			from tb_institusi
			");
		$kab = $datakab->row_array();
		//print_r($kab);
		$kabupaten=$kab['id_kab'];
		$query = $this->db->query("
			select *
			from ref_kecamatan
			where KDKAB='$kabupaten'");
		return $query->result_array();
	}

	function GetComboPuskesmas($key){
		$sql = $this->db->query("
            SELECT KD_PUSK, NAMA_PUSKES FROM ref_puskesmas WHERE KODE_KEC='$key' ORDER BY NAMA_PUSKES"
        );
        return $sql->result_array();
	}

	function input_data_m($data){
		$time=date('Y-m-d H:i:s');
		$id_user=$this->session->userdata('id');

		$cek = $this->db->query("
			select * from tb_lplpo
			where `kode_pusk`=? and `periode`=?
			",
			array($data['kode_pusk'],
				$data['per_lplpo'].'-00')
			);
		if ($cek->num_rows > 0){
			return false;
		}else{

			$sql = $this->db->query("
				INSERT INTO `tb_lplpo` (
						`kode_kec`,
						`kode_pusk`,
						`periode`,
						`create_time`,
						`create_by`
				) VALUES (
					?,?,?,?,?
				)",
				array(
					$data['kode_kec'],
					$data['kode_pusk'],
					$data['per_lplpo'].'-00',
					$time,
					$id_user
				)
			);
			return true;
		}
		//return $sql;
	}

	function countAllData(){
		return $this->db->count_all("tb_lplpo");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_lplpo
			WHERE
				id IN ($kode)"
			);
		$this->db->query(
			"DELETE FROM
				tb_detail_lplpo
			WHERE
				id_lplpo IN ($kode)"
			);
	}

	function deletecurrentlplpo(){
		$last_row=$this->db->query("
			SELECT id FROM tb_lplpo ORDER BY id DESC LIMIT 1
			");
		$id=$last_row->row_array();
		$last_id=$id['id'];
		$this->db->query(
			"DELETE FROM
				tb_lplpo
			WHERE
				id = $last_id"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_Pemusnahan");
		$query=$this->db->query("
			SELECT l.id as id,kec.KECDESC AS nama_kec, pusk.NAMA_PUSKES AS nama_pusk,DATE_FORMAT(l.periode,'%M %Y') AS periode
			FROM tb_lplpo l
			JOIN ref_kecamatan kec ON(kec.KDKEC = l.kode_kec)
			JOIN ref_puskesmas pusk ON(pusk.KD_PUSK = l.kode_pusk)
			order by l.id desc
			limit $start,$limit
			");
		return $query->result();
	}

	function GetListPbf($key){
	    $query = $this->db->query("select * from ref_pbf where SuplierName like '%$key%'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function getNewLplpo($value){
		/*$newlplpo=$this->db->query("
			SELECT dl.kode_obat,dl.stok_akhir,IFNULL(db.pemberian,0) AS pemberian,
			dl.nama_obj
			FROM tb_detail_lplpo dl
			LEFT JOIN tb_detail_distribusi db ON(db.kode_obat_req = dl.kode_obat)
			WHERE db.id_lplpo = $value
			GROUP BY dl.kode_obat
			ORDER BY dl.nama_obj
			");
		*/
		$newlplpo=$this->db->query("
			SELECT dl.kode_obat,dl.nama_obj,dl.stok_akhir,dl.pemberian
			FROM tb_detail_lplpo dl
			WHERE dl.id_lplpo = $value
			");
		return $newlplpo->result_array();
		/*if($newlplpo->num_rows() > 0){
			return $newlplpo->result_array();
		}else{
			return false;
		}*/

	}

	function cekpermintaan($id_lplpo){
		$query=$this->db->query("
			SELECT dl.id_lplpo as id_lplpo,dl.stok_opt as stok_opt,
			dl.kode_obat as kode_obat,oa.nama_obat AS nama_obat, oa.kekuatan AS kekuatan, dl.permintaan AS permintaan, oa.deskripsi AS sediaan
			FROM tb_detail_lplpo dl
			JOIN ref_obat_all oa ON(oa.id_obat = dl.kode_obat)
			WHERE NOT permintaan=0 AND id_lplpo = $id_lplpo
			and pemberian = 0
			");

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	function getIdLPLPO(){
		$id=$this->db->query("
			SELECT id from tb_lplpo order by id desc limit 1
			");
		return $id->row_array();
	}

	function getDataStok(){
		$newlplpo=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			oa.nama_obat as nama_obat, oa.kekuatan as kekuatan, oa.deskripsi as sediaan
			FROM tb_stok_obat so
			join ref_obat_all oa on(oa.id_obat=so.kode_obat)
			WHERE so.flag=1 and oa.kategori_objek = 'OBAT'
			order by so.nama_obj
			");

		return $newlplpo->result();
	}

	function getDataStokBmhp(){
		$newlplpo=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			oa.nama_obat as nama_obat, oa.kekuatan as kekuatan, oa.deskripsi as sediaan
			FROM tb_stok_obat so
			join ref_obat_all oa on(oa.id_obat=so.kode_obat)
			WHERE so.flag=1 and oa.kategori_objek = 'BMHP'
			order by so.nama_obj
			");

		return $newlplpo->result();
	}

	function getDataToChange($key){
		$query = $this->db->query("select * from tb_lplpo where id ='$key'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id'	=>$key->id,
	    		'kode_kec'	=>$key->kode_kec,
	    		'kode_pusk'	=>$key->kode_pusk,
	    		'periode'	=>$key->periode
	    	);
	    }
	    return $r;
	}

}
?>

<?php
Class Pemusnahan_Obat_Model extends CI_Model {

	function input_data_m($data){
		$data['create_time'] = date('Y-m-d H:i:s');
		$data['create_by'] = $this->session->userdata('id');
		$this->db->insert('tb_pemusnahan', $data);

		return $this->db->insert_id();
	}

	function detailpemusnahan($kode){
		$getID=$this->db->query("
			SELECT id FROM tb_pemusnahan ORDER BY id DESC LIMIT 1
			");
		if ($getID->num_rows() > 0)
			{
				$row=$getID->row();
				$last_id=$row->id;
			}
		//print_r($kode);
		//$last_id=$getID['id'];

		for ($i=0; $i < sizeof($kode) ; $i++) {
			$this->db->query("
				insert INTO tb_detail_pemusnahan(
					id_tb_pemusnahan,
					id_stok_obat
					)
				value(?,?)
				",array(
					$last_id,
					$kode[$i])
				);
		}
	}
	function pemusnahandata($kode){
		$this->db->query("
			update tb_stok_obat set flag=0
			where id_stok in ($kode)"
			);
		// $this->db->query(
		// 	"DELETE FROM
		// 		tb_stok_obat
		// 	WHERE
		// 		id_stok IN ($kode)"
		// 	);
	}

	function countAllData(){
		return $this->db->count_all("tb_stok_obat");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_Pemusnahan
			WHERE
				id IN ($kode)"
			);
	}

	function getData(){
		$query=$this->db->query("
			SELECT so.id_stok AS id,so.kode_obat AS kode_obat, so.nama_obj AS nama_obat,
			so.stok AS stok, so.expired AS ed, ((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS lama,
			YEAR(so.expired)-YEAR(CURDATE()) AS lamatahun, so.no_faktur AS no_faktur,so.no_batch AS no_batch,
			tp.dana,tp.tahun_anggaran
			FROM tb_stok_obat so
			JOIN tb_penerimaan tp ON tp.id = so.id_faktur
			WHERE ((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) >=-3 AND so.flag=1
			ORDER BY so.kode_obat
			");
		return $query->result();
	}

	function getDataRusak(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			select distinct dp.*,so.nama_obj as nama_obat,so.id_stok
			from tb_detail_pemusnahan dp
			join tb_stok_obat so on (so.kode_obat=dp.kode_obat)
			where dp.jenis_rusak=1 and dp.status=1
			");
		return $query->result();
	}

	function GetListPbf($key){
	    $query = $this->db->query("select * from ref_pbf where SuplierName like '%$key%'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function GetListObatBeri($key){
	    $query = $this->db->query("
	    	SELECT so.kode_obat as id_obat,so.expired as ed,so.no_faktur as no_faktur,
	    	so.nama_obj as nama_obj, so.stok as stok, so.no_batch as no_batch,
	    	so.id_stok as id_stok, p.dana AS sumber_dana,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_penerimaan p ON(p.no_faktur=so.no_faktur)
	    	where so.nama_obj like '%$key%' and so.flag=1 AND so.stok > 0
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'kode_obat'	=>$key->id_obat,
	    		'id_stok'	=>$key->id_stok,
	    		'stok'	=>$key->stok,
	    		'value'	=>$key->nama_obj,
	    		'nobatch'	=>$key->no_batch,
	    		'ed'=>$key->ed,
	    		'no_faktur'=>$key->no_faktur
	    	);
	    }
	    return $r;
	}

	function getDataStokRusak(){
		$query=$this->db->query("
			select dp.*,so.nama_obj as nama_obat
			from tb_detail_pemusnahan dp
			join tb_stok_obat so on (so.id_stok=dp.id_stok)
			where dp.jenis_rusak=2 and dp.status=1
			");
		return $query->result();
	}

	function insertStokRusak($data){
		$getID=$this->db->query("
			SELECT id FROM tb_pemusnahan ORDER BY id DESC LIMIT 1
			");
		if ($getID->num_rows() > 0)
			{
				$row=$getID->row();
				$last_id=$row->id;
			}

		$sql = $this->db->query("
			INSERT INTO `tb_detail_pemusnahan` (
					`id_tb_pemusnahan`,
					`kode_obat`,
					`nomor_faktur`,
					`no_batch`,
					`jumlah`,
					`jenis_rusak`,
					`id_stok`
			) VALUES (
				?,?,?,?,?,2,?
			)",
			array($last_id,
				$data['kode_obat'],
				$data['no_faktur'],
				$data['no_batch'],
				$data['jml_rusak'],
				$data['id_stok']
			)
		);

		$upt = $this->db->query("
	            update tb_stok_obat
	            set stok= (stok-?)
	            where id_stok = ?
		        ",
		        array($data['jml_rusak'],
						$data['id_stok'])
	        );
	}

	function getLog(){
		$query=$this->db->query("
			SELECT pm.create_time AS tanggal,dp.nomor_faktur,dp.no_batch,oa.object_name,
			dp.jumlah,dp.jenis_rusak,u.long_name,pm.saksi1,pm.saksi2, dp.id, dp.id_stok
			FROM tb_detail_pemusnahan dp
			JOIN tb_pemusnahan pm ON(pm.id=dp.id_tb_pemusnahan)
			JOIN ref_obat_all oa ON(oa.id_obat=dp.kode_obat)
			JOIN tb_user u ON(u.id_user=pm.create_by)
			");
		return $query->result();
	}

	function getDataEksport(){
		$query=$this->db->query("
			SELECT pm.create_time AS tanggal,dp.nomor_faktur,dp.no_batch,oa.object_name,
			dp.jumlah,dp.jenis_rusak,pm.saksi1,pm.saksi2,p.pbf
			FROM tb_detail_pemusnahan dp
			JOIN tb_pemusnahan pm ON(pm.id=dp.id_tb_pemusnahan)
			JOIN tb_penerimaan p ON p.no_faktur = dp.nomor_faktur
			JOIN ref_obat_all oa ON(oa.id_obat=dp.kode_obat)
			");
		return $query->result();
	}

	function getDataInstitusi(){
		$query=$this->db->query("
			SELECT *
			FROM tb_institusi ins
			LEFT JOIN ref_kabupaten rk ON (rk.CKabID = ins.id_kab)
			LEFT JOIN ref_propinsi rp ON (rp.CPropID = ins.id_prop)
			");
		return $query->row_array();
	}

	function update_qty($id, $data) {
			$this->db->where('id', $id);
			$query = $this->db->update('tb_detail_pemusnahan', $data);

			return $query;
	}
}
?>

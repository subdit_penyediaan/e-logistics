<?php
Class Ketersediaan_obat_puskesmas_model extends CI_Model {
	//sum setahun(*group by year where year now):bulan sekarang = rata2 penggunaan
	//ketersediaan=sisa stok : rata2 penggunaan
	//sisa stok diambil stok sisa pada bulan terakhir
	

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$tahun=date('Y');
		$bulan=date('m');
		$hari=date('d');
		$current=date('Y-m-d');
		if(($bulan-3)>0){
			$default=$tahun.'-'.($bulan-3).'-'.$hari;
		}elseif(($bulan-3)==0){
			//desember tahun sebelumnya
			$default=($tahun-1).'-12-'.$hari;
		}elseif(($bulan-3)==-1){
			//november tahun sebelumnya
			$default=($tahun-1).'-11-'.$hari;
		}else{
			//oktober tahun sebelumnya
			$default=($tahun-1).'-10-'.$hari;
		}
		

		$query=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE so.flag=1 AND dd.create_time BETWEEN '$default' AND '$current'
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function show_data($set){
		$where = "";

		if(!(empty($set['awal']))){
			//AND dd.create_time BETWEEN '2014-01-25' AND '2014-07-30'
			$where .= "AND dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."'";
			//WHERE p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
		}

		if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";		
		}

		if($set['kategori']!='all'){	
			$where .= "AND so.kategori='".$set['kategori']."'";	
		}
		
		
		$query = $this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			JOIN tb_penerimaan p ON(p.no_faktur=so.no_faktur)
			WHERE so.flag=1
				$where
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}


	function getRows($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}

		$query=$this->db->query("
			SELECT l.kode_pusk,dl.kode_obat,SUM(distinct(dl.sedia)) AS sedia,
			so.nama_obj as nama_obat
			FROM tb_detail_lplpo dl
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
			JOIN tb_stok_obat so ON(so.kode_obat=dl.kode_obat)
				WHERE $where
			GROUP BY l.kode_pusk,dl.kode_obat
			ORDER BY l.kode_pusk
			");//WHERE MONTH(l.periode)=2
		return $query->result();
	}

	function getPusk($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}

		$query=$this->db->query("
			SELECT l.kode_pusk,dl.kode_obat,dl.sedia,rp.NAMA_PUSKES
			FROM tb_detail_lplpo dl
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
				WHERE $where
			GROUP BY l.kode_pusk
			ORDER BY l.kode_pusk
			");
		return $query->result();
	}
}
?>
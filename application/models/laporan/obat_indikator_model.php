<?php
Class Obat_indikator_model extends CI_Model {
	function getDataEksport(){
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function getRows($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			$where .= "l.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}

		$query=$this->db->query("
			SELECT IF(ISNULL(l.kode_pusk),'',l.kode_pusk) AS kode_pusk,rb.id_obatindikator AS id_obatindikator,ro.nama_indikator,SUM(dl.terima + dl.stok_akhir) AS sedia
			FROM ref_obatindikator ro
			JOIN ref_obatgenerik rb ON rb.id_obatindikator = ro.id_indikator
			LEFT JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = rb.id
			LEFT JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			WHERE $where AND ro.delete_time is null
			GROUP BY rb.id_obatindikator,l.kode_pusk

			UNION ALL

			SELECT rob.id_indikator IS NULL AS kode_pusk,rob.id_indikator AS id_obatindikator, rob.nama_indikator,rob.status IS NULL AS sedia
			FROM ref_obatindikator rob
			WHERE rob.delete_time is null and rob.id_indikator NOT IN (
				SELECT rb.id_obatindikator
				FROM ref_obatindikator ro
				JOIN ref_obatgenerik rb ON rb.id_obatindikator = ro.id_indikator
				LEFT JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = rb.id
				LEFT JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
				WHERE $where AND ro.delete_time is null
				GROUP BY rb.id_obatindikator
				)

			ORDER BY id_obatindikator
		");

		return $query->result();
	}

	function create_table($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
			$periode=$set['awal'];
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
			$periode=$set['awal'].'-'.$set['akhir'];
		}
		$ins=$this->db->query("select * from tb_institusi");
		$temp=$ins->row_array();
		$wil=$temp['id_kab'];
		$this->log_save($periode,$wil);
		$namatabel='pkm_'.$periode.'_'.$wil;
		$this->db->query("DROP TABLE IF EXISTS $namatabel");
		$query=$this->db->query("
			create table $namatabel as
			SELECT l.kode_pusk,dl.kode_generik as kode_obat,SUM(distinct(dl.sedia)) AS sedia,
			dl.nama_obj
			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
				WHERE $where
			GROUP BY l.kode_pusk,dl.kode_generik
			ORDER BY l.kode_pusk
			");//WHERE MONTH(l.periode)=2
		//return $query->result();


	}

	function log_save($periode,$wil)
    {
       date_default_timezone_set('Asia/Jakarta');

       $log['id'] = date('d-m-Y')."/".date('H:i:s');
       $log['periode'] = $periode;
       $log['cara_kirim'] = '<a href="#">online</a>';//$this->input->post('metode_kirim');
       //$log['tglsistem'] = $timenow;
       $log['status'] = "pending";
       $log['namatable'] = "pkm_".$periode."_".$wil;
       $log['id_user'] = $this->session->userdata('id');
       $log['jenis_laporan'] = 7;
       $this->db->insert('log_sinkron', $log);
       //return $hasil;
    }

	function getPusk($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}

		$query=$this->db->query("
			SELECT l.kode_pusk,dl.kode_generik as kode_obat,dl.sedia,rp.NAMA_PUSKES
			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
				WHERE $where
			GROUP BY l.kode_pusk
			ORDER BY l.kode_pusk
			");
		return $query->result();
	}
}
?>

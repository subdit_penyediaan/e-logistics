<?php
Class Obat_Kadaluarsa_Model extends CI_Model {


	function countAllData(){
		return $this->db->count_all("tb_stok_obat");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_obat_kadaluarsa
			WHERE
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT so.kode_obat AS kode_obat, oa.deskripsi AS sediaan,
			so.stok AS stok, so.expired AS ed, MONTH(CURDATE())-MONTH(so.expired) AS lama,
			YEAR(so.expired)-YEAR(CURDATE()) AS lamatahun,so.nama_obj AS nama_obj,p.dana AS dana,
			((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON (oa.id_obat = so.kode_obat)
			JOIN tb_penerimaan p ON (p.no_faktur = so.no_faktur)
			WHERE so.flag=1
			ORDER BY jarak DESC
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_obat_kadaluarsa p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function GetListPbf($key){
	    $query = $this->db->query("select * from ref_pbf where SuplierName like '%$key%'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode'	=>$key->kode,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function show_data($set){
		$where = "";
		$limit = "";
		$join  = "";
		$select = ", '' as id_obatprogram";

		if(!(empty($set['tahun_ed']))){
			$where .= " AND EXTRACT(YEAR FROM so.expired)=".$set['tahun_ed'];
		}

		if(!(empty($set['bulan_ed']))){
			$where .= " AND EXTRACT(MONTH FROM so.expired)=".$set['bulan_ed'];
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}

		if($set['dana']!='all'){
			$where .= "AND p.dana='".$set['dana']."'";
		}

		if($set['tahun_anggaran']!='all'){
			$where .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
		}

		// if($set['kategori']!='all'){
		// 	$where .= "AND so.kategori='".$set['kategori']."'";
		// }

		if(!(empty($set['program']))){
			if ($set['program']!='all'){
				$where .= "AND d.id_obatprogram ='".$set['program']."'";
				$join = "LEFT JOIN map_inn_program d ON d.id_inn = c.id";
				$select = ", d.id_obatprogram";
			}
		}

		if(isset($set['limit'])){
			$limit .= " limit ".$set['limit'];
		}

		if (isset($set['jarak'])) {
			$where .= 'AND ((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) '.$set['jarak'];
		}

		$query = $this->db->query("
			SELECT so.no_batch as no_batch,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj, oa.deskripsi AS sediaan,
			so.stok AS stok, so.expired AS ed, MONTH(CURDATE())-MONTH(so.expired) AS lama, so.harga,
			YEAR(so.expired)-YEAR(CURDATE()) AS lamatahun, oa.kekuatan AS kekuatan, p.dana AS dana,p.tahun_anggaran,oa.org_pembuat as pabrik,
			((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak $select
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON (oa.id_obat = so.kode_obat)
			JOIN tb_penerimaan p ON (p.id=so.id_faktur)
      LEFT JOIN ref_obatgenerik c ON c.id = oa.id_generik
            	$join
			WHERE so.flag=1
				$where
			ORDER BY jarak DESC
				$limit
			"
			);

		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}
}
?>

 <style>
	
</style>
<script type="text/javascript">
$(document).ready(function(){
	
	
	var url="laporan/pengeluaran/get_data_pengeluaran";
	$('#list_pengeluaran').load(url);	
	//============== submit add form

	//eksport button
	$("#btn_eksport").click(function(){
		var urlprint="laporan/pengeluaran/printOut";
		window.open(urlprint);		
	})
	//end eksport button
	$("#btn_show").click(function(){
		var url2="laporan/pengeluaran/search_data_by";
		var form_data = {
			awal:$("#periodeawal").val(),
			akhir:$("#periodeakhir").val()
			//dana:$("#dana").val()
		}
		//alert("cek ajak");
		$.ajax({
			type:"POST",
			url:url2,
			data: form_data,
			success:function(e){
				
				$("#list_pengeluaran").html(e);//+"#list_sedian_obat");
			}
		});
	})

	$('#btn_show2').click(function(){
		$.get(url,function(data,status){
			$('#list_pengeluaran').load()
		})
	})

	//============== end submit add form
	
});
function search_data_by(){

}

function databydana(val){
	var url="laporan/pengeluaran/get_data_by/"+val;
	//alert(url);
	$('#list_pengeluaran').load(url);	
}

//======= get puskesmas
function get_puskesmas(val) {
    if(val == 'add'){
    	alert("silakan tambah data puskesmas melalui menu/profil/unit penerima");
    }else{
    	//alert("cek");
    	$.get('laporan/pengeluaran/get_data_puskesmas/' + val, function(data) {$('#nmpuskesmas_report').html(data);});
    }
}

//====== end get puskesmas
</script>
<div class="panel panel-primary" id="halaman_pengeluaran">
	<div class="panel-heading">Detail Pengeluaran Obat</div>
	<div id="up-konten"class="panel-body" style="padding:15px;">
		<!-- bag. isi -->
			<div class="" id="">
		<!--form method="POST" name="frmInput" style="" id="frmInput" action="<?php echo site_url('laporanmanual/pengeluaran_obat/process_form'); ?>"-->
				<table class="table">
						<tr>
							<td>Kode Obat</td>
							<td>
								: <?php echo $obat['kode_obat'];?>
							</td>
							
						</tr>
						<tr>
							<td>Nama Obat</td>
							<td>
								: <?php echo $obat['nama_obat']." ".$obat['kekuatan']." ".$obat['sediaan'];?>
							</td>
							
						</tr>
					</table>
			</div>
			<!--/form-->
			<div id="list_pengeluaran">
				<table class="table tabel-condensed">
				<thead>
				<tr class="active">
					<th>No.</th>
					<th>Nama Puskesmas</th>
					<th>Periode</th>						
					<th>Jumlah Pengeluaran</th>
					<th>Tanggal dikeluarkan</th>			
				</tr>
				</thead>
				<tbody id="trcontent">
					<?php
						$i=1;
						foreach ($detailkeluar as $rows) {
							//$saldo_akhir=$rows->saldo_persediaan-$rows->pengeluaran;
							echo '<tr style=""><td>'.$i.'.</td>
							<td>'.$rows->nama_pusk.'</td>
							<td>'.$rows->periode.'</td>					
							<td>'.$rows->pemberian.'</td>
							<td>'.$rows->tanggal_pemberian.'</td></tr>';					
							$i++;
						}
					?>
				</tbody>
			</table>

			</div>
	</div>
</div>
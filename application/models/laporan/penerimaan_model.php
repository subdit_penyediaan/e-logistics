<?php
Class Penerimaan_Model extends CI_Model {


	function countAllData(){
		return $this->db->count_all("tb_detail_faktur");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_penerimaan
			WHERE
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga, oa.kekuatan as kekuatan,df.nama_obj as nama_obj
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_penerimaan p ON(p.no_faktur=df.no_faktur)
			order by df.nama_obj
			");
		return $query->result();
	}

	function getDataBy($limit,$start,$key){
		$this->db->limit($limit, $start);
		//$this->db->where('dana',$key);
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_penerimaan p ON(p.no_faktur=df.no_faktur)
			where p.dana ='$key'
			order by df.kode_obat
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_penerimaan p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function GetListPbf($key){
	    $query = $this->db->query("select * from ref_pbf where SuplierName like '%$key%'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode'	=>$key->kode,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function show_data($set){
		$where = "";
		$join = "";
		$select = ", '' as id_obatprogram";

		if(!(empty($set['awal']))){
			$where .= "WHERE p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."'";
			//WHERE p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
		}

		if($set['dana']!='all'){
			if(!(empty($set['awal']))){
				$where .= "AND p.dana='".$set['dana']."'";
			}else{
				$where .= "WHERE p.dana='".$set['dana']."'";
			}

		}

		if($set['tahun_anggaran']!='all'){
			if(!(empty($set['awal']))){
				$where .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
			}else{
				$where .= "WHERE p.tahun_anggaran='".$set['tahun_anggaran']."'";
			}

		}

		if($set['kategori']!='all'){
			if((!(empty($set['awal'])))||(!(empty($set['awal'])))){
				$where .= "AND oa.kategori='".$set['kategori']."'";
			}else{
				$where .= "WHERE oa.kategori='".$set['kategori']."'";
			}
		}

		if(!(empty($set['nama']))){
			$where .= "AND df.nama_obj LIKE '%".$set['nama']."%'";
		}

		if(!(empty($set['id_faktur']))){
			$where .= "AND p.id = '".$set['id_faktur']."'";
		}

		if(!(empty($set['program']))){
			if ($set['program']!='all'){
				$select = ", d.id_obatprogram";
				$join = "JOIN ref_obatgenerik c ON c.id = oa.id_generik
								LEFT JOIN map_inn_program d ON d.id_inn = c.id";
				$where .= "AND d.id_obatprogram ='".$set['program']."'";
			}
		}

		$query = $this->db->query("
			SELECT df.no_batch as no_batch,df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date,df.jumlah_kec AS jumlah,p.no_kontrak,
			p.dana AS dana,df.harga AS harga,df.nama_obj as nama_obj,p.tahun_anggaran,oa.org_pembuat as pabrik, p.no_faktur
			$select
			FROM tb_penerimaan p
			JOIN tb_detail_faktur df ON(df.id_faktur = p.id)
			JOIN ref_obat_all oa ON(oa.id_obat = df.kode_obat)
			$join
			$where
			order by df.nama_obj
			");
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}

	}

	function getDataFaktur($key){
	    $query = $this->db->query("
	    	SELECT *
	    	FROM tb_penerimaan
			WHERE no_faktur LIKE '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'no_faktur'	=>$key->no_faktur,
	    		'pbf' =>$key->pbf,
	    		'anggaran' =>$key->dana,
	    		'tahun' =>$key->tahun_anggaran,
	    		'id' =>$key->id
	    	);
	    }
	    return $r;
	}
}
?>

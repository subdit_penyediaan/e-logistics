<?php
Class Kartu_stok_model extends CI_Model {


	function countAllData(){
		return $this->db->count_all("tb_penerimaan");
	}

	function getData($limit,$start=0){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_penerimaan");
		$query=$this->db->query("
			SELECT id,no_faktur,tanggal,nama_dok,DATE_FORMAT(periode,'%M %Y') as periode,
			pbf,dana,no_kontrak
			FROM tb_penerimaan
			order by id desc
			limit $start,$limit
			");
		return $query->result();
	}

	function GetListPbf($key){
	    $query = $this->db->query("
	    	select * from ref_pbf
	    	where SuplierName like '%$key%' or alamat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function getResult($key){
		$query=$this->db->query("
			select distinct nama_objek from tb_stok_obat so
			join hlp_generik hg on (hg.id_hlp=so.kode_generik)
			where nama_objek like '%$key%'
			");
		foreach ($query->result() as $key) {
	    	$r[] = array(

	    		'value'	=>$key->nama_objek
	    	);
	    }
	    return $r;
		//return $query->result();
	}

	function getKartuStok($key){
		$nama_generik=$key['nama_generik'];
		$sumber_dana=$key['sumber_dana'];
		$and='';
		if($key['sumber_dana']!='all'){
			$and="AND p.dana='$sumber_dana'";
		}

		$query=$this->db->query("
			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			p.tanggal AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,p.pbf AS alur,so.no_batch,
			df.jumlah_kec AS penerimaan,IFNULL((df.jumlah_kec*0),0) AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id=so.id_stok)
			JOIN tb_penerimaan p ON(p.id=df.id_faktur)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik'	$and

			UNION

			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			dd.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,rp.NAMA_PUSKES AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			UNION

			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			d.tanggal_trans AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,d.unit_eks AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			pem.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			CASE dd.jenis_rusak WHEN 1 THEN 'rusak saat penerimaan'
			WHEN 2 THEN 'rusak saat stok opnam'
			WHEN 3 THEN 'kadaluarsa: dimusnahkan'
			ELSE '' END AS alur,so.no_batch,
			IFNULL((dd.jumlah*0),0) AS penerimaan,dd.jumlah AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_pemusnahan dd ON(dd.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dd.id_tb_pemusnahan
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			ORDER BY tanggal
			");
		return $query->result();
		//dd.create_time AS tanggal,
	}

	function getKartuStokEksport($key){
		$nama_generik=$key['nama_generik'];
		$sumber_dana=$key['sumber_dana'];
		$and='';
		if($key['sumber_dana']!='all'){
			$and="AND p.dana='$sumber_dana'";
		}

		$query=$this->db->query("
			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			p.tanggal AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,p.pbf AS alur,so.no_batch,
			df.jumlah_kec AS penerimaan,IFNULL((df.jumlah_kec*0),0) AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id=so.id_stok)
			JOIN tb_penerimaan p ON(p.id=df.id_faktur)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik'	$and

			UNION

			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			dd.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,rp.NAMA_PUSKES AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			UNION

			SELECT DISTINCT p.dana as sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat as pabrik,
			d.tanggal_trans AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,d.unit_eks AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa on oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			pem.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			CASE dd.jenis_rusak WHEN 1 THEN 'rusak saat penerimaan'
			WHEN 2 THEN 'rusak saat stok opnam'
			WHEN 3 THEN 'kadaluarsa: dimusnahkan'
			ELSE '' END AS alur,so.no_batch,
			IFNULL((dd.jumlah*0),0) AS penerimaan,dd.jumlah AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_pemusnahan dd ON(dd.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dd.id_tb_pemusnahan
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE 1=1 AND hg.nama_objek ='$nama_generik' $and

			ORDER BY tanggal
			");
		return $query->result_array();

	}

	function getDaftar(){
		$query=$this->db->query("
			SELECT distinct so.kode_generik,hg.nama_objek,hg.sediaan
			FROM tb_stok_obat so
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			");
		return $query->result();
	}

	function getKartuStokDetail($key){
		$kode_obat = $key['kode'];
		$sumber_dana = $key['sumber_dana'];
		$batch = $key['batch'];
		$nobatch = $key['kode_batch'];
		$and = '';

		if ($key['sumber_dana']!='all') {
			$and="AND p.dana='$sumber_dana'";
		}

		if ($batch != 'all') {
			$and=" AND so.no_batch = '$nobatch'";
		}

		$query=$this->db->query("
			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			p.tanggal AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,p.pbf AS alur,so.no_batch,
			df.jumlah_kec AS penerimaan,IFNULL((df.jumlah_kec*0),0) AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id=so.id_stok)
			JOIN tb_penerimaan p ON(p.id=df.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			dd.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,rp.NAMA_PUSKES AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			LEFT JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			d.tanggal_trans AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,d.unit_eks AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			pem.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			CASE dd.jenis_rusak WHEN 1 THEN 'rusak saat penerimaan'
			WHEN 2 THEN 'rusak saat stok opnam'
			WHEN 3 THEN 'kadaluarsa: dimusnahkan'
			ELSE '' END AS alur,so.no_batch,
			IFNULL((dd.jumlah*0),0) AS penerimaan,dd.jumlah AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_pemusnahan dd ON(dd.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dd.id_tb_pemusnahan
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			ORDER BY tanggal
			");
		return $query->result();
		//dd.create_time AS tanggal,
	}

	function GetListObat($key){

	    $query = $this->db->query("
	    	select * from tb_stok_obat
	    	where nama_obj like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_obat'	=>$key->kode_obat,
	    		'value'	=>$key->nama_obj,
	    		'ed'	=>$key->expired,
	    		'nobatch'	=>$key->no_batch
	    	);
	    }
	    return $r;
	}

	function getKartuStokDetailEksport($key){
		$kode_obat=$key['kode'];
		$sumber_dana=$key['sumber_dana'];
		$and='';
		if($key['sumber_dana']!='all'){
			$and="AND p.dana='$sumber_dana'";
		}

		$query=$this->db->query("
			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			p.tanggal AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,p.pbf AS alur,so.no_batch,
			df.jumlah_kec AS penerimaan,IFNULL((df.jumlah_kec*0),0) AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id=so.id_stok)
			JOIN tb_penerimaan p ON(p.id=df.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			dd.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,rp.NAMA_PUSKES AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			d.tanggal_trans AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,d.unit_eks AS alur,so.no_batch,
			IFNULL((dd.pemberian*0),0) AS penerimaan,dd.pemberian AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			UNION

			SELECT DISTINCT p.dana AS sumber_dana,p.tahun_anggaran,oa.deskripsi,oa.org_pembuat AS pabrik,
			pem.create_time AS tanggal,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			CASE dd.jenis_rusak WHEN 1 THEN 'rusak saat penerimaan'
			WHEN 2 THEN 'rusak saat stok opnam'
			WHEN 3 THEN 'kadaluarsa: dimusnahkan'
			ELSE '' END AS alur,so.no_batch,
			IFNULL((dd.jumlah*0),0) AS penerimaan,dd.jumlah AS pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_pemusnahan dd ON(dd.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dd.id_tb_pemusnahan
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			JOIN tb_penerimaan p ON(p.id=so.id_faktur)
			JOIN ref_obat_all oa ON oa.id_obat=so.kode_obat
			WHERE so.kode_obat = '$kode_obat' $and

			ORDER BY tanggal
			");
		return $query->result_array();
		//dd.create_time AS tanggal,
	}

}
?>

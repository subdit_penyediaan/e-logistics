<?php
Class Mutasi_obat_model extends CI_Model {
	//sum setahun(*group by year where year now):bulan sekarang = rata2 penggunaan
	//ketersediaan=sisa stok : rata2 penggunaan
	//sisa stok diambil stok sisa pada bulan terakhir
	

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$tahun=date('Y');
		$bulan=date('m');
		$hari=date('d');
		$current=date('Y-m-d');
		if(($bulan-3)>0){
			$default=$tahun.'-'.($bulan-3).'-'.$hari;
		}elseif(($bulan-3)==0){
			//desember tahun sebelumnya
			$default=($tahun-1).'-12-'.$hari;
		}elseif(($bulan-3)==-1){
			//november tahun sebelumnya
			$default=($tahun-1).'-11-'.$hari;
		}else{
			//oktober tahun sebelumnya
			$default=($tahun-1).'-10-'.$hari;
		}
		

		$query=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE so.flag=1 AND dd.create_time BETWEEN '$default' AND '$current'
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function show_data($set){
		$where = "";

		if(!(empty($set['awal']))){
			//AND dd.create_time BETWEEN '2014-01-25' AND '2014-07-30'
			$where .= "AND dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."'";
			//WHERE p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
		}

		if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";		
		}

		if($set['kategori']!='all'){	
			$where .= "AND so.kategori='".$set['kategori']."'";	
		}
		
		
		$query = $this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			JOIN tb_penerimaan p ON(p.no_faktur=so.no_faktur)
			WHERE so.flag=1
				$where
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}


	function getRows($set){
		$where = "";
		$where2 = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
			$where2 .= "d.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
			$where2 .= "d.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}
		//$this->create_table($set);
		$sql="
			SELECT kode_pusk, kode_obat,SUM(keluar) AS keluar, nama_obj
			FROM (
			SELECT l.kode_pusk,dd.kode_obat_res AS kode_obat,SUM(dd.pemberian) AS keluar,so.nama_obj
			FROM tb_detail_distribusi dd
			JOIN tb_lplpo l ON l.id = dd.id_lplpo
			JOIN tb_stok_obat so ON so.id_stok = dd.id_stok
			LEFT JOIN tb_distribusi d ON d.id_distribusi = dd.id_distribusi
				WHERE $where
			GROUP BY kode_pusk,kode_obat

			UNION

			SELECT d.kode_up AS kode_pusk,dd.kode_obat_res AS kode_obat,SUM(dd.pemberian) AS keluar,so.nama_obj
			FROM tb_detail_distribusi dd			
			JOIN tb_stok_obat so ON so.id_stok = dd.id_stok
			RIGHT JOIN tb_distribusi d ON d.id_distribusi = dd.id_distribusi
				WHERE $where2
			GROUP BY kode_pusk,kode_obat
			) ex
			GROUP BY kode_pusk,kode_obat
			ORDER BY kode_pusk
			";
		$query=$this->db->query($sql);//WHERE MONTH(l.periode)=2		
		return $query;

	}

	function create_table($set){
		$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
			$periode=$set['awal'];
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
			$periode=$set['awal'].'-'.$set['akhir'];
		}
		$ins=$this->db->query("select * from tb_institusi");
		$temp=$ins->row_array();
		$wil=$temp['id_kab'];
		$this->log_save($periode,$wil);
		$namatabel='pkm_'.$periode.'_'.$wil;
		$this->db->query("DROP TABLE IF EXISTS $namatabel");
		$query=$this->db->query("
			create table $namatabel as
			SELECT l.kode_pusk,dl.kode_generik as kode_obat,SUM(distinct(dl.sedia)) AS sedia,
			dl.nama_obj
			FROM tb_detail_lplpo_kel dl
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
				WHERE $where
			GROUP BY l.kode_pusk,dl.kode_generik
			ORDER BY l.kode_pusk
			");//WHERE MONTH(l.periode)=2
		//return $query->result();

		
	}

	function log_save($periode,$wil)
    {
       date_default_timezone_set('Asia/Jakarta');

       $log['id'] = date('d-m-Y')."/".date('H:i:s');
       $log['periode'] = $periode;
       $log['cara_kirim'] = '<a href="#">online</a>';//$this->input->post('metode_kirim');
       //$log['tglsistem'] = $timenow;
       $log['status'] = "pending";
       $log['namatable'] = "pkm_".$periode."_".$wil;
       $log['id_user'] = $this->session->userdata('id');
       $log['jenis_laporan'] = 7;
       $this->db->insert('log_sinkron', $log);
       //return $hasil;
    }

	function getPusk($set){
		$where = "";
		$where2 = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
			$where2 .= "d.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
			$where2 .= "d.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}

		$query=$this->db->query("
			SELECT kode_pusk, kode_obat,SUM(keluar) AS keluar, nama_obj, faskes
			FROM (
			SELECT l.kode_pusk,dd.kode_obat_res AS kode_obat,SUM(dd.pemberian) AS keluar,so.nama_obj, p.NAMA_PUSKES AS faskes
			FROM tb_detail_distribusi dd
			JOIN tb_lplpo l ON l.id = dd.id_lplpo
			JOIN tb_stok_obat so ON so.kode_obat = dd.kode_obat_res
			LEFT JOIN tb_distribusi d ON d.id_distribusi = dd.id_distribusi
			JOIN ref_puskesmas p ON p.KD_PUSK = l.kode_pusk
			WHERE $where
			GROUP BY kode_pusk
			UNION

			SELECT d.kode_up AS kode_pusk,dd.kode_obat_res AS kode_obat,SUM(dd.pemberian) AS keluar,so.nama_obj, d.unit_eks AS faskes
			FROM tb_detail_distribusi dd
			LEFT JOIN tb_lplpo l ON l.id = dd.id_lplpo
			JOIN tb_stok_obat so ON so.kode_obat = dd.kode_obat_res
			RIGHT JOIN tb_distribusi d ON d.id_distribusi = dd.id_distribusi
			WHERE $where2
			GROUP BY kode_pusk,kode_obat
			) ex
			GROUP BY kode_pusk
			ORDER BY kode_pusk
			");
		return $query->result();
	}
}
?>
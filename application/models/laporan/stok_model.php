<?php
Class Stok_Model extends CI_Model {

	function getData($limit,$start){
		$query=array();
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, df.jumlah_kec AS saldo_persediaan,
			IFNULL(SUM(distinct(dd.pemberian)),0) AS pengeluaran, so.stok AS stok, df.harga AS harga,
			p.dana AS sumber_dana
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.no_batch = so.no_batch)
			JOIN tb_penerimaan p ON (p.no_faktur=df.no_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE 1=1
			GROUP BY df.no_batch
			ORDER BY so.nama_obj ASC
			");

		return $query->result();// && $query2->result();
		//return $query2->result();
		//mau secara general atau dirinci tiap no.batch
		//klo general 1 harga, klo dirinci harga lama sm baru bisa dilacak
	}

	function getLapStok(){
		$query2=$this->db->query("
			select stok,harga from tb_stok_obat order by nama_obj
			");
		return $query2->result();
	}

	function getDataBy($limit,$start,$key){
		$this->db->limit($limit, $start);
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_Stok p ON(p.no_faktur=df.no_faktur)
			where p.dana ='$key'
			order by df.kode_obat
			");
		return $query->result();
	}

	function getDataEksport(){
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_Stok p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function countAllData(){
		$set['awal']=$this->session->userdata('awal');
        $set['akhir']=$this->session->userdata('akhir');//dana,kategori
        $set['dana']=$this->session->userdata('dana');
        $set['kategori']=$this->session->userdata('kategori');
        $set['nama']=$this->session->userdata('nama');
        //print_r($set);
		$where = "";

		if(!(empty($set['awal']))){
			$where .= "AND (df.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
		}

		if($set['dana']!='all'){
			$where .= "AND p.dana='".$set['dana']."'";
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
		}

		//print_r($where);die;
		$query = $this->db->query("
			SELECT COUNT(*) AS total_row
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.no_batch = so.no_batch)
			JOIN tb_penerimaan p ON(p.no_faktur = so.no_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.no_batch=so.no_batch)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.no_batch=so.no_batch)
			WHERE 1=1
				$where

			ORDER BY so.nama_obj
			"
			);
		//return $this->db->count_all("tb_detail_faktur");
		$temp=$query->row();
		return $temp->total_row;
	}

	function show_data($set){

		$where = $where2 = $where3 = "";
		$join = "";

		if(!(empty($set['awal']))){
			//$where .= "AND (df.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where .= "AND (p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where2 .= "AND (dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where3 .= "AND (pem.cur_date BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
		}
		if(isset($set['dana'])){
			if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";
				$where2 .= "AND p.dana='".$set['dana']."'";
				$where3 .= "AND p.dana='".$set['dana']."'";
			}
		}
		if(isset($set['tahun_anggaran'])){
			if($set['tahun_anggaran']!='all'){
				$where .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
				$where2 .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
				$where3 .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
			}
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where2 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where3 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
			$where2 .= "AND so.kategori='".$set['kategori']."'";
			$where3 .= "AND so.kategori='".$set['kategori']."'";
		}

		if($set['jprogram']!='all'){
			$join .= "JOIN map_inn_program rog ON rog.id_inn = so.kode_generik";
			$where .= "AND rog.id_obatprogram = ".$set['jprogram'];
			$where2 .= "AND rog.id_obatprogram = ".$set['jprogram'];
			$where3 .= "AND rog.id_obatprogram = ".$set['jprogram'];
		}

		//print_r($where);die;
		$sql = "
			SELECT kode_obat,nama_obj, SUM(saldo_persediaan) AS saldo_persediaan,SUM(pengeluaran) AS pengeluaran,
			SUM(jml_rusak) AS jml_rusak,
			(SUM(saldo_persediaan)- (SUM(pengeluaran) + SUM(jml_rusak))) AS stok,
			harga,expired, sumber_dana,no_batch,tahun_anggaran, pabrik,sediaan,id_stok
			FROM (
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, df.jumlah_kec AS saldo_persediaan,
			0 AS pengeluaran,0 AS jml_rusak,
			so.stok AS stok, so.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
			0 AS jml_rusak,
			0 AS stok, so.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where2
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			0 AS pengeluaran,IFNULL(CASE dp.status WHEN 0 THEN dp.jumlah END,0) AS jml_rusak, 0 AS stok, so.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where3
			GROUP BY so.id_stok) ex

			GROUP BY id_stok
			ORDER BY nama_obj
			";

		// print_r($sql);
		$query = $this->db->query($sql);
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}

	function show_data_fornas($set){

		$where = $where2 = $where3 = "";
		$join = "";

		if(!(empty($set['awal']))){
			//$where .= "AND (df.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where .= "AND (p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where2 .= "AND (dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where3 .= "AND (pem.cur_date BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
		}
		if(isset($set['dana'])){
			if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";
				$where2 .= "AND p.dana='".$set['dana']."'";
				$where3 .= "AND p.dana='".$set['dana']."'";
			}
		}
		if(isset($set['tahun_anggaran'])){
			if($set['tahun_anggaran']!='all'){
				$where .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
				$where2 .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
				$where3 .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
			}
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where2 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where3 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}

		$sql = "
			SELECT ref_fornas.id_fornas, ref_fornas.nama_obat, SUM(saldo_persediaan) AS saldo_persediaan,
			SUM(pengeluaran) AS pengeluaran,
			SUM(jml_rusak) AS jml_rusak, (SUM(saldo_persediaan)- (SUM(pengeluaran) + SUM(jml_rusak))) AS stok, ref_fornas.sediaan, max(harga) as harga
			FROM (
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, df.jumlah_kec AS saldo_persediaan,
			0 AS pengeluaran,0 AS jml_rusak,
			so.stok AS stok, max(so.harga) AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
			0 AS jml_rusak,
			0 AS stok, 0 AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where2
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			0 AS pengeluaran,IFNULL(CASE dp.status WHEN 0 THEN dp.jumlah END,0) AS jml_rusak, 0 AS stok, 0 AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where3
			GROUP BY so.id_stok) ex
			left join map_obat_fornas on map_obat_fornas.id_obat = ex.kode_obat
			left join ref_fornas on map_obat_fornas.id_fornas = ref_fornas.id_fornas
			GROUP BY ref_fornas.id_fornas
			ORDER BY nama_obat
			";

		// print_r($sql); die();
		$query = $this->db->query($sql);
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query;
		}
	}

	function getKelFornas($set){
		$query=$this->db->query("
			SELECT mof.id_fornas,SUM(DISTINCT(so.stok)) AS stok_akhir,
			IFNULL(SUM(dd.pemberian),0) AS jumlah_penggunaan,SUM(DISTINCT(df.jumlah_kec)) AS jumlah_penerimaan,
			rf.nama_obat AS nama_fornas,fp.nama_obat AS parent
			FROM tb_stok_obat so
			JOIN map_obat_fornas mof ON(mof.id_obat=so.kode_obat)
			JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN tb_detail_faktur df ON(df.id=so.id_stok)
			JOIN map_fornas_parent fp ON(fp.id_fornas=rf.parent)
			GROUP BY mof.id_fornas
			ORDER BY mof.id_fornas
			");
		return $query->result();
	}

	function getRows(){
		/*$where = "";

		if($set['awal']==$set['akhir']){
			//$bulan=$set['awal'];
			$where .= "l.periode = '".$set['awal']."-00'";
		}else{
			$where .= "l.periode BETWEEN '".$set['awal']."-00' AND '".$set['akhir']."-00'";
		}*/

		$query=$this->db->query("
			SELECT kode_obat,nama_obj,id_hlp,nama_objek,dana,
						SUM(penerimaan) AS penerimaan,
						IFNULL(SUM(pemberian),0) AS pemberian
			FROM (
			/*saldo*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						SUM(df.jumlah_kec) AS penerimaan,
						0 AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						JOIN tb_detail_faktur df ON(df.id=so.id_stok)
						GROUP BY hg.id_hlp,p.dana
			UNION
			/*rusak*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						0 AS penerimaan,
						IFNULL(SUM(dd.pemberian),0) AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						LEFT JOIN tb_detail_distribusi dd ON(so.id_stok=dd.id_stok)
						GROUP BY hg.id_hlp,p.dana

			UNION
			/*rusak*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						0 AS penerimaan,
						dp.jumlah AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
						JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
						WHERE dp.status = 0
						GROUP BY hg.id_hlp,p.dana

			) qry

			GROUP BY id_hlp,dana
			ORDER BY nama_objek

			");
		return $query->result();
	}

	function getRowSaldo(){
		$query = $this->db->query("
			SELECT id_hlp,nama_objek,dana,
	SUM(penerimaan) AS penerimaan,
	IFNULL(SUM(pemberian),0) AS pemberian, SUM(penerimaan)-IFNULL(SUM(pemberian),0) AS saldo
			FROM (
			/*saldo*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						SUM(df.jumlah_kec) AS penerimaan,
						0 AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						JOIN tb_detail_faktur df ON(df.id=so.id_stok)
						WHERE YEAR(p.tanggal) = YEAR(NOW())-INTERVAL 1 YEAR
						GROUP BY hg.id_hlp,p.dana
			UNION
			/*pemberian*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						0 AS penerimaan,
						IFNULL(SUM(dd.pemberian),0) AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						LEFT JOIN tb_detail_distribusi dd ON(so.id_stok=dd.id_stok)
						WHERE YEAR(dd.create_time) = YEAR(NOW())-INTERVAL 1 YEAR
						GROUP BY hg.id_hlp,p.dana

			UNION
			/*rusak*/
			SELECT so.kode_obat,so.nama_obj,hg.id_hlp,hg.nama_objek,p.dana,
						0 AS penerimaan,
						dp.jumlah AS pemberian
						FROM tb_stok_obat so
						JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
						JOIN tb_penerimaan p ON(p.id=so.id_faktur)
						JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
						JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
						WHERE dp.status = 0 AND YEAR(pem.cur_date) = YEAR(NOW())-INTERVAL 1 YEAR
						GROUP BY hg.id_hlp,p.dana

			) qry

			GROUP BY id_hlp,dana
			ORDER BY nama_objek
			");
		return $query->result();
	}

	function getSumberDana(){
		$query=$this->db->query("
			SELECT p.dana
			FROM tb_penerimaan p
			GROUP BY p.dana
		");
		return $query->result();
	}
}
?>

<?php
Class Lap_Lplpo_Model extends CI_Model {
	
	function getDataKecamatan(){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			select id_kab
			from tb_institusi
			");
		$kab = $datakab->row_array();
		//print_r($kab);
		$kabupaten=$kab['id_kab'];
		$query = $this->db->query("
			select *
			from ref_kecamatan
			where KDKAB='$kabupaten'");
		return $query->result_array();
	}

	function GetComboPuskesmas($key){
		$sql = $this->db->query("
            SELECT KD_PUSK, NAMA_PUSKES FROM ref_puskesmas WHERE KODE_KEC='$key' ORDER BY NAMA_PUSKES"
        );
        return $sql->result_array();
	}
	
	function countAllData(){
		return $this->db->count_all("tb_stok_obat");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				tb_ketersediaan_obat
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT dd.kode_obat_res as kode_obat,SUM(DISTINCT(dd.pemberian)) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_detail_distribusi dd
			JOIN tb_stok_obat so ON(so.kode_obat=dd.kode_obat_res)
			WHERE EXTRACT(YEAR FROM dd.create_time)= YEAR(NOW())
			GROUP BY dd.kode_obat_res
			ORDER BY so.nama_obj
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function show_data($set){
		//order by dl.nama_obj
		$query = $this->db->query("
			SELECT dl.stok_awal as stok_awal, dl.rusak_ed as rusak_ed,
			dl.id AS id, dl.id_lplpo AS id_lplpo,dl.kode_obat AS kode_obat,
			oa.nama_obat AS nama_obat, oa.deskripsi AS sediaan,
			dl.terima AS terima, dl.sedia AS sedia, dl.pakai AS pakai,
			dl.stok_akhir AS stok_akhir, dl.stok_opt AS stok_opt,
			dl.permintaan AS permintaan, dl.dana AS dana_mahasiswa, dl.ket AS ket,
			dl.nama_obj as nama_obj,dl.pemberian as pemberian
			FROM tb_detail_lplpo dl
			JOIN ref_obat_all oa ON(oa.id_obat = dl.kode_obat)
			JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
			WHERE l.kode_kec =? AND l.kode_pusk = ? AND l.periode =?
			order by dl.nama_obj
			",array(
				$set['kec'],
				$set['nama_pusk'],
				$set['per'].'-00',
				)
			);
		//print_r($query);
		return $query->result();
		/*if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();	
		}*/
	}

	function eksis($data){
		$cek = $this->db->query("
			select * from tb_lplpo
			where `kode_pusk`=? and `periode`=?
			",
			array($data['nama_pusk'],
				$data['per'].'-00')
			);
		if ($cek->num_rows > 0){
			return true;
		}else{ return false; }
	}

	function getProfilPrint($key){
		$query=$this->db->query("
			SELECT p.NAMA_PUSKES AS nama_pusk, kec.KECDESC AS nama_kec, kab.CKabDescr AS nama_kab,
			prop.CPropDescr AS nama_prop
			FROM ref_puskesmas p
			JOIN ref_kecamatan kec ON (kec.KDKEC = p.KODE_KEC)
			JOIN ref_kabupaten kab ON (kab.CKabID=p.KODE_KAB)
			JOIN ref_propinsi prop ON (prop.CPropID=p.KODE_PROP)
			WHERE p.KD_PUSK='$key'
			");
		return $query->row_array();
	}
}
?>
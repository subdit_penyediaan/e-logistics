<?php
Class Ketersediaan_ukp4_model extends CI_Model {
	//sum setahun(*group by year where year now):bulan sekarang = rata2 penggunaan
	//ketersediaan=sisa stok : rata2 penggunaan
	//sisa stok diambil stok sisa pada bulan terakhir
	
	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan, 
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE so.flag=1
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}

	function getDataEksport(){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function show_data($set){
		$where = "";

		$tahun=$set['tahun'];
		$tahun_sbl=$tahun-1;
		//$where.= "AND p.tahun =$tahun";

		if($set['periode']==1){
			
			$bulan=2;
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-28"';
		}elseif ($set['periode']==2){
			$bulan=5;//desember-mei
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-31"';
			//AND dd.create_time BETWEEN "2013-12-01" AND "2014-5-31"';
		}elseif ($set['periode']==3){
			
			$bulan=8;//desember-agustus
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-30"';
		}elseif ($set['periode']==4){
			$bulan=11;//desember-november
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-30"';	
		}

		/*if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";		
		}

		if($set['kategori']!='all'){	
			$where .= "AND so.kategori='".$set['kategori']."'";	
		}*/
		$this->create_table($set);
		
		/*$query = $this->db->query("
			SELECT p.tahun,ru.id_ukp4,ru.nama_ukp4,p.jumlah AS perencanaan,
			IFNULL(SUM(DISTINCT(so.stok)),0) AS stok,IFNULL(SUM(DISTINCT(dd.pemberian)),0)AS pemberian
			FROM tb_perencanaan p
			JOIN ref_ukp4 ru ON (ru.id_ukp4=p.id_ukp4)
			LEFT JOIN tb_stok_obat so ON (so.id_ukp4=ru.id_ukp4)
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE so.flag=1 
				$where
			GROUP BY ru.id_ukp4
			ORDER BY ru.id_ukp4
			");*/
		$query = $this->db->query("
			SELECT p.id AS id,p.id_ukp4,ru.nama_ukp4,so.fisik_stok AS stok,IFNULL(p.jumlah,0) AS perencanaan,
			IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS pemberian
			FROM map_obat_ukp4 mou
			RIGHT JOIN tb_perencanaan p ON(p.id_ukp4=mou.id_ukp4)
			LEFT JOIN tb_detail_stok_opnam so ON(so.kode_obat=mou.id_obat)
			JOIN tb_stok_opnam sop ON(sop.id=so.id_so)
			JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=mou.id_obat)
			WHERE p.tahun=$tahun AND MONTH(sop.periode_stok_opnam)=$bulan 
				$where
			GROUP BY p.id_ukp4

			UNION 
			SELECT p2.id AS id,p2.id_ukp4,ru.nama_ukp4,IFNULL((p2.jumlah*0),0) AS stok,IFNULL(p2.jumlah,0) AS perencanaan,
			IFNULL((p2.jumlah*0),0) AS pemberian
			FROM tb_perencanaan p2
			JOIN ref_ukp4 ru ON (ru.id_ukp4=p2.id_ukp4)
			WHERE p2.tahun=$tahun AND p2.id_ukp4 NOT IN (SELECT p.id_ukp4
				FROM map_obat_ukp4 mou
				RIGHT JOIN tb_perencanaan p ON(p.id_ukp4=mou.id_ukp4)
				LEFT JOIN tb_detail_stok_opnam so ON(so.kode_obat=mou.id_obat)
				JOIN tb_stok_opnam sop ON(sop.id=so.id_so)
				JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
				LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=mou.id_obat)
				WHERE p.tahun=$tahun AND MONTH(sop.periode_stok_opnam)=$bulan 
					$where
				GROUP BY p.id_ukp4)
			ORDER BY id
			");

		return $query->result();
	}

	function create_table($set){
		$where = "";

		$ins=$this->db->query("select * from tb_institusi");
		$temp=$ins->row_array();
		if($temp['levelinstitusi']==2){
			$wil=$temp['id_prop'];
		}elseif($temp['levelinstitusi']==3){
			$wil=$temp['id_kab'];
		}

		$tahun=$set['tahun'];
		$tahun_sbl=$tahun-1;
		//$where.= "AND p.tahun =$tahun";

		if($set['periode']==1){
			$periode='B03';
			$bulan=2;
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-28"';
		}elseif ($set['periode']==2){
			$periode='B06';
			$bulan=5;//desember-mei
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-31"';
			//AND dd.create_time BETWEEN "2013-12-01" AND "2014-5-31"';
		}elseif ($set['periode']==3){
			$periode='B09';
			$bulan=8;//desember-agustus
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-30"';
		}elseif ($set['periode']==4){
			$periode='B12';
			$bulan=11;//desember-november
			$where.='AND dd.create_time BETWEEN "'.$tahun_sbl.'-12-01" AND "'.$tahun.'-'.$bulan.'-30"';	
		}
		$this->log_save($periode,$tahun,$wil);
		$namatabel='ketersediaanukp4_'.$periode.'_'.$tahun.'_'.$wil;
		$this->db->query("DROP TABLE IF EXISTS $namatabel");
		$create=$this->db->query("
			CREATE TABLE $namatabel AS

			SELECT p.id AS id,p.id_ukp4,ru.nama_ukp4,so.fisik_stok AS stok,IFNULL(p.jumlah,0) AS perencanaan,
			IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS pemberian,(IFNULL(SUM(DISTINCT(dd.pemberian)),0)+so.fisik_stok) AS jumlah,
			ROUND((((IFNULL(SUM(DISTINCT(dd.pemberian)),0)+so.fisik_stok)/IFNULL(p.jumlah,0))*100),2) AS ketersediaan
			FROM map_obat_ukp4 mou
			RIGHT JOIN tb_perencanaan p ON(p.id_ukp4=mou.id_ukp4)
			LEFT JOIN tb_detail_stok_opnam so ON(so.kode_obat=mou.id_obat)
			JOIN tb_stok_opnam sop ON(sop.id=so.id_so)
			JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=mou.id_obat)
			WHERE p.tahun=$tahun AND MONTH(sop.periode_stok_opnam)=$bulan 
				$where
			GROUP BY p.id_ukp4

			UNION 
			SELECT p2.id AS id,p2.id_ukp4,ru.nama_ukp4,IFNULL((p2.jumlah*0),0) AS stok,IFNULL(p2.jumlah,0) AS perencanaan,
			IFNULL((p2.jumlah*0),0) AS pemberian,IFNULL((p2.jumlah*0),0) AS jumlah,IFNULL((p2.jumlah*0),0) AS ketersediaan
			FROM tb_perencanaan p2
			JOIN ref_ukp4 ru ON (ru.id_ukp4=p2.id_ukp4)
			WHERE p2.tahun=$tahun AND p2.id_ukp4 NOT IN (SELECT p.id_ukp4
				FROM map_obat_ukp4 mou
				RIGHT JOIN tb_perencanaan p ON(p.id_ukp4=mou.id_ukp4)
				LEFT JOIN tb_detail_stok_opnam so ON(so.kode_obat=mou.id_obat)
				JOIN tb_stok_opnam sop ON(sop.id=so.id_so)
				JOIN ref_ukp4 ru ON(ru.id_ukp4=mou.id_ukp4)
				LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=mou.id_obat)
				WHERE p.tahun=$tahun AND MONTH(sop.periode_stok_opnam)=$bulan 
					$where
				GROUP BY p.id_ukp4)
			ORDER BY id
			");
		//return $query->result();
		/*if($create){
			echo "berhasil bikin tabel";
		}else{
			echo "gagal bikin tabel";
		}*/
	}

	function log_save($periode,$tahun,$wil)
    {
       date_default_timezone_set('Asia/Jakarta');

       $log['id'] = date('d-m-Y')."/".date('H:i:s');
       $log['periode'] = $periode ."/". $tahun;
       $log['cara_kirim'] = '<a href="#">online</a>';//$this->input->post('metode_kirim');
       //$log['tglsistem'] = $timenow;
       $log['status'] = "pending";
       $log['namatable'] = "ketersediaanukp4_".$periode."_".$tahun."_".$wil;
       $log['id_user'] = $this->session->userdata('id');
       $log['jenis_laporan'] = 4;
       $this->db->insert('log_sinkron', $log);
       //return $hasil;
    }

	function getKelUkp4(){
		$query=$this->db->query("
			SELECT  mof.id_ukp4 AS id_ukp4,ru.nama_ukp4 AS nama_ukp4,
				IFNULL(SUM(dd.pemberian),0) AS jumlah_penggunaan,SUM(DISTINCT(so.stok)) AS jumlah_stok, 
				MONTH(NOW()) AS cur_month
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			JOIN map_obat_ukp4 mof ON(mof.id_obat=dd.kode_obat_req)
			JOIN ref_ukp4 ru ON(ru.id_ukp4=mof.id_ukp4)
			WHERE so.flag=1
			GROUP BY mof.id_ukp4
			ORDER BY mof.id_ukp4
			");
		return $query->result();
	}

	function mk_perencanaan(){
		$query=$this->db->query("
			select *
			from ref_ukp4
			order by nama_ukp4
			");

		return $query->result();
	}

	function cek_data($set){
		if($set['periode']==1){
			$periode='B03';
		}elseif ($set['periode']==2){
			$periode='B06';
		}elseif ($set['periode']==3){
			$periode='B09';
		}elseif ($set['periode']==4){
			$periode='B12';
		}
		$periode_match=$periode.'/'.$set['tahun'];
		//print_r($periode_match);
		$query=$this->db->query("
			select * from log_sinkron where periode='$periode_match'
			");
		if($query->num_rows()>0){
			return $query->row_array();
		}else {return false;}
	}

	function show_exist($namatabel){
		$query=$this->db->query("
			select * from $namatabel
			");
		return $query->result();
	}	
}
?>
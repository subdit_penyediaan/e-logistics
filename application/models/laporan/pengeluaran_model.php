<?php
Class Pengeluaran_Model extends CI_Model {

	function getDataKecamatan(){
		//ambil data kabupaten dari profil user > $kab
		$datakab = $this->db->query("
			select id_kab
			from tb_institusi
			");
		$kab = $datakab->row_array();
		//print_r($kab);
		$kabupaten=$kab['id_kab'];
		$query = $this->db->query("
			select *
			from ref_kecamatan
			where KDKAB='$kabupaten'");
		return $query->result_array();
	}

	function GetComboPuskesmas($key){
		$sql = $this->db->query("
            SELECT KD_PUSK, NAMA_PUSKES FROM ref_puskesmas WHERE KODE_KEC='$key' ORDER BY NAMA_PUSKES"
        );
        return $sql->result_array();
	}

	function countAllData(){
		return $this->db->count_all("tb_detail_faktur");
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		$query=$this->db->query("
			SELECT DISTINCT so.no_batch AS no_batch,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
			so.harga AS harga,(IFNULL(SUM(dd.pemberian),0) * so.harga) AS nilai,oa.deskripsi,oa.org_pembuat AS pabrik,p.dana,
			p.tahun_anggaran,so.id_stok
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN ref_obat_all oa ON oa.id_obat = so.kode_obat
			JOIN tb_penerimaan p ON p.id = so.id_faktur
			WHERE 1=1 OR so.kode_obat NOT IN (SELECT kode_obat_res FROM tb_detail_distribusi)
				AND EXTRACT(YEAR FROM dd.create_time)=EXTRACT(YEAR FROM NOW())
			GROUP BY so.id_stok
			ORDER BY so.nama_obj

			");
		return $query->result();
		//mau secara general atau dirinci tiap no.batch
		//klo general 1 harga, klo dirinci harga lama sm baru bisa dilacak
	}

	function getDataBy($limit,$start,$key){
		$this->db->limit($limit, $start);
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_pengeluaran p ON(p.no_faktur=df.no_faktur)
			where p.dana ='$key'
			order by df.kode_obat
			");
		return $query->result();
	}

	function getDataEksport(){
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obat AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_pengeluaran p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function GetListPbf($key){
	    $query = $this->db->query("select * from ref_pbf where SuplierName like '%$key%'");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode'	=>$key->kode,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->SuplierName.', '.$key->alamat
	    	);
	    }
	    return $r;
	}

	function show_data($set){
		$where = "";
		$join="";
		$or="OR so.kode_obat NOT IN (SELECT kode_obat_res FROM tb_detail_distribusi)
				AND EXTRACT(YEAR FROM dd.create_time)=EXTRACT(YEAR FROM NOW())";

		if(!(empty($set['awal']))){
			$where .= "AND (dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$or ="";
		}

		if(!(empty($set['kode_kab']))){
			$join .= "JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)";
			$where .= "AND d.kode_up='".$set['kode_kab']."'";
			$or ="";
			$set['pusk']='all';
		}

		if($set['dana']!='all'){
			$where .= "AND p.dana='".$set['dana']."'";
		}

		if($set['tahun_anggaran']!='all'){
			$where .= "AND p.tahun_anggaran='".$set['tahun_anggaran']."'";
		}

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
		}

		if($set['pusk']!='all'){
			//if()
			$where .= "AND l.kode_pusk='".$set['pusk']."' OR d.kode_up ='".$set['pusk']."'";
			$join .= "LEFT JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)";
			$or ="";
		}

		if($set['unit_eksternal']!='all'){
			//if()
			$where .= "AND d.kode_up ='".$set['unit_eksternal']."'";
			$join .= "LEFT JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)";
			$or ="";
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$or ="";
		}

		$query = $this->db->query("
			SELECT so.no_batch as no_batch,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
			so.harga as harga,(IFNULL(SUM(dd.pemberian),0) * so.harga) AS nilai,oa.deskripsi,oa.org_pembuat AS pabrik,p.dana,
			p.tahun_anggaran,so.id_stok
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			LEFT JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			LEFT JOIN ref_obat_all oa ON oa.id_obat = so.kode_obat
			JOIN tb_penerimaan p ON p.id = so.id_faktur
				$join
			WHERE 1=1 $or
				$where
			GROUP BY so.id_stok
			ORDER BY so.nama_obj
			");
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}

	function getDataDetail($id_stok,$awal="",$akhir=""){
		$where="";

		if($this->session->userdata('pusk')!='all'){
			$kode_pusk=$this->session->userdata('pusk');
			$where .= "AND l.kode_pusk = '".$kode_pusk."'";
			//$this->session->unset_userdata('pusk');
		}

		if($awal!=""){
			$where .= "AND dd.create_time BETWEEN '".$awal."' AND '".$akhir."'";
		}

		$query=$this->db->query("
			SELECT pusk.NAMA_PUSKES AS nama_pusk,DATE_FORMAT(l.periode,'%M %Y') AS periode,
			dd.create_time AS tanggal_pemberian, dd.pemberian AS pemberian,dd.id, l.no_dok
			FROM tb_detail_distribusi dd
			JOIN tb_lplpo l ON(l.id=dd.id_lplpo)
			LEFT JOIN ref_puskesmas pusk ON(pusk.KD_PUSK=l.kode_pusk)
			WHERE dd.id_stok='$id_stok'
				$where
			");

		return $query->result();
	}

	function getDataDetailCito($id_stok,$awal="",$akhir=""){
		$where="";
		$join="";

		if(($this->session->userdata('pusk')!='')&&($this->session->userdata('pusk')!='all')){
			$kode_pusk=$this->session->userdata('pusk');
			//$join .=
			$where .= "AND d.kode_up = '".$kode_pusk."'";
			//$this->session->unset_userdata('pusk');
		}

		if($awal!=""){
			$where .= "AND d.tanggal_trans BETWEEN '".$awal."' AND '".$akhir."'";
		}

		$query=$this->db->query("
			SELECT d.unit_eks AS unit_eks, DATE_FORMAT(d.tanggal_trans,'%M %Y') AS periode,
			dd.pemberian AS pemberian, d.tanggal_trans AS tanggal_pemberian,dd.id, d.no_dok
			FROM tb_detail_distribusi dd
			JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			WHERE dd.id_stok='$id_stok'
				$where
			");
		return $query->result();
	}

	function getInfoObat($id_stok){
		$query=$this->db->query("
			SELECT so.no_batch,so.kode_obat as kode_obat,oa.nama_obat AS nama_obat, oa.kekuatan AS kekuatan, oa.deskripsi AS sediaan
			FROM tb_stok_obat so
			JOIN ref_obat_all oa ON(oa.id_obat=so.kode_obat)
			WHERE so.id_stok='$id_stok'
			");
		return $query->row_array();
	}

	function getDataInstitusi(){
		$query=$this->db->query("
			select * from tb_institusi
			");
		return $query->row_array();
	}
	function getPropinsi(){
		$query = $this->db->query("
			select *
			from ref_propinsi
			");
		return $query->result_array();
	}

	function GetComboKab($key){
		$sql = $this->db->query("
            SELECT CKabID, CKabDescr FROM ref_kabupaten WHERE CPropID='$key' ORDER BY CKabDescr"
        );
        return $sql->result_array();
	}

	function getKabupaten($id_prop){
		$query = $this->db->query("
			select *
			from ref_kabupaten
			where CPropID='$id_prop'
			");
		return $query->result_array();
	}

	function show_data2($set){
		$where = '';
		$join = "";
		$select = ", '' as id_obatprogram";

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}
		if(!(empty($set['awal']))){
			//$where .= "AND (dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where .= "AND d.tanggal_trans BETWEEN '".$set['awal']."' AND '".$set['akhir']."' ";
		}

		if(($set['kode_kab']!='')||($set['kode_prov']!='')){
			if(($set['kode_kab']!='all')&&($set['kode_kab']!='0')){
				//$join .= "JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)";
				$where .= "AND d.kode_up='".$set['kode_kab']."'";
			}elseif(($set['kode_kab']=='0')&&($set['kode_prov']!='all')){
				$where .= "AND d.kode_up='".$set['kode_prov']."'";
			}
		}

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
		}

		if(!(empty($set['program']))){
			if ($set['program']!='all'){
				$select = ", mip.id_obatprogram";
				$join = "JOIN ref_obatgenerik c ON c.id = oa.id_generik
								LEFT JOIN map_inn_program mip ON mip.id_inn = c.id";
				$where .= "AND mip.id_obatprogram ='".$set['program']."'";
			}
		}

		$sql = "
			SELECT so.no_batch AS no_batch,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,dd.id_stok,
			so.harga AS harga,(IFNULL(SUM(dd.pemberian),0) * so.harga) AS nilai,oa.deskripsi,oa.org_pembuat AS pabrik,p.dana,p.tahun_anggaran
			$select
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			LEFT JOIN tb_distribusi d ON(d.id_distribusi=dd.id_distribusi)
			JOIN ref_obat_all oa ON oa.id_obat = so.kode_obat
			JOIN tb_penerimaan p ON p.id = so.id_faktur
			$join
			WHERE 1=1
				$where
			GROUP BY so.id_stok
			ORDER BY so.nama_obj
			";
		$query = $this->db->query($sql);
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}
}
?>

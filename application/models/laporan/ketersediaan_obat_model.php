<?php
Class Ketersediaan_Obat_Model extends CI_Model {
	//sum setahun(*group by year where year now):bulan sekarang = rata2 penggunaan
	//ketersediaan=sisa stok : rata2 penggunaan
	//sisa stok diambil stok sisa pada bulan terakhir

	function countAllData(){
		return $this->db->count_all("tb_stok_obat");
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		//$query=$this->db->get("tb_detail_faktur");
		$tahun=date('Y');
		$bulan=date('m');
		$hari=date('d');
		$current=date('Y-m-d');
		if(($bulan-3)>0){
			$default=$tahun.'-'.($bulan-3).'-'.$hari;
		}elseif(($bulan-3)==0){
			//desember tahun sebelumnya
			$default=($tahun-1).'-12-'.$hari;
		}elseif(($bulan-3)==-1){
			//november tahun sebelumnya
			$default=($tahun-1).'-11-'.$hari;
		}else{
			//oktober tahun sebelumnya
			$default=($tahun-1).'-10-'.$hari;
		}


		$query=$this->db->query("
			SELECT DISTINCT so.kode_obat AS kode_obat,IFNULL(SUM(DISTINCT(dd.pemberian)),0) AS jumlah_penggunaan,
			SUM(DISTINCT(so.stok)) AS jumlah_stok, MONTH(NOW()) AS cur_month, so.nama_obj AS nama_obj
			FROM tb_stok_obat so
			LEFT JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			WHERE so.flag=1 AND dd.create_time BETWEEN '$default' AND '$current'
			GROUP BY so.no_batch
			ORDER BY so.nama_obj
			");
		return $query->result();
	}

	function getDataEksport(){
		$query=$this->db->query("
			SELECT df.kode_obat AS kode_obat,oa.nama_obj AS nama_obat,oa.deskripsi AS sediaan,
			p.tanggal AS tanggal_terima,df.expired AS expired_date, df.jumlah_kec AS jumlah,
			p.dana AS dana, df.harga AS harga
			FROM tb_detail_faktur df
			JOIN ref_obat_all oa ON(oa.id_obat=df.kode_obat)
			JOIN tb_ketersediaan_obat p ON(p.no_faktur=df.no_faktur)
			order by df.kode_obat
			");
		return $query->result_array();
	}

	function show_data($set)
	{
		$where = $where2 = $where3 = $where4 = "";
		$join = "";

		if(!(empty($set['awal']))){
			//$where .= "AND (p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where2 .= "AND (dd.create_time BETWEEN '2013-01-01' AND '".$set['akhir']."') ";
			$where3 .= "AND (pem.cur_date BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
			$where4 .= "AND (dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."') ";
		}
		if(isset($set['dana'])){
			if($set['dana']!='all'){
				$where .= "AND p.dana='".$set['dana']."'";
				$where2 .= "AND p.dana='".$set['dana']."'";
				$where3 .= "AND p.dana='".$set['dana']."'";
				$where4 .= "AND p.dana='".$set['dana']."'";
			}
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where2 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where3 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$where4 .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
		}

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
			$where2 .= "AND so.kategori='".$set['kategori']."'";
			$where3 .= "AND so.kategori='".$set['kategori']."'";
			$where4 .= "AND so.kategori='".$set['kategori']."'";
		}

		if($set['jprogram']!='all'){
			$join .= "JOIN map_inn_program rog ON rog.id_inn = so.kode_generik";
			$where .= "AND rog.id_obatprogram = ".$set['jprogram'];
			$where2 .= "AND rog.id_obatprogram = ".$set['jprogram'];
			$where3 .= "AND rog.id_obatprogram = ".$set['jprogram'];
			$where4 .= "AND rog.id_obatprogram = ".$set['jprogram'];
		}

		//print_r($where);die;
		$sql = "
			SELECT kode_obat,nama_obj, SUM(saldo_persediaan) AS saldo_persediaan,SUM(pengeluaran) AS pengeluaran,
			SUM(jml_rusak) AS jml_rusak, (SUM(saldo_persediaan)- (SUM(pengeluaran) + SUM(jml_rusak))) AS stok,
			harga,expired, sumber_dana,no_batch,tahun_anggaran, pabrik,sediaan,id_stok,
			sum(rerata_pengeluaran) as rerata_pengeluaran
			FROM (
			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, df.jumlah_kec AS saldo_persediaan,
			0 AS pengeluaran,0 AS jml_rusak,
			so.stok AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
			0 AS jml_rusak,
			0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where2
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			0 AS pengeluaran,
			0 AS jml_rusak,
			0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok,
			IFNULL(SUM(dd.pemberian),0) AS rerata_pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where4
			GROUP BY so.id_stok

			UNION

			SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
			0 AS pengeluaran,IFNULL(CASE dp.status WHEN 0 THEN dp.jumlah END,0) AS jml_rusak, 0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
			FROM tb_stok_obat so
			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
			LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
			JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
				$join
			WHERE 1=1 $where3
			GROUP BY so.id_stok) ex

			GROUP BY id_stok
			ORDER BY nama_obj
			";

		$query = $this->db->query($sql);
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}

	function getKelFornas($set){
		$where="";

		if(!(empty($set['nama']))){
			$where .= "AND rf.nama_obat LIKE '%".$set['nama']."%'";
		}

		$query=$this->db->query("
			SELECT mof.id_fornas AS id_fornas,rf.nama_obat AS nama_fornas,mfp.nama_obat AS parent,
			SUM(so.stok)AS jumlah_stok,
			MONTH(NOW()) AS cur_month
			FROM map_obat_fornas mof
			JOIN tb_stok_obat so ON(so.kode_obat=mof.id_obat)
			JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			JOIN map_fornas_parent mfp ON(mfp.id_fornas=rf.parent)
			WHERE so.flag=1
				$where
			GROUP BY mof.id_fornas
			ORDER BY mof.id_fornas
			");
		return $query->result();
	}

	function getfornasout($set){
		$where="";

		if(!(empty($set['nama']))){
			$where .= "AND rf.nama_obat LIKE '%".$set['nama']."%'";
		}

		$query=$this->db->query("
			SELECT mof.id_fornas AS id_fornas,IFNULL(SUM(dd.pemberian),0)AS jumlah_penggunaan
			FROM map_obat_fornas mof
			JOIN tb_stok_obat so ON (so.kode_obat=mof.id_obat)
			JOIN tb_detail_distribusi dd ON(dd.no_batch=so.no_batch)
			WHERE dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
				$where
			GROUP BY mof.id_fornas
			");
		return $query->result();
	}

	function getKelUkp4(){
		$query=$this->db->query("
			SELECT  mof.id_ukp4 AS id_ukp4,ru.nama_ukp4 AS nama_ukp4,
				IFNULL(SUM(dd.pemberian),0) AS jumlah_penggunaan,SUM(DISTINCT(so.stok)) AS jumlah_stok,
				MONTH(NOW()) AS cur_month
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			JOIN map_obat_ukp4 mof ON(mof.id_obat=dd.kode_obat_req)
			JOIN ref_ukp4 ru ON(ru.id_ukp4=mof.id_ukp4)
			WHERE so.flag=1
			GROUP BY mof.id_ukp4
			ORDER BY mof.id_ukp4
			");
		return $query->result();
	}

	function get_obat_generik($set){
		$where="";

		if(!(empty($set['nama']))){
			$where .= "AND hg.nama_objek LIKE '%".$set['nama']."%'";
		}

		$query=$this->db->query("
			SELECT  hg.id_hlp,hg.nama_objek AS nama_obj,IFNULL(SUM(dd.pemberian),0) AS jumlah_penggunaan,SUM(so.stok) AS jumlah_stok,
				MONTH(NOW()) AS cur_month
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
			JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)
			WHERE so.flag=1
				$where
			GROUP BY so.kode_generik
			ORDER BY hg.nama_objek
			");
		return $query->result();
	}

	function get_pengeluaran_generik($set){
		$where="";
		$join="";

		if(!(empty($set['nama']))){
			$where .= "AND hg.nama_objek LIKE '%".$set['nama']."%'";
			$join .= "JOIN hlp_generik hg ON(hg.id_hlp=so.kode_generik)";
		}

		$query=$this->db->query("
			SELECT so.kode_generik,dd.pemberian AS jumlah_penggunaan
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
			$join
			WHERE so.flag=1
				AND dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
				$where
			GROUP BY so.kode_generik
			");
		return $query->result();
	}

	function get_pengeluaran($set){
		$where = "";
		$join ="";

		if(!(empty($set['awal']))){
			//AND dd.create_time BETWEEN '2014-01-25' AND '2014-07-30'
			$where .= "AND dd.create_time BETWEEN '".$set['awal']."' AND '".$set['akhir']."'";
			//WHERE p.tanggal BETWEEN '".$set['awal']."' AND '".$set['akhir']."'
		}

		// if($set['dana']!='all'){
		// 		$where .= "AND p.dana='".$set['dana']."'";
		// 		$join .= "JOIN tb_penerimaan p ON(p.no_faktur=so.no_faktur)";
		// }

		if($set['kategori']!='all'){
			$where .= "AND so.kategori='".$set['kategori']."'";
		}

		if(!(empty($set['nama']))){
			$where .= "AND so.nama_obj LIKE '%".$set['nama']."%'";
			$or ="";
		}

		if($set['jprogram']!='all'){
			$join .= "JOIN ref_obatgenerik rog ON rog.id = so.kode_generik";
			$where .= "AND rog.id_obatprogram = ".$set['jprogram'];
		}

		$sql = "
			SELECT so.no_batch,SUM(dd.pemberian) AS jumlah_penggunaan
			FROM tb_stok_obat so
			JOIN tb_detail_distribusi dd ON(dd.kode_obat_res=so.kode_obat)
				$join
			WHERE so.flag=1
				$where
			GROUP BY so.no_batch
			";
		$query=$this->db->query($sql);

		//print_r($sql);//die();
		if(!empty($set['eksport'])){
			return $query->result_array();
		}else{
			return $query->result();
		}
	}
}
?>

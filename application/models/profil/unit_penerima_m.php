<?php
	class Unit_penerima_m extends CI_Model{
		function construct(){
			parent::__construct();
		}

		function input_data_m($data){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}
			$data['desa'] = substr($data['kode_up'],1);
			$kode_prop = substr($kode_kab, 0,2);
			$time=date('Y-m-d H:i:s');
			$id_user=$this->session->userdata('id');

			$sql = $this->db->query("
				INSERT INTO `ref_puskesmas` (
						`KODE_KEC`,
						`telp`,
						`KODE_DESA`,
						`no_fax`,
						`KD_PUSK`,
						`JENIS_PUSK`,
						`poned`,
						`ALAMAT_PUSKESMAS`,
						`dtpk`,
						`email`,
						`ex`,
						`ye`,
						`create_time`,
						`create_by`,
						`NAMA_PUSKES`,
						`KODE_KAB`,`KODE_PROP`,
						`status_update`,`buffer`,`periode_distribusi`
				) VALUES (
					?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,1,?,?
				)",
				array(
					$data['kode_kec'],
					$data['telp'],
					$data['desa'],
					$data['fax'],
					$data['kode_up'],
					$data['jenis_up'],
					$data['poned'],
					$data['alamat_up'],
					$data['dtpk'],
					$data['email'],
					$data['long'],
					$data['lat'],
					$time,$id_user,
					$data['nama_puskes'],
					$kode_kab,$kode_prop,$data['buffer'],$data['periode_distribusi']
				)
			);
			//return $sql;
		}

		function getDataPuskesmas($limit,$start){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}

			$this->db->limit($limit, $start);
			$this->db->where("KODE_KAB",$kode_kab);
			$this->db->order_by("NAMA_PUSKES");
			$query=$this->db->get("ref_puskesmas");
			//$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
			return $query->result();
		}

		function countAllPuskesmas(){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}

			//return $this->db->count_all("ref_puskesmas");
			$this->db->where('KODE_KAB',$kode_kab);
			$this->db->from('ref_puskesmas');

			return $this->db->count_all_results();
		}

		function countdatakecamatan(){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}// else kode provinsi

			//return $this->db->count_all("ref_puskesmas");
			$this->db->where('KDKAB',$kode_kab);
			$this->db->from('ref_kecamatan');

			return $this->db->count_all_results();
		}

		function getDataUnitEks(){
			$query = $this->db->query("select * from ref_puskesmas where KODE_DESA like '006%'");

			return $query->result();
		}

		function getDataKecamatan(){
			//ambil data kabupaten dari profil user > $kab
			$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			$query = $this->db->query("
				select *
				from ref_kecamatan
				where KDKAB='$kabupaten'");
			return $query->result_array();
		}

		function getDataVillages(){
			//ambil data kabupaten dari profil user > $kab
			$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			$query = $this->db->query("
				select *
				from ref_villages
				where code_sub_district like '$kabupaten%'");
			return $query->result_array();
		}

		function GetComboVillage($key){
			$sql = $this->db->query("
	            SELECT code, name FROM ref_villages WHERE code_sub_district ='$key' ORDER BY name"
	        );
	        return $sql->result_array();
		}

		function getAddDataKecamatan(){
			//ambil data kabupaten dari profil user > $kab
			$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			$query = $this->db->query("
				select *
				from ref_kecamatan
				where KDKAB='$kabupaten'");
			return $query->result();
		}

		function input_data_kecamatan_m($data){
			//ambil kode kecamatan lama + 1
			//ambil kode kabupaten di tb_institusi
			$sql = $this->db->query("
				INSERT INTO `ref_kecamatan` (
						`KODE_KEC`,
						`KECDESC`,
						`KDKAB`
				) VALUES (
					?,?,?
				)",
				array(
					$kode_kec,
					$data['nama_kec'],
					$kode_kab
				)
			);
			//return $sql;
		}

	function getDataDetailKecamatan($key){
		$query=$this->db->query("
			select * from ref_kecamatan where KDKEC = $key
			");
		return $query->row_array();
	}

	function getDataKelurahan($key){

			$query = $this->db->query("
				select *
				from ref_villages
				where code_sub_district='$key'
				order by code
				");
			return $query->result();
		}

	function inputkecamatan($data){
		$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			//echo $kabupaten."<br>";
		// $kode_kec =  $this->db->query("
		// 	select KDKEC from ref_kecamatan
		// 	where KDKAB = $kabupaten
		// 	order by KDKEC desc
		// 	limit 1
		// 	");
		// $kode = $kode_kec->row_array();
		// $kode_baru=$kode['KDKEC']+1;
		//echo $kode_baru."<br>";
		$query = $this->db->query("
			INSERT INTO `ref_kecamatan` (
						`KDKEC`,
						`KECDESC`,
						`KDKAB`,
						`status_update`
				) VALUES (
					?,?,?,1
				)",
				array(
					$data['kode'],
					$data['nama_kec'],
					$kabupaten
				)
		);
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_kecamatan
			WHERE
				KDKEC IN ($kode)"
			);
	}

	function deleteDataPusk($kode){
		$this->db->query(
			"DELETE FROM
				ref_puskesmas
			WHERE
				KD_PUSK IN ('$kode')"
			);
	}

	function inputkelurahan($data){

		$kode_kel =  $this->db->query("
			select code from ref_villages
			where code_sub_district = ?
			order by code desc
			limit 1
			",array($data['kode_kec'])
			);
		//$kode = $kode_kel->row_array();
		if($kode_kel->num_rows() > 0){
			$kode = $kode_kel->row_array();
			$kode_baru=$kode['code']+1;
			echo $kode_baru;
		}else{
			$kode_baru=$data['kode_kec'].'001';
			echo $kode_baru;
		}

		//echo $kode_baru."<br>";
		$query = $this->db->query("
			INSERT INTO `ref_villages` (
						`code_sub_district`,
						`code`,
						`name`,
						`status_update`
				) VALUES (
					?,?,?,1
				)",
				array(
					$data['kode_kec'],
					$kode_baru,
					$data['nama_kel']
				)
		);
	}

	function deleteDataKelurahan($kode){
		$this->db->query(
			"DELETE FROM
				ref_villages
			WHERE
				code IN ($kode)"
			);
	}

	function GetInfoPusk($kodepusk){
		$query=$this->db->query("
			SELECT pusk.KD_PUSK AS kode_pusk,pusk.NAMA_PUSKES AS nama_pusk,pusk.KODE_DESA AS kode_desa,
			pusk.KODE_KEC AS kode_kec, pusk.ALAMAT_PUSKESMAS AS alamat_pusk,
			pusk.ex AS longi, pusk.ye AS lati,
			pusk.JENIS_PUSK AS jenis_pusk, pusk.telp AS telp, pusk.no_fax AS fax,pusk.poned AS poned,
			pusk.dtpk AS dtpk, pusk.email AS email,kec.KECDESC AS nama_kec,vilg.name AS nama_desa,buffer,periode_distribusi
			FROM ref_puskesmas pusk
			LEFT JOIN ref_kecamatan kec ON(kec.KDKEC=pusk.KODE_KEC)
			LEFT JOIN ref_villages vilg ON(vilg.code=pusk.KODE_DESA)
			WHERE pusk.KD_PUSK='$kodepusk'
			");
		return $query->row_array();
	}

	function update_data_m($data){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}

			$time=date('Y-m-d H:i:s A');
			$id_user=$this->session->userdata('id');

			$sql = $this->db->query("
				UPDATE `ref_puskesmas`
				SET
						`KD_PUSK`=?,
						`KODE_KEC`=?,
						`telp`=?,
						`no_fax`=?,
						`JENIS_PUSK`=?,
						`poned`=?,
						`ALAMAT_PUSKESMAS`=?,
						`dtpk`=?,
						`email`=?,
						`ex`=?,
						`ye`=?,
						`update_time`=?,
						`create_by`=?,
						`NAMA_PUSKES`=?,
						`KODE_KAB`=?,
						`status_update`=1,
						`buffer`=?,
						`periode_distribusi`=?
					WHERE
					`KD_PUSK`=?
				",
				array(
					$data['kode_upnew'],
					$data['kode_kec'],
					$data['telp'],
					$data['fax'],
					$data['jenis_up'],
					$data['poned'],
					$data['alamat_up'],
					$data['dtpk'],
					$data['email'],
					$data['long'],
					$data['lat'],
					$time,$id_user,
					$data['nama_puskes'],
					$kode_kab,
					round($data['buffer']/100,2),
					$data['periode_distribusi'],
					$data['kode_up']
				)
			);
			//return $sql;
		}

	function getDataInstitusi(){
		$query=$this->db->query("
			select * from tb_institusi
			");
		return $query->row_array();
	}

	function getResult($key){
		$query=$this->db->query("
			select *
			from ref_puskesmas
			where NAMA_PUSKES like '%$key%'
			");
		foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'value'	=>$key->NAMA_PUSKES,
	    		'KD_PUSK'	=>$key->KD_PUSK,
	    		'KODE_KEC'	=>$key->KODE_KEC,
	    		'ALAMAT_PUSKESMAS'	=>$key->ALAMAT_PUSKESMAS,
	    		'telp'	=>$key->telp,
	    		'longitud'	=>$key->ex,
	    		'latitud'	=>$key->ye,
	    		'email'	=>$key->email,
	    		'no_fax'	=>$key->no_fax,
	    		'poned'	=>$key->poned
	    	);
	    }
	    return $r;
		//return $query->result();
	}
}
?>

<?php
Class Setting_Institusi_Model extends CI_Model {
	function getDataInstitusi(){
		$query = $this->db->query("
			SELECT ti.*,rk.CKabDescr AS nama_kab,rp.CPropDescr AS nama_prop
			FROM tb_institusi ti
			LEFT JOIN ref_kabupaten rk ON(rk.CKabID=ti.id_kab)
			LEFT JOIN ref_propinsi rp ON(rp.CPropID=ti.id_prop)
			");
		return $query->row_array();
	}

	function getKabupaten($id_prop = ''){
		$where = '';
		if($id_prop!=''){
			$where .='where CPropID = "'.$id_prop.'"';
		}
		$query = $this->db->query("
			select *
			from ref_kabupaten
				$where
			");
		return $query->result_array();
	}

	function getPropinsi(){
		$query = $this->db->query("
			select *
			from ref_propinsi
			");
		return $query->result_array();
	}

	function input_data_m($data){
		$time = date('Y-m-d H:i:s');
		$user = $this->session->userdata('id');
		$sql = $this->db->query("
			update tb_institusi
			set
				levelinstitusi=?,
				id_kab=?,
				id_prop=?,
				nama_fasilitas=?,
				alamat=?,
				longitude=?,
				latitude=?,
				notelp=?,
				pengelola=?,
				struktur_org=?,
				pj=?,
				nama_pj=?,
				jabatan_pj=?,
				cp_pj=?,
				url_bank_data1=?,
				username=?,
				password=?,
				update_time=?,
				user_id=?,
				url_bank_data2=?,
				url_rko=?,
				username_rko=?,
				password_rko=?
			",
			array(
				$data['level'],
				$data['levelkab'],
				$data['levelprop'],
				$data['nf'],
				$data['almt'],
				$data['longi'],
				$data['lat'],
				$data['notelp'],
				$data['pengelola'],
				$data['struktur_org'],
				$data['bag_pj'],
				$data['pj'],
				$data['jab_pj'],
				$data['cp_pj'],
				$data['webservice1'],
				$data['username'],
				$data['password'], $time, $user,				
				$data['webservice2'],
				$data['url_rko'],
				$data['username_rko'],
				$data['password_rko']
			)
		);
		return $sql;	
	}

	function GetLocation($id_kab){
		$query=$this->db->query("
			SELECT ex,ye
			FROM ref_kabupaten
			WHERE CKabID=$id_kab
			");
		return $query->row_array();
	}

	function change_group(){
		$this->db->query("
			UPDATE sys_menu
			SET status='1'
			WHERE id IN (8,10,32,38)
			");
		$this->db->query("
			UPDATE sys_menu
			SET status='0'
			WHERE id IN (7,9,31,33,13,39)
			");
	}

	function reset_menu(){
		$this->db->query("
			UPDATE sys_menu
			SET status='0'
			WHERE id IN (8,10,32,38)
			");
		$this->db->query("
			UPDATE sys_menu
			SET status='1'
			WHERE id IN (7,9,31,33,13,39)
			");	
	}

	function getNamaProp($kode){
		$query=$this->db->query("
			select *
			from ref_propinsi
			where CPropID='$kode'
			");
		return $query->row_array();
	}

	function getNamaKab($kode){
		$query=$this->db->query("
			select *
			from ref_kabupaten
			where CKabID='$kode'
			");
		return $query->row_array();
	}
}
?>
<?php
	class Unit_penerima_pusat_m extends CI_Model{
		function construct(){
			parent::__construct();
		}

		function input_data_m($data){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}

			$time=date('Y-m-d H:i:s A');
			$id_user=$this->session->userdata('id');

			$sql = $this->db->query("
				INSERT INTO `ref_puskesmas` (
						`KODE_KEC`,
						`telp`,
						`KODE_DESA`,
						`no_fax`,
						`KD_PUSK`,
						`JENIS_PUSK`,
						`poned`,
						`ALAMAT_PUSKESMAS`,
						`dtpk`,
						`email`,
						`ex`,
						`ye`,
						`create_time`,
						`create_by`,
						`NAMA_PUSKES`,
						`KODE_KAB`
				) VALUES (
					?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
				)",
				array(
					$data['kode_kec'],
					$data['telp'],
					$data['desa'],
					$data['fax'],
					$data['kode_up'],
					$data['jenis_up'],
					$data['poned'],
					$data['alamat_up'],
					$data['dtpk'],
					$data['email'],
					$data['long'],
					$data['lat'],
					$time,$id_user,
					$data['nama_puskes'],
					$kode_kab
				)
			);
			//return $sql;	
		}

		function getDataPropinsi($limit,$start){
			$this->db->limit($limit, $start);
			//$this->db->where("KODE_KAB",$kode_kab);
			$this->db->order_by("CPropDescr");
			$query=$this->db->get("ref_propinsi");
			//$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
			return $query->result();
		}

		function countAllPropinsi(){
			return $this->db->count_all("ref_propinsi");
		}

		function countAllKab($id_prop){
			$this->db->where('CPropID',$id_prop);
			$this->db->from('ref_kabupaten');

			return $this->db->count_all_results();
		}

		
		function getDataUnitEks(){
			$query = $this->db->query("select * from ref_puskesmas where KODE_DESA like '006%'");

			return $query->result();
		}

		function getDataKecamatan(){
			//ambil data kabupaten dari profil user > $kab
			$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			$query = $this->db->query("
				select *
				from ref_kecamatan
				where KDKAB='$kabupaten'");
			return $query->result_array();
		}

		function GetComboVillage($key){
			$sql = $this->db->query("
	            SELECT code, name FROM ref_villages WHERE code_sub_district ='$key' ORDER BY name"
	        );
	        return $sql->result_array();
		}

		function getAddDataKecamatan(){
			//ambil data kabupaten dari profil user > $kab
			$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			$query = $this->db->query("
				select *
				from ref_kecamatan
				where KDKAB='$kabupaten'");
			return $query->result();
		}

		function input_data_kecamatan_m($data){
			//ambil kode kecamatan lama + 1
			//ambil kode kabupaten di tb_institusi
			$sql = $this->db->query("
				INSERT INTO `ref_kecamatan` (
						`KODE_KEC`,
						`KECDESC`,
						`KDKAB`
				) VALUES (
					?,?,?
				)",
				array(
					$kode_kec,
					$data['nama_kec'],
					$kode_kab
				)
			);
			//return $sql;	
		}

	function getDataDetailKecamatan($key){
		$query=$this->db->query("
			select * from ref_kecamatan where KDKEC = $key
			");
		return $query->row_array();
	}

	//function getDataKab($limit,$start,$id_prop){
	function getDataKab($id_prop){	
			//$this->db->limit($limit, $start);
			$this->db->where("CPropID",$id_prop);
			$this->db->order_by("CKabDescr");
			$query=$this->db->get("ref_kabupaten");
			//$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
			return $query->result();
		}
	function inputkecamatan($nama_kec){
		$datakab = $this->db->query("
				select id_kab
				from tb_institusi
				");
			$kab = $datakab->row_array();
			//print_r($kab);
			$kabupaten=$kab['id_kab'];
			//echo $kabupaten."<br>";
		$kode_kec =  $this->db->query("
			select KDKEC from ref_kecamatan 
			where KDKAB = $kabupaten
			order by KDKEC desc
			limit 1
			");
		$kode = $kode_kec->row_array();
		$kode_baru=$kode['KDKEC']+1;
		//echo $kode_baru."<br>";
		$query = $this->db->query("
			INSERT INTO `ref_kecamatan` (
						`KDKEC`,
						`KECDESC`,
						`KDKAB`
				) VALUES (
					?,?,?
				)",
				array(
					$kode_baru,
					$nama_kec,
					$kabupaten
				)
		);
	}

	function deleteDataKab($kode){
		$this->db->query(
			"DELETE FROM
				ref_kabupaten
			WHERE 
				CKabID IN ($kode)"
			);
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_kecamatan
			WHERE 
				KDKEC IN ($kode)"
			);
	}

	function deleteDataPusk($kode){
		$this->db->query(
			"DELETE FROM
				ref_puskesmas
			WHERE 
				KD_PUSK IN ('$kode')"
			);
	}

	function inputkelurahan($data){

		$kode_kel =  $this->db->query("
			select code from ref_villages 
			where code_sub_district = ?
			order by code desc
			limit 1
			",array($data['kode_kec'])
			);
		//$kode = $kode_kel->row_array();
		if($kode_kel->num_rows() > 0){
			$kode = $kode_kel->row_array();
			$kode_baru=$kode['code']+1;
			echo $kode_baru;	
		}else{
			$kode_baru=$data['kode_kec'].'001';			
			echo $kode_baru;
		}
		
		//echo $kode_baru."<br>";
		$query = $this->db->query("
			INSERT INTO `ref_villages` (
						`code_sub_district`,
						`code`,
						`name`
				) VALUES (
					?,?,?
				)",
				array(
					$data['kode_kec'],
					$kode_baru,
					$data['nama_kel']
				)
		);
	}

	function deleteDataKelurahan($kode){
		$this->db->query(
			"DELETE FROM
				ref_villages
			WHERE 
				code IN ($kode)"
			);
	}

	
	function update_data_m($data){
			$query = $this->db->query("select id_kab from tb_institusi");
			if ($query->num_rows() > 0)
			{
				$row=$query->row();
				$kode_kab=$row->id_kab;
			}

			$time=date('Y-m-d H:i:s A');
			$id_user=$this->session->userdata('id');

			$sql = $this->db->query("
				UPDATE `ref_puskesmas` 
				SET
						`KODE_KEC`=?,
						`telp`=?,
						`KODE_DESA`=?,
						`no_fax`=?,
						
						`JENIS_PUSK`=?,
						`poned`=?,
						`ALAMAT_PUSKESMAS`=?,
						`dtpk`=?,
						`email`=?,
						`ex`=?,
						`ye`=?,
						`create_time`=?,
						`create_by`=?,
						`NAMA_PUSKES`=?,
						`KODE_KAB`=?
					WHERE
					`KD_PUSK`=?
				",
				array(
					$data['kode_kec'],
					$data['telp'],
					$data['desa'],
					$data['fax'],
					
					$data['jenis_up'],
					$data['poned'],
					$data['alamat_up'],
					$data['dtpk'],
					$data['email'],
					$data['long'],
					$data['lat'],
					$time,$id_user,
					$data['nama_puskes'],
					$kode_kab,
					$data['kode_up']
				)
			);
			//return $sql;	
		}

	function getDataInstitusi(){
		$query=$this->db->query("
			select * from tb_institusi
			");
		return $query->row_array();
	}

	function input_data_kab($data){
		$kodeprop=$data['kode_prop'];
		// $query_lastkab=$this->db->query("
		// 	select * from ref_kabupaten where CPropID='$kodeprop'
		// 	order by CKabID desc limit 1
		// 	");
		// $temp=$query_lastkab->row_array();
		$new_kode_prop=$data['kode_kab'];//$temp['CKabID']+1;

		$time=date('Y-m-d H:i:s A');
		$id_user=$this->session->userdata('id');

		$sql = $this->db->query("
			INSERT INTO `ref_kabupaten` (
					`CPropID`,
					`CKabID`,
					`CKabDescr`,
					`ex`,
					`ye`
			) VALUES (
				?,?,?,?,?
			)",
			array(
				$kodeprop,
				$new_kode_prop,
				$data['nama_kab'],
				$data['long'],
				$data['lat']
			)
		);

	}
}
?>
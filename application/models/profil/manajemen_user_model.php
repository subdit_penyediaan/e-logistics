<?php
Class Manajemen_User_Model extends CI_Model {
	function input_data_m($data){
		$id_user = $this->session->userdata('id');
		$sql = $this->db->query("
			INSERT INTO `tb_user` (
					`username`,
					`pswd`,
					`long_name`,
					`email`,
					`cp`,
					`jabatan`,
					`group_id`,
					`kode_puskesmas`
			) VALUES (
				?,?,?,?,?,?,?,?
			)",
			array(
				$data['username'],
				$data['pswd'],
				$data['long_name'],
				$data['email'],
				$data['cp'],
				$data['jabatan'],
				$data['group_id'],
				$data['kode_puskesmas']
			)
		);

		return $sql;
	}

	function countAllData(){
		return $this->db->count_all("tb_user");
	}

	function deleteData($kode){		
		$data = array(
			'status'=>'0',
			'delete_time'=> date('Y-m-d H:i:s')
			);
		$this->db->where_in('id_user', $kode);

		return $this->db->update('tb_user',$data);
	}

	function getData($limit ='', $start=''){
			if($start=='')$start=0;
			// $this->db->limit($limit, $start);
			$this->db->where('status','1');
			$query=$this->db->get("tb_user");
			return $query->result();
		}

	function getDataToChange($key){
		$this->db->where('id_user', $key);		
		$query = $this->db->get('tb_user');
		
	    return $query;
	}

	function update_data_m($data){
		$time=date('Y-m-d H:i:s');

		$sql = $this->db->query("
			UPDATE `tb_user` 
			SET
					`long_name`=?,
					`email`=?,
					`jabatan`=?,
					`cp`=?,
					`group_id`=?,
					`kode_puskesmas`=?,
					`update_time`=?
			WHERE 
				`id_user`=?
			",
			array(
				$data['long_name'],
				$data['email'],
				$data['jabatan'],
				$data['cp'],
				$data['group_id'],$data['kode_puskesmas'],$time,
				$data['id_user']
			)
		);

		return $sql;
	}	
}
?>
<?php
Class Pkm_detail_model extends CI_Model {

	function proses($mode,$timenow,$bulan,$tahun,$wil)
    {
      $hasil = "error";
      switch($mode)
      {
          case "pkm" :
           $tahun=date('Y');
           $bulan=date('m');
           $hari=date('d');
           $current=$timenow;
           
           $namatable = $mode."_".$bulan."_".$tahun."_".$wil;
        
            $this->db->query("DROP TABLE IF EXISTS $namatable");
           $h=$this->db->query("
              create table $namatable as
              SELECT l.kode_pusk,dl.kode_obat as kode_obat,SUM(distinct(dl.sedia)) AS sedia,
              dl.nama_obj
              FROM tb_detail_lplpo dl
              JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
              JOIN ref_puskesmas rp ON(rp.KD_PUSK=l.kode_pusk)
                WHERE EXTRACT(MONTH FROM l.periode)=$bulan AND EXTRACT(YEAR FROM l.periode)=$tahun
              GROUP BY l.kode_pusk,dl.kode_obat
              ORDER BY l.kode_pusk
            ");
           if($h)
           {
               $query=$this->db->query("select * from $namatable");
           }
           $hasil = $query->result();
          break;
      }
      return $hasil;
	}
}
?>
<?php
Class Integrasi_Model extends CI_Model {

    function proses($mode,$timenow,$bulan,$tahun,$wil)
    {
      $hasil = "error";
      switch($mode)
      {
            case "ketersediaanobat" :
               // $tahun=date('Y');
               // $bulan=date('m');
               $hari='28';
               $current=$timenow;
               if(($bulan-3)>0){
                      $default=$tahun.'-'.($bulan-3).'-'.$hari;
               }elseif(($bulan-3)==0){ //maret
                      //desember tahun sebelumnya
                      $default=($tahun-1).'-12-'.$hari;
               }elseif(($bulan-3)==-1){ //februari
                      //november tahun sebelumnya
                      $default=($tahun-1).'-11-'.$hari;
               }else{ //januari
                      //oktober tahun sebelumnya
                      $default=($tahun-1).'-10-'.$hari;
               }
            $namatable = $mode."_".$bulan."_".$tahun."_".$wil;
           //$this->db->query("CALL s_ketersedian_obat('$default','$current','$wil')");
            $suffix = "";
            $where = $where2 = $where3 = $where4 = "";
            $join = "";

            // $where .= "AND (p.tanggal BETWEEN '".$default."' AND '".$current."') ";
            $where4 .= "AND (dd.create_time BETWEEN '".$default."' AND '".$current."') ";
            $where3 .= "AND (pem.cur_date BETWEEN '".$default."' AND '".$current."') ";
            $where2 .= "AND (dd.create_time BETWEEN '2013-01-01' AND '".$current."') ";
            $this->db->query("DROP TABLE IF EXISTS $namatable");
            $sql = "
                create table $namatable as
                SELECT kode_obat,nama_obj,(FLOOR((SUM(rerata_pengeluaran)/3))) AS jml_penggunaan,
                (SUM(saldo_persediaan)- (SUM(pengeluaran) + SUM(jml_rusak))) AS jml_stok,
                if ((FLOOR((SUM(rerata_pengeluaran)/3))) = 0, 'tak terhingga', ROUND(((SUM(saldo_persediaan)- (SUM(pengeluaran) + SUM(jml_rusak)))/(FLOOR((SUM(rerata_pengeluaran)/3)))))) AS cur_mont,
                'all' as kelompok
                FROM (
                SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, df.jumlah_kec AS saldo_persediaan,
                0 AS pengeluaran,0 AS jml_rusak,
                so.stok AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
                p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
                FROM tb_stok_obat so
                JOIN tb_detail_faktur df ON(df.id = so.id_stok)
                JOIN tb_penerimaan p ON(p.id = so.id_faktur)
                JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
                    $join
                WHERE 1=1 $where
                GROUP BY so.id_stok

                UNION

                SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
                IFNULL(SUM(dd.pemberian),0) AS pengeluaran,
                0 AS jml_rusak,
                0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
                p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
                FROM tb_stok_obat so
                JOIN tb_detail_faktur df ON(df.id = so.id_stok)
                JOIN tb_penerimaan p ON(p.id = so.id_faktur)
                LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
                JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
                    $join
                WHERE 1=1 $where2
                GROUP BY so.id_stok

                UNION

                SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
          			0 AS pengeluaran,
          			0 AS jml_rusak,
          			0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
          			p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok,
          			IFNULL(SUM(dd.pemberian),0) AS rerata_pengeluaran
          			FROM tb_stok_obat so
          			JOIN tb_detail_faktur df ON(df.id = so.id_stok)
          			JOIN tb_penerimaan p ON(p.id = so.id_faktur)
          			LEFT JOIN tb_detail_distribusi dd ON(dd.id_stok=so.id_stok)
          			JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
          				$join
          			WHERE 1=1 $where4
          			GROUP BY so.id_stok

                UNION

                SELECT so.kode_obat AS kode_obat,so.nama_obj AS nama_obj, 0 AS saldo_persediaan,
                0 AS pengeluaran,IFNULL(CASE dp.status WHEN 0 THEN dp.jumlah END,0) AS jml_rusak, 0 AS stok, df.harga AS harga,so.expired, p.dana AS sumber_dana,so.no_batch AS no_batch,
                p.tahun_anggaran,roa.org_pembuat AS pabrik,roa.deskripsi AS sediaan,so.id_stok, 0 as rerata_pengeluaran
                FROM tb_stok_obat so
                JOIN tb_detail_faktur df ON(df.id = so.id_stok)
                JOIN tb_penerimaan p ON(p.id = so.id_faktur)
                LEFT JOIN tb_detail_pemusnahan dp ON(dp.id_stok=so.id_stok)
                JOIN tb_pemusnahan pem ON pem.id = dp.id_tb_pemusnahan
                JOIN ref_obat_all roa ON roa.id_obat = so.kode_obat
                    $join
                WHERE 1=1 $where3
                GROUP BY so.id_stok) ex

                GROUP BY id_stok
                ORDER BY nama_obj
           ";
           $h=$this->db->query($sql);
           // print_r($sql);
           // die();
           //$namatable = $mode."_".$bulan."_".$tahun."_".$wil;

           if($h)
           {
               $query=$this->db->query("select * from $namatable");
           }
           $hasil = $query->result();
          break;
      }
      return $hasil;
	}
}
?>

<?php
Class Log_Sinkron_Model extends CI_Model {

    function getData($namatable)
    {
        return $this->db->get($namatable)->result();
    }

    function DoAdd($data)
    {
        date_default_timezone_set('Asia/Jakarta');

        $log['id'] = date('d-m-Y')."/".date('H:i:s');
        $log['periode'] = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];
        $log['namatable'] = $data['namatable'];
        $log['jenis_laporan']=1;
        $hasil = $this->db->insert('log_sinkron', $log);
        return $hasil;
    }

	function GetList($search,$filter,$mode,$limit = NULL, $offset = 0)
    {
        if($mode == "total")
        {
            $this->db->select('COUNT(b.id) AS total_book');
        }
        else
        {
            $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
            $this->db->where('b.jenis_laporan', '1');
            $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
            $this->db->order_by("b.id", 'ASC');
        }

        if($mode == "total")
        {
            return (int) $this->db->get('log_sinkron AS b', 1)->row()->total_book;
        }
        else
        {
            return $this->db->get('log_sinkron AS b', $limit, $offset)->result();
        }
	}

    function DoUpdate($data){
        //date_default_timezone_set('Asia/Jakarta');

        $id = $data['periode'];
        //$log['periode'] = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];
        //$log['namatable'] = $data['namatable'];
        //$log['jenis_laporan'] = 1;
        $this->db->where('id', $id);
        $hasil = $this->db->update('log_sinkron', $log); 
        //$hasil = $this->db->insert('log_sinkron', $log);
        return $hasil;
    }

    function getData2($limit,$start){
        //$this->db->where('jenis_laporan', '1');
        //$this->db->limit($limit, $start);
        //$this->db->order_by("KECDESC");
        //$this->db->order_by("NAMA_PUSKES");
        //$query=$this->db->get("log_sinkron");
        //$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
        //return $query->result();

        $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
        $this->db->where('b.jenis_laporan', '1');
        $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
        $this->db->order_by("b.id", 'ASC');
        $this->db->limit($limit, $start);
        return $this->db->get('log_sinkron AS b')->result();
    }

    function countAllData(){
        $this->db->where('jenis_laporan', '1');
        return $this->db->count_all_results("log_sinkron");
    }
}
?>
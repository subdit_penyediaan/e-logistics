<?php
Class Log_sinkron_ukp4_model extends CI_Model {

    function getData($namatable)
    {
        return $this->db->get($namatable)->result();
    }

    function DoAdd($data)
    {
        date_default_timezone_set('Asia/Jakarta');

        $log['id'] = date('d-m-Y')."/".date('H:i:s');
        $log['periode'] = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];
        $log['namatable'] = $data['namatable'];
        $hasil = $this->db->insert('log_sinkron', $log);
        return $hasil;
    }

	function GetList($search,$filter,$mode,$limit = NULL, $offset = 0)
    {
        if($mode == "total")
        {
            $this->db->select('COUNT(b.id) AS total_book');
        }
        else
        {
            $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
            $this->db->where('b.jenis_laporan', '4');
            $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
            $this->db->order_by("b.id", 'DESC');
        }

        if($mode == "total")
        {
            return (int) $this->db->get('log_sinkron AS b', 1)->row()->total_book;
        }
        else
        {
            return $this->db->get('log_sinkron AS b', $limit, $offset)->result();
        }
	}

    function getData2($limit,$start){        

        $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
        $this->db->where('b.jenis_laporan', '4');
        $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
        $this->db->order_by("b.id", 'ASC');
        $this->db->limit($limit, $start);
        return $this->db->get('log_sinkron AS b')->result();
    }

    function countAllData(){
        $this->db->where('jenis_laporan', '4');
        return $this->db->count_all_results("log_sinkron");
    }
}
?>
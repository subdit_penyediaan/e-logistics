<?php
Class Log_sinkron_distribusi_model extends CI_Model {

    function getData($namatable)
    {
        $this->db->select($namatable.'.*');
        $this->db->select('ref_obat_all.object_name as nama_obj, ref_obat_all.deskripsi as sediaan');
        $this->db->select("ifnull(ref_puskesmas.NAMA_PUSKES, unit_penerima) as unit_penerima", FALSE);
        $this->db->join('ref_obat_all', 'ref_obat_all.id_obat = '.$namatable.'.kode_obat');
        $this->db->join('ref_puskesmas', 'ref_puskesmas.KD_PUSK = '.$namatable.'.kode_penerima', 'left');
        // $this->db->join('tb_unitexternal', 'tb_unitexternal.id = '.$namatable.'.kode_penerima', 'left'))

        return $this->db->get($namatable)->result();
    }

    function DoAdd($data)
    {
        date_default_timezone_set('Asia/Jakarta');

        $log['id'] = date('d-m-Y')."/".date('H:i:s');
        $log['periode'] = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];
        $log['namatable'] = $data['namatable'];
        $log['jenis_laporan'] = 5;
        $hasil = $this->db->insert('log_sinkron', $log);
        return $hasil;
    }

	function GetList($search,$filter,$mode,$limit = NULL, $offset = 0)
    {
        if($mode == "total")
        {
            $this->db->select('COUNT(b.id) AS total_book');
        }
        else
        {
            $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
            $this->db->where('b.jenis_laporan', '2');
            $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
            //$this->db->order_by("b.id", 'DESC');
            $this->db->order_by("b.id", 'ASC');
        }

        if($mode == "total")
        {
            return (int) $this->db->get('log_sinkron AS b', 1)->row()->total_book;
        }
        else
        {
            return $this->db->get('log_sinkron AS b', $limit, $offset)->result();
        }
	}

    function DoUpdate($data){
        $id = $data['periode'];
        $log['cara_kirim'] = $data['cara_kirim'];
        $log['status'] = $data['status'];
        $log['id_user'] = $data['id_user'];

        $this->db->where('id', $id);
        $hasil = $this->db->update('log_sinkron', $log);

        return $hasil;
    }

    function getData2($limit,$start){

        $this->db->join('tb_user AS a', 'a.id_user = b.id_user');
        $this->db->where('b.jenis_laporan', '5');
        $this->db->select('b.id,b.periode,b.cara_kirim,b.status,a.username,b.namatable');
        $this->db->order_by("b.id", 'ASC');
        $this->db->limit($limit, $start);
        return $this->db->get('log_sinkron AS b')->result();
    }

    function countAllData(){
        $this->db->where('jenis_laporan', '5');
        return $this->db->count_all_results("log_sinkron");
    }
}
?>

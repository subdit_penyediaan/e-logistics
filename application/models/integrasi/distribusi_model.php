<?php
Class Distribusi_model extends CI_Model {

	function proses($mode,$timenow,$bulan,$tahun,$wil)
  {
      $hasil = "error";
      $periode = $tahun.'-'.$bulan.'-00';
      switch($mode) {
          case "distribusi" :
              $namatable = $mode."_".$bulan."_".$tahun."_".$wil;
              $this->db->query("DROP TABLE IF EXISTS $namatable");
              $h=$this->db->query("
                  create table $namatable as
                  select kode_obat_res as kode_obat, pemberian, tb_lplpo.periode, kode_pusk as kode_penerima,
									ref_puskesmas.NAMA_PUSKES as unit_penerima
                  from tb_detail_distribusi
                  join tb_lplpo on tb_lplpo.id = tb_detail_distribusi.id_lplpo
									join ref_puskesmas on ref_puskesmas.KD_PUSK = tb_lplpo.kode_pusk
                  where tb_lplpo.periode = '$periode' and pemberian > 0
                  union all
                  select kode_obat_res as kode_obat, pemberian, tb_distribusi.periode, kode_up as kode_penerima,
									unit_eks as unit_penerima
                  from tb_detail_distribusi
                  join tb_distribusi on tb_distribusi.id_distribusi = tb_detail_distribusi.id_distribusi
                  where year(tb_distribusi.periode) = '$tahun' and month(tb_distribusi.periode) = '$bulan'
												and pemberian > 0
                  order by kode_obat, kode_penerima
              ");
              if ($h) {
                  $query=$this->db->query("select * from $namatable");
              }
              $hasil = $query->result();

          break;
      }

      return $hasil;
	}
}
?>

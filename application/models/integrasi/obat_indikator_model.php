<?php
Class obat_indikator_model extends CI_Model {

	function proses($mode,$timenow,$bulan,$tahun,$wil)
    {
      $hasil = "error";
      switch($mode)
      {
          case "obatindikator" :
           // $tahun=date('Y');
           // $bulan=date('m');
           // $hari=date('d');
           $current=$timenow;
           /*if(($bulan-3)>0){
                  $default=$tahun.'-'.($bulan-3).'-'.$hari;
           }elseif(($bulan-3)==0){
                  //desember tahun sebelumnya
                  $default=($tahun-1).'-12-'.$hari;
           }elseif(($bulan-3)==-1){
                  //november tahun sebelumnya
                  $default=($tahun-1).'-11-'.$hari;
           }else{
                  //oktober tahun sebelumnya
                  $default=($tahun-1).'-10-'.$hari;
           }*/
           //$namatable = $mode."_".$bulan."_".$tahun."_".$wil;
           $namatable = $mode."_".$bulan."_".$tahun."_".$wil;
           //$this->db->query("CALL s_kadaluarsa_obat('$default','$current','$wil')");
           /*$this->db->query("
              CREATE TABLE $namatable AS
              SELECT so.kode_obat AS kode_obat, oa.deskripsi AS sediaan,
              so.stok AS stok, so.expired AS ed,so.nama_obj AS nama_obj,p.dana AS dana,
              ((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak
              FROM tb_stok_obat so
              JOIN ref_obat_all oa ON (oa.id_obat = so.kode_obat)
              JOIN tb_penerimaan p ON (p.no_faktur = so.no_faktur)
              WHERE so.flag=1
              ORDER BY jarak DESC
            ");*/
            $this->db->query("DROP TABLE IF EXISTS $namatable");
           $h=$this->db->query("
              create table $namatable as
              SELECT IF(ISNULL(l.kode_pusk),'',l.kode_pusk) AS kode_pusk,rb.id_obatindikator AS id_obatindikator,ro.nama_indikator,SUM(dl.terima + dl.stok_akhir) AS sedia
                  FROM ref_obatindikator ro
                  JOIN ref_obatgenerik rb ON rb.id_obatindikator = ro.id_indikator
                  LEFT JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = rb.id
                  LEFT JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
                  WHERE MONTH(l.periode) = $bulan AND YEAR(l.periode) = $tahun AND ro.delete_time IS NULL
                  GROUP BY rb.id_obatindikator,l.kode_pusk

                  UNION ALL

                  SELECT rob.id_indikator IS NULL AS kode_pusk,rob.id_indikator AS id_obatindikator, rob.nama_indikator,rob.status IS NULL AS sedia
                  FROM ref_obatindikator rob
                  WHERE rob.delete_time IS NULL AND rob.id_indikator NOT IN (
                    SELECT rb.id_obatindikator
                    FROM ref_obatindikator ro
                    JOIN ref_obatgenerik rb ON rb.id_obatindikator = ro.id_indikator
                    LEFT JOIN tb_detail_lplpo_kel dl ON dl.kode_generik = rb.id
                    LEFT JOIN tb_lplpo l ON(l.id=dl.id_lplpo)
                    WHERE MONTH(l.periode) = $bulan AND YEAR(l.periode) = $tahun AND ro.delete_time IS NULL
                    GROUP BY rb.id_obatindikator
                    )
              ORDER BY id_obatindikator
            ");
           if($h)
           {
               $query=$this->db->query("select * from $namatable");
           }
           $hasil = $query->result();
          break;
      }
      return $hasil;
	}
}
?>

<?php
Class Data_obat_model extends CI_Model {

	function input_data_m($data){
		$empty="";
		$sql = $this->db->query("
			INSERT INTO `ref_obat_all` (
					`id_obat`,`barcode`,`kode_binfar`,`object_name`,
					`nama_obat`,`deskripsi`,`kekuatan`,`tipe_sediaan`,
					`gol_obat`,`detil_kemasan`,`kemasan_unit`,
					`satuan_jual_unit`,`satuan_klinis`,
					`satuan_klinis_unit`,`org_pembuat`,
					`status`,`kategori`,`jenis_obat`,`kategori_objek`,`id_generik`
			) VALUES (
				?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?
			)",
			array(
				$data['new_kode_master'],$data['barcode'],$data['kode_binfar'],
				$data['nama_obat'],$data['nama_dagang'],$data['sediaan'],
				$data['text_kemasan'],//$empty,
				$empty,//$data['tipe_sediaan?'],
				$data['gol_obat'],
				$data['detil_kemasan'],//$empty,//$data['detil_kemasan?'],
				$data['kemasan'],
				$empty,//$data['satuan_jual_unit?'],
				$data['val_satuan_klin'],$data['satuan_klin'],$data['produsen'],
				$data['status'],$data['kategori'],$data['jenis_obat'],
				$data['kategori_objek'],$data['kode_generik']
			)
		);
		return $sql;	
	}

	function getlistfornas($key){
			
	    $query = $this->db->query("select * from ref_fornas where nama_obat like '%$key%'");
	    //$query->result_array() as $row;
	    
	    $r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id'	=>$key->id_fornas,
	    		'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obat
	    	);
	    }
	    return $r;

	   //echo(json_encode($return));
	    //return $query->result_array();
	}

	function getSediaan(){
		$query=$this->db->query("
			select *
			from  ref_obat_sediaan
			
			");
		return $query->result_array();
	}

	function getSatuan(){
		$query=$this->db->query("
			select *
			from  ref_obat_satuan
			
			");
		return $query->result_array();
	}
	function getGolongan(){
		$query=$this->db->query("
			select *
			from  ref_obat_gol
			
			");
		return $query->result_array();
	}

	function GetListPabrik($key){
	    $query = $this->db->query("
	    	select * from ref_pabrik 
	    	where nama_prod_obat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->nama_prod_obat
	    	);
	    }
	    return $r;
	}

	function GetListFDA($key){
	    $query = $this->db->query("
	    	select * from ref_alkesgenerik
	    	where generik_name like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->generik_name,
	    		'kode_fda'	=>$key->id_sys
	    	);
	    }
	    return $r;
	}

	function GetListINN($key){
	    $query = $this->db->query("
	    	select * from ref_obatgenerik
	    	where generik_object like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->generik_object,
	    		'kode_fda'	=>$key->id
	    	);
	    }
	    return $r;
	}
}
?>
<?php
Class Obat_indikator_model extends CI_Model {

	function countAllData(){
		$this->db->where('status',1);
		return $this->db->count_all_results("ref_obatindikator");
	}


	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$this->db->where('status',1);
		$query=$this->db->get("ref_obatindikator");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_obatindikator
			WHERE nama_indikator like '%$key%'
			");
		return $query->result();
	}

	function GetInfo($kode){
		//unionkan dengan ref alkes
		$query=$this->db->query("
			select * from ref_obatindikator
			where id_indikator='$kode'
			");
		return $query->row_array();
	}
}
?>
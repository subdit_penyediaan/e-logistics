<?php
Class Master_Konten_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `map_obat_zat_id` (
					`id_konten`,
					`id_obat`,
					`strength_vol`,
					`container_vol`,
					`strength_unit`,
					`container_unit`
			) VALUES (
				?,?,?,?,?,?
			)",
			array(
				$data['id_konten'],
				$data['kode_obat'],
				$data['strength'],
				$data['container'],
				$data['unit_str'],
				$data['unit_cnt']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_gol");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				map_obat_zat_id
			WHERE 
				id_map IN ($kode)"
			);
	}

	function getData($id_obat){
		
		$query=$this->db->query("
			SELECT moz.id_map AS id_map,moz.strength_vol AS strength_vol,
			ros1.satuan_obat AS satuan_strength,moz.container_vol AS container_vol,
			ros2.satuan_obat AS satuan_container,rk.nama_konten AS nama_konten
			FROM  map_obat_zat_id moz
			LEFT JOIN ref_obat_satuan ros1 ON(ros1.id=moz.strength_unit)
			LEFT JOIN ref_obat_satuan ros2 ON(ros2.id=moz.container_unit)
			JOIN ref_konten rk ON(rk.id=moz.id_konten)
			WHERE moz.id_obat='$id_obat'
			");
		return $query->result();
	}

	function getSatuan(){
		$query=$this->db->query("
			select *
			from  ref_obat_satuan
			
			");
		return $query->result_array();
	}

	function GetListKonten($key){
    	
	    $query = $this->db->query("
	    	select * from ref_konten
	    	where nama_konten like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_konten'	=>$key->id,
	    		'value'	=>$key->nama_konten
	    		//'category'	=>$key->parent
	    	);
	    }
	    return $r;
	}

	function GetListCui($key){
    	
	    $query = $this->db->query("
	    	select * from ref_cui
	    	where name_cui like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'code_cui'	=>$key->code_cui,
	    		'value'	=>$key->name_cui
	    	);
	    }
	    return $r;
	}

	function input_master_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_konten` (
					`nama_konten`,
					`kode_cui`
			) VALUES (
				?,?
			)",
			array(
				$data['nama_konten'],
				$data['kode_cui']
			)
		);
		//return $sql;	
	}
}
?>
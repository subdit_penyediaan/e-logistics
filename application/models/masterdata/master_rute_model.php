<?php
Class Master_Rute_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `map_obat_rute` (
					`id_obat`,
					`id_rute`
			) VALUES (
				?,?
			)",
			array(
				$data['kode_obat'],
				$data['id_rute']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_gol");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				map_obat_rute
			WHERE 
				id_map IN ($kode)"
			);
	}

	function getData($id_obat){
		//$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->query("
			select mor.id_map as id_map, ror.rute_obat as nama_rute
			from map_obat_rute mor
			join ref_obat_rute ror on(ror.id=mor.id_rute)
			where mor.id_obat = '$id_obat'
			");
		return $query->result();
	}

	function GetListRute($key){
    	
	    $query = $this->db->query("
	    	select * from ref_obat_rute
	    	where rute_obat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_rute'	=>$key->id,
	    		'value'	=>$key->rute_obat
	    	);
	    }
	    return $r;
	}
}
?>
<?php
Class Master_Ukp4_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `map_obat_ukp4` (
					`id_ukp4`,
					`id_obat`
			) VALUES (
				?,?
			)",
			array(
				$data['id_ukp4'],
				$data['kode_obat']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_gol");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				map_obat_ukp4
			WHERE 
				id_map IN ($kode)"
			);
	}

	function getData($id_obat){
		$query=$this->db->query("
			select mou.id_map as id_map,mou.id_ukp4 as id_ukp4,
			ru.nama_ukp4 as nama_ukp4,ru.kemasan as kemasan
			from map_obat_ukp4 mou
			join ref_ukp4 ru on(ru.id_ukp4=mou.id_ukp4)
			where mou.id_obat='$id_obat'
			");
		return $query->result();
	}

	function GetListUkp4($key){
    	
	    $query = $this->db->query("
	    	select * from ref_ukp4
	    	where nama_ukp4 like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_ukp4'	=>$key->id_ukp4,
	    		'value'	=>$key->nama_ukp4
	    	);
	    }
	    return $r;
	}
}
?>
<?php
Class Detail_Data_Obat_Model extends CI_Model {

	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `stok_obat` (
					`nama_obat`,
					`expired`,
					`stok`,
					`harga`,
					`sumber_dana`
			) VALUES (
				?,?,?,?,?
			)",
			array(
				$data['nama_obat_fornas'],
				$data['tanggal_ex'],
				$data['jumlahstok'],
				$data['harga'],
				$data['sumber_dana']
			)
		);
		//return $sql;	
	}


	function getlistfornas($key){
			
	    $query = $this->db->query("select * from ref_fornas where nama_obat like '%$key%'");
	    //$query->result_array() as $row;
	    
	    $r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id'	=>$key->id_fornas,
	    		'label'	=>$key->nama_obat,
	    		'value'	=>$key->nama_obat
	    	);
	    }
	    return $r;

	   //echo(json_encode($return));
	    //return $query->result_array();
	}

	function getSediaan(){
		$query=$this->db->query("
			select *
			from  ref_obat_sediaan
			
			");
		return $query->result_array();
	}

	function getSatuan(){
		$query=$this->db->query("
			select *
			from  ref_obat_satuan
			
			");
		return $query->result_array();
	}
	function getGolongan(){
		$query=$this->db->query("
			select *
			from  ref_obat_gol
			
			");
		return $query->result_array();
	}

	function GetListPabrik($key){
	    $query = $this->db->query("
	    	select * from ref_pabrik 
	    	where nama_prod_obat like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		//'kode_obat'	=>$key->kode_obat,
	    		//'label'	=>$key->SuplierName,
	    		'value'	=>$key->nama_prod_obat
	    	);
	    }
	    return $r;
	}
	
}
?>
<?php
Class Master_Atc_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `map_obat_atc` (
					`id_obat`,
					`atc`
			) VALUES (
				?,?
			)",
			array(
				$data['kode_obat'],
				$data['kode_atc']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_gol");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				map_obat_atc
			WHERE 
				id_map IN ($kode)"
			);
	}

	function getData($id_obat){
		$query=$this->db->query("
			select moa.id_map as id_map,
			moa.atc as kode_atc, rwa.nama_atc as nama_atc,
			rwa.nama_atc_eng as nama_atc_eng
			from map_obat_atc moa
			join ref_who_atc rwa on(rwa.kode_atc=moa.atc)
			where id_obat='$id_obat'
			");
		return $query->result();
	}

	function GetListAtc($key){
    	
	    $query = $this->db->query("
	    	select * from ref_who_atc
	    	where nama_atc like '%$key%' or nama_atc_eng like '%$key%'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_atc'	=>$key->kode_atc,
	    		'value'	=>$key->nama_atc//.'<br><i>eng: '.$key->nama_atc_eng.'</i>'
	    		//'category'	=>$key->parent
	    	);
	    }
	    return $r;
	}
}
?>
<?php
Class Master_generik_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_obatgenerik` (
					`generik_object`,
					`dosage_form`,
					`strength`,
					`nama_zat`
			) VALUES (
				?,?,?,?
			)",
			array(
				$data['nama_master_generik_obat'],
				$data['dosage_form'],
				$data['strength'],
				$data['nama_zat_generik']
			)
		);
		//return $sql;	
		$this->add_hlp_tab($data);
	}

	function add_hlp_tab($data){
		$id = $this->db->query("
			select id from ref_obatgenerik order by id desc limit 1
			");
		$temp = $id->row_array();
		$this->db->query("
			insert into hlp_generik (id_hlp,nama_objek,sediaan)
			values (?,?,?)",
			array($temp['id'],
				$data['nama_master_generik_obat'],
				$data['dosage_form'])
			);
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_obatgenerik
			WHERE 
				id IN ($kode)"
			);
	}

	function countAllData(){
		return $this->db->count_all("ref_obatgenerik");
	}

	function getData($limit,$start){
		$this->db->select('gen.*,pro.nama_program,ind.nama_indikator');
		$this->db->limit($limit, $start);
		$this->db->join('ref_obatprogram pro','pro.id=gen.id_obatprogram','left');
		$this->db->join('ref_obatindikator ind','ind.id_indikator=gen.id_obatindikator','left');
		$query=$this->db->get("ref_obatgenerik gen");
		//$query=$this->db->query("
		//	select * from ref_obat_sediaan limit $start,$limit
		//");
		return $query->result();
	}

	function GetInfoObat($kode){
		//unionkan dengan ref alkes
		$query=$this->db->query("
			select * from ref_obatgenerik
			where id='$kode'
			");
		return $query->row_array();
	}

	function update_data_m($data){
		$sql = $this->db->query("
			UPDATE `ref_obatgenerik` 
			SET
				`generik_object`=?,
				`dosage_form`=?,
				`strength`=?,
				`nama_zat`=?,
				`id_obatindikator`=?,
				`id_obatprogram`=?
			WHERE
				`id`=?
			",
			array(
				$data['nama_master_generik'],
				$data['dosage_form'],
				$data['strength'],
				$data['nama_zat_generik'],
				$data['id_obatindikator'],
				$data['id_obatprogram'],
				$data['kode_generik']
			)
		);
		//return $sql;	
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT gen.*,pro.nama_program,ind.nama_indikator
			FROM ref_obatgenerik gen
			left join ref_obatindikator ind on ind.id_indikator=gen.id_obatindikator
			left join ref_obatprogram pro on pro.id=gen.id_obatprogram
			WHERE generik_object like '%$key%' OR dosage_form like '%$key%' OR nama_zat like '%$key%'
			");
		return $query->result();
	}
}
?>
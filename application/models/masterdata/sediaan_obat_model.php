<?php
Class Sediaan_Obat_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_obat_sediaan` (
					`jenis_sediaan`
			) VALUES (
				?
			)",
			array(
				$data['nama_sediaan_obat']
			)
		);
		//return $sql;	
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_obat_sediaan
			WHERE 
				id IN ($kode)"
			);
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_sediaan");
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_obat_sediaan");
		//$query=$this->db->query("
		//	select * from ref_obat_sediaan limit $start,$limit
		//");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_obat_sediaan
			WHERE jenis_sediaan like '%$key%'
			");
		return $query->result();
	}
}
?>
<?php
Class Produsen_Obat_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_pabrik` (
					`nama_prod_obat`
			) VALUES (
				?
			)",
			array(
				//$data['nama_gol_obat'],
				$data['nama_produsen']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_pabrik");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_pabrik
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_pabrik");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_pabrik
			WHERE nama_prod_obat like '%$key%'
			");
		return $query->result();
	}
}
?>
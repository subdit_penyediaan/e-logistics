<?php
Class Ukp4_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_obat_gol` (
					`nama_golongan`,
					`keterangan`
			) VALUES (
				?,?
			)",
			array(
				$data['nama_gol_obat'],
				$data['ket_gol_obat']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_ukp4");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_obat_gol
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		$this->db->order_by("id");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_ukp4");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_ukp4
			WHERE nama_ukp4 like '%$key%' OR kemasan like '%$key%'
			");
		return $query->result();
	}
}
?>
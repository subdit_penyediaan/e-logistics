<?php
Class Master_alkesgen_Model extends CI_Model {
	function input_data_m($data){
		$id_sys='AK'.$data['kode_alkesgen'];
		$sql = $this->db->query("
			INSERT INTO `ref_alkesgenerik` (
					`kode`,
					`generik_name`,
					`parent`,
					`select`,
					`fda_code`,
					`fda_name`,`id_sys`
			) VALUES (
				?,?,?,?,?,?,?
			)",
			array(
				$data['kode_alkesgen'],
				$data['nama_alkesgen'],
				$data['parent'],
				$data['selected'],
				$data['fda_code'],
				$data['fda_name'],$id_sys
			)
		);
		//return $sql;	
		$this->db->query("
			insert into hlp_generik (id_hlp,nama_objek)
			values (?,?)",array($id_sys,$data['nama_alkesgen'])
			);
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_alkesgenerik
			WHERE 
				kode IN ($kode)"
			);
	}

	function countAllData(){
		return $this->db->count_all("ref_alkesgenerik");
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_alkesgenerik");
		//$query=$this->db->query("
		//	select * from ref_obat_sediaan limit $start,$limit
		//");
		return $query->result();
	}

	function GetInfoObat($kode){
		//unionkan dengan ref alkes
		$query=$this->db->query("
			select * from ref_alkesgenerik
			where kode='$kode'
			");
		return $query->row_array();
	}

	function update_data_m($data){
		$sql = $this->db->query("
			UPDATE `ref_alkesgenerik` 
			SET
				`generik_name`=?,
				`parent`=?,
				`select`=?,
				`fda_code`=?,
				`fda_name`=?
			WHERE
				`kode`=?
			",
			array(				
				$data['nama_alkesgen'],
				$data['parent'],
				$data['selected'],
				$data['fda_code'],
				$data['fda_name'],
				$data['kode_alkesgen']
			)
		);
		//return $sql;	
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_alkesgenerik
			WHERE generik_name like '%$key%' OR fda_name like '%$key%' OR kode like '%$key%'
			");
		return $query->result();
	}
}
?>
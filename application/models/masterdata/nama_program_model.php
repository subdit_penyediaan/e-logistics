<?php
Class Nama_program_model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_pbf` (
					`SuplierName`,
					`alamat`,
					`Telpon`
			) VALUES (
				?,?,?
			)",
			array(
				$data['nama_pbf'],
				$data['almt_pbf'],
				$data['telp_pbf']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		$this->db->where('status',1);
		return $this->db->count_all_results("ref_obatprogram");
	}

	function deleteData($kode){
		/*$this->db->query(
			"DELETE FROM
				ref_pbf
			WHERE 
				id_pbf IN ($kode)"
			);*/
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$this->db->where('status',1);
		$query=$this->db->get("ref_obatprogram");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_obatprogram
			WHERE nama_program like '%$key%'
			");
		return $query->result();
	}

	function GetInfo($kode){
		//unionkan dengan ref alkes
		$query=$this->db->query("
			select * from ref_obatprogram
			where id='$kode'
			");
		return $query->row_array();
	}
}
?>
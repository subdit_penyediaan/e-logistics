<?php
Class Pbf_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_pbf` (
					`SuplierName`,
					`alamat`,
					`Telpon`
			) VALUES (
				?,?,?
			)",
			array(
				$data['nama_pbf'],
				$data['almt_pbf'],
				$data['telp_pbf']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_pbf");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_pbf
			WHERE 
				id_pbf IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_pbf");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_pbf
			WHERE SuplierName like '%$key%' OR alamat like '%$key%'
			");
		return $query->result();
	}
}
?>
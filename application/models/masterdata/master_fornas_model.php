<?php
Class Master_Fornas_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `map_obat_fornas` (
					`id_fornas`,
					`id_obat`
			) VALUES (
				?,?
			)",
			array(
				$data['id_fornas'],
				$data['kode_obat']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_gol");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				map_obat_fornas
			WHERE 
				id_map IN ($kode)"
			);
	}

	function getData($id_obat){
		$query=$this->db->query("
			SELECT mof.id_map AS id_map,
			mof.id_fornas AS id_fornas,rf.nama_obat AS nama_fornas,mp.nama_obat AS parent
			FROM map_obat_fornas mof
			JOIN ref_fornas rf ON(rf.id_fornas=mof.id_fornas)
			JOIN map_fornas_parent mp ON(mp.id_fornas=rf.parent)
			where mof.id_obat='$id_obat'
			");
		return $query->result();
	}

	function GetListFornas($key){
    	
	    $query = $this->db->query("
	    	SELECT rf.id_fornas as id_fornas,rf.nama_obat as nama_obat,
	    	mfp.nama_obat AS parent
			FROM ref_fornas rf
			LEFT JOIN map_fornas_parent mfp ON(mfp.id_fornas= rf.parent)
				    	WHERE rf.nama_obat LIKE '%$key%'
				    	AND rf.selected = 'y'
	    	");
		$r = array();
	    foreach ($query->result() as $key) {
	    	$r[] = array(
	    		'id_fornas'	=>$key->id_fornas,
	    		'value'	=>$key->nama_obat,
	    		'category'	=>$key->parent
	    	);
	    }
	    return $r;
	}
}
?>
<?php
Class Rute_Obat_Model extends CI_Model {
	function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_obat_rute` (
					`rute_obat`
			) VALUES (
				?
			)",
			array(
				$data['nama_rute_obat']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_obat_rute");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_obat_rute
			WHERE 
				id IN ($kode)"
			);
	}

	function getData($limit,$start){
			$this->db->limit($limit, $start);
			//$this->db->order_by("KECDESC");
			//$this->db->order_by("NAMA_PUSKES");
			$query=$this->db->get("ref_obat_rute");
			//$query=$this->db->query("SELECT KECDESC,KDKEC,kdkab FROM ref_puskesmas ORDER BY KECDESC");
			return $query->result();
		}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_obat_rute
			WHERE rute_obat like '%$key%'
			");
		return $query->result();
	}
}
?>
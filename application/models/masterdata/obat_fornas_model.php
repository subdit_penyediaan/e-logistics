<?php
Class Obat_Fornas_Model extends CI_Model {
function input_data_m($data){
		$sql = $this->db->query("
			INSERT INTO `ref_pbf` (
					`SuplierName`,
					`alamat`,
					`Telpon`
			) VALUES (
				?,?,?
			)",
			array(
				$data['nama_pbf'],
				$data['almt_pbf'],
				$data['telp_pbf']
			)
		);
		//return $sql;	
	}

	function countAllData(){
		return $this->db->count_all("ref_fornas");
	}

	function deleteData($kode){
		$this->db->query(
			"DELETE FROM
				ref_pbf
			WHERE 
				id_pbf IN ($kode)"
			);
	}

	function getData($limit,$start){
		$this->db->limit($limit, $start);
		//$this->db->order_by("KECDESC");
		//$this->db->order_by("NAMA_PUSKES");
		$query=$this->db->get("ref_fornas");
		return $query->result();
	}

	function searchData($key){
		$query=$this->db->query("
			SELECT * FROM ref_fornas
			WHERE nama_obat like '%$key%'
			");
		return $query->result();
	}	
}
?>
<?php
	class Elogistik_m extends CI_Model{
		function construct(){
			parent::__construct();
		}

		function getDataPuskesmas($limit,$start){
			$this->db->limit($limit, $start);
			$this->db->order_by("NAMA_PUSKES");
			$query=$this->db->get("ref_puskesmas");
			return $query->result();
		}

		function countAllPuskesmas(){
			return $this->db->count_all("ref_puskesmas");
		}

		function getProfil(){
			$level=$this->db->query("
				select levelinstitusi from tb_institusi
				");
			$result=$level->row_array();
			$profil=$result['levelinstitusi'];
			if($profil==3){
				$query=$this->db->query("
					SELECT i.levelinstitusi as levelinstitusi,kab.CKabDescr AS nama_kab, prop.CPropDescr AS nama_prop
					FROM tb_institusi i
					JOIN ref_kabupaten kab ON (kab.CKabID=i.id_kab)
					JOIN ref_propinsi prop ON(prop.CPropID=kab.CPropID)
				");
			}elseif($profil==1){
				$query=$this->db->query("
					SELECT *
					FROM tb_institusi 
					
				");
			}elseif($profil==2){
				$query=$this->db->query("
					SELECT i.levelinstitusi as levelinstitusi, prop.CPropDescr AS nama_prop
					FROM tb_institusi i
					
					JOIN ref_propinsi prop ON(prop.CPropID=i.id_prop)
				");
			}
			return $query->row_array();
		}

		function cek_stok_opnam(){
			$query=$this->db->query("
				SELECT MONTH(periode_stok_opnam)
				FROM tb_stok_opnam
				WHERE MONTH(periode_stok_opnam) IN (MONTH(CURRENT_DATE()))
				");
			if($query->num_rows()>0){
				return true;
			}else{
				return false;
			}//*/
			//return $query->array_result();
			//return $query->row_array();
		}

		function getMenuParent($data){
			$this->db->distinct();
			$this->db->select('mo.id,mo.label_modul,mo.class,mo.status');
			$this->db->join('sys_menu me', 'me.modul_id = mo.id');
			$this->db->join('sys_group gr', 'gr.id_menu = me.id');
			$this->db->join('tb_institusi ins', 'ins.levelinstitusi=gr.level');			
			if ($data['puskesmas']==3) {
				$this->db->where('puskesmas', '1');
			}
			$this->db->where('mo.status', '1');
			$query = $this->db->get('sys_modul mo');
			// print_r ($this->db->last_query());
			return $query->result();
		}

		function getMenuChild($data){
			$this->db->select('*');
			$this->db->where('status','1');
			if ($data['puskesmas']==3) {
				$this->db->where('puskesmas', '1');
			}
			$query = $this->db->get('sys_menu');

			return $query->result();
		}

		function getMenuAll($data){
			$this->db->select('mo.*, me.*');
			$this->db->join('sys_menu me', 'me.modul_id = mo.id');
			$this->db->join('sys_group gr', 'gr.id_menu = me.id');
			$this->db->join('tb_institusi ins', 'ins.levelinstitusi=gr.level');			
			if ($data['puskesmas']==3) {
				$this->db->where('puskesmas', '1');
			}
			$query = $this->db->get('sys_modul mo');

			return $query->result();
		}

		function getKadaluarsa(){
			$sql = "SELECT so.no_batch as no_batch,so.kode_obat AS kode_obat, so.nama_obj AS nama_obj, oa.deskripsi AS sediaan,
					so.stok AS stok, so.expired AS ed, MONTH(CURDATE())-MONTH(so.expired) AS lama,
					YEAR(so.expired)-YEAR(CURDATE()) AS lamatahun, oa.kekuatan AS kekuatan, p.dana AS dana,p.tahun_anggaran,oa.org_pembuat as pabrik,
					((YEAR(so.expired)-YEAR(CURDATE()))*-12+MONTH(CURDATE())-MONTH(so.expired)) AS jarak
					FROM tb_stok_obat so
					JOIN ref_obat_all oa ON (oa.id_obat = so.kode_obat)
					JOIN tb_penerimaan p ON (p.id=so.id_faktur)
					WHERE so.flag=1
					ORDER BY jarak DESC
					limit 10";
					
			return $this->db->query($sql);
		}

	}
?>